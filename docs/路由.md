# 路由

整个系统由 `静态路由` 和 `动态路由` 两部分组

## 静态路由
直接在 Router 中引入，和用户权限无关
包含:
  - Home => basePath/
  - Login => basePath/login

## 动态路由
所有动态路由在 `routerConfig` 中

## Saga 中进行 url 跳转
```ts
import { push } from 'connected-react-router'
import { put, call } from 'redux-saga/effects'

export function* login(username, password) {
  /* do something before redirection */
  yield put(push('/home'))
}
```
