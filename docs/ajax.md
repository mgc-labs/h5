# Ajax

项目中使用 `axios` 发送 ajax，`axios-mock-adapter` 提供 mock 数据。

- [axios](https://github.com/axios/axios#request-config)
- [axios-mock-adapter](https://github.com/ctimmerm/axios-mock-adapter)

## 典型用法

### 基础用法
```js
import request, { mocker } from 'utils/request';

const requestConfig = {
  url: 'someApiUrl',
  method: 'post'
};
const requestOptions = {
  useMock: false, // 不使用 mock 数据
  globalErrorMsg: true, // 打开全局提示，当请求出错时会有全局的错误提示
};
request(requestConfig, requestOptions)
  .then(response => {
    // response
  })
  .catch(err => {
    // error
  });
```

### 使用 mock 数据
```js
import request, { mocker } from 'utils/request';

const API_URL = 'someApiUrl';

// 注册该 API 的 mock 数据，更多用法查看 axios-mock-adapter 文档
mocker.on('post', API_URL).reply(200, {
  userId: 123456,
  username: 'buddy',
});

const requestConfig = {
  url: API_URL,
  method: 'post'
};
const requestOptions = {
  useMock: true, // 使用 mock 数据
};
request(requestConfig, requestOptions)
  .then(response => {
    // response
  })
  .catch(err => {
    // error
  });
```

#### POST
```js
import qs from 'qs';

request(
  {
    method: 'post',
    url: API_CAPTCHA,
    data: qs.stringify({
      foo: bar,
    }),
  },
);
```

#### GET
```js
request(
  {
    method: 'get',
    url: API_CAPTCHA,
    params: {
      foo: bar,
    },
  },
);
```

