/**
 * 汇总 reducers 并输出
 */
import { LOCATION_CHANGE } from 'connected-react-router/immutable';
import { Map } from 'immutable';
import { Reducer } from 'redux';
import { combineReducers } from 'redux-immutable';

import globalReducer from 'modules/module.app/reducer';
import authorizationReducer from 'modules/module.authorization/reducer';

import { PersistActionTypes } from 'utils/persistence/actionTypes';

import { DESTROY_ACTION, DESTROY_ACTION_PREFIX } from './CONFIG';

const initialState = Map({
  action: null,
  location: null,
});

function routerReducer(state = initialState, action) {
  const { payload, type } = action;
  if (type === LOCATION_CHANGE) {
    const location = payload.location || payload;

    return state.set('location', location).set('action', payload.action);
  }
  return state;
}

const patchReducer = reducer => (state, action) => {
  const { type, payload } = action;
  if (type === PersistActionTypes.STATE_LOAD_SUCCESS) {
    return state.mergeDeep(payload);
  }
  if (type === DESTROY_ACTION) {
    return state.withMutations(map => {
      action.payload.forEach(key => {
        map.delete(key);
      });
      return map;
    });
  }
  if (type.startsWith(DESTROY_ACTION_PREFIX)) {
    const key = type.replace(DESTROY_ACTION_PREFIX, '');
    return state.delete(key);
  }
  return reducer(state, action);
};

export interface IInjectedReducers {
  [propName: string]: Reducer;
}

export default function createReducer(
  injectedReducers?: IInjectedReducers,
): Reducer {
  return patchReducer(
    combineReducers({
      authorization: authorizationReducer,
      global: globalReducer,
      router: routerReducer,
      ...injectedReducers,
    }),
  );
}
