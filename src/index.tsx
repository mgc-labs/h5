import '@babel/polyfill';
import LocaleProvider from 'antd/es/locale-provider/';
import zh_CN from 'antd/es/locale-provider/zh_CN';
import { ConnectedRouter } from 'connected-react-router/immutable';
import { createBrowserHistory } from 'history';
import moment from 'moment';
import 'moment/locale/zh-cn';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import 'url-search-params-polyfill';

moment.locale('zh-cn');

import App from './App';
import configureStore from './configureStore';

import TransportCapacity from 'modules/module.bigScreen.transportCapacity/Loadable';
import Index from 'modules/module.index/';
import Login from 'modules/module.login/Loadable';

const { pathname } = location;
let basename = '/';
if (pathname.length > 0) {
  const paths = pathname.split('/');
  if (paths[1]) {
    basename += paths[1];
  }
}
const initialState = {};
const history = createBrowserHistory({
  basename,
});

ReactDOM.render(
  configureStore(initialState, history)(
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Index} />
        <Route path="/login" exact component={Login} />
        <Route path="/transportCapacity" exact component={TransportCapacity} />
        <Route
          render={() => (
            <ConnectedRouter history={history}>
              <LocaleProvider locale={zh_CN}>
                <App basename={basename} />
              </LocaleProvider>
            </ConnectedRouter>
          )}
        />
      </Switch>
    </BrowserRouter>,
  ),
  document.getElementById('app') as HTMLElement,
);

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
// if (process.env.NODE_ENV === 'production') {
//   require('offline-plugin/runtime').install({
//     autoUpdate: true,
//   });
// }
