/**
 * App 入口
 */
import * as React from 'react';
import { hot } from 'react-hot-loader';
import { Route, Switch } from 'react-router';

import AppComponent from 'modules/module.app';
import Authorization from 'modules/module.authorization/';
import TransportCapacity from 'modules/module.bigScreen.transportCapacity/Loadable';
import Login from 'modules/module.login/Loadable';

import ErrorBoundary from 'components/ErrorBoundary/';

import './theme/default/index.less';

// setConfig({ logLevel: 'debug' });

const App = ({ basename }) => (
  <ErrorBoundary>
    <Switch>
      <Route
        path="/login"
        render={props => <Login {...props} basename={basename} />}
      />
      <Route
        path="/transportCapacity"
        render={props => <TransportCapacity {...props} basename={basename} />}
      />
      <Route
        render={props => (
          <Authorization {...props}>
            <AppComponent basename={basename} />
          </Authorization>
        )}
      />
    </Switch>
  </ErrorBoundary>
);

export default hot(module)(App);
