import CompanyAccount, * as companyAccountUtils from 'modules/module.company.account/Loadable';
import CompanyManagement, * as companyManagementUtils from 'modules/module.company.management/Loadable';
import CompanyStaff, * as companyStaffUtils from 'modules/module.company.staff/Loadable';
import LtlOrderDetail, * as ltlOrderDetailUtils from 'modules/module.ltl.order.detail/Loadable';
import LtlOrderEdit, * as ltlOrderEditUtils from 'modules/module.ltl.order.edit/Loadable';
import LtlOrder, * as ltlOrderUtils from 'modules/module.ltl.order/Loadable';

import IOS, * as IOSUtils from 'modules/module.ios/Loadable';

import HomeUpdate, * as homeUpdateUtils from 'modules/module.home.update/Loadable';
import Proclamation, * as proclamationUtils from 'modules/module.proclamation/Loadable';

import FixpathDetail, * as fixpathDetailUtil from 'modules//module.fixpath.detail/Loadable';
import FixpathAddpath, * as fixpathAddpathUtil from 'modules/module.fixpath.addpath/Loadable';
import FixpathList, * as fixpathListUtils from 'modules/module.fixpath.list/Loadable';
import FixpathSchedule, * as fixpathScheduleUtils from 'modules/module.fixpath.schedule/Loadable';
import WaybillDetail, * as waybillDetaillUtils from 'modules/module.waybill.detail/Loadable';
import WaybillMap, * as waybillMaplUtils from 'modules/module.waybill.map/Loadable';
import Waybill, * as waybillUtils from 'modules/module.waybill/Loadable';

import StowageDetail, * as StowageDetailUtils from 'modules/module.stowage.detail/Loadable';
import StowageManage, * as StowageManageUtils from 'modules/module.stowage.manage/Loadable';
import Stowage, * as StowageUtils from 'modules/module.stowage/Loadable';

import AddAddressManage, * as AddAddressManageUtils from 'modules/module.add.address/Loadable';
import AddressManage, * as AddressManageUtils from 'modules/module.address.manage/Loadable';

import CustomerAdd, * as customerAddUtils from 'modules/module.customer.add/Loadable';
import Customer, * as customerUtils from 'modules/module.customer/Loadable';
import Home, * as homeUtils from 'modules/module.home/Loadable';
import ManageDriver, * as ManageDriverUtils from 'modules/module.manage.driver/Loadable';
import ManageVehicle, * as ManageVehicleUtils from 'modules/module.manage.vehicle/Loadable';
import StaffAuthorization, * as staffAuthorizationUtils from 'modules/module.staff.authorization/Loadable';
// import Test, * as testUtils from 'modules/module.test/Loadable';
import TransportDispatch, * as TransportDispatchUtils from 'modules/module.transport.dispatch/Loadable';
import VehicleOrderAdd, * as vehicleOrderAddUtil from 'modules/module.vehicle.order.add/Loadable';
import vehicleOrderDetail, * as vehicleOrderDetailUtil from 'modules/module.vehicle.order.detail/Loadable';
import VehicleOrder, * as vehicleOrderUtils from 'modules/module.vehicle.order/Loadable';
import { Component } from 'react';
import { LoadableComponent } from 'react-loadable';

interface IConstants {
  [propName: string]: string;
}

interface IActions {
  [propName: string]: () => any;
}

interface IComponentOption {
  actions?: IActions;
  connectModel?: any;
  routeId: string;
}

export interface IRouterConfig {
  component: (config: IComponentOption) => LoadableComponent | Component;
  allowMultipleInstance: boolean; // 是否运行多开
  closable: boolean; // 是否运行关闭
  defaultOpen: boolean; // 是否默认打开
  constants?: IConstants;
  excludeFromMenu?: boolean; // 不出现在左侧菜单中
  actionFactory?: (constants: IConstants, routeId: string) => any;
  modelFactory?: (routeId: string) => any;
  reducerFactory?: (constants: IConstants, routeId: string) => any;
  sagaFactory?: (constants: IConstants, option: any, routeId: string) => any;
  connectModel?: () => any;
  connectModels?: object;
}

const routerConfig: Map<string, IRouterConfig> = new Map([
  [
    '/home',
    {
      allowMultipleInstance: false,
      closable: false,
      component: Home,
      defaultOpen: true,
      constants: homeUtils.constants,
      actionFactory: homeUtils.actionFactory,
      reducerFactory: homeUtils.reducerFactory,
      sagaFactory: homeUtils.sagaFactory,
    },
  ],
  [
    '/customer',
    {
      allowMultipleInstance: true,
      closable: true,
      component: Customer,
      defaultOpen: false,
      constants: customerUtils.constants,
      actionFactory: customerUtils.actionFactory,
      reducerFactory: customerUtils.reducerFactory,
      sagaFactory: customerUtils.sagaFactory,
    },
  ],
  [
    '/customer/add',
    {
      allowMultipleInstance: true,
      closable: true,
      component: CustomerAdd,
      defaultOpen: false,
      modelFactory: customerAddUtils.modelFactory,
    },
  ],
  [
    '/customer/edit',
    {
      allowMultipleInstance: true,
      closable: true,
      component: CustomerAdd,
      defaultOpen: false,
      modelFactory: customerAddUtils.modelFactory,
    },
  ],
  [
    '/ltlOrder',
    {
      allowMultipleInstance: true,
      closable: true,
      component: LtlOrder,
      defaultOpen: false,
      constants: ltlOrderUtils.constants,
      actionFactory: ltlOrderUtils.actionFactory,
      reducerFactory: ltlOrderUtils.reducerFactory,
      sagaFactory: ltlOrderUtils.sagaFactory,
    },
  ],
  [
    '/ltlOrder/edit',
    {
      allowMultipleInstance: true,
      closable: true,
      component: LtlOrderEdit,
      defaultOpen: false,
      constants: ltlOrderEditUtils.constants,
      actionFactory: ltlOrderEditUtils.actionFactory,
      reducerFactory: ltlOrderEditUtils.reducerFactory,
      sagaFactory: ltlOrderEditUtils.sagaFactory,
    },
  ],
  [
    '/ltlOrder/add',
    {
      allowMultipleInstance: true,
      closable: true,
      component: LtlOrderEdit,
      defaultOpen: false,
      constants: ltlOrderEditUtils.constants,
      actionFactory: ltlOrderEditUtils.actionFactory,
      reducerFactory: ltlOrderEditUtils.reducerFactory,
      sagaFactory: ltlOrderEditUtils.sagaFactory,
    },
  ],
  [
    '/ltlOrder/detail',
    {
      allowMultipleInstance: true,
      closable: true,
      component: LtlOrderDetail,
      defaultOpen: false,
      constants: ltlOrderDetailUtils.constants,
      actionFactory: ltlOrderDetailUtils.actionFactory,
      reducerFactory: ltlOrderDetailUtils.reducerFactory,
      sagaFactory: ltlOrderDetailUtils.sagaFactory,
    },
  ],
  [
    '/fixpath/list',
    {
      allowMultipleInstance: false,
      closable: true,
      component: FixpathList,
      defaultOpen: false,
      constants: fixpathListUtils.constants,
      actionFactory: fixpathListUtils.actionFactory,
      reducerFactory: fixpathListUtils.reducerFactory,
      sagaFactory: fixpathListUtils.sagaFactory,
    },
  ],
  [
    '/fixpath/schedule',
    {
      allowMultipleInstance: true,
      closable: true,
      component: FixpathSchedule,
      defaultOpen: false,
      modelFactory: fixpathScheduleUtils.modelFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/fixpath/addpath',
    {
      allowMultipleInstance: true,
      closable: true,
      component: FixpathAddpath,
      defaultOpen: false,
      modelFactory: fixpathAddpathUtil.modelFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/fixpath/updatepath',
    {
      allowMultipleInstance: true,
      closable: true,
      component: FixpathAddpath,
      defaultOpen: false,
      modelFactory: fixpathAddpathUtil.modelFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/fixpath/detail',
    {
      allowMultipleInstance: true,
      closable: true,
      component: FixpathDetail,
      defaultOpen: false,
      modelFactory: fixpathDetailUtil.modelFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/waybill/list',
    {
      allowMultipleInstance: true,
      closable: true,
      component: Waybill,
      defaultOpen: false,
      constants: waybillUtils.constants,
      actionFactory: waybillUtils.actionFactory,
      reducerFactory: waybillUtils.reducerFactory,
      sagaFactory: waybillUtils.sagaFactory,
    },
  ],
  [
    '/waybill/detail',
    {
      allowMultipleInstance: true,
      closable: true,
      component: WaybillDetail,
      defaultOpen: false,
      constants: waybillDetaillUtils.constants,
      actionFactory: waybillDetaillUtils.actionFactory,
      reducerFactory: waybillDetaillUtils.reducerFactory,
      sagaFactory: waybillDetaillUtils.sagaFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/company/account',
    {
      allowMultipleInstance: false,
      closable: true,
      component: CompanyAccount,
      defaultOpen: false,
      constants: companyAccountUtils.constants,
      actionFactory: companyAccountUtils.actionFactory,
      reducerFactory: companyAccountUtils.reducerFactory,
      sagaFactory: companyAccountUtils.sagaFactory,
    },
  ],
  [
    '/company/management',
    {
      allowMultipleInstance: false,
      closable: true,
      component: CompanyManagement,
      defaultOpen: false,
      constants: companyManagementUtils.constants,
      actionFactory: companyManagementUtils.actionFactory,
      reducerFactory: companyManagementUtils.reducerFactory,
      sagaFactory: companyManagementUtils.sagaFactory,
    },
  ],
  [
    '/company/staff',
    {
      allowMultipleInstance: false,
      closable: true,
      component: CompanyStaff,
      defaultOpen: false,
      modelFactory: companyStaffUtils.modelFactory,
    },
  ],
  [
    '/waybill/map',
    {
      allowMultipleInstance: true,
      closable: true,
      component: WaybillMap,
      defaultOpen: false,
      constants: waybillMaplUtils.constants,
      actionFactory: waybillMaplUtils.actionFactory,
      reducerFactory: waybillMaplUtils.reducerFactory,
      sagaFactory: waybillMaplUtils.sagaFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/company/proclamation',
    {
      allowMultipleInstance: false,
      closable: true,
      component: Proclamation,
      defaultOpen: false,
      constants: proclamationUtils.constants,
      actionFactory: proclamationUtils.actionFactory,
      reducerFactory: proclamationUtils.reducerFactory,
      sagaFactory: proclamationUtils.sagaFactory,
    },
  ],
  [
    '/vehicleOrder/list',
    {
      allowMultipleInstance: false,
      closable: true,
      component: VehicleOrder,
      defaultOpen: false,
      constants: vehicleOrderUtils.constants,
      actionFactory: vehicleOrderUtils.actionFactory,
      reducerFactory: vehicleOrderUtils.reducerFactory,
      sagaFactory: vehicleOrderUtils.sagaFactory,
    },
  ],
  [
    '/stowage',
    {
      allowMultipleInstance: false,
      closable: true,
      component: Stowage,
      defaultOpen: false,
      modelFactory: StowageUtils.modelFactory,
    },
  ],
  [
    '/stowageManage',
    {
      allowMultipleInstance: false,
      closable: true,
      component: StowageManage,
      defaultOpen: false,
      constants: StowageManageUtils.constants,
      actionFactory: StowageManageUtils.actionFactory,
      reducerFactory: StowageManageUtils.reducerFactory,
      sagaFactory: StowageManageUtils.sagaFactory,
    },
  ],
  [
    '/stowageManage/stowageDetail',
    {
      allowMultipleInstance: true,
      closable: true,
      component: StowageDetail,
      defaultOpen: false,
      excludeFromMenu: true,
      constants: StowageDetailUtils.constants,
      actionFactory: StowageDetailUtils.actionFactory,
      reducerFactory: StowageDetailUtils.reducerFactory,
      sagaFactory: StowageDetailUtils.sagaFactory,
    },
  ],
  [
    '/manageDriver',
    {
      allowMultipleInstance: true,
      closable: true,
      component: ManageDriver,
      defaultOpen: false,
      modelFactory: ManageDriverUtils.modelFactory,
    },
  ],
  [
    '/manageVehicle',
    {
      allowMultipleInstance: true,
      closable: true,
      component: ManageVehicle,
      defaultOpen: false,
      modelFactory: ManageVehicleUtils.modelFactory,
    },
  ],
  [
    '/ios',
    {
      allowMultipleInstance: false,
      closable: true,
      component: IOS,
      defaultOpen: false,
      constants: IOSUtils.constants,
      actionFactory: IOSUtils.actionFactory,
      reducerFactory: IOSUtils.reducerFactory,
      sagaFactory: IOSUtils.sagaFactory,
    },
  ],
  [
    '/vehicleOrder/detail',
    {
      allowMultipleInstance: true,
      closable: true,
      component: vehicleOrderDetail,
      defaultOpen: false,
      constants: vehicleOrderDetailUtil.constants,
      actionFactory: vehicleOrderDetailUtil.actionFactory,
      reducerFactory: vehicleOrderDetailUtil.reducerFactory,
      sagaFactory: vehicleOrderDetailUtil.sagaFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/vehicleOrder/add',
    {
      allowMultipleInstance: true,
      closable: true,
      component: VehicleOrderAdd,
      defaultOpen: false,
      modelFactory: vehicleOrderAddUtil.modelFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/vehicleOrder/update',
    {
      allowMultipleInstance: true,
      closable: true,
      component: VehicleOrderAdd,
      defaultOpen: false,
      modelFactory: vehicleOrderAddUtil.modelFactory,
      excludeFromMenu: true,
    },
  ],
  [
    '/company/addressManage',
    {
      allowMultipleInstance: true,
      closable: true,
      component: AddressManage,
      defaultOpen: false,
      modelFactory: AddressManageUtils.modelFactory,
    },
  ],
  [
    '/company/addAddressManage',
    {
      allowMultipleInstance: true,
      closable: true,
      component: AddAddressManage,
      defaultOpen: false,
      excludeFromMenu: true,
      modelFactory: AddAddressManageUtils.modelFactory,
    },
  ],
  [
    '/company/editAddressManage',
    {
      allowMultipleInstance: true,
      closable: true,
      component: AddAddressManage,
      defaultOpen: false,
      excludeFromMenu: true,
      modelFactory: AddAddressManageUtils.modelFactory,
    },
  ],
  [
    '/transportDispatch',
    {
      allowMultipleInstance: true,
      closable: true,
      component: TransportDispatch,
      defaultOpen: false,
      modelFactory: TransportDispatchUtils.modelFactory,
    },
  ],
  [
    '/staff/authorization',
    {
      allowMultipleInstance: true,
      closable: true,
      component: StaffAuthorization,
      defaultOpen: false,
      modelFactory: staffAuthorizationUtils.modelFactory,
    },
  ],
  [
    '/home/update',
    {
      allowMultipleInstance: false,
      closable: true,
      component: HomeUpdate,
      defaultOpen: false,
      modelFactory: homeUpdateUtils.modelFactory,
    },
  ],
]);

export default routerConfig;
