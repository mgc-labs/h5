/**
 * 全局定义
 * @author ryan bian
 */
const usp = new URLSearchParams(location.search);

// 最大可开标签数
export const MAX_TABS_LIMIT: number = 20;

// 使用 mock 数据
export const USE_MOCK: boolean = usp.get('useMock') === 'true';

// 重新获取短信验证码间隔
export const SMS_CODE_REFETCH_INTERVAL: number = 60;

// cookie 失效时间
export const LOGIN_EXPIRE_DAY: number = 365;

// 主题色和主题名映射
export const THEME_ENUM = new Map([
  ['3ba1ff', 'default'],
  ['07979c', 'green'],
  ['3861da', 'purple'],
  ['cd403c', 'red'],
  ['b6917a', 'brown'],
  ['f6531b', 'orange'],
]);

// 用于销毁 store 下某个 key
export const DESTROY_ACTION_PREFIX = '@@DESTROY/';
export const DESTROY_ACTION = '@@DESTROY_KEYS';

export const TLAS_MESSAGE = 'whosyourdaddy';
