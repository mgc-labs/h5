import { AppActionTypes } from 'modules/module.app/constants';

export interface IPersistenceType {
  IMMEDIATE: string;
  DEBOUNCE: string;
}

export const PersistenceType: IPersistenceType = {
  IMMEDIATE: 'IMMEDIATE',
  DEBOUNCE: 'DEBOUNCE',
};

const Whitelist = {
  [AppActionTypes.INIT_ROUTE]: {
    type: PersistenceType.DEBOUNCE,
    dataPath: ['global', 'route', 'routeIds'],
  },
  [AppActionTypes.TOGGLE_NAV_COLLAPSE]: {
    type: PersistenceType.DEBOUNCE,
    dataPath: ['global', 'ui'],
  },
  [AppActionTypes.REMOVE_ROUTE_ALLOWED]: {
    type: PersistenceType.DEBOUNCE,
    dataPath: ['global', 'route', 'routeIds'],
  },
};

export const getPersistenceConfig = (type: string) => {
  if (Whitelist[type]) {
    return Whitelist[type];
  }
  return {};
};
