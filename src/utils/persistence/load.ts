import { AuthActionTypes } from 'modules/module.authorization/constants';
import { call, put, race, take } from 'redux-saga/effects';
import { stateLoad } from './actions';
import * as PersistenceEngine from './engine';

export default function* loadSaga() {
  yield race({
    getResouceDone: take(AuthActionTypes.GET_RESOURCE_SUCCESS),
    getResouceFail: take(AuthActionTypes.GET_RESOURCE_ERROR),
  });
  yield put(stateLoad.request());
  try {
    const state = yield call(PersistenceEngine.load);
    yield put(stateLoad.success(state));
  } catch (err) {
    yield put(stateLoad.failure(err));
  }
}
