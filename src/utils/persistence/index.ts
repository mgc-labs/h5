import { all, fork, take } from 'redux-saga/effects';

import { PersistActionTypes } from './actionTypes';
import { init } from './engine';
import load from './load';
import persistence from './persistence';

export default function* rootSaga() {
  yield all([fork(load), fork(persistence)]);
  const { payload } = yield take(PersistActionTypes.INIT);
  init(`${payload.userName}@${payload.domainName}`);
}
