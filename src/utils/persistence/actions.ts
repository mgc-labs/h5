import { PersistActionTypes } from './actionTypes';

export const signalUnsavedChanges = () => ({
  type: PersistActionTypes.UNSAVED_CHANGES,
});

export const signalSavedChanges = () => ({
  type: PersistActionTypes.SAVED_CHANGES,
});

export const stateSave = {
  request: state => ({
    type: PersistActionTypes.STATE_SAVE_REQUEST,
    payload: state,
  }),
  success: () => ({ type: PersistActionTypes.STATE_SAVE_SUCCESS }),
  failure: () => ({ type: PersistActionTypes.STATE_SAVE_FAILURE, error: true }),
};

export const stateLoad = {
  request: () => ({ type: PersistActionTypes.STATE_LOAD_REQUEST }),
  success: state => ({
    type: PersistActionTypes.STATE_LOAD_SUCCESS,
    payload: state,
  }),
  failure: err => ({
    type: PersistActionTypes.STATE_LOAD_FAILURE,
    payload: err,
    error: true,
  }),
};
