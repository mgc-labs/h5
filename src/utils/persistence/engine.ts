import { fromJS, Map } from 'immutable';
import localForage from 'localforage';

const STORAGE_KEY: string = '@@TMS/PERSIST';
// const storage = localForage.createInstance({
//   name: STORAGE_KEY,
// });
export function init(suffix) {
  localForage.config({
    name: `${STORAGE_KEY}_${suffix}`,
  });
}

export function save(saveData) {
  return localForage.setItem(saveData.dataPath, saveData.state);
}

export function load() {
  let persistStore = Map();
  return localForage
    .iterate((value, key) => {
      const dataPath = key.split('|');
      persistStore = persistStore.withMutations(map => {
        map.setIn(dataPath, fromJS(value));
      });
    })
    .then(() => {
      return persistStore;
    });
}
