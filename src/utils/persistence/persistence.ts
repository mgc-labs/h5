import { Map } from 'immutable';
import { AnyAction } from 'redux';
import { delay } from 'redux-saga';
import { call, put, select, spawn, take } from 'redux-saga/effects';
import { signalSavedChanges, signalUnsavedChanges, stateSave } from './actions';
import { PersistActionTypes } from './actionTypes';
import * as PersistenceEngine from './engine';
import Lock from './lock';
import { getPersistenceConfig, PersistenceType } from './whitelist';

const DEBOUNCE_TIME = 1000; // debounce time in milisseconds

// let's separate this function for better modularity
function* save(saveData, action?: AnyAction) {
  yield put(stateSave.request(saveData));
  try {
    yield call(PersistenceEngine.save, saveData, action);
    yield put(stateSave.success());
  } catch (e) {
    yield put(stateSave.failure());
  }
}

function* debounceSave(saveData) {
  yield delay(DEBOUNCE_TIME);
  yield call(save, saveData);
}

// signals to the UI that there are unsaved changes
export function* signalPersistenceState() {
  yield put(signalUnsavedChanges());
  yield take(PersistActionTypes.STATE_SAVE_SUCCESS); // waits for a SERVER_SAVE success to continue
  yield put(signalSavedChanges());
}

export default function* persistenceSaga() {
  const debounceLock = new Lock(debounceSave);
  const unsavedLock = new Lock(signalPersistenceState);

  while (true) {
    const action = yield take();
    const { type, dataPath } = getPersistenceConfig(action.type);
    if (!type) {
      continue;
    }
    const state = yield select((store: Map<string, any>) =>
      store.getIn(dataPath),
    );
    const saveData = {
      state: state.toJS(),
      dataPath: dataPath.join('|'),
    };
    // each persistent action cancels the debounce timer
    yield debounceLock.cancel();

    // this lock prevents multiple unsaved changes actions from being dispatched
    yield unsavedLock.execute();

    if (type === PersistenceType.IMMEDIATE) {
      yield spawn(save, saveData); // save immediately
    } else if (type === PersistenceType.DEBOUNCE) {
      // a new debounce timer is created
      yield debounceLock.execute(saveData, action);
    }
  }
}
