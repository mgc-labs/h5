/**
 * 验证字符串是否为字数格式
 * @param {string} value 待验证的字符串.
 * @param {max?: number; min?: number; decimal?: number} pars 参数。
 * max: 最大值
 * min: 最小值
 * decimal: 小数位数，默认为2
 * @author djd
 */
export function checkNumber(
  value: string,
  pars?: { max?: number; min?: number; decimal?: number },
) {
  if (value === undefined || value === null || value === '') {
    return true;
  }
  value = value + '';
  if (!/^[0-9]+([.]{1}[0-9]{1,})?$/.test(value)) {
    return false;
  }

  if (pars) {
    const { max = Infinity, min = -Infinity, decimal = 2 } = pars;
    const decimalValue = value.split('.')[1];

    if (decimalValue !== undefined) {
      if (decimalValue.length > decimal) {
        return false;
      }
    }

    const floatValue = Number(value);
    if (floatValue < min) {
      return false;
    } else if (floatValue > max) {
      return false;
    }
  }

  return true;
}

/**
 * 验证字符串是否为字数格式
 * @param {string} value 待验证的字符串.
 * @param {max?: number; min?: number; decimal?: number} pars 参数。
 * max: 最大值
 * min: 最小值
 * decimal: 小数位数，默认为2
 */
export function checkNumberForm(
  message: string,
  pars?: { max?: number; min?: number; decimal?: number },
) {
  return (rule: any, value: string, callback: any) => {
    if (checkNumber(value, pars)) {
      callback();
    } else {
      callback(message);
    }
  };
}
