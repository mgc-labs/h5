export function mapDataToForm(item: any) {
  const formData = {};
  Reflect.ownKeys(item).map((key: string) => {
    formData[key] = { name: key, value: item[key] };
  });
  return formData;
}
