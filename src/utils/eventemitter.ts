/**
 * TMS event emitter
 */
type IListener = (...params: any[]) => any;

export default class EventEmitter {
  private store: Map<[string, IListener], IListener> = new Map();
  public once(eventName: string, listener: IListener) {
    this.store.set([eventName, listener], listener);
    return this;
  }
  public on(eventName: string, listener: IListener) {
    this.store.set([eventName, listener], listener);
    return this;
  }
  public off(eventName: string, listener: IListener) {
    return this.removeListener(eventName, listener);
  }
  public emit(eventName: string, ...args: any[]) {
    for (const [key, listener] of this.store.entries()) {
      if (key[0] === eventName) {
        listener(...args);
      }
    }
  }
  public removeListener(eventName: string, listener: IListener) {
    for (const key of this.store.keys()) {
      if (key[0] === eventName && key[1] === listener) {
        this.store.delete(key);
      }
    }
    return this;
  }
  public removeAllListeners(eventName?: string) {
    for (const key of this.store.keys()) {
      if (eventName) {
        if (key[0] === eventName) {
          this.store.delete(key);
        }
      } else {
        this.store.delete(key);
      }
    }
    return this;
  }
  public has(eventName: string) {
    let hasEventName = false;
    for (const key of this.store.keys()) {
      if (key[0] === eventName) {
        hasEventName = true;
        break;
      }
    }
    return hasEventName;
  }
}
