/**
 * Model Factory
 * @author ryan bian
 */
import { Collection } from 'immutable';
import { camelCase, fromPairs } from 'lodash';
import { connect } from 'react-redux';
import {
  ActionMap,
  createActions,
  handleActions,
  ReducerMap,
} from 'redux-actions';

interface IModelActions {
  [actionName: string]: any;
}

interface IConstants {
  [constantType: string]: string;
}

interface IModelConfig {
  namespace: string;
  state: Collection<any, any>;
  actions: ActionMap<any, any>;
  reducers: ReducerMap<any, any>;
  effects: (CONSTANTS: IConstants, actions: IModelActions) => any;
}

const modelFactory = (config: IModelConfig) => {
  const PREFIX_KEY: string = config.namespace.toUpperCase();
  const PREFIX_KEY_LOWERCASE: string = PREFIX_KEY.split('/')
    .map((part: string) => camelCase(part))
    .join('/');
  const actionKeys = Object.keys(config.actions).map((key: string) => [
    key,
    `${PREFIX_KEY}__${key}`,
  ]);
  const constants: IConstants = fromPairs(actionKeys);
  const creatorConfig = {
    namespace: '__',
  };
  const actions = createActions(
    {
      [PREFIX_KEY]: config.actions,
    },
    creatorConfig,
  );

  const prefixedReducers = {};
  Object.keys(config.reducers).forEach((key: string) => {
    Object.assign(prefixedReducers, {
      [`${PREFIX_KEY}__${key}`]: config.reducers[key],
    });
  });

  const reducer = handleActions(prefixedReducers, config.state, creatorConfig);
  const saga = config.effects(constants, actions[PREFIX_KEY_LOWERCASE]);

  return {
    saga,
    reducer,
    connectModel(connectFunction) {
      const connectConfig = connectFunction(
        actions[PREFIX_KEY_LOWERCASE],
        state => config.state,
      );
      return connect(
        connectConfig.mapStateToProps,
        connectConfig.mapDispatchToProps,
      );
    },
  };
};

export default modelFactory;
