export const conditionLayout = {
  xxl: { span: 6 },
  xl: { span: 8 },
  md: { span: 12 },
  sm: { span: 12 },
  xs: { span: 24 },
};

export const conditionItemLayout = {
  labelCol: {
    xs: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 18 },
  },
};
