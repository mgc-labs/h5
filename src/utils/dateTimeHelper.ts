import { padStart } from 'lodash';
import { Moment } from 'moment';

export function toYYYYMMDDHHmm(date: Date | string | Moment) {
  if (date === null || date === undefined) {
    return '';
  } else if (date instanceof Date) {
    const year = String(date.getFullYear());
    const month = padStart(String(date.getMonth() + 1), 2, '0');
    const day = padStart(String(date.getDate()), 2, '0');
    return `${year}-${month}-${day}`;
  } else if (typeof date === 'string') {
    return date.replace(/:\d*$/, '');
  } else {
    return date.format('YYYY-MM-DD HH:mm');
  }
}

export function toYYYYMMDDS(date: Date | string | Moment) {
  if (!date) {
    return;
  }

  if (date instanceof Date) {
    const year = String(date.getFullYear());
    const month = padStart(String(date.getMonth() + 1), 2, '0');
    const day = padStart(String(date.getDate()), 2, '0');
    return `${year}-${month}-${day} 00:00:00`;
  } else if (typeof date === 'string') {
    return date.replace(/:\d*$/, '');
  } else {
    return `${date.format('YYYY-MM-DD')} 00:00:00`;
  }
}

export function toYYYYMMDDE(date: Date | string | Moment) {
  if (!date) {
    return;
  }

  if (date instanceof Date) {
    const year = String(date.getFullYear());
    const month = padStart(String(date.getMonth() + 1), 2, '0');
    const day = padStart(String(date.getDate()), 2, '0');
    return `${year}-${month}-${day} 23:59:59`;
  } else if (typeof date === 'string') {
    return date.replace(/:\d*$/, '');
  } else {
    return `${date.format('YYYY-MM-DD')} 23:59:59`;
  }
}
