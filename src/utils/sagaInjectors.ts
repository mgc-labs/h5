import checkStore from './checkStore';
import SAGA_CONSTANTS from './constants';

interface IDescriptor {
  mode: SAGA_CONSTANTS;
  saga: () => any;
}

export function injectSagaFactory(store) {
  return function injectSaga(key: string, descriptor: IDescriptor, args) {
    const newDescriptor: IDescriptor = {
      ...descriptor,
      mode: descriptor.mode || SAGA_CONSTANTS.RESTART_ON_REMOUNT,
    };
    const { saga, mode } = newDescriptor;

    let hasSaga = Reflect.has(store.injectedSagas, key);

    if (process.env.NODE_ENV !== 'production') {
      const oldDescriptor = store.injectedSagas[key];
      // enable hot reloading of daemon and once-till-unmount sagas
      if (hasSaga && oldDescriptor.saga !== saga) {
        oldDescriptor.task.cancel();
        hasSaga = false;
      }
    }

    if (
      !hasSaga ||
      (hasSaga &&
        mode !== SAGA_CONSTANTS.DAEMON &&
        mode !== SAGA_CONSTANTS.ONCE_TILL_UNMOUNT)
    ) {
      store.injectedSagas[key] = {
        ...newDescriptor,
        task: store.runSaga(saga, args),
      };
    }
  };
}

export function ejectSagaFactory(store, isValid) {
  return function ejectSaga(key) {
    if (!isValid) {
      checkStore(store);
    }

    if (Reflect.has(store.injectedSagas, key)) {
      const descriptor = store.injectedSagas[key];
      if (descriptor.mode && descriptor.mode !== SAGA_CONSTANTS.DAEMON) {
        descriptor.task.cancel();
        // Clean up in production; in development we need `descriptor.saga` for hot reloading
        if (process.env.NODE_ENV === 'production') {
          // Need some value to be able to detect `ONCE_TILL_UNMOUNT` sagas in `injectSaga`
          store.injectedSagas[key] = 'done'; // eslint-disable-line no-param-reassign
        }
      }
    }
  };
}

export default function getInjectors(store) {
  checkStore(store);

  return {
    ejectSaga: ejectSagaFactory(store, true),
    injectSaga: injectSagaFactory(store),
  };
}
