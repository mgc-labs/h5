const setName = name => {
  return name.replace(/([A-Z])/g, '_$1').toUpperCase();
};

type ICreateConstants = (actions: any) => any;
type ICreateActionFactory = (actions: any) => any;

export const createConstants: ICreateConstants = actions => {
  const constants = {};
  Object.keys(actions).forEach(key => {
    constants[setName(key)] = actions[key];
  });
  return constants;
};

export const createActionFactory: ICreateActionFactory = actions => {
  return CONSANTS => {
    const actionsObjs = {};
    Object.keys(actions).forEach(key => {
      actionsObjs[key] = options => ({
        type: CONSANTS[setName(key)],
        payload: options,
      });
    });
    return actionsObjs;
  };
};
