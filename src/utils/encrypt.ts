/**
 * encrypt utils
 * @author ryan.bian
 */
import md5 from 'blueimp-md5';
import { TLAS_MESSAGE } from '../CONFIG';

const salt = window.btoa(TLAS_MESSAGE);
// const salt = '';

export default (str: string) => {
  return md5(str + salt);
};
