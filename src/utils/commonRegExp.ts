/**
 * 常用正则
 * @author ryan bian
 */

/**
 * 手机号码
 * 移动：134(0-8)、135、136、137、138、139、147、150、151、152、157、158、159、178、182、183、184、187、188、198
 * 联通：130、131、132、145、155、156、175、176、185、186、166
 * 电信：133、153、173、177、180、181、189、199
 * 全球星：1349
 * 虚拟运营商：170
 */
// 手机号码
export const MOBILE_PHONE = /^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\d{8}$/;
// 固定电话，带分机号+手机号
export const FIXED_TELEPHONE = /^(((0\d{2,3})-?)(\d{7,8})(-?(\d{3,}))?)|(1[3,4,5,7,8,9]\d{9})$/;
// 后端正则，手机号+固话正则，保留
export const FIXED_TELEPHONE_BACK = /^(((\\+\\d{2}-)?0\\d{2,3}-\\d{7,8})|((\\+\\d{2}-)?(\\d{2,3}-)?([1][3,4,5,7,8][0-9]\\d{8})))$/;
// 最大100，支持两位小数
export const UP_100_TOW_DECIMAL = /^(([1-9]\d?(\.\d{1,2})?)|(0\.\d{1,2}))$/;
// 最大100000，支持两位小数
export const UP_100000_TOW_DECIMAL = /^(([1-9](\d{1,4})?(\.\d{1,2})?)|(0\.\d{1,2}))$/;
// 最大500，支持两位小数
export const UP_500_TOW_DECIMAL = /^(([1-4]?\d{2}(\.\d{1,2})?)|([1-9]?[1-9](\.\d{1,2})?)|(0\.\d{1,2}))$/;
// 最大500，只支持整数
export const UP_500_NUMBER = /^(([1-4]?\d{2})|([1-9]?[1-9]))$/;

// form对应的正则rules
export const UP_100000_TOW_DECIMAL_OBJ = {
  pattern: UP_100000_TOW_DECIMAL,
  message: '请输入0-100000，支持两位小数',
};
export const UP_100_TOW_DECIMAL_OBJ = {
  pattern: UP_100_TOW_DECIMAL,
  message: '请输入0-100，支持两位小数',
};
export const UP_500_TOW_DECIMAL_OBJ = {
  pattern: UP_500_TOW_DECIMAL,
  message: '请输入0-500，支持两位小数',
};
export const UP_500_NUMBER_OBJ = {
  pattern: UP_500_NUMBER,
  message: '请输入0-500，仅支持整数',
};
