/**
 * 动态生产 actionType
 * @author ryan bian
 */
interface IConstants {
  [propName: string]: string;
}
type IActionTypePrefixer = (key: string, CONSTANTS: IConstants) => IConstants;

let actionTypePrefixer: IActionTypePrefixer;
actionTypePrefixer = (key, CONSTANTS) => {
  const ret = {};
  const UPPERCASEDKEY = key.toUpperCase();
  Object.keys(CONSTANTS).forEach(constant => {
    Object.assign(ret, {
      [constant]: `${UPPERCASEDKEY}/${constant}`,
    });
  });
  return ret;
};

export default actionTypePrefixer;
