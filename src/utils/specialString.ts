import { get, isObject, upperFirst } from 'lodash';

const specialString = [
  {
    search: /\'/,
    string: "'",
    entity: '&#39;',
  },
  {
    search: /\"/,
    string: '"',
    entity: '&#34;',
  },
  {
    search: /\(/,
    string: '(',
    entity: '&#40;',
  },
  {
    search: /\)/,
    string: ')',
    entity: '&#41;',
  },
  {
    string: '<',
    search: '<',
    entity: '&lt;',
  },
  {
    string: '>',
    search: '>',
    entity: '&gt;',
  },
  {
    search: /\//,
    string: '/',
    entity: '&#47;',
  },
];

const replaceMothed = (data, key, toKey) => {
  if(typeof data !== 'string') {
    return data;
  }
  specialString.map(item => {
    const re = new RegExp(item[key], 'g');
    data = data.replace(re, item[toKey]);
  });
  return data;
};

const replaceData = (params, fromKey, toKey) => {
  if (isObject(params) && Array.isArray(params)) {
    params.map(item => {
      replaceData(item, fromKey, toKey);
    });
  } else if (isObject(params)) {
    Object.keys(params).map(key => {
      if (typeof params[key] === 'string') {
        params[key] = replaceMothed(params[key], fromKey, toKey);
      } else if (isObject(params[key])) {
        replaceData(params[key], fromKey, toKey);
      }
    });
  } else if (typeof params === 'string' && params.indexOf('=') > 0) {
    params = replaceMothed(params, fromKey, toKey);
  }
  return params;
};

export default {
  set: data => {
    const newData = replaceData(data, 'search', 'entity');
    return newData;
  },
  reset: data => {
    const newData = replaceData(data, 'entity', 'string');
    return newData;
  },
};
