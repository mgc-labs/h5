import { message } from 'antd';
import axios, { AxiosRequestConfig } from 'axios';
import MockAdapter from 'axios-mock-adapter';
import invariant from 'invariant';
import { get, isObject, upperFirst } from 'lodash';
import qs from 'qs';
import specialString from './specialString';

const headers = {
  post: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
};

const mockAxios = axios.create();
const normalAxios = axios.create({
  headers,
});
const mock = new MockAdapter(mockAxios);

const instances = [mockAxios, normalAxios];

class Mocker {
  mockMap = new Map();
  public on(method, path) {
    invariant(
      !this.mockMap.has(`${method}@${path}`),
      `${method} on ${path} has been duplicatly mocked!`,
    );
    this.mockMap.set(`${method}@${path}`, true);
    return mock[`on${upperFirst(method)}`](path);
  }
  public reset() {
    mock.reset();
  }
}

export interface IRequestOptions {
  useMock?: boolean;
  globalErrorMsg?: boolean;
}

const trimParams = params => {
  if (isObject(params) && !Array.isArray(params)) {
    Object.keys(params).map(key => {
      if (typeof params[key] === 'string') {
        params[key] = params[key].replace(/(^\s+)|(\s+$)/g, '');
      }
    });
  } else if (typeof params === 'string' && params.indexOf('=') > 0) {
    const newParams = qs.parse(params);
    params = trimParams(newParams);
    params = qs.stringify(params);
  }
  return params;
};

// refer to: https://github.com/axios/axios#request-config
const request = async (
  config: AxiosRequestConfig,
  options: IRequestOptions = {
    globalErrorMsg: true,
  },
): Promise<any> => {
  try {
    if (config.data) {
      config.data = trimParams(config.data);
      config.data = specialString.set(config.data);
    }

    if (config.params) {
      config.params = trimParams(config.params);
      config.params = specialString.set(config.params);
    }

    const res = options.useMock
      ? await mockAxios(config)
      : await normalAxios(config);

    return res;
  } catch (err) {
    if (options.globalErrorMsg) {
      message.error(err.msg || err.message);
    }
    throw err;
  }
};

// interceptors
instances.forEach(instance => {
  instance.interceptors.response.use(
    response => {
      response.data = specialString.reset(response.data);
      if (response.status === 200) {
        if (response.data.result !== 'error') {
          return response.data;
        }
        return Promise.reject(response.data);
      }
      return Promise.reject(response.data);
    },
    error => {
      const status = get(error, ['response', 'status']);
      const statusText = get(error, ['response', 'statusText']);
      if (status === 401 && statusText === 'Unauthorized') {
        // 没有登陆，跳回登陆页
        setTimeout(() => {
          const paths = location.pathname.split('/').slice(0, 2);
          paths.push('login');
          window.onbeforeunload = null;
          const nextPathname = paths.join('/');
          if (location.pathname === nextPathname) {
            return;
          }
          window.location.href = nextPathname;
        }, 3000);
      }
      const errorData = get(error, ['response', 'data']);
      if (errorData) {
        return Promise.reject(errorData);
      } else {
        return Promise.reject(error);
      }
    },
  );
});

export const mocker = new Mocker();
export const cancelSource = axios.CancelToken.source();

export default request;
