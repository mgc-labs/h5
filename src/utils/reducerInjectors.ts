import createReducer from '../reducers';
import checkStore from './checkStore';

export function injectReducerFactory(store, isValid) {
  return function injectReducer(
    key: string,
    reducer: (state: any, action: any) => any,
  ) {
    if (!isValid) {
      checkStore(store);
    }

    // Check `store.injectedReducers[key] === reducer` for hot reloading when a key is the same but a reducer is different
    if (
      Reflect.has(store.injectedReducers, key) &&
      store.injectedReducers[key] === reducer
    ) {
      return;
    }

    store.injectedReducers[key] = reducer;
    store.replaceReducer(createReducer(store.injectedReducers));
  };
}

export function ejectReducerFactory(store, isValid) {
  return function ejectReducer(key: string) {
    if (!isValid) {
      checkStore(store);
    }
    store.dispatch({
      type: `@@DESTROY/${key}`,
    });

    delete store.injectedReducers[key];
    store.replaceReducer(createReducer(store.injectedReducers));
  };
}

export default function getInjectors(store) {
  checkStore(store);

  return {
    ejectReducer: ejectReducerFactory(store, true),
    injectReducer: injectReducerFactory(store, true),
  };
}
