import hoistNonReactStatics from 'hoist-non-react-statics';
import PropTypes from 'prop-types';
import React from 'react';
import { Reducer } from 'redux';

import getInjectors from './reducerInjectors';

export interface IInjectReducerConfig {
  key: string;
  reducer: Reducer;
}

/**
 * Dynamically injects a reducer
 *
 * @param {string} key A key of the reducer
 * @param {function} reducer A reducer that will be injected
 *
 */
export default ({ key, reducer }: IInjectReducerConfig) => WrappedComponent => {
  class ReducerInjector extends React.PureComponent {
    public static WrappedComponent = WrappedComponent;
    public static contextTypes = {
      store: PropTypes.object.isRequired,
    };
    public static displayName = `withReducer(${WrappedComponent.displayName ||
      WrappedComponent.name ||
      'Component'})`;

    public injectors = getInjectors(this.context.store);

    public componentWillMount() {
      const { injectReducer } = this.injectors;

      injectReducer(key, reducer);
    }

    public render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  return hoistNonReactStatics(ReducerInjector, WrappedComponent);
};
