// import { createStructuredSelector, createSelector } from 'reselect';
import { Hash } from './Hash';

import { RECORD_SELECTED_CHANGED } from 'modules/module.ltl.order/constants';

export class TableSelectCtrl {
  private list: Hash[];

  public setList(list: Hash[]) {
    this.list = list || [];
  }

  public setSelectedStatus(record: Hash, dispatch) {
    record.isSelected = !record.isSelected;

    dispatch({
      type: RECORD_SELECTED_CHANGED,
      payload: this.list,
    });
  }

  public setAllSelectedStatus(dispatch) {
    // this.list
    // dispatch({
    //   type: RECORD_ALLSELECTED_CHANGED,
    //   payload: this.list,
    // });
  }

  public selectedCount() {
    return this.list.filter(item => item.isSelected).length;
  }
}

export function tableSelectCtrlInit(): TableSelectCtrl {
  return new TableSelectCtrl();
}
