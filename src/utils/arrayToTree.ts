/**
 * arrayToTree
 * Convert a plain array of nodes to a nested data structure
 * @author ryan bian
 */
import { cloneDeep, keyBy } from 'lodash';

type Tree<T> = T & {
  children?: Array<Tree<T>>;
};

interface IArrayToTreeOptions {
  childrenProperty: string;
  parentProperty: string;
  customID: string;
  rootID: string;
}

function createTree(array, rootNodes, customID, childrenProperty) {
  const tree = [];

  for (const rootNode in rootNodes) {
    if (!rootNodes[rootNode] && !rootNodes.hasOwnProperty(rootNode)) {
      continue;
    }
    const node = rootNodes[rootNode];
    const childNode = array[node[customID]];

    if (childNode) {
      node[childrenProperty] = createTree(
        array,
        childNode,
        customID,
        childrenProperty,
      );
    }

    tree.push(node);
  }

  return tree;
}

function groupByParents(array, options) {
  const arrayByID = keyBy(array, options.customID);

  return array.reduce((prev, item) => {
    let parentID = item[options.parentProperty];
    if (!parentID || !arrayByID.hasOwnProperty(parentID)) {
      parentID = options.rootID;
    }

    if (parentID && prev.hasOwnProperty(parentID)) {
      prev[parentID].push(item);
      return prev;
    }

    prev[parentID] = [item];
    return prev;
  }, {});
}

export default function arrayToTree<T>(
  data: T[],
  options: Partial<IArrayToTreeOptions>,
): Array<Tree<T>> {
  options = Object.assign(
    {
      parentProperty: 'parent_id',
      childrenProperty: 'children',
      customID: 'id',
      rootID: '0',
    },
    options,
  );

  if (!Array.isArray(data)) {
    throw new TypeError('Expected an array but got an invalid argument');
  }

  const grouped = groupByParents(cloneDeep(data), options);
  return createTree(
    grouped,
    grouped[options.rootID],
    options.customID,
    options.childrenProperty,
  );
}
