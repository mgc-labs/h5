import { List, Map } from 'immutable';

export type ImmutableListMap = List<Map<string, any>>;
export type ImmutableMap = Map<string, any>;
export type ImmutableListString = List<string>;
