/**
 * Create Store
 */
import {
  connectRouter,
  routerMiddleware,
} from 'connected-react-router/immutable';
import { fromJS } from 'immutable';
import * as React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'utils/persistence/';
import createReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(initialState = {}, history) {
  const rootReducer = createReducer();

  const store = ((window as any).store = createStore(
    connectRouter(history)(rootReducer),
    fromJS(initialState),
    compose(
      applyMiddleware(
        sagaMiddleware,
        routerMiddleware(history),
        // ... other middlewares ...
      ),
    ),
  ));

  // Extensions
  Object.assign(store, {
    injectedReducers: {}, // Reducer registry
    injectedSagas: {}, // Saga registry
    runSaga: sagaMiddleware.run,
  });

  sagaMiddleware.run(rootSaga);

  return Component => <Provider store={store}>{Component}</Provider>;
}
