/**
 * AddAddress model
 * @author hefan
 * @date 2018/9/18 下午3:32:29
 */
import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { fromJS, List, Map } from 'immutable';
import { call, put, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { addDepot, getData, modifyDepot } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: Map(),
      error: false,
      loading: false,
      saveOk: false,
    }),
    actions: {
      GET_ASYNC_DATA: param => param,
      GET_ASYNC_DATA_ERROR: error => error,
      GET_ASYNC_DATA_SUCCESS: data => data,
      ADD_DEPOT_DATA: param => param,
      ADD_DEPOT_DATA_ERROR: error => error,
      ADD_DEPOT_DATA_SUCCESS: data => data,
      EDIT_DEPOT_DATA: param => param,
      EDIT_DEPOT_DATA_ERROR: error => error,
      EDIT_DEPOT_DATA_SUCCESS: data => data,
      GO_BACK: () => ({}),
      SAVE_FIELDS: fields => fields,
    },
    reducers: {
      GET_ASYNC_DATA(state, action) {
        return state
          .set('error', false)
          .set('loading', true)
          .set('saveOk', false);
      },
      GET_ASYNC_DATA_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_ASYNC_DATA_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data));
      },
      ADD_DEPOT_DATA(state, action) {
        return state
          .set('error', false)
          .set('loading', true)
          .set('data', fromJS(action.payload));
      },
      ADD_DEPOT_DATA_ERROR(state, action) {
        return state
          .set('error', action.payload.error)
          .set('loading', false)
          .set('data', fromJS(action.payload.data));
      },
      ADD_DEPOT_DATA_SUCCESS(state) {
        return state
          .set('loading', false)
          .set('saveOk', true)
          .set('data', Map());
      },
      EDIT_DEPOT_DATA(state, action) {
        return state
          .set('error', false)
          .set('loading', true)
          .set('data', fromJS(action.payload));
      },
      EDIT_DEPOT_DATA_ERROR(state, action) {
        return state
          .set('error', action.payload.error)
          .set('loading', false)
          .set('data', fromJS(action.payload.data));
      },
      EDIT_DEPOT_DATA_SUCCESS(state) {
        return state
          .set('loading', false)
          .set('saveOk', true)
          .set('data', Map());
      },
      SAVE_FIELDS(state, action) {
        const oldData = state.get('data').toJS();
        return state.set(
          'data',
          fromJS({
            ...oldData,
            ...action.payload,
          }),
        );
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const data = yield call(getData, action.payload);
          yield put(actions.getAsyncDataSuccess(data));
        } catch (err) {
          yield put(actions.getAsyncDataError(err));
        }
      }

      function* addData(action) {
        try {
          const data = yield call(addDepot, action.payload);
          message.success(data.msg);
          yield put(actions.addDepotDataSuccess(data));
        } catch (err) {
          yield put(
            actions.addDepotDataError({
              error: err,
              data: action.payload,
            }),
          );
        }
      }

      function* editData(action) {
        try {
          const data = yield call(modifyDepot, action.payload);
          message.success(data.msg);
          yield put(actions.editDepotDataSuccess());
        } catch (err) {
          yield put(
            actions.editDepotDataError({
              error: err,
              data: action.payload,
            }),
          );
        }
      }

      function* goBackFun() {
        yield put(push('/company/addressManage'));
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ASYNC_DATA, fetchData);
        yield takeLatest(CONSTANTS.ADD_DEPOT_DATA, addData);
        yield takeLatest(CONSTANTS.EDIT_DEPOT_DATA, editData);
        yield takeLatest(CONSTANTS.GO_BACK, goBackFun);
      };
    },
  });
