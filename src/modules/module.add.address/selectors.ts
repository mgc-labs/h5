/**
 * AddAddress selectors
 * @author hefan
 * @date 2018/9/18 下午3:32:29
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectSaved = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('saveOk'));

export const makeSelectRouteIds = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.getIn(['route', 'routeIds']),
  );
