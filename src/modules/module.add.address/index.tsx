/**
 * AddAddress Component
 * @author hefan
 * @date 2018/9/18 下午3:32:29
 */
import * as React from 'react';
import { createStructuredSelector } from 'reselect';

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Select from 'antd/es/select';
import Spin from 'antd/es/spin';
const { confirm } = Modal;

const FormItem = Form.Item;
const { Option } = Select;
import AddressSelect from 'components/AddressSelect';

import {
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
  makeSelectRouteIds,
  makeSelectSaved,
} from './selectors';

import CustomerSelect from 'components/CustomerSelect';
import { FIXED_TELEPHONE } from 'utils/commonRegExp.ts';
import styles from './index.module.less';

export interface IAddAddressProps {
  children: React.ReactChildren;
  getAsyncData: (params: {}) => any;
  addDepotData: (param: {}) => void;
  editDepotData: (param: {}) => void;
  data: {};
  searchParams: {};
  saveOk: boolean;
  close: () => void;
  routeIds: [];
  goBack: () => void;
  refresh: () => void;
  eventEmitter: {};
  saveFields: (fields: {}) => void;
}

class AddAddress extends React.PureComponent<IAddAddressProps> {
  public componentDidMount() {
    const {
      searchParams,
      eventEmitter,
      refresh,
      routeIds,
      goBack,
    } = this.props;

    if (searchParams && searchParams.utmsDepotId) {
      this.fetchData();
    }

    eventEmitter.on('close', (requestClose, eventSource) => {
      if (
        eventSource === 'FROM_TAB_CLOSE' ||
        eventSource === 'FROM_TAB_REFRESH'
      ) {
        this.cancelHandle();
      } else {
        goBack();

        setTimeout(() => {
          requestClose();
        }, 200);

        for (const item of routeIds) {
          if (item.split('?').includes('/company/addressManage')) {
            refresh(item);
            break;
          }
        }
      }
    });
  }
  public componentDidUpdate() {
    if (this.props.saveOk) {
      this.props.close();
    }
  }
  public render() {
    const {
      form: { getFieldDecorator },
      loading,
    } = this.props;

    const {
      customerName,
      depotType,
      depotName, // 地址名称
      contact, // 联系人
      contactWay, // 联系方式
      province,
      city,
      region,
      address,
      longitude,
      latitude,
    } = this.props.data.toJS();

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    };
    const submitFormLayout = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 10, offset: 7 },
      },
    };

    return (
      <Spin spinning={loading}>
        <Card className={styles.AddAddress}>
          <Form onSubmit={this.handleSubmit} style={{ marginTop: 8 }}>
            <FormItem {...formItemLayout} label="客户">
              {getFieldDecorator('customerName', {
                initialValue: customerName,
                rules: [
                  {
                    required: true,
                    message: '请选择客户',
                  },
                ],
              })(<CustomerSelect />)}
            </FormItem>
            <FormItem {...formItemLayout} label="地址类型">
              {getFieldDecorator('depotType', {
                initialValue: depotType,
                rules: [
                  {
                    required: true,
                    message: '请选择地址类型',
                  },
                ],
              })(
                <Select placeholder="请选择地址类型">
                  <Option value="store">仓库</Option>
                  <Option value="site">网点/门店</Option>
                </Select>,
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="地址名称">
              {getFieldDecorator('depotName', {
                initialValue: depotName,
                rules: [
                  {
                    required: true,
                    message: '请输入地址名称',
                  },
                ],
              })(<Input placeholder="请输入地址名称" maxLength={50} />)}
            </FormItem>
            <FormItem {...formItemLayout} label="详细地址">
              {getFieldDecorator('address', {
                initialValue: address,
                rules: [
                  {
                    required: true,
                    message: '请选择详细地址',
                  },
                ],
              })(
                <AddressSelect
                  placeholder="请输入收货地址"
                  onSelected={this.onAddressSelected}
                />,
              )}
            </FormItem>
            <FormItem {...formItemLayout} className={styles.FormHidden}>
              {getFieldDecorator('longitude', {
                initialValue: longitude,
              })(<Input type="hidden" />)}
            </FormItem>
            <FormItem {...formItemLayout} className={styles.FormHidden}>
              {getFieldDecorator('latitude', {
                initialValue: latitude,
              })(<Input type="hidden" />)}
            </FormItem>
            <FormItem {...formItemLayout} className={styles.FormHidden}>
              {getFieldDecorator('province', {
                initialValue: province,
              })(<Input type="hidden" />)}
            </FormItem>
            <FormItem {...formItemLayout} className={styles.FormHidden}>
              {getFieldDecorator('city', {
                initialValue: city,
              })(<Input type="hidden" />)}
            </FormItem>
            <FormItem {...formItemLayout} className={styles.FormHidden}>
              {getFieldDecorator('region', {
                initialValue: region,
              })(<Input type="hidden" />)}
            </FormItem>
            <FormItem {...formItemLayout} label="联系人">
              {getFieldDecorator('contact', {
                initialValue: contact,
                rules: [
                  {
                    required: true,
                    message: '请输入联系人姓名',
                    max: 20,
                  },
                ],
              })(<Input placeholder="请输入联系人姓名" maxLength={20} />)}
            </FormItem>
            <FormItem {...formItemLayout} label="联系电话">
              {getFieldDecorator('contactWay', {
                initialValue: contactWay,
                rules: [
                  {
                    required: true,
                    message: '请输入联系电话',
                    pattern: FIXED_TELEPHONE,
                  },
                ],
              })(<Input placeholder="请输入联系电话" maxLength={20} />)}
            </FormItem>
            <FormItem {...submitFormLayout} style={{ marginTop: 32 }}>
              <Button
                type="primary"
                htmlType="submit"
                loading={this.submitting}
              >
                保存
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.cancelHandle}>
                取消
              </Button>
            </FormItem>
          </Form>
        </Card>
      </Spin>
    );
  }
  private fetchData() {
    const {
      searchParams: { utmsDepotId },
    } = this.props;
    this.props.getAsyncData({
      utmsDepotId: parseInt(utmsDepotId),
    });
  }
  private handleSubmit = e => {
    const {
      form,
      searchParams,
      addDepotData,
      editDepotData,
      data,
    } = this.props;
    const dataJs = data.toJS();
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const customer = values.customerName.split(',');
        if (searchParams && searchParams.utmsDepotId) {
          // 修改
          editDepotData({
            utmsDepotId: dataJs.utmsDepotId,
            ...values,
            customerName:
              customer.length > 1 ? customer[1] : dataJs.customerName,
            customerCode:
              customer.length > 1 ? customer[0] : dataJs.customerCode,
          });
        } else {
          // 新增
          addDepotData({
            ...values,
            customerName: customer[1],
            customerCode: customer[0],
          });
        }
      }
    });
  };
  private cancelHandle = () => {
    const change = this.props.form.isFieldsTouched();
    this.quitHandle(change);
  };
  private quitHandle(change: boolean) {
    if (change) {
      confirm({
        title: '有未保存的信息',
        content: <p>如放弃，填写的信息将丢失</p>,
        okText: '继续填写',
        cancelText: '放弃',
        onCancel: () => {
          this.props.close();
        },
      });
    } else {
      this.props.close();
    }
  }
  private onAddressSelected = result => {
    const { setFieldsValue } = this.props.form;

    setFieldsValue({
      longitude: result ? result.lng : '',
      latitude: result ? result.lat : '',
      province: result ? result.province : '',
      city: result ? result.city : '',
      region: result ? result.region : '',
    });
  };
}

const AddAddressForm = Form.create()(AddAddress);

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    const globalState = state => state.get('global');

    return {
      mapStateToProps: createStructuredSelector({
        data: makeSelectData(currentState),
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        saveOk: makeSelectSaved(currentState),
        routeIds: makeSelectRouteIds(globalState),
      }),
      mapDispatchToProps: dispatch => ({
        getAsyncData: params => dispatch(actions.getAsyncData(params)),
        addDepotData: param => dispatch(actions.addDepotData(param)),
        editDepotData: param => dispatch(actions.editDepotData(param)),
        goBack: () => dispatch(actions.goBack()),
        saveFields: fields => dispatch(actions.saveFields(fields)),
      }),
    };
  })(AddAddressForm);
};
