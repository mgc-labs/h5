/**
 * AddAddress service
 * @author hefan
 * @date 2018/9/18 下午3:32:29
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_LIST = {
  SELECTDEPOT: '/ehuodiGateway/utmsCore/utmsDepotcs/selectDepot',
  ADDDEPOT: '/ehuodiGateway/utmsCore/utmsDepotcs/addDepot',
  MODIFYDEPOT: '/ehuodiGateway/utmsCore/utmsDepotcs/modifyDepot',
};

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_LIST.SELECTDEPOT)
    .reply(() => import('./mock/mock').then(exports => [200, exports.default]));

  mocker
    .on('post', API_LIST.ADDDEPOT)
    .reply(() =>
      import('./mock/addDepot').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_LIST.MODIFYDEPOT)
    .reply(() =>
      import('./mock/modifyDepot').then(exports => [200, exports.default]),
    );
}

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getData = data => {
  return request(
    {
      method: 'post',
      url: API_LIST.SELECTDEPOT,
      data: qs.stringify(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

/**
 * 新增
 * @param data
 */
export const addDepot = data => {
  return request(
    {
      method: 'post',
      url: API_LIST.ADDDEPOT,
      data,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

/**
 * 修改
 * @param data
 */
export const modifyDepot = data => {
  return request(
    {
      method: 'post',
      url: API_LIST.MODIFYDEPOT,
      data,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
