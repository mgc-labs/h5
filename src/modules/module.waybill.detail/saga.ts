/**
 * Waybill Saga
 * @author yanwei
 * @date 2018-9-12 14:18:17
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import request, { mocker } from 'utils/request';
import { getResources } from './service';

export default (CONSTANTS, actions) => {
  function* getData(action) {
    const { payload } = action;
    try {
      const data = yield call(getResources, payload);
      yield put(actions.getAsyncDataDone(data));
    } catch (err) {
      yield put(actions.getAsyncDataError(err));
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* dataSource() {
    yield takeLatest(CONSTANTS.GET_ASYNC_DATA, getData);
  };
};
