/**
 * WaybillDetail Component
 * @author yanwei
 * @date 2018-9-12 17:43:40
 */
import { List, Map } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectDataSource } from './selectors';

import Button from 'antd/es/button';
import Col from 'antd/es/col';
import Row from 'antd/es/row';
import Table from 'antd/es/table';
import {
  FormDetailCard,
  FormDetailField,
  formLayoutCols3,
} from 'components/FormDetailCard';
import GLOBAL from 'utils/GLOBAL';

import ImageZoom from 'components/ImageZoom';

import styles from './index.module.less';

export interface IWaybillDetailProps {
  dataSource: any;
  searchParams: {
    tradeNumber: string;
  };
  children: React.ReactChildren;
  getAsyncData: (params?: object) => any;
}

class WaybillDetail extends React.PureComponent<IWaybillDetailProps> {
  state = {
    bordered: false,
    loading: false,
  };
  componentDidMount() {
    this.selectTradeDetail();
  }
  selectTradeDetail = () => {
    const { getAsyncData, searchParams } = this.props;
    getAsyncData(searchParams);
  };
  public renderTable() {
    const { dataSource } = this.props;
    if (
      !dataSource ||
      !dataSource.data ||
      !dataSource.data.utmsTradeWayPoints.length
    ) {
      return null;
    }
    const list = (dataSource && dataSource.data.utmsTradeWayPoints) || [];
    const tableProps = {
      columns: [
        {
          title: '规划地址',
          dataIndex: 'address',
          key: 'address',
        },
        {
          title: '实际地址',
          dataIndex: 'reachAddress',
          key: 'reachAddress',
        },
        {
          title: '联系人',
          dataIndex: 'contact',
          key: 'contact',
        },
        {
          title: '结果',
          key: 'reach',
          dataIndex: 'reach',
        },
        {
          title: '到达时间',
          key: 'reachDate',
          dataIndex: 'reachDate',
        },
        {
          title: '回单',
          key: 'isReceipt',
          dataIndex: 'isReceipt',
        },
        {
          title: '代收（元）',
          key: 'driverDelegateAmount',
          dataIndex: 'driverDelegateAmount',
        },
        {
          title: '备注',
          key: 'remark',
          dataIndex: 'remark',
        },
      ],
      dataSource: list.map((d, i) => {
        const reach = d.reach === 0 ? '未到达' : d.reach === 1 ? '已到达' : '';
        const isReceipt =
          d.isReceipt === 0 ? '无' : d.isReceipt === 1 ? '有' : '';

        return {
          key: i,
          address: d.address,
          reachAddress: d.reachAddress || GLOBAL.emptyRecord,
          contact: d.contact,
          reach,
          reachDate: d.reachDate || GLOBAL.emptyRecord,
          isReceipt,
          driverDelegateAmount: d.driverDelegateAmount || GLOBAL.emptyRecord,
          remark: d.remark || GLOBAL.emptyRecord,
        };
      }),
    };
    return <Table pagination={false} {...tableProps} />;
  }
  renderBasic = () => {
    const { dataSource } = this.props;
    const detail = dataSource.data || {};
    const { status } = detail;
    let statusTxt;

    if (status === 0) {
      statusTxt = '待调度';
    } else if (status === 1) {
      statusTxt = '待接单';
    } else if (status === 2) {
      statusTxt = '待签到';
    } else if (status === 3) {
      statusTxt = '待装货';
    } else if (status === 4) {
      statusTxt = '待卸货';
    } else if (status === 5) {
      statusTxt = '已完成';
    } else if (status === 6) {
      statusTxt = '已取消';
    } else if (status === 7) {
      statusTxt = '待改派';
    }
    const goodsTxt =
      detail.goodsWeight +
      '千克/' +
      detail.goodsVolume +
      '方/' +
      detail.goodsNumber +
      '件';
    return (
      <FormDetailCard title="基本信息">
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField label="订单号" value={detail.tradeNumber} />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField label="发布时间" value={detail.createDate} />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField label="用车时间" value={detail.useCarDate} />
          </Col>
        </Row>
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField label="状态" value={statusTxt} />
          </Col>
        </Row>
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField label="客户名称" value={detail.customerName} />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="联系电话"
              value={detail.customerMobilenumber}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField label="所需车型" value={detail.fromCarStruct} />
          </Col>
        </Row>
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField label="货物名称" value={detail.goodsName} />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField label="规格" value={goodsTxt} />
          </Col>
        </Row>
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField label="司机姓名" value={detail.driverName} />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="联系电话"
              value={detail.driverMobilenumber}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField label="车型" value={detail.driverCarStruct} />
          </Col>
        </Row>
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="车牌号"
              value={detail.driverCarplatenumber}
            />
          </Col>
        </Row>
      </FormDetailCard>
    );
  };
  toHourMinute = minutes => {
    return Math.floor(minutes / 60) !== 0
      ? Math.floor(minutes / 60) + '小时' + (minutes % 60) + '分钟'
      : (minutes % 60) + '分钟';
  };
  renderWay = () => {
    const { dataSource } = this.props;
    const detail = dataSource.data || {};
    return (
      <FormDetailCard title="运输结果">
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="预计里程（千米）"
              value={detail.preDistance}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="预计运输时间"
              value={this.toHourMinute(detail.preTransTime)}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="预计派送点数"
              value={detail.preSendPointNum}
            />
          </Col>
        </Row>
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="实际里程（千米）"
              value={detail.actualDistance}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="实际运输时间"
              value={this.toHourMinute(detail.actualTransTime)}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="实际派送点数"
              value={detail.actualSendPoinNum}
            />
          </Col>
        </Row>

        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="补充工作量"
              value={detail.extraInfo || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </FormDetailCard>
    );
  };
  renderService = () => {
    const { dataSource } = this.props;
    const detail = dataSource.data || {};
    return (
      <FormDetailCard title="增值服务">
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="回单"
              value={detail.isReceipt === 1 ? '有' : '无'}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="备注"
              value={detail.receiptRemark || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
        <Row>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="代收（元）"
              value={detail.driverDelegateAmount}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="备注"
              value={detail.delegateRemark || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </FormDetailCard>
    );
  };
  public render() {
    const { dataSource } = this.props;
    const detail = dataSource.data || {};
    const tradeImages = detail.utmsTradeImages || [];
    const images = [];
    tradeImages.forEach(img => {
      const d = {
        src: img,
        alt: '',
      };
      images.push(d);
    });

    return (
      <div className={styles.WaybillDetail}>
        {this.renderBasic()}
        <FormDetailCard title="运输信息">
          {detail.status === 2 ||
          detail.status === 3 ||
          detail.status === 4 ||
          detail.status === 5 ||
          (detail.status === 6 && detail.driverMobilenumber !== null) ? (
            <Link to={`/waybill/map?tradeNumber=${detail.tradeNumber}`}>
              <Button
                type="primary"
                style={{ position: 'absolute', top: '7px', right: '16px' }}
              >
                运输轨迹
              </Button>
            </Link>
          ) : null}
          {this.renderTable()}
        </FormDetailCard>
        {this.renderWay()}
        {this.renderService()}
        <FormDetailCard title="卸货凭证">
          {images.length ? (
            <ImageZoom
              // hotKey={{}}
              // controller={{}}
              // zIndex={1000}
              // onClick={() => null}
              backdrop="rgba(0, 0, 0, 0.65)"
              set={images}
            />
          ) : null}
        </FormDetailCard>
      </div>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getAsyncData: params => dispatch(actions.getAsyncData(params)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    dataSource: makeSelectDataSource(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(WaybillDetail);
};
