/**
 * WaybillDetail Reducers
 * @author yanwei
 * @date 2018-9-12 17:43:40
 */

import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  dataSource: {},
  error: false,
  loading: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_ASYNC_DATA:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_ASYNC_DATA_SUCCESS:
        return state.set('loading', false).set('dataSource', action.payload);
      case CONSANTS.GET_ASYNC_DATA_ERROR:
        return state.set('error', action.error).set('loading', false);
      default:
        return state;
    }
  };
};
