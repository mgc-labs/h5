/**
 * Waybill Actions
 * @author yanwei
 * @date 2018-9-12 14:18:17
 */
import { AnyAction } from 'redux';
import {
  GET_ASYNC_DATA,
  GET_ASYNC_DATA_ERROR,
  GET_ASYNC_DATA_SUCCESS,
} from './constants';

export default CONSANTS => ({
  getAsyncData: (params): AnyAction => ({
    payload: params,
    type: CONSANTS.GET_ASYNC_DATA,
  }),

  getAsyncDataDone: (dataSource): AnyAction => ({
    payload: dataSource,
    type: CONSANTS.GET_ASYNC_DATA_SUCCESS,
  }),

  getAsyncDataError: (error): AnyAction => ({
    error,
    type: CONSANTS.GET_ASYNC_DATA_ERROR,
  }),
});
