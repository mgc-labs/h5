export default {
  result: 'success',
  count: 1,
  data: [
    {
      tradeNumber: 'UTMS15366573150188767',

      createDate: '2018-08-30 13:00:00',

      useCarDate: '2018-08-30 13:00:00',

      goodsWeight: 2,

      goodsVolume: 3,

      goodsNumber: 3,

      goodsName: '水果',

      driverName: '张三',

      driverCarplatenumber: '浙A888888',

      driverMobilenumber: '18666668888',

      status: '0',

      pre_delegate_amount: 23,

      actual_delegate_amount: 23,

      receipt_status: '1',
    },
    {
      tradeNumber: 'UTMS1536657315018767',

      createDate: '2018-08-30 13:00:00',

      useCarDate: '2018-08-30 13:00:00',

      goodsWeight: 2,

      goodsVolume: 3,

      goodsNumber: 3,

      goodsName: '水果',

      driverName: '张三',

      driverCarplatenumber: '浙A888888',

      driverMobilenumber: '18666668888',

      status: '1',

      pre_delegate_amount: 23,

      actual_delegate_amount: 23,

      receipt_status: '0',
    },
    {
      tradeNumber: 'UTMS15366573150d10767',

      createDate: '2018-08-30 13:00:00',

      useCarDate: '2018-08-30 13:00:00',

      goodsWeight: 2,

      goodsVolume: 3,

      goodsNumber: 3,

      goodsName: '水果',

      driverName: '张三',

      driverCarplatenumber: '浙A888888',

      driverMobilenumber: '18666668888',

      status: '2',

      pre_delegate_amount: 23,

      actual_delegate_amount: 23,

      receipt_status: '2',
    },
    {
      tradeNumber: 'UTMS153865731501o0767',

      createDate: '2018-08-30 13:00:00',

      useCarDate: '2018-08-30 13:00:00',

      goodsWeight: 2,

      goodsVolume: 3,

      goodsNumber: 3,

      goodsName: '水果',

      driverName: '张三',

      driverCarplatenumber: '浙A888888',

      driverMobilenumber: '18666668888',

      status: '3',

      pre_delegate_amount: 23,

      actual_delegate_amount: 23,

      receipt_status: '2',
    },
    {
      tradeNumber: 'UTMS15366670315010767',

      createDate: '2018-08-30 13:00:00',

      useCarDate: '2018-08-30 13:00:00',

      goodsWeight: 2,

      goodsVolume: 3,

      goodsNumber: 3,

      goodsName: '水果',

      driverName: '张三',

      driverCarplatenumber: '浙A888888',

      driverMobilenumber: '18666668888',

      status: '4',

      pre_delegate_amount: 23,

      actual_delegate_amount: 23,

      receipt_status: '2',
    },
    {
      tradeNumber: 'UTMS15366473150188767',

      createDate: '2018-08-30 13:00:00',

      useCarDate: '2018-08-30 13:00:00',

      goodsWeight: 2,

      goodsVolume: 3,

      goodsNumber: 3,

      goodsName: '水果',

      driverName: '张三',

      driverCarplatenumber: '浙A888888',

      driverMobilenumber: '18666668888',

      status: '5',

      pre_delegate_amount: 23,

      actual_delegate_amount: 23,

      receipt_status: '1',
    },
    {
      tradeNumber: 'UTMS15366573150128767',

      createDate: '2018-08-30 13:00:00',

      useCarDate: '2018-08-30 13:00:00',

      goodsWeight: 2,

      goodsVolume: 3,

      goodsNumber: 3,

      goodsName: '水果',

      driverName: '张三',

      driverCarplatenumber: '浙A888888',

      driverMobilenumber: '18666668888',

      status: '6',

      pre_delegate_amount: 23,

      actual_delegate_amount: 23,

      receipt_status: '1',
    },
  ],
};
