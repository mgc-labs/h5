/**
 * WaybillDetail selectors
 * @author yanwei
 * @date 2018-9-12 17:43:40
 */
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, state => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, state => state.get('loading'));

export const makeSelectData = selectState =>
  createSelector(selectState, state => state.get('data'));

export const makeSelectDataSource = selectState =>
  createSelector(selectState, state => state.get('dataSource'));
