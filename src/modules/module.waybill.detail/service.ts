/**
 * waybill.list Service
 * @author yanw
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';

// API list
const DETAIL_RESOURCE = '/ehuodiGateway/utmsTrade/utmsTrade/selectTradeDetail';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', DETAIL_RESOURCE)
    .reply(() =>
      import('./mock/detail').then(exports => [200, exports.default]),
    );
}

/**
 * 获取资源信息
 */
export const getResources = data => {
  return request(
    {
      method: 'post',
      url: DETAIL_RESOURCE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
