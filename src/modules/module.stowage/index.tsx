/**
 * Stowage Component
 * @author hefan
 * @date 2018/9/21 上午8:47:06
 */
import { push } from 'connected-react-router';
import msngr from 'msngr';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Form from 'antd/es/form';
import FormItem from 'antd/es/form/FormItem';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Spin from 'antd/es/spin';

import MyTable from 'components/MyTable';
import { conditionItemLayout, conditionLayout } from 'utils/conditionLayout';
const formItemLayout = { ...conditionItemLayout, colon: false };
import {
  PageBottom,
  PageBottomButtons,
  PageBottomCheck,
} from 'components/PageBottom';
const RangePicker = DatePicker.RangePicker;
import { toYYYYMMDDE, toYYYYMMDDS } from 'utils/dateTimeHelper';
import GLOBAL from 'utils/GLOBAL';
import ModalForm from './ModalForm.tsx';
const { emptyRecord } = GLOBAL;

const columns = [
  {
    title: '第三方单号',
    dataIndex: 'thirdSystemId',
    key: 'thirdSystemId',
    fixed: 'left',
    width: 110,
  },
  {
    title: '订单号',
    dataIndex: 'orderNumber',
    key: 'orderNumber',
    render: (text, record) => (
      <Link to={`/ltlOrder/detail?id=${record.utmsPartOrderId}`}>{text}</Link>
    ),
    fixed: 'left',
    width: 250,
  },
  {
    title: '状态',
    dataIndex: 'status',
    key: 'status',
    width: 80,
  },
  {
    title: '下单时间',
    dataIndex: 'gmtCreate',
    key: 'gmtCreate',
    width: 190,
  },
  {
    title: '要求送达时间',
    dataIndex: 'requireDate',
    key: 'requireDate',
    width: 190,
    render: text => `${text || emptyRecord}`,
  },
  {
    title: '收货人',
    dataIndex: 'contact',
    key: 'contact',
    width: 90,
  },
  {
    title: '联系电话',
    dataIndex: 'phone',
    key: 'phone',
    width: 140,
  },
  {
    title: '收货地址',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: '客户',
    dataIndex: 'customerName',
    key: 'customerName',
    width: 150,
  },
];

import {
  makeSelectData,
  makeSelectDataCount,
  makeSelectDepotCount,
  makeSelectDepotData,
  makeSelectError,
  makeSelectLoading,
  makeSelectVehicleData,
} from './selectors';

import styles from './index.module.less';

export interface IStowageProps {
  children: React.ReactChildren;
  data: any;
  vehicleData: any;
  getAsyncData: (params?: {}) => any;
  getDepotData: () => void;
  depotCount: number;
  goAddDepotPage: () => void;
}

const defaultModelData = {
  name: '',
  warehouseId: null,
  orderNum: 0,
  count: 0,
  volume: 0,
  load: 0,
  isOverload: false,
  recommend: '',
};

class Stowage extends React.PureComponent<IStowageProps> {
  state = {
    selectedRowKeys: [],
    modelData: {
      ...defaultModelData,
    },
    showModel: false,
    skipCount: 0,
    pageSize: 15,
    current: 1,
  };
  public componentDidMount() {
    this.fetchDataFun();

    this.props.getDepotData();

    msngr('GLOBAL', 'activeRouteId', 'change').on(this.getDepotHandle);
  }
  public componentDidUpdate(prevProps) {
    if (prevProps.depotCount === -1 && this.props.depotCount === 0) {
      Modal.error({
        title: '跳转到新键地址',
        content: (
          <div>
            <p>当前无仓库装货位置，请先创建仓库位置</p>
          </div>
        ),
        okText: '去创建',
        onOk: () => {
          this.props.goAddDepotPage();
        },
      });
    }
  }

  public componentWillUnmount() {
    msngr('GLOBAL', 'activeRouteId', 'change').drop(this.getDepotHandle);
  }

  getDepotHandle = (routeId: string) => {
    if (routeId.split('?').includes('/stowage')) {
      this.props.getDepotData();
    }
  };

  public getFields() {
    const { getFieldDecorator } = this.props.form;
    const layout = {
      xxl: { span: 6 },
      xl: { span: 6 },
      md: { span: 12 },
      sm: { span: 12 },
      xs: { span: 24 },
    };
    return (
      <Form>
        <Row gutter={10}>
          <Col {...layout}>
            <FormItem label="客户名称" {...formItemLayout}>
              {getFieldDecorator('customerName')(
                <Input placeholder="请输入客户名称" />,
              )}
            </FormItem>
          </Col>
          <Col {...layout}>
            <FormItem label="第三方单号" {...formItemLayout}>
              {getFieldDecorator('thirdSystemId')(
                <Input placeholder="请输入第三方订单号" />,
              )}
            </FormItem>
          </Col>
          <Col {...layout}>
            <FormItem label="订单号" {...formItemLayout}>
              {getFieldDecorator('orderNumber')(
                <Input placeholder="请输入订单号" />,
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={10}>
          <Col {...layout}>
            <FormItem label="收货地址" {...formItemLayout}>
              {getFieldDecorator('address')(
                <Input placeholder="请输入收货地址" />,
              )}
            </FormItem>
          </Col>
          <Col {...layout}>
            <FormItem label="下单时间" {...formItemLayout}>
              {getFieldDecorator('dataRange')(<RangePicker />)}
            </FormItem>
          </Col>
          <Col {...layout}>
            <FormItem {...formItemLayout}>
              <Button
                onClick={this.searchHandle}
                type="primary"
                htmlType="submit"
                style={{ marginRight: '12px', marginLeft: '6px' }}
              >
                查询
              </Button>
              <Button onClick={this.reSetHandle}>重置</Button>
            </FormItem>
            {/* <FormItem {...formItemLayout}>

            </FormItem> */}
          </Col>
        </Row>
      </Form>
    );
  }
  public getTableList() {
    const { data, dataCount, loading } = this.props;
    const {
      modelData: { isOverload },
      selectedRowKeys,
      current,
    } = this.state;

    const dataSource = data ? data.toJS() : [];

    const rowSelection = {
      selectedRowKeys: this.state.selectedRowKeys,
      onChange: this.onSelectChange,
      getCheckboxProps: record => ({
        disabled: record.status === '已发货', // Column configuration not to be checked
        status: record.status,
      }),
    };

    const paginationConfig = {
      current,
      total: dataCount,
      onChange: this.paginationChange,
      onShowSizeChange: this.paginationChange,
    };

    return (
      <Spin spinning={loading}>
        <MyTable
          rowSelection={rowSelection}
          columns={columns}
          dataSource={dataSource}
          pagination={paginationConfig}
          rowKey="utmsPartOrderId"
          scroll={{ x: 1500 }}
        />
        <PageBottom
          leftChild={
            <PageBottomCheck
              total={dataSource.length}
              checked={this.state.selectedRowKeys.length}
              onAllCheckChanged={this.onAllCheckChanged}
            />
          }
          rightChild={
            <PageBottomButtons
              buttons={[
                <Button
                  key={1}
                  type="primary"
                  onClick={this.orderToGroupHandle}
                  disabled={isOverload || !selectedRowKeys.length}
                >
                  配载到组
                </Button>,
              ]}
            />
          }
        >
          {isOverload ? (
            <div className={styles.BottomTip}>超载，不允许添加分组</div>
          ) : null}
        </PageBottom>
      </Spin>
    );
  }

  public render() {
    const { depotList } = this.props;

    const { showModel, modelData, selectedRowKeys } = this.state;

    return (
      <Card className={styles.Stowage}>
        <div className={styles.FormWrap}>{this.getFields()}</div>
        {this.getTableList()}
        <ModalForm
          visible={showModel}
          okHandle={this.allowOrderToGroupHandle}
          cancleHandle={() => {
            this.handleCancel(true);
          }}
          data={modelData}
          depotList={depotList}
          selectedRowKeys={selectedRowKeys}
        />
      </Card>
    );
  }

  /**
   * 设置选择项
   */
  private onSelectChange = (selectedRowKeys: [], selectedRows) => {
    const { modelData } = this.state;
    let { count, volume, load, isOverload } = modelData;

    load = 0;
    volume = 0;
    count = 0;

    selectedRows.map(item => {
      load += item.goodsWeight;
      volume += item.goodsVolume;
      count += item.goodsNumber;
    });

    isOverload = volume > 50 ? true : false;

    this.setState({
      selectedRowKeys,
      modelData: {
        ...modelData,
        orderNum: selectedRowKeys.length,
        count,
        volume,
        load,
        isOverload,
      },
    });
  };

  /**
   * 全选
   */
  private onAllCheckChanged = checked => {
    const data = this.props.data.toJS();

    if (!checked) {
      // this.setState({ selectedRowKeys: [] });
      this.onSelectChange([], []);
    } else {
      const selectedRowKeys = [];
      data.map(d => {
        selectedRowKeys.push(d.utmsPartOrderId);
      });

      this.onSelectChange(selectedRowKeys, data);
    }
  };

  /**
   * 重置
   */
  private reSetHandle = () => {
    const { form } = this.props;
    form.resetFields();

    this.searchHandle();
  };

  /**
   * 页码变化
   */
  private paginationChange = params => {
    const { skipCount, pageSize, current } = params;

    this.setState({
      skipCount,
      pageSize,
      current,
    });
    this.onSelectChange([], []);
    this.fetchDataFun({
      skipCount,
      pageSize,
    });
  };

  /**
   * 查询
   */
  private searchHandle = () => {
    this.setState({
      skipCount: 0,
      current: 1,
    });

    this.onSelectChange([], []);
    this.fetchDataFun();
  };

  private fetchDataFun(params?: {}) {
    const values = this.props.form.getFieldsValue();
    const { pageSize } = this.state;

    const { dataRange, ...options } = values;
    const createDateStart = dataRange ? toYYYYMMDDS(dataRange[0]) : undefined;
    const createDateEnd = dataRange ? toYYYYMMDDE(dataRange[1]) : undefined;

    this.props.getAsyncData({
      pageSize,
      skipCount: 0,
      createDateStart,
      createDateEnd,
      ...params,
      ...options,
    });
  }

  /**
   * 配载到组
   */
  private orderToGroupHandle = () => {
    const { selectedRowKeys, modelData } = this.state;

    const recommend = this.vehicleType();

    this.setState({
      showModel: true,
      modelData: {
        ...modelData,
        recommend,
      },
    });
  };

  private vehicleType(): string {
    const vehicleData = this.props.vehicleData.toJS();
    const { modelData } = this.state;

    for (const element of vehicleData) {
      if (element.vehicleVolume >= Number(modelData.volume)) {
        return `${element.vehicleLong / 1000}米${element.vehicleType}`;
      }
    }
    return '';
  }

  /**
   * 确认配载到组
   */
  private allowOrderToGroupHandle = params => {
    const { skipCount, pageSize } = this.state;
    if (params.result) {
      this.handleCancel();
      this.fetchDataFun({
        skipCount,
        pageSize,
      });
    }
  };

  private handleCancel = (keepSelected?: boolean) => {
    const state = {
      showModel: false,
    };
    // 不保留
    if (!keepSelected) {
      Object.assign(state, {
        selectedRowKeys: [],
        modelData: {
          ...defaultModelData,
        },
      });
    }
    this.setState(state);
  };
}

const StowageForm = Form.create()(Stowage);

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        data: makeSelectData(currentState),
        vehicleData: makeSelectVehicleData(currentState),
        dataCount: makeSelectDataCount(currentState),
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        depotList: makeSelectDepotData(currentState),
        depotCount: makeSelectDepotCount(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getAsyncData: (params?: {}) => dispatch(actions.getAsyncData(params)),
        getDepotData: () => dispatch(actions.getDepotData()),
        goAddDepotPage: () => dispatch(actions.goAddDepotPage()),
      }),
    };
  })(StowageForm);
};
