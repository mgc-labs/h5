/**
 * Stowage model
 * @author hefan
 * @date 2018/9/21 上午8:47:06
 */
import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { fromJS, List, Map } from 'immutable';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { getData, reqSelectDepotList, reqVehicleInfoList } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: List(),
      error: false,
      loading: false,
      dataCount: 0,
      vehicleData: List(),
      depotList: List(),
      depotCount: -1,
    }),
    actions: {
      GET_ASYNC_DATA: params => params,
      GET_ASYNC_DATA_ERROR: error => error,
      GET_ASYNC_DATA_SUCCESS: data => data,
      SET_ORDER_TO_GROUP: params => params,
      SET_ORDER_TO_GROUP_ERROR: error => error,
      SET_ORDER_TO_GROUP_SUCCESS: data => data,
      GET_DEPOT_DATA: () => ({}),
      GET_DEPOT_DATA_ERROR: error => error,
      GET_DEPOT_DATA_SUCCESS: data => data,
      GO_ADD_DEPOT_PAGE: () => ({}),
    },
    reducers: {
      GET_ASYNC_DATA(state) {
        return state.set('error', false).set('loading', true);
      },
      GET_ASYNC_DATA_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_ASYNC_DATA_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('dataCount', action.payload.listData.count)
          .set('data', fromJS(action.payload.listData.data))
          .set('vehicleData', fromJS(action.payload.newVehicleData));
      },
      SET_ORDER_TO_GROUP(state, action) {
        return state.set('loading', true);
      },
      SET_ORDER_TO_GROUP_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      SET_ORDER_TO_GROUP_SUCCESS(state, action) {
        return state.set('loading', false);
      },
      GET_DEPOT_DATA(state) {
        return state.set('depotCount', -1);
      },
      GET_DEPOT_DATA_SUCCESS(state, action) {
        return state
          .set('depotList', action.payload.data)
          .set('depotCount', action.payload.count);
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const [listData, vehicleData] = yield all([
            call(getData, {
              ...action.payload,
              status: ['0'],
            }),
            call(reqVehicleInfoList),
          ]);

          const newVehicleData = vehicleData.data.filter(item => {
            if (['厢式货车', '中面', '小面'].includes(item.vehicleType)) {
              return item;
            }
          });

          newVehicleData.sort((a, b) => {
            return a.vehicleVolume - b.vehicleVolume;
          });
          yield put(actions.getAsyncDataSuccess({ listData, newVehicleData }));
        } catch (err) {
          yield put(actions.getAsyncDataError(err));
        }
      }

      function* fetchDeoptList(action) {
        try {
          const data = yield call(reqSelectDepotList, {
            depotType: 'store',
          });

          yield put(actions.getDepotDataSuccess(data));
        } catch (error) {
          yield put(actions.getDepotDataError(error));
        }
      }

      function* goAddDepotPage() {
        yield put(push('/company/addAddressManage'));
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ASYNC_DATA, fetchData);
        yield takeLatest(CONSTANTS.GET_DEPOT_DATA, fetchDeoptList);
        yield takeLatest(CONSTANTS.GO_ADD_DEPOT_PAGE, goAddDepotPage);
      };
    },
  });
