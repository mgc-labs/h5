/**
 * Stowage service
 * @author hefan
 * @date 2018/9/21 上午8:47:06
 */
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const ODERTOGROUP = '/ehuodiGateway/utmsTrade/utmsPartGroup/partOder2Group';

if (process.env.NODE_ENV === 'development') {
  // mocker
  //   .on('post', ODERTOGROUP)
  //   .reply(() =>
  //     import('./mock/partOder2Group').then(exports => [200, exports.default]),
  //   );
}

/**
 * 配载到组
 * @param data
 */
export const reqSetOderToGroup = data => {
  return request(
    {
      method: 'post',
      url: ODERTOGROUP,
      data,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
