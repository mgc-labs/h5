/**
 * Stowage service
 * @author hefan
 * @date 2018/9/21 上午8:47:06
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const PARTORDERLIST = '/ehuodiGateway/utmsTrade/utmsPartOrder/partOrderList';
const SELECTDEPOTLIST = '/ehuodiGateway/utmsCore/utmsDepotcs/selectDepotList';
const VEHICLEINFOLIST =
  '/ehuodiGateway/utmsDispatch/utmsVehicleTypeInfocs/selectUtmsVehicleTypeInfo';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  // mocker
  //   .on('post', PARTORDERLIST)
  //   .reply(() => import('./mock/mock').then(exports => [200, exports.default]));
  // mocker
  //   .on('post', ODERTOGROUP)
  //   .reply(() =>
  //     import('./mock/partOder2Group').then(exports => [200, exports.default]),
  //   );
  // mocker
  //   .on('post', SELECTDEPOTLIST)
  //   .reply(() =>
  //     import('./mock/selectDepotList').then(exports => [200, exports.default]),
  //   );
  // mocker
  //   .on('post', VEHICLEINFOLIST)
  //   .reply(() =>
  //     import('./mock/vehicleInfo').then(exports => [200, exports.default]),
  //   );
}

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getData = data => {
  return request(
    {
      method: 'post',
      url: PARTORDERLIST,
      data,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

/**
 * 查询仓库
 * @param data
 */
export const reqSelectDepotList = data => {
  return request(
    {
      method: 'post',
      url: SELECTDEPOTLIST,
      data: qs.stringify(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

/**
 * 车辆信息
 * @param data
 */
export const reqVehicleInfoList = data => {
  return request(
    {
      method: 'post',
      url: VEHICLEINFOLIST,
      data,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
