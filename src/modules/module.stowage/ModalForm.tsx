import styles from 'modules/module.stowage/index.module.less';
import React from 'react';

import Alert from 'antd/es/alert';
import Button from 'antd/es/button';
import Col from 'antd/es/col';
import Form from 'antd/es/form';
import FormItem from 'antd/es/form/FormItem';
import Input from 'antd/es/input';
import message from 'antd/es/message';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import { InputBox } from 'components/InputBox';
const Option = Select.Option;
const { confirm } = Modal;

import { reqSetOderToGroup } from './modelService';

function round(number: number, precision: number) {
  return Math.round(+number + 'e' + precision) / Math.pow(10, precision);
}

interface IModalFormProps {
  visible: boolean;
  data: {};
  form: any;
  depotList: [];
  okHandle?: (params: {}) => void;
  cancleHandle?: () => void;
  selectedRowKeys: [];
}

class FormModule extends React.PureComponent<IModalFormProps> {
  state = {
    confirmLoading: false,
  };
  handleOk = async () => {
    const values = this.props.form.getFieldsValue();
    this.setState({
      confirmLoading: true,
    });
    try {
      const data = await reqSetOderToGroup({
        partOrderIds: this.props.selectedRowKeys,
        ...values,
      });
      message.success('已完成配载');
      this.props.okHandle(data);
    } catch (error) {
      // ignore
    } finally {
      this.setState({
        confirmLoading: false,
      });
    }
  };
  handleCancel = () => {
    const change = this.props.form.isFieldsTouched();

    if (change) {
      confirm({
        title: '有未保存的信息',
        content: <p>如放弃，填写的信息将丢失</p>,
        okText: '继续填写',
        cancelText: '放弃',
        onCancel: () => {
          this.props.cancleHandle();
        },
      });
    } else {
      this.props.cancleHandle();
    }
  };

  render() {
    const {
      visible,
      data: { count, load, volume, orderNum, recommend },
      form,
      depotList,
    } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        span: 4,
      },
      wrapperCol: {
        span: 15,
      },
    };

    return (
      <Modal
        visible={visible}
        title="创建订单组"
        onOk={this.handleOk}
        centered={true}
        maskClosable={false}
        onCancel={this.handleCancel}
        className={styles.StowageModel}
        footer={[
          <Button key="back" onClick={this.handleCancel}>
            取消
          </Button>,
          <Button
            key="submit"
            type="primary"
            loading={this.state.confirmLoading}
            onClick={() => {
              this.handleOk();
            }}
          >
            确定
          </Button>,
        ]}
      >
        <Row>
          <Alert message={`建议车型：${recommend}`} type="info" showIcon />
        </Row>
        <Row>
          <Col xs={12}>
            <div className={styles.ListTerm}>订单数：</div>
            <div className={styles.ListDetail}>{orderNum}个</div>
          </Col>
          <Col xs={12}>
            <div className={styles.ListTerm}>总件数：</div>
            <div className={styles.ListDetail}>{count}件</div>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <div className={styles.ListTerm}>总体积：</div>
            <div className={styles.ListDetail}>
              {volume && round(volume, 2)}方
            </div>
          </Col>
          <Col xs={12}>
            <div className={styles.ListTerm}>总重量：</div>
            <div className={styles.ListDetail}>
              {load && round(load, 2)}
              千克
            </div>
          </Col>
        </Row>
        <FormItem {...formItemLayout} label="仓库名：">
          {getFieldDecorator('warehouseId', {
            rules: [
              {
                required: true,
                message: '请选择仓库',
              },
            ],
          })(
            <Select
              style={{
                width: '100%',
              }}
              placeholder="请选择仓库"
            >
              {depotList.map(item => (
                <Option value={item.utmsDepotId} key={item.utmsDepotId}>
                  {item.depotName}
                </Option>
              ))}
            </Select>,
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="分组名称：">
          {getFieldDecorator('name', {
            initialValue: '',
            rules: [
              {
                required: true,
                message: '请输入分组名称',
                max: 50,
              },
            ],
          })(<InputBox placeholder="请输入分组名称" maxLength={50} />)}
        </FormItem>
      </Modal>
    );
  }
}

const ModalForm = Form.create({
  mapPropsToFields(props) {
    return {
      name: Form.createFormField({
        name: props.data.name,
      }),
      warehouseId: Form.createFormField({
        warehouseId: props.data.warehouseId,
      }),
    };
  },
})(FormModule);

export default ModalForm;
