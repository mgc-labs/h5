/**
 * Stowage Loadable
 * @author hefan
 * @date 2018/9/21 上午8:47:06
 */
import Loadable from 'react-loadable';

import LoadingIndicator from 'components/LoadingIndicator';

let Component;
if (process.env.NODE_ENV === 'development') {
  Component = require('./index').default;
} else {
  Component = config =>
    Loadable({
      loader: () =>
        import(/* webpackChunkName: "stowage" */ './index').then(exports => {
          return exports.default(config);
        }),
      loading: LoadingIndicator,
    });
}

export default Component;

export { default as modelFactory } from './model';
