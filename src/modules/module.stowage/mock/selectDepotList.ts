export default {
  result: 'success',
  count: 2,
  data: [
    {
      utmsDepotId: 101419, // 地址编号
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      depotType: 'store', // 地址类型
      depotName: '仓库001', // 地址名称
      contact: '王经理', // 联系人
      contactWay: '18109281203', // 联系方式
      province: '',
      city: '',
      region: '',
      address: '',
      longitude: 120.02823,
      latitude: 30.02823,
      isDefault: 1,
    },
    {
      utmsDepotId: 101420,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      depotType: 'store', // 地址类型
      depotName: '仓库002', // 地址名称
      contact: '王经理', // 联系人
      contactWay: '18109281203', // 联系方式
      province: '',
      city: '',
      region: '',
      address: '',
      longitude: 120.02823,
      latitude: 30.02823,
      isDefault: 1,
    },
    {
      utmsDepotId: 101421,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      depotType: 'store', // 地址类型
      depotName: '仓库003', // 地址名称
      contact: '王经理', // 联系人
      contactWay: '18109281203', // 联系方式
      province: '',
      city: '',
      region: '',
      address: '',
      longitude: 120.02823,
      latitude: 30.02823,
      isDefault: 1,
    },
  ],
  code: '',
  msg: '地址列表查询成功 ',
};
