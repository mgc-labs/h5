export default {
  result: 'success',

  count: 6,

  data: [
    {
      utmsPartOrderId: 1, // 主键

      orderNumber: 'O0003',

      status: '待配载',

      customerCode: 'A000003',

      customerName: '张三',

      thirdSystemId: 'B00005',

      requireDate: '2018-09-10 11:00:00',

      contact: '张三',

      phone: '13822302819',

      address: '萧山区传化大厦',

      province: '浙江省杭州市',

      longitude: 120.7760657,

      latitude: 30.296932,

      goodsName: '快销品',

      goodsNumber: 11, // 件数

      goodsWeight: 0.011,

      goodsVolume: 11,

      delegateAmount: 3520,

      isReceipt: true,

      remark: '急',

      gmtCreate: '2018-09-15 10:32:50',
    },
    {
      utmsPartOrderId: 2, // 主键

      orderNumber: 'O0003',

      status: '待配载',

      customerCode: 'A000003',

      customerName: '张三',

      thirdSystemId: 'B00005',

      requireDate: '2018-09-10 11:00:00',

      contact: '张三',

      phone: '13822302819',

      address: '萧山区传化大厦',

      province: '浙江省杭州市',

      longitude: 120.7760657,

      latitude: 30.296932,

      goodsName: '快销品',

      goodsNumber: 11, // 件数

      goodsWeight: 0.011,

      goodsVolume: 11,

      delegateAmount: 3520,

      isReceipt: true,

      remark: '急',

      gmtCreate: '2018-09-15 10:32:50',
    },
    {
      utmsPartOrderId: 3, // 主键

      orderNumber: 'O0003',

      status: '待配载',

      customerCode: 'A000003',

      customerName: '张三',

      thirdSystemId: 'B00005',

      requireDate: '2018-09-10 11:00:00',

      contact: '张三',

      phone: '13822302819',

      address: '萧山区传化大厦',

      province: '浙江省杭州市',

      longitude: 120.7760657,

      latitude: 30.296932,

      goodsName: '快销品',

      goodsNumber: 11, // 件数

      goodsWeight: 0.011,

      goodsVolume: 11,

      delegateAmount: 3520,

      isReceipt: true,

      remark: '急',

      gmtCreate: '2018-09-15 10:32:50',
    },
    {
      utmsPartOrderId: 4, // 主键

      orderNumber: 'O0003',

      status: '待配载',

      customerCode: 'A000003',

      customerName: '张三',

      thirdSystemId: 'B00005',

      requireDate: '2018-09-10 11:00:00',

      contact: '张三',

      phone: '13822302819',

      address: '萧山区传化大厦',

      province: '浙江省杭州市',

      longitude: 120.7760657,

      latitude: 30.296932,

      goodsName: '快销品',

      goodsNumber: 11, // 件数

      goodsWeight: 0.011,

      goodsVolume: 11,

      delegateAmount: 3520,

      isReceipt: true,

      remark: '急',

      gmtCreate: '2018-09-15 10:32:50',
    },
    {
      utmsPartOrderId: 5, // 主键

      orderNumber: 'O0003',

      status: '待配载',

      customerCode: 'A000003',

      customerName: '张三',

      thirdSystemId: 'B00005',

      requireDate: '2018-09-10 11:00:00',

      contact: '张三',

      phone: '13822302819',

      address: '萧山区传化大厦',

      province: '浙江省杭州市',

      longitude: 120.7760657,

      latitude: 30.296932,

      goodsName: '快销品',

      goodsNumber: 11, // 件数

      goodsWeight: 0.011, // 重量

      goodsVolume: 11, // 体积

      delegateAmount: 3520,

      isReceipt: true,

      remark: '急',

      gmtCreate: '2018-09-15 10:32:50',
    },
    {
      utmsPartOrderId: 6, // 主键

      orderNumber: 'O0003',

      status: '待配载',

      customerCode: 'A000003',

      customerName: '张三',

      thirdSystemId: 'B00005',

      requireDate: '2018-09-10 11:00:00',

      contact: '张三',

      phone: '13822302819',

      address: '萧山区传化大厦',

      province: '浙江省杭州市',

      longitude: 120.7760657,

      latitude: 30.296932,

      goodsName: '快销品',

      goodsNumber: 11, // 件数

      goodsWeight: 0.011,

      goodsVolume: 11,

      delegateAmount: 3520,

      isReceipt: true,

      remark: '急',

      gmtCreate: '2018-09-15 10:32:50',
    },
  ],

  code: 'EHD123456789123',

  msg: '查询成功 ',
};
