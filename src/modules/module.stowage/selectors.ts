/**
 * Stowage selectors
 * @author hefan
 * @date 2018/9/21 上午8:47:06
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectVehicleData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('vehicleData'),
  );

export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('dataCount'),
  );

export const makeSelectDepotData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('depotList'),
  );

export const makeSelectDepotCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('depotCount'),
  );
