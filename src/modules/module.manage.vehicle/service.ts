/**
 * ManageVehicle Service
 * @author chengle
 * @date 2018-9-13 15:44:27
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
// import { USE_MOCK } from '../../CONFIG';
const USE_MOCK = false;

// API list
const API_LIST =
  '/ehuodiGateway/utmsDispatch/utmsVehiclecs/selectUtmsVehicleList';
const API_DELETE =
  '/ehuodiGateway/utmsDispatch/utmsVehiclecs/deleteUtmsVehicle';
const API_ADDITION = '/ehuodiGateway/utmsDispatch/utmsVehiclecs/addUtmsVehicle';
const API_UPDATE =
  '/ehuodiGateway/utmsDispatch/utmsVehiclecs/modifyUtmsVehicle';
const API_BINDING =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/bindingUtmsDriverAndVehicle';
const API_DETAILS =
  '/ehuodiGateway/utmsDispatch/utmsVehiclecs/selectUtmsVehicleDetail';
const API_DRIVER =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/selectUnbindingUtmsDriverList';
const API_DELETEVEHICLE =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/untiedUtmsDriverAndVehicle'; // 删除绑定的司机

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_LIST)
    .reply(() => import('./mock/list').then(exports => [200, exports.default]));
}

/**
 * 获取车辆列表
 */
export const getList = ops =>
  request(
    {
      method: 'post',
      url: API_LIST,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 获取车辆详情
 */
export const getDetails = ops =>
  request(
    {
      method: 'post',
      url: API_DETAILS,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 删除车辆
 */
export const setDelete = ops =>
  request(
    {
      method: 'post',
      url: API_DELETE,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 添加车辆
 */
export const addVehicle = ops =>
  request(
    {
      method: 'post',
      url: API_ADDITION,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 修改车辆
 */
export const editVehicle = ops =>
  request(
    {
      method: 'post',
      url: API_UPDATE,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 绑定司机
 */
export const bindingDriver = ops =>
  request(
    {
      method: 'post',
      url: API_BINDING,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 删除司机
 */
export const setDeleteDriver = ops =>
  request(
    {
      method: 'post',
      url: API_DELETEVEHICLE,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 查询未绑定的司机列表
 */
export const getDriver = () =>
  request(
    {
      method: 'post',
      url: API_DRIVER,
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);
