/**
 * ManageVehicle selectors
 * @author chengle
 * @date 2018-9-14 13:25:46
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('dataCount'),
  );
