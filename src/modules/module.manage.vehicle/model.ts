/**
 * ManageVehicle model
 * @author chengle
 * @date 2018-9-13 15:44:27
 */
import message from 'antd/es/message';
import { fromJS, List, Map } from 'immutable';
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { getList } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: List(),
      filter: Map(),
      dataCount: 0,
      error: false,
      loading: false,
    }),
    actions: {
      // 列表
      GET_ASYNC_LIST: data => data,
      GET_ASYNC_LIST_ERROR: error => error,
      GET_ASYNC_LIST_SUCCESS: data => data,
    },
    reducers: {
      // 列表
      GET_ASYNC_LIST(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_ASYNC_LIST_ERROR(state, action) {
        return state.set('error', action.error).set('loading', false);
      },
      GET_ASYNC_LIST_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data))
          .set('dataCount', action.payload.count);
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchList(action) {
        const { payload } = action;
        try {
          const data = yield call(getList, payload);
          yield put(actions.getAsyncListSuccess(data));
        } catch (err) {
          yield put(actions.getAsyncListError(err));
          message.error(err.toString());
        }
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ASYNC_LIST, fetchList);
      };
    },
  });
