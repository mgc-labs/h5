/**
 * ManageVehicle Component
 * @author chengle
 * @date 2018-9-14 13:25:46
 */
import {
  Button,
  Card,
  Cascader,
  Col,
  Divider,
  Form,
  Icon,
  Input,
  Modal,
  Pagination,
  Popconfirm,
  Row,
  Select,
} from 'antd';
import { WrappedFormUtils } from 'antd/es/form/Form';
import MyTable from 'components/MyTable';
import { OptionButtons } from 'components/OptionButtons';
import { List, Map } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import ModalForm from './form';
import DetailsModule from './vehicleDetails';

import {
  makeSelectData,
  makeSelectDataCount,
  makeSelectError,
  makeSelectLoading,
  // makeSelectPageIndex,
} from './selectors';

import styles from './index.module.less';

import { addVehicle, editVehicle, getDetails, setDelete } from './service';

import GLOBAL from 'utils/GLOBAL.ts';

const Search = Input.Search;
const Option = Select.Option;
const confirm = Modal.confirm;
const FormItem = Form.Item;

export interface IManageVehicleProps {
  children: React.ReactChildren;
  list: List<Map<string, any>>;
  dataCount: number;
  // pageIndex: number;
  loading: boolean;
  form: WrappedFormUtils;
  getAsyncList: (options?: object) => any;
}

@Form.create()
class ManageVehicle extends React.PureComponent<IManageVehicleProps> {
  state = {
    current: 1,
    skipCount: 0,
    pageSize: 15,
    ModalTitle: '添加车辆',
    visible: false,
    confirmLoading: false,
    detailsVisible: false,
    utmsVehicleId: Number,
    detailData: {
      identificationCode: '',
      vehicleAttributive: '',
      plateNumber: '',
      engineNumber: '',
      brandModel: '',
      energyType: '',
      vehicleLong: '',
      vehicleType: '',
      approvedLoad: '',
      vehicleRealLong: '',
      vehicleRealWidth: '',
      vehicleRealHeight: '',
      plateColor: '',
      utmsUsersDriversId: '',
      driverName: '',
      phoneNumber: '',
    },
    driverData: {},
  };
  private formRef;

  public componentDidMount() {
    this.handleSubmit();
  }

  // 添加车辆
  addVehicle = () => {
    this.setState({
      ModalTitle: '添加车辆',
      confirmLoading: false,
      visible: true,
      driverData: {},
    });
  };
  handleOk = () => {
    const form = this.formRef.props.form;
    form.validateFields(async (err, values) => {
      if (err) {
        return;
      }
      if (!values.utmsUsersDriversId) {
        delete values.utmsUsersDriversId;
      }
      if (values.carModelCarLong && values.carModelCarLong.length > 0) {
        values.vehicleType = values.carModelCarLong[0].vehicleType;
        values.vehicleLong = values.carModelCarLong[0].vehicleLong;
        delete values.carModelCarLong;
      }
      this.setState({
        confirmLoading: true,
      });
      if (this.state.ModalTitle === '添加车辆') {
        await addVehicle(values).then(
          data => {
            this.setState({ visible: false, confirmLoading: false }, () => {
              form.resetFields();
              this.handleSubmit();
            });
          },
          () => {
            this.setState({
              confirmLoading: false,
            });
          },
        );
      } else {
        await editVehicle(
          Object.assign(values, { utmsVehicleId: this.state.utmsVehicleId }),
        ).then(
          data => {
            this.setState({ visible: false, confirmLoading: false }, () => {
              form.resetFields();
              this.handleSubmit();
            });
          },
          () => {
            this.setState({
              confirmLoading: false,
            });
          },
        );
      }
    });
  };
  handleCancel = () => {
    const form = this.formRef.props.form;
    // form.resetFields();
    this.setState({
      visible: false,
      utmsVehicleId: Number,
    });
  };

  // 编辑车辆
  public editVehicle = value => {
    this.getDetailsFn(value.utmsVehicleId, () => {
      const form = this.formRef.props.form;
      if (this.state.detailData.driverName) {
        this.setState(
          {
            ModalTitle: '编辑车辆',
            confirmLoading: false,
            visible: true,
            utmsVehicleId: value.utmsVehicleId,
            driverData: {
              driverName: this.state.detailData.driverName,
              phoneNumber: this.state.detailData.phoneNumber,
              utmsUsersDriversId: this.state.detailData.utmsUsersDriversId,
            },
          },
          form.setFieldsValue({
            identificationCode: this.state.detailData.identificationCode,
            vehicleAttributive: this.state.detailData.vehicleAttributive + '',
            plateNumber: this.state.detailData.plateNumber,
            engineNumber: this.state.detailData.engineNumber,
            brandModel: this.state.detailData.brandModel,
            energyType: this.state.detailData.energyType
              ? this.state.detailData.energyType + ''
              : '',
            carModelCarLong: [
              {
                value: `${this.state.detailData.vehicleLong / 1000}米${
                  this.state.detailData.vehicleType
                }`,
              },
            ],
            approvedLoad: this.state.detailData.approvedLoad,
            vehicleRealLong: this.state.detailData.vehicleRealLong,
            vehicleRealWidth: this.state.detailData.vehicleRealWidth,
            vehicleRealHeight: this.state.detailData.vehicleRealHeight,
            plateColor: this.state.detailData.plateColor,
            utmsUsersDriversId: this.state.detailData.utmsUsersDriversId,
          }),
        );
      } else {
        this.setState(
          {
            ModalTitle: '编辑车辆',
            confirmLoading: false,
            visible: true,
            utmsVehicleId: value.utmsVehicleId,
            driverData: {},
          },
          form.setFieldsValue({
            identificationCode: this.state.detailData.identificationCode,
            vehicleAttributive: this.state.detailData.vehicleAttributive + '',
            plateNumber: this.state.detailData.plateNumber,
            engineNumber: this.state.detailData.engineNumber,
            brandModel: this.state.detailData.brandModel,
            energyType: this.state.detailData.energyType
              ? this.state.detailData.energyType + ''
              : '',
            carModelCarLong: [
              {
                value: `${this.state.detailData.vehicleLong / 1000}米${
                  this.state.detailData.vehicleType
                }`,
              },
            ],
            approvedLoad: this.state.detailData.approvedLoad,
            vehicleRealLong: this.state.detailData.vehicleRealLong,
            vehicleRealWidth: this.state.detailData.vehicleRealWidth,
            vehicleRealHeight: this.state.detailData.vehicleRealHeight,
            plateColor: this.state.detailData.plateColor,
            utmsUsersDriversId: this.state.detailData.utmsUsersDriversId,
          }),
        );
      }
    });
    // console.log(value);
    // console.log(form.getFieldsValue());
  };

  // 删除车辆
  public deleteConfirm = async value => {
    await setDelete({ utmsVehicleId: value.utmsVehicleId }).then(data => {
      if (data.result === 'success') {
        this.handleSubmit();
      }
    });
  };

  // 车辆详情
  public details = value => {
    this.getDetailsFn(value.utmsVehicleId, () => {
      this.setState({
        detailsVisible: true,
      });
    });
  };
  public detailsCancel = () => {
    this.setState({
      detailsVisible: false,
      detailData: {},
    });
  };
  public getDetailsFn = async (id, Fn) => {
    await getDetails({ utmsVehicleId: id }).then(data => {
      // _this.handleSubmit();
      this.setState(
        {
          detailData: data.data, // Object.assign(data.data, this.state.detailData),
        },
        Fn,
      );
    });
  };

  public handleSubmit = (options: any = {}) => {
    this.props.getAsyncList(
      Object.assign(
        {
          skipCount:
            options.skipCount === 0 || options.skipCount
              ? options.skipCount
              : this.state.skipCount,
          pageSize: this.state.pageSize,
        },
        this.props.form.getFieldsValue(),
      ),
    );
  };

  public selectChange = (key, value) => {
    const ops = {};
    ops[key] = value;
    this.props.form.setFieldsValue(ops);
    this.setState({ current: 1, skipCount: 0 }, this.handleSubmit);
  };

  public handleReset = () => {
    this.props.form.resetFields();
    this.setState({ current: 1, skipCount: 0 }, this.handleSubmit);
  };

  public renderFilterForm() {
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    return (
      <Form className={styles.form} layout="inline">
        <Row type="flex">
          <Col span={24}>
            <FormItem label="司机绑定">
              {getFieldDecorator('bindingStatus', {
                initialValue: '',
              })(
                <Select
                  style={{ width: 120 }}
                  onChange={this.selectChange.bind(this, 'bindingStatus')}
                >
                  <Option value="">全部</Option>
                  <Option value="0">未绑定</Option>
                  <Option value="1">已绑定</Option>
                </Select>,
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('searchInfo')(
                <Input
                  style={{ width: 220 }}
                  placeholder="车辆识别码/车牌号码/绑定司机"
                  onKeyDown={e => {
                    if (e.keyCode === 13) {
                      this.selectChange(
                        'searchInfo',
                        this.props.form.getFieldValue('searchInfo'),
                      );
                    }
                  }}
                />,
              )}
              <Button
                type="primary"
                style={{ marginLeft: 16, marginRight: 12 }}
                onClick={this.selectChange.bind('searchInfo')}
              >
                查询
              </Button>
              <Button onClick={this.handleReset}>重置</Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  public renderTable() {
    const { dataCount, list, loading } = this.props;
    const tableProps = {
      rowKey: 'utmsVehicleId',
      columns: [
        {
          title: '车辆识别码',
          dataIndex: 'identificationCode',
          key: 'identificationCode',
          render: (text, record) => {
            return <span className="my-nowrap">{text}</span>;
          },
        },
        {
          title: '发动机号',
          dataIndex: 'engineNumber',
          key: 'engineNumber',
          width: 120,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '品牌型号',
          dataIndex: 'brandModel',
          key: 'brandModel',
          width: 120,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '车牌号',
          dataIndex: 'plateNumber',
          key: 'plateNumber',
          width: 120,
        },
        {
          title: '车辆类型',
          dataIndex: 'vehicleType',
          key: 'vehicleType',
          render: (text, record) => (
            <span className="my-nowrap">
              {record.vehicleLong
                ? record.vehicleLong / 1000 + '米' + record.vehicleType
                : GLOBAL.emptyRecord}
            </span>
          ),
        },
        {
          title: '能源类型',
          dataIndex: 'energyType',
          key: 'energyType',
          render: (text, record) => {
            return record.energyType ? (
              <span className="my-nowrap">
                {['', '柴油', '汽油', '新能源'][text]}
              </span>
            ) : (
              GLOBAL.emptyRecord
            );
          },
        },
        {
          title: '车牌颜色',
          dataIndex: 'plateColor',
          key: 'plateColor',
          render: (text, record) => {
            return (
              <span className="my-nowrap">{text || GLOBAL.emptyRecord}</span>
            );
          },
        },
        {
          title: '绑定司机',
          dataIndex: 'phoneNumber',
          key: 'phoneNumber',
          render: (text, record) =>
            record.phoneNumber ? (
              <Link
                className="my-nowrap"
                to={'/manageDriver?phoneNumber=' + text}
              >
                {text}
              </Link>
            ) : (
              GLOBAL.emptyRecord
            ),
        },
        {
          title: '添加时间',
          dataIndex: 'createDate',
          key: 'createDate',
          render: (text, record) => {
            return (
              <span className="my-nowrap">{text || GLOBAL.emptyRecord}</span>
            );
          },
        },
        {
          title: '操作',
          fixed: 'right',
          width: 105,
          render: (text, record) => (
            <span>
              <OptionButtons>
                <a href="javascript:;" onClick={this.details.bind(this, record)}>
                  详情
                </a>
                {/* <Divider type="vertical" /> */}
                <a
                  href="javascript:;"
                  onClick={this.editVehicle.bind(this, record)}
                >
                  编辑
                </a>
                {/* <Divider type="vertical" /> */}
                <Popconfirm
                  placement="bottom"
                  title={'是否删除该车辆'}
                  onConfirm={this.deleteConfirm.bind(this, record)}
                  okText="是"
                  cancelText="否"
                >
                  <a href="javascript:;">删除</a>
                </Popconfirm>
              </OptionButtons>
            </span>
          ),
        },
      ],
      dataSource: list.toJS(),
      loading,
      scroll: { x: 1350 },
      pagination: {
        current: this.state.current,
        total: dataCount,
        onChange: (pageData, index) => {
          this.setState(
            {
              pageSize: pageData.pageSize,
              skipCount: pageData.skipCount,
              current: pageData.current,
            },
            this.handleSubmit,
          );
        },
        onShowSizeChange: pageData => {
          this.setState(
            {
              pageSize: pageData.pageSize,
              skipCount: 0,
              current: 1,
            },
            this.handleSubmit,
          );
        },
      },
    };
    return <MyTable {...tableProps} />;
  }

  public saveFormRef = formRef => {
    this.formRef = formRef;
  };

  public render() {
    const { visible, confirmLoading, ModalTitle, detailsVisible } = this.state;
    const formItemLayout = {
      labelCol: {
        span: 5,
      },
      wrapperCol: {
        span: 15,
      },
    };
    return (
      <Card title="车辆管理" className={styles.ManageVehicle}>
        <Button
          type="primary"
          className={styles.buttonRight}
          onClick={this.addVehicle}
        >
          <Icon type="usergroup-add" theme="outlined" />
          添加车辆
        </Button>
        {this.renderFilterForm()}
        <div>{this.renderTable()}</div>

        <ModalForm
          wrappedComponentRef={this.saveFormRef}
          visible={visible}
          onCancel={this.handleCancel}
          onCreate={this.handleOk}
          title={ModalTitle}
          confirmLoading={confirmLoading}
          utmsVehicleId={this.state.utmsVehicleId}
          handleSubmit={this.handleSubmit}
          driverData={this.state.driverData}
        />

        <DetailsModule
          visible={detailsVisible}
          detailData={this.state.detailData}
          onCancel={this.detailsCancel}
          onCreate={this.detailsCancel}
        />
      </Card>
    );
  }
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        list: makeSelectData(currentState),
        dataCount: makeSelectDataCount(currentState),
        // pageIndex: makeSelectPageIndex(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getAsyncList: options => dispatch(actions.getAsyncList(options)),
      }),
    };
  })(ManageVehicle);
};
