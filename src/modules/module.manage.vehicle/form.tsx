import { Col, Form, Input, message, Modal, Row, Select } from 'antd';
import { WrappedFormUtils } from 'antd/es/form/Form';
import CarModelCarLong from 'components/CarModelCarLong';
import React, { PureComponent } from 'react';
import { bindingDriver, getDriver, setDeleteDriver } from './service';

const FormItem = Form.Item;
const Option = Select.Option;

interface IModalFormProps {
  title: string;
  form: WrappedFormUtils;
  driverData: any;
  visible: boolean;
  confirmLoading: boolean;
  utmsVehicleId: string;
  handleSubmit: () => void;
  onCancel: () => void;
  onCreate: () => void;
}

@Form.create()
export default class ModalForm extends PureComponent<IModalFormProps> {
  state = {
    btnVisible: true,
    value: undefined,
    identificationCodeStatus: 1,
    DriverData: [],
    selectDriver: Number,
  };

  public componentDidMount() {
    this.getDriverData();
  }

  public getDriverData = async () => {
    try {
      const data = await getDriver();
      const driverData = data.data || [];
      if (this.props.driverData) {
        let isHas = false;
        driverData.map(item => {
          if (
            item.utmsUsersDriversId === this.props.driverData.utmsUsersDriversId
          ) {
            isHas = true;
          }
        });
        if (!isHas) {
          driverData.push(this.props.driverData);
        }
      }
      this.setState({ DriverData: driverData });
    } catch (err) {
      //
    }
  };
  public componentWillReceiveProps(nextProps) {
    if (nextProps.visible !== this.props.visible) {
      this.getDriverData();
    }
    if (!nextProps.visible) {
      this.reset();
    }
  }
  reset = () => {
    if (this.props.form.getFieldsValue().carModelCarLong) {
      this.props.form.setFieldsValue({ carModelCarLong: '' });
    }
    this.props.form.resetFields();
    this.setState({ btnVisible: true, identificationCodeStatus: 1 }, () => {
      this.props.onCancel();
    });
  };
  seva = async value => {
    if (this.props.title === '添加车辆') {
      if (fieldsValue.utmsUsersDriversId) {
        this.setState({
          btnVisible: true,
          utmsUsersDriversId: fieldsValue.utmsUsersDriversId,
        });
      } else {
        this.setState({ btnVisible: true });
      }
    } else {
      await bindingDriver({
        utmsVehicleId: this.props.utmsVehicleId,
        utmsUsersDriversId: fieldsValue.utmsUsersDriversId,
      }).then(data => {
        if (data.result === 'success') {
          this.props.handleSubmit();
          this.setState({ btnVisible: true });
        }
      });
    }
  };

  btn = () => {
    if (!this.state.btnVisible) {
      return (
        <span>
          <a
            href="javascript:;"
            className="ml10"
            onClick={() => {
              if (
                !parseInt(
                  this.props.form.getFieldsValue().utmsUsersDriversId,
                  10,
                )
              ) {
                message.error('请选择绑定司机');
                return;
              }
              this.seva();
            }}
          >
            保存
          </a>
          <a
            href="javascript:;"
            className="ml10"
            onClick={value => {
              this.setState({ btnVisible: true });
              if (this.props.driverData.utmsUsersDriversId) {
                this.props.form.setFieldsValue({
                  utmsUsersDriversId: this.props.driverData.utmsUsersDriversId,
                });
              } else {
                this.props.form.setFieldsValue({
                  utmsUsersDriversId: '',
                });
              }
            }}
          >
            取消
          </a>
        </span>
      );
    } else {
      return (
        <span>
          <a
            href="javascript:;"
            className="ml10"
            onClick={value => {
              this.setState({ btnVisible: false });
            }}
          >
            修改
          </a>
          {parseInt(this.props.form.getFieldsValue().utmsUsersDriversId, 10) ? (
            <a
              href="javascript:;"
              className="ml10"
              onClick={async value => {
                const form = this.props.form;
                if (!parseInt(form.getFieldValue('utmsUsersDriversId'), 10)) {
                  return;
                }
                await setDeleteDriver({
                  utmsVehicleId: this.props.utmsVehicleId,
                  utmsUsersDriversId: form.getFieldsValue().utmsUsersDriversId,
                }).then(data => {
                  if (data.result === 'success') {
                    form.setFieldsValue({
                      utmsUsersDriversId: '',
                    });
                    this.getDriverData();
                    this.props.handleSubmit();
                  }
                });
              }}
            >
              删除
            </a>
          ) : (
            ''
          )}
        </span>
      );
    }
  };

  render() {
    const { visible, form, title, confirmLoading, utmsVehicleId } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        span: 7,
      },
      wrapperCol: {
        span: 15,
      },
    };

    return (
      <Modal
        centered={true}
        maskClosable={false}
        bodyStyle={{ height: 'calc(55vh)', overflow: 'auto' }}
        title={title}
        visible={visible}
        onOk={() => {
          this.props.onCreate();
        }}
        confirmLoading={confirmLoading}
        onCancel={() => {
          this.reset();
          {
            /*form.resetFields();
          this.setState(
            { btnVisible: true, identificationCodeStatus: 1 },
            () => {
              form.setFieldsValue({ carModelCarLong: '' });
              onCancel();
            },
          );*/
          }
        }}
      >
        <Form className="ant-advanced-search-form">
          <Row>
            <Col span={24}>
              <FormItem {...formItemLayout} label="车辆归属">
                {getFieldDecorator('vehicleAttributive', {
                  initialValue: '1',
                })(
                  <Select
                    disabled={parseInt(utmsVehicleId, 10) ? true : false}
                    onChange={value => {
                      this.setState({
                        identificationCodeStatus: parseInt(value, 10),
                      });
                    }}
                  >
                    <Option value="1">公司自有车辆</Option>
                    <Option value="2">社会临时车辆</Option>
                  </Select>,
                )}
              </FormItem>
            </Col>
            {(this.state.identificationCodeStatus === 1 &&
              title === '添加车辆') ||
            title === '编辑车辆' ? (
              <Col span={24}>
                <FormItem {...formItemLayout} label="车辆识别代码">
                  {getFieldDecorator('identificationCode', {
                    rules: [{ required: true, message: '请输入车辆识别代码!' }],
                  })(
                    <Input
                      disabled={title === '编辑车辆'}
                      maxLength={20}
                      onKeyDown={e => {
                        if (e.keyCode === 13) {
                          onCreate();
                        }
                      }}
                    />,
                  )}
                </FormItem>
              </Col>
            ) : (
              ''
            )}
            <Col span={24}>
              <FormItem {...formItemLayout} label="车牌号码">
                {getFieldDecorator('plateNumber', {
                  rules: [{ required: true, message: '请输入车牌号码!' }],
                })(
                  <Input
                    maxLength={10}
                    onKeyDown={e => {
                      if (e.keyCode === 13) {
                        onCreate();
                      }
                    }}
                  />,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem {...formItemLayout} label="发动机号">
                {getFieldDecorator('engineNumber')(
                  <Input
                    maxLength={20}
                    onKeyDown={e => {
                      if (e.keyCode === 13) {
                        onCreate();
                      }
                    }}
                  />,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem {...formItemLayout} label="品牌型号">
                {getFieldDecorator('brandModel')(
                  <Input
                    maxLength={20}
                    onKeyDown={e => {
                      if (e.keyCode === 13) {
                        onCreate();
                      }
                    }}
                  />,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem {...formItemLayout} label="能源类型">
                {getFieldDecorator('energyType')(
                  <Select>
                    <Option value="1">柴油</Option>
                    <Option value="2">汽油</Option>
                    <Option value="3">新能源</Option>
                  </Select>,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem {...formItemLayout} label="车型车长">
                {getFieldDecorator('carModelCarLong')(
                  <CarModelCarLong maxSelect={1} noHot={true} />,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem {...formItemLayout} label="核定载质量（千克）">
                {getFieldDecorator('approvedLoad')(
                  <Input
                    maxLength={6}
                    onKeyDown={e => {
                      if (e.keyCode === 13) {
                        onCreate();
                      }
                    }}
                  />,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem {...formItemLayout} label="整车长宽高（毫米）">
                <Row>
                  <Col md={8}>
                    <FormItem>
                      {getFieldDecorator('vehicleRealLong')(
                        <Input
                          style={{ width: 85 }}
                          className="mr5"
                          maxLength={5}
                          prefix={
                            <span style={{ color: 'rgba(0,0,0,.25)' }}>长</span>
                          }
                          onKeyDown={e => {
                            if (e.keyCode === 13) {
                              onCreate();
                            }
                          }}
                        />,
                      )}
                    </FormItem>
                  </Col>
                  <Col md={8}>
                    <FormItem>
                      {getFieldDecorator('vehicleRealWidth')(
                        <Input
                          style={{ width: 85 }}
                          className="mr5 ml5"
                          maxLength={5}
                          prefix={
                            <span style={{ color: 'rgba(0,0,0,.25)' }}>宽</span>
                          }
                          onKeyDown={e => {
                            if (e.keyCode === 13) {
                              onCreate();
                            }
                          }}
                        />,
                      )}
                    </FormItem>
                  </Col>
                  <Col md={8}>
                    <FormItem>
                      {getFieldDecorator('vehicleRealHeight')(
                        <Input
                          style={{ width: 89 }}
                          className="ml5"
                          maxLength={5}
                          prefix={
                            <span style={{ color: 'rgba(0,0,0,.25)' }}>高</span>
                          }
                          onKeyDown={e => {
                            if (e.keyCode === 13) {
                              onCreate();
                            }
                          }}
                        />,
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </FormItem>
            </Col>

            <Col span={24}>
              <FormItem {...formItemLayout} label="车牌颜色">
                {getFieldDecorator('plateColor')(
                  <Select>
                    <Option value="蓝色">蓝色</Option>
                    <Option value="黄色">黄色</Option>
                    <Option value="绿色">绿色</Option>
                  </Select>,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem {...formItemLayout} label="绑定司机">
                {getFieldDecorator('utmsUsersDriversId')(
                  <Select
                    showSearch
                    optionFilterProp="children"
                    disabled={
                      title === '编辑车辆' ? this.state.btnVisible : false
                    }
                  >
                    {(this.state.DriverData || []).map(item => {
                      if (item.utmsUsersDriversId) {
                        return (
                          <Option
                            key={item.utmsUsersDriversId}
                            value={item.utmsUsersDriversId}
                          >
                            {item.driverName} - {item.phoneNumber}
                          </Option>
                        );
                      }
                    })}
                  </Select>,
                )}
                {title === '编辑车辆' ? this.btn() : ''}
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}
