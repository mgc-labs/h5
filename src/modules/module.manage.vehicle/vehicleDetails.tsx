import { Button, Col, Form, Modal, Row } from 'antd';
import React, { PureComponent } from 'react';
import GLOBAL from 'utils/GLOBAL.ts';

const FormItem = Form.Item;

interface IDetailsModuleProps {
  visible: boolean;
  detailData: any;
  onCreate: () => void;
  onCancel: () => void;
}

class DetailsModule extends PureComponent<IDetailsModuleProps> {
  render() {
    const { visible, detailData, onCreate, onCancel } = this.props;
    const formItemLayout = {
      labelCol: {
        span: 7,
      },
      wrapperCol: {
        span: 15,
      },
    };
    return (
      <Modal
        bodyStyle={{ height: 'calc(55vh)', overflow: 'auto' }}
        title="车辆详情"
        visible={visible}
        onOk={onCreate}
        onCancel={onCancel}
      >
        <Row>
          <Col span={24}>
            <FormItem {...formItemLayout} label="车辆归属">
              {detailData.vehicleAttributive === 1 ? '自有车辆' : ''}
              {detailData.vehicleAttributive === 2 ? '社会临时车辆' : ''}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="车辆识别代码">
              {detailData.identificationCode || GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="车牌号码">
              {detailData.plateNumber || GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="发动机号">
              {detailData.engineNumber || GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="品牌型号">
              {detailData.brandModel || GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="能源类型">
              {detailData.energyType
                ? ['无', '柴油', '汽油', '新能源'][detailData.energyType]
                : GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="车型车长">
              {detailData.vehicleLong
                ? detailData.vehicleLong / 1000 + '米' + detailData.vehicleType
                : GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="核定载质量">
              {detailData.approvedLoad
                ? detailData.approvedLoad + ' 千克'
                : GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="整车长宽高(毫米)">
              {detailData.vehicleRealLong
                ? detailData.vehicleRealLong +
                  ' x ' +
                  detailData.vehicleRealWidth +
                  ' x ' +
                  detailData.vehicleRealHeight
                : GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="车牌颜色">
              {detailData.plateColor || GLOBAL.emptyRecord}
            </FormItem>
          </Col>
          <Col span={24}>
            <FormItem {...formItemLayout} label="绑定司机">
              {detailData.driverName
                ? detailData.driverName + ' -- ' + detailData.phoneNumber
                : GLOBAL.emptyRecord}
            </FormItem>
          </Col>
        </Row>
      </Modal>
    );
  }
}

export default DetailsModule;
