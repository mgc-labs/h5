import Checkbox from 'antd/es/checkbox';
import * as React from 'react';
import shallowequal from 'shallowequal';

import { CustomPoint } from '../../ctrls/customPoint';
import { InfoWindow } from '../../ctrls/infoWindow';

import Spin from 'antd/es/spin';
import { loading } from 'components/ImageZoom/components/Image/index.module.less';
import { setTimeout } from 'timers';
import styles from './index.module.less';

declare const BMap: any;
declare const BMapLib: any;
export interface IAddressMapProps {
  data: any;
}
interface IState {
  data: any;
  depot: any;
  store: any;
  checkedList: any;
  loading: boolean;
}
export class AddressMap extends React.PureComponent<IAddressMapProps> {
  state: IState;
  private divMap: HTMLDivElement;
  private map: any;
  private infoWindow: InfoWindow;
  constructor(props: any) {
    super(props);
    this.state = {
      data: [] || props.data,
      checkedList: ['仓库', '网点/门店'],
      depot: [], // 仓库
      store: [], // 网点
      loading: false,
    };
  }
  componentDidMount() {
    this.initMap();
  }
  componentWillReceiveProps(nextProps) {
    if (!shallowequal(this.state.data, nextProps.data)) {
      this.setState(
        {
          data: nextProps.data || {},
        },
        () => {
          this.initMarkes();
        },
      );
    }
  }
  removeMapElement = (element: Element) => {
    if (element) {
      element.parentElement.removeChild(element);
    }
  };
  onShowInfo = data => {
    if (!this.infoWindow) {
      this.infoWindow = new InfoWindow();
      this.infoWindow.setPosition(
        new BMap.Point(data.longitude, data.latitude),
      );
      this.map.addOverlay(this.infoWindow);
    } else {
      this.infoWindow.setPosition(
        new BMap.Point(data.longitude, data.latitude),
      );
    }
    this.infoWindow.setData(data);
    this.infoWindow.show();
  };
  onHideInfo = () => {
    this.infoWindow.hide();
  };
  initMarkes() {
    this.setState({ loading: true });

    if (this.state.data.length === 0) {
      return;
    }
    const depot = []; // 仓库
    const store = []; // 网点
    const mks = [];
    this.state.data.forEach((d, i) => {
      const cp = new CustomPoint({
        data: d,
        onClick: this.onShowInfo,
        onShowInfo: this.onShowInfo,
        onHideInfo: this.onHideInfo,
      });
      if (d.depotType === '仓库') {
        depot.push(cp);
      } else if (d.depotType === '网点/门店') {
        store.push(cp);
      }
      this.map.addOverlay(cp);

      const pt = new BMap.Point(d.longitude, d.latitude);
      mks.push(pt);
      if (i === this.state.data.length - 1) {
        this.setState({ loading: false });
      }
    });
    this.setState({
      depot,
      store,
    });
    setTimeout(() => {
      this.map.setViewport(mks);
    }, 100);
  }
  initMap() {
    // 创建Map实例
    this.map = new BMap.Map(this.divMap, {
      minZoom: 1,
      maxZoom: 19,
      enableMapClick: false,
    });

    this.map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
    // 去掉地图左下解的LOGO与文字
    this.map.addEventListener('tilesloaded', () => {
      this.removeMapElement(document.querySelector('.anchorBL'));
      this.removeMapElement(document.querySelector('.BMap_cpyCtrl'));
    });

    this.map.centerAndZoom('杭州', 12);
  }
  handleChange = checkedList => {
    this.setState({
      checkedList,
    });
    this.state.depot.map(d => {
      d.hide();
    });
    this.state.store.map(d => {
      d.hide();
    });
    checkedList.map(d => {
      if (d === '仓库') {
        this.state.depot.map(item => {
          item.show();
        });
      }
      if (d === '网点/门店') {
        this.state.store.map(item => {
          item.show();
        });
      }
    });
  };
  render() {
    return (
      <div className={styles.AddressMap}>
        <div className={styles.AddressMap__Checkbox}>
          <Checkbox.Group
            value={this.state.checkedList}
            onChange={this.handleChange}
          >
            <Checkbox value="仓库">仓库</Checkbox>
            <Checkbox value="网点/门店">网点</Checkbox>
          </Checkbox.Group>
        </div>
        <Spin spinning={this.state.loading}>
          <div
            className={styles.AddressMap__Box}
            ref={node => (this.divMap = node)}
          />
        </Spin>
      </div>
    );
  }
}
