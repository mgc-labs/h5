import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { fromJS, List, Map } from 'immutable';
import { call, put, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { getData, reqDelData, reqModifyData } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      mapData: List(), // 地图模式
      mapLoading: false, // 地图模式
      data: List(),
      dataCount: 0,
      error: false,
      loading: false,
      setDefaultOk: false,
      delectOk: false,
    }),
    actions: {
      GET_ASYNC_DATA: params => params,
      GET_ASYNC_DATA_ERROR: error => error,
      GET_ASYNC_DATA_SUCCESS: data => data,
      ADD_ADDRESS_DATA: data => data,
      EDIT_ADDRESS_DATA: data => data,
      DEL_ADDRESS_DATA: params => params,
      DEL_ADDRESS_DATA_ERROR: error => error,
      DEL_ADDRESS_DATA_SUCCESS: data => data,
      SET_DEFAULT: param => param,
      SET_DEFAULT_ERROR: error => error,
      SET_DEFAULT_SUCCESS: data => data,
      GET_MAP_DATA: params => params,
      GET_MAP_DATA_ERROR: error => error,
      GET_MAP_DATA_SUCCESS: data => data,
    },
    reducers: {
      GET_ASYNC_DATA(state, action) {
        return state
          .set('error', false)
          .set('loading', true)
          .set('setDefaultOk', false)
          .set('delectOk', false);
      },
      GET_ASYNC_DATA_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_ASYNC_DATA_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('dataCount', action.payload.count)
          .set('data', fromJS(action.payload.data));
      },
      DEL_ADDRESS_DATA(state, action) {
        return state.set('error', false).set('loading', true);
      },
      DEL_ADDRESS_DATA_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      DEL_ADDRESS_DATA_SUCCESS(state, action) {
        return state.set('loading', false).set('delectOk', true);
      },
      SET_DEFAULT(state, action) {
        return state.set('error', false).set('loading', true);
      },
      SET_DEFAULT_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      SET_DEFAULT_SUCCESS(state, action) {
        return state.set('loading', false).set('setDefaultOk', true);
      },
      GET_MAP_DATA(state, action) {
        return state.set('mapLoading', true);
      },
      GET_MAP_DATA_ERROR(state, action) {
        return state.set('mapLoading', false);
      },
      GET_MAP_DATA_SUCCESS(state, action) {
        return state
          .set('mapLoading', false)
          .set('mapData', fromJS(action.payload.data));
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const data = yield call(getData, action.payload);
          yield put(actions.getAsyncDataSuccess(data));
        } catch (err) {
          yield put(actions.getAsyncDataError(err));
        }
      }

      function* getMapData(action) {
        try {
          const data = yield call(getData, action.payload);
          yield put(actions.getMapDataSuccess(data));
        } catch (err) {
          yield put(actions.getMapDataError(err));
        }
      }

      function* delData(action) {
        const { utmsDepotId, ...formData } = action.payload;
        try {
          const data = yield call(reqDelData, { utmsDepotId });

          message.success(data.msg);

          yield put(actions.delAddressDataSuccess(data));
          yield put(actions.getAsyncData({ ...formData }));
        } catch (error) {
          yield put(actions.delAddressDataError(error));
        }
      }

      function* addData(action) {
        try {
          yield put(push(`/company/addAddressManage`));
        } catch (error) {
          message.error(error.toString());
        }
      }

      function* editData(action) {
        try {
          yield put(
            push(`/company/editAddressManage?utmsDepotId=${action.payload}`),
          );
        } catch (error) {
          message.error(error.toString());
        }
      }

      function* setDefault(action) {
        const { utmsDepotId, ...fetchDataParams } = action.payload;
        try {
          const data = yield call(reqModifyData, {
            utmsDepotId,
            isDefault: 1,
          });

          message.success(data.msg);

          yield put(actions.setDefaultSuccess());

          yield put(actions.getAsyncData(fetchDataParams));
        } catch (error) {
          yield put(actions.setDefaultError(error));
        }
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ASYNC_DATA, fetchData);
        yield takeLatest(CONSTANTS.DEL_ADDRESS_DATA, delData);
        yield takeLatest(CONSTANTS.ADD_ADDRESS_DATA, addData);
        yield takeLatest(CONSTANTS.EDIT_ADDRESS_DATA, editData);
        yield takeLatest(CONSTANTS.SET_DEFAULT, setDefault);
        yield takeLatest(CONSTANTS.GET_MAP_DATA, getMapData);
      };
    },
  });
