/**
 * fixpath.list Service
 * @author ryan bian
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const PATHLIST_RESOURCE =
  '/ehuodiGateway/utmsCore/utmsDepotcs/selectDepotList?';
const DELETE_ADDRESS = '/ehuodiGateway/utmsCore/utmsDepotcs/deleteDepot';
const MODIFY_DEPOT = '/ehuodiGateway/utmsCore/utmsDepotcs/modifyDepot';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', PATHLIST_RESOURCE)
    .reply(() => import('./mock/mock').then(exports => [200, exports.default]));

  mocker
    .on('post', DELETE_ADDRESS)
    .reply(() =>
      import('./mock/delete').then(exports => [200, exports.default]),
    );

  // mocker
  //   .on('post', MODIFY_DEPOT)
  //   .reply(() =>
  //     import('./mock/modifyDepot').then(exports => [200, exports.default]),
  //   );
}

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getData = data => {
  return request(
    {
      method: 'post',
      url: PATHLIST_RESOURCE,
      data: qs.stringify(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

/**
 * 删除
 * @param data
 */
export const reqDelData = data => {
  return request(
    {
      method: 'post',
      url: DELETE_ADDRESS,
      data: qs.stringify(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

/**
 * 修改
 * @param data
 */
export const reqModifyData = data => {
  return request(
    {
      method: 'post',
      url: MODIFY_DEPOT,
      data,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
