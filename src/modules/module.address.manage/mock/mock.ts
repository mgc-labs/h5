export default {
  result: 'success',
  count: 2,
  data: [
    {
      utmsDepotId: 101419,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      depotType: 'Z00001_K001', // 客户编码
      depotName: '美团', // 客户名称
      contact: '王经理', // 联系人
      contactWay: '18109281203', // 联系方式
      province: '浙江省',
      city: '杭州市',
      region: '西湖区',
      address: '浙江省杭州市西湖区古墩路366号',
      longitude: 120.02823,
      latitude: 30.02823,
      isDefault: '1',
    },
    {
      utmsDepotId: 101418,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      depotType: 'Z00001_K002', // 客户编码
      depotName: '美团', // 客户名称
      contact: '王经理', // 联系人
      contactWay: '18109281203', // 联系方式
      province: '',
      city: '',
      region: '',
      address: '',
      longitude: 120.02823,
      latitude: 30.02823,
      isDefault: '0',
    },
  ],
  code: '',
  msg: '地址列表查询成功 ',
};
