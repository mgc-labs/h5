// import {
//   ColorUnGroupdSelectedPoint,
//   ColorUnGroupdUnSelectedPoint,
// } from '../const';

const pointWidth = 22;
const pointHeight = 30;

// const pointInWidth = 60;
// const pointInHeight = pointInWidth;

declare const BMap: any;

export class CustomPoint extends BMap.Overlay {
  public data: any;
  constructor({ data, onClick, onShowInfo, onHideInfo }) {
    super();
    this.data = data;
    this.point = new BMap.Point(data.longitude, data.latitude);
    this.onClick = onClick;
    this.onShowInfo = onShowInfo;
    this.onHideInfo = onHideInfo;
  }

  initialize(map) {
    this.map = map;
    const div = document.createElement('div');
    div.style.position = 'absolute';
    div.style.width = '22px';
    div.style.height = '30px';
    div.style.cursor = 'pointer';
    div.style.zIndex = '1';
    if (this.data.depotType === '仓库') {
      div.innerHTML =
        "<img src='http://image.tf56.com/dfs/group1/M00/6C/D8/CiFBClvWvK2AavVpAAAFmv6mZVg018.png' width='22' height='30'>";
    } else if (this.data.depotType === '网点/门店') {
      div.innerHTML =
        "<img src='http://image.tf56.com/dfs/group1/M00/6C/D5/CiFBCVvWvKyACWhzAAAFg7bh-nQ272.png' width='22' height='30'>";
    }

    div.onclick = e => {
      if (this.onClick) {
        this.onClick(this);
      }
    };

    div.onmouseenter = () => {
      if (this.onShowInfo) {
        this.onShowInfo(this.data);
      }
    };

    div.onmouseout = () => {
      if (this.onHideInfo) {
        this.onHideInfo(this.data);
      }
    };

    this.map.getPanes().labelPane.appendChild(div);
    this.div = div;

    return div;
  }

  draw() {
    const pixel = this.map.pointToOverlayPixel(this.point);
    this.div.style.left = pixel.x - pointWidth / 2 + 'px';
    this.div.style.top = pixel.y - pointHeight / 2 + 'px';
  }

  destroy() {
    // this.map.getPanes().labelPane.remove(this.div);
    this.div.onclick = null;
    this.data = null;
    this.point = null;
    this.onClick = null;
    this.onShowInfo = null;
    this.onHideInfo = null;

    this.map = null;
  }
}
