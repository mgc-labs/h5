/**
 * AddressManage Component
 * @author hefan
 * @date 2018/9/17 上午9:10:13
 */
import { Map } from 'immutable';
import * as React from 'react';
import { createStructuredSelector } from 'reselect';

import {
  makeSelectData,
  makeSelectDataCount,
  makeSelectError,
  makeSelectLoading,
  makeSelectMapData,
} from './selectors';

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Divider from 'antd/es/divider';
import Form from 'antd/es/form';
import FormItem from 'antd/es/form/FormItem';
import Input from 'antd/es/input';
import Popconfirm from 'antd/es/popconfirm';
import Select from 'antd/es/select';
import Spin from 'antd/es/spin';
import Tabs from 'antd/es/tabs';
import MyTable from 'components/MyTable';
import { OptionButtons } from 'components/OptionButtons';

const Option = Select.Option;

// import styles from './index.module.less';
import CustomerSelect from 'components/CustomerSelect';
import { curr } from 'modules/module.ios/components/map/ctrls/customInfoWindow.module.less';
import { AddressMap } from './components/map';
import styles from './index.module.less';

const depotType = {
  site: '网点/门店',
  store: '仓库',
};

export interface IAddressManageProps {
  children: React.ReactChildren;
  getAsyncData: (params: {}) => any;
  delAddressData: (params: {}) => any;
  addAddressData: () => any;
  editAddressData: (id?: string) => any;
  setDefault: (id?: string) => any;
  getMapData: (params: {}) => any;
  data: any;
  mapData: any;
  dataCount: number;
  loading: boolean;
}

class AddressManage extends React.PureComponent<IAddressManageProps> {
  state = {
    skipCount: 0,
    pageSize: 15,
    current: 1,
    activeKey: '1',
  };
  public componentDidMount() {
    this.fetchDataFun();
  }
  renderMap = () => {
    const { mapData } = this.props;

    const data = mapData
      ? mapData
          .map(d => ({
            key: d.get('utmsDepotId'),
            depotName: d.get('depotName'),
            depotType: d.get('depotType'),
            province: d.get('province'),
            city: d.get('city'),
            region: d.get('region'),
            address: d.get('address'),
            contact: d.get('contact'),
            contactWay: d.get('contactWay'),
            longitude: d.get('longitude'),
            latitude: d.get('latitude'),
            isDefault: d.get('isDefault'),
          }))
          .toArray()
      : [];
    return <AddressMap data={data} />;
  };
  public render() {
    const operations = (
      <div className="btn">
        <Button type="primary" onClick={this.props.addAddressData}>
          新增地址
        </Button>
      </div>
    );
    return (
      <Spin spinning={this.props.loading}>
        {/* <div className={styles.AddressManageTitle}>
          <div className={styles.AddressManageTitleTxt}>地址管理</div>
          <div className="btn">
            <Button type="primary" onClick={this.props.addAddressData}>
              新增地址
            </Button>
          </div>
        </div> */}
        <Card>
          <Tabs
            tabBarExtraContent={operations}
            onChange={this.handleTabsChange}
          >
            <Tabs.TabPane
              tab={'客户地址列表'}
              key="1"
              style={{ height: '100%' }}
            >
              {this.getFields()}
              {this.getTable()}
            </Tabs.TabPane>
            <Tabs.TabPane tab={'地图模式'} key="2" style={{ height: '100%' }}>
              {this.renderMap()}
            </Tabs.TabPane>
          </Tabs>
        </Card>
      </Spin>
    );
  }
  handleTabsChange = activeKey => {
    const { getMapData } = this.props;
    if (activeKey === '2') {
      if (this.state.activeKey === '1') {
        getMapData({
          coordinateType: 'BD09',
          pageSize: 1000000,
          skipCount: 0,
        });
      }
      this.setState({
        activeKey,
      });
    }
  };
  private getTable() {
    const columns = [
      {
        key: 'mark',
        render: record =>
          record.isDefault === 1 ? (
            <div style={{ whiteSpace: 'nowrap' }}>默认仓库</div>
          ) : null,
      },
      {
        title: '客户名称',
        dataIndex: 'customerName',
        key: 'customerName',
      },
      {
        title: '地址名称',
        dataIndex: 'depotName',
        key: 'depotName',
      },
      {
        title: '类型',
        dataIndex: 'depotType',
        key: 'depotType',
        // render: text => depotType[text],
      },
      {
        title: '省市区',
        key: 'province',
        render: record => `${record.province}${record.city}${record.region}`,
      },
      {
        title: '详细地址',
        dataIndex: 'address',
      },
      {
        title: '操作',
        key: 'action',
        width: 105,
        render: (text, record) => (
          <span>
            <OptionButtons>
              <a
                href="javascript:;"
                onClick={() => this.props.editAddressData(record.utmsDepotId)}
              >
                编辑
              </a>
              {/* <Divider type="vertical" /> */}
              <Popconfirm
                title="是否确认删除？"
                onConfirm={() => {
                  this.delAddressHandle(record.utmsDepotId);
                }}
              >
                <a href="javascript:;">删除</a>
              </Popconfirm>
              {record.isDefault === 1 ? null : (
                // <Divider type="vertical" />
                <a
                  href="javascript:;"
                  onClick={() => {
                    this.setDefaultHanlde(record.utmsDepotId);
                  }}
                >
                  设为默认仓库
                </a>
              )}
            </OptionButtons>
          </span>
        ),
      },
    ];
    const { data, dataCount } = this.props;

    const { current } = this.state;
    const paginationConfig = {
      current,
      total: dataCount,
      onChange: this.paginationChange,
      onShowSizeChange: this.paginationChange,
    };

    const dataSource = data
      ? data
          .map(d => ({
            key: d.get('utmsDepotId'),
            utmsDepotId: d.get('utmsDepotId'),
            customerName: d.get('customerName') || '--',
            depotName: d.get('depotName'),
            depotType: d.get('depotType'),
            province: d.get('province'),
            city: d.get('city'),
            region: d.get('region'),
            address: d.get('address'),
            contact: d.get('contact'),
            contactWay: d.get('contactWay'),
            longitude: d.get('longitude'),
            latitude: d.get('latitude'),
            isDefault: d.get('isDefault'),
          }))
          .toArray()
      : [];
    return (
      <MyTable
        columns={columns}
        dataSource={dataSource}
        pagination={paginationConfig}
      />
    );
  }
  private getFields() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form layout="inline" style={{ marginBottom: '20px' }}>
        <FormItem label="客户名称">
          {getFieldDecorator('customerCode', {
            initialValue: '',
          })(
            <CustomerSelect
              style={{ minWidth: 100 }}
              showDefaultValue={false}
            />,
          )}
        </FormItem>
        <FormItem label="类型">
          {getFieldDecorator('depotType', {
            initialValue: '',
          })(
            <Select style={{ width: '90px' }}>
              <Option value="">全部</Option>
              <Option value="store">仓库</Option>
              <Option value="site">网点/门店</Option>
            </Select>,
          )}
        </FormItem>

        <FormItem>
          {getFieldDecorator('keyword')(<Input placeholder="地址名称" />)}
        </FormItem>

        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            onClick={() => {
              this.onSearchHandle();
            }}
          >
            查询
          </Button>
        </FormItem>

        <FormItem>
          <Button onClick={this.handleReset}>重置</Button>
        </FormItem>
      </Form>
    );
  }

  private fetchDataFun(params?: {}) {
    const formData = this.props.form.getFieldsValue();
    const { pageSize } = this.state;

    this.props.getAsyncData({
      pageSize,
      skipCount: 0,
      ...formData,
      ...params,
    });
  }

  private onSearchHandle = () => {
    this.setState({
      skipCount: 0,
      current: 1,
    });

    this.fetchDataFun();
  };

  private handleReset = () => {
    this.props.form.resetFields();

    this.onSearchHandle();
  };

  /**
   * 页码变化
   */
  private paginationChange = params => {
    const { skipCount, pageSize, current } = params;
    this.setState({
      skipCount,
      pageSize,
      current,
    });
    this.fetchDataFun({
      skipCount,
      pageSize,
    });
  };

  /**
   * 设置默认仓库
   */
  private setDefaultHanlde = utmsDepotId => {
    const { pageSize, skipCount } = this.state;
    const formData = this.props.form.getFieldsValue();

    this.props.setDefault({
      utmsDepotId,
      skipCount,
      pageSize,
      ...formData,
    });
  };

  /**
   * 删除
   */
  private delAddressHandle = utmsDepotId => {
    const { pageSize, skipCount } = this.state;
    const formData = this.props.form.getFieldsValue();

    this.props.delAddressData({
      utmsDepotId,
      skipCount,
      pageSize,
      ...formData,
    });
  };
}

const AddressManageForm = Form.create()(AddressManage);

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId, Map());

    return {
      mapStateToProps: createStructuredSelector({
        data: makeSelectData(currentState),
        dataCount: makeSelectDataCount(currentState),
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        mapData: makeSelectMapData(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getAsyncData: params => dispatch(actions.getAsyncData(params)),
        delAddressData: params => dispatch(actions.delAddressData(params)),
        addAddressData: () => dispatch(actions.addAddressData()),
        editAddressData: id => dispatch(actions.editAddressData(id)),
        setDefault: id => dispatch(actions.setDefault(id)),
        getMapData: params => dispatch(actions.getMapData(params)),
      }),
    };
  })(AddressManageForm);
};
