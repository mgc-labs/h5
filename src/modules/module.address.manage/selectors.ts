/**
 * AddressManage selectors
 * @author hefan
 * @date 2018/9/17 上午9:10:13
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('dataCount'),
  );

export const makeSelectMapData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('mapData'),
  );
