/**
 * VehicleOrder Reducers
 * @author lhf
 * @date 2018-9-11 16:31:42
 */
import { fromJS, List, Map } from 'immutable';

// The initial state of the App
const initialState = Map({
  data: List(),
  error: false,
  loading: false,
  dataCount: 0,
  filter: Map(),
  pageIndex: 0,
  statistics: Map(), // 统计数据
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_ORDER_LIST_DATA:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_ORDER_LIST_DATA_SUCCESS:
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data || []))
          .set('dataCount', action.payload.count)
          .set('pageIndex', action.meta);
      case CONSANTS.GET_ORDER_LIST_DATA_ERROR:
        return state.set('error', action.error).set('loading', false);
      case CONSANTS.UPDATE_FILTER:
        return state.set('filter', Map(action.payload)).set('pageIndex', 0);
      case CONSANTS.SEND_ORDER:
        return state.set('error', false).set('loading', true);
      case CONSANTS.DELE_ORDER:
        return state.set('error', false).set('loading', true);
      case CONSANTS.OPERA_DONE:
        return state.set('error', action.error).set('loading', false);
      case CONSANTS.GET_STATISTICS:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_STATISTICS_SUCCESS:
        return state
          .set('error', false)
          .set('loading', false)
          .set('statistics', fromJS(action.payload));
      case CONSANTS.GET_STATISTICS_ERROR:
        return state.set('error', action.error).set('loading', false);
      default:
        return state;
    }
  };
};
