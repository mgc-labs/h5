/**
 * vehicle.order Service
 * @author lhf
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// 订单列表
const API_ORDERLIST =
  '/ehuodiGateway/utmsTrade/orderWholeCar/selectByCondition';
const API_SEND = '/ehuodiGateway/utmsTrade/orderWholeCar/deliver';
const API_DELE = '/ehuodiGateway/utmsTrade/orderWholeCar/delete';
const API_TOTAL = '/ehuodiGateway/utmsTrade/orderWholeCar/orderStatistics';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_ORDERLIST)
    .reply(() =>
      import('./mock/orderList').then(exports => [200, exports.default]),
    );
}

// 订单列表
export const getOrderList = data =>
  request(
    {
      method: 'post',
      url: API_ORDERLIST,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );

// 发货
export const sendGood = data =>
  request(
    {
      method: 'post',
      url: API_SEND,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

// 删除
export const deleOrder = data =>
  request(
    {
      method: 'post',
      url: API_DELE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

// 查询统计数据
export const getStatistics = data =>
  request(
    {
      method: 'post',
      url: API_TOTAL,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);
