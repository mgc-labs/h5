/**
 * VehicleOrder Saga
 * @author lhf
 * @date 2018-9-11 16:31:42
 */
import message from 'antd/es/message';
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import { deleOrder, getOrderList, getStatistics, sendGood } from './service';

export default (CONSTANTS, actions, routeId) => {
  function* fetchOrderList(action) {
    const { payload, meta } = action;
    try {
      const filterData = yield select(state =>
        state.getIn([routeId, 'filter']),
      );
      const listData = yield call(
        getOrderList,
        Object.assign(
          filterData.toJS(),
          {
            skipCount: 0,
            pageSize: 15,
          },
          payload,
        ),
      );

      yield put(actions.getOrderListDataDone(listData, meta));
    } catch (err) {
      yield put(actions.getOrderListDataError(err));
    }
  }

  // 发货
  function* sendGoods(action) {
    const { orderWholeCarId, pageIndex, ...formData } = action.payload;
    try {
      const data = yield call(sendGood, {
        orderWholeCarId,
      });
      if (data.code) {
        message.error(data.msg);
      } else {
        message.success(data.msg);
        yield put(actions.getOrderListData(formData, pageIndex));
      }
      yield put(actions.operaDone(data));
    } catch (err) {
      yield put(actions.operaDone(err));
    }
  }

  // 删除订单
  function* deleteOrder(action) {
    const { orderWholeCarId, pageIndex, ...formData } = action.payload;
    try {
      const data = yield call(deleOrder, {
        orderWholeCarId,
      });
      if (data.code) {
        message.error(data.msg);
      } else {
        message.success(data.msg);
        yield put(actions.getOrderListData(formData, pageIndex));
        yield put(actions.getStatistics());
      }
      yield put(actions.operaDone(data));
    } catch (err) {
      yield put(actions.operaDone(err));
    }
  }

  // 统计数据
  function* getCount(action) {
    try {
      const data = yield call(getStatistics, action.payload);
      yield put(actions.getStatisticsDone(data));
    } catch (err) {
      yield put(actions.getStatisticsError(err));
    }
  }

  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_ORDER_LIST_DATA, fetchOrderList);
    yield takeEvery(CONSTANTS.UPDATE_FILTER, fetchOrderList);
    yield takeLatest(CONSTANTS.SEND_GOOD, sendGoods);
    yield takeLatest(CONSTANTS.DELE_ORDER, deleteOrder);
    yield takeLatest(CONSTANTS.GET_STATISTICS, getCount);
  };
};
