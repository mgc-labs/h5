/**
 * VehicleOrder Actions
 * @author lhf
 * @date 2018-9-11 16:31:42
 */
import { AnyAction } from 'redux';
import {
  DELE_ORDER,
  GET_ORDER_LIST_DATA,
  GET_ORDER_LIST_DATA_ERROR,
  GET_ORDER_LIST_DATA_SUCCESS,
  GET_STATISTICS,
  OPERA_DONE,
  SEND_GOOD,
  UPDATE_FILTER,
} from './constants';

export default CONSANTS => ({
  getOrderListData: (options, pageIndex): AnyAction => ({
    payload: options,
    meta: pageIndex,
    type: CONSANTS.GET_ORDER_LIST_DATA,
  }),

  getOrderListDataDone: (data, pageIndex): AnyAction => ({
    payload: data,
    meta: pageIndex,
    type: CONSANTS.GET_ORDER_LIST_DATA_SUCCESS,
  }),

  getOrderListDataError: (error): AnyAction => ({
    error,
    type: CONSANTS.GET_ORDER_LIST_DATA_ERROR,
  }),

  updateFilter: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.UPDATE_FILTER,
  }),

  sendGood: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.SEND_GOOD,
  }),

  deleOrder: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.DELE_ORDER,
  }),

  operaDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.OPERA_DONE,
  }),

  getStatistics: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_STATISTICS,
  }),

  getStatisticsDone: (data, pageIndex): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_STATISTICS_SUCCESS,
  }),

  getStatisticsError: (error): AnyAction => ({
    error,
    type: CONSANTS.GET_STATISTICS_ERROR,
  }),
});
