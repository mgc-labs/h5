/**
 * VehicleOrder selectors
 * @author lhf
 * @date 2018-9-11 16:31:42
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => {
    return state ? state.get('error') : '';
  });

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectOrderData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('dataCount'),
  );

export const makeSelectPageIndex = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('pageIndex'),
  );
export const makeSelectStatistics = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('statistics'),
  );
