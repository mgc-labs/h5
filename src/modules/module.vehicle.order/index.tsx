/**
 * VehicleOrder Component，整车订单
 * @author lhf
 * @date 2018-9-11 16:31:42
 */
import Badge from 'antd/es/badge';
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Divider from 'antd/es/divider';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Popconfirm from 'antd/es/popconfirm';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import Table from 'components/MyTable';
import { OptionButtons } from 'components/OptionButtons';
import { List, Map } from 'immutable';
import msngr from 'msngr';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import GLOBAL from 'utils/GLOBAL';
import {
  makeSelectDataCount,
  makeSelectError,
  makeSelectLoading,
  makeSelectOrderData,
  makeSelectPageIndex,
  makeSelectStatistics,
} from './selectors';

import styles from './index.module.less';

const Option = Select.Option;
const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const gutter = 16;
const colLayProps = {
  sm: 24,
  xs: 24,
  md: 12,
  lg: 12,
  xl: 6,
};
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
  colon: false,
};
export interface IVehicleOrderProps {
  children: React.ReactChildren;
  loading: boolean;
  form: WrappedFormUtils;
  orderList: List<Map<string, any>>;
  dataCount: number;
  pageIndex: number;
  permissions: List<Map<string, any>>;
  statistics: Map<string, any>;
  routeId: string;
  opera: string; // 删除和发货操作
  getOrderListData: (options?: object, pageIndex?: number) => any;
  sendGood: (data) => void;
  deleOrder: (data) => void;
  updateFilter: (data?: object) => any;
  getStatistics: (data?) => any;
}
interface IVehicleOrderState {
  permissions: any[];
  skipCount: number;
  pageSize: number;
}

class VehicleOrder extends React.PureComponent<
  IVehicleOrderProps,
  IVehicleOrderState
> {
  constructor(props) {
    super(props);
    this.state = {
      permissions: [],
      skipCount: 0,
      pageSize: 15,
    };
  }
  public componentDidMount() {
    this.setStatus();
    this.initSearch();
    this.handlePermissions();
    msngr('/vehicleOrder/list', 'refresh').on(d => {
      this.initSearch();
    });
    msngr('GLOBAL', 'activeRouteId', 'change').on(routeId => {
      // console.log(routeId, 'cccc', this.props.routeId);
    });
  }

  public componentWillUnmount() {
    msngr('/vehicleOrder/list', 'refresh').drop(d => {
      this.initSearch();
    });
  }

  // 取权限
  public handlePermissions() {
    const { permissions } = this.props;
    this.setState({
      permissions: permissions.toArray().map(data => data.get('operateKey')),
    });
  }

  public render() {
    return (
      <div className={styles.VehicleOrder}>
        <Card style={{ width: '100%' }}>{this.renderTopCard()}</Card>
        <Card
          title="整车订单"
          extra={this.renderAddBtn()}
          style={{ marginTop: 12 }}
        >
          {this.renderSelectBar()}
          {this.renderTable()}
        </Card>
      </div>
    );
  }

  private initSearch() {
    this.props.getStatistics();
    this.props.getOrderListData();
  }

  /*
   * 单个顶部卡片
   * @param label
   * @param value
   */
  private renderCard(label, value) {
    return (
      <div className={styles.CardSingle}>
        <div className={styles.CardSingle_label}>{label}</div>
        <div className={styles.CardSingle_value}>{value || 0}</div>
      </div>
    );
  }

  // 顶部所有卡片
  private renderTopCard() {
    const { statistics } = this.props;
    return (
      <div className={styles.TopCard}>
        {this.renderCard('总订单', statistics.get('total'))}
        {this.renderCard('待调度', statistics.get('pendingScheduling'))}
        {this.renderCard('配送中', statistics.get('distribution'))}
        {this.renderCard('已签收', statistics.get('haveSignedIn'))}
        {this.renderCard('未签收', statistics.get('notSigned'))}
      </div>
    );
  }

  private renderAddBtn() {
    const { permissions } = this.state;
    return (
      <Row style={{ textAlign: 'right' }}>
        {permissions.includes('/vehicleOrder/add') ? (
          <Link to="/vehicleOrder/add">
            <Button type="primary">整车开单</Button>
          </Link>
        ) : (
          ''
        )}
      </Row>
    );
  }

  // 设置状态为'全部'
  private setStatus() {
    const { setFieldsValue } = this.props.form;
    setFieldsValue({ status: '' });
  }

  // 搜索栏
  private renderSelectBar() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className={styles.SelectBar}>
        <Form onSubmit={this.handleSearch}>
          <Row gutter={gutter}>
            <Col className="gutter-row" {...colLayProps}>
              <FormItem {...formItemLayout} label="订单状态">
                {getFieldDecorator('status')(
                  <Select style={{ width: '100%' }}>
                    <Option value="">全部</Option>
                    <Option value="0">待发货</Option>
                    <Option value="1">待调度</Option>
                    <Option value="2">配送中</Option>
                    <Option value="3">已签收</Option>
                    <Option value="4">未签收</Option>
                  </Select>,
                )}
              </FormItem>
            </Col>
            <Col {...colLayProps}>
              <FormItem {...formItemLayout} label="下单时间">
                {getFieldDecorator('gmtCreateGte')(
                  <RangePicker style={{ width: '100%' }} />,
                )}
              </FormItem>
            </Col>
            <Col {...colLayProps}>
              <FormItem {...formItemLayout} label="用车时间">
                {getFieldDecorator('useCarDateGte')(
                  <RangePicker style={{ width: '100%' }} />,
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={gutter}>
            <Col {...colLayProps}>
              <FormItem {...formItemLayout} label="客户名称">
                {getFieldDecorator('customerName')(
                  <Input placeholder="请输入" />,
                )}
              </FormItem>
            </Col>
            <Col {...colLayProps}>
              <FormItem {...formItemLayout} label="订单号">
                {getFieldDecorator('orderNumber')(
                  <Input placeholder="请输入" />,
                )}
              </FormItem>
            </Col>
            <Col {...colLayProps}>
              <FormItem {...formItemLayout} label="第三方订单号">
                {getFieldDecorator('thirdSystemId')(
                  <Input placeholder="请输入" />,
                )}
              </FormItem>
            </Col>
            <Col {...colLayProps} style={{ marginTop: 5 }}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 12 }} onClick={this.handleReset}>
                重置
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }

  private renderTable() {
    const {
      dataCount,
      getOrderListData,
      orderList,
      loading,
      pageIndex,
    } = this.props;
    const { permissions } = this.state;
    const showDivider1 =
      permissions.includes('delete') &&
      (permissions.includes('send') ||
        permissions.includes('/vehicleOrder/update'));
    const showDivider2 =
      permissions.includes('send') &&
      permissions.includes('/vehicleOrder/update');
    const statusMap = ['default', 'default', 'processing', 'success', 'error'];
    const status = ['待发货', '待调度', '配送中', '已签收', '未签收'];
    const columns = [
      {
        title: '客户',
        width: 150,
        dataIndex: 'customerName',
        key: 'customerName',
        fixed: 'left',
      },
      {
        title: '订单号',
        width: 220,
        dataIndex: 'orderNumber',
        key: 'orderNumber',
        render: (text, record) =>
          permissions.includes('/vehicleOrder/detail') ? (
            <Link
              to={`/vehicleOrder/detail?id=${record.orderWholeCarId}`}
              onClick={() => {
                this.props.refresh(
                  `/vehicleOrder/detail?id=${record.orderWholeCarId}`,
                );
              }}
            >
              {text}
            </Link>
          ) : (
            <span>{text}</span>
          ),
        fixed: 'left',
      },
      {
        title: '第三方订单号',
        width: 200,
        dataIndex: 'thirdSystemId',
        key: 'thirdSystemId',
      },
      {
        title: '下单时间',
        width: 180,
        dataIndex: 'gmtCreate',
        key: 'gmtCreate',
      },
      {
        title: '用车时间',
        width: 180,
        dataIndex: 'useCarDate',
        key: 'useCarDate',
      },
      {
        title: '要求送达时间',
        width: 180,
        dataIndex: 'requireDate',
        key: 'requireDate',
      },
      {
        title: '所需车型',
        dataIndex: 'car',
        key: 'car',
      },
      {
        title: '状态',
        width: 100,
        dataIndex: 'status',
        key: 'status',
        render: val => <Badge status={statusMap[val]} text={status[val]} />,
      },
    ];
    if (orderList.filter(data => data.get('status') === 0).size > 0) {
      columns.push({
        title: '操作',
        width: 105,
        dataIndex: 'carStruct',
        key: 'action',
        render: (text, record) => {
          return (
            <span>
              {record.status === 0 ? (
                <span>
                  <OptionButtons>
                    {/* {showDivider1 ? <Divider type="vertical" /> : ''} */}
                    {permissions.includes('send') ? (
                      <Popconfirm
                        title="是否确认发货？"
                        onConfirm={() => {
                          this.sendGood(record.orderWholeCarId);
                        }}
                      >
                        <a href="javascript:void(0);">发货</a>
                      </Popconfirm>
                    ) : null}
                    {/* {showDivider2 ? <Divider type="vertical" /> : ''} */}
                    {permissions.includes('/vehicleOrder/update') ? (
                      <Link
                        to={`/vehicleOrder/update?id=${record.orderWholeCarId}`}
                      >
                        修改
                      </Link>
                    ) : null}
                    {permissions.includes('delete') ? (
                      <Popconfirm
                        title="是否确认删除？"
                        onConfirm={() => {
                          this.deleOrder(record.orderWholeCarId);
                        }}
                      >
                        <a href="javascript:void(0);">删除</a>
                      </Popconfirm>
                    ) : null}
                  </OptionButtons>
                </span>
              ) : null}
            </span>
          );
        },
        fixed: 'right',
      });
    }

    const tableProps = {
      columns,
      dataSource: orderList
        .map(item => ({
          key: item.get('orderWholeCarId'),
          orderWholeCarId: item.get('orderWholeCarId'),
          customerName: item.get('customerName'),
          thirdSystemId: item.get('thirdSystemId') || GLOBAL.emptyRecord,
          orderNumber: item.get('orderNumber'),
          gmtCreate: item.get('gmtCreate'),
          useCarDate: item.get('useCarDate') || GLOBAL.emptyRecord,
          requireDate: item.get('requireDate') || GLOBAL.emptyRecord,
          carStruct: item.get('carStruct'),
          carLong: item.get('carLong'),
          car: item.get('carLong')
            ? item.get('carLong') / 1000 + '米' + item.get('carStruct')
            : item.get('carStruct'),
          status: item.get('status'),
        }))
        .toArray(),
      scroll: { x: 1500 },
      loading,
      pagination: {
        current: pageIndex,
        total: dataCount,
        onChange: this.paginationChange,
        onShowSizeChange: this.paginationChange,
      },
    };

    return <Table {...tableProps} />;
  }

  // 查询
  private handleSearch = e => {
    e.preventDefault();
    const data = this.getSearchData();
    this.props.updateFilter(data);
  };

  // 获取查询数据
  private getSearchData() {
    const { form } = this.props;
    const data = form.getFieldsValue();
    const gmtCreateValue = data.gmtCreateGte;
    const useCarDateValue = data.useCarDateGte;
    const dateData = {
      gmtCreateGte:
        gmtCreateValue && gmtCreateValue.length
          ? gmtCreateValue[0].format('YYYY-MM-DD 00:00:00')
          : '',
      gmtCreateLte:
        gmtCreateValue && gmtCreateValue.length
          ? gmtCreateValue[1].format('YYYY-MM-DD 23:59:59')
          : '',
      useCarDateGte:
        useCarDateValue && useCarDateValue.length
          ? useCarDateValue[0].format('YYYY-MM-DD 00:00:00')
          : '',
      useCarDateLte:
        useCarDateValue && useCarDateValue.length
          ? useCarDateValue[1].format('YYYY-MM-DD 23:59:59')
          : '',
    };
    return Object.assign(data, dateData);
  }

  // 重置
  private handleReset = () => {
    this.props.form.resetFields();
    this.setStatus();
    this.props.updateFilter();
  };

  // 发货
  private sendGood(id) {
    const { skipCount, pageSize } = this.state;
    const { pageIndex } = this.props;
    const formData = this.getSearchData();
    this.props.sendGood({
      orderWholeCarId: id,
      pageIndex,
      pageSize,
      skipCount,
      ...formData,
    });
  }

  // 删除
  private deleOrder(id) {
    const { skipCount, pageSize } = this.state;
    const { pageIndex } = this.props;
    const formData = this.getSearchData();
    this.props.deleOrder({
      orderWholeCarId: id,
      pageIndex,
      pageSize,
      skipCount,
      ...formData,
    });
  }

  // 翻页change
  private paginationChange = params => {
    const { skipCount, pageSize, current } = params;
    this.setState({
      skipCount,
      pageSize,
    });
    this.props.getOrderListData(
      { pageSize, skipCount },
      current === 0 ? 1 : current,
    );
  };
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getOrderListData: (options, pageIndex) =>
      dispatch(actions.getOrderListData(options, pageIndex)),
    updateFilter: data => dispatch(actions.updateFilter(data)),
    deleOrder: data => dispatch(actions.deleOrder(data)),
    sendGood: data => dispatch(actions.sendGood(data)),
    getStatistics: data => dispatch(actions.getStatistics(data)),
  });
  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    orderList: makeSelectOrderData(selectState),
    dataCount: makeSelectDataCount(selectState),
    pageIndex: makeSelectPageIndex(selectState),
    statistics: makeSelectStatistics(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(
    withConnect,
    Form.create(),
  )(VehicleOrder);
};
