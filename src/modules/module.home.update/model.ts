/**
 * HomeUpdate model
 * @author ggm
 * @date 2018/10/26 下午1:18:24
 */
import message from 'antd/es/message';
import { fromJS, List, Map } from 'immutable';
import { call, put, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { getData } from './service';

// The initial state of the App
const initialState = Map({
  data: List(),
  error: false,
  loading: false,
});
export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: List(),
      error: false,
      loading: false,
      dataCount: 0,
    }),
    actions: {
      GET_ASYNC_DATA: param => param,
      GET_ASYNC_DATA_ERROR: error => error,
      GET_ASYNC_DATA_SUCCESS: data => data,
    },
    reducers: {
      GET_ASYNC_DATA(state = initialState, action) {
        return state
          .set('error', false)
          .set('loading', true)
          .set('hasmoredata', true);
      },
      GET_ASYNC_DATA_ERROR(state = initialState, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_ASYNC_DATA_SUCCESS(state = initialState, action) {
        let data = state.get('data');
        if (data) {
          data = data.toJS();
          data.push(...action.payload.data);
        } else {
          data = action.payload.data;
        }
        if (!action.payload.data || action.payload.data.length <= 0) {
          return state.set('loading', false).set('hasmoredata', false);
        }
        return state.set('loading', false).set('data', fromJS(data));
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const data = yield call(getData, action.payload);
          yield put(actions.getAsyncDataSuccess(data));
        } catch (err) {
          yield put(actions.getAsyncDataError(err));
          message.error(err.toString());
        }
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ASYNC_DATA, fetchData);
      };
    },
  });
