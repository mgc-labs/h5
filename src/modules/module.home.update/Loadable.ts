/**
 * HomeUpdate Loadable
 * @author ggm
 * @date 2018/10/26 下午1:18:24
 */
import Loadable from 'react-loadable';

import LoadingIndicator from 'components/LoadingIndicator';

let Component;
if (process.env.NODE_ENV === 'development') {
  Component = require('./index').default;
} else {
  Component = config =>
    Loadable({
      loader: () =>
        import(/* webpackChunkName: "home-update" */ './index').then(exports =>
          exports.default(config),
        ),
      loading: LoadingIndicator,
    });
}

export default Component;

export { default as modelFactory } from './model';
