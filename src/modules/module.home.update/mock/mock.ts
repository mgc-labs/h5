export default {
  result: 'success',
  code: 'UTMS001000000001',
  count: 15,
  data: [
    {
      utmsVersionManageId: 123456,
      versionNo: 'v1.5.5',
      systemName: '经营管理',
      program: '货主管理',
      description:
        '1、风豹经营管理增加客户注册入口，会员认证入口。2、线索与注册打通，直接基于线索完成注册。',
      isDeleted: 0,
      opreateJobcard: '15119',
      opereateUserName: '郭井阳',
      gmtCreate: '2017-10-20 19:53:00',
      gmtModified: '2017-10-20 19:53:00',
    },
    {
      utmsVersionManageId: 123726,
      versionNo: 'V1.5.5',
      systemName: '经营管理',
      program: '货主管理',
      description:
        '1、风豹经营管理增加客户注册入口，会员认证入口。2、线索与注册打通，直接基于线索完成注册。',
      isDeleted: 0,
      opreateJobcard: '15119',
      opereateUserName: '郭井阳',
      gmtCreate: '2017-10-20 19:53:00',
      gmtModified: '2017-10-20 19:53:00',
    },
    {
      utmsVersionManageId: 123126,
      versionNo: '1.5.5',
      systemName: '经营管理',
      program: '货主管理',
      description:
        '1、风豹经营管理增加客户注册入口，会员认证入口。2、线索与注册打通，直接基于线索完成注册。',
      isDeleted: 0,
      opreateJobcard: '15119',
      opereateUserName: '郭井阳',
      gmtCreate: '2017-10-20 19:53:00',
      gmtModified: '2017-10-20 19:53:00',
    },
    {
      utmsVersionManageId: 123226,
      versionNo: '1.5.5',
      systemName: '经营管理',
      program: '货主管理',
      description:
        '1、风豹经营管理增加客户注册入口，会员认证入口。2、线索与注册打通，直接基于线索完成注册。',
      isDeleted: 0,
      opreateJobcard: '15119',
      opereateUserName: '郭井阳',
      gmtCreate: '2017-10-20 19:53:00',
      gmtModified: '2017-10-20 19:53:00',
    },
    {
      utmsVersionManageId: 123326,
      versionNo: '1.5.5',
      systemName: '经营管理',
      program: '货主管理',
      description:
        '1、风豹经营管理增加客户注册入口，会员认证入口。2、线索与注册打通，直接基于线索完成注册。',
      isDeleted: 0,
      opreateJobcard: '15119',
      opereateUserName: '郭井阳',
      gmtCreate: '2017-10-20 19:53:00',
      gmtModified: '2017-10-20 19:53:00',
    },
    {
      utmsVersionManageId: 123406,
      versionNo: '1.5.5',
      systemName: '经营管理',
      program: '货主管理',
      description:
        '1、风豹经营管理增加客户注册入口，会员认证入口。2、线索与注册打通，直接基于线索完成注册。',
      isDeleted: 0,
      opreateJobcard: '15119',
      opereateUserName: '郭井阳',
      gmtCreate: '2017-10-20 19:53:00',
      gmtModified: '2017-10-20 19:53:00',
    },
    {
      utmsVersionManageId: 123416,
      versionNo: '1.5.5',
      systemName: '经营管理',
      program: '货主管理',
      description:
        '1、风豹经营管理增加客户注册入口，会员认证入口。2、线索与注册打通，直接基于线索完成注册。',
      isDeleted: 0,
      opreateJobcard: '15119',
      opereateUserName: '郭井阳',
      gmtCreate: '2017-10-20 19:53:00',
      gmtModified: '2017-10-20 19:53:00',
    },
    {
      utmsVersionManageId: 123436,
      versionNo: '1.5.5',
      systemName: '经营管理',
      program: '货主管理',
      description:
        '1、风豹经营管理增加客户注册入口，会员认证入口。2、线索与注册打通，直接基于线索完成注册。',
      isDeleted: 0,
      opreateJobcard: '15119',
      opereateUserName: '郭井阳',
      gmtCreate: '2017-10-20 19:53:00',
      gmtModified: '2017-10-20 19:53:00',
    },
  ],
  msg: '查询版本列表成功！',
};
