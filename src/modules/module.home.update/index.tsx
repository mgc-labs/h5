/**
 * HomeUpdate Component
 * @author ggm
 * @date 2018/10/26 下午1:18:24
 */
import * as React from 'react';
import ReactDOM from 'react-dom';

import Card from 'antd/es/card';
import Icon from 'antd/es/icon';

import { List, Map } from 'immutable';
import { createStructuredSelector } from 'reselect';

import {
  makeSelectData,
  makeSelectError,
  makeSelectHasMore,
  makeSelectLoading,
} from './selectors';

import styles from './index.module.less';

export interface IHomeUpdateProps {
  children: React.ReactChildren;
  data: List<Map<string, any>>;
  hasmoredata: boolean;
  getAsyncData: (param?: any) => any;
}

class HomeUpdate extends React.PureComponent<IHomeUpdateProps> {
  conditions = {
    current: 1,
    skipCount: 0,
    pageSize: 10,
  };
  public componentDidMount() {
    this.props.getAsyncData(this.conditions);
  }
  public render() {
    return (
      <Card
        title="联运配送管理系统更新记录"
        className={styles.HomeUpdate}
        onScroll={this.onScroll}
        bodyStyle={{ marginTop: '15px' }}
      >
        {this.renderVersion()}
      </Card>
    );
  }
  private renderDescription = data => {
    const dataArr = data.match(/\n/g) ? data.split(/\n/g) : [data];
    const html = [];
    dataArr.map((item, index) => {
      const key = 'description' + index;
      html.push(<p key={key}>{item}</p>);
    });
    return html;
  };
  private renderVersion = () => {
    const versions = this.props.data.toJS();
    const Lis = versions.reduce((total, item, index) => {
      total.push(
        <li
          className={styles.HomeUpdate__li}
          key={`${item.utmsVersionManageId}__${index}`}
        >
          <div className={styles.HomeUpdate__version}>
            <p style={{ fontSize: '16px' }}>
              {item.versionNo
                ? item.versionNo.toUpperCase().indexOf('V') === 0
                  ? item.versionNo.toUpperCase()
                  : `V${item.versionNo}`
                : ''}
            </p>
            <p style={{ color: '#9BA0AA' }}>
              {item.gmtCreate ? item.gmtCreate.substring(0, 10) : ''}
            </p>
          </div>
          <Card
            className={styles.HomeUpdate__card}
            bodyStyle={{
              paddingBottom: '50px',
              position: 'relative',
              top: '-25px',
            }}
          >
            <h3>{item.program}</h3>
            {this.renderDescription(item.description)}
          </Card>
        </li>,
      );
      return total;
    }, []);
    if (Lis.length > 0) {
      return <ul style={{ paddingTop: '15px' }}>{Lis}</ul>;
    } else {
      return (
        <Card
          style={{
            border: 'none',
            textAlign: 'center',
            fontSize: '14px',
            color: 'rgba(0, 0, 0, 0.45)',
          }}
        >
          <Icon type="frown" theme="outlined" style={{ marginRight: '5px' }} />
          暂无更新记录
        </Card>
      );
    }
  };
  private onScroll = e => {
    const hasMore = this.props.hasmoredata;
    const clientHeight = e.target.clientHeight;
    const scrollHeight = e.target.scrollHeight;
    const scrollTop = e.target.scrollTop;
    if (clientHeight + scrollTop >= scrollHeight) {
      this.conditions.skipCount =
        this.conditions.current * this.conditions.pageSize;
      this.conditions.current += 1;
      if (hasMore) {
        this.props.getAsyncData(this.conditions);
      }
    }
  };
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        data: makeSelectData(currentState),
        hasmoredata: makeSelectHasMore(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getAsyncData: param => dispatch(actions.getAsyncData(param)),
      }),
    };
  })(HomeUpdate);
};
