/**
 * HomeUpdate selectors
 * @author ggm
 * @date 2018/10/26 下午1:18:24
 */
import { List, Map } from 'immutable';
import { createSelector } from 'reselect';
// The initial state of the App
const initialState = Map({
  data: List(),
  error: false,
  loading: false,
});
export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any> = initialState) =>
    state.get('error'),
  );

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any> = initialState) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any> = initialState) =>
    state.get('data'),
  );

export const makeSelectHasMore = selectState =>
  createSelector(selectState, (state: Map<string, any> = initialState) =>
    state.get('hasmoredata'),
  );
