/**
 * HomeUpdate service
 * @author ggm
 * @date 2018/10/26 下午1:18:24
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_VERSION_LIST =
  '/ehuodiGateway/utmsCore/utmsVersionManagecs/queryUtmsVersionManageList';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_VERSION_LIST)
    .reply(() => import('./mock/mock').then(exports => [200, exports.default]));
}

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getData = data => {
  return request(
    {
      method: 'post',
      url: API_VERSION_LIST,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
