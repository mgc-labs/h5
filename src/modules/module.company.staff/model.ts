/**
 * CompanyStaff model
 * @author HuangSiFei
 * @date 2018-9-19 17:33:32
 */
import message from 'antd/es/message';
import { fromJS, List, Map } from 'immutable';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { getCompanyList, getStaff, getStaffDetails } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      staffList: List(),
      companyTree: List(),
      pickStaff: Map(),
      selectedCompany: Map(),
      error: false,
      loading: false,
      dataCount: 0,
    }),
    actions: {
      GET_COMPANY_TREE: () => ({}),
      GET_COMPANY_TREE_ERROR: error => error,
      GET_COMPANY_TREE_SUCCESS: companyTree => companyTree,

      GET_STAFF_LIST: data => data,
      GET_STAFF_LIST_ERROR: error => error,
      GET_STAFF_LIST_SUCCESS: staffList => staffList,

      ON_SELECTED_COMPANY: data => data,

      ALL_CHECKED_CHANGED: data => data,

      CHECKED_CHANGED: data => data,

      GET_STAFF_DETAILS: userName => userName,
      GET_STAFF_DETAILS_ERROR: error => error,
      GET_STAFF_DETAILS_SUCCESS: staff => staff,

      SELECT_COMPANY: code => code,

      RESET_STAFF_DETAILS: () => ({}),
    },
    reducers: {
      GET_COMPANY_TREE(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_COMPANY_TREE_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_COMPANY_TREE_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('companyTree', fromJS(action.payload));
      },

      GET_STAFF_LIST(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_STAFF_LIST_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_STAFF_LIST_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('dataCount', action.payload.count)
          .set('staffList', fromJS(action.payload.data));
      },

      ON_SELECTED_COMPANY(state, action) {
        return state.set('selectedCompany', fromJS(action.payload));
      },

      ALL_CHECKED_CHANGED(state, action) {
        const checked = action.payload;
        let staffList = state.get('staffList') as List<Map<string, any>>;
        staffList = staffList.map(item => {
          if (item.get('status') === 1) {
            return item.set('isChecked', checked);
          } else {
            return item;
          }
        });
        return state.set('staffList', staffList);
      },

      CHECKED_CHANGED(state, action) {
        const record = action.payload;
        let staffList = state.get('staffList') as List<Map<string, any>>;
        staffList = staffList.map(item => {
          if (item.get('utmsUsersId') === record.utmsUsersId) {
            return item.set('isChecked', !record.isChecked);
          } else {
            return item;
          }
        });

        return state.set('staffList', staffList);
      },

      GET_STAFF_DETAILS(state, action) {
        return state.set('error', false);
      },
      GET_STAFF_DETAILS_SUCCESS(state, action) {
        return state.set('pickStaff', fromJS(action.payload));
      },
      GET_STAFF_DETAILS_ERROR(state, action) {
        return state.set('error', action.payload);
      },

      SELECT_COMPANY(state, action) {
        return state.set('selectedCompany', fromJS(action.payload));
      },

      RESET_STAFF_DETAILS(state, action) {
        return state.set('pickStaff', Map());
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchStaff(action) {
        const params = action.payload ? action.payload : {};
        try {
          const data = yield call(getStaff, params);
          yield put(actions.getStaffListSuccess(data));
        } catch (err) {
          yield put(actions.getStaffListError(err));
          message.error(err.toString());
        }
      }

      function* fetchDeatils(action) {
        try {
          const data = yield call(getStaffDetails, {
            userName: action.payload,
          });
          yield put(actions.getStaffDetailsSuccess(data));
        } catch (err) {
          yield put(actions.getStaffDetailsError(err));
          message.error(err.toString());
        }
      }

      function* fetchCompany() {
        try {
          const data = yield call(getCompanyList);
          yield put(actions.getCompanyTreeSuccess(data));
          yield call(fetchStaff, {
            payload: {
              organizationCode: data[0].organizationCode,
            },
          });
        } catch (err) {
          yield put(actions.getCompanyTreeError(err));
          message.error(err.toString());
        }
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_COMPANY_TREE, fetchCompany);
        yield takeLatest(CONSTANTS.GET_STAFF_LIST, fetchStaff);
        yield takeLatest(CONSTANTS.GET_STAFF_DETAILS, fetchDeatils);
      };
    },
  });
