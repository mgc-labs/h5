export default {
  result: 'success',
  count: 18,
  data: [
    {
      utmsDictionaryId: 123,
      dictionaryType: '车型',
      parentDictionaryCode: '',
      parentDictionaryName: 'department',
      dictionaryName: '总裁办',
      dictionaryCode: '111',
      orderNumber: 111,
      description: '车型:厢式车的字典描述',
    },
    {
      utmsDictionaryId: 234,
      dictionaryType: '车型',
      parentDictionaryCode: '',
      parentDictionaryName: 'department',
      dictionaryName: '研发中心',
      dictionaryCode: '001',
      orderNumber: 2222,
      description: '车型:厢式车的字典描述',
    },
  ],
  code: 'UTMS123456789123',
  msg: '查询字典列表成功 ',
};
