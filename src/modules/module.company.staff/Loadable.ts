/**
 * CompanyStaff Loadable
 * @author HuangSiFei
 * @date 2018-9-19 17:33:32
 */
import Loadable from 'react-loadable';

import LoadingIndicator from 'components/LoadingIndicator';

let Component;
if (process.env.NODE_ENV === 'development') {
  Component = require('./index').default;
} else {
  Component = config =>
    Loadable({
      loader: () =>
        import(/* webpackChunkName: "company-staff" */ './index').then(
          exports => exports.default(config),
        ),
      loading: LoadingIndicator,
    });
}

export default Component;

export { default as modelFactory } from './model';
