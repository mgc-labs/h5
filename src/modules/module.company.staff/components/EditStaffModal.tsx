/**
 * EditStaffModal Component
 * @author HuangSiFei
 * @date 2018-9-14 11:05:19
 */
import Col from 'antd/es/col';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Select from 'antd/es/select';
import MutiSelectWithEditableOptions from 'components/MutiSelectWithEditableOptions';
import { fromJS, is, List, Map } from 'immutable';
import * as React from 'react';
import { MOBILE_PHONE } from 'utils/commonRegExp';
import { addDictionary, deleteDictionary, getDictionary } from '../service';

import styles from 'modules/module.company.management/index.module.less';

const FormItem = Form.Item;
const Option = Select.Option;
const itemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

interface IEditStaffModalProps {
  form: any;
  formValue: any;
  visible: boolean;
  companyTree: List<Map<string, any>>;
  selectedNode: string;
  onClose: () => void;
  onConfirm: (values) => void;
  refreshDictionary: (option) => void;
}

interface IEditStaffModalStates {
  resetSelect: boolean;
  checkedDepartment: string;
  checkedPosition: string;
  departmentList: List<Map<string, any>>;
  positionList: List<Map<string, any>>;
}

class EditStaff extends React.PureComponent<
  IEditStaffModalProps,
  IEditStaffModalStates
> {
  constructor(props) {
    super(props);
    this.state = {
      resetSelect: false,
      checkedDepartment: '',
      checkedPosition: '',
      departmentList: List(),
      positionList: List(),
    };
  }
  public componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({ resetSelect: true });
    } else {
      this.setState({ resetSelect: false });
    }
  }
  public async componentDidMount() {
    const departments = await this.getDictionaryList('department');
    const positions = await this.getDictionaryList('position');
    this.setState({
      departmentList: fromJS(departments),
      positionList: fromJS(positions),
    });
  }
  public getDictionaryList = async type => {
    const result = await getDictionary({
      parentDictionaryCode: type,
    });
    if (result.result === 'success') {
      return result.data;
    }
  };
  public handleSubmit = () => {
    const { form, onConfirm } = this.props;
    const { getFieldValue } = form;
    form.validateFields((errors, values) => {
      if (!errors) {
        onConfirm(
          Object.assign(
            getFieldValue('utmsUsersId')
              ? { utmsUsersId: getFieldValue('utmsUsersId') }
              : {},
            values,
          ),
        );
      }
    });
  };
  public handleClose = () => {
    this.setState({
      checkedDepartment: '',
      checkedPosition: '',
    });
    this.props.onClose();
  };
  public removeOption = async (type, option) => {
    const { getFieldValue } = this.props.form;
    this.setState({
      checkedDepartment: getFieldValue('departmentNames'),
      checkedPosition: getFieldValue('postNames'),
    });
    const params = {
      bizType: type,
      dictionaryCode: option,
    };
    const result = await deleteDictionary(params);
    if (result.result === 'success') {
      const newState = {};
      newState[params.bizType + 'List'] = fromJS(
        await this.getDictionaryList(params.bizType),
      );
      this.setState(newState);
    }
  };
  public addOption = async (type, option) => {
    const { getFieldValue } = this.props.form;
    this.setState({
      checkedDepartment: getFieldValue('departmentNames'),
      checkedPosition: getFieldValue('postNames'),
    });
    const params = {
      bizType: type,
      dictionaryCode: option,
    };
    const result = await addDictionary(params);
    if (result.result === 'success') {
      const newState = {};
      newState[params.bizType + 'List'] = fromJS(
        await this.getDictionaryList(params.bizType),
      );
      this.setState(newState);
    }
  };
  public addToForm = (type, options) => {
    const { form } = this.props;
    if (type === 'department') {
      form.setFieldsValue({
        departmentNames: options
          .filter(option => {
            return option.checked;
          })
          .map(option => {
            return option.value;
          })
          .join(','),
      });
    } else {
      form.setFieldsValue({
        postNames: options
          .filter(option => {
            return option.checked;
          })
          .map(option => {
            return option.value;
          })
          .join(','),
      });
    }
  };
  public renderEditor() {
    const { form, companyTree, selectedNode, formValue } = this.props;
    const { getFieldDecorator, getFieldValue } = form;
    const {
      checkedDepartment,
      checkedPosition,
      departmentList,
      positionList,
    } = this.state;
    const departmentNames = getFieldValue('departmentNames') || '';
    const postNames = getFieldValue('postNames') || '';
    const initialCompany = selectedNode;
    const options = companyTree.map((company, index) => {
      return (
        <Option key={company.get('organizationCode')}>
          {company.get('organizationName')}
        </Option>
      );
    });
    const departmentArray = (departmentNames + ',' + checkedDepartment).split(
      ',',
    );
    const positionArray = (postNames + ',' + checkedPosition).split(',');
    const departmentProps = {
      options: departmentList
        .filter(d => {
          return d.get('dictionaryCode');
        })
        .map(d => ({
          value: d.get('dictionaryName'),
          key: d.get('dictionaryCode'),
          checked: departmentArray.includes(d.get('dictionaryName')),
        }))
        .toArray(),
      placeholder: '选择所属部门',
      addInputPlaceHolder: '请输入添加部门的名称',
      addBtnText: '添加部门',
      resetSelect: this.state.resetSelect,
      tagSize: 5,
      optionSize: 20,
      removeOption: option => this.removeOption('department', option),
      addOption: option => this.addOption('department', option),
      addToForm: departments => this.addToForm('department', departments),
    };
    const positionProps = {
      options: positionList
        .filter(d => {
          return d.get('dictionaryCode');
        })
        .map(d => ({
          value: d.get('dictionaryName'),
          key: d.get('dictionaryCode'),
          checked: positionArray.includes(d.get('dictionaryName')),
        }))
        .toArray(),
      placeholder: '选择所属岗位',
      addInputPlaceHolder: '请输入添加岗位的名称',
      addBtnText: '添加岗位',
      resetSelect: this.state.resetSelect,
      isMuti: true,
      tagSize: 5,
      optionSize: 20,
      removeOption: option => this.removeOption('position', option),
      addOption: option => this.addOption('position', option),
      addToForm: positions => this.addToForm('position', positions),
    };
    return (
      <Form>
        <FormItem {...itemLayout} label="用户名" required>
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: '请输入用户名' }],
          })(
            <Input
              placeholder="如工号，姓名，常用名等，用于登录系统使用"
              autoComplete="off"
              maxLength={60}
              disabled={formValue.get('userName') ? true : false}
            />,
          )}
        </FormItem>
        <FormItem {...itemLayout} label="姓名" required>
          {getFieldDecorator('realName', {
            rules: [{ required: true, message: '输入员工真实姓名' }],
          })(<Input placeholder="输入员工真实姓名" maxLength={40} />)}
        </FormItem>
        <FormItem {...itemLayout} label="所属公司" required>
          {getFieldDecorator('organizationCode', {
            initialValue: getFieldValue('organizationCode') || initialCompany,
            rules: [{ required: true, message: '请输入所属公司' }],
          })(
            <Select
              placeholder="请输入所属公司"
              getPopupContainer={triggerNode => triggerNode.parentNode}
            >
              {options}
            </Select>,
          )}
        </FormItem>
        <FormItem {...itemLayout} label="手机号" required>
          {getFieldDecorator('mobileNumber', {
            rules: [
              { required: true, message: '请输入手机号' },
              {
                pattern: MOBILE_PHONE,
                message: '请输入正确的手机号!',
              },
            ],
          })(
            <Input
              placeholder="输入手机号，支持短信验证码登录"
              maxLength={11}
            />,
          )}
        </FormItem>
        <FormItem {...itemLayout} label="部门">
          {getFieldDecorator('departmentNames')(
            <MutiSelectWithEditableOptions {...departmentProps} />,
          )}
        </FormItem>
        <FormItem {...itemLayout} label="岗位">
          {getFieldDecorator('postNames')(
            <MutiSelectWithEditableOptions {...positionProps} />,
          )}
        </FormItem>
      </Form>
    );
  }
  public render() {
    const { visible, formValue } = this.props;
    const modalProps = {
      title: formValue.get('utmsUsersId') ? '修改员工' : '新增员工',
      visible,
      centered: true,
      maskClosable: false,
      onOk: this.handleSubmit,
      onCancel: this.handleClose,
    };
    return <Modal {...modalProps}>{this.renderEditor()}</Modal>;
  }
}
const EditStaffModal = Form.create({
  mapPropsToFields(props: any) {
    const params = {};
    const formObj = props.formValue.toObject();
    if (formObj.organizationCode) {
      Object.keys(formObj).map(key => {
        params[key] = Form.createFormField({
          value: formObj[key],
        });
      });
    } else if (props.selectedNode) {
      params.organizationCode = Form.createFormField({
        value: props.selectedNode,
      });
    }
    return { ...params };
  },
})(EditStaff);
export default EditStaffModal;
