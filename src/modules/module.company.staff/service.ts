/**
 * CompanyStaff service
 * @author HuangSiFei
 * @date 2018-9-19 17:33:32
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_STAFF_LIST =
  '/ehuodiGateway/utmsCore/utmsUserscs/selectUtmsUsersList';
const API_COMPANYDATA =
  '/ehuodiGateway/utmsCore/utmsOrganizationcs/selectUtmsOrganizationList';
const API_UPDATESTAFF = '/ehuodiGateway/utmsCore/utmsUserscs/updateUtmsUsers';
const API_ADDSTAFF = '/ehuodiGateway/utmsCore/utmsUserscs/createUtmsUsers';
const API_USERSTATUS =
  '/ehuodiGateway/utmsCore/utmsUserscs/updateUtmsUsersStatus';
const API_UPDATEPWD = '/ehuodiGateway/utmsCore/utmsUserscs/updateUtmsUsersPwd';
const API_STAFFDETAILS = '/ehuodiGateway/utmsCore/utmsUserscs/selectUtmsUsers';
const API_DELETEDICTIONARY =
  '/ehuodiGateway/utmsCore/utmsDictionarycs/delCustUtmsDictionary';
const API_ADDDICTIONARY =
  '/ehuodiGateway/utmsCore/utmsDictionarycs/createCustUtmsDictionary';
const API_GETDICTIONARY =
  '/ehuodiGateway/utmsCore/utmsDictionarycs/selectCustDictionaryList';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_ADDDICTIONARY)
    .reply(() =>
      import('./mock/success').then(exports => [200, exports.default]),
    );
}

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getStaff = data =>
  request(
    {
      method: 'post',
      url: API_STAFF_LIST,
      data,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
export const getCompanyList = () =>
  request(
    {
      method: 'post',
      url: API_COMPANYDATA,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

export const updateUser = params =>
  request(
    {
      method: 'post',
      url: API_UPDATESTAFF,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

export const addUser = params =>
  request(
    {
      method: 'post',
      url: API_ADDSTAFF,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

export const updateUserStatus = params =>
  request(
    {
      method: 'post',
      url: API_USERSTATUS,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

export const updateUserPwd = params =>
  request(
    {
      method: 'post',
      url: API_UPDATEPWD,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

export const getStaffDetails = params =>
  request(
    {
      method: 'post',
      url: API_STAFFDETAILS,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

export const getDictionary = params =>
  request(
    {
      method: 'post',
      url: API_GETDICTIONARY,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);
export const deleteDictionary = params =>
  request(
    {
      method: 'post',
      url: API_DELETEDICTIONARY,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

export const addDictionary = params =>
  request(
    {
      method: 'post',
      url: API_ADDDICTIONARY,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);
