/**
 * CompanyStaff selectors
 * @author HuangSiFei
 * @date 2018-9-19 17:33:32
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

const authData = state => state.get('authorization');
// const accountInfo = state => state.get('accountInfo');

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectCompanyTree = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('companyTree'),
  );

export const makeSelectStaffList = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('staffList'),
  );

export const makeSelectCompany = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('selectedCompany'),
  );

export const makePickStaff = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('pickStaff'),
  );

export const makeSelectDepartment = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('departmentList'),
  );

export const makeSelectPosition = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('positionList'),
  );

// export const makeSelectUserData = selectState =>
//   createSelector(authData, (state: Map<string, any>) => state.get('userData'));

export const makeSelectAccountInfo = selectState =>
  createSelector(authData, (state: Map<string, any>) =>
    state.get('accountInfo'),
  );

export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('dataCount'),
  );
