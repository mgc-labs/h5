/**
 * CompanyStaff Component
 * @author HuangSiFei
 * @date 2018-9-19 17:33:32
 */
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import CheckBox from 'antd/es/checkbox';
import Col from 'antd/es/col';
import Divider from 'antd/es/divider';
import Dropdown from 'antd/es/dropdown';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Layout from 'antd/es/layout';
import Menu from 'antd/es/menu';
import message from 'antd/es/message';
import Modal from 'antd/es/modal';
import Popconfirm from 'antd/es/popconfirm';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import MyTable from 'components/MyTable';
import { OptionButtons } from 'components/OptionButtons';
import {
  PageBottom,
  PageBottomButtons,
  PageBottomCheck,
} from 'components/PageBottom';
import SiderCompanyTree from 'components/SiderCompanyTree';
import StaffAuthorizationSettingModal from 'components/StaffAuthorizationSettingModal';
import { fromJS, List, Map } from 'immutable';
import * as React from 'react';
import { createStructuredSelector } from 'reselect';
import GLOBAL from 'utils/GLOBAL';
import EditStaffModal from './components/EditStaffModal';

import {
  makePickStaff,
  makeSelectAccountInfo,
  makeSelectCompanyTree,
  makeSelectDataCount,
  makeSelectDepartment,
  makeSelectError,
  makeSelectLoading,
  makeSelectPosition,
  makeSelectStaffList,
  // makeSelectUserData,
} from './selectors';

import {
  addUser,
  updateUser,
  updateUserPwd,
  updateUserStatus,
} from './service';

import './index.less';
import styles from './index.module.less';

const { Content } = Layout;
const { Item: FormItem } = Form;
const { Option } = Select;
const { Item: MenuItem } = Menu;
const confirm = Modal.confirm;

export interface ICompanyStaffProps {
  children: React.ReactChildren;
  form: WrappedFormUtils;
  companyTree: List<Map<string, any>>;
  dataCount: number;
  staffList: List<Map<string, any>>;
  pickStaff: Map<string, any>;
  loading: boolean;
  // userData: Map<string, any>;
  permissions: List<Map<string, any>>;
  accountInfo: Map<string, any>;
  getCompanyTree: () => any;
  getStaffList: (options?: object) => any;
  onAllCheckChanged: (checked: boolean) => void;
  onCheckedChanged: (record: any) => void;
  getStaffDetails: (userName: string) => void;
  resetStaffDetails: () => any;
}

interface ICompanyStaffState {
  openModal: boolean;
  openAuthModal: boolean;
  authStaff: List<any>;
  skipCount: number;
  pageSize: number;
  current: number;
  selectedKeys: List<any>;
  selectedNode: string;
}

@Form.create()
class CompanyStaff extends React.PureComponent<
  ICompanyStaffProps,
  ICompanyStaffState
> {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      openAuthModal: false,
      authStaff: List(),
      skipCount: 0,
      pageSize: 15,
      current: 1,
      selectedKeys: List(),
      selectedNode: '',
    };
  }
  public paginationChange = params => {
    const { skipCount, pageSize, current } = params;
    this.setState({
      skipCount,
      pageSize,
      current,
    });
    this.doSearch(params);
  };
  public doSearch = (params?: any) => {
    const { form, companyTree } = this.props;
    const { getFieldsValue } = form;
    const paramObj = Object.assign(
      {
        organizationCode:
          this.state.selectedNode || companyTree.get(0).get('organizationCode'),
        status: getFieldsValue().status,
        keywords: getFieldsValue().keywords,
        pageSize: this.state.pageSize,
        skipCount: this.state.skipCount,
      },
      params,
    );
    paramObj.keywords = paramObj.keywords ? paramObj.keywords.trim() : '';
    this.props.getStaffList(paramObj);
  };
  public onChangeStatus = value => {
    this.setState({
      current: 1,
    });
    this.doSearch({
      skipCount: 0,
      status: value,
    });
  };
  public componentDidMount() {
    this.props.getCompanyTree();
  }

  public handleSelect = selected => {
    this.setState({
      selectedKeys: List([selected.get('utmsOrganizationId') + '']),
      selectedNode: selected.get('organizationCode'),
      current: 1,
    });
    this.doSearch({
      skipCount: 0,
      organizationCode: selected.get('organizationCode'),
    });
  };
  public handleSubmit = e => {
    e.preventDefault();
    const { selectedNode } = this.state;
    if (selectedNode) {
      this.doSearch({
        organizationCode: selectedNode,
        skipCount: 0,
      });
    } else {
      this.doSearch({
        skipCount: 0,
      });
    }
  };
  public handleReset = () => {
    this.props.form.resetFields();
    const { companyTree } = this.props;
    this.setState({
      selectedKeys: List([companyTree.get(0).get('utmsOrganizationId') + '']),
      selectedNode: companyTree.get(0).get('organizationCode'),
    });
    this.doSearch({
      organizationCode: this.props.companyTree.get(0).get('organizationCode'),
    });
  };

  public handleOpenModal = (staff?, item?) => {
    if (staff) {
      this.props.getStaffDetails(staff.userName);
    } else {
      this.props.resetStaffDetails();
    }
    this.setState({
      openModal: true,
    });
  };
  public handleCloseModal = () => {
    this.setState({
      openModal: false,
    });
  };
  public handleEditStaff = async values => {
    const { companyTree } = this.props;
    let selectedKey = '';
    companyTree.map(company => {
      if (company.get('organizationCode') === values.organizationCode) {
        selectedKey = company.get('utmsOrganizationId') + '';
      }
    });
    if (values.utmsUsersId) {
      const result = await updateUser(values);
      if (result.result === 'success') {
        message.success('修改员工成功');
        this.setState({
          openModal: false,
          selectedKeys: List([selectedKey]),
          selectedNode: values.organizationCode,
        });
        this.doSearch({ organizationCode: values.organizationCode });
      } else {
        message.error('修改员工失败');
      }
    } else {
      const result = await addUser(values);
      if (result.result === 'success') {
        Modal.info({
          title: '员工创建成功',
          content: `用户名:${
            values.userName
          }，密码123456，请登录系统更改密码。`,
        });
        this.setState({
          openModal: false,
          selectedKeys: List([selectedKey]),
          selectedNode: values.organizationCode,
        });
        this.doSearch({ organizationCode: values.organizationCode });
      } else {
        message.error('新增员工失败');
      }
    }
  };

  public showToggleStatus = record => {
    confirm({
      title: '',
      content: `禁用后, ${record.realName} 将被限制登录，是否继续？`,
      onOk: () => this.confirmStatus(record),
    });
  };

  public showResetPwd = record => {
    confirm({
      title: '',
      content: `你确定要重置 ${record.realName} 的密码？`,
      onOk: () => this.confirmReset(record),
    });
  };

  // public renderMoreBtn = (record, permissionArray) => {
  //   const menu = (
  //     <Menu>
  //       {permissionArray.map(item => {
  //         if (item === 'status') {
  //           return (
  //             <MenuItem key={'status'}>
  //               {/* <Popconfirm
  //                 title={popConfirmMsg}
  //                 onConfirm={() => this.confirmStatus(record)}
  //               >
  //                 <a>{popConfirmBtn}</a>
  //               </Popconfirm> */}
  //               <a onClick={() => this.showToggleStatus(record)}>禁用</a>
  //             </MenuItem>
  //           );
  //         }
  //         if (item === 'reset') {
  //           return (
  //             <MenuItem key={'reset'}>
  //               {/* <Popconfirm
  //                 title="你确定要重制密码？"
  //                 onConfirm={() => this.confirmReset(record)}
  //               >
  //                 <a>重置</a>
  //               </Popconfirm> */}
  //               <a onClick={() => this.showResetPwd(record)}>重置</a>
  //             </MenuItem>
  //           );
  //         }
  //         if (item === 'auth') {
  //           return (
  //             <MenuItem key={'auth'}>
  //               <a onClick={() => this.showAuthModal(record)}>权限</a>
  //             </MenuItem>
  //           );
  //         }
  //       })}
  //     </Menu>
  //   );
  //   return (
  //     <React.Fragment>
  //       <Dropdown overlay={menu}>
  //         <a>更多</a>
  //       </Dropdown>
  //     </React.Fragment>
  //   );
  // };

  confirmStatus = async record => {
    const params = {
      utmsUsersId: record.utmsUsersId,
      status: record.status === 9 ? 1 : 9,
    };
    const result = await updateUserStatus(params);
    if (result.result === 'success') {
      message.success('修改状态成功');
      this.doSearch();
    }
  };

  confirmReset = async record => {
    const url = this.props.accountInfo.get('domainName');
    const params = {
      utmsUsersId: record.utmsUsersId,
      userName: record.userName,
      operateType: 'RESET',
    };
    const result = await updateUserPwd(params);
    if (result.result === 'success') {
      Modal.info({
        title: '密码重置成功',
        content: '密码已重置，新密码为：' + url + '123456',
      });
    }
  };

  public renderSearchParams = () => {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form
        className={styles.StaffList__Form}
        layout="inline"
        onSubmit={this.handleSubmit}
      >
        <Row type="flex">
          <Col span={20}>
            <FormItem label="状态">
              {getFieldDecorator('status', { initialValue: '1' })(
                <Select
                  className={styles.StaffList__SelectAction}
                  onSelect={this.onChangeStatus}
                >
                  <Option value="1">生效中</Option>
                  <Option value="9">已作废</Option>
                  <Option value="">全部</Option>
                </Select>,
              )}
            </FormItem>
            <FormItem label="关键词搜索">
              {getFieldDecorator('keywords')(
                <Input
                  placeholder="请输入用户名/姓名/手机号"
                  style={{ width: '180px' }}
                />,
              )}
            </FormItem>
            <FormItem className={styles.StaffList__FormAction}>
              <Button type="primary" htmlType="submit">
                查 询
              </Button>
              <Button onClick={this.handleReset}>重 置</Button>
            </FormItem>
          </Col>
          <Col span={4} className={styles.StaffList__RightAction}>
            <Button type="primary" onClick={() => this.handleOpenModal()}>
              新增员工
            </Button>
          </Col>
        </Row>
      </Form>
    );
  };

  public renderStaffOperation = (
    item,
    record,
    permissionArray,
    dividerShow,
    moreBtn,
  ) => {
    return (
      <React.Fragment>
        <OptionButtons>
          {[
            permissionArray.includes('edit') && (
              <a
                href="javascript:;"
                onClick={() => this.handleOpenModal(record, item)}
              >
                修改
                {record.isChecked}
              </a>
            ),
            ...moreBtn.map(h => {
              if (h === 'status') {
                return (
                  <a onClick={() => this.showToggleStatus(record)}>禁用</a>
                );
              }
              if (h === 'reset') {
                return <a onClick={() => this.showResetPwd(record)}>重置</a>;
              }
              if (h === 'auth') {
                return <a onClick={() => this.showAuthModal(record)}>权限</a>;
              }
            }),
          ]}
        </OptionButtons>
        {/* {dividerShow ? <Divider type="vertical" /> : ''} */}
        {/* {moreBtn.length > 0 ? this.renderMoreBtn(record, moreBtn) : ''} */}
      </React.Fragment>
    );
  };

  public renderTable() {
    const {
      dataCount,
      staffList,
      loading,
      onAllCheckChanged,
      permissions,
      accountInfo,
    } = this.props;
    const versionsCode = accountInfo.get('versionsCode');
    const permissionArray = [];
    permissions.toArray().map(cur => {
      return permissionArray.push(cur.get('operateKey'));
    });
    const moreBtn = permissionArray.filter(permission => permission !== 'edit');
    const dividerShow = permissionArray.includes('edit') && moreBtn.length > 0;
    const { current } = this.state;
    const { openAuthModal, authStaff } = this.state;
    const staffArray = staffList.map(item => item.toObject()).toArray();
    const tableProps = {
      columns: [
        {
          width: 40,
          key: 'checkbox',
          render: (record: any) => {
            if (permissionArray.includes('auth') && record.status === 1) {
              return (
                <CheckBox
                  checked={record.isChecked}
                  onChange={() => this.props.onCheckedChanged(record)}
                />
              );
            }
          },
        },
        {
          title: '用户名',
          dataIndex: 'userName',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '姓名',
          dataIndex: 'realName',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '手机号',
          dataIndex: 'mobileNumber',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '部门',
          dataIndex: 'departmentNames',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '岗位',
          dataIndex: 'postNames',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '操作',
          key: 'action',
          dataIndex: 'action',
          render: (item, record) => (
            <React.Fragment>
              {record.status === 1 ? (
                this.renderStaffOperation(
                  item,
                  record,
                  permissionArray,
                  dividerShow,
                  moreBtn,
                )
              ) : (
                <Popconfirm
                  title="确认要启用该员工？"
                  onConfirm={() => this.confirmStatus(record)}
                >
                  <a href="javascript:;">启用</a>
                </Popconfirm>
              )}
            </React.Fragment>
          ),
        },
      ],
      dataSource: staffList
        .map(d => ({
          userName: d.get('userName'),
          realName: d.get('realName'),
          mobileNumber: d.get('mobileNumber'),
          departmentNames: d.get('departmentNames'),
          postNames: d.get('postNames'),
          isChecked: d.get('isChecked'),
          utmsUsersId: d.get('utmsUsersId'),
          status: d.get('status'),
        }))
        .toArray(),
      loading,
      pagination: {
        current,
        total: dataCount,
        onChange: this.paginationChange,
        onShowSizeChange: this.paginationChange,
      },
      rowKey: 'userName',
    };
    const checkItemLength = staffArray.filter(item => item.isChecked).length;
    const enableItemLength = staffArray.filter(item => item.status === 1)
      .length;
    const batchAuthDisable = checkItemLength < 1;

    return (
      <React.Fragment>
        <MyTable {...tableProps} />
        {permissionArray.includes('auth') ? (
          <PageBottom
            leftChild={
              <PageBottomCheck
                total={enableItemLength}
                checked={checkItemLength}
                onAllCheckChanged={onAllCheckChanged}
              />
            }
            rightChild={
              <PageBottomButtons
                buttons={[
                  <Button
                    key="auth"
                    type="primary"
                    disabled={batchAuthDisable}
                    onClick={() => this.showAuthModal()}
                  >
                    批量分配权限
                  </Button>,
                ]}
              />
            }
          />
        ) : (
          ''
        )}

        <StaffAuthorizationSettingModal
          versionsCode={versionsCode}
          visible={openAuthModal}
          onClose={this.handleCancel}
          onOk={this.handleCancel}
          staff={authStaff.toArray()}
        />
      </React.Fragment>
    );
  }

  public renderContent() {
    return (
      <Card>
        {this.renderSearchParams()}
        {this.renderTable()}
      </Card>
    );
  }

  public render() {
    const { companyTree, pickStaff } = this.props;
    const { selectedKeys, selectedNode } = this.state;
    const selectedKeysProp =
      selectedKeys.size > 0
        ? selectedKeys
        : companyTree.get(0)
          ? List([companyTree.get(0).get('utmsOrganizationId') + ''])
          : List();
    const selectedNodeProp =
      selectedNode.length > 0
        ? selectedNode
        : companyTree.get(0)
          ? companyTree.get(0).get('organizationCode')
          : '';
    return (
      <React.Fragment>
        <Layout className={'staffLayout'}>
          <SiderCompanyTree
            companyTree={companyTree}
            handleSelect={this.handleSelect}
            selectedKeys={selectedKeysProp}
          />
          <Layout style={{ background: '#fff' }}>
            <Content>{this.renderContent()}</Content>
          </Layout>
        </Layout>
        <EditStaffModal
          formValue={fromJS(pickStaff)}
          visible={this.state.openModal}
          onConfirm={this.handleEditStaff}
          onClose={this.handleCloseModal}
          refreshDictionary={this.refreshDictionary}
          companyTree={companyTree}
          selectedNode={selectedNodeProp}
        />
      </React.Fragment>
    );
  }

  private hanldUsers = record => {
    let selectedStaff = List();
    if (record) {
      selectedStaff = selectedStaff.push(record);
    } else {
      const { staffList } = this.props;
      selectedStaff = staffList
        .filter(staff => {
          return staff.get('isChecked');
        })
        .map(staff => {
          return staff.toObject();
        });
    }
    this.setState({
      authStaff: selectedStaff,
    });
  };

  private showAuthModal = (record?) => {
    this.hanldUsers(record);
    this.setState({
      openAuthModal: true,
    });
  };

  private handleCancel = () => {
    this.setState({
      openAuthModal: false,
    });
  };
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        companyTree: makeSelectCompanyTree(currentState),
        staffList: makeSelectStaffList(currentState),
        pickStaff: makePickStaff(currentState),
        // userData: makeSelectUserData(currentState),
        dataCount: makeSelectDataCount(currentState),
        accountInfo: makeSelectAccountInfo(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getCompanyTree: () => dispatch(actions.getCompanyTree()),
        getStaffList: options => dispatch(actions.getStaffList(options)),
        onAllCheckChanged: checked =>
          dispatch(actions.allCheckedChanged(checked)),
        onCheckedChanged: (record: any) =>
          dispatch(actions.checkedChanged(record)),
        getStaffDetails: (record: string) =>
          dispatch(actions.getStaffDetails(record)),
        resetStaffDetails: () => dispatch(actions.resetStaffDetails()),
      }),
    };
  })(CompanyStaff);
};
