import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_SAVE_AUTHORIZATION_SETTING =
  '/ehuodiGateway/utmsCore/utmsUserscs/updateUtmsUsersRsc';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码

  mocker
    .on('post', API_SAVE_AUTHORIZATION_SETTING)
    .reply(() => import('./mock/save').then(exports => [200, exports.default]));
}

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const saveAuthorizationSetting = options => {
  return request(
    {
      method: 'post',
      url: API_SAVE_AUTHORIZATION_SETTING,
      data: {
        ...options,
      },
    },
    {
      useMock: USE_MOCK,
    },
  );
};
