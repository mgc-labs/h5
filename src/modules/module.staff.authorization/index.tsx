/**
 * StaffAuthorization Component
 * @author yanrong.tian
 * @date 2018-9-19 13:41:45
 */
import {
  Button,
  Card,
  Col,
  Divider,
  Icon,
  Input,
  message,
  Popconfirm,
  Row,
  Table,
  Tag,
} from 'antd';
import { PageBottom } from 'components/PageBottom';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { selectStateFn } from './selectors';

import StaffAuthorizationSettingModal from 'components/StaffAuthorizationSettingModal';

import styles from './index.module.less';

interface IProps {}
interface IState {
  visible: boolean;
  staff: any[];
}

class StaffAuthorization extends React.PureComponent<IProps> {
  state: IState;
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      staff: [],
    };
  }
  public render() {
    const { visible } = this.state;
    return (
      <div>
        <Button onClick={this.showModal}>显示</Button>
        <StaffAuthorizationSettingModal
          visible={visible}
          onCancel={this.handleCancel}
          onOk={this.handleCancel}
          staff={this.state.staff}
        />
      </div>
    );
  }

  // 给一个员工分配权限，标记出之前已分配的权限；
  // 给一个以上的员工分配权限，不标记之前已分配的权限；
  private hanldUsers = () => {
    this.count = this.count || 0;

    const data = [
      {
        utmsUsersId: '44',
        userName: '15119',
        realName: '郭井阳',
        mobileNumber: '15968425614',
        departmentNames: '运营部',
        postNames: '调度',
      },
      {
        utmsUsersId: '45',
        userName: '17733',
        realName: '王红',
        mobileNumber: '15968425614',
        departmentNames: '运营部',
        postNames: '调度',
      },
    ];
    const newData = [];
    data.map((item, i) => {
      if (this.count <= i) {
        newData.push(item);
      }
    });
    this.setState({
      staff: newData,
    });
    this.count = this.count + 1;
  };

  private showModal = () => {
    this.hanldUsers();
    this.setState({
      visible: true,
    });
  };

  private handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    return {
      mapStateToProps: createStructuredSelector({
        // selectState: (storeKey: string) => selectStateFn(routeId, storeKey),
      }),
      mapDispatchToProps: dispatch => ({
        // actions
      }),
    };
  })(StaffAuthorization);
};
