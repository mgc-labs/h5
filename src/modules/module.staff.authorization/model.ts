/**
 * StaffAuthorization Saga
 * @author yanrong.tian
 * @date 2018-9-19 13:41:45
 */
import message from 'antd/es/message';
import { fromJS, List, Map } from 'immutable';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

// mport { getSchedule } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      // state
    }),
    actions: {
      // actions
    },
    effects(CONSTANTS, actions) {
      return function* rootSaga() {
        // effects
      };
    },
    reducers: {
      // reducers
    },
  });
