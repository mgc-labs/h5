/**
 * StaffAuthorization selectors
 * @author yanrong.tian
 * @date 2018-9-19 13:41:45
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const selectStateFn = (routeId, storeKey) => {
  const selectState = state => state.get(routeId);
  return createSelector(selectState, (state: Map<string, any>) =>
    state.get(storeKey),
  );
};
