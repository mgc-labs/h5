import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';
import Radio from 'antd/es/radio';
import RadioGroup from 'antd/es/radio/group';
import Row from 'antd/es/row';
import Tooltip from 'antd/es/tooltip';
import * as React from 'react';
import {
  UP_100000_TOW_DECIMAL_OBJ,
  UP_100_TOW_DECIMAL_OBJ,
  UP_500_NUMBER_OBJ,
} from 'utils/commonRegExp';

const gutter = 16;
const colLayProps = {
  sm: 24,
  xs: 24,
  md: 6,
  lg: 6,
  xl: 6,
};
const FormItem = Form.Item;
export interface IAddOrderCommonInfoProps {
  children?: React.ReactChildren;
  form: WrappedFormUtils;
}
interface IAddOrderCommonInfoState {}
class AddOrderCommonInfo extends React.PureComponent<
  IAddOrderCommonInfoProps,
  IAddOrderCommonInfoState
> {
  render() {
    return (
      <div>
        {this.renderGoodsInfo()}
        {this.renderServiceInfo()}
        {this.renderCostInfo()}
      </div>
    );
  }

  // 货物信息
  private renderGoodsInfo() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Card title="货物信息" style={{ marginTop: 10 }}>
        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem label="货物名称" required>
              {getFieldDecorator('goodsName', {
                rules: [
                  {
                    required: true,
                    message: '请输入货物名称',
                  },
                  {
                    min: 2,
                    message: '货物名称最少为2个字',
                  },
                  {
                    max: 200,
                    message: '货物名称最大为200个字',
                  },
                ],
              })(
                <Input
                  placeholder="请输入货物名称"
                  minLength={2}
                  maxLength={200}
                />,
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem label="重量(千克)" required>
              {getFieldDecorator('goodsWeight', {
                rules: [
                  {
                    required: true,
                    message: '请输入重量',
                  },
                  UP_100000_TOW_DECIMAL_OBJ,
                ],
              })(<Input placeholder="请输入重量" />)}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="体积(方)" required>
              {getFieldDecorator('goodsVolume', {
                rules: [
                  {
                    required: true,
                    message: '请输入体积',
                  },
                  UP_100_TOW_DECIMAL_OBJ,
                ],
              })(<Input placeholder="请输入体积" />)}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="件数(件)" required>
              {getFieldDecorator('goodsNumber', {
                rules: [
                  {
                    required: true,
                    message: '请输入件数',
                  },
                  UP_500_NUMBER_OBJ,
                ],
              })(<Input placeholder="请输入件数" />)}
            </FormItem>
          </Col>
        </Row>
      </Card>
    );
  }

  private renderServiceInfo() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Card title="增值服务" style={{ marginTop: 10 }}>
        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem label="回单">
              {getFieldDecorator('isReceipt', { initialValue: '1' })(
                <RadioGroup>
                  <Radio value="1">有</Radio>
                  <Radio value="0">无</Radio>
                </RadioGroup>,
              )}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="代收(元)">
              {getFieldDecorator('collictionAmount', {
                rules: [UP_100000_TOW_DECIMAL_OBJ],
              })(<Input placeholder="请输入代收" />)}
            </FormItem>
          </Col>
        </Row>
      </Card>
    );
  }

  // 运费信息
  private renderCostInfo() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Card title="运费信息" style={{ marginTop: 10 }}>
        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem
              label={
                <span>
                  客户运费(元)&nbsp;
                  <Tooltip title="上游客户结算的运费，便于进行后期对账和收入分析">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              }
            >
              {getFieldDecorator('customerFreight', {
                rules: [UP_100000_TOW_DECIMAL_OBJ],
              })(<Input placeholder="请输入客户运费" />)}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem
              label={
                <span>
                  司机运费(元)&nbsp;
                  <Tooltip title="结算给司机的运费，便于进行后期对账和支出分析">
                    <Icon type="question-circle-o" />
                  </Tooltip>
                </span>
              }
            >
              {getFieldDecorator('driverFreight', {
                rules: [UP_100000_TOW_DECIMAL_OBJ],
              })(<Input placeholder="请输入司机运费" />)}
            </FormItem>
          </Col>
        </Row>
      </Card>
    );
  }
}

export default AddOrderCommonInfo;
