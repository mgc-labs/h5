export default {
  result: 'success',
  count: 1,
  data: {
    orderWholeCarId: 1,
    gmtCreate: '2018-09-15 HH:mm:ss',
    gmtModified: '2018-09-15 HH:mm:ss',
    thirdSystemId: '213123',
    orderNumber: '213123',
    tradeNumber: 11,
    status: 1,
    carStruct: '厢式货车',
    carLong: 8500,
    useCarDate: '2018-09-15 HH:mm:ss',
    requireDate: '2018-09-15 HH:mm:ss',
    estimatedTransportTime: 500,
    remark: '21啊实打实',
    goodsName: '苹果',
    goodsWeight: 12.543,
    goodsVolume: 545.442,
    goodsNumber: 1454,
    driverFreight: 12.22,
    customerFreight: 45.55,
    isReceipt: 0,
    collictionAmount: null,
    isDeleted: 0,
    applicationFlag: 'asdasd',
    organizationCode: '12321asdas',
    organizationName: '中原野战队',
    customerCode: 'sadas',
    customerName: '啊实打实多',
    dataSource: 0,
    loadList: [
      {
        loadInfoId: 1,
        orderWholeCarId: 11,
        longitude: 12.32,
        latitude: 123.255,
        loadType: 0,
        addressId: 1,
        pointAddress: '太平洋东海龙宫',
        contact: '小B',
        phone: 13854647875,
        isDeleted: 0,
      },
      {
        loadInfoId: 2,
        orderWholeCarId: 11,
        longitude: 12.32,
        latitude: 123.255,
        loadType: 1,
        addressId: 1,
        pointAddress: '太平洋东海龙宫',
        contact: '小B',
        phone: 13854647875,
        isDeleted: 0,
      },
      {
        loadInfoId: 3,
        orderWholeCarId: 11,
        longitude: 12.32,
        latitude: 123.255,
        loadType: 1,
        addressId: 1,
        pointAddress: '太平洋东海龙宫',
        contact: '小B',
        phone: 13854647875,
        isDeleted: 0,
      },
    ],
  },
};
