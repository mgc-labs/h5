/**
 * VehicleOrderAdd Component
 * @author lhf
 * @date 2018-9-17 09:58:49
 */
import * as React from 'react';
import { createStructuredSelector } from 'reselect';

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Spin from 'antd/es/spin';
import CarModelCarLong from 'components/CarModelCarLong';
import CustomerSelect from 'components/CustomerSelect';
import LoadInfoEdit from 'components/LoadInfoEdit';
import { PageBottom, PageBottomButtons } from 'components/PageBottom';
import TextArea from 'components/TextArea';
import { Map } from 'immutable';
import moment from 'moment';
import msngr from 'msngr';
import AddOrderCommonInfo from './AddOrderCommonInfo';
import {
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
  makeSelectSaveData,
  makeSelectSendData,
} from './selectors';

import styles from './index.module.less';

const FormItem = Form.Item;
const confirm = Modal.confirm;
export interface IVehicleOrderAddProps {
  loading: boolean;
  children: React.ReactChildren;
  form: WrappedFormUtils;
  orderDetail: Map<string, any>;
  saveData: string; // 是否保存成功
  sendData: any; // 发货结果
  searchParams: {
    id: number;
  };
  getOrder: (routeId: number) => any;
  saveOrderConfig: (data) => void;
  toList: (handleClose?) => void;
  close: () => void;
  sendGood: (data) => void;
  refresh: (data?) => void;
  eventEmitter: object;
}
interface IVehicleOrderAddState {
  showAddress: boolean;
  loadType: number; // load/unload，当前选择的是装还是卸的地址
  loadIndex: number; // 装卸的index
  loadData: {};
  unloadData: object[];
  selectCustomer: string; // 记录默认客户
}
const gutter = 16;
const colLayProps = {
  sm: 24,
  xs: 24,
  md: 6,
  lg: 6,
  xl: 6,
};
class VehicleOrderAdd extends React.PureComponent<
  IVehicleOrderAddProps,
  IVehicleOrderAddState
> {
  selectedAddress: any;
  constructor(props) {
    super(props);
    this.state = {
      showAddress: false,
      loadIndex: 0,
      loadType: 0,
      loadData: {},
      unloadData: [],
      selectCustomer: '',
    };
  }
  public componentDidMount() {
    const { searchParams } = this.props;
    if (searchParams.id) {
      this.props.getOrder(Number(searchParams.id));
    }
    this.props.eventEmitter.on('close', (requestClose, eventSource) => {
      if (
        eventSource === 'FROM_TAB_CLOSE' ||
        eventSource === 'FROM_TAB_REFRESH'
      ) {
        this.cancleEdit(requestClose);
      } else {
        requestClose();
      }
    });
  }
  public componentWillReceiveProps(nextProps) {
    if (nextProps.saveData && nextProps.saveData !== this.props.saveData) {
      this.sendGoods(nextProps.saveData);
    }
    if (
      nextProps.orderDetail &&
      nextProps.orderDetail !== this.props.orderDetail
    ) {
      this.setFormData(nextProps);
    }
  }
  public render() {
    const { loading } = this.props;
    const { id } = this.props.searchParams;
    return (
      <div
        className={styles.VehicleOrderAdd}
        onSubmit={e => {
          this.onHandleSubmit(e);
        }}
      >
        <Spin spinning={loading}>
          <Form layout="horizontal">
            {this.renderBasicInfo()}
            <AddOrderCommonInfo form={this.props.form} />
            <PageBottom
              rightChild={
                <PageBottomButtons
                  buttons={[
                    <Button
                      key={1}
                      onClick={() => {
                        this.cancleEdit();
                      }}
                    >
                      取消
                    </Button>,
                    <Button key={2} type="primary" htmlType="submit">
                      {id ? '保存' : '确认开单'}
                    </Button>,
                  ]}
                />
              }
            />
          </Form>
        </Spin>
      </div>
    );
  }

  private renderBasicInfo() {
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    const { searchParams, orderDetail } = this.props;
    const { loadData, unloadData } = this.state;
    // 装卸货地址
    const loadInfoProps = {
      closeAddressModal: (data, formData, loadtype) => {
        this.closeAddressModal(data, formData, loadtype);
      },
      showAddressModal: (type, index) => {
        this.showAddressModal(type, index);
      },
      form: this.props.form,
    };
    // 时间控件
    const datePickerProps = {
      showTime: { format: 'HH:mm', hideDisabledOptions: true, minuteStep: 10 },
      format: 'YYYY-MM-DD HH:mm',
      style: { width: '100%' },
      disabledDate: current => {
        return current < moment();
      },
      disabledTime: current => {
        return {
          disabledHours: () =>
            moment(current).isSame(moment(), 'day')
              ? this.range(
                  0,
                  moment()
                    .add(2, 'h')
                    .hour(),
                )
              : [],
          disabledMinutes: () =>
            moment(current).isSame(moment().add(2, 'h'), 'hour')
              ? this.range(0, moment().minute())
              : [],
          disabledSeconds: () => [],
        };
      },
    };
    return (
      <Card title="基本信息">
        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem label="" type="hidden" style={{ margin: 0 }}>
              {getFieldDecorator('customerCode')(<Input type="hidden" />)}
            </FormItem>
            <FormItem label="选择客户">
              {getFieldDecorator('customerName')(
                <CustomerSelect
                  disabled={searchParams.id ? true : false}
                  onChange={this.onChangeCustomer}
                />,
              )}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="第三方订单号">
              {getFieldDecorator('thirdSystemId')(
                <Input
                  placeholder="请输入第三方订单号"
                  disabled={searchParams.id ? true : false}
                  maxLength={30}
                />,
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem label="所需车型" required>
              {getFieldDecorator('carStruct', {
                rules: [
                  {
                    required: true,
                    message: '请选择车型',
                  },
                ],
              })(<CarModelCarLong placeholder="请选择车型" maxSelect={1} />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem label="用车时间" required>
              {getFieldDecorator('useCarDate', {
                initialValue: moment().add(2, 'h'),
                rules: [
                  {
                    required: true,
                    message: '请选择用车时间',
                  },
                ],
              })(
                <DatePicker
                  {...datePickerProps}
                  onChange={(a, b) => {
                    if (moment(a).isBefore(moment().add(2, 'h'), 'minute')) {
                      setTimeout(() => {
                        setFieldsValue({
                          useCarDate: moment(a).add(2, 'h'),
                        });
                      }, 0);
                    }
                  }}
                />,
              )}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="要求送达时间">
              {getFieldDecorator('requireDate')(
                <DatePicker
                  {...datePickerProps}
                  disabledDate={current => current < moment().add(2, 'h')}
                  showToday={false}
                />,
              )}
            </FormItem>
          </Col>
        </Row>

        <LoadInfoEdit
          fields={{ name: 'load', value: [loadData] }}
          loadtype={0}
          {...loadInfoProps}
        />

        <LoadInfoEdit
          fields={{ name: 'unload', value: unloadData }}
          loadtype={1}
          {...loadInfoProps}
          onDele={key => {
            this.deleUnloadData(key);
          }}
        />

        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem label="备注">
              {getFieldDecorator('remark')(
                <TextArea maxLength={500} placeholder="请输入备注" />,
              )}
            </FormItem>
          </Col>
        </Row>
      </Card>
    );
  }

  private onChangeCustomer = data => {
    const { selectCustomer } = this.state;
    if (selectCustomer === '') {
      this.setState({ selectCustomer: data });
    }
  };

  // 时间选择可选范围
  private range(start, end) {
    const result = [];
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    return result;
  }

  // 设置当前loadtype和index
  private showAddressModal(loadType: number, index) {
    this.setState({ loadType, loadIndex: index });
  }
  // 关闭常用地址弹出框，填充数据
  private closeAddressModal(data, formData, loadType) {
    const { form } = this.props;
    const { loadIndex } = this.state;
    if (loadType === 0) {
      form.setFieldsValue({
        load: [formData],
      });
      this.setState({ loadData: data });
    } else if (loadType === 1) {
      const unloadData2 = this.state.unloadData;
      unloadData2[loadIndex] = data;
      const unloadData = form.getFieldValue('unload');
      unloadData[loadIndex] = formData;
      form.setFieldsValue({
        unload: unloadData,
      });
      this.setState({ unloadData: unloadData2 });
    }
  }

  // 删除卸货信息
  private deleUnloadData(key) {
    const { unloadData } = this.state;
    unloadData[key] = null;
    this.setState({ unloadData });
  }

  // 取消页面编辑
  private cancleEdit(requestClose?) {
    const { form, searchParams, orderDetail } = this.props;
    const { selectCustomer } = this.state;
    const keys = form.getFieldValue('unloadKeys');
    const comparativeFields = [
      'thirdSystemId',
      'useCarDate',
      'requireDate',
      'remark',
      'load',
      'load[0].pointAddress',
      'load[0].contact',
      'load[0].phone',
      'unload',
      'goodsName',
      'goodsWeight',
      'goodsVolume',
      'goodsNumber',
      'isReceipt',
      'collictionAmount',
      'customerFreight',
      'driverFreight',
    ];
    keys.map(d => {
      comparativeFields.push(`unload[${d}].pointAddress`);
      comparativeFields.push(`unload[${d}].contact`);
      comparativeFields.push(`unload[${d}].phone`);
    });
    let showConfirm = false; // 判断是否需要触发离开提示窗口
    if (!searchParams.id) {
      comparativeFields.push('carStruct');
      if (
        selectCustomer !== form.getFieldValue('customerName') ||
        form.isFieldsTouched(comparativeFields)
      ) {
        showConfirm = true;
      }
    } else {
      const propsCar =
        orderDetail.get('carLong') / 1000 + '米' + orderDetail.get('carStruct');
      if (
        form.isFieldsTouched(comparativeFields) ||
        propsCar !== form.getFieldValue('carStruct')[0].value
      ) {
        showConfirm = true;
      }
    }
    if (showConfirm) {
      confirm({
        title: '有未保存的信息',
        content: '如放弃，填写的信息将丢失',
        okText: '继续填写',
        cancelText: '放弃',
        centered: true,
        onCancel: () => {
          if (requestClose) {
            requestClose();
          } else {
            this.toList();
          }
        },
      });
    } else {
      if (requestClose) {
        requestClose();
      } else {
        this.toList();
      }
    }
  }

  // 开单成功之后
  private sendGoods(id) {
    const isEdit = this.props.searchParams.id ? true : false;
    confirm({
      title: `${isEdit ? '修改' : '开单'}成功`,
      content: `整车${isEdit ? '修改' : '开单'}成功，是否直接发货，调度司机。`,
      okText: '完成发货',
      cancelText: '晚点再说',
      iconType: 'check-circle',
      centered: true,
      onOk: () => {
        this.props.sendGood({
          orderWholeCarId: id,
          handleClose: this.props.close,
        });
      },
      onCancel: () => {
        this.toList();
      },
    });
  }

  // 跳转List页面并刷新
  private toList() {
    this.props.toList();
    msngr('/vehicleOrder/list', 'refresh').emit(true);
    this.props.close();
  }

  private onHandleSubmit(e) {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((errors, values) => {
      if (!errors) {
        const data = this.getSubmitData();
        this.props.saveOrderConfig(data);
      }
    });
  }
  // 获取提交的数据
  private getSubmitData() {
    const { loadData, unloadData } = this.state;
    const { getFieldsValue } = this.props.form;
    const formData = getFieldsValue();
    const points = [Object.assign({}, formData.load[0], loadData)].concat(
      unloadData
        .map((value, index) => {
          if (
            value &&
            value.pointAddress &&
            formData.unload[index] &&
            formData.unload[index].pointAddress
          ) {
            return Object.assign({}, value, formData.unload[index]);
          }
        })
        .filter(v => v && v.pointAddress),
    );

    const customer = formData.customerName;
    const data = Object.assign({}, formData, {
      useCarDate: formData.useCarDate
        ? formData.useCarDate.format('YYYY-MM-DD HH:mm:00')
        : '',
      requireDate: formData.requireDate
        ? formData.requireDate.format('YYYY-MM-DD HH:mm:00')
        : '',
      carLong: formData.carStruct[0].vehicleLong,
      carStruct: formData.carStruct[0].vehicleType,
      loadList: points,
    });
    delete data.load;
    delete data.unload;
    delete data.unloadKeys;

    if (customer && customer.indexOf(',') > 0) {
      return Object.assign(data, {
        customerCode: customer.split(',')[0],
        customerName: customer.split(',')[1],
      });
    } else {
      return data;
    }
  }

  // 修改的情况下，设置formdata
  private setFormData(props) {
    const { orderDetail, form } = props;
    const unload = orderDetail
      .get('loadList')
      .filter(d => d.get('loadType') === 1 || d.get('loadType') === '1')
      .toJS();
    const load = orderDetail
      .get('loadList')
      .filter(d => d.get('loadType') === 0 || d.get('loadType') === '0')
      .toJS()[0];
    const carString =
      orderDetail.get('carLong') / 1000 + '米' + orderDetail.get('carStruct');
    if (orderDetail.size) {
      form.setFieldsValue({
        customerName: orderDetail.get('customerName'),
        customerCode: orderDetail.get('customerCode'),
        thirdSystemId: orderDetail.get('thirdSystemId'),
        carStruct: [{ value: carString }],
        useCarDate: orderDetail.get('useCarDate')
          ? moment(orderDetail.get('useCarDate'))
          : moment().add(2, 'h'),
        requireDate: orderDetail.get('requireDate')
          ? moment(orderDetail.get('requireDate'))
          : '',
        goodsName: orderDetail.get('goodsName'),
        goodsNumber: orderDetail.get('goodsNumber'),
        goodsVolume: orderDetail.get('goodsVolume'),
        goodsWeight: orderDetail.get('goodsWeight'),
        remark: orderDetail.get('remark'),
        isReceipt: orderDetail.get('isReceipt').toString(),
        customerFreight: orderDetail.get('customerFreight'),
        driverFreight: orderDetail.get('driverFreight'),
        collictionAmount: orderDetail.get('collictionAmount'),
        unloadKeys: unload.map((data, index) => index),
        load: [
          {
            pointAddress: load.pointAddress,
            phone: load.phone,
            contact: load.contact,
          },
        ],
      });
      setTimeout(() => {
        form.setFieldsValue({
          unload: unload.map(data => ({
            pointAddress: data.pointAddress,
            phone: data.phone,
            contact: data.contact,
          })),
        });
      }, 0);
      this.setState({
        loadData: load,
        unloadData: unload,
      });
    }
  }
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        orderDetail: makeSelectData(currentState),
        saveData: makeSelectSaveData(currentState),
        sendData: makeSelectSendData(currentState),
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getOrder: options => dispatch(actions.getOrder(options)),
        saveOrderConfig: options => dispatch(actions.saveOrderConfig(options)),
        toList: options => dispatch(actions.toList(options)),
        sendGood: data => dispatch(actions.sendGood(data)),
      }),
    };
  })(Form.create()(VehicleOrderAdd));
};
