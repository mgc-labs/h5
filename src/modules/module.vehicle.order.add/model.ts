import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { fromJS, List, Map } from 'immutable';
import msngr from 'msngr';
import { delay } from 'redux-saga';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';
import { sendGood } from '../module.vehicle.order/service';
import { addOrder, getData, updateOrder } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: Map(),
      error: false,
      loading: false,
      saveData: '',
      sendData: '',
    }),
    actions: {
      GET_ORDER: orderWholeCarId => orderWholeCarId,
      GET_ORDER_ERROR: error => error,
      GET_ORDER_SUCCESS: data => data,
      SAVE_ORDER_CONFIG: data => data,
      SAVE_ORDER_CONFIG_SUCCESS: data => data,
      SAVE_ORDER_CONFIG_ERROR: error => error,
      SEND_GOOD: data => data,
      OPERA_DONE: data => data,
      TO_LIST: handleClose => handleClose,
    },
    reducers: {
      GET_ORDER(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_ORDER_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_ORDER_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data));
      },
      SAVE_ORDER_CONFIG(state, action) {
        return state.set('loading', true).set('saveData', '');
      },
      SAVE_ORDER_CONFIG_SUCCESS(state, action) {
        return state.set('loading', false).set('saveData', action.payload);
      },
      SAVE_ORDER_CONFIG_ERROR(state, action) {
        return state.set('loading', false).set('error', action.payload);
      },
      SEND_GOOD(state, action) {
        return state.set('error', false).set('loading', true);
      },
      OPERA_DONE(state, action) {
        return state
          .set('error', action.error)
          .set('loading', false)
          .set('sendData', action.payload);
      },
      TO_LIST(state) {
        return state
          .set('error', false)
          .set('loading', false)
          .set('saveData', '');
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const data = yield call(getData, { orderWholeCarId: action.payload });
          yield put(actions.getOrderSuccess(data));
        } catch (err) {
          yield put(actions.getOrderError(err));
        }
      }

      // 提交form
      function* submitOrder(action) {
        const data = yield select(state => state.getIn([namespace, 'data']));
        const orderWholeCarId = data.get('orderWholeCarId');
        const updateData = Object.assign({ orderWholeCarId }, action.payload);
        try {
          const result: any = orderWholeCarId
            ? yield call(updateOrder, updateData)
            : yield call(addOrder, action.payload);
          if (result.result === 'success') {
            yield put(
              actions.saveOrderConfigSuccess(result.data.orderWholeCarId),
            );
          }
        } catch (err) {
          yield put(actions.saveOrderConfigError(err));
        }
      }

      // 发货
      function* sendGoods(action) {
        try {
          const { handleClose, ...others } = action.payload;
          const data = yield call(sendGood, others);
          if (data.code) {
            message.error(data.msg);
          } else {
            message.success(data.msg);
          }
          yield put(actions.operaDone(data));
          yield toList();
          msngr('/vehicleOrder/list', 'refresh').emit(true);
          setTimeout(() => {
            handleClose();
          }, 200);
        } catch (err) {
          yield put(actions.operaDone(err));
        }
      }

      function* toList(action) {
        yield put(push('/vehicleOrder/list'));
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ORDER, fetchData);
        yield takeLatest(CONSTANTS.SAVE_ORDER_CONFIG, submitOrder);
        yield takeLatest(CONSTANTS.SEND_GOOD, sendGoods);
        yield takeLatest(CONSTANTS.TO_LIST, toList);
      };
    },
  });
