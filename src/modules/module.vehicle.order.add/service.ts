/**
 * fixpath.list Service
 * @author ryan bian
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const ADD_ORDER = '/ehuodiGateway/utmsTrade/orderWholeCar/addOrder';
const UPDATE_ORDER = '/ehuodiGateway/utmsTrade/orderWholeCar/update';
const GET_ORDER = '/ehuodiGateway/utmsTrade/orderWholeCar/selectDetail?';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', GET_ORDER)
    .reply(() => import('./mock/mock').then(exports => [200, exports.default]));
}

// 获取详情
export const getData = data => {
  return request(
    {
      method: 'post',
      url: GET_ORDER,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
// 新增开单
export const addOrder = data => {
  return request(
    {
      method: 'post',
      url: ADD_ORDER,
      data,
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
// 更新开单
export const updateOrder = data => {
  return request(
    {
      method: 'post',
      url: UPDATE_ORDER,
      data: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
