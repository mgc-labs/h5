/**
 * VehicleOrderAdd selectors
 * @author lhf
 * @date 2018-9-17 09:58:49
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );
export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectSaveData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('saveData'),
  );
export const makeSelectSendData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('sendData'),
  );
