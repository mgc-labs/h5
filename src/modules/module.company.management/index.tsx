/**
 * CompanyManagement Component
 * @author HuangSiFei
 * @date 2018-9-14 11:05:19
 */
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Divider from 'antd/es/divider';
import Layout from 'antd/es/layout';
import message from 'antd/es/message';
import Modal from 'antd/es/modal';
import Popconfirm from 'antd/es/popconfirm';
import Table from 'antd/es/table';
import MyTable from 'components/MyTable';
import SiderCompanyTree from 'components/SiderCompanyTree';
import { fromJS, List, Map } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import GLOBAL from 'utils/GLOBAL';
import EditCompanyModal from './EditCompanyModal';
import {
  addCompany,
  deleteCompany,
  orderCompany,
  updateCompany,
} from './service';

import {
  makeSelectCompany,
  makeSelectData,
  makeSelectDataCount,
  makeSelectError,
  makeSelectList,
  makeSelectLoading,
  makeSelectPageIndex,
} from './selectors';

import './index.less';
import styles from './index.module.less';

const { Content } = Layout;

export interface ICompanyManagementProps {
  children: React.ReactChildren;
  companyTree: List<Map<string, any>>;
  companyList: List<Map<string, any>>;
  loading: boolean;
  pageIndex: number;
  dataCount: number;
  selectedCompany: Map<string, any>;
  getCompanyTree: () => any;
  doSearch: (params) => any;
  selectCompany: (data?: object) => any;
  updateCompany: (company) => void;
  addCompany: (company) => void;
}

interface ICompanyManagementState {
  openModal: boolean;
  hideBranchTable: boolean;
  pickCompany: Map<string, any>;
  skipCount: number;
  pageSize: number;
  current: number;
}

class CompanyManagement extends React.PureComponent<
  ICompanyManagementProps,
  ICompanyManagementState
> {
  constructor(props) {
    super(props);
    this.state = {
      hideBranchTable: false,
      openModal: false,
      pickCompany: Map(),
      skipCount: 0,
      pageSize: 15,
      current: 1,
    };
  }
  public paginationChange = params => {
    const { skipCount, pageSize, current } = params;
    this.setState({
      skipCount,
      pageSize,
      current,
    });
    this.doSearch(params);
  };
  public doSearch = (params?: any) => {
    this.props.doSearch(
      Object.assign(
        {
          pageSize: this.state.pageSize,
          skipCount: this.state.skipCount,
        },
        params,
      ),
    );
  };
  public componentDidMount() {
    this.props.getCompanyTree();
    this.doSearch();
  }

  public handleSelect = selected => {
    this.setState({
      hideBranchTable: selected.get('organizationType') === '分公司',
      current: 1,
    });
    this.props.selectCompany(selected);
    if (selected.get('organizationType') === '总公司') {
      this.doSearch({
        organizationCode: selected.get('organizationCode'),
        skipCount: 0,
      });
    }
  };

  public renderContent() {
    return (
      <Card className={'companyCard'}>
        {this.renderHeaderTable()}
        {this.renderBranchTable()}
      </Card>
    );
  }

  public handleOpenModal = (company?) => {
    this.setState({
      openModal: true,
      pickCompany: company ? fromJS(company) : Map(),
    });
  };
  public handleCloseModal = () => {
    this.setState({
      openModal: false,
    });
  };
  public handleEditCompany = async values => {
    if (values.utmsOrganizationId) {
      const result = await updateCompany(values);
      if (result.result === 'success') {
        message.success('修改公司成功');
        this.setState({
          openModal: false,
        });
        this.doSearch();
        this.props.getCompanyTree();
      } else {
        message.error('修改公司失败');
      }
    } else {
      const result = await addCompany(values);
      if (result.result === 'success') {
        message.success('新增公司成功');
        this.setState({
          openModal: false,
        });
        this.doSearch();
        this.props.getCompanyTree();
      } else {
        message.error('新增公司失败');
      }
    }
  };

  public handleDeleteCompany = async key => {
    const result = await deleteCompany({ utmsOrganizationId: key });
    if (result.result === 'success') {
      message.success('删除公司成功');
      this.setState({
        openModal: false,
        hideBranchTable: false,
      });
      this.doSearch();
      this.props.getCompanyTree();
    } else {
      if (result.code === 'UTMS001020200100') {
        Modal.error({
          title: '无法删除',
          content: result.msg,
        });
      } else {
        message.error('删除公司失败');
      }
    }
  };

  public renderHeaderTable = () => {
    const { selectedCompany, doSearch, loading } = this.props;
    const cardProps = {
      title: selectedCompany.get('organizationName'),
      bordered: false,
    };
    const tableProps = {
      columns: [
        {
          title: '公司名称',
          dataIndex: 'organizationName',
          key: 'organizationName',
          className: styles.nowrap,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '公司联系人',
          dataIndex: 'contacts',
          key: 'contacts',
          className: styles.nowrap,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '公司地址',
          dataIndex: 'address',
          key: 'address',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '操作',
          dataIndex: 'action',
          key: 'action',
          render: (text, record) => (
            <span>
              <a
                href="javascript:;"
                onClick={() => this.handleOpenModal(record)}
              >
                修改
              </a>
              {record.organizationType === '分公司' ? (
                <span>
                  <Divider type="vertical" />
                  <Popconfirm
                    title="你确定要删除该分公司吗？"
                    onConfirm={() => this.handleDeleteCompany(record.key)}
                  >
                    <a href="javascript:;">删除</a>
                  </Popconfirm>
                </span>
              ) : (
                ''
              )}
            </span>
          ),
        },
      ],
      dataSource: [
        {
          organizationName: selectedCompany.get('organizationName'),
          contacts:
            selectedCompany.get('managerName') +
            '/' +
            selectedCompany.get('managerMobileNumber'),
          address:
            (selectedCompany.get('province')
              ? selectedCompany.get('province')
              : '') +
            (selectedCompany.get('city') ? selectedCompany.get('city') : '') +
            (selectedCompany.get('region')
              ? selectedCompany.get('region')
              : '') +
            (selectedCompany.get('street')
              ? selectedCompany.get('street')
              : ''),
          key: selectedCompany.get('utmsOrganizationId'),
          region: selectedCompany.get('region'),
          city: selectedCompany.get('city'),
          province: selectedCompany.get('province'),
          organizationType: selectedCompany.get('organizationType'),
          street: selectedCompany.get('street'),
        },
      ],
      loading,
      pagination: false,
      rowKey: 'key',
    };
    return (
      <Card {...cardProps}>
        <Table {...tableProps} />
      </Card>
    );
  };

  public renderBranchTable = () => {
    const { hideBranchTable } = this.state;
    if (hideBranchTable) {
      return;
    }
    const { dataCount, companyList, loading } = this.props;
    const { current } = this.state;
    const tableProps = {
      columns: [
        {
          title: '公司名称',
          dataIndex: 'organizationName',
          key: 'organizationName',
          className: styles.nowrap,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '公司联系人',
          dataIndex: 'contacts',
          key: 'contacts',
          className: styles.nowrap,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '公司地址',
          dataIndex: 'address',
          key: 'address',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '操作',
          dataIndex: 'action',
          key: 'action',
          width: '105px',
          render: (text, record) => (
            <span>
              <a
                href="javascript:;"
                onClick={() => this.handleOpenModal(record)}
              >
                修改
              </a>
              <Divider type="vertical" />
              <Popconfirm
                title="你确定要删除该公司吗"
                onConfirm={() => this.handleDeleteCompany(record.key)}
              >
                <a href="javascript:;">删除</a>
              </Popconfirm>
            </span>
          ),
        },
      ],
      dataSource: companyList
        ? companyList
            .map(company => ({
              key: company.get('utmsOrganizationId'),
              organizationName: company.get('organizationName'),
              contacts:
                company.get('managerName') +
                '/' +
                company.get('managerMobileNumber'),
              address:
                (company.get('province') ? company.get('province') : '') +
                (company.get('city') ? company.get('city') : '') +
                (company.get('region') ? company.get('region') : '') +
                (company.get('street') ? company.get('street') : ''),
              region: company.get('region'),
              city: company.get('city'),
              province: company.get('province'),
              street: company.get('street'),
            }))
            .toArray()
        : [],
      loading,
      pagination: {
        current,
        total: dataCount,
        onChange: this.paginationChange,
        onShowSizeChange: this.paginationChange,
      },
      rowKey: 'key',
    };
    const cardProps = {
      title: '下属分公司',
      bordered: false,
      extra: (
        <Button type="primary" onClick={() => this.handleOpenModal()}>
          新增分公司
        </Button>
      ),
    };
    return (
      <Card {...cardProps}>
        <MyTable {...tableProps} />
      </Card>
    );
  };

  public moveNode = async ids => {
    const result = await orderCompany({
      utmsOrganizationIds: ids,
    });
    if (result.result === 'success') {
      this.props.getCompanyTree();
      this.doSearch();
    }
  };

  public render() {
    const { companyTree } = this.props;
    const { pickCompany } = this.state;
    return (
      <React.Fragment>
        <Layout>
          <SiderCompanyTree
            companyTree={companyTree}
            handleSelect={this.handleSelect}
            moveNode={this.moveNode}
          />
          <Layout className={'companyLayout'}>
            <Content>{this.renderContent()}</Content>
          </Layout>
        </Layout>
        <EditCompanyModal
          formValue={pickCompany}
          visible={this.state.openModal}
          onConfirm={this.handleEditCompany}
          onClose={this.handleCloseModal}
        />
      </React.Fragment>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getCompanyTree: () => dispatch(actions.getCompanyTree()),
    doSearch: params => dispatch(actions.getList(params)),
    selectCompany: data => dispatch(actions.selectCompany(data)),
    addCompany: company => dispatch(actions.addCompany(company)),
    updateCompany: company => dispatch(actions.updateCompany(company)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    companyTree: makeSelectData(selectState),
    companyList: makeSelectList(selectState),
    dataCount: makeSelectDataCount(selectState),
    pageIndex: makeSelectPageIndex(selectState),
    selectedCompany: makeSelectCompany(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(CompanyManagement);
};
