/**
 * CompanyManagement selectors
 * @author HuangSiFei
 * @date 2018-9-14 11:05:19
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('companyTree'),
  );

export const makeSelectList = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('companyList'),
  );

export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('dataCount'),
  );

export const makeSelectPageIndex = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('pageIndex'),
  );

export const makeSelectCompany = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('selectedCompany'),
  );
