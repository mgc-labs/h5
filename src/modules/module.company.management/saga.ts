/**
 * CompanyManagement Saga
 * @author HuangSiFei
 * @date 2018-9-14 11:05:19
 */
import qs from 'qs';
import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_COMPANYDATA =
  '/ehuodiGateway/utmsCore/utmsOrganizationcs/selectUtmsOrganizationList';
const API_CREATEORG =
  '/ehuodiGateway/utmsCore/utmsOrganizationcs/createUtmsOrganization';
const API_UPDATEORG =
  '/ehuodiGateway/utmsCore/utmsOrganizationcs/updateUtmsOrganization';
// const API_DELETEORG =
//   '/ehuodiGateway/utmsCore/utmsOrganizationcs/deleteUtmsOrganization';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_COMPANYDATA)
    .reply(() =>
      import('./mock/company').then(exports => [200, exports.default]),
    );
}

/**
 * 获取公司信息
 */
const getCompanyList = (params?) =>
  request(
    {
      method: 'post',
      url: API_COMPANYDATA,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);

/**
 * 新建分公司
 */
const addCompany = params =>
  request(
    {
      method: 'post',
      url: API_CREATEORG,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

/**
 * 修改公司
 */
const updateCompany = params =>
  request(
    {
      method: 'post',
      url: API_UPDATEORG,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

/**
 * 删除公司
 */
// const deleteCompany = (params?) =>
//   request(
//     {
//       method: 'post',
//       url: API_DELETEORG,
//       data: qs.stringify(params),
//     },
//     {
//       useMock: false,
//       globalErrorMsg: true,
//     },
//   ).then(res => res.data);

export default (CONSTANTS, actions) => {
  function* getCompanyData(action) {
    const { payload } = action;
    try {
      if (payload) {
        const data = yield call(
          getCompanyList,
          Object.assign(
            {
              organizationType: '分公司',
            },
            payload,
          ),
        );
        yield put(actions.getListDone(data));
      } else {
        const data = yield call(getCompanyList);
        const selected = data.data.filter(company => {
          return company.organizationType === '总公司';
        })[0];
        yield put(actions.getCompanyTreeDone(data, selected));
      }
    } catch (err) {
      yield put(actions.getCompanyTreeError(err));
    }
  }

  function* addCompanyAsync(action) {
    const { payload } = action;
    try {
      const data = yield call(addCompany, payload);
      yield put(actions.addCompanyDone(data));
    } catch (err) {
      yield put(actions.addCompanyError(err));
    }
  }

  function* updateCompanyAsync(action) {
    const { payload } = action;
    try {
      const data = yield call(updateCompany, payload);
      yield put(actions.updateCompanyDone(data));
    } catch (err) {
      yield put(actions.updateCompanyError(err));
    }
  }

  // function* deleteCompanyAsync(action) {
  //   const { payload } = action;
  //   try {
  //     const data = yield call(deleteCompany, {
  //       utmsOrganizationId: payload,
  //     });
  //     yield put(actions.deleteCompanyDone(data));
  //   } catch (err) {
  //     yield put(actions.deleteCompanyError(err));
  //   }
  // }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_COMPANY_TREE, getCompanyData);
    yield takeLatest(CONSTANTS.GET_LIST, getCompanyData);
    yield takeLatest(CONSTANTS.ADD_COMPANY, addCompanyAsync);
    yield takeLatest(CONSTANTS.UPDATE_COMPANY, updateCompanyAsync);
    // yield takeLatest(CONSTANTS.DELETE_COMPANY, deleteCompanyAsync);
  };
};
