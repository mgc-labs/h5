/**
 * EditCompanyModal Component
 * @author HuangSiFei
 * @date 2018-9-14 11:05:19
 */
import Col from 'antd/es/col';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import AddressSelect from 'components/AddressSelect';
import * as React from 'react';
import { FIXED_TELEPHONE } from 'utils/commonRegExp';

import styles from 'modules/module.company.management/index.module.less';

const FormItem = Form.Item;
const itemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

export interface IEditCompanyModalProps {
  form: any;
  formValue: any;
  onClose: () => void;
  onConfirm: (values) => void;
  visible: boolean;
}

class EditCompany extends React.PureComponent<IEditCompanyModalProps> {
  // public componentWillReceiveProps(nextProps) {
  //   if (nextProps.visible && !this.props.visible) {
  //     nextProps.form.resetFields();
  //   }
  // }
  public handleSubmit = () => {
    const { form, onConfirm } = this.props;
    const { getFieldValue } = form;
    form.validateFields((errors, values) => {
      if (!errors) {
        const params = { street: '' };
        Object.keys(values).map(key => {
          if (key === 'street') {
            params.street = values[key];
          } else {
            params[key] = values[key];
          }
        });
        onConfirm(
          Object.assign(
            getFieldValue('key')
              ? { utmsOrganizationId: getFieldValue('key') }
              : {},
            params,
          ),
        );
      }
    });
  };
  public handleClose = () => {
    this.props.onClose();
  };
  public onAddressSelected = result => {
    const { setFieldsValue } = this.props.form;
    setFieldsValue({
      street: result ? result.address : '',
      city: result ? result.city : '',
      province: result ? result.province : '',
      region: result ? result.region : '',
    });
  };
  public renderEditor() {
    const { form, formValue } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Form>
        <FormItem {...itemLayout} label="公司名称" required>
          {getFieldDecorator('organizationName', {
            rules: [{ required: true, message: '请输入公司名称' }],
          })(<Input placeholder="请输入公司名称" maxLength={60} />)}
        </FormItem>
        <FormItem {...itemLayout} label="联系人" required>
          <Col span={11}>
            <FormItem required>
              {getFieldDecorator('managerName', {
                rules: [{ required: true, message: '请输入姓名' }],
              })(<Input placeholder="请输入姓名" maxLength={20} />)}
            </FormItem>
          </Col>
          <Col span={2}>
            <span
              style={{
                display: 'inline-block',
                width: '100%',
                textAlign: 'center',
              }}
            >
              -
            </span>
          </Col>
          <Col span={11}>
            <FormItem required>
              {getFieldDecorator('managerMobileNumber', {
                rules: [
                  { required: true, message: '请输入联系方式' },
                  {
                    pattern: FIXED_TELEPHONE,
                    message: '请输入正确的联系方式!',
                  },
                ],
              })(<Input placeholder="请输入联系方式" maxLength={13} />)}
            </FormItem>
          </Col>
        </FormItem>
        <Form.Item {...itemLayout} label="公司地址">
          {getFieldDecorator('street', {
            initialValue: '',
            rules: [{ max: 200, message: '最多输入200个字' }],
          })(
            <AddressSelect
              placeholder="请输入公司地址"
              onSelected={this.onAddressSelected}
            />,
          )}
        </Form.Item>
        <FormItem {...itemLayout} label="区域" style={{ display: 'none' }}>
          {getFieldDecorator('region')(<Input type="hidden" />)}
        </FormItem>
        <FormItem {...itemLayout} label="城市" style={{ display: 'none' }}>
          {getFieldDecorator('city')(<Input type="hidden" />)}
        </FormItem>
        <FormItem {...itemLayout} label="省份" style={{ display: 'none' }}>
          {getFieldDecorator('province')(<Input type="hidden" />)}
        </FormItem>
      </Form>
    );
  }
  public render() {
    const { visible, formValue } = this.props;
    const modalProps = {
      title: formValue.get('key') ? '修改公司' : '新增公司',
      visible,
      centered: true,
      maskClosable: false,
      onOk: this.handleSubmit,
      onCancel: this.handleClose,
    };
    return <Modal {...modalProps}>{this.renderEditor()}</Modal>;
  }
}

const EditCompanyModal = Form.create({
  mapPropsToFields(props: any) {
    const params = {};
    const formObj = props.formValue.toObject();
    Object.keys(formObj).map(key => {
      if (key === 'contacts') {
        const contactArray = formObj[key].split('/');
        params.managerName = Form.createFormField({
          value: contactArray[0],
        });
        params.managerMobileNumber = Form.createFormField({
          value: contactArray[1],
        });
      } else {
        params[key] = Form.createFormField({
          value: formObj[key],
        });
      }
    });
    return { ...params };
  },
})(EditCompany);
export default EditCompanyModal;
