/**
 * CompanyStaff service
 * @author HuangSiFei
 * @date 2018-9-19 17:33:32
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

const API_CREATEORG =
  '/ehuodiGateway/utmsCore/utmsOrganizationcs/createUtmsOrganization';
const API_UPDATEORG =
  '/ehuodiGateway/utmsCore/utmsOrganizationcs/updateUtmsOrganization';
const API_DELETEORG =
  '/ehuodiGateway/utmsCore/utmsOrganizationcs/deleteUtmsOrganization';
const API_UPDATEORDER =
  '/ehuodiGateway/utmsCore/utmsOrganizationcs/updateUtmsOrganizationOrder';

export const updateCompany = params =>
  request(
    {
      method: 'post',
      url: API_UPDATEORG,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

export const addCompany = params =>
  request(
    {
      method: 'post',
      url: API_CREATEORG,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 删除公司
 */
export const deleteCompany = params =>
  request(
    {
      method: 'post',
      url: API_DELETEORG,
      data: qs.stringify(params),
    },
    {
      useMock: false,
    },
  ).then(res => res, err => err);

/**
 * 调整顺序
 */
export const orderCompany = params =>
  request(
    {
      method: 'post',
      url: API_UPDATEORDER,
      // data: params,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);
