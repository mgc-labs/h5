/**
 * CompanyManagement Reducers
 * @author HuangSiFei
 * @date 2018-9-14 11:05:19
 */

import { fromJS, List, Map } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  payload: List(),
  selectedCompany: Map(),
  error: false,
  loading: false,
  dataCount: 0,
});

export default CONSTANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSTANTS.GET_COMPANY_TREE:
        return state.set('error', false).set('loading', true);
      case CONSTANTS.GET_COMPANY_TREE_SUCCESS:
        return state
          .set('loading', false)
          .set('companyTree', fromJS(action.payload.data))
          .set('selectedCompany', fromJS(action.selectedCompany));
      case CONSTANTS.GET_COMPANY_TREE_ERROR:
        return state.set('error', action.error).set('loading', false);

      case CONSTANTS.GET_LIST:
        return state.set('error', false).set('loading', true);
      case CONSTANTS.GET_LIST_SUCCESS:
        return state
          .set('loading', false)
          .set('companyList', fromJS(action.payload.data))
          .set('dataCount', action.payload.count);
      case CONSTANTS.GET_LIST_ERROR:
        return state.set('error', action.error).set('loading', false);

      case CONSTANTS.ADD_COMPANY:
        return state.set('error', false).set('loading', true);
      case CONSTANTS.ADD_COMPANY_SUCCESS:
        return state.set('loading', false);
      case CONSTANTS.ADD_COMPANY_ERROR:
        return state.set('error', action.error).set('loading', false);

      case CONSTANTS.UPDATE_COMPANY:
        return state.set('error', false).set('loading', true);
      case CONSTANTS.UPDATE_COMPANY_SUCCESS:
        return state.set('loading', false);
      case CONSTANTS.UPDATE_COMPANY_ERROR:
        return state.set('error', action.error).set('loading', false);

      case CONSTANTS.DELETE_COMPANY:
        return state.set('error', false).set('loading', true);
      case CONSTANTS.DELETE_COMPANY_SUCCESS:
        return state.set('loading', false);
      case CONSTANTS.DELETE_COMPANY_ERROR:
        return state.set('error', action.error).set('loading', false);

      case CONSTANTS.ON_SELECTED_COMPANY:
        return state.set('selectedCompany', fromJS(action.selected));
      default:
        return state;
    }
  };
};
