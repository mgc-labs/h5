/**
 * CompanyManagement Component
 * @author HuangSiFei
 * @date 2018-9-14 11:05:19
 */
import Layout from 'antd/es/layout';
import Tree from 'antd/es/tree';
import { List, Map } from 'immutable';
import * as React from 'react';

const { Sider } = Layout;
const { TreeNode } = Tree;

interface ISiderCompanyTreeProps {
  companyTree: List<Map<string, any>>;
  handleSelect: (value) => void;
}

export default class SiderCompanyTree extends React.PureComponent<
  ISiderCompanyTreeProps
> {
  public onSelect = selectedKeys => {
    const { handleSelect, companyTree } = this.props;
    const selected = companyTree
      .filter(company => {
        return company.get('organizationCode') === selectedKeys[0];
      })
      .toArray();
    handleSelect(selected[0]);
  };

  public renderTreeNode() {
    const { companyTree } = this.props;
    const branch = companyTree
      ? companyTree
          .filter(company => {
            return company.get('organizationType') !== '总公司';
          })
          .map(company => ({
            title: company.get('organizationName'),
            key: company.get('organizationCode'),
          }))
          .toArray()
      : [];
    return branch
      ? branch.map(company => {
          return <TreeNode key={company.key} title={company.title} />;
        })
      : [];
  }

  public renderSider() {
    const { companyTree } = this.props;
    const headQuarter = companyTree
      ? companyTree
          .filter(company => {
            return company.get('organizationType') === '总公司';
          })
          .map(company => ({
            title: company.get('organizationName'),
            key: company.get('organizationCode'),
          }))
          .toArray()
      : [];
    const treeProps = {
      autoExpandParent: true,
      onSelect: this.onSelect,
    };
    return (
      <Tree {...treeProps}>
        <TreeNode {...headQuarter[0]}>{this.renderTreeNode()}</TreeNode>
      </Tree>
    );
  }
  public render() {
    return <Sider style={{ background: '#fff' }}>{this.renderSider()}</Sider>;
  }
}
