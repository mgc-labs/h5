/**
 * CompanyManagement Actions
 * @author HuangSiFei
 * @date 2018-9-14 11:05:19
 */
import { AnyAction } from 'redux';

export default CONSTANTS => ({
  getCompanyTree: (): AnyAction => ({
    type: CONSTANTS.GET_COMPANY_TREE,
  }),
  getCompanyTreeDone: (data, selected): AnyAction => ({
    payload: data,
    selectedCompany: selected,
    type: CONSTANTS.GET_COMPANY_TREE_SUCCESS,
  }),
  getCompanyTreeError: (error): AnyAction => ({
    error,
    type: CONSTANTS.GET_COMPANY_TREE_ERROR,
  }),

  getList: (params): AnyAction => ({
    payload: params,
    type: CONSTANTS.GET_LIST,
  }),
  getListDone: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.GET_LIST_SUCCESS,
  }),
  getListError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSTANTS.GET_LIST_ERROR,
  }),

  selectCompany: (data): AnyAction => ({
    selected: data,
    type: CONSTANTS.ON_SELECTED_COMPANY,
  }),

  onSelectedCompanyDone: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.ON_SELECTED_COMPANY_SUCCESS,
  }),

  // deleteCompany: (data): AnyAction => ({
  //   payload: data,
  //   type: CONSTANTS.DELETE_COMPANY,
  // }),
  // deleteCompanyDone: (data): AnyAction => ({
  //   payload: data,
  //   type: CONSTANTS.DELETE_COMPANY_SUCCESS,
  // }),
  // deleteCompanyError: (error): AnyAction => ({
  //   error: true,
  //   payload: error,
  //   type: CONSTANTS.DELETE_COMPANY_ERROR,
  // }),

  addCompany: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.ADD_COMPANY,
  }),
  addCompanyDone: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.ADD_COMPANY_SUCCESS,
  }),
  addCompanyError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSTANTS.ADD_COMPANY_ERROR,
  }),

  updateCompany: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.UPDATE_COMPANY,
  }),
  updateCompanyDone: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.UPDATE_COMPANY_SUCCESS,
  }),
  updateCompanyError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSTANTS.UPDATE_COMPANY_ERROR,
  }),
});
