/**
 * ManageDriver model
 * @author chengle
 * @date 2018-9-13 15:44:27
 */
import message from 'antd/es/message';
import { fromJS, List, Map } from 'immutable';
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import {
  // addDriver,
  bindingVehicle,
  // editDriver,
  getList,
  getVehicle,
  // setDelete,
  setDeleteVehicle,
} from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: List(),
      dataCount: 0,
      error: false,
      loading: false,
      utmsUsersDriversId: Number,
    }),
    actions: {
      // 列表
      GET_ASYNC_LIST: data => data,
      GET_ASYNC_LIST_ERROR: error => error,
      GET_ASYNC_LIST_SUCCESS: data => data,
      // 删除司机
      // SET_DELETE_DRIVER: data => data,
      // SET_DELETE_DRIVER_ERROR: error => error,
      // SET_DELETE_DRIVER_SUCCESS: data => data,
      // 添加司机
      // SET_ADD_DRIVER: data => data,
      // SET_ADD_DRIVER_ERROR: error => error,
      // SET_ADD_DRIVER_SUCCESS: data => data,
      // 编辑司机
      // SET_EDIT_DRIVER: data => data,
      // SET_EDIT_DRIVER_ERROR: error => error,
      // SET_EDIT_DRIVER_SUCCESS: data => data,
      // 绑定车辆
      SET_BIND_VEHICLE: data => data,
      SET_BIND_VEHICLE_ERROR: error => error,
      SET_BIND_VEHICLE_SUCCESS: data => data,
      // 查询未绑定车辆
      SET_GET_VEHICLE: data => data,
      SET_GET_VEHICLE_ERROR: error => error,
      SET_GET_VEHICLE_SUCCESS: data => data,
      // 删除绑定车辆
      SET_DELETE_VEHICLE: data => data,
      SET_DELETE_VEHICLE_ERROR: error => error,
      SET_DELETE_VEHICLE_SUCCESS: data => data,
    },
    reducers: {
      // 列表
      GET_ASYNC_LIST(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_ASYNC_LIST_ERROR(state, action) {
        return state.set('error', action.error).set('loading', false);
      },
      GET_ASYNC_LIST_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data))
          .set('dataCount', action.payload.count);
      },
      // 删除司机
      // SET_DELETE_DRIVER(state, action) {
      //   return state.set('utmsUsersDriversId', action.payload);
      // },
      // SET_DELETE_DRIVER_ERROR(state, action) {
      //   return state.set('error', action.error).set('loading', false);
      // },
      // SET_DELETE_DRIVER_SUCCESS(state, action) {
      //   return state
      //     .set('loading', false)
      //     // .set('data', fromJS(action.payload.data));
      // },
      // 添加司机
      // SET_ADD_DRIVER(state, action) {
      //   return state.set('error', false).set('loading', true);
      // },
      // SET_ADD_DRIVER_ERROR(state, action) {
      //   return state.set('error', action.error).set('loading', false);
      // },
      // SET_ADD_DRIVER_SUCCESS(state, action) {
      //   return state
      //     .set('loading', false)
      //     .set('data', fromJS(action.payload.data));
      // },
      // 编辑司机
      // SET_EDIT_DRIVER(state, action) {
      //   return state.set('error', false).set('loading', true);
      // },
      // SET_EDIT_DRIVER_ERROR(state, action) {
      //   return state.set('error', action.error).set('loading', false);
      // },
      // SET_EDIT_DRIVER_SUCCESS(state, action) {
      //   return state
      //     .set('loading', false)
      //     .set('data', fromJS(action.payload.data));
      // },
      // 绑定车辆
      // SET_BIND_VEHICLE(state, action) {
      //   return state.set('error', false).set('loading', true);
      // },
      // SET_BIND_VEHICLE_ERROR(state, action) {
      //   return state.set('error', action.error).set('loading', false);
      // },
      // SET_BIND_VEHICLE_SUCCESS(state, action) {
      //   return state
      //     .set('loading', false)
      //     .set('data', fromJS(action.payload.data));
      // },
      // 查询未绑定车辆
      // SET_GET_VEHICLE(state, action) {
      //   return state.set('error', false).set('loading', true);
      // },
      // SET_GET_VEHICLE_ERROR(state, action) {
      //   return state.set('error', action.error).set('loading', false);
      // },
      // SET_GET_VEHICLE_SUCCESS(state, action) {
      //   return state
      //     .set('loading', false)
      //     .set('data', fromJS(action.payload.data));
      // },
      // 删除绑定车辆
      // SET_DELETE_VEHICLE(state, action) {
      //   return state.set('error', false).set('loading', true);
      // },
      // SET_DELETE_VEHICLE_ERROR(state, action) {
      //   return state.set('error', action.error).set('loading', false);
      // },
      // SET_DELETE_VEHICLE_SUCCESS(state, action) {
      //   return state
      //     .set('loading', false)
      //     .set('data', fromJS(action.payload.data));
      // },
    },
    effects(CONSTANTS, actions) {
      function* fetchList(action) {
        const { payload } = action;
        try {
          const data = yield call(getList, payload);
          yield put(actions.getAsyncListSuccess(data));
        } catch (err) {
          yield put(actions.getAsyncListError(err));
          message.error(err.toString());
        }
      }

      // function* deleteDriver(action) {
      //   const { payload } = action;
      //   try {
      //     const data = yield call(setDelete, payload);
      //     yield put(actions.setDeleteDriverSuccess(data));
      //   } catch (err) {
      //     yield put(actions.setDeleteDriverError(err));
      //     message.error(err.toString());
      //   }
      // }

      // function* addDriverFn(action) {
      //   const { payload } = action;
      //   try {
      //     const data = yield call(addDriver, payload);
      //     yield put(actions.setAddDriverSuccess(data));
      //   } catch (err) {
      //     yield put(actions.setAddDriverError(err));
      //     message.error(err.toString());
      //   }
      // }

      // function* editDriverFn(action) {
      //   const { payload } = action;
      //   try {
      //     const data = yield call(editDriver, payload);
      //     yield put(actions.setEditDriverSuccess(data));
      //   } catch (err) {
      //     yield put(actions.setEditDriverError(err));
      //     message.error(err.toString());
      //   }
      // }

      // function* bindingVehicleFn(action) {
      //   const { payload } = action;
      //   try {
      //     const data = yield call(bindingVehicle, payload);
      //     yield put(actions.setBindVehicleSuccess(data));
      //   } catch (err) {
      //     yield put(actions.setBindVehicleError(err));
      //     message.error(err.toString());
      //   }
      // }

      // function* getVehicleFn(action) {
      //   const { payload } = action;
      //   try {
      //     const data = yield call(getVehicle, payload);
      //     yield put(actions.getVehicleSuccess(data));
      //   } catch (err) {
      //     yield put(actions.getVehicleError(err));
      //     message.error(err.toString());
      //   }
      // }

      // function* setDeleteVehicleFn(action) {
      //   const { payload } = action;
      //   try {
      //     const data = yield call(setDeleteVehicle, payload);
      //     yield put(actions.setDeleteVehicleSuccess(data));
      //   } catch (err) {
      //     yield put(actions.setDeleteVehicleError(err));
      //     message.error(err.toString());
      //   }
      // }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ASYNC_LIST, fetchList);
        // yield takeLatest(CONSTANTS.SET_DELETE_DRIVER, deleteDriver);
        // yield takeLatest(CONSTANTS.SET_ADD_DRIVER, addDriverFn);
        // yield takeLatest(CONSTANTS.SET_EDIT_DRIVER, editDriverFn);
        // yield takeLatest(CONSTANTS.SET_BIND_VEHICLE, bindingVehicleFn);
        // yield takeLatest(CONSTANTS.SET_GET_VEHICLE, getVehicleFn);
        // yield takeLatest(CONSTANTS.SET_DELETE_VEHICLE, setDeleteVehicleFn);
      };
    },
  });
