/**
 * ManageDriver selectors
 * @author chengle
 * @date 2018-9-13 15:44:27
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

// 列表
export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('dataCount'),
  );

// 分页
// export const makeSelectPageIndex = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('pageIndex'),
//   );

// 删除
// export const makeDeleteDriver = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('utmsUsersDriversId'),
//   );

// export const makeDeleteDriverError = selectState =>
//   createSelector(selectState, (state: Map<string, any>) => state.get('error'));

// export const makeDeleteDriverLoading = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('loading'),
//   );

// 添加司机
// export const makeAddDriver = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('utmsUsersDriversId'),
//   );

// export const makeAddDriverError = selectState =>
//   createSelector(selectState, (state: Map<string, any>) => state.get('error'));

// export const makeAddDriverLoading = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('loading'),
//   );

// 编辑司机
// export const makeEditDriver = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('utmsUsersDriversId'),
//   );

// export const makeEditDriverError = selectState =>
//   createSelector(selectState, (state: Map<string, any>) => state.get('error'));

// export const makeEditDriverLoading = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('loading'),
//   );

// 绑定车辆
// export const makeBindVehicle = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('utmsUsersDriversId'),
//   );

// export const makeBindVehicleError = selectState =>
//   createSelector(selectState, (state: Map<string, any>) => state.get('error'));

// export const makeBindVehicleLoading = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('loading'),
//   );

// 查询未绑定车辆
// export const makeSelectVehicle = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('utmsUsersDriversId'),
//   );

// export const makeSelectVehicleError = selectState =>
//   createSelector(selectState, (state: Map<string, any>) => state.get('error'));

// export const makeSelectVehicleLoading = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('loading'),
//   );

// 删除绑定车辆
// export const makeDeleteVehicle = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('utmsUsersDriversId'),
//   );

// export const makeDeleteVehicleError = selectState =>
//   createSelector(selectState, (state: Map<string, any>) => state.get('error'));

// export const makeDeleteVehicleLoading = selectState =>
//   createSelector(selectState, (state: Map<string, any>) =>
//     state.get('loading'),
//   );
