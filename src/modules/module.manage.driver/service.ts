/**
 * ManageDriver service
 * @author chengle
 * @date 2018-9-13 15:44:27
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
// import { USE_MOCK } from '../../CONFIG';
const USE_MOCK = false;

// API list
const API_LIST =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/selectDriverListByCondition';
const API_DELETE = '/ehuodiGateway/utmsDispatch/utmsDrivercs/deleteUtmsDriver'; // 删除司机
const API_ADDITION = '/ehuodiGateway/utmsDispatch/utmsDrivercs/addUtmsDriver'; // 新建司机
const API_UPDATE = '/ehuodiGateway/utmsDispatch/utmsDrivercs/modifyUtmsDriver'; // 编辑司机
const API_BINDING =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/bindingUtmsDriverAndVehicle'; // 绑定车辆
const API_DETAILS =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/selectUtmsDriverDetailByPhone'; // 查询司机详情
const API_DRIVER =
  '/ehuodiGateway/utmsDispatch/utmsVehiclecs/selectUnbindingUtmsVehicleList'; // 查询未绑定的车辆列表
const API_DELETEVEHICLE =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/untiedUtmsDriverAndVehicle'; // 删除绑定的车辆
const API_VEHICLEDETAILS =
  '/ehuodiGateway/utmsDispatch/utmsVehiclecs/selectUtmsVehicleDetail'; // 查询车辆详情

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_LIST)
    .reply(() => import('./mock/list').then(exports => [200, exports.default]));
}

/**
 * 获取司机列表
 */
export const getList = ops =>
  request(
    {
      method: 'post',
      url: API_LIST,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 删除司机
 */
export const setDelete = ops =>
  request(
    {
      method: 'post',
      url: API_DELETE,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 添加司机
 */
export const addDriver = ops =>
  request(
    {
      method: 'post',
      url: API_ADDITION,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 编辑司机
 */
export const editDriver = ops =>
  request(
    {
      method: 'post',
      url: API_UPDATE,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 绑定车辆
 */
export const bindingVehicle = ops =>
  request(
    {
      method: 'post',
      url: API_BINDING,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 查询未绑定的车辆列表
 */
export const getVehicle = ops =>
  request(
    {
      method: 'post',
      url: API_DRIVER,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 删除绑定的车辆
 */
export const setDeleteVehicle = ops =>
  request(
    {
      method: 'post',
      url: API_DELETEVEHICLE,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 获取车辆详情
 */
export const getVehicleDetails = ops =>
  request(
    {
      method: 'post',
      url: API_VEHICLEDETAILS,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);
