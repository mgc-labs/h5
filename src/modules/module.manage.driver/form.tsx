import {
  Button,
  Col,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Row,
  Select,
} from 'antd';

import React, { PureComponent } from 'react';
import { MOBILE_PHONE } from 'utils/commonRegExp.ts';
import styles from './index.module.less';
import { bindingVehicle, getVehicle, setDeleteVehicle } from './service';

const FormItem = Form.Item;
const Search = Input.Search;
const Option = Select.Option;
const { confirm, info } = Modal;

class FormModule extends PureComponent {
  state = {
    btnVisible: true,
    value: undefined,
    vehicleData: [],
    selectVehicle: Number,
  };

  public componentDidMount() {
    this.getVehicleData();
  }
  public getVehicleData = async () => {
    try {
      const data = await getVehicle();
      const vehicleData = data.data || [];
      if (this.props.vehicleDataState) {
        let isHas = false;
        vehicleData.map(item => {
          if (
            item.utmsVehicleId === this.props.vehicleDataState.utmsVehicleId
          ) {
            isHas = true;
          }
        });
        if (!isHas) {
          vehicleData.push(this.props.vehicleDataState);
        }
      }
      this.setState({ vehicleData });
    } catch (err) {
      //
    }
  };
  public componentWillReceiveProps(nextProps) {
    if (nextProps.visible !== this.props.visible) {
      this.getVehicleData();
    }
  }

  seva = async value => {
    if (this.props.title === '添加司机') {
      if (this.props.form.getFieldsValue().utmsVehicleId) {
        this.setState({
          btnVisible: true,
          utmsVehicleId: this.props.form.getFieldsValue().utmsVehicleId,
        });
      } else {
        this.setState({ btnVisible: true });
      }
    } else {
      await bindingVehicle({
        utmsUsersDriversId: this.props.utmsUsersDriversId,
        utmsVehicleId: this.props.form.getFieldsValue().utmsVehicleId,
      }).then(data => {
        if (data.result === 'success') {
          this.props.handleSubmit();
          this.setState({ btnVisible: true });
        }
      });
    }
  };

  btn = () => {
    if (!this.state.btnVisible) {
      return (
        <span>
          <a
            href="javascript:;"
            className="ml10"
            onClick={() => {
              if (!parseInt(this.props.form.getFieldsValue().utmsVehicleId, 10)) {
                message.error('请选择绑定车辆');
                return;
              }
              this.seva();
            }}
          >
            保存
          </a>
          <a
            href="javascript:;"
            className="ml10"
            onClick={value => {
              this.setState({ btnVisible: true });
              if (this.props.vehicleDataState.utmsVehicleId) {
                this.props.form.setFieldsValue({
                  utmsVehicleId: this.props.vehicleDataState.utmsVehicleId,
                });
              } else {
                this.props.form.setFieldsValue({
                  utmsVehicleId: Number,
                });
              }
            }}
          >
            取消
          </a>
        </span>
      );
    } else {
      return (
        <span>
          <a
            href="javascript:;"
            className="ml10"
            onClick={value => this.setState({ btnVisible: false })}
          >
            修改
          </a>
          {parseInt(this.props.form.getFieldsValue().utmsVehicleId) ? (
            <a
              href="javascript:;"
              className="ml10"
              onClick={async value => {
                await setDeleteVehicle({
                  utmsUsersDriversId: this.props.utmsUsersDriversId,
                  utmsVehicleId: this.props.form.getFieldsValue().utmsVehicleId,
                }).then(data => {
                  if (data.result === 'success') {
                    this.props.form.setFieldsValue({
                      utmsVehicleId: Number,
                    });
                    this.getVehicleData();
                    this.props.handleSubmit();
                  }
                });
              }}
            >
              删除
            </a>
          ) : (
            ''
          )}
        </span>
      );
    }
  };

  render() {
    const {
      visible,
      onCancel,
      onCreate,
      form,
      title,
      confirmLoading,
    } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        span: 6,
      },
      wrapperCol: {
        span: 15,
      },
    };
    return (
      <Modal
        centered={true}
        maskClosable={false}
        title={title}
        visible={visible}
        onOk={onCreate}
        confirmLoading={confirmLoading}
        onCancel={() => {
          {
            /*const change = this.props.form.isFieldsTouched();
          if (change) {
            confirm({
              title: '有未保存的信息',
              content: <p>如放弃，填写的信息将丢失</p>,
              okText: '继续填写',
              cancelText: '放弃',
              onCancel: () => {*/
          }
          this.setState({ btnVisible: true });
          onCancel();
          {
            /*},
            });
          } else {
            this.setState({ btnVisible: true });
            onCancel();
          }*/
          }
        }}
      >
        <Form layout="horizontal">
          <FormItem {...formItemLayout} label="手机号码">
            {getFieldDecorator('phoneNumber', {
              rules: [
                { required: true, message: '请输入手机号码!' },
                {
                  pattern: MOBILE_PHONE,
                  message: '请输入正确的联系方式',
                },
              ],
            })(
              <Input
                disabled={title === '编辑司机'}
                maxLength={20}
                onKeyDown={e => {
                  if (e.keyCode === 13) {
                    onCreate();
                  }
                }}
              />,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="姓名">
            {getFieldDecorator('driverName')(
              <Input
                maxLength={20}
                onKeyDown={e => {
                  if (e.keyCode === 13) {
                    onCreate();
                  }
                }}
              />,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="驾照类型">
            {getFieldDecorator('driverLicenseType')(
              <Select>
                <Option value="A1">A1</Option>
                <Option value="A2">A2</Option>
                <Option value="A3">A3</Option>
                <Option value="A4">A4</Option>
                <Option value="B1">B1</Option>
                <Option value="B2">B2</Option>
                <Option value="B3">B3</Option>
                <Option value="B4">B4</Option>
                <Option value="C1">C1</Option>
                <Option value="C2">C2</Option>
                <Option value="C3">C3</Option>
                <Option value="C4">C4</Option>
              </Select>,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="驾龄">
            {getFieldDecorator('drivingAge')(
              <InputNumber
                min={0}
                max={100}
                className={styles.ManageDriver__InputNumber}
                onKeyDown={e => {
                  if (e.keyCode === 13) {
                    onCreate();
                  }
                }}
              />,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="年龄">
            {getFieldDecorator('age')(
              <InputNumber
                min={0}
                max={100}
                className={styles.ManageDriver__InputNumber}
                onKeyDown={e => {
                  if (e.keyCode === 13) {
                    onCreate();
                  }
                }}
              />,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="绑定车辆">
            {getFieldDecorator('utmsVehicleId')(
              <Select
                showSearch
                optionFilterProp="children"
                disabled={title === '编辑司机' ? this.state.btnVisible : false}
              >
                {this.state.vehicleData.map(item => {
                  if (item && item.utmsVehicleId) {
                    return (
                      <Option
                        key={item.utmsVehicleId}
                        value={item.utmsVehicleId}
                      >
                        {item.plateNumber} - {item.vehicleLong / 1000}米
                        {item.vehicleType}
                      </Option>
                    );
                  }
                })}
              </Select>,
            )}
            {title === '编辑司机' ? this.btn() : ''}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

const ModalForm = Form.create()(FormModule);

export default ModalForm;
