/**
 * ManageDriver Component
 * @author chengle
 * @date 2018-9-13 15:44:27
 */
import {
  Button,
  Card,
  Cascader,
  Col,
  Divider,
  Form,
  Icon,
  Input,
  InputNumber,
  Modal,
  Pagination,
  Popconfirm,
  Row,
  Select,
} from 'antd';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Table from 'components/MyTable';
import { List, Map } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import {
  makeSelectData,
  makeSelectDataCount,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

import DetailsModule from '../module.manage.vehicle/vehicleDetails';
import ModalForm from './form';

import styles from './index.module.less';

import { addDriver, editDriver, getVehicleDetails, setDelete } from './service';

import GLOBAL from 'utils/GLOBAL.ts';

const Search = Input.Search;
const Option = Select.Option;
const confirm = Modal.confirm;
const FormItem = Form.Item;

export interface IManageDriverProps {
  list: List<Map<string, any>>;
  dataCount: number;
  loading: boolean;
  children: React.ReactChildren;
  form: WrappedFormUtils;
  getAsyncList: (options?: object) => any;
  setBindVehicle: (options?: object) => any;
  getVehicle: (options?: object) => any;
  setDeleteVehicle: (options?: object) => any;
}

@Form.create({})
class ManageDriver extends React.PureComponent<IManageDriverProps> {
  constructor(props) {
    super(props);
    this.state = {
      current: 1,
      skipCount: 0,
      pageSize: 15,
      ModalTitle: '添加司机',
      visible: false,
      detailsVisible: false,
      confirmLoading: false,
      utmsUsersDriversId: Number,
      detailData: {},
      vehicleDataState: {},
    };
  }

  public componentDidMount() {
    this.handleSubmit();
    // document.addEventListener('keydown', this.handleEnterKey);
  }

  // 添加司机
  public addDriver = () => {
    this.setState({
      ModalTitle: '添加司机',
      confirmLoading: false,
      visible: true,
      vehicleDataState: {},
    });
  };
  public handleOk = () => {
    const form = this.formRef.props.form;
    form.validateFields(async (err, values) => {
      if (err) {
        return;
      }
      if (!values.utmsVehicleId) {
        delete values.utmsVehicleId;
      }
      this.setState({
        confirmLoading: true,
      });

      if (this.state.ModalTitle === '添加司机') {
        await addDriver(values).then(
          data => {
            this.setState({ visible: false, confirmLoading: false }, () => {
              form.resetFields();
              this.handleSubmit();
            });
          },
          err => {
            this.setState({
              confirmLoading: false,
            });
          },
        );
      } else {
        await editDriver(
          Object.assign(values, {
            utmsUsersDriversId: this.state.utmsUsersDriversId,
          }),
        ).then(
          data => {
            this.setState({ visible: false, confirmLoading: false }, () => {
              form.resetFields();
              this.handleSubmit();
            });
          },
          err => {
            this.setState({
              confirmLoading: false,
            });
          },
        );
      }
    });
  };
  public handleCancel = () => {
    const form = this.formRef.props.form;
    form.resetFields();
    this.setState({ visible: false, utmsUsersDriversId: Number });
  };

  // 编辑司机
  public editDriver = value => {
    // console.log(this.formRef.props.form);
    const form = this.formRef.props.form;
    // console.log(value);
    // console.log(form.getFieldsValue());
    if (value.plateNumber) {
      this.setState(
        {
          ModalTitle: '编辑司机',
          confirmLoading: false,
          visible: true,
          utmsUsersDriversId: value.utmsUsersDriversId,
          vehicleDataState: {
            plateNumber: value.plateNumber,
            utmsVehicleId: value.utmsVehicleId,
            vehicleLong: value.vehicleLong,
            vehicleType: value.vehicleType,
          },
        },
        form.setFieldsValue({
          phoneNumber: value.phoneNumber,
          driverName: value.driverName,
          driverLicenseType: value.driverLicenseType,
          drivingAge: value.drivingAge,
          age: value.age,
          plateNumber: value.plateNumber,
          utmsVehicleId: value.utmsVehicleId,
        }),
      );
    } else {
      this.setState(
        {
          ModalTitle: '编辑司机',
          confirmLoading: false,
          visible: true,
          utmsUsersDriversId: value.utmsUsersDriversId,
          vehicleDataState: {},
        },
        form.setFieldsValue({
          phoneNumber: value.phoneNumber,
          driverName: value.driverName,
          driverLicenseType: value.driverLicenseType,
          drivingAge: value.drivingAge,
          age: value.age,
          plateNumber: value.plateNumber,
          utmsVehicleId: value.utmsVehicleId,
        }),
      );
    }
  };

  // 删除司机
  public deleteConfirm = async value => {
    await setDelete({ utmsUsersDriversId: value.utmsUsersDriversId }).then(
      data => {
        if (data.result === 'success') {
          this.handleSubmit();
        }
      },
    );
  };

  // 车辆详情
  public details = value => {
    this.getDetailsFn(value.utmsVehicleId, () => {
      this.setState({
        detailsVisible: true,
      });
    });
  };
  public detailsCancel = () => {
    this.setState({
      detailsVisible: false,
      detailData: {},
    });
  };
  public getDetailsFn = async (id, Fn) => {
    await getVehicleDetails({ utmsVehicleId: id }).then(data => {
      // console.log(data, 222);
      this.setState(
        {
          detailData: data.data, // Object.assign(data.data, this.state.detailData),
        },
        Fn,
      );
    });
  };

  public handleSubmit = e => {
    if (this.props.searchParams.phoneNumber) {
      this.props.form.setFieldsValue({
        searchInfo: this.props.searchParams.phoneNumber,
      });
    }
    this.props.getAsyncList(
      Object.assign(
        {
          skipCount: this.state.skipCount,
          pageSize: this.state.pageSize,
        },
        this.props.form.getFieldsValue(),
      ),
    );
  };

  public selectChange = (key, value) => {
    const ops = {};
    ops[key] = value;
    this.props.form.setFieldsValue(ops);
    this.setState({ current: 1, skipCount: 0 }, this.handleSubmit);
  };

  public handleReset = () => {
    this.props.form.resetFields();
    this.setState({ current: 1, skipCount: 0 }, this.handleSubmit);
  };

  public renderFilterForm() {
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    return (
      <Form className={styles.form} layout="inline">
        <Row type="flex">
          <Col span={24}>
            <FormItem label="车辆绑定">
              {getFieldDecorator('bindingStatus', {
                initialValue: '',
              })(
                <Select
                  style={{ width: 120 }}
                  onChange={this.selectChange.bind(this, 'bindingStatus')}
                >
                  <Option value="">全部</Option>
                  <Option value="0">未绑定</Option>
                  <Option value="1">已绑定</Option>
                </Select>,
              )}
            </FormItem>
            <FormItem label="添加方式">
              {getFieldDecorator('addMethod', {
                initialValue: '',
              })(
                <Select
                  style={{ width: 120 }}
                  onChange={this.selectChange.bind(this, 'addMethod')}
                >
                  <Option value="">全部</Option>
                  <Option value="1">后台添加</Option>
                  <Option value="2">快捷派单</Option>
                </Select>,
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('searchInfo')(
                <Input
                  style={{ width: 220 }}
                  placeholder="司机姓名/手机号/绑定车辆"
                  onKeyDown={e => {
                    if (e.keyCode === 13) {
                      this.selectChange(
                        'searchInfo',
                        this.props.form.getFieldValue('searchInfo'),
                      );
                    }
                  }}
                />,
              )}
              <Button
                type="primary"
                style={{ marginLeft: 16, marginRight: 12 }}
                onClick={this.selectChange.bind('searchInfo')}
              >
                查询
              </Button>
              <Button onClick={this.handleReset}>重置</Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  public renderTable() {
    const { dataCount, list, loading } = this.props;
    const tableProps = {
      rowKey: 'utmsUsersDriversId',
      columns: [
        {
          title: '司机姓名',
          dataIndex: 'driverName',
          width: 120,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '手机号码',
          dataIndex: 'phoneNumber',
          width: 130,
        },
        {
          title: '车牌号',
          dataIndex: 'plateNumber',
          width: 120,
          render: (text, record) =>
            record.plateNumber ? (
              <a href="javascript:;" onClick={this.details.bind(this, record)}>
                {record.plateNumber}
              </a>
            ) : (
              GLOBAL.emptyRecord
            ),
        },
        {
          title: '车型车长',
          key: 'vehicleType',
          width: 120,
          render: record => (
            <span>
              {record.vehicleLong
                ? record.vehicleLong / 1000 + '米' + record.vehicleType
                : GLOBAL.emptyRecord}
            </span>
          ),
        },
        {
          title: '驾照类型',
          dataIndex: 'driverLicenseType',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '驾龄',
          dataIndex: 'drivingAge',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '年龄',
          dataIndex: 'age',
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '添加方式',
          dataIndex: 'addMethod',
          render: text => <span>{text === 1 ? '后台添加' : '快捷派单'}</span>,
        },
        {
          title: '添加时间',
          dataIndex: 'createDate',
          width: 160,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '最后登录时间',
          dataIndex: 'lastLoginDate',
          width: 160,
          render: (text, record) => text || GLOBAL.emptyRecord,
        },
        {
          title: '操作',
          fixed: 'right',
          width: 130,
          render: (text, record) => (
            <span>
              <a
                href="javascript:;"
                onClick={this.editDriver.bind(this, record)}
              >
                编辑
              </a>
              <Divider type="vertical" />
              <Popconfirm
                placement="bottom"
                title={'是否删除该司机'}
                onConfirm={this.deleteConfirm.bind(this, record)}
                okText="是"
                cancelText="否"
              >
                <a href="javascript:;">删除</a>
              </Popconfirm>
            </span>
          ),
        },
      ],
      scroll: { x: 1300 },
      dataSource: list.toJS(),
      loading,
      pagination: {
        current: this.state.current,
        total: dataCount,
        pageSize: this.state.pageSize,
        onChange: (pageData, index) => {
          this.setState(
            {
              pageSize: pageData.pageSize,
              skipCount: pageData.skipCount,
              current: pageData.current,
            },
            this.handleSubmit,
          );
        },
        onShowSizeChange: pageData => {
          this.setState(
            {
              pageSize: pageData.pageSize,
              skipCount: 0,
              current: 1,
            },
            this.handleSubmit,
          );
        },
      },
    };
    return <Table {...tableProps} />;
  }

  public saveFormRef = formRef => {
    this.formRef = formRef;
  };

  public render() {
    const { data } = this.props;
    return (
      <Card title="司机管理" className={styles.ManageDriver}>
        <Button
          type="primary"
          className={styles.buttonRight}
          onClick={this.addDriver}
        >
          <Icon type="usergroup-add" theme="outlined" />
          添加司机
        </Button>
        {this.renderFilterForm()}
        <div>{this.renderTable()}</div>
        <ModalForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleOk}
          title={this.state.ModalTitle}
          confirmLoading={this.state.confirmLoading}
          utmsUsersDriversId={this.state.utmsUsersDriversId}
          vehicleDataState={this.state.vehicleDataState}
          handleSubmit={this.handleSubmit}
        />

        <DetailsModule
          visible={this.state.detailsVisible}
          detailData={this.state.detailData}
          onCancel={this.detailsCancel}
          onCreate={this.detailsCancel}
        />
      </Card>
    );
  }
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        list: makeSelectData(currentState),
        dataCount: makeSelectDataCount(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getAsyncList: options => dispatch(actions.getAsyncList(options)),
      }),
    };
  })(ManageDriver);
};
