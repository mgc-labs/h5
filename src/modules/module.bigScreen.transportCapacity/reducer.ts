import { fromJS, List, Map } from 'immutable';

import * as c from './constants';

const initialState = Map({
  getWarehouseLoading: false,
  getWarehouseError: null,
  warehouseList: [],

  getWeekInfoLoading: false,
  getWeekInfoError: null,
  weekInfo: [],

  getTodayDataLoading: false,
  getTodayError: null,
  todayData: {},

  getCarListLoading: false,
  carList: [],
  carListError: null,

  theme: 1,
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    // 仓库
    case c.GET_WAREHOUSE:
      return state
        .set('getWarehouseLoading', true)
        .set('getWarehouseError', false);
    case c.GET_WAREHOUSE_SUCCESS:
      const warehouseList = action.payload || [];
      // const defaultWarehouse = warehouseList.filter(h => h.isDefault)[0];
      return state
        .set('warehouseList', warehouseList)
        .set('getWarehouseLoading', false)
        .set('getWarehouseError', false);
    case c.GET_WAREHOUSE_ERROR:
      return state
        .set('getWarehouseLoading', false)
        .set('getWarehouseError', action.error);

    // 近7日用车数
    case c.GET_WEEKINFO:
      return state
        .set('getWeekInfoLoading', true)
        .set('getWeekInfoError', false);
    case c.GET_WEEKINFO_SUCCESS:
      return state
        .set('weekInfo', action.payload || [])
        .set('getWeekInfoLoading', false)
        .set('getWeekInfoError', false);
    case c.GET_WEEKINFO_ERROR:
      return state
        .set('getWeekInfoLoading', false)
        .set('getWeekInfoError', action.error);

    // 车辆位置
    case c.GET_CAR:
      return state.set('getCarListLoading', true).set('carListError', false);
    case c.GET_CAR_SUCCESS:
      return state
        .set('getCarListLoading', false)
        .set('carList', action.payload || []);
    case c.GET_CAR_ERROR:
      return state
        .set('getCarListLoading', false)
        .set('carListError', action.error);

    // 今日统计数据
    case c.GET_TODAYDATA:
      return state.set('getTodayDataLoading', true).set('getTodayError', false);
    case c.GET_TODAYDATA_SUCCESS:
      return state
        .set('getTodayDataLoading', false)
        .set('todayData', action.payload || {});
    case c.GET_TODAYDATA_ERROR:
      return state
        .set('getTodayError', action.error)
        .set('getTodayDataLoading', false);

    // 主题色修改
    case c.THEME_CHANGE:
      return state.set('theme', action.payload);
    default:
      return state;
  }
};
export default reducer;
