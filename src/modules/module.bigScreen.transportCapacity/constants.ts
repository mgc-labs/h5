/**
 * Ios Constants
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */

export const KEY = 'bigScreen.transportCapacity';

// 获取仓库
export const GET_WAREHOUSE = 'BSTC/GET_WAREHOUSE';
export const GET_WAREHOUSE_SUCCESS = 'BSTC/GET_WAREHOUSE_SUCCESS';
export const GET_WAREHOUSE_ERROR = 'BSTC/GET_WAREHOUSE_ERROR';

// 获取近7日用车数
export const GET_WEEKINFO = 'BSTC/GET_WEEKINFO';
export const GET_WEEKINFO_SUCCESS = 'BSTC/GET_WEEKINFO_SUCCESS';
export const GET_WEEKINFO_ERROR = 'BSTC/GET_WEEKINFO_ERROR';

// 获取今日数据
export const GET_TODAYDATA = 'BSTC/GET_TODAYDATA';
export const GET_TODAYDATA_SUCCESS = 'BSTC/GET_TODAYDATA_SUCCESS';
export const GET_TODAYDATA_ERROR = 'BSTC/GET_TODAYDATA_ERROR';

// 主题色修改
export const THEME_CHANGE = 'BSTC/THEME_CHANGE';

// 获取车辆
export const GET_CAR = 'BSTC/GET_CAR';
export const GET_CAR_SUCCESS = 'BSTC/GET_CAR_SUCCESS';
export const GET_CAR_ERROR = 'BSTC/GET_CAR_ERROR';
