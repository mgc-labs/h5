export default {
  result: 'success',
  count: 18,
  data: {
    totalPOWO: '1231', // 总订单
    distributionPOWO: '21321', // 运输中订单
    haveSignedInPOWO: '21312', // 已签收订单
    notSignedPOWO: '1231', // 未签收订单
    totalTO: '21321', // 总运单(用车)
    pendingSchedulingTO: '21312', // 带调度运单
    distributionTO: '1231', // 运输中运单
    finishedTO: '21321', // 已完成运单
  },
  code: 'EHD123456789123',
  msg: '统计成功 ',
};
