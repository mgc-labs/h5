function random() {
  return Math.ceil(Math.random() * 100);
}

export default {
  result: 'success',
  count: 2,
  data: [
    { date: '2018-10-17', tradeCount: random() },
    { date: '2018-10-18', tradeCount: random() },
    { date: '2018-10-19', tradeCount: random() },
    { date: '2018-10-20', tradeCount: random() },
    { date: '2018-10-21', tradeCount: random() },
    { date: '2018-10-22', tradeCount: random() },
    { date: '2018-10-23', tradeCount: random() },
  ],
  code: '',
  msg: '地址列表查询成功 ',
};
