/**
 * Ios Actions
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */
import * as c from './constants';

// 获取仓库列表
export const getWarehouse = () => ({
  type: c.GET_WAREHOUSE,
});

export const getWarehouseDone = data => ({
  type: c.GET_WAREHOUSE_SUCCESS,
  payload: data,
});

export const getWarehouseError = error => ({
  type: c.GET_WAREHOUSE_ERROR,
  error,
});

// 获取近7日用车数
export const getWeekInfo = () => ({
  type: c.GET_WEEKINFO,
});

export const getWeekInfoDone = data => ({
  type: c.GET_WEEKINFO_SUCCESS,
  payload: data,
});

export const getWeekInfoError = error => ({
  type: c.GET_WEEKINFO_ERROR,
  error,
});

// 获取今日统计数据
export const getTodayData = () => ({
  type: c.GET_TODAYDATA,
});

export const getTodayDataDone = data => ({
  type: c.GET_TODAYDATA_SUCCESS,
  payload: data,
});

export const getTodayDataError = error => ({
  type: c.GET_TODAYDATA_ERROR,
  error,
});

// 主题色修改
export const themeChanged = data => ({
  type: c.THEME_CHANGE,
  payload: data,
});

// 获取车辆数据
export const getCarList = () => ({
  type: c.GET_CAR,
});

export const getCarListDone = data => ({
  type: c.GET_CAR_SUCCESS,
  payload: data,
});

export const getCarListError = error => ({
  type: c.GET_CAR_ERROR,
  error,
});
