import classnames from 'classnames';
import debounce from 'lodash/debounce';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import * as actions from './actions';
import BigScreenBottom from './bottom/bigScreenBottom';
import * as constants from './constants';
import styles from './css/index.module.less';
import BigScreenMap from './map/bigScreenMap';
import { CarsInfoListComponent } from './map/components/carsInfoList/carsInfoList';
import reducer from './reducer';
import saga from './saga';
import { makeSelectTheme } from './selectors';

class TransportCapacity extends React.PureComponent<any, any> {
  private resizeDebounced: () => void;
  private screenMap: any;
  private screenBottom: any;
  private divBigScreen: HTMLDivElement;
  private carsInfoList: HTMLDivElement;

  constructor(options) {
    super(options);
    this.state = {
      carsInfoList: [],
    };

    this.resizeDebounced = debounce(() => {
      this.screenMap.getWrappedInstance().setSize();
      this.screenBottom.getWrappedInstance().resize();

      this.setRootFontSize();
    }, 500);
  }

  componentDidMount() {
    this.screenMap
      .getWrappedInstance()
      .setBottom(this.screenBottom.getWrappedInstance().getHeight());

    window.addEventListener('resize', this.onResize);

    this.setRootFontSize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  getSize() {
    return {
      width: window.innerWidth,
      height: window.innerHeight,
    };
  }

  setRootFontSize() {
    const size = this.getSize();
    let fontSize;
    if (size.width > 1280) {
      fontSize = (size.width / 1920) * 100;
    } else {
      fontSize = (1280 / 1920) * 100;
    }

    document.querySelector('html').style.fontSize = fontSize + 'px';
  }

  onThemeChanged = theme => {
    this.props.themeChanged(theme);
  };

  onFullScreen = isFullScreen => {
    const element = this.divBigScreen as any;
    const fullScreen =
      element.requestFullScreen || // W3C
      element.webkitRequestFullScreen || // Chrome等
      element.mozRequestFullScreen || // FireFox
      element.msRequestFullScreen; // IE11

    const doc = document as any;
    const extFullScreen =
      doc.exitFullscreen || // W3C
      doc.mozCancelFullScreen || // Chrome等
      doc.webkitExitFullscreen || // FireFox
      doc.webkitExitFullscreen; // IE11

    this.setState({
      isFullScreen,
    });

    if (isFullScreen) {
      if (fullScreen) {
        fullScreen.call(element);
      }
    } else {
      if (extFullScreen) {
        extFullScreen.call(document);
      }
    }
  };

  onClickOfMultiple = d => {
    this.setState(
      {
        carsInfoList: d.subPoints,
      },
      () => {
        this.carsInfoList.style.display = 'block';
      },
    );
  };

  onCarsInfoListClose = () => {
    this.carsInfoList.style.display = 'none';
  };

  onResize = () => {
    this.resizeDebounced();
    this.screenMap
      .getWrappedInstance()
      .setBottom(this.screenBottom.getWrappedInstance().getHeight());
  };

  render() {
    const state = this.state;

    return (
      <div
        className={classnames(styles.divBigScreen, {
          bstc__theme2: this.props.theme === 2,
        })}
        ref={obj => {
          this.divBigScreen = obj;
        }}
      >
        <div className={styles.divLogo} />
        <BigScreenMap
          clickOfMultiple={this.onClickOfMultiple}
          partyid={state.partyid}
          ref={obj => {
            this.screenMap = obj;
          }}
        />
        <div className={styles.divTheme}>
          <div
            className={styles.divThemeBtn1}
            onClick={() => this.onThemeChanged(1)}
          />
          <div
            className={styles.divThemeBtn2}
            onClick={() => this.onThemeChanged(2)}
          />
        </div>
        <div className={styles.divBottomContainer}>
          <BigScreenBottom
            ref={obj => {
              this.screenBottom = obj;
            }}
          />
        </div>
        {state.isFullScreen ? (
          <div
            className={styles.btnFullScreenEx}
            onClick={() => this.onFullScreen(false)}
          />
        ) : (
          <div
            className={styles.btnFullScreen}
            onClick={() => this.onFullScreen(true)}
          />
        )}
        <div
          className={styles.carsInfoList}
          ref={obj => {
            this.carsInfoList = obj;
          }}
        >
          <CarsInfoListComponent
            dataList={state.carsInfoList}
            close={this.onCarsInfoListClose}
          />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  themeChanged: theme => dispatch(actions.themeChanged(theme)),
});
const mapStateToProps = createStructuredSelector({
  theme: makeSelectTheme(),
});
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({
  key: constants.KEY,
  reducer,
});
const withSaga = injectSaga({
  key: constants.KEY,
  saga,
});

export default compose(
  withReducer,
  withConnect,
  withSaga,
)(TransportCapacity);
