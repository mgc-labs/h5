import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import styles from './styles.module.less';
import Title from './title';
import TodayData from './todayData';
import UseCarOfLast7Days from './useCarOfLast7Days';

export interface IBigScreenBottomProps {}

class BigScreenBottom extends React.PureComponent<IBigScreenBottomProps, any> {
  private divBottom: HTMLDivElement;
  private useCarOfLast7Days: any;

  resize() {
    this.useCarOfLast7Days.getWrappedInstance().setSize();
  }

  getHeight() {
    return this.divBottom.offsetHeight;
  }

  render() {
    return (
      <div
        className={styles.divBottom}
        ref={obj => {
          this.divBottom = obj;
        }}
      >
        <UseCarOfLast7Days
          ref={obj => {
            this.useCarOfLast7Days = obj;
          }}
        />
        <Title />
        <TodayData />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({});
const mapStateToProps = createStructuredSelector({});
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
);

export default compose(withConnect)(BigScreenBottom);
