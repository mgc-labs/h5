import * as echarts from 'echarts';
import moment from 'moment';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import * as actions from '../../actions';
import { makeSelectTheme, makeSelectWeekInfo } from '../../selectors';
import styles from './styles.module.less';

export interface IUseCarOfLast7DaysProps {
  theme: number;
  weekInfo: any[];
  getWeekInfo: () => void;
}

class UseCarOfLast7Days extends React.PureComponent<
  IUseCarOfLast7DaysProps,
  any
> {
  private div: HTMLDivElement;
  private chart: echarts.ECharts;
  private lastFetchDate: string;
  private timer: any;
  componentDidMount() {
    this.chart = echarts.init(this.div);
    this.setBaseData(this.props.theme);
    this.beginUpdate();
  }
  componentWillUnmount() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.theme !== nextProps.theme) {
      this.setBaseData(nextProps.theme);
    }
    if (this.props.weekInfo !== nextProps.weekInfo) {
      this.lastFetchDate = this.getToday();
      let list = nextProps.weekInfo || [];
      list = list.slice(list.length - 7);
      this.setData(list);
    }
  }

  setSize() {
    this.chart.resize();
  }

  beginUpdate() {
    const run = () => {
      if (this.lastFetchDate !== this.getToday()) {
        this.props.getWeekInfo();
      }
      this.timer = setTimeout(() => {
        run();
      }, 1000);
    };
    run();
  }

  getToday() {
    return moment().format('YYYY.MM.DD');
  }

  setBaseData(theme: number) {
    const option = {
      title: {
        show: false,
        text: '近7日用车数',
        left: 'center',
      },
      tooltip: {},
      legend: {
        show: false,
        data: ['销量'],
      },
      grid: {
        left: '12%',
        right: '45',
        bottom: 25,
      },
      xAxis: {
        name: '日期',
        nameTextStyle: { color: theme === 1 ? '#a4ceff' : '#4D4D4D' },
        axisLabel: { color: theme === 1 ? '#a4ceff' : '#4D4D4D' },
        type: 'category',
      },
      yAxis: {
        name: '用车量',
        axisLabel: { color: theme === 1 ? '#ffffff' : '#4D4D4D' },
        nameTextStyle: { color: theme === 1 ? '#a4ceff' : '#4D4D4D' },
        splitLine: { show: false },
      },
      series: [
        {
          color: ['#215fe9'],
          name: '用车量',
          type: 'bar',
          label: {
            show: true,
            position: 'top',
            // position: "inside",
            color: theme === 1 ? '#a4ceff' : '#4D4D4D',
          },
        },
      ],
    };

    this.chart.setOption(option);
  }

  setData(data) {
    if (data && data.length) {
      // const chartWidth = this.div.offsetWidth;

      this.chart.setOption({
        dataset: {
          source: data,
        },
      });
    }
  }

  render() {
    return (
      <div className={styles.divLeft}>
        <div
          className={styles.divUserOf7days}
          ref={obj => {
            this.div = obj;
          }}
        />
        <div className={styles.title}>近7日用车数</div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getWeekInfo: () => dispatch(actions.getWeekInfo()),
});
const mapStateToProps = createStructuredSelector({
  weekInfo: makeSelectWeekInfo(),
  theme: makeSelectTheme(),
});
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
);

export default compose(withConnect)(UseCarOfLast7Days);
