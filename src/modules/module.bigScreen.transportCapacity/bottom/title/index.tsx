import moment from 'moment';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import * as constants from '../../constants';
import { makeSelectAccountInfo } from '../../selectors';
import styles from './styles.module.less';

interface ITitleProps {
  accountInfo: any;
}

class Title extends React.PureComponent<ITitleProps, any> {
  private timer: any;
  constructor(options) {
    super(options);
    this.state = {
      date: this.getToday(),
      time: moment().format('HH:mm:ss'),
    };
  }
  componentDidMount() {
    this.beginUpdateTime();
  }
  componentWillUnmount() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  beginUpdateTime = () => {
    const run = () => {
      this.setState({
        date: this.getToday(),
        time: moment().format('HH:mm:ss'),
      });
      this.timer = setTimeout(() => {
        run();
      }, 1000);
    };
    run();
  };
  getToday = () => {
    return moment().format('YYYY.MM.DD');
  };

  formatCompanyName(companyName) {
    if (companyName.length > 20) {
      companyName = companyName.substr(0, 20) + '...';
    }
    return companyName;
  }

  getCompanyName() {
    const { accountInfo } = this.props;
    let companyName = accountInfo.get('headOrganizationName');
    const key = constants.KEY + '.companyName';
    if (localStorage) {
      if (companyName) {
        localStorage.setItem(key, companyName);
      } else {
        companyName = localStorage.getItem(key);
      }
    }
    return this.formatCompanyName(companyName);
  }
  render() {
    const { date, time } = this.state;
    const companyName = this.getCompanyName();
    return (
      <div className={styles.divMiddle}>
        <div className={styles.divT}>
          <div className={styles.divCompanyName}>{companyName}</div>
        </div>
        <div className={styles.divTitle}>运力实时监控大屏</div>
        <div className={styles.divDateTime}>
          <div className={styles.divDate}>{date}</div>
          <div className={styles.divTime}>{time}</div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({});
const mapStateToProps = createStructuredSelector({
  accountInfo: makeSelectAccountInfo(),
});
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
);

export default compose(withConnect)(Title);
