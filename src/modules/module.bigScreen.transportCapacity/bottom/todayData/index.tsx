import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import * as actions from '../../actions';
import { makeSelectTheme, makeSelectTodayData } from '../../selectors';
import styles from './styles.module.less';

export interface ITodayDataProps {
  todayData: any;
  getTodayData: () => void;
}

class TodayData extends React.PureComponent<ITodayDataProps, any> {
  private timer: any;
  componentDidMount() {
    this.beginUpdateTodayData();
  }
  componentWillUnmount() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }
  beginUpdateTodayData() {
    const run = () => {
      this.props.getTodayData();
      this.timer = setTimeout(run, 5 * 60 * 1000); // 5分钟
    };
    run();
  }
  render() {
    const { todayData } = this.props;
    return (
      <div className={styles.divRight}>
        <div className={styles.title1}>当日用车数据</div>
        <div className={styles.divTysz}>运输中</div>
        <div className={styles.divYsz}>{todayData.distributionTO}</div>
        <div className={styles.divTjh}>总用车</div>
        <div className={styles.divJh}>{todayData.totalPOWO}</div>
        <div className={styles.divTdqd}>待调度</div>
        <div className={styles.divDqd}>{todayData.pendingSchedulingTO}</div>
        <div className={styles.divTyswc}>已完成</div>
        <div className={styles.divYswc}>{todayData.finishedTO}</div>
        <div className={styles.title2}>当日订单数据</div>
        <div className={styles.divTdds}>订单数</div>
        <div className={styles.divDds}>{todayData.totalTO}</div>
        <div className={styles.divTytt}>
          <div className={styles.point} />
          配送中
        </div>
        <div className={styles.divYtt}>{todayData.distributionPOWO}</div>
        <div className={styles.divTpsz}>
          <div className={styles.point} />
          已签收
        </div>
        <div className={styles.divPsz}>{todayData.haveSignedInPOWO}</div>
        <div className={styles.divTwtt}>
          <div className={styles.point} />
          未签收
        </div>
        <div className={styles.divWtt}>{todayData.notSignedPOWO}</div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getTodayData: () => dispatch(actions.getTodayData()),
});
const mapStateToProps = createStructuredSelector({
  todayData: makeSelectTodayData(),
  theme: makeSelectTheme(),
});
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
);

export default compose(withConnect)(TodayData);
