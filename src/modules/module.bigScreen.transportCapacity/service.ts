/**
 * Auth Service
 * @author djd
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_GET_WAREHOUSE =
  '/ehuodiGateway/utmsCore/utmsDepotcs/selectDepotList?a=3';

const API_GET_WEEKINFO =
  '/ehuodiGateway/utmsTrade/utmsTrade/statUseCarConditionThirty?a=2';

const API_GET_TODAY = '/ehuodiGateway/utmsTrade/bigScreen/todayCount';

const API_GET_CAR =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/selectRealtimeDriversInfo?coordinateType=BD09';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码

  mocker
    .on('post', API_GET_WAREHOUSE)
    .reply(() =>
      import('./mock/getWarehouse').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_GET_WEEKINFO)
    .reply(() =>
      import('./mock/getCountWeekInfo').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_GET_TODAY)
    .reply(() =>
      import('./mock/getTodayData').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_GET_CAR)
    .reply(() =>
      import('./mock/getCar').then(exports => [200, exports.default]),
    );
}

/**
 * 获取仓库列表
 */
export const askGetWarehouse = params =>
  request(
    {
      method: 'post',
      url: API_GET_WAREHOUSE,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 获取近7日用车数
 */
export const askWeekInfo = params =>
  request(
    {
      method: 'post',
      url: API_GET_WEEKINFO,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 获取今日统计数据
 */
export const askTodayData = params =>
  request(
    {
      method: 'post',
      url: API_GET_TODAY,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 获取车辆数据
 */
export const askCar = params =>
  request(
    {
      method: 'post',
      url: API_GET_CAR,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);
