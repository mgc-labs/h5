import styles from './car.module.less';

declare const BMap: any;

export class CarPoint extends BMap.Overlay {
  constructor({ point, data }) {
    super();
    this.point = point;
    this.data = data;
    this.pointWidth = 64;
    this.pointHeight = this.pointWidth;
  }

  initialize(map) {
    this.map = map;

    const div = document.createElement('div');

    this.map.getPanes().labelPane.appendChild(div);
    this.div = div;

    this.divCarInfo = document.createElement('div');
    this.divCarInfo.className = styles['bg-car-info'];
    div.appendChild(this.divCarInfo);

    this.divCarPoint = document.createElement('div');
    if (this.data.busyStatus) {
      this.divCarPoint.className = styles['bg-car-poing-m'];
      div.className = styles['bg-car-m'];
    } else {
      this.divCarPoint.className = styles['bg-car-poing-x'];
      div.className = styles['bg-car-x'];
    }
    div.appendChild(this.divCarPoint);

    return div;
  }

  draw() {
    const pixel = this.map.pointToOverlayPixel(this.point);
    const x = pixel.x - this.pointWidth / 2;
    const y = pixel.y - this.pointHeight / 2;
    this.div.style.left = x + 'px';
    this.div.style.top = y + 'px';

    // 线路名称信息显示框
    this.divCarInfo.style.top = '-21px';
    const routeName = this.data.routeName;
    let width = 0;

    if (this.data.pointCount > 1) {
      // subPoints
      width = 150;
      this.div.style.cursor = 'default';
      this.divCarInfo.style.width = `${width}px`;
      this.divCarInfo.style.maxWidth = `${width}px`;
      this.divCarInfo.style.paddingRight = '20px';

      const divBtn = document.createElement('div');
      divBtn.className = styles['bg-car-info-btn'];

      const span = document.createElement('span');
      span.innerHTML = `当前位置有${this.data.pointCount}辆车, 点击查看`;
      this.divCarInfo.innerHTML = '';
      this.divCarInfo.appendChild(span);
      this.divCarInfo.appendChild(divBtn);
      this.divCarInfo.style.display = 'block';
      this.div.onmouseover = this.div.onmouseenter = this.div.onmousemove = this.div.onmouseout = null;
      this.divCarInfo.onclick = () => {
        // this.groutPoint
        if (this.clickOfMultiple) {
          this.clickOfMultiple(this.data);
        }
      };
    } else {
      this.div.style.cursor = 'pointer';
      width = 157;
      if (routeName) {
        this.divCarInfo.innerHTML = routeName;
        this.divCarInfo.style.display = 'block';
      } else {
        this.divCarInfo.style.display = 'none';
      }

      this.divCarPoint.onmouseover = this.divCarPoint.onmouseenter = this.divCarPoint.onmousemove = () => {
        if (this.mouseover) {
          this.mouseover(this);
        }
      };

      this.divCarPoint.onmouseout = () => {
        if (this.mouseout) {
          this.mouseout(this);
        }
      };
    }

    this.divCarInfo.style.left = -width / 2 + this.pointWidth / 2 + 'px';
  }

  setPosition(point) {
    this.point = point;
  }

  destroy() {
    // this.map.getPanes().labelPane.remove(this.div);
    this.div.onclick = null;
    this.map = null;
  }
}
