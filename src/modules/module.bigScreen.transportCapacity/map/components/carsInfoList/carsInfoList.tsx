import * as React from 'react';
import styles from './carsInfoList.module.less';

export interface ICarsInfoListComponentProps {
  dataList: any[];
  close: () => void;
}

export class CarsInfoListComponent extends React.PureComponent<
  ICarsInfoListComponentProps,
  any
> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onclose = () => {
    if (this.props.close) {
      this.props.close();
    }
  };

  render() {
    const carItemList = this.props.dataList.map((m, index) => {
      return (
        <div className={styles.carsInfoItem} key={index}>
          <div className={styles.localAddress}>
            当前位置：
            {m.coordinateName || ''}
          </div>
          <div className={styles.newSignal}>
            最新信号：
            {m.lastUploadPositionDate || ''}
          </div>
          <div className={styles.carsInfoItemDetail}>
            <div className={styles.routeName}>{m.routeName || ''}</div>
            <div className={styles.driver}>
              司机信息：
              {(m.driverName || '') + ' ' + (m.phoneNumber || '')}
            </div>
            <div className={styles.plate}>
              车牌号码：
              {m.plateNumber || ''}
            </div>
            <div className={styles.carI}>
              车辆信息：
              {m.vehicleType || ''}
            </div>
          </div>
          {index < this.props.dataList.length - 1 ? (
            <div className={styles.splitLine} />
          ) : (
            ''
          )}
        </div>
      );
    });

    return (
      <div className={styles.carsInfoList}>
        <div className={styles.title}>共有 {carItemList.length} 辆车</div>
        <div className={styles['btn-close']} onClick={this.onclose} />
        <div className={styles['div-scroll']}>{carItemList}</div>
      </div>
    );
  }
}
