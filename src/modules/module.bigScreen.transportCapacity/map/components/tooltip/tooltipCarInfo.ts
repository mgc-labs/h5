import styles from './tooltipCarInfo.module.less';

declare const BMap: any;
const pointWidth = 32;

export class TooltipCarInfo extends BMap.Overlay {
  constructor() {
    super();
    this.width = 312;
    // this.height = 201;
  }

  initialize(map) {
    this.map = map;

    const div = document.createElement('div');
    div.className = styles['bg-tooltip-carinfo'];
    // $(div)
    //   .on('click', '.to-detail', () => {
    //     const orderId = this.data.tradeNumber;
    //     const url = `${window.location.protocol}//${
    //       window.location.host
    //     }/#/orderDetail?tab=${encodeURIComponent('全部')}&order_id=${orderId}`;
    //     // document.location.href = url;
    //     window.open(url);
    //   })
    //   .mouseenter(() => {
    //     if (this.mouseenter) {
    //       this.mouseenter();
    //     }
    //   })
    //   .mouseleave(() => {
    //     if (this.mouseleave) {
    //       this.mouseleave();
    //     }
    //   });

    this.divCurrow = document.createElement('div');
    this.divCurrow.className = styles['bg-tooltip-carinfo-curr'];
    this.div = div;
    this.div.style.width = this.width + 'px';
    // this.div.style.height = this.height + "px";
    this.map.getPanes().labelPane.appendChild(this.div);
    return this.div;
  }

  draw() {
    const pixel = this.map.pointToOverlayPixel(this.point);

    const height = this.div.offsetHeight;

    // 框位置
    let fx = pixel.x - this.width / 2;
    let fy = pixel.y - height - pointWidth / 2 - 10;

    // 箭头位置
    let cx = this.width / 2 - 14;
    let cy = height - 5;

    // 下面的做法，没有找到官方的做法，只能自己写
    const posDiv = this.div.parentElement.parentElement.parentElement;
    if (posDiv) {
      const container = this.map.getContainer();
      const mWidth = container.offsetWidth;
      const mHeight = container.offsetHeight;

      const mx = fx + posDiv.offsetLeft;
      const mt = fy + posDiv.offsetTop;

      if (mx < 0) {
        // 如果左边距超出
        fx = pixel.x - this.width / 2 - mx;
        cx = cx + mx;
      } else if (mx + this.width > mWidth) {
        // 如果右边距超出
        fx = pixel.x - this.width / 2 - (this.width + mx - mWidth) - 4;
        cx = cx + (this.width + mx - mWidth) + 3;
      }
      if (mt < 0) {
        fy = pixel.y + pointWidth / 2 + 10;
        cy = -12;
        this.divCurrow.style.transform = 'rotate(180deg)';
      } else {
        this.divCurrow.style.transform = 'rotate(0deg)';
      }
    }

    this.div.style.left = fx + 'px';
    this.div.style.top = fy + 'px';

    this.divCurrow.style.left = cx + 'px';
    this.divCurrow.style.top = cy + 'px';
  }

  show() {
    super.show();
    this.draw();
  }

  setPosition(point) {
    this.point = point;
  }

  setData(data) {
    this.data = data;
    this.div.innerHTML = '';
    this.div.appendChild(this.divCurrow);

    const bgFrame = document.createElement('div');
    bgFrame.className = styles['bg-frame'];
    bgFrame.innerHTML = `
    <div>
      <div class="${styles['bg-address-time']}">
        <div class="${styles['bg-address']}">当前位置：${data.coordinateName ||
      ''}</div>
        <div class="${
          styles['bg-time']
        }">最新信号：${data.lastUploadPositionDate || ''}</div>
      </div>
      <div class="${styles['bg-order-info']}">
        <div class="${styles['bg-info-frame']}">
          <div>司机信息：${data.driverName || ''} ${data.phoneNumber ||
      ''}</div>
          <div class="${styles.status}">${
      data.busyStatus ? '忙碌' : '空闲'
    }</div>
          <div>车牌号码：${data.plateNumber || ''}</div>
          <div>车辆信息：${data.vehicleType || ''}</div>
        </div>
      </div>
    </div>
  `;

    this.div.appendChild(bgFrame);
  }
}
