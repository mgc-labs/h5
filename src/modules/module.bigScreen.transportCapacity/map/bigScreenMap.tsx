import debounce from 'lodash/debounce';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import * as actions from '../actions';
import imgWarehouse1 from '../map/img/imgWarehouse_1.svg';
import {
  makeSelectCarList,
  makeSelectTheme,
  makeSelectWarehouseList,
} from '../selectors';
import styles from './bigScreenMap.module.less';
import { mapStyle } from './bigScreenMapConfig';
import { CarPoint } from './components/car/car';
import { TooltipCarInfo } from './components/tooltip/tooltipCarInfo';

const timeIntervalGetCarList = 15; // 15秒

declare const BMap: any;
declare const BMapLib: any;

export interface IBigScreenMapProps {
  theme: number;
  warehouseList: any;
  carList: any;
  getWarehouseList: () => void;
  getCarList: () => void;
  clickOfMultiple: (value: any) => void;
}

interface IPlay {
  id: number; // 司机编号
  isPlayed: boolean; // 是否已轮播
}

class BigScreenMap extends React.PureComponent<IBigScreenMapProps, any> {
  private bodyMouseMoveDebounced: () => void;
  private divMap: HTMLDivElement;
  private map: any;
  private warehousePointList: any[] = [];
  private bottom: number;
  private carImgList: any[] = null;
  private carList: any[] = null;
  private distanceOfzoom: any;
  private carInfoWindowInfo: TooltipCarInfo;
  private playList: IPlay[] = []; // 轮播
  private playTimer: any;
  private fetchCarListTimer: any;
  private isFetchCarStoped: boolean = false; // 是否暂停刷新车辆信息

  onBodyMouseMove = () => {
    this.isFetchCarStoped = true;
    this.bodyMouseMoveDebounced();
  };

  componentDidMount() {
    this.initDistanceOfZoom();
    this.initMap();

    this.bodyMouseMoveDebounced = debounce(() => {
      this.isFetchCarStoped = false;
    }, 60 * 1000);

    document.body.addEventListener('mousemove', this.onBodyMouseMove);

    this.props.getWarehouseList();

    this.beginGetCarList();
    this.beginPlay();
  }
  componentWillUnmount() {
    document.body.removeEventListener('mousemove', this.onBodyMouseMove);
    if (this.fetchCarListTimer) {
      clearTimeout(this.fetchCarListTimer);
    }
    if (this.playTimer) {
      clearTimeout(this.playTimer);
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.theme !== nextProps.theme) {
      if (nextProps.theme === 1) {
        this.map.setMapStyle(mapStyle);
      } else {
        this.map.setMapStyle({});
      }
      this.map.reset();
    }

    if (this.props.warehouseList !== nextProps.warehouseList) {
      this.fillWarehouse(nextProps.warehouseList);
    }
    if (this.props.carList !== nextProps.carList) {
      this.carList = nextProps.carList;
      this.mergePlayList();
      this.drawCarPointsAndSetViewport();
    }
  }

  beginGetCarList() {
    if (this.fetchCarListTimer) {
      clearTimeout(this.fetchCarListTimer);
    }
    const run = () => {
      if (!this.isFetchCarStoped) {
        this.props.getCarList();
      }
      this.fetchCarListTimer = setTimeout(run, timeIntervalGetCarList * 1000); // 15秒
    };
    run();
  }

  mergePlayList() {
    const drivers = this.carList.map(h => h.utmsDriverId);

    // 删除不存在的
    this.playList = this.playList.filter(h => drivers.includes(h.id));

    // 插入新增的
    const addedPlayList = this.carList
      .filter(h => !this.playList.some(p => p.id === h.utmsDriverId))
      .map(h => ({
        id: h.utmsDriverId,
        isPlayed: false,
      }));
    this.playList = this.playList.concat(addedPlayList);
  }

  beginPlay() {
    if (this.playTimer) {
      clearTimeout(this.playTimer);
    }

    const run = () => {
      // 获取没有轮播过的车辆
      if (!this.isFetchCarStoped) {
        if (this.playList.length) {
          let play = this.playList.find(h => !h.isPlayed);
          if (!play) {
            // 如果已全部轮播，更新全部的状态
            this.playList.map(h => (h.isPlayed = false));
            play = this.playList[0];
          }
          play.isPlayed = true;

          const data = this.carList.find(h => h.utmsDriverId === play.id);

          // 获取对应点
          const pointData = this.carImgList.find(car => {
            return car.data.subPoints.find(p => p.utmsDriverId === play.id);
          }).data;
          const point = new BMap.Point(pointData.longitude, pointData.latitude);

          this.showCarInfo({
            point,
            data,
          });
        }
      }

      this.playTimer = setTimeout(run, 1000 * 5); // 5秒
    };
    run();
  }

  // 当地图缩放到不同级别时，多远距离的点合并到一起
  initDistanceOfZoom() {
    this.distanceOfzoom = {};
    const distanceOfZoom19 = 20; // 当地图缩放到19级别时，相距20米的2个点合并到一起

    for (let i = 19; i >= 3; i--) {
      this.distanceOfzoom[i] = distanceOfZoom19 * Math.pow(2, 19 - i); // 多远距离的点合并到一起，单位：米
    }
  }
  removeMapElement = (element: Element) => {
    if (element) {
      element.parentElement.removeChild(element);
    }
  };

  initMap() {
    this.map = new BMap.Map(this.divMap, {
      minZoom: 1,
      maxZoom: 19,
      enableMapClick: false,
    });
    this.map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
    this.map.setMapStyle(mapStyle);

    // 去掉地图左下解的LOGO与文字
    this.map.addEventListener('tilesloaded', () => {
      this.removeMapElement(document.querySelector('.anchorBL'));
      this.removeMapElement(document.querySelector('.BMap_cpyCtrl'));
      // this.addGraph();
    });
    // 地图放大级别变化时
    this.map.addEventListener('zoomend', () => {
      this.drawCarPoints();
      this.hideCarInfo();
    });

    this.map.centerAndZoom('杭州', 12);

    const ctrl = new BMapLib.TrafficControl({
      showPanel: false, // 是否显示路况提示面板
    });
    this.map.addControl(ctrl);

    const refreshBtn = this.divMap.querySelector('.maplibTcUpdate') as any;
    const maplibTc = this.divMap.querySelector('.maplibTc') as any;
    setInterval(() => {
      if (maplibTc.offsetWidth) {
        refreshBtn.click();
      }
    }, 60 * 1000);
  }

  setSize() {
    this.map.reset();
  }

  fillWarehouse(warehouseList: any[]) {
    this.warehousePointList.map(h => {
      this.map.removeOverlay(h);
    });
    this.warehousePointList = [];
    warehouseList.map(h => {
      const { longitude, latitude } = h;
      const point = new BMap.Point(longitude, latitude);
      const icon = new BMap.Icon(imgWarehouse1, new BMap.Size(63, 45), {
        anchor: new BMap.Size(31, 22),
      });
      const maker = new BMap.Marker(point, {
        icon,
        enableClicking: false,
      });
      this.map.addOverlay(maker);
      this.warehousePointList.push(maker);
    });
  }

  setBottom = bottom => {
    this.bottom = bottom;
  };

  drawCarPointsAndSetViewport = () => {
    // this.hideCarInfo();

    // if (this.hideCarsInfoList) {
    //   this.hideCarsInfoList();
    // }

    const carPointList = this.drawCarPoints();
    if (carPointList && carPointList.length) {
      this.map.setViewport(carPointList, {
        margins: [10, 10, this.bottom + 20, 10],
        enableAnimation: true,
      });
    } else {
      this.map.centerAndZoom('杭州', 12);
    }
  };

  drawCarPoints() {
    if (this.carImgList) {
      this.carImgList.map(m => {
        this.map.removeOverlay(m);
      });
    }
    this.carImgList = [];

    if (!this.carList) {
      return [];
    }
    const carList = this.carList.map(c => ({
      ...c,
      point: new BMap.Point(c.longitude, c.latitude),
      pointCount: 1,
      subPoints: [
        { ...c }, // 先把自己放进来
      ], // 当多个点合并到一个点时，多个点的信息数组
    }));

    const distanceOfzoom = this.distanceOfzoom[this.map.getZoom()]; // 获取当前地图缩放级别对应的合并点距离

    for (let i = 0; i < carList.length - 1; i++) {
      const c1 = carList[i];
      for (let j = i + 1; j < carList.length; j++) {
        const c2 = carList[j];
        if (this.map.getDistance(c1.point, c2.point) < distanceOfzoom) {
          carList.splice(j, 1);
          c1.pointCount++;
          c1.subPoints.push(c2);
          j--;
        }
      }
    }

    const carPointList = [];

    carList.map(m => {
      const point = new BMap.Point(m.longitude, m.latitude);
      carPointList.push(point);
      const car = new CarPoint({
        point,
        data: m,
      });
      car.mouseover = d => {
        this.showCarInfo({
          point,
          data: d.data,
        });
      };
      car.mouseout = d => {
        this.hideCarInfo();
      };
      // 当点击合并的点时，右边弹出层显示列表
      car.clickOfMultiple = d => {
        if (this.props.clickOfMultiple) {
          this.props.clickOfMultiple(d);
        }
      };

      this.map.addOverlay(car);
      this.carImgList.push(car);
    });

    return carPointList;
  }

  showCarInfo({ point, data }) {
    if (!this.carInfoWindowInfo) {
      this.carInfoWindowInfo = new TooltipCarInfo();
      this.carInfoWindowInfo.setPosition(point);
      this.map.addOverlay(this.carInfoWindowInfo);

      // this.carInfoWindowInfo.mouseenter = () => {
      //   clearTimeout(this.closeCarInfo);
      // };
      // this.carInfoWindowInfo.mouseleave = () => {
      //   this.hideCarInfo();
      // };
    }

    this.carInfoWindowInfo.setPosition(point);
    this.carInfoWindowInfo.setData(data);
    this.carInfoWindowInfo.show();
  }

  hideCarInfo() {
    if (this.carInfoWindowInfo) {
      this.carInfoWindowInfo.hide();
    }
  }

  render() {
    return (
      <div
        className={styles.divMap}
        ref={obj => {
          this.divMap = obj;
        }}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getWarehouseList: () => dispatch(actions.getWarehouse()),
  getCarList: () => dispatch(actions.getCarList()),
});
const mapStateToProps = createStructuredSelector({
  warehouseList: makeSelectWarehouseList(),
  theme: makeSelectTheme(),
  carList: makeSelectCarList(),
});
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
);

export default compose(withConnect)(BigScreenMap);
