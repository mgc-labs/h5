/**
 * Ios Saga
 * @author djd
 * @date 2018/10/24 下午5:43:33
 */
import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { call, put, take, takeLatest } from 'redux-saga/effects';
import * as actions from './actions';
import * as c from './constants';
import { askCar, askGetWarehouse, askTodayData, askWeekInfo } from './service';

function* getWarehouse() {
  try {
    const data = yield call(askGetWarehouse, { depotType: 'store' });
    yield put(actions.getWarehouseDone(data));
  } catch (err) {
    yield put(actions.getWarehouseError(err));
    message.error(err.msg);
  }
}

function* getWeekInfo() {
  try {
    const data = yield call(askWeekInfo, {});
    yield put(actions.getWeekInfoDone(data));
  } catch (err) {
    yield put(actions.getWeekInfoError(err));
    message.error(err.msg);
  }
}

function* getTodayData() {
  try {
    const data = yield call(askTodayData, {});
    yield put(actions.getTodayDataDone(data));
  } catch (err) {
    yield put(actions.getTodayDataError(err));
    message.error(err.msg);
  }
}

function* getCar() {
  try {
    const data = yield call(askCar, { skipCount: 0, pageSize: 100000 });
    yield put(actions.getCarListDone(data));
  } catch (err) {
    yield put(actions.getCarListError(err));
    message.error(err.msg);
  }
}

export default function* saga() {
  yield takeLatest(c.GET_WAREHOUSE, getWarehouse);
  yield takeLatest(c.GET_WEEKINFO, getWeekInfo);
  yield takeLatest(c.GET_TODAYDATA, getTodayData);
  yield takeLatest(c.GET_CAR, getCar);
}
