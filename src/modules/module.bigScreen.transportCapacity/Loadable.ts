/**
 * Ios Loadable
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */
import Loadable from 'react-loadable';

import LoadingIndicator from 'components/LoadingIndicator';

let Component;
if (process.env.NODE_ENV === 'development') {
  Component = require('./index').default;
} else {
  Component = Loadable({
    loader: () =>
      import(/* webpackChunkName: "bigScreen-transportCapacity" */ './index'),
    loading: LoadingIndicator,
  });
}

export default Component;
