/**
 * Ios selectors
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

const selectState = state => state.get('bigScreen.transportCapacity');
const authorizationState = state => state.get('authorization');

export const makeSelectWarehouseList = () =>
  createSelector(selectState, state => state.get('warehouseList'));

export const makeSelectWeekInfo = () =>
  createSelector(selectState, state => state.get('weekInfo'));

export const makeSelectTodayData = () =>
  createSelector(selectState, state => state.get('todayData'));

export const makeSelectTheme = () =>
  createSelector(selectState, state => state.get('theme'));

export const makeSelectCarList = () =>
  createSelector(selectState, state => state.get('carList'));

export const makeSelectAccountInfo = () =>
  createSelector(authorizationState, state => state.get('accountInfo'));
