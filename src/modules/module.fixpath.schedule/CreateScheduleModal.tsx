/**
 * CreateScheduleModal Component
 * @author ryan bian
 * @date 2018-9-17 09:04:56
 */
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Modal from 'antd/es/modal';
import Select from 'antd/es/select';
import TimePicker from 'antd/es/time-picker';
import DriverPicker from 'components/DriverPicker';
import * as React from 'react';
import { WEEKDAYS } from './WeeklyEditor';

import styles from './index.module.less';

const Option = Select.Option;
const FormItem = Form.Item;
const itemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const WeekdaySelectOptions = WEEKDAYS.map(day => (
  <Option key={day.weekDay} value={day.weekDay}>
    {day.displayName}
  </Option>
));

export interface ICreateScheduleModalProps {
  form?: WrappedFormUtils;
  onClose: () => void;
  onConfirm: (values) => void;
  visible: boolean;
}

@Form.create()
export default class CreateScheduleModal extends React.PureComponent<
  ICreateScheduleModalProps
> {
  public componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      nextProps.form.resetFields();
    }
  }
  public handleSubmit = () => {
    const { form, onConfirm } = this.props;
    form.validateFields((errors, values) => {
      if (!errors) {
        onConfirm(values);
      }
    });
  };
  public handleClose = () => {
    this.props.onClose();
  };

  public renderEditor() {
    const { form } = this.props;
    const { getFieldDecorator, setFieldsValue } = form;
    return (
      <Form>
        <FormItem {...itemLayout} label="选择周期" required>
          {getFieldDecorator('weekdays', {
            rules: [{ required: true, message: '请选择周期' }],
          })(
            <Select mode="multiple" showArrow={true}>
              {WeekdaySelectOptions}
            </Select>,
          )}
        </FormItem>
        <FormItem {...itemLayout} label="用车时间" required>
          {getFieldDecorator('usecartime', {
            rules: [{ required: true, message: '请选择用车时间' }],
          })(<TimePicker format="HH:mm" />)}
        </FormItem>
        <FormItem {...itemLayout} label="选择司机">
          {getFieldDecorator('driver')(
            <DriverPicker
              renderOption={item => {
                const value = [
                  item.utmsDriverId,
                  item.driverName,
                  item.plateNumber,
                ].join('@');
                return (
                  <Option key={item.utmsDriverId} value={value}>
                    {item.driverName}-{item.plateNumber}
                  </Option>
                );
              }}
            />,
          )}
        </FormItem>
      </Form>
    );
  }
  public render() {
    const { visible } = this.props;
    const modalProps = {
      title: '添加班次',
      visible,
      maskClosable: false,
      centered: true,
      onOk: this.handleSubmit,
      onCancel: this.handleClose,
    };
    return <Modal {...modalProps}>{this.renderEditor()}</Modal>;
  }
}
