/**
 * FixpathSchedule selectors
 * @author ryan bian
 * @date 2018-9-17 09:04:56
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectDetail = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('detail'));
