/**
 * FixpathSchedule Model
 * @author ryan bian
 * @date 2018-9-17 09:04:56
 */
import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { fromJS, List, Map } from 'immutable';
import _ from 'lodash';
import msngr from 'msngr';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';
import { getData } from '../module.fixpath.detail/service';
import { addSchedule, getSchedule, updateSchedule } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: Map({
        workScheduleDetailList: List(),
      }),
      detail: Map(),
      error: false,
      loading: false,
    }),
    actions: {
      GET_SCHEDULE: routeId => routeId,
      GET_SCHEDULE_ERROR: error => error,
      GET_SCHEDULE_SUCCESS: data => data,
      GET_DETAIL: routeId => routeId,
      GET_DETAIL_ERROR: error => error,
      GET_DETAIL_SUCCESS: data => data,
      CREATE_SCHEDULE: schedule => {
        const { weekdays, usecartime, driver } = schedule;
        const driverData = driver ? driver.split('@') : [];
        return weekdays.map(day =>
          Map({
            uuid: _.uniqueId('uuid'),
            weekDay: Number(day),
            usecartime: usecartime.format('HH:mm'),
            driverId: driver ? driverData[0] : '',
            driverName: driver ? driverData[1] : '',
            plateNumber: driver ? driverData[2] : '',
          }),
        );
      },
      DELETE_SCHEDULE: scheduleData => {
        return {
          ...scheduleData,
          weekday: Number(scheduleData.weekDay),
        };
      },
      SAVE_SCHEDULE_CONFIG: (dateRange, handleClose) => ({
        startDate: dateRange[0].format('YYYY-MM-DD'),
        endDate: dateRange[1].format('YYYY-MM-DD'),
        handleClose,
      }),
      SAVE_SCHEDULE_CONFIG_DONE: () => null,
      TO_LIST: () => null,
    },
    reducers: {
      GET_SCHEDULE(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_SCHEDULE_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_SCHEDULE_SUCCESS(state, action) {
        return state.set('loading', false).set('data', fromJS(action.payload));
      },
      CREATE_SCHEDULE(state, action) {
        return state.updateIn(['data', 'workScheduleDetailList'], list => {
          return list.concat(action.payload);
        });
      },
      DELETE_SCHEDULE(state, action) {
        const { uuid } = action.payload;
        return state.updateIn(['data', 'workScheduleDetailList'], list => {
          return list.filter(map => {
            return !(
              String(map.get('uuid')) === uuid || String(map.get('id')) === uuid
            );
          });
        });
      },
      SAVE_SCHEDULE_CONFIG(state) {
        return state.set('loading', true);
      },
      SAVE_SCHEDULE_CONFIG_DONE(state) {
        return state.set('loading', false);
      },
      GET_DETAIL(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_DETAIL_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_DETAIL_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('detail', fromJS(action.payload.data || {}));
      },
      TO_LIST(state) {
        return state.set('error', false).set('loading', false);
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const data = yield call(getSchedule, action.payload);
          yield put(actions.getScheduleSuccess(data));
        } catch (err) {
          yield put(actions.getScheduleError(err));
          message.error(err.toString());
        }
      }

      function* submitSchedule(action) {
        const { endDate, handleClose, startDate } = action.payload;
        try {
          const scheduleData = yield select(state =>
            state.getIn([namespace, 'data']),
          );
          const detail = yield select(state =>
            state.getIn([namespace, 'detail']),
          );
          const addConfig = Object.assign(
            {
              routeId: detail.get('routeId'),
              customerCode: detail.get('customerCode'),
              workScheduleDetailList: JSON.stringify(
                scheduleData.get('workScheduleDetailList', List()).toJS(),
              ),
            },
            { startDate, endDate },
          );
          if (scheduleData.get('workScheduleId')) {
            const updateConfig = Object.assign(
              {
                workScheduleId: scheduleData.get('workScheduleId'),
              },
              addConfig,
            );
            yield call(updateSchedule, updateConfig);
          } else {
            yield call(addSchedule, addConfig);
          }
          message.success('保存成功!');
          yield put(actions.saveScheduleConfigDone());
          yield put(actions.toList());
          msngr('/fixpath/list', 'refresh').emit(true);
          setTimeout(() => {
            handleClose();
          }, 200);
        } catch (err) {
          message.error(err.toString());
        }
      }

      function* fetchDetail(action) {
        try {
          const data = yield call(getData, { routeId: action.payload });
          if (data.code) {
            message.error(data.msg);
          }
          yield put(actions.getDetailSuccess(data));
        } catch (err) {
          yield put(actions.getDetailError(err));
        }
      }

      function* toList() {
        yield put(push('/fixpath/list'));
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_SCHEDULE, fetchData);
        yield takeLatest(CONSTANTS.SAVE_SCHEDULE_CONFIG, submitSchedule);
        yield takeLatest(CONSTANTS.GET_DETAIL, fetchDetail);
        yield takeLatest(CONSTANTS.TO_LIST, toList);
      };
    },
  });
