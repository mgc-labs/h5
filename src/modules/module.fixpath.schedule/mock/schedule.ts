export default {
  result: 'success',
  data: {
    scheduleId: 1,
    routeId: 123,
    customerCode: 111,
    customerName: '陈师傅',
    routeName: '线路名称1',
    isEveryDay: 0,
    workScheduleDetail: [
      {
        useCarTime: '18:00',
        weekday: 1,
        driverId: '11',
        driverName: '陈师傅',
        plateNumber: '浙A123456',
      },
      {
        useCarTime: '09:00',
        weekday: 1,
        driverId: '11',
        driverName: '赵师傅',
        plateNumber: '浙A123451',
      },
      {
        useCarTime: '10:00',
        weekday: 3,
        driverId: '13',
        driverName: '王师傅',
        plateNumber: '浙A123457',
      },
    ],
    navigationDistance: 10,
    navigationTimecost: 30,
  },
};
