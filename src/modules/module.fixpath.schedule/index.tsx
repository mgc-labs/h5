/**
 * FixpathSchedule Component
 * @author ryan bian
 * @date 2018-9-17 09:04:56
 */
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Spin from 'antd/es/spin';
import { PageBottom } from 'components/PageBottom';
import { List, Map } from 'immutable';
import moment from 'moment';
import msngr from 'msngr';
import * as React from 'react';
import { createStructuredSelector } from 'reselect';
import CreateScheduleModal from './CreateScheduleModal';
import {
  makeSelectData,
  makeSelectDetail,
  makeSelectError,
  makeSelectLoading,
} from './selectors';
import WeeklyEditor from './WeeklyEditor';

import styles from './index.module.less';

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const confirm = Modal.confirm;

export interface IFixpathScheduleProps {
  children: React.ReactChildren;
  form: WrappedFormUtils;
  scheduleData: Map<string, any>;
  detail: Map<string, any>;
  searchParams: {
    routeId: number;
  };
  loading: boolean;
  getSchedule: (routeId: number) => any;
  createSchedule: (createSchedule) => void;
  deleteSchedule: (scheduleData) => void;
  saveScheduleConfig: (dateRange, handleClose) => void;
  getDetail: (routeId: number) => any;
  close: () => void;
  toList: () => void;
}

interface IFixpathScheduleState {
  openModal: boolean;
}

@Form.create()
class FixpathSchedule extends React.PureComponent<
  IFixpathScheduleProps,
  IFixpathScheduleState
> {
  state = {
    openModal: false,
  };
  public componentDidMount() {
    const { close, searchParams } = this.props;
    if (!searchParams.routeId) {
      Modal.error({
        title: '参数异常',
        content: '缺少线路ID，页面将关闭',
        onOk() {
          close();
        },
      });
    } else {
      this.props.getSchedule(searchParams.routeId);
      this.props.getDetail(searchParams.routeId);
    }
  }
  public componentWillReceiveProps(nextProps) {
    if (
      nextProps.scheduleData &&
      nextProps.scheduleData !== this.props.scheduleData
    ) {
      this.setRangeDate(nextProps);
    }
  }
  // 取消编辑
  public handleClose = () => {
    confirm({
      title: '有未保存的信息',
      content: '如放弃，填写的信息将丢失',
      okText: '继续填写',
      cancelText: '放弃',
      onCancel: () => {
        this.props.toList();
        this.props.close();
        msngr('/fixpath/list', 'refresh').emit(true);
      },
    });
  };
  public handleOpenModal = () => {
    this.setState({
      openModal: true,
    });
  };
  public handleCloseModal = () => {
    this.setState({
      openModal: false,
    });
  };
  public handleCreateSchedule = values => {
    this.props.createSchedule(values);
    this.setState({
      openModal: false,
    });
  };
  public handleSubmitSchedule = () => {
    const { form, saveScheduleConfig } = this.props;
    form.validateFields((errors, values) => {
      if (!errors) {
        saveScheduleConfig(values.dateRange, this.props.close);
      }
    });
  };
  public renderInfo() {
    const { form, scheduleData, detail } = this.props;
    const { getFieldDecorator, setFieldsValue } = form;

    return (
      <Form layout="inline">
        <Card title="运力计划" className={styles.FixpathSchedule__InfoCard}>
          <Row className={styles.FixpathSchedule__FormItem}>
            <Col md={12} lg={8}>
              <FormItem label="客户名称">{detail.get('customerName')}</FormItem>
            </Col>
            <Col md={12} lg={8}>
              <FormItem label="线路名称">{detail.get('routeName')}</FormItem>
            </Col>
          </Row>
        </Card>
        <Card>
          <Row className={styles.FixpathSchedule__FormItem}>
            <Col md={24} lg={24}>
              <FormItem required label="起止时间">
                {getFieldDecorator('dateRange', {
                  rules: [{ required: true, message: '请选择起止时间' }],
                })(<RangePicker />)}
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                type="dashed"
                icon="plus"
                block
                onClick={this.handleOpenModal}
              >
                添加班次
              </Button>
            </Col>
          </Row>
          {this.renderWeekEditor()}
        </Card>
      </Form>
    );
  }
  public renderWeekEditor() {
    const { scheduleData, deleteSchedule } = this.props;
    return (
      <WeeklyEditor
        data={scheduleData.get('workScheduleDetailList', List())}
        onDelete={deleteSchedule}
      />
    );
  }
  public renderBottom() {
    const { scheduleData } = this.props;
    const actions = [
      <Button
        key="cancle"
        className={styles.FixpathSchedule__BottomActionButton}
        onClick={this.handleClose}
      >
        取消
      </Button>,
      <Button
        key="save"
        type="primary"
        disabled={
          scheduleData.get('workScheduleDetailList', List()).count() === 0
        }
        onClick={this.handleSubmitSchedule}
      >
        保存线路
      </Button>,
    ];
    return <PageBottom rightChild={actions} />;
  }
  public render() {
    return (
      <React.Fragment>
        <Spin spinning={this.props.loading}>
          <div className={styles.FixpathSchedule}>
            {this.renderInfo()}
            {this.renderBottom()}
          </div>
        </Spin>
        <CreateScheduleModal
          visible={this.state.openModal}
          onConfirm={this.handleCreateSchedule}
          onClose={this.handleCloseModal}
        />
      </React.Fragment>
    );
  }

  private setRangeDate(props) {
    const { scheduleData, form } = props;
    if (
      scheduleData.size &&
      scheduleData.get('startDate') &&
      scheduleData.get('endDate') &&
      !form.getFieldValue('dateRange')
    ) {
      form.setFieldsValue({
        dateRange: [
          moment(scheduleData.get('startDate'), 'YYYY-MM-DD'),
          moment(scheduleData.get('endDate'), 'YYYY-MM-DD'),
        ],
      });
    }
  }
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        detail: makeSelectDetail(currentState),
        scheduleData: makeSelectData(currentState),
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getSchedule: options => dispatch(actions.getSchedule(options)),
        getDetail: options => dispatch(actions.getDetail(options)),
        createSchedule: schedule => dispatch(actions.createSchedule(schedule)),
        deleteSchedule: scehduleData =>
          dispatch(actions.deleteSchedule(scehduleData)),
        saveScheduleConfig: (dateRange, handleClose) =>
          dispatch(actions.saveScheduleConfig(dateRange, handleClose)),
        toList: options => dispatch(actions.toList(options)),
      }),
    };
  })(FixpathSchedule);
};
