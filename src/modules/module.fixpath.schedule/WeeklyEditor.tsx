/**
 * WeeklyEditor Component
 * @author ryan bian
 * @date 2018-9-17 09:04:56
 */
import Icon from 'antd/es/icon';
import { fromJS, List, Map } from 'immutable';
import _ from 'lodash';
import * as React from 'react';
import styles from './index.module.less';

export interface IWeeklyEditorProps {
  data: List<Map<string, string | number>>;
  onDelete: (scheduleData) => void;
}

export const WEEKDAYS = [
  { weekDay: 1, displayName: '周一' },
  { weekDay: 2, displayName: '周二' },
  { weekDay: 3, displayName: '周三' },
  { weekDay: 4, displayName: '周四' },
  { weekDay: 5, displayName: '周五' },
  { weekDay: 6, displayName: '周六' },
  { weekDay: 7, displayName: '周日' },
];

export default class WeeklyEditor extends React.PureComponent<
  IWeeklyEditorProps
> {
  public handleDeleteCard = e => {
    const { driverId, useCarTime, weekday, uuid } = e.currentTarget.dataset;
    this.props.onDelete({
      driverId,
      usecartime: useCarTime,
      weekDay: weekday,
      uuid,
    });
  };
  public renderScheduleCard = detail => {
    const uuid = detail.get('id') ? detail.get('id') : detail.get('uuid');
    return (
      <div className={styles.WeeklyEditor__Card} key={uuid}>
        <Icon
          data-weekday={detail.get('weekDay') || detail.get('weekday')}
          data-use-car-time={detail.get('usecartime')}
          data-driver-id={detail.get('driverId')}
          data-uuid={uuid}
          className={styles.WeeklyEditor__CardClose}
          type="close"
          onClick={this.handleDeleteCard}
        />
        <p>{detail.get('usecartime')}</p>
        <p>{detail.get('driverName')}</p>
        <p>{detail.get('plateNumber')}</p>
      </div>
    );
  };
  public renderDayColumn = day => {
    const { data } = this.props;

    return (
      <div className={styles.WeeklyEditor__DayColumn} key={day.weekDay}>
        <h5 className={styles.WeeklyEditor__DayTitle}>{day.displayName}</h5>
        {data
          .filter(d => Number(d.get('weekDay')) === day.weekDay)
          .sortBy(d => d.get('usecartime'))
          .map(this.renderScheduleCard)
          .toArray()}
      </div>
    );
  };
  public render() {
    return (
      <div className={styles.WeeklyEditor}>
        {WEEKDAYS.map(this.renderDayColumn)}
      </div>
    );
  }
}
