/**
 * fixpath.schedule Service
 * @author ryan bian
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_ADD_SCHEDULE =
  '/ehuodiGateway/utmsDispatch/utmsWorkSchedulecs/addWorkSchedule';
const API_UPDATE_SCHEDULE =
  '/ehuodiGateway/utmsDispatch/utmsWorkSchedulecs/updateWorkSchedule';
const API_GET_SCHEDULE =
  '/ehuodiGateway/utmsDispatch/utmsWorkSchedulecs/queryById';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_GET_SCHEDULE)
    .reply(() =>
      import('./mock/schedule').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_ADD_SCHEDULE)
    .reply(() =>
      import('./mock/success').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_UPDATE_SCHEDULE)
    .reply(() =>
      import('./mock/success').then(exports => [200, exports.default]),
    );
}

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getSchedule = routeId => {
  return request(
    {
      method: 'post',
      url: API_GET_SCHEDULE,
      data: qs.stringify({
        routeId,
      }),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data || { workScheduleDetailList: [] });
};

// 新增
export const addSchedule = data => {
  return request(
    {
      method: 'post',
      url: API_ADD_SCHEDULE,
      data: qs.stringify(data),
    },
    {
      useMock: USE_MOCK,
    },
  );
};

// 更新
export const updateSchedule = data => {
  return request(
    {
      method: 'post',
      url: API_UPDATE_SCHEDULE,
      data: qs.stringify(data),
    },
    {
      useMock: USE_MOCK,
    },
  );
};
