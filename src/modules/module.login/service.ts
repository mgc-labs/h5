/**
 * Auth Service
 * @author ryan bian
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

const API_SOURCE = '/ehuodiGateway';
// const API_SOURCE = '';

// API list
const API_LOGIN = `${API_SOURCE}/utmsCore/utmsBaseconfigcs/login`;
const API_LOGIN_LOGO = `${API_SOURCE}/utmsCore/utmsClientcs/selectLoginLogoImg`;

const API_CAPTCHA = `${API_SOURCE}/utmsCore/utmsBaseconfigcs/getImageCaptcha`;
const API_SMS_CAPTCHA = `${API_SOURCE}/utmsCore/utmsBaseconfigcs/getSmsCaptcha`;

const VALIDATE_IMAGE = `${API_SOURCE}/utmsCore/utmsBaseconfigcs/validateImageCaptcha`;

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_LOGIN)
    .reply(() =>
      import('./mock/login').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_CAPTCHA)
    .reply(() =>
      import('./mock/captcha').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_SMS_CAPTCHA)
    .reply(() => import('./mock/sms').then(exports => [200, exports.default]));

  // mocker
  //   .on('post', VALIDATE_IMAGE)
  //   .reply(() =>
  //     import('./mock/validate').then(exports => [200, exports.default]),
  //   );
  // mocker
  //   .on('post', VALIDATE_SMS)
  //   .reply(() =>
  //     import('./mock/validate').then(exports => [200, exports.default]),
  //   );
}

/**
 * 获取登陆图片
 */
export const getLogo = domainName =>
  request({
    method: 'post',
    url: API_LOGIN_LOGO,
    data: qs.stringify({
      domainName,
    }),
  });

/**
 * 请求登陆
 */
export const requestLogin = params =>
  request(
    {
      method: 'post',
      url: API_LOGIN,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  );

/**
 * 获取图片验证码
 */
export const getImageCaptcha = params =>
  request(
    {
      method: 'post',
      url: API_CAPTCHA,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

/**
 * 验证图片验证码
 */
export const validateImageCaptcha = params =>
  request(
    {
      method: 'post',
      url: VALIDATE_IMAGE,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  );

/**
 * 获取短信验证码
 */
export const getSmsCaptcha = params =>
  request(
    {
      method: 'post',
      url: API_SMS_CAPTCHA,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res);
/**
 * 验证短信验证码
 */
// export const validateSmsCaptcha = params =>
//   request(
//     {
//       method: 'post',
//       url: VALIDATE_SMS,
//       data: qs.stringify(params),
//     },
//     {
//       useMock: USE_MOCK,
//       globalErrorMsg: true,
//     },
//   ).then(res => res);
