/**
 * Passport Saga
 * @author ryan bian
 */
import { Modal } from 'antd';
import { push, replace } from 'connected-react-router';
import { Map } from 'immutable';
import {
  call,
  // fork,
  put,
  select,
  takeEvery,
  takeLatest,
} from 'redux-saga/effects';

import Cookies from 'js-cookie';
import localForage from 'localforage';
import { delay } from 'redux-saga';
import encrypt from 'utils/encrypt';
import {
  DESTROY_ACTION,
  LOGIN_EXPIRE_DAY,
  SMS_CODE_REFETCH_INTERVAL,
} from '../../CONFIG';
import * as actions from './actions';
import * as constants from './constants';
import * as service from './service';

const cookieConfig = {
  expires: LOGIN_EXPIRE_DAY,
  path: '/',
};

export const PASSWORD_SECURITY_NOTIFY = 'PASSWORD_SECURITY_NOTIFY';

export enum LOGIN_MODE {
  PASSWORD = 'password',
  PHONE = 'phone',
}

export enum LOGIN_RESPONSE_CODE {
  PASSWORD_INCORRECT = 'UTMS001030500101',
  PASSWORD_TOO_SIMPLE = 'UTMS001030500106',
  PASSWORD_EXPIRED = 'UTMS001030500107',
}

const INITIAL_STATE_KEYS = ['router', 'login', 'global'];

function securityCheck(code) {
  if (
    code === LOGIN_RESPONSE_CODE.PASSWORD_TOO_SIMPLE ||
    code === LOGIN_RESPONSE_CODE.PASSWORD_EXPIRED
  ) {
    return localForage.setItem(PASSWORD_SECURITY_NOTIFY, code);
  }
  return Promise.resolve();
}

function* doLogin(action) {
  const fieldsValue = yield select((state: Map<string, any>) =>
    state.getIn(['login', 'fieldsValue']).toObject(),
  );
  const loginMode = yield select((state: Map<string, any>) =>
    state.getIn(['login', 'mode']),
  );

  try {
    const { identifyCode, coordinateX } = action.payload;
    const loginParams = fieldsValue;
    if (loginMode === LOGIN_MODE.PASSWORD) {
      // 密码用 md5 加密
      Object.assign(loginParams, {
        coordinateX,
        password: encrypt(loginParams.password),
      });
      yield put(actions.validateImageCaptcha());
    } else {
      Object.assign(fieldsValue, {
        identifyCode,
      });
    }
    const { data, code } = yield call(service.requestLogin, loginParams);

    if (loginMode === LOGIN_MODE.PASSWORD) {
      yield put(actions.validateImageCaptchaDone(true));
      yield delay(250);
    }

    yield call(securityCheck, code);

    yield put(actions.loginSuccess(data));

    const domainPath = `/${fieldsValue.domainName}`;

    Cookies.set(
      'sessionKey',
      data.sessionkey,
      Object.assign({}, cookieConfig, {
        path: '/',
      }),
    );
    Cookies.set(
      'userName',
      data.userName,
      Object.assign({}, cookieConfig, {
        path: domainPath,
      }),
    );

    if (!location.pathname.startsWith(domainPath)) {
      window.location.href = `${domainPath}/`;
    } else {
      const allState = yield select(state => state);
      const destroyKeys = [];
      allState
        .filter((value, key: string) => {
          return !INITIAL_STATE_KEYS.includes(key);
        })
        .forEach((value, key: string) => {
          destroyKeys.push(key);
        });
      yield put({
        type: DESTROY_ACTION,
        payload: destroyKeys,
      });
      yield put(replace('/'));
    }
  } catch (err) {
    if (loginMode === LOGIN_MODE.PASSWORD) {
      if (err.code === LOGIN_RESPONSE_CODE.PASSWORD_INCORRECT) {
        // 密码错误 关闭弹框
        yield put(actions.validateImageCaptchaDone(true));
        yield delay(250);
        yield put(actions.toggleCaptchaModal(false));
      } else {
        // 重新刷新验证图片
        yield put(actions.validateImageCaptchaDone(new Error(err)));
        yield delay(250);
        yield put(actions.getImageCaptcha());
      }
    }
    yield put(actions.loginError(err));
  }
}

// 图片验证码
function* getImageCaptcha() {
  try {
    const loginFields = yield select((state: Map<string, any>) =>
      state.getIn(['login', 'fieldsValue']),
    );
    const data = yield call(service.getImageCaptcha, {
      domainName: loginFields.get('domainName'),
      userName: loginFields.get('userName'),
    });
    yield put(actions.getImageCaptchaDone(data));
    yield put(actions.toggleCaptchaModal(true));
  } catch (err) {
    yield put(actions.getImageCaptchaDone(new Error(err.msg)));
  }
}

function* startCountDownForNextSmsFetching() {
  let remain = SMS_CODE_REFETCH_INTERVAL;
  while (remain >= 0) {
    yield put(actions.smsRefetchCountDown(remain));
    remain -= 1;
    yield delay(1000);
  }
}

// 短信验证码
function* requestSmsCaptcha(action) {
  try {
    const countDown = yield select((state: Map<string, any>) =>
      state.getIn(['login', 'smsCaptcha', 'countDown']),
    );
    if (countDown === 0) {
      const data = yield call(service.getSmsCaptcha, action.payload);
      yield put(actions.requestIdentifyCodeSended(data));
      yield call(startCountDownForNextSmsFetching);
    }
  } catch (err) {
    // ignore
    yield put(actions.requestIdentifyCodeSended(new Error(err.msg)));
  }
}

// function* validateImageCaptcha(action) {
//   try {
//     const data = yield call(service.validateImageCaptcha, action.payload);
//     yield put(actions.validateImageCaptchaDone(data));
//     // todo: 提交
//   } catch (err) {
//     yield put(actions.validateImageCaptchaDone(new Error(err.msg)));
//     yield delay(1000);
//     yield put(actions.getImageCaptcha());
//   }
// }

// function* validateSmsCaptcha(action) {
//   try {
//     const data = yield call(service.validateSmsCaptcha, action.payload);
//     yield put(actions.validateSmsCaptchaDone(data));
//   } catch (err) {
//     // ignore
//     yield put(actions.validateSmsCaptchaDone(err));
//   }
// }

/**
 * Root saga manages watcher lifecycle
 */
export default function* rootSaga() {
  yield takeLatest(constants.REQUEST_LOGIN, doLogin);
  // 获取验证码
  yield takeEvery(constants.GET_IMAGE_CAPTCHA, getImageCaptcha);
  yield takeEvery(constants.REQUEST_IDENTIFY_CODE, requestSmsCaptcha);
  // 验证验证码
  // yield takeEvery(constants.VALIDATE_IMAGE_CAPTCHA, validateImageCaptcha);
  // yield takeEvery(constants.VALIDATE_SMS_CAPTCHA, validateSmsCaptcha);
}
