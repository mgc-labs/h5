/**
 * Login selectors
 * @author ryan bian
 * @date 2018-9-6 08:49:25
 */
import { createSelector } from 'reselect';

const selectState = state => state.get('login');

export const makeSelectImageCaptcha = () =>
  createSelector(selectState, state => state.get('imageCaptcha'));

export const makeSelectSmsCaptcha = () =>
  createSelector(selectState, state => state.get('smsCaptcha'));

export const makeSelectLoginStatus = () =>
  createSelector(selectState, state => state.get('loginStatus'));

export const makeSelectLoginMode = () =>
  createSelector(selectState, state => state.get('mode'));

export const makeSelectFieldsValue = () =>
  createSelector(selectState, state => state.get('fieldsValue'));
