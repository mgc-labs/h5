export function textSelect(element: HTMLInputElement) {
  const start = 0;
  const end = element.value.length;
  if (element.createTextRange) {
    // IE浏览器
    const range = element.createTextRange();
    range.moveStart('character', start);
    range.moveEnd('character', end);
    range.select();
  } else {
    element.setSelectionRange(start, end);
    element.focus();
  }
}
