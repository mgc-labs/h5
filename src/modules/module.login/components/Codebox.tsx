/**
 * CodeBox Component
 * @author ryan bian
 * @date 2018-9-6 09:32:59
 */
import * as React from 'react';

import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Spin from 'antd/es/spin';

import { textSelect } from 'modules/module.login/components/utils';
import styles from 'modules/module.login/index.module.less';

const InputGroup = Input.Group;

interface ICodeBoxProps {
  length: number;
  captcha: string;
  pending: boolean;
  visible: boolean;
  onRefresh: () => void;
  onChange: (code: string[]) => void;
  onCancel: () => void;
}

interface ICodeBoxState {
  code: string[];
}

export default class CodeBox extends React.PureComponent<
  ICodeBoxProps,
  ICodeBoxState
> {
  private inputs = [];
  public constructor(props) {
    super(props);
    this.state = {
      code: Array(props.length).fill(''),
    };
  }
  public componentWillReceiveProps(nextProps) {
    if (nextProps.captcha !== this.props.captcha) {
      this.setState(
        {
          code: Array(nextProps.length).fill(''),
        },
        () => {
          this.focusOn(0);
        },
      );
    }
  }
  public componentDidMount() {
    // focus on first input
    setTimeout(() => {
      this.focusOn(0);
    }, 0);
  }
  public onChange(e, i) {
    let newValue = e.target.value.trim();
    const { code } = this.state;
    if (newValue.length > 1) {
      newValue = newValue.charAt(0);
    }

    if (code[i] !== newValue && newValue) {
      this.focusOn(i + 1);
    }

    this.setState(
      {
        code: code.map((oldValue, vi) => {
          if (vi === i) {
            return newValue;
          }
          return oldValue;
        }),
      },
      () => {
        // if (this.state.code.every(v => v !== '')) {
        //   e.target.blur();
        // }
        this.props.onChange(this.state.code);
      },
    );
    textSelect(e.target);
    if (newValue !== '') {
      this.focusOn(i + 1);
    }
  }
  public getPrevBox(i) {
    return this.inputs[i - 1];
  }
  public getNextBox(i) {
    return this.inputs[i + 1];
  }
  public focusOn(i) {
    const element = this.inputs[i];
    if (element) {
      element.focus();
    }
  }
  public onKeyDown(e, i) {
    const inputElement = e.target;
    switch (e.keyCode) {
      case 8:
        // 删除完之后，退回到上一个输入框
        if (e.target.value === '') {
          // 如果空的话，那么就退回到上一个输入框
          e.preventDefault();
          if (i !== 0) {
            this.setState(
              state => {
                state.code[i - 1] = '';
                return state;
              },
              () => {
                this.inputs[i - 1].input.value = '';
                this.focusOn(i - 1);
              },
            );
          }
        }
        break;
      case 37: // 左
      case 38: // 上
        e.preventDefault();
        if (this.getPrevBox(i)) {
          this.focusOn(i - 1);
        } else {
          this.focusOn(i);
        }
        break;
      case 39: // 右
      case 40: // 下
        e.preventDefault();
        if (this.getNextBox(i)) {
          this.focusOn(i + 1);
        } else {
          this.focusOn(i);
        }
        break;
      default:
        // 不管你输入什么
        // 都会聚焦文本
        textSelect(inputElement);
    }
  }
  public handleImgFresh = e => {
    this.props.onRefresh();
  };
  public renderInputs() {
    const { length } = this.props;
    const { code } = this.state;
    const inputs = [];
    const inputStyle = {
      width: `${Math.floor(100 / length)}%`,
    };
    for (let i = 0; i < length; i += 1) {
      inputs.push(
        <Input
          key={i}
          style={inputStyle}
          maxLength={1}
          autoComplete="false"
          autoCorrect="off"
          autoCapitalize="off"
          spellCheck={false}
          value={code[i]}
          ref={node => (this.inputs[i] = node)}
          onFocus={e => textSelect(e.target)}
          onClick={e => textSelect(e.target)}
          onChange={e => this.onChange(e, i)}
          onKeyDown={e => this.onKeyDown(e, i)}
        />,
      );
    }
    return <InputGroup compact>{inputs}</InputGroup>;
  }
  public render() {
    const { captcha, visible, pending, onCancel } = this.props;
    return (
      <Modal
        title="图片验证码"
        width={296}
        visible={visible}
        footer={null}
        onCancel={onCancel}
        maskClosable={false}
      >
        <div className={styles.Codebox__Container}>
          <Spin spinning={pending}>
            <img
              className={styles.Codebox__CaptchaImage}
              alt="点击刷新图片验证码"
              src={captcha}
              onClick={this.handleImgFresh}
            />
          </Spin>
          <div className={styles.Codebox__RefreshTip}>看不清？点击图片刷新</div>
          {this.renderInputs()}
        </div>
      </Modal>
    );
  }
}
