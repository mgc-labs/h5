/**
 * JigsawSlider
 * @author ryan bian
 */
import { Icon, Modal } from 'antd';
import classnames from 'classnames';
import throttle from 'lodash/throttle';
import * as React from 'react';

import styles from './index.module.less';

export enum JigsawSliderStatus {
  INITIAL = 'INITIAL',
  PENDING = 'PENDING',
  PASS = 'PASS',
  FAIL = 'FAIL',
}

interface IJigsawSliderProps {
  backgroundImage: string;
  slideImage: string;
  status: JigsawSliderStatus;
  reloading: boolean;
  visible: boolean;
  offsetY: number;
  onChange: (x: number) => void;
  onCancel: () => void;
  onRefresh: () => void;
}
interface IJigsawSliderState {
  dragging: boolean;
  x: number;
}

const HANDLER_ICON_TYPES = {
  [JigsawSliderStatus.INITIAL]: 'arrow-right',
  [JigsawSliderStatus.PENDING]: 'arrow-right',
  [JigsawSliderStatus.PASS]: 'check',
  [JigsawSliderStatus.FAIL]: 'close',
};
const STATUS_MAP = {
  [JigsawSliderStatus.INITIAL]: 'default',
  [JigsawSliderStatus.PENDING]: 'pending',
  [JigsawSliderStatus.PASS]: 'correct',
  [JigsawSliderStatus.FAIL]: 'error',
};

const SLIDER_WIDTH = 232;
const HANDLER_WIDTH = 36;
const DRAG_WIDTH_LIMIT = SLIDER_WIDTH - HANDLER_WIDTH;

export default class JigsawSlider extends React.PureComponent<
  IJigsawSliderProps,
  IJigsawSliderState
> {
  static defaultProps = {
    status: JigsawSliderStatus.INITIAL,
    reloading: false,
    visible: false,
    onChange() {
      // empty
    },
    onCancel() {
      // empty
    },
    onRefresh() {
      // empty
    },
  };
  static getDerivedStateFromProps(props, state) {
    if (!state.dragging && props.status === JigsawSliderStatus.INITIAL) {
      return {
        x: 0,
      };
    }
    return null;
  }
  public state = {
    dragging: false,
    x: 0,
  };
  public handleDrag = throttle(e => {
    if (!this.state.dragging) {
      return false;
    }
    const offset = e.clientX - this.initialPosition;
    if (offset >= 0 && offset <= DRAG_WIDTH_LIMIT) {
      this.setState({
        x: offset,
      });
    }
  }, 1000 / 60);
  private initialPosition = 0;
  public componentDidMount() {
    window.addEventListener('mouseup', this.handleDragEnd, false);
    window.addEventListener('mousemove', this.handleDrag, false);
  }
  public componentWillUnmount() {
    window.removeEventListener('mouseup', this.handleDragEnd, false);
    window.removeEventListener('mousemove', this.handleDrag, false);
  }
  public handleDragStart = e => {
    this.setState({
      dragging: true,
    });
    this.initialPosition = e.clientX;
  };
  public handleDragEnd = e => {
    if (!this.state.dragging) {
      return false;
    }
    this.props.onChange(this.state.x);
    this.setState({
      dragging: false,
    });
  };
  public renderFigure() {
    const { backgroundImage, slideImage, reloading, offsetY } = this.props;
    const { x } = this.state;
    return (
      <div className={styles.JigsawSlider__Figure}>
        <img width={232} height={130} src={backgroundImage} />
        <img
          className={styles.JigsawSlider__ImagePart}
          src={slideImage}
          style={{ transform: `translateX(${x}px)`, top: `${offsetY}px` }}
        />
        <button
          className={styles.JigsawSlider__Reloading}
          onClick={this.props.onRefresh}
        >
          <Icon type="reload" theme="outlined" spin={reloading} />
        </button>
      </div>
    );
  }
  public renderSlider() {
    const { status } = this.props;
    const { dragging, x } = this.state;
    return (
      <div className={styles.JigsawSlider__Slider}>
        <div
          className={classnames(
            styles.JigsawSlider__Indicator,
            styles[`JigsawSlider__Indicator--${STATUS_MAP[status]}`],
          )}
          style={{
            width: x + 1,
          }}
        />
        <button
          className={classnames({
            [styles.JigsawSlider__Handler]: true,
            [styles['JigsawSlider__Handler--dragging']]: dragging,
            [styles[`JigsawSlider__Handler--${STATUS_MAP[status]}`]]: true,
          })}
          onMouseDown={this.handleDragStart}
          style={{
            transform: `translateX(${x}px)`,
          }}
        >
          <Icon type={HANDLER_ICON_TYPES[status]} theme="outlined" />
        </button>
        <span
          className={classnames({
            [styles.JigsawSlider__Info]: true,
            [styles['JigsawSlider__Info--hide']]: dragging,
          })}
        >
          向右拖动滑块填充拼图
        </span>
      </div>
    );
  }
  public renderTitle() {
    return <div className={styles.JigsawSlider__Title}>图片验证码</div>;
  }
  public renderBody() {
    return (
      <div className={styles.JigsawSlider__Content}>
        {this.renderTitle()}
        {this.renderFigure()}
        {this.renderSlider()}
      </div>
    );
  }
  public render() {
    const { onCancel, visible } = this.props;
    const modalProps = {
      width: 280,
      centered: true,
      visible,
      footer: null,
      onCancel,
    };
    return <Modal {...modalProps}>{this.renderBody()}</Modal>;
  }
}
