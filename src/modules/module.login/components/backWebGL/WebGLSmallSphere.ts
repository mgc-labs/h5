import {
  Mesh,
  MeshLambertMaterial,
  Object3D,
  SphereGeometry,
  TextureLoader,
} from 'three';
import { WebGLBase } from './WebGLBase';
const picURL = require('./img/smallSphere.png');

export class WebGLSmallSphere extends WebGLBase {
  private sphereRadius: number;
  private turnGroup: Object3D;
  private disposed = false;

  constructor(width, height) {
    super(width, height);

    this.sphereRadius = 20; // 球体半径
    this.init();
  }
  init() {
    super.init();
    this.addSphere(); // 球
    this.animate();
  }
  addSphere() {
    this.turnGroup = new Object3D();
    this.turnGroup.position.set(0, 0, 0);
    this.scene.add(this.turnGroup);

    const widthSegments = 50;
    const heightSegments = 30;

    const geometry = new SphereGeometry(
      this.sphereRadius, // 球体半径
      widthSegments, // 球体横截面上的面个数，最小3，默认8
      heightSegments, // 球体纵截面上的上半部份面个数，最小2，默认6
    );

    new TextureLoader().load(
      picURL,
      texture => {
        const material = new MeshLambertMaterial({
          map: texture,
        });

        material.transparent = true;
        material.opacity = 0.1;

        const mesh = new Mesh(geometry, material);
        mesh.position.set(0, 0, 0);
        mesh.name = name;
        this.turnGroup.add(mesh);
      },
      xhr => {
        console.log((xhr.loaded / xhr.total) * 100 + '% loaded');
      },
      xhr => {
        console.log('An error happened');
      },
    );
  }

  animate() {
    const run = () => {
      if (!this.disposed) {
        this.turnGroup.rotation.y += 0.001;
        this.render();
        requestAnimationFrame(run);
      }
    };
    run();
  }

  dispose() {
    this.disposed = true;

    this.scene.remove(this.turnGroup);

    super.dispose();
  }

  resize(width, height) {
    this.width = width;
    this.height = height;
    this.resizeDebounced();
  }
}
