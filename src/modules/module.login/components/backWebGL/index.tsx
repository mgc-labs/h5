import * as React from 'react';
import styles from './style.module.less';
import { WebGLBigSphere } from './WebGLBigSphere';
import { WebGLSmallSphere } from './WebGLSmallSphere';
import { WebGLUnionBack } from './WebGLUnionBack';

export class BackWebGL extends React.PureComponent {
  private bigSphere: WebGLBigSphere;
  private bigSphereDiv: HTMLDivElement;
  private smallSphere: WebGLSmallSphere;
  private smallSphereDiv: HTMLDivElement;
  private backDiv: HTMLDivElement;
  private back: WebGLUnionBack;
  constructor(props: any) {
    super(props);
  }

  getSize = () => {
    return {
      width: window.innerWidth,
      height: window.innerHeight,
    };
  };

  resize = () => {
    const { width, height } = this.getSize();
    this.backDiv.style.width = width + 'px';
    this.backDiv.style.height = height + 'px';
    this.back.resize(width, height);
  };

  componentDidMount() {
    const size = this.getSize();
    this.bigSphere = new WebGLBigSphere(800, 800);
    this.bigSphereDiv.appendChild(this.bigSphere.getElement());
    this.smallSphere = new WebGLSmallSphere(800, 800);
    this.smallSphereDiv.appendChild(this.smallSphere.getElement());
    this.back = new WebGLUnionBack(size.width, size.height);
    this.backDiv.appendChild(this.back.getElement());

    window.addEventListener('onresize', this.resize);
  }

  componentWillUnmount() {
    window.removeEventListener('onresize', this.resize);
    this.bigSphere.dispose();
    this.smallSphere.dispose();
    this.back.dispose();
  }

  render() {
    return (
      <div className={styles.webglContainer}>
        <div className={styles.bigSphere} ref={h => (this.bigSphereDiv = h)} />
        <div
          className={styles.bigSphere}
          ref={h => (this.smallSphereDiv = h)}
        />
        <div className={styles.back} ref={h => (this.backDiv = h)} />
      </div>
    );
  }
}
