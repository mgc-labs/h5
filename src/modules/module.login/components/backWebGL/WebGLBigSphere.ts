import {
  AdditiveBlending,
  Mesh,
  MeshLambertMaterial,
  Object3D,
  Points,
  PointsMaterial,
  SphereGeometry,
  Texture,
} from 'three';
import { WebGLBase } from './WebGLBase';

export class WebGLBigSphere extends WebGLBase {
  private sphereRadius: number;
  private turnGroup: Object3D;
  private disposed = false;

  constructor(width, height) {
    super(width, height);

    this.sphereRadius = 26; // 球体半径
    this.init();
  }
  init() {
    super.init();
    this.addSphere(); // 球
    this.animate();
  }
  createTexture() {
    const canvas = document.createElement('canvas');
    canvas.width = 64;
    canvas.height = 64;
    const context = canvas.getContext('2d');
    const gradient = context.createRadialGradient(
      canvas.width / 2,
      canvas.height / 2,
      0,
      canvas.width / 2,
      canvas.height / 2,
      canvas.width / 2,
    );

    gradient.addColorStop(0, 'rgba(100,100,100,1)');
    gradient.addColorStop(0.15, 'rgba(0,200,200,1)');
    gradient.addColorStop(0.3, 'rgba(0,58,156,1)');
    gradient.addColorStop(1, 'rgba(0,0,0,0)');

    context.fillStyle = gradient;
    context.fillRect(0, 0, canvas.width, canvas.height);

    const texture = new Texture(canvas);
    texture.needsUpdate = true;

    return texture;
  }
  addSphere() {
    this.turnGroup = new Object3D();
    this.turnGroup.position.set(0, 0, 0);
    this.scene.add(this.turnGroup);

    const widthSegments = 28;
    const heightSegments = 14;

    const geometry = new SphereGeometry(
      this.sphereRadius, // 球体半径
      widthSegments, // 球体横截面上的面个数，最小3，默认8
      heightSegments, // 球体纵截面上的上半部份面个数，最小2，默认6
    );

    const material = new PointsMaterial({
      color: '#ffffff',
      size: 3,
      transparent: true,
      blending: AdditiveBlending,
      depthWrite: false /*该属性决定了这个对象是否影响WebGL的深度缓存，将其设置为false，则各个粒子系统之间互不干涉*/,
      map: this.createTexture(),
    });
    material.alphaTest = 0.8;

    const points = new Points(geometry, material);
    points.sortParticles = true;
    this.turnGroup.add(points);

    // 球的表面
    const geometry2 = new SphereGeometry(
      this.sphereRadius, // 球体半径
      widthSegments, // 球体横截面上的面个数，最小3，默认8
      heightSegments, // 球体纵截面上的上半部份面个数，最小2，默认6
    );
    const material2 = new MeshLambertMaterial({ color: '#012053' }); // 材质
    material2.transparent = true;
    material2.opacity = 0.18;
    const meshSphere2 = new Mesh(geometry2, material2);
    this.turnGroup.add(meshSphere2);

    // 球的粒子连线
    const geometry3 = new SphereGeometry(
      this.sphereRadius, // 球体半径
      widthSegments, // 球体横截面上的面个数，最小3，默认8
      heightSegments, // 球体纵截面上的上半部份面个数，最小2，默认6
    );
    const material3 = new MeshLambertMaterial({ color: '#86BDFF' }); // 材质
    material3.wireframe = true;
    material3.transparent = true;
    material3.opacity = 0.2;
    const meshSphere3 = new Mesh(geometry3, material3);
    this.turnGroup.add(meshSphere3);
  }

  animate() {
    const run = () => {
      if (!this.disposed) {
        this.turnGroup.rotation.y += 0.002;
        this.render();
        requestAnimationFrame(run);
      }
    };
    run();
  }

  dispose() {
    this.disposed = true;

    this.scene.remove(this.turnGroup);

    super.dispose();
  }

  resize(width, height) {
    this.width = width;
    this.height = height;
    this.resizeDebounced();
  }
}
