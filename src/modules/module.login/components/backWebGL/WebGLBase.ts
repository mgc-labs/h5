import debounce from 'lodash/debounce';
import {
  DirectionalLight,
  PerspectiveCamera,
  Scene,
  SpotLight,
  WebGLRenderer,
} from 'three';

// 判断浏览器是否支持WebGL
export function isSupportWebGL() {
  try {
    const canvas = document.createElement('canvas');
    return !!(
      window.WebGLRenderingContext &&
      (canvas.getContext('webgl') || canvas.getContext('experimental-webgl'))
    );
  } catch (e) {
    return false;
  }
}

export abstract class WebGLBase {
  public resizeDebounced: any;
  public width: number;
  public height: number;
  public scene: Scene;

  private renderer: WebGLRenderer;
  private camera: PerspectiveCamera;
  private light: DirectionalLight;
  private spotLight: SpotLight;

  constructor(width, height) {
    this.resizeDebounced = debounce(size => {
      if (this.renderer) {
        this.renderer.setSize(this.width, this.height);
      }
    }, 100);
    this.resize(width, height);
  }

  getElement() {
    return this.renderer.domElement;
  }

  init() {
    this.renderer = new WebGLRenderer({ antialias: true, alpha: true }); // 抗据齿
    this.renderer.setSize(this.width, this.height);

    this.scene = new Scene();
    // 透视
    this.camera = new PerspectiveCamera(
      30, // 视角(度)
      this.width / this.height, // 纵横比,即平面长高比
      1, // 物体近平面离摄像头的距离
      150000, // 物体远平面离摄像头的距离
    );
    // 摄像头位置
    this.camera.position.x = 0;
    this.camera.position.y = -30;
    this.camera.position.z = 100;
    // 摄像头上方向
    this.camera.up.x = 0;
    this.camera.up.y = 1;
    this.camera.up.z = 0;
    // 摄像头观察点
    this.camera.lookAt({ x: 0, y: 0, z: 0 });
    this.scene.add(this.camera);

    // 直射光
    this.light = new DirectionalLight('#ffffff', 1);
    this.light.position.set(50, 50, 50);
    this.scene.add(this.light);

    // 聚光灯光源
    this.spotLight = new SpotLight('#ffffff', 6.5, 0, (270 / 180) * Math.PI);
    this.spotLight.position.set(50, 50, -40);
    this.scene.add(this.spotLight);
  }

  render() {
    this.renderer.render(this.scene, this.camera);
  }

  dispose() {
    this.scene.remove(this.camera);
    this.scene.remove(this.light);
    this.scene.remove(this.spotLight);
  }

  toRadian(deg) {
    return (deg * Math.PI) / 180;
  }

  distance(x1, y1, x2, y2) {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
  }
  abstract resize(width, height);
}
