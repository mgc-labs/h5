import {
  Object3D,
  Points,
  PointsMaterial,
  Texture,
  TorusKnotGeometry,
} from 'three';
import { WebGLBase } from './WebGLBase';

export class WebGLUnionBack extends WebGLBase {
  private backPointGroup: Object3D;
  private disposed = false;
  constructor(width, height) {
    super(width, height);
    this.init();
  }

  init() {
    super.init();

    this.addBackPoints(); // 添加背景星际
    this.animate();
  }

  animate() {
    const run = () => {
      if (!this.disposed) {
        this.backPointGroup.rotation.y += 0.00015;
        this.render();
        requestAnimationFrame(run);
      }
    };
    run();
  }

  createTexture() {
    const canvas = document.createElement('canvas');
    canvas.width = 64;
    canvas.height = 64;
    const context = canvas.getContext('2d');
    const gradient = context.createRadialGradient(
      canvas.width / 2,
      canvas.height / 2,
      0,
      canvas.width / 2,
      canvas.height / 2,
      canvas.width / 2,
    );

    gradient.addColorStop(0, 'rgba(100,100,100,1)');
    gradient.addColorStop(0.2, 'rgba(0,200,200,1)');
    gradient.addColorStop(0.4, 'rgba(0,0,64,1)');
    gradient.addColorStop(1, 'rgba(0,0,0,0)');

    context.fillStyle = gradient;
    context.fillRect(0, 0, canvas.width, canvas.height);

    const texture = new Texture(canvas);
    texture.needsUpdate = true;

    return texture;
  }

  addBackPoints() {
    // 添加背景星际
    const geometry = new TorusKnotGeometry(
      280, // radius
      53, // tube 管子半径
      150, // radialSegments
      12, // tubularSegments
      4,
      3,
    );
    const material = new PointsMaterial({
      color: '#ffffff',
      size: 3,
      map: this.createTexture(),
    });
    material.alphaTest = 0.9;

    this.backPointGroup = new Object3D();
    this.backPointGroup.position.x = 0;
    this.backPointGroup.position.y = -30;
    this.backPointGroup.position.z = 70;
    this.scene.add(this.backPointGroup);

    const points = new Points(geometry, material);
    points.rotation.x = this.toRadian(90);
    this.backPointGroup.add(points);
  }

  dispose() {
    this.disposed = true;

    this.scene.remove(this.backPointGroup);

    super.dispose();
  }

  resize(width, height) {
    this.width = width;
    this.height = height;

    this.resizeDebounced();
  }
}
