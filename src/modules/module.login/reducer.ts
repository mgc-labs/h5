/**
 * Login Reducers
 * @author ryan bian
 * @date 2018-9-6 08:49:25
 */

import { Map } from 'immutable';

import * as constants from './constants';
import { LOGIN_MODE } from './saga';

import { JigsawSliderStatus } from './components/JigsawSlider/JigsawSlider';

// The initial state of the App
const initialState = Map({
  mode: LOGIN_MODE.PASSWORD,
  imageCaptcha: Map({
    reloading: false,
    status: JigsawSliderStatus.INITIAL,
    backgroundImage: undefined,
    slideImage: undefined,
    showModal: false,
    location_y: 0,
  }),
  smsCaptcha: Map({
    pending: false,
    countDown: 0,
  }),
  // validateStatus: Map({
  //   pass: false,
  //   pending: false,
  // }),
  loginStatus: Map({
    error: false,
    pending: false,
  }),
  fieldsValue: Map(),
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.GET_IMAGE_CAPTCHA:
      return state.setIn(['imageCaptcha', 'reloading'], true);
    case constants.GET_IMAGE_CAPTCHA_DONE:
      return state
        .setIn(['imageCaptcha', 'reloading'], false)
        .update('imageCaptcha', (map: Map<string, any>) => {
          if (action.error) {
            return map;
          }
          return map
            .set('status', JigsawSliderStatus.INITIAL)
            .set('location_y', action.payload.location_y)
            .set(
              'backgroundImage',
              'data:image/png;base64,' + action.payload.bigImg,
              // `http://necaptcha.nosdn.127.net/e899e9150e3848459da421f614444a61.jpg`,
            )
            .set(
              'slideImage',
              'data:image/png;base64,' + action.payload.smallImg,
              // `http://necaptcha.nosdn.127.net/5ede197bd2fa43fa9f65fc7eb3f3b63d.png`,
            );
        });
    case constants.VALIDATE_IMAGE_CAPTCHA:
      // case constants.VALIDATE_SMS_CAPTCHA:
      return state.setIn(
        ['imageCaptcha', 'status'],
        JigsawSliderStatus.PENDING,
      );
    case constants.VALIDATE_IMAGE_CAPTCHA_DONE:
      // case constants.VALIDATE_SMS_CAPTCHA_DONE:
      return state.setIn(
        ['imageCaptcha', 'status'],
        action.error ? JigsawSliderStatus.FAIL : JigsawSliderStatus.PASS,
      );
    case constants.REQUEST_IDENTIFY_CODE:
      return state.setIn(['smsCaptcha', 'pending'], true);
    case constants.REQUEST_IDENTIFY_CODE_SENDED:
      return state.setIn(['smsCaptcha', 'pending'], false);
    case constants.REQUEST_LOGIN:
      return state
        .setIn(['loginStatus', 'error'], false)
        .setIn(['loginStatus', 'pending'], true);
    case constants.LOGIN_SUCCESS:
      return state
        .setIn(['loginStatus', 'error'], false)
        .setIn(['loginStatus', 'pending'], false);
    case constants.LOGIN_ERROR:
      return state
        .setIn(['loginStatus', 'error'], action.payload)
        .setIn(['loginStatus', 'pending'], false);
    case constants.TOGGLE_LOGIN_TYPE:
      if (state.get('mode') === action.payload) {
        return state;
      }
      return state.set('mode', action.payload).set(
        'fieldsValue',
        Map({
          domainName: state.getIn(['fieldsValue', 'domainName']),
        }),
      );
    case constants.UPDATE_FIELDS_VALUE:
      return state.update('fieldsValue', (map: Map<string, any>) =>
        map.merge(action.payload),
      );
    case constants.SMS_REFETCH_COUNT_DOWN:
      return state.setIn(['smsCaptcha', 'countDown'], action.payload);
    case constants.TOGGLE_CAPTCHA_MODAL:
      return state.setIn(['imageCaptcha', 'showModal'], action.payload);
    default:
      return state;
  }
};

export default reducer;
