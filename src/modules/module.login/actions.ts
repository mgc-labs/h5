/**
 * Login Actions
 * @author ryan bian
 * @date 2018-9-6 08:49:25
 */
import { isError } from 'lodash';
import * as CONSTANTS from './constants';

// 请求短信验证码
export const requestIdentifyCode = params => ({
  payload: params,
  type: CONSTANTS.REQUEST_IDENTIFY_CODE,
});
export const requestIdentifyCodeSended = result => ({
  type: CONSTANTS.REQUEST_IDENTIFY_CODE_SENDED,
  payload: result,
  error: isError(result),
});
// 验证短信验证码
// export const validateSmsCaptcha = data => ({
//   type: CONSTANTS.VALIDATE_SMS_CAPTCHA,
//   payload: data,
// });
// export const validateSmsCaptchaDone = result => ({
//   type: CONSTANTS.VALIDATE_SMS_CAPTCHA_DONE,
//   payload: result,
//   error: isError(result),
// });

// 请求图片验证码
export const getImageCaptcha = () => ({
  type: CONSTANTS.GET_IMAGE_CAPTCHA,
});
export const getImageCaptchaDone = result => ({
  type: CONSTANTS.GET_IMAGE_CAPTCHA_DONE,
  payload: result,
  error: isError(result),
});
// 验证图片验证码
export const validateImageCaptcha = () => ({
  type: CONSTANTS.VALIDATE_IMAGE_CAPTCHA,
});
export const validateImageCaptchaDone = result => ({
  type: CONSTANTS.VALIDATE_IMAGE_CAPTCHA_DONE,
  payload: result,
  error: isError(result),
});

// 重新获取短信倒计时
export const smsRefetchCountDown = countDown => ({
  type: CONSTANTS.SMS_REFETCH_COUNT_DOWN,
  payload: countDown,
});

// 请求登陆
export const requestLogin = params => ({
  payload: params,
  type: CONSTANTS.REQUEST_LOGIN,
});
export const loginSuccess = data => ({
  type: CONSTANTS.LOGIN_SUCCESS,
  payload: data,
});
export const loginError = error => ({
  error: true,
  payload: error,
  type: CONSTANTS.LOGIN_ERROR,
});

export const toggleLoginMode = mode => ({
  type: CONSTANTS.TOGGLE_LOGIN_TYPE,
  payload: mode,
});

export const updateFieldsValue = data => ({
  type: CONSTANTS.UPDATE_FIELDS_VALUE,
  payload: data,
});

export const toggleCaptchaModal = visible => ({
  type: CONSTANTS.TOGGLE_CAPTCHA_MODAL,
  payload: visible,
});
