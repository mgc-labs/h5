export default {
  result: 'success',
  count: null,
  data: {
    sessionkey: 'a1c28c6c57f9bb90bf0095cd4881b253',
    versionsCode: 'P', // 订购版本字典编码;P:普通；Z:智能；D:定制
    buyEndDate: '2019-10-01 00:00:00', // 订购截止时间
    status: '1', // 账户状态：1：生效中；9：已失效
    organizationCode: '', // 组织编号
    lastLoginDate: '2018-10-11 12:01:39', // 上次登录时间
    loginDate: '2018-10-13 17:08:15', // 本次登录时间
    mobileNumber: '', // 员工手机
    departmentNames: '财务部', // 部门名称
    postNames: '出纳', // 岗位名称
    headPortrait: '', // 头像
    isAdmin: '1', // 是否管理员 0:否；1：是
    utmsResourceIds: '', // 资源列表 拥有多个资源时，用[],[]隔开的一串资源编号，比如：[1],[22],[34]
    userName: '13579', // 用户名
    realName: '张三', // 用户姓名
  },
  msg: '登录成功！',
};
