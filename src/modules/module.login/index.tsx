/**
 * Login Component
 * @author yanwei
 * @date 2018-9-6 08:49:25
 */
import classnames from 'classnames';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import * as actions from './actions';
import reducer from './reducer';
import rootSaga, { LOGIN_MODE } from './saga';
import {
  makeSelectFieldsValue,
  makeSelectImageCaptcha,
  makeSelectLoginMode,
  makeSelectLoginStatus,
  makeSelectSmsCaptcha,
} from './selectors';

import Button from 'antd/es/button';
import Col from 'antd/es/col';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';
import message from 'antd/es/message';
import Row from 'antd/es/row';
import Spin from 'antd/es/spin';

import { MOBILE_PHONE } from 'utils/commonRegExp';

// import Codebox from './components/Codebox';
import JigsawSlider from './components/JigsawSlider/JigsawSlider';
import { getLogo } from './service';

import CloseSvg from './img/close';
import CodeSvg from './img/code';
import DomainSvg from './img/domain';
import defaultLogo from './img/logo.png';
import MobileSvg from './img/mobile';
import PwSvg from './img/pw';
import UserSvg from './img/user';

import { BackWebGL } from './components/backWebGL';

import styles from './index.module.less';
import Cookies from 'js-cookie';

export interface ILoginProps extends React.Props<LoginPage> {
  basename: string;
  children: React.ReactNode;
  form: WrappedFormUtils;
  mode: LOGIN_MODE;
  imageCaptcha: Map<string, any>;
  smsCaptcha: Map<string, any>;
  loginStatus: Map<string, any>;
  fieldsValue: Map<string, any>;
  getSmsCaptcha: (params) => any;
  getImageCaptcha: () => any;
  // validateImageCaptcha: (params) => any;
  toggleLoginMode: (mode: LOGIN_MODE) => void;
  getMobileMess: (params?: object) => any;
  requestLogin: (params) => any;
  updateFieldsValue: (data) => any;
  toggleCaptchaModal: (visible) => any;
}

interface ILoginState {
  companyLogo: string;
  canUseWebGL: boolean;
}

const FormItem = Form.Item;

class LoginPage extends React.PureComponent<ILoginProps, ILoginState> {
  static getIsSupportWebGL() {
    try {
      const canvas = document.createElement('canvas');
      return !!(
        (window as any).WebGLRenderingContext &&
        (canvas.getContext('webgl') || canvas.getContext('experimental-webgl'))
      );
    } catch (e) {
      return false;
    }
  }
  public state = {
    canUseWebGL: false,
    companyLogo: defaultLogo,
  };
  public componentDidMount() {
    document.body.style.minWidth = '100%';

    this.isSupportWebGL();

    const { form, basename } = this.props;
    if (basename) {
      const domainName = basename.replace('/', '');
      form.setFieldsValue({
        domainName,
      });
      getLogo(domainName).then(res => {
        if (res.data) {
          this.setState({
            companyLogo: res.data,
          });
        }
      });
    }
    this.props.toggleCaptchaModal(false);

    try {
      Object.keys(Cookies()).map((key: string) => {
        Cookies.remove(key);
        Cookies.remove(key, {
          path: '/',
        });
      });
    } catch (err) {
      // nothing
    }
  }

  public componentWillUnmount() {
    document.body.style.minWidth = '';
  }

  public handleSubmit = e => {
    e.preventDefault();
    const {
      // basename,
      form: { validateFields },
      // history,
      mode,
      requestLogin,
    } = this.props;
    validateFields((err, values) => {
      if (err) {
        return;
      }
      if (mode === LOGIN_MODE.PASSWORD) {
        // 获取图形验证码
        this.handleRefreshCaptcha();
      } else {
        requestLogin({
          identifyCode: values.identifyCode,
        });
      }
    });
  };

  public handleRefreshCaptcha = () => {
    this.props.getImageCaptcha();
  };

  /**
   * 获取验证码
   */
  public fetchCaptcha = e => {
    e.preventDefault();
    const { form, getSmsCaptcha } = this.props;
    const mobileNumber = form.getFieldValue('mobileNumber');
    const domainName = form.getFieldValue('domainName');
    if (!mobileNumber) {
      message.error('请输入手机号');
    } else {
      getSmsCaptcha({
        mobileNumber,
        domainName,
      });
    }
  };

  public handleClearInput = value => {
    this.props.form.resetFields(value);
  };

  public handleCancel = () => {
    this.props.toggleCaptchaModal(false);
  };
  /**
   * 图片验证
   */
  public handleValidateImage = (x: number) => {
    // const { requestLogin } = this.props;
    // const userName = this.props.form.getFieldValue('userName');
    // const domainName = getFieldValue('domainName');
    // 图片验证
    // const params = {
    //   domainName,
    //   userName,
    //   captcha: codeArray.join(''),
    // };
    // this.props.validateImageCaptcha({
    //   coordinateX: x,
    // });
    this.props.requestLogin({
      coordinateX: x,
    });
  };

  public handleToggleMode = (e: any) => {
    const mode = e.currentTarget.dataset.mode;
    this.props.toggleLoginMode(mode);
  };

  public renderPasswordPane() {
    const { fieldsValue } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <React.Fragment>
        <FormItem>
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: '请输入登录用户名' }],
            initialValue: fieldsValue.get('userName'),
          })(
            <Input
              prefix={<Icon component={UserSvg} />}
              placeholder="请输入登录用户名"
              suffix={
                fieldsValue.get('userName') ? (
                  <Icon
                    component={CloseSvg}
                    onClick={() => {
                      this.handleClearInput('userName');
                    }}
                  />
                ) : null
              }
            />,
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: '请输入登录密码' }],
            initialValue: fieldsValue.get('password'),
          })(
            <Input
              prefix={<Icon component={PwSvg} />}
              type={'password'}
              placeholder="请输入登录密码"
              suffix={
                fieldsValue.get('password') ? (
                  <Icon
                    component={CloseSvg}
                    onClick={() => {
                      this.handleClearInput('password');
                    }}
                  />
                ) : null
              }
            />,
          )}
        </FormItem>
      </React.Fragment>
    );
  }
  public renderPhonePane() {
    const { fieldsValue, smsCaptcha } = this.props;
    const { getFieldDecorator } = this.props.form;
    const countDown = smsCaptcha.get('countDown');
    return (
      <React.Fragment>
        <FormItem>
          <Row gutter={10}>
            <Col span={16}>
              {getFieldDecorator('mobileNumber', {
                rules: [
                  {
                    required: true,
                    message: '请输入绑定手机号!',
                  },
                  {
                    pattern: MOBILE_PHONE,
                    message: '手机号不正确!',
                  },
                ],
              })(
                <Input
                  prefix={<Icon component={MobileSvg} />}
                  placeholder="请输入绑定手机号"
                  suffix={
                    fieldsValue.get('mobileNumber') ? (
                      <Icon
                        component={CloseSvg}
                        onClick={() => {
                          this.handleClearInput('mobileNumber');
                        }}
                      />
                    ) : null
                  }
                />,
              )}
            </Col>
            <Col span={8}>
              <Spin spinning={smsCaptcha.get('pending')}>
                <button
                  className={styles.Login__RequestSmsCode}
                  onClick={this.fetchCaptcha}
                  disabled={countDown > 0}
                >
                  {countDown === 0 ? '获取验证码' : `重新获取${countDown}`}
                </button>
              </Spin>
            </Col>
          </Row>
        </FormItem>
        <FormItem>
          {getFieldDecorator('identifyCode', {
            rules: [{ required: true, message: '请输入验证码' }],
            initialValue: fieldsValue.get('identifyCode'),
          })(
            <Input
              prefix={<Icon component={CodeSvg} />}
              type="identifyCode"
              placeholder="请输入验证码"
              suffix={
                fieldsValue.get('identifyCode') ? (
                  <Icon
                    component={CloseSvg}
                    onClick={() => {
                      this.handleClearInput('identifyCode');
                    }}
                  />
                ) : null
              }
            />,
          )}
        </FormItem>
      </React.Fragment>
    );
  }
  public isSupportWebGL() {
    if (LoginPage.getIsSupportWebGL()) {
      this.setState({
        canUseWebGL: true,
      });
    }
  }
  public render() {
    const {
      form: { getFieldDecorator },
      basename,
      mode,
      imageCaptcha,
      loginStatus,
    } = this.props;
    const tabTypeOne = classnames(styles.Login__Switcher, {
      [styles['Login__Switcher--active']]: mode === LOGIN_MODE.PASSWORD,
    });
    const tabTypeTwo = classnames(styles.Login__Switcher, {
      [styles['Login__Switcher--active']]: mode === LOGIN_MODE.PHONE,
    });

    return (
      <main className={styles.Login}>
        {this.state.canUseWebGL ? (
          <BackWebGL />
        ) : (
          <div className={styles.Login__Bg} />
        )}

        {imageCaptcha.get('showModal') ? (
          <JigsawSlider
            visible={imageCaptcha.get('showModal')}
            status={imageCaptcha.get('status')}
            reloading={imageCaptcha.get('reloading')}
            backgroundImage={imageCaptcha.get('backgroundImage')}
            slideImage={imageCaptcha.get('slideImage')}
            offsetY={imageCaptcha.get('location_y')}
            onRefresh={this.handleRefreshCaptcha}
            onChange={this.handleValidateImage}
            onCancel={this.handleCancel}
          />
        ) : null}
        <div className={styles.Login__Content}>
          <div className={styles.Login__Brand}>
            <div
              className={styles.Login__BrandLogo}
              style={{ backgroundImage: `url(${this.state.companyLogo})` }}
            />
          </div>
          <div className={styles.Login__Main}>
            <div className={styles.Login__ModeSwitch}>
              <button
                data-mode="password"
                className={tabTypeOne}
                onClick={this.handleToggleMode}
              >
                账户密码登录
              </button>
              <button
                data-mode="phone"
                className={tabTypeTwo}
                onClick={this.handleToggleMode}
              >
                手机号登录
              </button>
            </div>
            <Form onSubmit={this.handleSubmit} className={styles.Login__Form}>
              <FormItem>
                {getFieldDecorator('domainName', {
                  rules: [{ required: true, message: '请输入组织编码' }],
                })(
                  <Input
                    prefix={<Icon component={DomainSvg} />}
                    placeholder="请输入组织编码"
                    disabled={!!basename}
                  />,
                )}
              </FormItem>
              {mode === 'password'
                ? this.renderPasswordPane()
                : this.renderPhonePane()}
              <FormItem>
                <Button
                  type="primary"
                  htmlType="submit"
                  block
                  loading={loginStatus.get('pending')}
                >
                  登录
                </Button>
              </FormItem>
            </Form>
          </div>
        </div>
      </main>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getSmsCaptcha: params => dispatch(actions.requestIdentifyCode(params)),
  getImageCaptcha: () => dispatch(actions.getImageCaptcha()),
  // validateImageCaptcha: params =>
  //   dispatch(actions.validateImageCaptcha(params)),
  toggleLoginMode: mode => dispatch(actions.toggleLoginMode(mode)),
  updateFieldsValue: data => dispatch(actions.updateFieldsValue(data)),
  requestLogin: data => dispatch(actions.requestLogin(data)),
  toggleCaptchaModal: visible => dispatch(actions.toggleCaptchaModal(visible)),
});

const mapStateToProps = createStructuredSelector({
  imageCaptcha: makeSelectImageCaptcha(),
  smsCaptcha: makeSelectSmsCaptcha(),
  mode: makeSelectLoginMode(),
  fieldsValue: makeSelectFieldsValue(),
  loginStatus: makeSelectLoginStatus(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga: rootSaga });

export default compose(
  withReducer,
  withConnect,
  withSaga,
  Form.create({
    onFieldsChange: (props: ILoginProps, fields: any) => {
      if (fields.password && props.imageCaptcha.get('showModal')) {
        return;
      }
      const newFieldsValue = {};
      Object.keys(fields).forEach(key => {
        if (!fields[key].validating) {
          Object.assign(newFieldsValue, {
            [key]: fields[key].value,
          });
        }
      });
      if (Object.keys(newFieldsValue).length > 0) {
        props.updateFieldsValue(newFieldsValue);
      }
    },
  }),
)(LoginPage);
