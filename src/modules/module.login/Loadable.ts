/**
 * Login Loadable
 * @author ryan bian
 * @date 2018-9-6 08:49:25
 */
import Loadable from 'react-loadable';

import LoadingIndicator from 'components/LoadingIndicator';

export default Loadable({
  loader: () => import(/* webpackChunkName: "login" */ './index'),
  loading: LoadingIndicator,
});
