/**
 * VehicleOrderDetail Component
 * @author lhf
 * @date 2018-9-15 10:48:48
 */
import { List, Map } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import Col from 'antd/es/col';
import Row from 'antd/es/row';
import {
  FormDetailCard,
  FormDetailField,
  formLayoutCols2,
  formLayoutCols3,
  gutter,
} from 'components/FormDetailCard';
import GLOBAL from 'utils/GLOBAL';
import styles from './index.module.less';
import {
  makeSelectDetail,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

export interface IVehicleOrderDetailProps {
  children: React.ReactChildren;
  detail: Map<string, any>;
  searchParams: {
    id: number;
  };
  getDetail: (options?: object) => any;
}

class VehicleOrderDetail extends React.PureComponent<IVehicleOrderDetailProps> {
  public componentDidMount() {
    this.getDetail();
  }

  public render() {
    return (
      <div className={styles.VehicleOrderDetail}>
        {this.renderTransportInfo()}
        {this.renderBasicInfo()}
        {this.renderGoodsInfo()}
        {this.renderServiceInfo()}
        {this.renderCostInfo()}
      </div>
    );
  }

  private getDetail() {
    const { searchParams } = this.props;
    this.props.getDetail({
      orderWholeCarId: searchParams.id,
    });
  }

  // 运输信息
  private renderTransportInfo() {
    const { detail } = this.props;
    const status = ['待发货', '待调度', '配送中', '已签收', '未签收'];
    const tradeStatus = [
      '待调度',
      '待接单',
      '待签到',
      '待装货',
      '待卸货',
      '已完成',
      '已取消',
      '待改派',
    ];
    return (
      <FormDetailCard title="运输信息">
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField label="订单号" value={detail.get('orderNumber')} />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="状态"
              value={status[detail.get('status')] || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="运单号"
              value={detail.get('tradeNumber') || GLOBAL.emptyRecord}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="状态"
              value={
                tradeStatus[detail.get('tradeStatus')] || GLOBAL.emptyRecord
              }
            />
          </Col>
        </Row>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="司机姓名"
              value={detail.get('driverName') || GLOBAL.emptyRecord}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="司机电话"
              value={detail.get('driverPhone') || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </FormDetailCard>
    );
  }

  // 渲染装卸地
  private renderAddress(data) {
    const name = data.get('loadType') === 1 ? '卸货地' : '装货地';
    return (
      <Row gutter={gutter} key={data.get('loadInfoId')}>
        <Col {...formLayoutCols3}>
          <FormDetailField label={name} value={data.get('pointAddress')} />
        </Col>
        <Col {...formLayoutCols3}>
          <FormDetailField
            label="联系人"
            value={data.get('contact') || GLOBAL.emptyRecord}
          />
        </Col>
        <Col {...formLayoutCols3}>
          <FormDetailField
            label="联系电话"
            value={data.get('phone') || GLOBAL.emptyRecord}
          />
        </Col>
      </Row>
    );
  }

  // 运输信息
  private renderBasicInfo() {
    const { detail } = this.props;
    return (
      <FormDetailCard title="基本信息">
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="选择客户"
              value={detail.get('customerName')}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="角色码"
              value={detail.get('customerCode')}
            />
          </Col>
        </Row>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="所需车型"
              value={
                detail.get('carLong')
                  ? detail.get('carLong') / 1000 +
                    '米' +
                    detail.get('carStruct')
                  : detail.get('carStruct')
              }
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="用车时间"
              value={detail.get('useCarDate')}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="要求送达时间"
              value={detail.get('requireDate')}
            />
          </Col>
        </Row>
        {detail
          .get('loadList', List())
          .map(item => this.renderAddress(item))
          .toArray()}
        <Row gutter={gutter}>
          <Col {...formLayoutCols2}>
            <FormDetailField
              label="备注"
              value={detail.get('remark') || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </FormDetailCard>
    );
  }

  // 货物信息
  private renderGoodsInfo() {
    const { detail } = this.props;
    return (
      <FormDetailCard title="货物信息">
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField label="货物名称" value={detail.get('goodsName')} />
          </Col>
        </Row>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="重量(千克)"
              value={detail.get('goodsWeight')}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="体积(方)"
              value={detail.get('goodsVolume')}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="件数(件)"
              value={detail.get('goodsNumber')}
            />
          </Col>
        </Row>
      </FormDetailCard>
    );
  }

  // 增值服务信息
  private renderServiceInfo() {
    const { detail } = this.props;
    return (
      <FormDetailCard title="增值服务">
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="回单"
              value={detail.get('isReceipt') ? '是' : '否'}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="代收(元)"
              value={detail.get('collictionAmount') || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </FormDetailCard>
    );
  }
  // 货物信息
  private renderCostInfo() {
    const { detail } = this.props;
    return (
      <FormDetailCard title="运费信息">
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="客户运费(元)"
              value={detail.get('customerFreight') || GLOBAL.emptyRecord}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="司机运费(元)"
              value={detail.get('driverFreight') || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </FormDetailCard>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getDetail: options => dispatch(actions.getDetail(options)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    detail: makeSelectDetail(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(VehicleOrderDetail);
};
