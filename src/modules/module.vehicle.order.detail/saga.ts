/**
 * VehicleOrderDetail Saga
 * @author lhf
 * @date 2018-9-15 10:48:48
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import { getDetail } from './service';

export default (CONSTANTS, actions) => {
  function* fetchDetail(action) {
    try {
      const data = yield call(getDetail, action.payload);
      yield put(actions.getDetailDone(data));
    } catch (err) {
      yield put(actions.getDetailError(err));
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_DETAIL, fetchDetail);
  };
};
