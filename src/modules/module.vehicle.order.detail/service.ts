/**
 * vehicle.order.detail Service
 * @author lhf
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const PATHLIST_DETAIL = '/ehuodiGateway/utmsTrade/orderWholeCar/selectDetail';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', PATHLIST_DETAIL)
    .reply(() =>
      import('./mock/detail').then(exports => [200, exports.default]),
    );
}

/**
 * 获取详情
 */
export const getDetail = data => {
  return request(
    {
      method: 'post',
      url: PATHLIST_DETAIL,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
