/**
 * VehicleOrderDetail Reducers
 * @author lhf
 * @date 2018-9-15 10:48:48
 */

import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  data: {},
  error: false,
  loading: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_DETAIL:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_DETAIL_SUCCESS:
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data));
      case CONSANTS.GET_DETAIL_ERROR:
        return state.set('error', action.error).set('loading', false);
      default:
        return state;
    }
  };
};
