/**
 * VehicleOrderDetail Actions
 * @author lhf
 * @date 2018-9-15 10:48:48
 */
import { AnyAction } from 'redux';

export default CONSANTS => ({
  getDetail: (options): AnyAction => {
    return {
      payload: options,
      type: CONSANTS.GET_DETAIL,
    };
  },
  getDetailDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_DETAIL_SUCCESS,
  }),
  getDetailError: (error): AnyAction => ({
    error,
    type: CONSANTS.GET_DETAIL_ERROR,
  }),
});
