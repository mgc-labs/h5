/**
 * VehicleOrderDetail selectors
 * @author lhf
 * @date 2018-9-15 10:48:48
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectDetail = selectState =>
  createSelector(selectState, (state: Map<string, any>) => {
    return state.get('data');
  });
