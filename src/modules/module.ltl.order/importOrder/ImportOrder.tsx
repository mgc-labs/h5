/**
 * Import Order Component
 * @author ryan bian
 */
import Alert from 'antd/es/alert';
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Icon from 'antd/es/icon';
import Modal from 'antd/es/modal';
import Select from 'antd/es/select';
import Table from 'antd/es/table';
import Upload from 'antd/es/upload';
import { Map } from 'immutable';
import * as React from 'react';

import CustomerSelect from 'components/CustomerSelect';
import { COLUMN_LIST, COLUMN_WIDTH, COLUMNS_CONFIG } from './columns';
import EditableCell from './EditableCell';
import EditableFormRow from './EditableFormRow';

import { importModifyOrders } from '../service';

import importTemplatePath from '../assets/template.xlsx';
import styles from '../index.module.less';

const FormItem = Form.Item;

interface IImportOrderProps {
  importData: Map<string, any>;
  form: WrappedFormUtils;
  onImport: (data: object) => void;
  onReImport: () => void;
  editImportValid: (data: object) => void;
  setImportStatus: (status: string) => void;
}

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 12,
  },
};

@Form.create({})
export default class ImportOrder extends React.PureComponent<
  IImportOrderProps
> {
  public normFile = e => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };
  public onFileChange = ({ fileList }) => {
    setTimeout(() => {
      this.props.form.setFieldsValue({
        file: fileList.slice(-1),
      });
    }, 10);
  };
  public handleSubmit = () => {
    const { importData, form, onImport } = this.props;
    const status = importData.get('status');
    if (status === 'beforeImport') {
      form.validateFields((errors, values) => {
        if (!errors) {
          onImport(values);
        }
      });
    } else if (status === 'invalid') {
      this.props.onReImport();
    }
  };
  public handleSave = row => {
    this.props.editImportValid(row);
  };
  public handleReImport = () => {
    const { importData } = this.props;
    importModifyOrders({
      customerCode: importData.get('customerCode'),
      customerName: importData.get('customerName'),
      fileId: importData.get('fileId'),
      isContinue: 1,
      total: importData.get('total'),
      rightCount: importData.get('rightCount'),
      errorCount: importData.get('errorCount'),
    }).then(() => {
      this.props.setImportStatus('beforeImport');
    });
  };
  public renderPreImportEditor() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Alert
          message="注意：批量导入订单请使用官方提供的模版，否则会导入失败。"
          type="info"
          showIcon
        />
        <Form className={styles.ImportOrder__PreEditor}>
          <FormItem {...formItemLayout} label="选择客户" required>
            {getFieldDecorator('customer', {
              rules: [{ required: true, message: '请选择客户' }],
            })(<CustomerSelect />)}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="上传文件"
            required
            extra={
              <a href={importTemplatePath} download="订单批量导入模版.xlsx">
                订单批量导入模版.xlsx
              </a>
            }
          >
            {getFieldDecorator('file', {
              rules: [{ required: true, message: '请上传导入文件' }],
              getValueFromEvent: this.normFile,
              valuePropName: 'fileList',
            })(
              <Upload beforeUpload={() => false} onChange={this.onFileChange}>
                <Button>
                  <Icon type="upload" /> 点击上传
                </Button>
              </Upload>,
            )}
          </FormItem>
        </Form>
      </div>
    );
  }
  public renderInvalidEditor() {
    const { importData } = this.props;
    const orderDatas = importData.get('orderDatas').toArray();
    const dataSource = orderDatas.map((list, index) => ({
      index,
      key: index,
      ...list.toObject(),
    }));
    const columns = COLUMN_LIST.map((columnName, index) => {
      const column = COLUMNS_CONFIG[columnName];
      return {
        ...column,
        dataIndex: columnName,
        onCell: record => ({
          ...column,
          record,
          dataIndex: columnName,
          handleSave: this.handleSave,
        }),
      };
    });
    // console.log(dataSource);
    const tableProps = {
      className: styles.EditTable,
      title: () => '导入失败的列表',
      rowClassName: () => 'editable-row',
      components: {
        body: {
          row: EditableFormRow,
          cell: EditableCell,
        },
      },
      dataSource,
      columns,
      bordered: true,
      scroll: {
        x: COLUMN_WIDTH,
        y: 200,
      },
      rowKey: 'key',
      size: 'small',
    };
    return (
      <div>
        <Alert
          message={`导入${importData.get('total')}条订单。 成功${importData.get(
            'rightCount',
          )}条，失败${importData.get('errorCount')}条。`}
          description="导入失败的内容可双击进行直接修改，修改完成后点击“已修改，继续导入”"
          type="info"
          showIcon
        />
        <Table {...tableProps} />
      </div>
    );
  }
  public render() {
    const { importData } = this.props;
    const modalProps = {
      visible: true,
      width: 800,
      onOk: this.handleSubmit,
      confirmLoading: importData.get('pending'),
      maskClosable: false,
      centered: true,
    };
    const status = importData.get('status');
    if (status === 'beforeImport') {
      Object.assign(modalProps, {
        title: '订单导入',
        okText: '导入Excel',
        cancelText: '取消',
        children: this.renderPreImportEditor(),
        onCancel: () => this.props.setImportStatus('success'),
      });
    } else if (status === 'invalid') {
      Object.assign(modalProps, {
        title: '导入确认',
        okText: '已修改，继续导入',
        cancelText: '重新导入Excel',
        children: this.renderInvalidEditor(),
        onCancel: this.handleReImport,
        okButtonProps: {
          disabled: importData
            .get('orderDatas')
            .some(map =>
              map.get('error', Map()).some(value => value && value !== true),
            ),
        },
      });
    } else {
      Object.assign(modalProps, {
        visible: false,
      });
    }
    return <Modal {...modalProps} />;
  }
}
