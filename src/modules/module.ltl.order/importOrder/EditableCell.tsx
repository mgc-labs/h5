import DatePicker from 'antd/es/date-picker';
import Form from 'antd/es/form';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';
import classnames from 'classnames';
import moment from 'moment';
import * as React from 'react';
import EditableContext from './EditableContext';

import styles from '../index.module.less';

const FormItem = Form.Item;

interface IEditableCellProps {
  dataIndex: string;
  title: string;
  index: number;
  className: string;
  width: number;
  type: string;
  record: object;
  handleSave: (data: object) => void;
}

export default class EditableCell extends React.PureComponent<
  IEditableCellProps
> {
  private form;
  save = () => {
    const { dataIndex, record, handleSave } = this.props;
    this.form.validateFields([dataIndex], (errors, values) => {
      if (errors) {
        return;
      }
      handleSave({ ...record, ...values });
    });
  };
  render() {
    const {
      dataIndex,
      title,
      record,
      index,
      handleSave,
      className,
      width,
      type,
      ...restProps
    } = this.props;
    const errorMap = {};
    record.error.forEach((value, key) => {
      Object.assign(errorMap, {
        [key]: value,
      });
    });
    return (
      <td {...restProps} className={classnames(className, styles.EditableCell)}>
        {
          <EditableContext.Consumer>
            {form => {
              this.form = form;
              const Comp = type === 'datePicker' ? DatePicker : Input;
              const props = {};
              if (errorMap[dataIndex] && errorMap[dataIndex] !== true) {
                Object.assign(props, {
                  validateStatus: 'error',
                  help: errorMap[dataIndex],
                });
              }
              return (
                <FormItem {...props} style={{ margin: 0 }}>
                  {errorMap[dataIndex] ? (
                    form.getFieldDecorator(dataIndex, {
                      rules: [
                        {
                          required: true,
                          message: `${title} is required.`,
                        },
                      ],
                      initialValue:
                        type === 'datePicker'
                          ? moment(record[dataIndex])
                          : record[dataIndex],
                    })(
                      <Comp
                        ref={node => (this.input = node)}
                        size={'small'}
                        suffix={
                          <button
                            className={styles.EditableCell__ConfirmBtn}
                            onClick={this.save}
                          >
                            <Icon type="check" />
                          </button>
                        }
                        onChange={() => {
                          if (type === 'datePicker') {
                            this.save();
                          }
                        }}
                      />,
                    )
                  ) : (
                    <div className={styles.EditableCell__ValueWrap}>
                      {restProps.children}
                    </div>
                  )}
                </FormItem>
              );
            }}
          </EditableContext.Consumer>
        }
      </td>
    );
  }
}
