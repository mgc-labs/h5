export const COLUMN_LIST = [
  'thirdSystemId',
  'longitude',
  'latitude',
  'contact',
  'phone',
  'province',
  'address',
  'requireDate',
  'goodsName',
  'goodsWeight',
  'goodsVolume',
  'goodsNumber',
  'isReceipt',
  'delegateAmount',
  'remark',
];

export const COLUMNS_CONFIG = {
  address: {
    title: '详细地址',
    width: 150,
  },
  contact: {
    title: '联系人',
    width: 120,
  },
  delegateAmount: {
    title: '代收货款(元)',
    width: 120,
  },
  goodsName: {
    title: '货物名称',
    width: 120,
  },
  goodsNumber: {
    title: '件数(件)',
    width: 120,
  },
  goodsVolume: {
    title: '体积(方)',
    width: 120,
  },
  goodsWeight: {
    title: '重量(千克)',
    width: 120,
  },
  isReceipt: {
    title: '回单服务',
    width: 120,
  },
  latitude: {
    title: '纬度',
    width: 120,
  },
  longitude: {
    title: '经度',
    width: 120,
  },
  phone: {
    title: '联系电话',
    width: 120,
  },
  province: {
    title: '省市区',
    width: 140,
  },
  remark: {
    title: '备注',
    width: 150,
  },
  requireDate: {
    title: '要求到达时间',
    width: 120,
    type: 'datePicker',
  },
  thirdSystemId: {
    title: '第三方单号',
    width: 120,
    // fixed: 'left',
  },
};

export const COLUMN_WIDTH = COLUMN_LIST.reduce(
  (prev, key) => (prev += COLUMNS_CONFIG[key].width),
  0,
);
