import * as React from 'react';

const EditableContext = React.createContext();

export default EditableContext;
