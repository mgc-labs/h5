import Form from 'antd/es/form';
import * as React from 'react';

import EditableContext from './EditableContext';

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

export default Form.create()(EditableRow);
