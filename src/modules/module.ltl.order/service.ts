/**
 * Auth Service
 * @author djd
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_FETCHORDERLIST =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/partOrderList';
const API_IMPORT_ORDERS =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/importPartOrderList';
const API_IMPORT_MODIFY_ORDERS =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/continueImportPartOrder';
const API_ORDER_STATISTICS =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/orderStatistics';

const API_ORDER_DELETES =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/deletePartOrder';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_FETCHORDERLIST)
    .reply(() =>
      import('./mock/orderList').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_IMPORT_ORDERS)
    .reply(() =>
      import('./mock/import').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_IMPORT_MODIFY_ORDERS)
    .reply(() =>
      import('./mock/import').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_ORDER_DELETES)
    .reply(() =>
      import('./mock/deleteOrder').then(exports => [200, exports.default]),
    );
}

/**
 * 查询订单信息
 */
export const getOrderList = params =>
  request(
    {
      method: 'post',
      url: API_FETCHORDERLIST,
      data: params,
    },
    {
      useMock: false,
    },
  );

export const importOrders = data =>
  request(
    {
      method: 'post',
      url: API_IMPORT_ORDERS,
      data,
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  );

export const importModifyOrders = data =>
  request(
    {
      method: 'post',
      url: API_IMPORT_MODIFY_ORDERS,
      data,
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  );

export const getOrderStatistics = data =>
  request(
    {
      method: 'post',
      url: API_ORDER_STATISTICS,
      data,
    },
    {
      useMock: false,
    },
  );

export const deleteOrders = data =>
  request(
    {
      method: 'post',
      url: API_ORDER_DELETES,
      data,
    },
    {
      useMock: false,
    },
  );
