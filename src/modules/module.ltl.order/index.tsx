/**
 * LtlOrder Component
 * @author djd
 * @date 2018/9/13 下午1:48:50
 */
import msngr from 'msngr';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import CheckBox from 'antd/es/checkbox';
import Modal from 'antd/es/modal';
import MyTable from 'components/MyTable';
import {
  PageBottom,
  PageBottomButtons,
  PageBottomCheck,
} from 'components/PageBottom';

import { List, Map } from 'immutable';
import GLOBAL from 'utils/GLOBAL';
import { Hash } from 'utils/Hash';

import {
  makeSelectError,
  makeSelectImportData,
  makeSelectLoading,
  makeSelectOrderCount,
  makeSelectOrderData,
  makeSelectOrderStatistics,
} from './selectors';

import Condition from './condition';
import styles from './index.module.less';

import ImportOrder from './importOrder/ImportOrder';
import { TotalComponent } from './totalComponent';

export interface ILtlOrderProps {
  children: React.ReactChildren;
  data: List<Map<string, any>>;
  loading?: boolean;
  statistics: List<Map<string, any>>;
  count: number;
  importData: Map<string, any>;
  permissions: List<Map<string, any>>;
  getOrderListData: (pars?: any) => any;
  onCheckedChanged: (record: any) => void;
  onAllCheckChanged: (checked: boolean) => void;
  onDeleteOrders: (partOrderIds: string[]) => void;
  importOrders: (data: object) => void;
  importModifyOrders: (data: object) => void;
  editImportValid: (data: object) => void;
  setImportStatus: (status) => void;
  getOrderStatistics: () => any;
}
const DEFAULT_NULL = GLOBAL.emptyRecord;
class LtlOrder extends React.PureComponent<ILtlOrderProps> {
  public state = {
    current: 1,
    permissions: [],
  };

  private pars = {
    pageSize: 15,
    skipCount: 0,
  };
  private condition = {};
  onShowSizeChange = pageInfo => {
    if (pageInfo) {
      pageInfo.current = 1;
    }
    this.onFetch(pageInfo);
  };
  onFetch = pageInfo => {
    this.pars.pageSize =
      (pageInfo ? pageInfo.pageSize : 15) || this.pars.pageSize;
    this.pars.skipCount = pageInfo ? pageInfo.skipCount : 0;
    this.props.getOrderListData({
      ...this.pars,
      ...pageInfo,
      ...this.condition,
    });
    this.setState({
      current: (pageInfo ? pageInfo.current : 1) || 1,
    });
  };

  onSearch = (props: Hash) => {
    this.pars.skipCount = 0;
    this.condition = props;
    this.setState({
      current: 1,
    });
    this.props.getOrderListData({
      ...this.pars,
      ...this.condition,
    });
    this.props.getOrderStatistics();
  };
  setImportStatus = (par: any) => {
    this.props.setImportStatus('beforeImport');
  };

  onDeleteOrders = () => {
    const { onDeleteOrders, getOrderStatistics } = this.props;
    const list = this.props.data
      .toJS()
      .filter(h => h.isChecked && h.status === '待配载')
      .map(h => h.utmsPartOrderId);
    if (list.length > 0) {
      Modal.confirm({
        title: '删除确认',
        content: '是否删除选中的订单',
        iconType: 'info-circle',
        className: 'ant-confirm-info',
        centered: true,
        onOk() {
          onDeleteOrders(list);
        },
      });
    }
  };

  public componentDidMount() {
    this.handlePermissions();
    this.props.getOrderListData({
      ...this.pars,
      ...this.condition,
    });
    this.props.getOrderStatistics();
    msngr('/ltlOrder', 'import success').on(this.updateHandler);
    msngr('/ltlOrder', 'refresh').on(this.updateHandler);
  }
  public componentWillUnmount() {
    msngr('/ltlOrder', 'import success').drop(this.updateHandler);
    msngr('/ltlOrder', 'refresh').drop(this.updateHandler);
  }
  // 取权限
  public handlePermissions() {
    const { permissions } = this.props;
    this.setState({
      permissions: permissions.toArray().map(data => data.get('operateKey')),
    });
  }
  public updateHandler = () => {
    this.props.getOrderListData({
      ...this.pars,
      ...this.condition,
    });
    this.props.getOrderStatistics();
  };

  render() {
    const { current, permissions } = this.state;
    const {
      importData,
      importOrders,
      importModifyOrders,
      setImportStatus,
      editImportValid,
      data,
      count,
      loading,
      statistics,
    } = this.props;
    const list = data.toJS();
    const columns = [
      {
        width: 40,
        fixed: 'left',
        key: 'checkbox',
        render: (record: any) => {
          if (record.status === '待配载') {
            return (
              <CheckBox
                checked={record.isChecked}
                onChange={() => this.props.onCheckedChanged(record)}
              />
            );
          }
        },
      },
      {
        title: '第三方订单号',
        dataIndex: 'thirdSystemId',
        width: 225,
        fixed: 'left',
        render: text => (text ? text : DEFAULT_NULL),
      },
      {
        title: '订单号',
        dataIndex: 'orderNumber',
        width: 225,
        fixed: 'left',
        render: (text, record) => (
          <span>
            <Link to={`/ltlOrder/detail?id=${record.utmsPartOrderId}`}>
              {text}
            </Link>
          </span>
        ),
      },
      {
        title: '状态',
        dataIndex: 'status',
        width: 80,
      },
      {
        title: '客户名称',
        width: 150,
        dataIndex: 'customerName',
      },
      {
        title: '发布时间',
        dataIndex: 'gmtCreate',
        width: 190,
      },
      {
        title: '要求送达时间',
        width: 190,
        dataIndex: 'requireDate',
        render: text => (text ? text : DEFAULT_NULL),
      },
      {
        title: '收货人姓名',
        width: 120,
        dataIndex: 'contact',
      },
      {
        title: '联系电话',
        width: 135,
        dataIndex: 'phone',
      },
      {
        title: '省市区',
        width: 180,
        dataIndex: 'province',
      },
      {
        title: '收货地址',
        width: 250,
        dataIndex: 'address',
      },
      {
        title: '操作',
        key: 'operation',
        fixed: 'right',
        width: 110,
        render: (text, record) => {
          if (record.status === '待配载') {
            return (
              <Link to={`/ltlOrder/edit?id=${record.utmsPartOrderId}`}>
                修改
              </Link>
            );
          }
        },
      },
    ];

    let width = 0;
    columns.map(col => {
      width += col.width || 0;
    });
    return (
      <div className={styles.LtlOrder}>
        <TotalComponent {...statistics} />

        <Card
          title="零担订单"
          bordered={true}
          extra={
            <div>
              {permissions.includes('add') ? (
                <Link to="/ltlOrder/add">
                  <Button style={{ marginRight: '20px' }}>零担开单</Button>
                </Link>
              ) : (
                ''
              )}
              <Button type="primary" onClick={this.setImportStatus}>
                订单导入
              </Button>
            </div>
          }
        >
          <Condition onSearch={this.onSearch} />
          <MyTable
            columns={columns}
            rowKey="utmsPartOrderId"
            dataSource={list}
            scroll={{ x: width }}
            loading={loading}
            pagination={{
              current,
              onChange: this.onFetch,
              onShowSizeChange: this.onShowSizeChange,
              total: count,
            }}
          />
        </Card>
        <PageBottom
          leftChild={
            <PageBottomCheck
              total={list.filter(item => item.status === '待配载').length}
              checked={
                list.filter(item => item.isChecked && item.status === '待配载')
                  .length
              }
              onAllCheckChanged={this.props.onAllCheckChanged}
            />
          }
          rightChild={
            <PageBottomButtons
              buttons={[
                <Button key={2} type="primary" onClick={this.onDeleteOrders}>
                  批量删除
                </Button>,
              ]}
            />
          }
        />
        <ImportOrder
          importData={importData}
          onImport={importOrders}
          onReImport={importModifyOrders}
          editImportValid={editImportValid}
          setImportStatus={setImportStatus}
        />
      </div>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getOrderListData: (data: any) => dispatch(actions.getOrderListData(data)),
    onCheckedChanged: (record: any) => dispatch(actions.checkedChanged(record)),
    onAllCheckChanged: checked => dispatch(actions.allCheckedChanged(checked)),
    onDeleteOrders: (list: string[]) => dispatch(actions.deleteOrders(list)),
    importOrders: data => dispatch(actions.importOrders(data)),
    importModifyOrders: data => dispatch(actions.importModifyOrders(data)),
    editImportValid: data => dispatch(actions.editImportValid(data)),
    backToImport: () => dispatch(actions.backToImport()),
    setImportStatus: status => dispatch(actions.setImportStatus(status)),
    getOrderStatistics: status => dispatch(actions.getOrderStatistics()),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    data: makeSelectOrderData(selectState),
    count: makeSelectOrderCount(selectState),
    importData: makeSelectImportData(selectState),
    statistics: makeSelectOrderStatistics(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(LtlOrder);
};
