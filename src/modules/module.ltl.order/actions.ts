/**
 * LtlOrder Actions
 * @author djd
 * @date 2018/9/13 下午1:48:50
 */
import { AnyAction } from 'redux';

export default CONSTANTS => ({
  getOrderListData: (data): AnyAction => ({
    type: CONSTANTS.GET_LIST_DATA,
    payload: data,
  }),

  getOrderListDone: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.GET_LIST_DATA_SUCCESS,
  }),

  getOrderListError: (error): AnyAction => ({
    error,
    type: CONSTANTS.GET_LIST_DATA_ERROR,
  }),

  checkedChanged: (data): AnyAction => ({
    type: CONSTANTS.RECORD_CHECKED_CHANGED,
    payload: data,
  }),

  allCheckedChanged: (checked): AnyAction => ({
    type: CONSTANTS.RECORD_ALLCHECKED_CHANGED,
    payload: checked,
  }),

  importOrders: data => ({
    type: CONSTANTS.IMPORT_ORDERS,
    payload: data,
  }),

  importOrdersDone: (data, error) => ({
    type: CONSTANTS.IMPORT_ORDERS_DONE,
    payload: data,
    error,
  }),

  importModifyOrders: data => ({
    type: CONSTANTS.IMPORT_MODIFY_ORDERS,
    payload: data,
  }),

  editImportValid: data => ({
    type: CONSTANTS.EDIT_IMPORT_INVALID,
    payload: data,
  }),

  backToImport: data => ({
    type: CONSTANTS.BACK_TO_IMPORT,
  }),

  setImportStatus: status => ({
    type: CONSTANTS.SET_IMPORT_STATUS,
    payload: status,
  }),
  getOrderStatistics: (): AnyAction => ({
    type: CONSTANTS.GET_ORDER_STATISTICS,
  }),

  getOrderStatisticsDone: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.GET_ORDER_STATISTICS_SUCCESS,
  }),

  getOrderStatisticsError: (error): AnyAction => ({
    error,
    type: CONSTANTS.GET_ORDER_STATISTICS_ERROR,
  }),

  deleteOrders: (list): AnyAction => ({
    type: CONSTANTS.DELETE_ORDERS,
    payload: list,
  }),
  deleteOrdersDone: (): AnyAction => ({
    type: CONSTANTS.DELETE_ORDERS_SUCCESS,
  }),
  deleteOrdersError: (error): AnyAction => ({
    type: CONSTANTS.DELETE_ORDERS_ERROR,
    error,
  }),
});
