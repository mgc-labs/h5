/**
 * LtlOrder Saga
 * @author djd
 * @date 2018/9/13 下午1:48:50
 */
import message from 'antd/es/message';
import Modal from 'antd/es/modal';
import { isObject } from 'lodash';
import msngr from 'msngr';
import { call, put, select, take, takeLatest } from 'redux-saga/effects';

import * as services from './service';

let fetchPars: any = null;

export default (CONSTANTS, actions, routeId) => {
  function* getData(action) {
    try {
      fetchPars = action.payload;

      const data = yield call(services.getOrderList, fetchPars);

      yield put(actions.getOrderListDone(data));
    } catch (err) {
      yield put(actions.getOrderListError(err));
      message.error(err.msg);
    }
  }
  function* getOrderStatistics() {
    try {
      const data = yield call(services.getOrderStatistics, {});
      yield put(actions.getOrderStatisticsDone(data));
    } catch (err) {
      yield put(actions.getOrderStatisticsError(err));
    }
  }

  function* doImportOrders(action) {
    const { customer, file } = action.payload;
    const [customerCode, customerName] = customer.split(',');
    try {
      const fd = new FormData();
      fd.append('customerCode', customerCode);
      fd.append('customerName', customerName);
      fd.append('file', file[0].originFileObj);
      const result = yield call(services.importOrders, fd);
      let success;
      if (isObject(result.data)) {
        // 导入失败，需要修改
        success = false;
      } else {
        success = true;
        // 导入成功
        Modal.success({
          title: '导入成功',
          content: `导入${result.count}条订单，成功导入${result.count}条。`,
        });
        msngr('/ltlOrder', 'import success').emit(true);
      }
      yield put(actions.importOrdersDone(result, !success));
    } catch (err) {
      yield put(actions.importOrdersDone(err, true));
    }
  }

  function* doImportModifyOrders(action) {
    try {
      const importData = yield select(state =>
        state.getIn([routeId, 'import']),
      );
      const result = yield call(services.importModifyOrders, {
        customerCode: importData.get('customerCode'),
        customerName: importData.get('customerName'),
        fileId: importData.get('fileId'),
        total: importData.get('total'),
        rightCount: importData.get('rightCount'),
        errorCount: importData.get('errorCount'),
        isContinue: 0,
        orderDatas: importData.get('orderDatas').toJS(),
      });
      let success;
      if (isObject(result.data)) {
        // 导入失败，需要修改
        success = false;
      } else {
        success = true;
        // 导入成功
        Modal.success({
          title: '导入成功',
          content: `导入${result.count}条订单，成功导入${result.count}条。`,
        });
        msngr('/ltlOrder', 'import success').emit(true);
      }
      yield put(actions.importOrdersDone(result, !success));
    } catch (err) {
      yield put(actions.importOrdersDone(err, true));
    }
  }

  function* deleteOrders(action) {
    try {
      const result = yield call(services.deleteOrders, action.payload);

      yield put(actions.deleteOrdersDone());
      yield put(actions.getOrderStatistics());
      yield put(actions.getOrderListData(fetchPars));
    } catch (err) {
      yield put(actions.deleteOrdersError(err));
      message.error(err.msg);
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_LIST_DATA, getData);
    yield takeLatest(CONSTANTS.IMPORT_ORDERS, doImportOrders);
    yield takeLatest(CONSTANTS.IMPORT_MODIFY_ORDERS, doImportModifyOrders);
    yield takeLatest(CONSTANTS.GET_ORDER_STATISTICS, getOrderStatistics);
    yield takeLatest(CONSTANTS.DELETE_ORDERS, deleteOrders);
  };
};
