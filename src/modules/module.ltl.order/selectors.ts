/**
 * LtlOrder selectors
 * @author djd
 * @date 2018/9/13 下午1:48:50
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectOrderData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));

export const makeSelectOrderCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('count'));

export const makeSelectImportData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('import'));
export const makeSelectOrderStatistics = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('statistics'),
  );
