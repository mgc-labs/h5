/**
 * LtlOrder Reducers
 * @author djd
 * @date 2018/9/13 下午1:48:50
 */

import { fromJS, List, Map } from 'immutable';

// The initial state of the App
const initialState = Map({
  data: List(),
  count: 0,
  error: false,
  loading: false,
  import: Map({
    status: 'success',
    pending: false,
    orderDatas: List(),
    fileId: undefined,
    customerCode: undefined,
    customerName: undefined,
    total: 0,
    rightCount: 0,
    errorCount: 0,
  }),
});

export default CONSTANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSTANTS.GET_LIST_DATA:
        return state.set('error', false).set('loading', true);
      case CONSTANTS.GET_LIST_DATA_SUCCESS:
        const { count, data } = action.payload;

        return state
          .set('loading', false)
          .set('data', fromJS(data))
          .set('count', count);

      case CONSTANTS.GET_LIST_DATA_ERROR:
        return state.set('error', action.error).set('loading', false);
      case CONSTANTS.RECORD_CHECKED_CHANGED: {
        const record = action.payload;

        let list = state.get('data') as List<Map<string, any>>;
        list = list.map(item => {
          if (item.get('utmsPartOrderId') === record.utmsPartOrderId) {
            return item.set('isChecked', !record.isChecked);
          } else {
            return item;
          }
        });

        return state.set('data', list);
      }

      case CONSTANTS.RECORD_ALLCHECKED_CHANGED: {
        const checked = action.payload;

        let list = state.get('data') as List<Map<string, any>>;
        list = list.map(item => {
          return item.set('isChecked', checked);
        });
        return state.set('data', list);
      }

      case CONSTANTS.IMPORT_ORDERS:
        return state.setIn(['import', 'pending'], true);
      case CONSTANTS.IMPORT_ORDERS_DONE:
        const importErrorData = action.payload.data;
        return state.update('import', (map: Map<string, any>) => {
          const nextMap = map.set('pending', false);
          if (action.error) {
            if (importErrorData) {
              return nextMap
                .set('status', 'invalid')
                .merge({
                  customerCode: importErrorData.customerCode,
                  customerName: importErrorData.customerName,
                  fileId: importErrorData.fileId,
                  total: importErrorData.total,
                  rightCount: importErrorData.rightCount,
                  errorCount: importErrorData.errorCount,
                })
                .set('orderDatas', fromJS(importErrorData.orderDatas));
            }
          } else {
            return nextMap.set('status', 'valid');
          }
          return nextMap;
        });
      case CONSTANTS.EDIT_IMPORT_INVALID:
        const newData = action.payload;
        return state.updateIn(['import', 'orderDatas'], list => {
          const { index, error, ...others } = newData;
          let nextList = list;
          for (const key of error.keys()) {
            if (nextList.getIn([index, key]) !== newData[key]) {
              nextList = nextList.setIn([index, 'error', key], true);
            }
          }
          return nextList.update(index, v => v.merge(others));
        });
      case CONSTANTS.SET_IMPORT_STATUS:
        return state.setIn(['import', 'status'], action.payload);
      case CONSTANTS.GET_ORDER_STATISTICS:
        return state.set('error', false).set('loading', true);
      case CONSTANTS.GET_ORDER_STATISTICS_SUCCESS:
        return state
          .set('loading', false)
          .set('statistics', action.payload.data);
      case CONSTANTS.GET_ORDER_STATISTICS_ERROR:
        return state.set('error', action.error).set('loading', false);
      default:
        return state;
    }
  };
};
