import * as React from 'react';

import styles from './index.module.less';

import Button from 'antd/es/button';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';

import Form, { FormComponentProps } from 'antd/es/form';
import Input from 'antd/es/input';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import { Link } from 'react-router-dom';
import { Hash } from 'utils/Hash';

import { conditionItemLayout, conditionLayout } from 'utils/conditionLayout';
const formItemLayout = { ...conditionItemLayout, colon: false };
export interface IConditionProps extends FormComponentProps {
  onSearch: (pars?: Hash) => void;
}

class Condition extends React.PureComponent<IConditionProps, any> {
  layout = {
    xxl: { span: 6 },
    xl: { span: 6 },
    md: { span: 12 },
    sm: { span: 12 },
    xs: { span: 24 },
  };
  onSearch = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        const { createDate, ...other } = fieldsValue;
        let createDateStart = '';
        let createDateEnd = '';
        if (createDate && createDate.length > 0) {
          createDateStart = createDate[0].format('YYYY-MM-DD') + ' 00:00:00';
          createDateEnd = createDate[1].format('YYYY-MM-DD') + ' 23:59:59';
        }

        if (this.props.onSearch) {
          const param = {
            createDateStart,
            createDateEnd,
            ...other,
          };
          if (param.status || param.status === 0) {
            param.status = [param.status];
            if (param.status[0] === 2) {
              param.status.unshift(1);
            }
          }
          const keys = Object.keys(param);
          for (const key of keys) {
            if (!param[key]) {
              delete param[key];
            }
          }
          this.props.onSearch(param);
        }
      }
    });
  };

  onClear = () => {
    const { setFieldsValue, validateFields } = this.props.form;
    setFieldsValue({
      status: '',
      createDate: undefined,
      thirdSystemId: '',
      orderNumber: '',
      customerName: '',
      address: '',
    });
    if (this.props.onSearch) {
      this.props.onSearch();
    }
  };
  render() {
    const statusList = [
      { value: '', text: '全部' },
      { value: 0, text: '待配载' },
      // { value: 1, text: '待发货' },
      { value: 2, text: '待调度' },
      { value: 3, text: '配送中' },
      { value: 4, text: '已签收' },
      { value: 5, text: '未签收' },
    ];

    const { getFieldDecorator } = this.props.form;

    return (
      <div className={styles.condition}>
        <Form onSubmit={this.onSearch}>
          <Row gutter={10}>
            <Col {...this.layout}>
              <Form.Item
                {...formItemLayout}
                label="状态"
                style={{ marginBottom: '14px' }}
              >
                {getFieldDecorator('status', {
                  initialValue: '',
                })(
                  <Select placeholder="全部">
                    {statusList.map((item, index) => (
                      <Select.Option key={index} value={item.value}>
                        {item.text}
                      </Select.Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
            </Col>
            <Col {...this.layout}>
              <Form.Item
                {...formItemLayout}
                label="发布时间"
                style={{ marginBottom: '14px' }}
              >
                {getFieldDecorator('createDate', {
                  // initialValue: '',
                  rules: [{ type: 'array' }],
                })(<DatePicker.RangePicker style={{ width: '100%' }} />)}
              </Form.Item>
            </Col>
            <Col {...this.layout}>
              <Form.Item
                {...formItemLayout}
                label="第三方单号"
                style={{ marginBottom: '14px' }}
              >
                {getFieldDecorator('thirdSystemId', {
                  initialValue: '',
                })(<Input />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={10}>
            <Col {...this.layout}>
              <Form.Item {...formItemLayout} label="订单号">
                {getFieldDecorator('orderNumber', {
                  initialValue: '',
                })(<Input />)}
              </Form.Item>
            </Col>
            <Col {...this.layout}>
              <Form.Item {...formItemLayout} label="客户名称">
                {getFieldDecorator('customerName', {
                  initialValue: '',
                })(<Input />)}
              </Form.Item>
            </Col>
            <Col {...this.layout}>
              <Form.Item {...formItemLayout} label="收货地址">
                {getFieldDecorator('address', {
                  initialValue: '',
                })(<Input />)}
              </Form.Item>
            </Col>
            <Col {...this.layout}>
              <Form.Item {...formItemLayout}>
                <Button
                  type="primary"
                  htmlType="submit"
                  style={{ marginRight: '12px', marginLeft: '6px' }}
                >
                  查询
                </Button>
                <Button onClick={this.onClear}>重置</Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

export default Form.create()(Condition);
