import * as React from 'react';

import styles from './index.module.less';

import accounting from 'accounting';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Divider from 'antd/es/divider';
import Row from 'antd/es/row';
import Component from 'modules/module.company.account/Loadable';

export interface ITotalComponentProps {
  total?: number; // 总订单数量
  waitGroup?: number; // 待配置
  pendingScheduling?: number; // 待调度数量
  distribution?: number; // 运输中数量
  haveSignedIn?: number; // 已签收数量
  notSigned?: number; // 未签收数量
}

export class TotalComponent extends React.PureComponent<ITotalComponentProps> {
  layout = {
    lg: { span: 4 },
    md: { span: 8 },
    xs: { span: 24 },
  };
  state = {
    obj: {
      total: null,
      waitGroup: null,
      pendingScheduling: null,
      distribution: null,
      haveSignedIn: null,
      notSigned: null,
    },
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.total) {
      const obj = nextProps;
      this.setState({
        obj,
      });
    }
  }
  getTotalItem = ({
    title,
    value,
    border = false,
  }: {
    title: string;
    value?: number;
    border?: boolean;
  }) => {
    return (
      <div className={`${styles.item} ${border ? styles.border : ''}`}>
        <div className={styles.title}>{title}</div>
        <div className={styles.value}>
          {accounting.formatNumber(value, {
            // precision: 2,
            thousand: ',',
          })}
        </div>
      </div>
    );
  };
  render() {
    return (
      <div className={styles.total}>
        <Card bordered={false}>
          <Row gutter={16}>
            <Col {...this.layout} key={1}>
              {this.getTotalItem({
                title: '总订单',
                value: this.state.obj.total,
                border: true,
              })}
            </Col>
            <Col {...this.layout} key={2}>
              {this.getTotalItem({
                title: '待配载',
                value: this.state.obj.waitGroup,
                border: true,
              })}
            </Col>
            <Col {...this.layout} key={3}>
              {this.getTotalItem({
                title: '待调度',
                value: this.state.obj.pendingScheduling,
              })}
            </Col>
            <Col {...this.layout} key={4}>
              {this.getTotalItem({
                title: '配送中',
                value: this.state.obj.distribution,
                border: true,
              })}
            </Col>
            <Col {...this.layout} key={5}>
              {this.getTotalItem({
                title: '已签收',
                value: this.state.obj.haveSignedIn,
                border: true,
              })}
            </Col>
            <Col {...this.layout} key={6}>
              {this.getTotalItem({
                title: '未签收',
                value: this.state.obj.notSigned,
              })}
            </Col>
          </Row>
        </Card>
      </div>
    );
  }
}
