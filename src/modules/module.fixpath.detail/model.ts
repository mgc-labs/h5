import message from 'antd/es/message';
import { fromJS, List, Map } from 'immutable';
import { call, put, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { getData } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: Map(),
      error: false,
      loading: false,
    }),
    actions: {
      GET_DETAIL: routeId => routeId,
      GET_DETAIL_ERROR: error => error,
      GET_DETAIL_SUCCESS: data => data,
    },
    reducers: {
      GET_DETAIL(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_DETAIL_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_DETAIL_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data || {}));
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const data = yield call(getData, { routeId: action.payload });
          if (data.code) {
            message.error(data.msg);
          }
          yield put(actions.getDetailSuccess(data));
        } catch (err) {
          yield put(actions.getDetailError(err));
        }
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_DETAIL, fetchData);
      };
    },
  });
