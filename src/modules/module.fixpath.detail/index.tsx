/**
 * FixpathDetail Component
 * @author lhf
 * @date 2018-9-25 16:19:00
 */
import { List, Map } from 'immutable';
import * as React from 'react';
import { createStructuredSelector } from 'reselect';

import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Row from 'antd/es/row';
import {
  FormDetailCard,
  FormDetailField,
  formLayoutCols2,
  formLayoutCols3,
  gutter,
} from 'components/FormDetailCard';
import GLOBAL from 'utils/GLOBAL';
import styles from './index.module.less';
import {
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

export interface IFixpathDetailProps {
  children: React.ReactChildren;
  detail: Map<string, any>;
  searchParams: {
    id: number;
  };
  getDetail: (options?: number) => any;
}

class FixpathDetail extends React.PureComponent<IFixpathDetailProps> {
  public componentDidMount() {
    this.getDetail();
  }
  public render() {
    return (
      <div className={styles.FixpathDetail}>
        {this.renderBasicInfo()}
        {this.renderGoodsInfo()}
        {this.renderServiceInfo()}
        {this.renderCostInfo()}
      </div>
    );
  }

  private getDetail() {
    const { searchParams } = this.props;
    if (searchParams.id) {
      this.props.getDetail(searchParams.id);
    }
  }

  // 渲染装卸地
  private renderAddress(data) {
    const name =
      data.get('loadType') === 1 || data.get('loadType') === '1'
        ? '卸货地'
        : '装货地';
    return (
      <Row gutter={gutter} key={data.get('id')}>
        <Col {...formLayoutCols3}>
          <FormDetailField label={name} value={data.get('pointAddress')} />
        </Col>
        <Col {...formLayoutCols3}>
          <FormDetailField
            label="联系人"
            value={data.get('contact') || GLOBAL.emptyRecord}
          />
        </Col>
        <Col {...formLayoutCols3}>
          <FormDetailField
            label="联系电话"
            value={data.get('phone') || GLOBAL.emptyRecord}
          />
        </Col>
      </Row>
    );
  }

  // 线路信息
  private renderBasicInfo() {
    const { detail } = this.props;
    return (
      <Card title="线路信息" className={styles.FixpathDetail__CardInfo}>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="客户名称"
              value={detail.get('customerName')}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField label="线路名称" value={detail.get('routeName')} />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="所需车型"
              value={
                detail.get('carLong')
                  ? detail.get('carLong') / 1000 +
                    '米' +
                    detail.get('carStruct')
                  : detail.get('carStruct')
              }
            />
          </Col>
        </Row>
        {detail
          .get('routeWayPoints', List())
          .map(item => this.renderAddress(item))
          .toArray()}
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="预计运输距离"
              value={detail.get('navigationDistance') || GLOBAL.emptyRecord}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="预计运输时间"
              value={detail.get('navigationTimecost') || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </Card>
    );
  }

  // 货物信息
  private renderGoodsInfo() {
    const { detail } = this.props;
    return (
      <Card title="货物信息" className={styles.FixpathDetail__CardInfo}>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField label="货物名称" value={detail.get('goodsName')} />
          </Col>
        </Row>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="重量(千克)"
              value={detail.get('goodsWeight')}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="体积(方)"
              value={detail.get('goodsVolume')}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="件数(件)"
              value={detail.get('goodsNumber')}
            />
          </Col>
        </Row>
      </Card>
    );
  }

  // 增值服务信息
  private renderServiceInfo() {
    const { detail } = this.props;
    return (
      <Card title="增值服务" className={styles.FixpathDetail__CardInfo}>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="回单"
              value={detail.get('isReceipt') ? '是' : '否'}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="代收(元)"
              value={detail.get('collictionAmount') || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </Card>
    );
  }
  // 货物信息
  private renderCostInfo() {
    const { detail } = this.props;
    return (
      <Card title="运费信息" className={styles.FixpathDetail__CardInfo}>
        <Row gutter={gutter}>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="客户运费(元)"
              value={detail.get('customerFreight') || GLOBAL.emptyRecord}
            />
          </Col>
          <Col {...formLayoutCols3}>
            <FormDetailField
              label="司机运费(元)"
              value={detail.get('driverFreight') || GLOBAL.emptyRecord}
            />
          </Col>
        </Row>
      </Card>
    );
  }
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        detail: makeSelectData(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getDetail: option => dispatch(actions.getDetail(option)),
      }),
    };
  })(FixpathDetail);
};
