/**
 * DetailPage Component
 * @author yanwei
 * @date 2018-9-10 14:56:11
 */
import classnames from 'classnames';
import * as React from 'react';

import Button from 'antd/es/button';
import Select from 'antd/es/select';

import Col from 'antd/es/col';
import Row from 'antd/es/row';
import Switch from 'antd/es/switch';

const Option = Select.Option;
import styles from '../../index.module.less';

export interface IDetailPageProps {
  data: any;
  lbsPoints: any;
  onChange: (value?: string, field?: string) => any;
  handleChangeSwitch: (checked?: boolean) => any;
  handlePause: () => any;
  handlePlayback: () => any;
}
interface IState {
  mode: string;
  loading: boolean;
  filter: boolean;
  speed: number;
  topCollapse: boolean;
  bottomCollapse: boolean;
}

class DetailPage extends React.PureComponent<IDetailPageProps> {
  state: IState;
  // static defaultProps = {};
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      mode: '', // 坐标类型
      filter: true, // 是否优化轨迹
      speed: 3500, // 路书速度
      topCollapse: true, // 收起
      bottomCollapse: true,
    };
  }
  toggleTopCollapse = () => {
    this.setState({
      topCollapse: !this.state.topCollapse,
    });
  };
  toggleBottomCollapse = () => {
    this.setState({
      bottomCollapse: !this.state.bottomCollapse,
    });
  };
  handleChangeFilter = value => {
    this.props.onChange(value, 'filter');
  };
  handleChangeMode = value => {
    this.props.onChange(value, 'mode');
  };
  handleChangeSpeed = value => {
    this.props.onChange(value, 'speed');
  };
  renderWayList = () => {
    const list = [];
    const detail = this.props.data || {};
    const data = detail.utmsTradeWayPoints || [];
    const len = data.length;
    if (len === 0) {
      return;
    }
    for (let i = len - 1; i >= 0; i--) {
      const li =
        data[i].reachAddress || (data[i].reachLat && data[i].reachLng) ? (
          <li
            key={i}
            className={data[i].loadType === 0 ? styles.step4 : styles.step5}
          >
            <p>{data[i].reachAddress || '----'}</p>
            <p>{data[i].reachDate}</p>
          </li>
        ) : null;
      list.push(li);
    }
    return list;
  };
  renderWaybillInfo = () => {
    const { data } = this.props;
    const { topCollapse } = this.state;
    let statusTXT = '';
    if (data.status) {
      if (data.status === 0) {
        statusTXT = '待调度';
      } else if (data.status === 1) {
        statusTXT = '待接单';
      } else if (data.status === 2) {
        statusTXT = '待签到';
      } else if (data.status === 3) {
        statusTXT = '待装货';
      } else if (data.status === 4) {
        statusTXT = '待卸货';
      } else if (data.status === 5) {
        statusTXT = '已完成';
      } else if (data.status === 6) {
        statusTXT = '已取消';
      } else if (data.status === 7) {
        statusTXT = '待改派';
      }
    }

    return (
      <div className={styles.waybillSidebar__orderInfo}>
        <h3>
          订单信息
          <small>{statusTXT}</small>
        </h3>
        <h4>
          <span>运单号: </span>
          {data.tradeNumber}
        </h4>
        <p
          className={classnames({
            [styles.waybillSidebar__toggle]: true,
            [styles.waybillSidebar__down]: !topCollapse,
          })}
          onClick={() => {
            this.toggleTopCollapse();
          }}
        >
          {topCollapse ? '展开' : '收起'}
          <small />
        </p>
      </div>
    );
  };
  renderUserInfo = () => {
    const { data } = this.props;
    return (
      <div className={styles.waybillSidebar__userInfo}>
        <div className={styles.waybillSidebar__userInfoL}>
          <img
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAAXNSR0IArs4c6QAAAXpQTFRFAAAA////4+Pj////5ubm////8PDw////4+Pj7Ozs9vb24+Pj7Ozs8fHx5eXl6Ojo8vLy4+Pj7Ozs7u7u8fHx5OTk6urq7Ozs7+/v8vLy5OTk6+vr8PDw8/Pz4uLi6urq7+/v8fHx8/Pz4uLi6+vr8/Pz4+Pj7+/v8PDw8vLy4+Pj7u7u8fHx4uLi8PDw8fHx4+Pj8PDw8fHx4uLi8vLy4+Pj8fHx8vLy4+Pj8PDw8fHx4+Pj8fHx4uLi8vLy4uLi8vLy4uLi4uPi4+Pj5OTk5OXk5eXl5ubm5+fn6Ojo6enp6erp6urq6+vr6+zr7Ozs7O3r7e3t7e7s7u7t7u7u7u/t7+/u7+/v7/Du8PDw8fHx8fLw8vLx8vLy8vPx8/Pz8/Ty9PTz9PT09PXz9fb09vb19vb29/f39/j1+Pj2+Pj3+Pj4+fn4+vr4+vr5+vr6+/v5+/v7/Pz6/Pz8/P36/f37/f38/f77/v78/v7+/v/8///9///+////IZRGWAAAAEF0Uk5TAAMJCQoKEREbGxs3NzdOTk5cXFxcYGBgYGBmZmZmfHx8fHyPj4+vr6+vt7e3y8vL29vb5ubs7Ozy8vL19ff3+fnGGasbAAACnElEQVRIx4VW+0MSQRBeBCnzEZRWmmhhWBiGAYr5QJajQwiyMwpYNItM7UFRaOAa/3sHB3czezy+H3e+b2dvd+abI0SE1eHxBcLRaDjg8zisZADs97xbDGDLO32tD3344QYzYcM93INumQ2xrgi5LN34I37WE/4bZv4Y2L6gZDLpZDKlFPQk4yL/dkSnZxO0AyndWYxMYv4tY/sMhUjo60gxZuyvUIykngOcagScPy4IaFb/Dv3LLc8M/p568B4pmL9zu/fBBe5SUwr9qphL49vge0mSlH8Va1Nf5o/TknEmFtLefB7wC1TOc147K757s18845yfU5oxwu5Wvb0AghylHzmCZLyFWlfNSrwDayBL6QcsiMkpEJ9WBY8ZzlDEAiqDIzGv2i+o/tlbCQtqNPsJ9oeV3ET8Cn+NBeexGv8GCA7yCAnq/PgzPtI+5xVA8JCnSHDBRdQ4/wEIPvIcCUp1k4JfHAJCgKzh3iqV/2D615+Qz8IkKrbje8SvZHE0ahYoKMUBEwVrpo4/APyyGAwLH91CWefXj8RYQLjWFg71y/1iivmEh9Pw/apFv/prDnmE0tBw2vjXRKNqDjnE4tMyNDSYBWrxkSd4Kbd3Ur1sCy6rv5VsgeHyJneBQ6YTqmWUGgaqamfLKQU1kF2z+FxS7hgREBy1LXM3ldNblDxo8Q03igNBEhuaWxsjTZtJAyNiOv8EG1rbZsisKkhC6zpt83/FsS27OlbpbzoeMrsWv4RMMG5YZdOME9gepbSSkYWlEBhD4xGZDsI2GkKT8YECJx5Bzu0B+zvFITca7McPjnYZu8u9+cvXuw72mR5JgjOWHv8Ctrl1M319ztbv52RqcROyNxen7IN+aIYmFpZWVnd2VleWFiaGTOH/F+9koN3+ab4AAAAASUVORK5CYII="
            alt=""
          />
        </div>
        <div className={styles.waybillSidebar__userInfoR}>
          <h4>
            <span className={styles.waybillSidebar__userInfoName}>
              {data.driverName}
            </span>
            {data.driverMobilenumber}
          </h4>
          <h4>
            {data.driverCarStruct}
            <span className={styles.waybillSidebar__userInfoLine} />
            {data.driverCarplatenumber}
          </h4>
        </div>
      </div>
    );
  };
  renderTrack = () => {
    const { data, lbsPoints } = this.props;
    return (
      <div className={styles.waybillSidebar__orderBox}>
        <h3>在途跟踪</h3>
        <p>
          <span>实际运输距离: </span>
          {data.actualDistance}
        </p>
        <p>
          <span>轨迹点数: </span>
          {lbsPoints}
        </p>
        <ul>
          {data.finishDate !== null && data.finishDate ? (
            <li className={styles.step6}>
              <p>
                <span>订单完成时间：</span>
                {data.finishDate}
              </p>
            </li>
          ) : null}

          {this.renderWayList()}

          {data.signDate !== null ? (
            <li className={styles.step3}>
              <p>
                <span>司机签到时间：</span>
                {data.signDate}
              </p>
            </li>
          ) : null}

          {data.receiveDate !== null ? (
            <li className={styles.step2}>
              <p>
                <span>司机接单时间：</span>
                {data.receiveDate}
              </p>
            </li>
          ) : null}

          <li className={styles.step1}>
            <p>
              <span>订单发布时间：</span>
              {data.createDate}
            </p>
          </li>
        </ul>
      </div>
    );
  };
  handleChangeSwitch = checked => {
    this.setState({
      loading: true,
    });
    this.props.handleChangeSwitch(checked);
    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 500);
  };
  renderPathPlanning = () => {
    const { loading } = this.state;
    const { data } = this.props;
    const utmsTradeWayPoints = data.utmsTradeWayPoints || [];
    let tjnum = 0;

    return (
      <div className={styles.waybillSidebar__orderBox}>
        <h3>
          订单路径规划
          {utmsTradeWayPoints.length ? (
            <div className={styles.waybillSidebar__Switch}>
              <Switch
                loading={loading}
                checkedChildren="开"
                unCheckedChildren="关"
                defaultChecked
                onChange={this.handleChangeSwitch}
              />
            </div>
          ) : null}
        </h3>
        <p>
          <span>预计运输距离: </span>
          {data.preDistance || '--'}
          千米
        </p>
        <p>
          <span>装卸点: </span>
          {data.wayPointNum || '--'}个
        </p>
        {utmsTradeWayPoints.map((d, i) => {
          if (d.loadType === 1) {
            tjnum++;
          }
          return (
            <p key={i}>
              <span>
                {d.loadType === 0
                  ? '装货地址:'
                  : i === utmsTradeWayPoints.length - 1
                    ? '卸货地址:'
                    : `途经点${tjnum}:`}
              </span>
              {d.address}
            </p>
          );
        })}
      </div>
    );
  };
  renderPlayback = () => {
    const { bottomCollapse } = this.state;
    return (
      <div className={styles.waybillSidebar__bottomBox}>
        <p
          className={classnames({
            [styles.waybillSidebar__toggle]: true,
            [styles.waybillSidebar__down]: !bottomCollapse,
          })}
          onClick={() => {
            this.toggleBottomCollapse();
          }}
        >
          {bottomCollapse ? '展开' : '收起'}
          <small />
        </p>
        {!bottomCollapse ? (
          <div className={styles.waybillSidebar__orderBox}>
            <h3>播放轨迹</h3>
            <div>
              <Row>
                <Col span={6}>轨迹类型:</Col>
                <Col span={18}>
                  <Select
                    style={{ width: '100%' }}
                    defaultValue="true"
                    onChange={this.handleChangeFilter}
                  >
                    <Option value="true">优化轨迹</Option>
                    <Option value="false">原始轨迹</Option>
                  </Select>
                </Col>
              </Row>
              <Row>
                <Col span={6}>坐标类型:</Col>
                <Col span={18}>
                  <Select
                    style={{ width: '100%' }}
                    defaultValue=""
                    onChange={this.handleChangeMode}
                  >
                    <Option value="">全部</Option>
                    <Option value="GPS">GPS</Option>
                    <Option value="GSM">基站</Option>
                    <Option value="WIFI">Wi-Fi</Option>
                  </Select>
                </Col>
              </Row>
              <Row>
                <Col span={6}>速度:</Col>
                <Col span={18}>
                  <Select
                    style={{ width: '100%' }}
                    defaultValue="3500"
                    onChange={this.handleChangeSpeed}
                  >
                    <Option value="3500">非常快(3500m/s)</Option>
                    <Option value="3000">很快(3000m/s)</Option>
                    <Option value="2500">比较快(2500m/s)</Option>
                    <Option value="2000">一般(2000m/s)</Option>
                    <Option value="1500">慢速(1500m/s)</Option>
                    <Option value="1000">很慢(1000m/s)</Option>
                    <Option value="500">非常慢(500m/s)</Option>
                  </Select>
                </Col>
              </Row>
              <Row gutter={10}>
                <Col span={12}>
                  <Button onClick={this.props.handlePause}>暂停</Button>
                </Col>
                <Col span={12}>
                  <Button type="primary" onClick={this.props.handlePlayback}>
                    播放轨迹
                  </Button>
                </Col>
              </Row>
            </div>
          </div>
        ) : null}
      </div>
    );
  };

  public render() {
    const { topCollapse } = this.state;
    return (
      <div className={styles.waybillSidebar}>
        <h2>订单跟踪</h2>
        <div className={styles.waybillSidebar__sidebar}>
          <div className={styles.waybillSidebar__content}>
            {/* 用户信息 */}
            {this.renderUserInfo()}
            {/* 订单信息 */}
            {this.renderWaybillInfo()}
            {/* 订单路径 */}
            {!topCollapse ? this.renderPathPlanning() : null}
            {/* 在途跟踪 */}
            {this.renderTrack()}
            {/* 播放轨迹 */}
            {this.renderPlayback()}
          </div>
        </div>
      </div>
    );
  }
}

export default DetailPage;
