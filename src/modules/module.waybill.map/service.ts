/**
 * waybill.list Service
 * @author yanw
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const TRADE_TRACK_RESOURCE =
  '/ehuodiGateway/utmsTrade/utmsTrade/selectTradeTrack';

const LBS_TRACK_RESOURCE =
  '/ehuodiGateway/lbsApi/lbstrackcs/selectLbsTrackByDeviceIdForKa';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', TRADE_TRACK_RESOURCE)
    .reply(() =>
      import('./mock/tradetrack').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', LBS_TRACK_RESOURCE)
    .reply(() =>
      import('./mock/forka').then(exports => [200, exports.default]),
    );
}

/**
 * 获取资源信息
 */
export const tradeTrackService = data => {
  return request(
    {
      method: 'post',
      url: TRADE_TRACK_RESOURCE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

export const lbsTrackService = data => {
  return request(
    {
      method: 'post',
      url: LBS_TRACK_RESOURCE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
