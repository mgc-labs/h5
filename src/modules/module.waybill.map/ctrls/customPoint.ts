// import {
//   ColorUnGroupdSelectedPoint,
//   ColorUnGroupdUnSelectedPoint,
// } from '../const';

const pointWidth = 34;
const pointHeight = 42;

// const pointInWidth = 60;
// const pointInHeight = pointInWidth;

declare const BMap: any;

export class CustomPoint extends BMap.Overlay {
  public data: any;
  constructor({ type, data, onClick, onShowInfo, onHideInfo }) {
    super();
    this.type = type;
    this.data = data;
    this.point = new BMap.Point(data.longitude, data.latitude);
    this.onClick = onClick;
    this.onShowInfo = onShowInfo;
    this.onHideInfo = onHideInfo;
  }

  initialize(map) {
    this.map = map;
    const div = document.createElement('div');
    div.style.position = 'absolute';

    div.style.width = '34px';
    div.style.height = '42px';
    div.style.cursor = 'pointer';
    // loadType:0 装，1卸、2签到、3当时位置
    if (this.type === 0) {
      div.style.zIndex = '3';
      div.innerHTML =
        "<img src='https://image.tf56.com/dfs/group1/M00/66/B4/CiFBCVujVEyARfifAAAHS3KgShs905.png'>";
    } else if (this.type === 1) {
      div.style.zIndex = '1';
      div.innerHTML =
        "<img src='https://image.tf56.com/dfs/group1/M00/66/B4/CiFBCVujVDKAaRbkAAAGsTheId8483.png'>";
    } else if (this.type === 2) {
      div.style.zIndex = '1';
      div.innerHTML =
        "<img src='https://image.tf56.com/dfs/group1/M00/66/B4/CiFBClujVFmAa-yDAAAG7-raXJk566.png'>";
    } else if (this.type === 3) {
      div.style.zIndex = '1';
      div.innerHTML =
        "<img src='https://image.tf56.com/dfs/group1/M00/66/B4/CiFBCVujU-KAN73xAAAH4eb8avA311.png'>";
    }
    // div.appendChild(img);

    div.onclick = e => {
      if (this.onClick) {
        this.onClick(this);
      }
    };

    div.onmouseenter = () => {
      if (this.onShowInfo) {
        this.onShowInfo(this.data);
      }
    };

    div.onmouseout = () => {
      if (this.onHideInfo) {
        this.onHideInfo(this.data);
      }
    };

    this.map.getPanes().labelPane.appendChild(div);
    this.div = div;

    return div;
  }

  draw() {
    const pixel = this.map.pointToOverlayPixel(this.point);
    this.div.style.left = pixel.x - pointWidth / 2 + 'px';
    this.div.style.top = pixel.y - pointHeight / 2 + 'px';
  }

  destroy() {
    // this.map.getPanes().labelPane.remove(this.div);
    this.div.onclick = null;
    this.type = null;
    this.data = null;
    this.point = null;
    this.onClick = null;
    this.onShowInfo = null;
    this.onHideInfo = null;

    this.map = null;
  }
}
