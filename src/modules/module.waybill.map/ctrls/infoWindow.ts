import styles from './infoWindow.module.less';

declare const BMap: any;

export class InfoWindow extends BMap.Overlay {
  constructor() {
    super();
    this.width = 280;
    this.height = 114;
  }

  initialize(map) {
    this.map = map;

    this.div = document.createElement('div');
    this.div.style.width = `${this.width}px`;
    this.div.style.height = `${this.height}px`;
    this.div.className = styles.divInfoWindow;

    this.divCurrow = document.createElement('div');
    this.divCurrow.className = styles.curr;

    this.div.appendChild(this.divCurrow);

    this.content = document.createElement('div');
    this.content.className = styles.divf;
    this.div.appendChild(this.content);

    this.map.getPanes().labelPane.appendChild(this.div);

    return this.div;
  }

  draw() {
    const pixel = this.map.pointToOverlayPixel(this.point);
    // this.div.style.left = pixel.x - this.width / 2 + 'px';
    // this.div.style.top = pixel.y - this.height - 40 + 'px';
    // const height = this.div.offsetHeight;

    // 框位置
    let fx = pixel.x - this.width / 2;
    let fy = pixel.y - this.height - 31;

    // 箭头位置
    let cx = this.width / 2 - 11;
    let cy = this.height - 5;

    // 下面的做法，没有找到官方的做法，只能自己写
    const posDiv = this.div.parentElement.parentElement.parentElement;
    if (posDiv) {
      const container = this.map.getContainer();
      const mWidth = container.offsetWidth;
      const mHeight = container.offsetHeight;

      const mx = fx + posDiv.offsetLeft;
      const mt = fy + posDiv.offsetTop;

      if (mx < 0) {
        // 如果左边距超出
        fx = pixel.x - this.width / 2 - mx;
        cx = cx + mx + 0;
      } else if (mx + this.width > mWidth) {
        // 如果右边距超出
        fx = pixel.x - this.width / 2 - (this.width + mx - mWidth) - 0;
        cx = cx + (this.width + mx - mWidth) + 3 + 1;
      }
      if (mt - 90 < 0) {
        fy = pixel.y + 26;
        cy = -12 + 2;
        this.divCurrow.style.transform = 'rotate(45deg)';
      } else {
        cy -= 7;
        this.divCurrow.style.transform = 'rotate(-45deg)';
      }
    }

    this.div.style.left = fx + 'px';
    this.div.style.top = fy + 'px';

    this.divCurrow.style.left = cx + 'px';
    this.divCurrow.style.top = cy + 'px';
  }

  setSize() {
    this.div.style.width = this.width + 'px';
    this.div.style.height = this.height + 'px';
  }

  show() {
    super.show();
    this.draw();
    // this.setSize();
  }

  setPosition(point) {
    this.point = point;
  }

  showValue(value) {
    if (value === null || value === undefined || value === '') {
      return '';
    }
    return value;
  }

  setData(data) {
    let modetxt;
    if (data.mode === '1') {
      modetxt = 'GPS';
    } else if (data.mode === '2') {
      modetxt = '基站';
    } else if (data.mode === '3') {
      modetxt = 'WIFI';
    }
    const time = data.realtime.slice(0, 19);

    this.content.innerHTML =
      '<table><tr><th>当前位置：</th><td>' +
      this.showValue(data.useextendcode) +
      '</td></tr><tr><th>定位时间：</th><td>' +
      this.showValue(time) +
      '</td></tr><tr><th>速度：</th><td>' +
      this.showValue(data.speed) +
      'km/h</td></tr><tr><th>定位方式：</th><td>' +
      this.showValue(modetxt) +
      '</td></tr></table>';
  }
}
