/**
 * WaybillMap Component
 * @author yanwei
 * @date 2018-9-13 14:23:17
 */

import moment from 'moment';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import shallowequal from 'shallowequal';
import { CustomPoint } from './ctrls/customPoint';
import { InfoWindow } from './ctrls/infoWindow';

import message from 'antd/es/message';
import Spin from 'antd/es/spin';

import {
  makeSelectData,
  makeSelectError,
  makeSelectlbsPoints,
  makeSelectlbsSource,
  makeSelectLoading,
} from './selectors';

import styles from './index.module.less';

import DetailPage from './components/DetailPage/index';

declare const BMap: any;
declare const BMapLib: any;

export interface IWaybillMapProps {
  data: any;
  lbsSource: any;
  lbsPoints: any;
  children: React.ReactChildren;
  searchParams: {
    tradeNumber: string;
  };
  selectTradeTrack: (params?: object) => any;
  selectLbsTrack: (params?: object) => any;
}
interface IState {
  loading: boolean;
  data: object;
  lbsSource: object;
  // lbsPoints: number;
  checked: boolean;
  mode: string;
  filter: boolean;
  speed: number;
  lbsMarkers: object;
}
class WaybillMap extends React.PureComponent<IWaybillMapProps> {
  state: IState;
  private divMap: HTMLDivElement;
  private infoWindow: InfoWindow;
  private map: any;
  private lushu: object;
  private lbsPolyline: any;
  private locationCP: any;
  private driving: any;
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: {},
      lbsSource: [],
      checked: false,
      mode: '', // 坐标类型
      filter: true, // 是否优化轨迹
      speed: 3500, // 路书速度
      lbsMarkers: [], // 实际行驶轨迹点
    };
  }
  public componentDidMount() {
    this.initMap();
    const { selectTradeTrack, searchParams } = this.props;
    selectTradeTrack(
      Object.assign(searchParams, {
        coordinateType: 'BD09',
      }),
    );
  }
  initMap() {
    // 创建Map实例
    this.map = new BMap.Map(this.divMap, {
      minZoom: 1,
      maxZoom: 19,
      enableMapClick: false,
    });

    this.map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
    // 去掉地图左下解的LOGO与文字
    this.map.addEventListener('tilesloaded', () => {
      this.removeMapElement(document.querySelector('.anchorBL'));
      this.removeMapElement(document.querySelector('.BMap_cpyCtrl'));
    });

    this.map.centerAndZoom('杭州', 12);
  }
  componentWillReceiveProps(nextProps) {
    // 移除旧的轨迹
    // if (
    //   shallowequal(this.state.data, nextProps.data) ||
    //   this.state.lbsSource === nextProps.lbsSource
    // ) {
    //   return;
    // }

    if (!shallowequal(this.state.data, nextProps.data)) {
      this.setState(
        {
          data: nextProps.data || {},
        },
        () => {
          // 规划装卸签到点
          this.initPlanMarkers();
          // 实际装卸签到点
          this.initReachMarkers();
        },
      );
    }
    if (this.state.lbsSource !== nextProps.lbsSource) {
      this.setState(
        {
          lbsSource: nextProps.lbsSource || [],
          loading: true,
        },
        () => {
          if (this.map && this.locationCP) {
            this.map.removeOverlay(this.locationCP);
          }
          if (this.map && this.lbsPolyline) {
            this.map.removeOverlay(this.lbsPolyline);
          }
          if (this.lushu && this.lushu._marker) {
            // this.map.removeOverlay(this.lushu);
            this.map.removeOverlay(this.lushu._marker);
          }
          setTimeout(() => {
            // // 实际当前位置点
            this.locationMarker();
            // // 实际行驶轨迹点
            this.initLbsPolyline();
          }, 100);
          setTimeout(() => {
            this.setState({
              loading: false,
            });
          }, 200);
        },
      );
    }
  }
  removeMapElement = (element: Element) => {
    if (element) {
      element.parentElement.removeChild(element);
    }
  };
  /**
   * 请求lbs轨迹点
   */
  selectLbsTrack() {
    const { mode, filter } = this.state;
    const { data } = this.props;
    const list = data.utmsTradeWayPoints;
    const params = {
      partyId: data.driverId || '', // 手机号
      mode,
      filter,
      returnFilterData: true,
      datestart: data.signDate
        ? data.signDate
        : moment().format('YYYY-MM-DD HH:mm:ss'),
      dateend:
        list.length && list[list.length - 1].reachDate
          ? list[list.length - 1].reachDate
          : moment().format('YYYY-MM-DD HH:mm:ss'),
      format: 'BD09',
      collectType: 2,
    };
    this.props.selectLbsTrack(params);
  }
  /**
   * 表单
   */
  handleInputChange = (value, field) => {
    if (field === 'filter') {
      this.setState(
        {
          filter: value,
        },
        () => {
          this.selectLbsTrack();
        },
      );
    } else if (field === 'mode') {
      this.setState(
        {
          mode: value,
        },
        () => {
          this.selectLbsTrack();
        },
      );
    } else if (field === 'speed') {
      this.setState({
        speed: value,
      });
    }
  };

  /**
   * 开关
   */
  handleChangeSwitch = checked => {
    this.setState({
      loading: true,
    });

    if (!checked) {
      this.map.clearOverlays();
    } else {
      // 规划装卸签到点
      this.initPlanMarkers();
    }
    setTimeout(() => {
      // 实际装卸签到点
      this.initReachMarkers();
      // 实际当前位置点
      this.locationMarker();
      // 实际行驶轨迹点
      this.initLbsPolyline();
    }, 300);

    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 500);
  };

  /**
   * 暂停
   */
  handlePause = () => {
    // if (!Array.prototype.isPrototypeOf(this.lushu)) {
    //   return;
    // }
    if (!this.lushu._opts) {
      message.error('没有轨迹');
      return;
    }
    this.lushu.stop();
  };
  /**
   * 播放轨迹
   */
  handlePlayback = () => {
    if (!this.lushu._opts) {
      message.error('没有轨迹');
      return;
    }
    const { speed, lbsMarkers } = this.state;
    const pts = lbsMarkers;
    this.map.setViewport(pts, {
      margins: [0, 300, 0, 0],
    });
    this.lushu._opts.speed = speed;
    this.lushu.stop();
    this.lushu.start();
  };

  /**
   * 实际的装卸点
   */
  initReachMarkers() {
    const { data } = this.props;
    // 实际签到的点
    const lng = data.signLongitude;
    const lat = data.signLatitude;
    if (lng && lat) {
      const cp = new CustomPoint({
        type: 2,
        data: { longitude: lng, latitude: lat },
        onClick: null,
        onShowInfo: null,
        onHideInfo: null,
      });
      this.map.addOverlay(cp);
    }

    // 实际装卸的点
    if (!data.utmsTradeWayPoints || data.utmsTradeWayPoints.length === 0) {
      return;
    }
    data.utmsTradeWayPoints.map(d => {
      const cp = new CustomPoint({
        type: d.loadType === 1 ? 1 : 0,
        data: {
          longitude: d.reachLng,
          latitude: d.reachLat,
        },
        onClick: null,
        onShowInfo: null,
        onHideInfo: null,
      });
      this.map.addOverlay(cp);
    });
  }
  /**
   * 创建标注
   */
  initMarker(options) {
    const { lng, lat, url } = options;
    const pt = new BMap.Point(lng, lat);
    const icon = new BMap.Icon(url, new BMap.Size(34, 42));
    const marker = new BMap.Marker(pt, { icon }); // 创建标注
    return marker;
  }
  /**
   * 规划的装卸点驾车路径
   */
  initPlanMarkers() {
    const { data } = this.props;
    const list = data.utmsTradeWayPoints;
    if (!list) {
      return;
    }
    let p1;
    // let p2;
    const p3 = [];
    let p = 0;
    list.map((d, i) => {
      if (i === 0) {
        p1 = new BMap.Point(d.longitude, d.latitude);
      } else {
        if (d.longitude && d.latitude) {
          const pt = new BMap.Point(d.longitude, d.latitude);
          p3[p] = pt;
          p++;
        }
      }
    });
    const p2 = p3[p3.length - 1];
    p3.pop();
    this.driving = new BMap.DrivingRoute(this.map, {
      renderOptions: { map: this.map, autoViewport: true },
      policy: 'BMAP_DRIVING_POLICY_LEAST_DISTANCE',
    });
    this.driving.search(p1, p2, { waypoints: p3 }); // waypoints表示途经点
  }
  onShowInfo = data => {
    if (!this.infoWindow) {
      this.infoWindow = new InfoWindow();
      this.infoWindow.setPosition(
        new BMap.Point(data.longitude, data.latitude),
      );
      this.map.addOverlay(this.infoWindow);
    } else {
      this.infoWindow.setPosition(
        new BMap.Point(data.longitude, data.latitude),
      );
    }
    this.infoWindow.setData(data);
    this.infoWindow.show();
  };
  onHideInfo = () => {
    this.infoWindow.hide();
  };
  onCPClick = (cpoint: CustomPoint) => {};
  locationMarker() {
    // loadType:0 装，1卸、2签到、3当时位置
    const { lbsSource } = this.props;
    const l = lbsSource.length - 1;
    const data = lbsSource[l];
    if (lbsSource.length) {
      this.locationCP = new CustomPoint({
        type: 3,
        data,
        onClick: this.onCPClick,
        onShowInfo: this.onShowInfo,
        onHideInfo: this.onHideInfo,
      });
      this.map.addOverlay(this.locationCP);
    }
  }
  /**
   * 实际的驾车路径
   */
  initLbsPolyline() {
    const { lbsSource } = this.props;

    this.lushu = new Object();

    if (lbsSource.length === 0) {
      return;
    }
    const pts = [];
    lbsSource.map((d, i) => {
      // const p = new BMap.Point(d.longitude, d.latitude);
      pts[i] = new BMap.Point(d.longitude, d.latitude);
    });
    this.lbsPolyline = new BMap.Polyline(pts, {
      strokeColor: '#FF8000',
      strokeWeight: 3,
      strokeOpacity: 1,
    }); // 创建折线
    this.map.addOverlay(this.lbsPolyline); // 增加折线
    this.setState({
      lbsMarkers: pts,
    });

    // this.map.setViewport(pts);
    this.lushu = new BMapLib.LuShu(this.map, pts, {
      defaultContent: '',
      autoView: true,
      icon: new BMap.Icon(
        'http://lbsyun.baidu.com/jsdemo/img/car.png',
        new BMap.Size(52, 26),
        { anchor: new BMap.Size(27, 13) },
      ),
      speed: this.state.speed,
      enableRotation: true,
      landmarkPois: [],
    });
  }

  public render() {
    const { data, lbsPoints } = this.props;
    return (
      <div className={styles.WaybillMap}>
        <Spin spinning={this.state.loading}>
          <div
            className={styles.WaybillMap__box}
            ref={node => (this.divMap = node)}
          />
        </Spin>
        <DetailPage
          data={data}
          lbsPoints={lbsPoints}
          onChange={(value, field) => {
            this.handleInputChange(value, field);
          }}
          handlePause={() => {
            this.handlePause();
          }}
          handlePlayback={() => {
            this.handlePlayback();
          }}
          handleChangeSwitch={checked => {
            this.handleChangeSwitch(checked);
          }}
          // checked={this.state.checked}
        />
      </div>
    );
  }
}
// const WaybillMap = Form.create()(WaybillMap);

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    selectTradeTrack: params => dispatch(actions.selectTradeTrack(params)),
    selectLbsTrack: params => dispatch(actions.selectLbsTrack(params)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    data: makeSelectData(selectState),
    lbsSource: makeSelectlbsSource(selectState),
    lbsPoints: makeSelectlbsPoints(selectState),
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(WaybillMap);
};
