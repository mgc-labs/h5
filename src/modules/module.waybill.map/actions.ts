/**
 * WaybillMap Actions
 * @author yanwei
 * @date 2018-9-13 14:23:17
 */
import { AnyAction } from 'redux';

export default CONSANTS => ({
  selectTradeTrack: (params): AnyAction => ({
    payload: params,
    type: CONSANTS.SELECT_TRADE_TRACK,
  }),

  selectTradeTrackDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.SELECT_TRADE_TRACK_SUCCESS,
  }),

  selectTradeTrackError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSANTS.SELECT_TRADE_TRACK_ERROR,
  }),

  selectLbsTrack: (params): AnyAction => ({
    payload: params,
    type: CONSANTS.SELECT_LBS_TRACK,
  }),

  selectLbsTrackDone: (lbsSource): AnyAction => ({
    payload: lbsSource,
    type: CONSANTS.SELECT_LBS_TRACK_SUCCESS,
  }),

  selectLbsTrackError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSANTS.SELECT_LBS_TRACK_ERROR,
  }),
});
