/**
 * WaybillMap Reducers
 * @author yanwei
 * @date 2018-9-13 14:23:17
 */

import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  data: {},
  lbsSource: [], // 轨迹点
  lbsPoints: 0, // 轨迹点个数
  location: {}, // 当前位置
  error: false,
  loading: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.SELECT_TRADE_TRACK:
        return state.set('error', false).set('loading', true);
      case CONSANTS.SELECT_TRADE_TRACK_SUCCESS:
        return state.set('loading', false).set('data', action.payload.data);
      case CONSANTS.SELECT_TRADE_TRACK_ERROR:
        return state
          .set('error', action.error)
          .set('loading', false)
          .set('lbsSource', []);
      case CONSANTS.SELECT_LBS_TRACK:
        return state.set('error', false).set('loading', true);
      case CONSANTS.SELECT_LBS_TRACK_SUCCESS:
        return state
          .set('loading', false)
          .set('lbsSource', action.payload.data)
          .set('lbsPoints', action.payload.count);
      case CONSANTS.SELECT_LBS_TRACK_ERROR:
        return state.set('error', action.error).set('loading', false);
      default:
        return state;
    }
  };
};
