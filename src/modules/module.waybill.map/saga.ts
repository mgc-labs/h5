/**
 * WaybillMap Saga
 * @author yanwei
 * @date 2018-9-13 14:23:17
 */

import moment from 'moment';
import { call, put, takeEvery } from 'redux-saga/effects';
import { lbsTrackService, tradeTrackService } from './service';

export default (CONSTANTS, actions) => {
  function* tradeTrack(action) {
    const { payload } = action;
    try {
      const data = yield call(tradeTrackService, payload);
      const result = yield put(actions.selectTradeTrackDone(data));
      // 轨迹点
      const alldata = result.payload.data;
      const list = alldata.utmsTradeWayPoints;

      const params = {
        partyId: alldata.driverId || '', // 手机号
        mode: '',
        filter: true,
        returnFilterData: true,
        datestart: alldata.signDate
          ? alldata.signDate
          : moment().format('YYYY-MM-DD HH:mm:ss'),
        dateend:
          list.length && list[list.length - 1].reachDate
            ? list[list.length - 1].reachDate
            : moment().format('YYYY-MM-DD HH:mm:ss'),
        format: 'BD09',
        collectType: 2,
      };
      // const params = {
      //   partyId: 567951209, // 手机号
      //   mode: '',
      //   filter: true,
      //   returnFilterData: true,
      //   datestart: '2018-09-26 17:13:50',
      //   dateend: '2018-09-27 17:13:50',
      //   format: 'BD09',
      //   collectType: 2,
      // };

      yield put(actions.selectLbsTrack(params));
    } catch (err) {
      yield put(actions.selectTradeTrackError(err));
    }
  }

  function* lbsTrack(action) {
    const { payload } = action;
    try {
      const data = yield call(lbsTrackService, payload);
      yield put(actions.selectLbsTrackDone(data));
    } catch (err) {
      yield put(actions.selectLbsTrackError(err));
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeEvery(CONSTANTS.SELECT_TRADE_TRACK, tradeTrack);
    yield takeEvery(CONSTANTS.SELECT_LBS_TRACK, lbsTrack);
    // yield takeEvery(CONSTANTS.GET_ASYNC_STAT_DATA, getStatData);
  };
};
