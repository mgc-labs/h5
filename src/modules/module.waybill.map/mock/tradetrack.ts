export default {
  result: 'success',
  count: 1,
  data: {
    tradeNumber: 'UTMS15366573150188767',

    driverMobilenumber: '18666668888',

    driverName: '张三',

    driverCarplatenumber: '浙A12345',

    driverCarStruct: '小三轮 2.5米',

    customerName: 'lily总公司',

    customerMobilenumber: '18666668888',

    preDistance: 123,

    actualDistance: 123132132,

    wayPointNum: 12,

    lbsTrackPointNum: 13,

    createDate: '2018-08-30 13:00:00',

    dispatchDate: '2018-08-30 13:00:00',

    receiveDate: '2018-08-30 13:00:00',

    signDate: '2018-08-30 13:00:00',
    signLongitude: 120.3709403893534,
    signLatitude: 30.30554065388391,
    utmsTradeWayPoints: [
      {
        sortOrder: 0,

        loadType: 0,

        address: '杭州市江干区下沙经济技术开发区千帆路1号（全峰快递）',

        longitude: 120.186941,

        latitude: 30.289172,
        reachAddress:
          '浙江省杭州市江干区白杨街道天元公寓杭州经济技术开发区东部厂区',

        reachDate: '2018-09-16 10:48:23',

        reachLng: 120.37087136567267,

        reachLat: 30.305630718057216,
      },
      {
        sortOrder: 0,

        loadType: 1,

        address: '浙江省杭州市江干区白杨街道文淙南路',

        longitude: 120.243283,

        latitude: 30.310126,
        reachAddress: '浙江省杭州市江干区白杨街道文淙南路',

        reachDate: '2018-09-16 11:54:13',

        reachLng: 120.37086636390492,

        reachLat: 30.30563572269132,
      },
      {
        sortOrder: 0,

        loadType: 1,

        address: '浙江省杭州市江干区白杨街道文淙南路',

        longitude: 120.390462,

        latitude: 30.394395,
        reachAddress: '浙江省杭州市江干区白杨街道文淙南路',

        reachDate: '2018-09-16 11:54:22',

        reachLng: 120.38804861700086,

        reachLat: 30.306852224389125,
      },
      {
        sortOrder: 0,

        loadType: 1,

        address: '浙江省杭州市江干区白杨街道天元公寓杭州经济技术开发区东部厂区',

        longitude: 120.524417,

        latitude: 30.482574,
        reachAddress:
          '浙江省杭州市江干区白杨街道天元公寓杭州经济技术开发区东部厂区',

        reachDate: '2018-09-16 10:48:45',

        reachLng: 120.38803257156387,

        reachLat: 30.30574622671414,
      },
      {
        sortOrder: 0,

        loadType: 1,

        address: '中国浙江省杭州市江干区25号大街',

        longitude: 120.56811,

        latitude: 30.426288,
        reachAddress: '中国浙江省杭州市江干区25号大街',

        reachDate: '2018-09-16 11:04:07',

        reachLng: 120.39218903216488,

        reachLat: 30.322842709738943,
      },
      {
        sortOrder: 0,

        loadType: 1,

        address: '浙江省杭州市江干区白杨街道25号大街新雁公寓1幢A',

        longitude: 120.683668,

        latitude: 30.517923,
        reachAddress: '浙江省杭州市江干区白杨街道25号大街新雁公寓1幢A',

        reachDate: '2018-09-16 11:06:36',

        reachLng: 120.39218803170388,

        reachLat: 30.322842710627018,
      },
    ],
  },
};
