/**
 * WaybillMap selectors
 * @author yanwei
 * @date 2018-9-13 14:23:17
 */
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, state => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, state => state.get('loading'));

export const makeSelectData = selectState =>
  createSelector(selectState, state => state.get('data'));

export const makeSelectlbsSource = selectState =>
  createSelector(selectState, state => state.get('lbsSource'));

export const makeSelectlbsPoints = selectState =>
  createSelector(selectState, state => state.get('lbsPoints'));
