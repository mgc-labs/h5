export class MP {
  private isMouseIn: boolean;
  private i: number;
  private div: HTMLDivElement;
  private img: HTMLDivElement;
  constructor(div: HTMLDivElement) {
    this.i = 0;
    this.isMouseIn = false;
    this.div = div;
    this.img = this.div.nextSibling as HTMLDivElement;
    this.div.onmouseenter = () => {
      this.isMouseIn = true;
      this.i = 0;
      this.add();
    };
    this.div.onmouseout = () => {
      this.isMouseIn = false;
      this.cot();
    };
  }

  add() {
    if (this.i < 24 && this.isMouseIn) {
      requestAnimationFrame(() => {
        this.img.style.backgroundPosition = '0 -' + 50 * this.i + 'px';
        this.add();
      });
      this.i++;
    }
  }
  cot() {
    if (this.i > -1) {
      requestAnimationFrame(() => {
        this.img.style.backgroundPosition = '0 -' + 50 * this.i + 'px';
        this.cot();
      });
      this.i--;
    }
  }
}
