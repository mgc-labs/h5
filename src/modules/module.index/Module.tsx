/**
 * TMS 首页
 * @author: ryan bian
 */
import classNames from 'classnames';
import * as React from 'react';

import { Link } from 'react-router-dom';
import { MP } from './animation';
import video from './images/banner_video.mp4';
import styles from './index.module.less';

class IndexPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      position: '',
      animation: null,
      speed: 0, // s
      time: 13, // u
      m: 100, // m
    };
  }
  componentDidMount() {
    // this.handleWindowResize();
    new MP(this.div1);
    new MP(this.div2);
    new MP(this.div3);
    new MP(this.div4);
    new MP(this.div5);
    new MP(this.div6);
  }
  componentDidUpdate() {
    // this.handleWindowResize();
  }
  handleWindowResize = () => {
    this.syncFixedNextPosition();
  };
  syncFixedNextPosition = () => {
    const h =
      document.documentElement.offsetHeight || document.body.offsetHeight;
    // console.log('h:::::::::', h);
  };
  handleMouseEnter = e => {
    const elementBox = this.sloganRef.getBoundingClientRect();
    const halfWidth = this.sloganRef.offsetWidth / 2;
    const halfHeight = this.sloganRef.offsetHeight / 2;
    // 得到中心点
    const center = {
      x: elementBox.left + halfWidth,
      y: elementBox.top + halfHeight,
    };
    // 得到鼠标偏离中心点的距离
    const pagePoint = {
      x: e.pageX,
      y: e.pageY,
    };

    const disX = pagePoint.x - center.x;
    const disY = pagePoint.y - center.y;

    if (disY < 0 && Math.abs(disY / disX) >= 1) {
      // 上方
      this.setState({
        position: 'top',
      });
    } else if (disY > 0 && Math.abs(disY / disX) >= 1) {
      // 下
      this.setState({
        position: 'bottom',
      });
    } else if (disX < 0 && Math.abs(disY / disX) < 1) {
      // 左
      this.setState({
        position: 'left',
      });
    } else {
      // 右
      this.setState({
        position: 'right',
      });
    }
  };
  handleMouseLeave = e => {
    this.setState({
      position: '',
    });
  };
  render() {
    const { position } = this.state;
    const type = classNames({
      [styles.Index__Top_Slogan]: true,
      [styles.Index__Top_SloganTop]: position === 'top',
      [styles.Index__Top_SloganBottom]: position === 'bottom',
      [styles.Index__Top_SloganLeft]: position === 'left',
      [styles.Index__Top_SloganRight]: position === 'right',
    });

    return (
      <div className={styles.Index}>
        <div className={styles.Index__Top}>
          <div className={styles.Index__Header}>
            <div className={styles.Index__Header_Bar}>
              <a href="/">
                <i />
              </a>
              <div className={styles.Index__Header_Login}>
                <Link to="/login">登录</Link>
              </div>
            </div>
            <div
              className={type}
              ref={node => (this.sloganRef = node)}
              onMouseEnter={e => {
                this.handleMouseEnter(e);
              }}
              onMouseLeave={e => {
                this.handleMouseLeave(e);
              }}
            />
          </div>
          <div className={styles.Index__Top_Next}>
            <i />
          </div>
          <video
            className={styles.Index__Video}
            autoPlay
            loop
            muted
            src={video}
          />
        </div>
        <div className={styles.Index__RightBar}>
          <div className={styles.Index__RightBarItem}>
            客服
            <br />
            电话
          </div>
          <div className={styles.Index__RightBarItem}>
            APP
            <br />
            下载
          </div>
        </div>
        <div className={styles.Index__Content}>
          <div className={styles.Index__First}>
            <h2 />
            <div className={styles.Index__First_Box}>
              <div className={styles.Index__First_P}>
                <div
                  className={styles.Index__ImgBox}
                  ref={node => (this.div1 = node)}
                />
                <i
                  className={classNames({
                    [styles.Index__Img]: true,
                    [styles.Index__Img1]: true,
                  })}
                />
                <h3>订单管理</h3>
                <h4>支持API接口， Excel表格等形式批量订单导入，快速创建订单</h4>
              </div>
              <div className={styles.Index__First_P}>
                <div
                  className={styles.Index__ImgBox}
                  ref={node => (this.div2 = node)}
                />
                <i
                  className={classNames({
                    [styles.Index__Img]: true,
                    [styles.Index__Img2]: true,
                  })}
                />
                <h3>智能配载</h3>
                <h4>
                  基于智慧算法，合并共配订单，智能规划配送路径及卸货顺序，提升装载率
                </h4>
              </div>
              <div className={styles.Index__First_P}>
                <div
                  className={styles.Index__ImgBox}
                  ref={node => (this.div3 = node)}
                />
                <i
                  className={classNames({
                    [styles.Index__Img]: true,
                    [styles.Index__Img3]: true,
                  })}
                />
                <h3>高效调度</h3>
                <h4>实时运力分布显示，结合订单进行多种调度，减少车辆空驶率</h4>
              </div>
              <div className={styles.Index__First_P}>
                <div
                  className={styles.Index__ImgBox}
                  ref={node => (this.div4 = node)}
                />
                <i
                  className={classNames({
                    [styles.Index__Img]: true,
                    [styles.Index__Img4]: true,
                  })}
                />
                <h3>在途监控</h3>
                <h4>
                  配合多款硬件，跟踪车与货，监控行驶温湿度，电子围栏设定，及时预警异常情况
                </h4>
              </div>
              <div className={styles.Index__First_P}>
                <div
                  className={styles.Index__ImgBox}
                  ref={node => (this.div5 = node)}
                />
                <i
                  className={classNames({
                    [styles.Index__Img]: true,
                    [styles.Index__Img5]: true,
                  })}
                />
                <h3>数字财务</h3>
                <h4>
                  运输费用自动结算，电子账单一键生成，轻松查看公司利润、成本等指标
                </h4>
              </div>
              <div className={styles.Index__First_P}>
                <div
                  className={styles.Index__ImgBox}
                  ref={node => (this.div6 = node)}
                />
                <i
                  className={classNames({
                    [styles.Index__Img]: true,
                    [styles.Index__Img6]: true,
                  })}
                />
                <h3>增值服务</h3>
                <h4>
                  一键代收，保险理赔，运费到付，多种增值服务，赋予企业更大商业价值
                </h4>
              </div>
            </div>
          </div>
          <div className={styles.Index__Second}>
            <h2 />
            <div
              className={classNames(styles.Index__Floor, styles.Index__Floor1)}
            >
              <div className={classNames(styles.Index__Floor_bg)}>
                <div className={styles.Index__Floor_Text}>
                  <h5>经验输出</h5>
                  <p>专业城配公司技术团队研发，系统更贴合实际应用场景</p>
                </div>
              </div>
            </div>
            <div
              className={classNames(styles.Index__Floor, styles.Index__Floor2)}
            >
              <div className={classNames(styles.Index__Floor_bg)}>
                <div className={styles.Index__Floor_Text}>
                  <h5>落地简单</h5>
                  <p>SAAS服务+云端布署，操作简单，维护成本低</p>
                </div>
              </div>
            </div>
            <div
              className={classNames(styles.Index__Floor, styles.Index__Floor3)}
            >
              <div className={classNames(styles.Index__Floor_bg)}>
                <div className={styles.Index__Floor_Text}>
                  <h5>智能系统</h5>
                  <p>云平台+Iot设备+智慧算法，让配送更透明，更智能，更高效</p>
                </div>
              </div>
            </div>
            <div
              className={classNames(styles.Index__Floor, styles.Index__Floor4)}
            >
              <div className={classNames(styles.Index__Floor_bg)}>
                <div className={styles.Index__Floor_Text}>
                  <h5>促进引流</h5>
                  <p>开放多种接口，协同对接主流物流系统，精准引流</p>
                </div>
              </div>
            </div>
            <div
              className={classNames(styles.Index__Floor, styles.Index__Floor5)}
            >
              <div className={classNames(styles.Index__Floor_bg)}>
                <div className={styles.Index__Floor_Text}>
                  <h5>资金渠道</h5>
                  <p>
                    连接金融公司，以系统沉淀数据为背书，为客户提供无抵押融资服务
                  </p>
                </div>
              </div>
            </div>
            <div
              className={classNames(styles.Index__Floor, styles.Index__Floor6)}
            >
              <div className={classNames(styles.Index__Floor_bg)}>
                <div className={styles.Index__Floor_Text}>
                  <h5>服务无忧</h5>
                  <p>7*24小时在线客服，实时响应客户需求</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div className={styles.Index__Footer}>
            <div className={styles.Index__FooterL} />
            <div className={styles.Index__FooterR}>
              <div className={styles.Index__FooterR1} />
              <div className={styles.Index__FooterR2} />
            </div>
          </div>
          <p>浙公网安备 33010902000631号 | 浙ICP备14026855号-4</p>
        </footer>
        {/* <a href="/login">点击进入tms系统</a> */}
      </div>
    );
  }
}
export default IndexPage;
