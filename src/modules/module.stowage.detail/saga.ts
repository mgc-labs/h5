/**
 * StowageDetail Saga
 * @author hefan
 * @date 2018/9/15 下午4:21:48
 */
import message from 'antd/es/message';
import { call, put, takeLatest } from 'redux-saga/effects';

import { getGroupDetail } from './service';

export default (CONSTANTS, actions) => {
  function* getData(action) {
    try {
      const data = yield call(getGroupDetail, action.payload);
      if (data.data) {
        yield put(actions.getAsyncDataDone(data));
      } else {
        message.error(data.msg);
        yield put(actions.getAsyncDataError(data.msg));
      }
    } catch (err) {
      yield put(actions.getAsyncDataError(err));
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_ASYNC_DATA, getData);
  };
};
