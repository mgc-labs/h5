/**
 * StowageDetail Actions
 * @author hefan
 * @date 2018/9/15 下午4:21:48
 */
import { AnyAction } from 'redux';
import {
  GET_ASYNC_DATA,
  GET_ASYNC_DATA_ERROR,
  GET_ASYNC_DATA_SUCCESS,
} from './constants';

export default CONSANTS => ({
  getAsyncData: (param): AnyAction => ({
    payload: param,
    type: CONSANTS.GET_ASYNC_DATA,
  }),

  getAsyncDataDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_ASYNC_DATA_SUCCESS,
  }),

  getAsyncDataError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSANTS.GET_ASYNC_DATA_ERROR,
  }),
});
