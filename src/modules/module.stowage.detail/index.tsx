/**
 * StowageDetail Component
 * @author hefan
 * @date 2018/9/15 下午4:21:48
 */
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import Card from 'antd/es/card';
import Divider from 'antd/es/divider';
import Table from 'antd/es/table';
import Tabs from 'antd/es/tabs';
import DescriptionList from 'components/DescriptionList';
import Download from 'components/Download';
const { Description } = DescriptionList;
const { TabPane } = Tabs;
import GLOBAL from 'utils/GLOBAL';
const { emptyRecord } = GLOBAL;

const columns = [
  {
    title: '订单号',
    dataIndex: 'orderNumber',
    key: 'orderNumber',
  },
  {
    title: '详细地址',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: '省市区',
    dataIndex: 'province',
    key: 'province',
  },
  {
    title: '体积(方)',
    dataIndex: 'goodsVolume',
    key: 'goodsVolume',
  },
  {
    title: '重量(吨)',
    dataIndex: 'goodsWeight',
    key: 'goodsWeight',
  },
  {
    title: '件数(件)',
    dataIndex: 'goodsNumber',
    key: 'goodsNumber',
  },
  {
    title: '收货人',
    dataIndex: 'contact',
    key: 'contact',
  },
  {
    title: '联系电话',
    dataIndex: 'phone',
    key: 'phone',
  },
];

import {
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

// import 'ant-design-pro/lib/DescriptionList/style/index.css';
import styles from './index.module.less';

declare const BMap: any;

export interface IStowageDetailProps {
  children: React.ReactChildren;
  getAsyncData: (param: {}) => void;
}

class StowageDetail extends React.PureComponent<IStowageDetailProps> {
  state = {
    locationNum: 0,
    notLocationNum: 0,
  };
  private mapElm: HTMLElement;
  private map: {};
  /**
   * componentDidMount
   */
  public componentDidMount() {
    const { searchParams } = this.props;

    if (searchParams && searchParams.partGroupId) {
      this.props.getAsyncData({
        partGroupId: parseInt(searchParams.partGroupId),
        coordinateType: 'BD09',
      });
    }
  }
  public render() {
    const { data } = this.props;
    const dataJs = data ? data.toJS() : {};

    return (
      <div className={styles.StowageDetail}>
        <Card
          style={{
            marginBottom: '12px',
          }}
          key="s-1"
        >
          <DescriptionList size="large" title="基本信息">
            <Description term="配载单名称" key="name">
              {dataJs.name || emptyRecord}
            </Description>
            <Description term="配载ID" key="utmsPartGroupId">
              {dataJs.utmsPartGroupId || emptyRecord}
            </Description>
            <Description term="仓库名称" key="warehouseName">
              {dataJs.warehouseName || emptyRecord}
            </Description>
            <Description term="订单数" key="orderCount">
              {dataJs.orderCount || emptyRecord}
            </Description>
            <Description term="体积(方)" key="volume">
              {dataJs.volume || emptyRecord}
            </Description>
            <Description term="重量(千克)" key="weight">
              {dataJs.weight || emptyRecord}
            </Description>
            <Description term="件数(件)" key="quantity">
              {dataJs.quantity || emptyRecord}
            </Description>
            <Description term="制单人" key="realName">
              {dataJs.realName || emptyRecord}
            </Description>
            <Description term="生成时间" key="gmtCreate">
              {dataJs.gmtCreate || emptyRecord}
            </Description>
          </DescriptionList>
        </Card>
        <Card key="s-2">
          {/* <Divider /> */}
          <DescriptionList
            size="large"
            title={this.tabTitle()}
            style={{ margin: '0 16px 32px' }}
          >
            <Tabs onChange={this.switchTabHandle}>
              <TabPane key="1" tab="列表展示">
                {this.getListRender()}
              </TabPane>
              <TabPane key="2" tab="地图展示">
                {this.getMapRander()}
              </TabPane>
            </Tabs>
          </DescriptionList>
        </Card>
      </div>
    );
  }
  private tabTitle() {
    const { data } = this.props;

    return (
      <>
        订单明细
        <Download
          btnType="primary"
          className="pull-right"
          url={'/ehuodiGateway/utmsTrade/utmsPartOrder/exportPartOrderList'}
          params={{
            partGroupId: data.get('utmsPartGroupId'),
          }}
        >
          导出
        </Download>
      </>
    );
  }
  private switchTabHandle = activeKey => {
    if (activeKey === '2' && !this.map) {
      setTimeout(() => {
        this.map = new BMap.Map(this.mapElm);
        this.map.centerAndZoom(new BMap.Point(116.404, 39.915), 11);
        this.map.enableScrollWheelZoom(true);
        this.drawPoint();
      }, 1000);
    }
  };

  private drawPoint() {
    const data = this.props.data.toJS();
    const pointArray = [];
    const pIcon = new BMap.Icon(
      'https://image.tf56.com/dfs/group1/M00/34/22/CiFBClmyXUOAEh-OAAARqg9D7FA643.png',
      new BMap.Size(20, 20),
      { imageSize: { width: 20, height: 20 } },
    );

    if (data.partOrders) {
      let locationNum = 0;
      let notLocationNum = data.partOrders.length;
      data.partOrders.map((item, index) => {
        if (item.longitude) {
          ++locationNum;

          const point = new BMap.Point(item.longitude, item.latitude);

          const marker = new BMap.Marker(point, {
            icon: pIcon,
          });

          const label = new BMap.Label(`${index + 1}`);
          label.setStyle({
            color: '#ffffff',
            backgroundColor: 'rgb(47, 138, 241)',
            border: '1px solid rgb(175, 220, 240)',
            width: '20px',
            height: '20px',
            lineHeight: '18px',
            textAlign: 'center',
            borderRadius: '100%',
          });

          marker.setLabel(label);

          pointArray.push(point);

          this.map.addOverlay(marker);
        }
      });
      notLocationNum -= locationNum;
      const polygon = new BMap.Polyline(pointArray, {
        strokeColor: 'blue',
        strokeWeight: 2,
        strokeOpacity: 0.5,
      });

      if (data.warehouseLongitude && data.warehouseLatitude) {
        const storePoint = new BMap.Point(
          data.warehouseLongitude,
          data.warehouseLatitude,
        );
        pointArray.push(storePoint);
        const store = new BMap.Marker(storePoint, {
          icon: new BMap.Icon(
            'https://image.tf56.com/dfs/group1/M00/69/94/CiFBCVu8SROAdNC_AAAD_T3ZZRs656.png',
            new BMap.Size(20, 30),
            { imageSize: { width: 20, height: 30 } },
          ),
        });

        this.map.addOverlay(store);
      }

      this.map.addOverlay(polygon);
      this.map.setViewport(pointArray);

      this.setState({
        locationNum,
        notLocationNum,
      });
    }
  }
  private getListRender() {
    const { data } = this.props;
    const dataJs = data.toJS();

    const dataSource = dataJs ? dataJs.partOrders : [];

    return (
      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={false}
        rowKey="orderNumber"
      />
    );
  }
  private getMapRander() {
    const { locationNum, notLocationNum } = this.state;
    return (
      <div className={styles.MapWrap}>
        <div ref={node => (this.mapElm = node)} style={{ height: '400px' }} />
        <p className={styles.locationed}>已定位 {locationNum}</p>
        <p className={styles.location}>未定位 {notLocationNum}</p>
      </div>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getAsyncData: param => dispatch(actions.getAsyncData(param)),
  });

  const selectState = state => state.get(routeId);

  const mapStateToProps = createStructuredSelector({
    data: makeSelectData(selectState),
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(StowageDetail);
};
