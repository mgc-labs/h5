/**
 * fixpath.list Service
 * @author ryan bian
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_URL = {
  PARTGROUPDETAIL: '/ehuodiGateway/utmsTrade/utmsPartGroup/partGroupDetail',
};

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  // mocker
  //   .on('post', API_URL.PARTGROUPDETAIL)
  //   .reply(() => import('./mock/mock').then(exports => [200, exports.default]));
}

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getGroupDetail = data => {
  return request(
    {
      method: 'post',
      url: API_URL.PARTGROUPDETAIL,
      data: qs.stringify(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
