/**
 * Ios Reducers
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */

import { fromJS, List, Map } from 'immutable';
import { newLocOrderListSign } from './util';

const initialState = Map({
  locOrderList: [], // 地图上的所有点。由于性能问题，这里不用 immutable
  locOrderListSign: '', // 每次更新 locOrderList时，产生一个新的值
  unLocOrderData: Map({
    data: List(),
    count: 0,
  }),
  orderGroupShow: false,
  autoGroLoading: false,
  orderGroupList: [],
  selectedGroupId: null, // 当前的打开的订单组编号
  isShowOrderListOfGroup: false, // 是否显示订单列表
  checkedGroupIdList: [], // 选中的订单组编号
  orderListOfGroup: [],
  warehouseList: List(),
  warehouseLoaded: false, // 是否已加载仓库
  defaultWarehouse: undefined, // 默认仓库
  deleteGroupLoading: false,
  deleteOrderLoading: false,
  orderGroupSendLoading: false,
  moveOrderLoading: false,
  config: null,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      // 获取推荐车型
      case CONSANTS.GET_VEHICLE_REC:
        return state
          .set('getRecVehicleError', false)
          .set('getRecVehicleLoading', true);
      case CONSANTS.GET_VEHICLE_REC_SUCCESS:
        const vehicleList = action.payload
          .filter(h => ['厢式货车', '中面', '小面'].includes(h.vehicleType))
          .sort((v1, v2) => {
            return v1.vehicleVolume - v2.vehicleVolume;
          });
        return state
          .set('getRecVehicleLoading', false)
          .set('recVehicleList', fromJS(vehicleList));
      case CONSANTS.GET_VEHICLE_REC_ERROR:
        return state
          .set('getRecVehicleError', action.error)
          .set('getRecVehicleLoading', false);

      // 获取仓库
      case CONSANTS.GET_WAREHOUSE:
        return state
          .set('getWarehouseError', false)
          .set('getWarehouseLoading', true);
      case CONSANTS.GET_WAREHOUSE_SUCCESS:
        const warehouseList = action.payload || [];
        const defaultWarehouse = warehouseList.filter(h => h.isDefault)[0];

        return state
          .set('getWarehouseLoading', false)
          .set('warehouseList', warehouseList)
          .set('defaultWarehouse', defaultWarehouse)
          .set('warehouseLoaded', true);
      case CONSANTS.GET_WAREHOUSE_ERROR:
        return state
          .set('getWarehouseError', action.error)
          .set('getWarehouseLoading', false);

      // 自动配载
      case CONSANTS.AUTO_GRO_OPEN:
        return state.set('isShowAutoGroConfirm', true);
      case CONSANTS.AUTO_GRO_CLOSE:
        return state.set('isShowAutoGroConfirm', false);
      case CONSANTS.AUTO_GRO:
        return state.set('autoGroError', false).set('autoGroLoading', true);
      case CONSANTS.AUTO_GRO_SUCCESS:
        return state
          .set('autoGroLoading', false)
          .set('isShowAutoGroConfirm', false);
      case CONSANTS.AUTO_GRO_ERROR:
        return state
          .set('autoGroError', action.error)
          .set('autoGroLoading', false);

      // 手动配载
      case CONSANTS.CREATE_GROUP_OPEN:
        return state.set('isShowCreateGroup', true);
      case CONSANTS.CREATE_GROUP_CLOSE:
        return state.set('isShowCreateGroup', false);
      case CONSANTS.CREATE_GROUP:
        return state
          .set('createGroupError', false)
          .set('createGroupLoading', true);
      case CONSANTS.CREATE_GROUP_SUCCESS:
        return state
          .set('createGroupLoading', false)
          .set('isShowCreateGroup', false);
      case CONSANTS.CREATE_GROUP_ERROR:
        return state
          .set('createGroupError', action.error)
          .set('createGroupLoading', false);

      // 添加到组
      case CONSANTS.ADDTO_GROUP_OPEN:
        return state.set('isShowAddToGroup', true);
      case CONSANTS.ADDTO_GROUP_CLOSE:
        return state.set('isShowAddToGroup', false);
      case CONSANTS.ADDTO_GROUP:
        return state
          .set('addToGroupError', false)
          .set('addToGroupLoading', true);
      case CONSANTS.ADDTO_GROUP_SUCCESS:
        return state
          .set('addToGroupLoading', false)
          .set('isShowAddToGroup', false);
      case CONSANTS.ADDTO_GROUP_ERROR:
        return state
          .set('addToGroupError', action.error)
          .set('addToGroupLoading', false);

      // 获取配置
      case CONSANTS.GET_CONFIG:
        return state.set('getConfigError', false).set('getConfigLoading', true);
      case CONSANTS.GET_CONFIG_SUCCESS: {
        return state
          .set('getConfigLoading', false)
          .set('config', action.payload)
          .set('isShowConfigModal', true);
      }

      case CONSANTS.GET_CONFIG_ERROR:
        return state
          .set('getConfigError', action.error)
          .set('getConfigLoading', false);

      // 保存配置
      case CONSANTS.CONFIG_SAVE:
        return state
          .set('saveConfigError', false)
          .set('saveConfigLoading', true);
      case CONSANTS.CONFIG_SAVE_SUCCESS:
        return state
          .set('saveConfigLoading', false)
          .set('isShowConfigModal', false);
      case CONSANTS.CONFIG_SAVE_ERROR:
        return state
          .set('saveConfigError', action.error)
          .set('saveConfigLoading', false);
      case CONSANTS.CONFIG_CANCEL:
        return state.set('isShowConfigModal', false);

      // 订单组列表
      case CONSANTS.GET_GROUP:
        return state
          .set('orderGroupError', false)
          .set('orderGroupLoading', true);
      case CONSANTS.GET_GROUP_SUCCESS:
        const ogList = action.payload as any[];

        ogList.map((item, index) => {
          item.no = index + 1;
        });

        let selectedGroupId = state.get('selectedGroupId');
        let isShowOrderListOfGroup = state.get('isShowOrderListOfGroup');
        let orderListOfGroup = state.get('orderListOfGroup');
        let orderGroupShow = state.get('orderGroupShow');
        if (selectedGroupId !== null) {
          // 如果当前打开的组 已被删掉了，则需要关闭打开的订单列表，
          if (!ogList.find(h => h.utmsPartGroupId === selectedGroupId)) {
            selectedGroupId = null;
            isShowOrderListOfGroup = false;
            orderListOfGroup = [];
          }
        }

        const checkedGroupIdList = state.get('checkedGroupIdList') as string[];
        const ckdGroupIdList = [];
        checkedGroupIdList.map(h => {
          if (ogList.find(h2 => h2.utmsPartGroupId === h)) {
            ckdGroupIdList.push(h);
          }
        });

        if (ogList.length < 1) {
          orderGroupShow = false;
        }

        return state
          .set('selectedGroupId', selectedGroupId)
          .set('orderGroupLoading', false)
          .set('orderGroupList', ogList)
          .set('checkedGroupIdList', ckdGroupIdList)
          .set('orderGroupShow', orderGroupShow)
          .set('isShowOrderListOfGroup', isShowOrderListOfGroup)
          .set('orderListOfGroup', orderListOfGroup);

      case CONSANTS.GET_GROUP_ERROR:
        return state
          .set('orderGroupError', action.error)
          .set('orderGroupLoading', false);

      // 订单组列表，是否显示
      case CONSANTS.GROUP_SHOW: {
        return state
          .set('orderGroupShow', action.payload)
          .set('selectedGroupId', null) // 取消选中组
          .set('isShowOrderListOfGroup', false); // 把订单也关闭
      }

      // 订单组列表，单行选择
      case CONSANTS.GROUP_CHECK: {
        const value = action.payload;
        const cIdList = [].concat(state.get('checkedGroupIdList'));
        const index = cIdList.indexOf(value);
        // 存在的更新,不存在的插入
        if (index > -1) {
          cIdList.splice(index, 1);
        } else {
          cIdList.push(value);
        }
        return state.set('checkedGroupIdList', cIdList);
      }
      // 订单组列表，全选择
      case CONSANTS.GROUP_ALL_CHECK: {
        const orderGroupList1 = state.get('orderGroupList') as any[];
        const checked = action.payload;

        if (checked) {
          return state.set(
            'checkedGroupIdList',
            orderGroupList1.map(item => item.utmsPartGroupId),
          );
        } else {
          return state.set('checkedGroupIdList', []);
        }
      }
      // 订单组，选中一个组时
      case CONSANTS.GROUP_SELECT: {
        return state
          .set('isShowOrderListOfGroup', true)
          .set('selectedGroupId', action.payload.utmsPartGroupId);
      }
      case CONSANTS.GROUP_DELETE: {
        return state.set('deleteGroupLoading', true);
      }
      case CONSANTS.GROUP_DELETE_SUCCESS: {
        return state.set('deleteGroupLoading', false);
      }
      case CONSANTS.GROUP_DELETE_ERROR: {
        return state.set('deleteGroupLoading', false);
      }

      // 获取一个组下的订单
      case CONSANTS.GET_ORDER_OF_GROUP:
        return state
          .set('getOrderListOfGroupError', false)
          .set('getOrderListLoading', true);
      case CONSANTS.GET_ORDER_OF_GROUP_SUCCESS: {
        const orlg = action.payload as any[];
        return state
          .set('getOrderListLoading', false)
          .set('orderListOfGroup', orlg);
      }
      case CONSANTS.GET_ORDER_OF_GROUP_ERROR:
        return state
          .set('getOrderListOfGroupError', action.error)
          .set('getOrderListLoading', false);

      // 订单手工排序
      case CONSANTS.ORDER_REORDER:
        return state
          .set('getOrderListOfGroupError', false)
          .set('getOrderListLoading', true);
      case CONSANTS.ORDER_REORDER_SUCCESS:
        return state
          .set('getOrderListLoading', false)
          .set('orderListOfGroup', [].concat(action.payload));
      case CONSANTS.ORDER_REORDER_ERROR:
        return state
          .set('getOrderListOfGroupError', action.error)
          .set('getOrderListLoading', false);
      case CONSANTS.ORDER_HIDE:
        return state
          .set('isShowOrderListOfGroup', false)
          .set('selectedGroupId', null)
          .set('orderListOfGroup', []);

      // 订单列表，单行选择
      case CONSANTS.ORDER_CHECK: {
        const olGroup = state.get('orderListOfGroup') as any[];
        const record = action.payload;
        const index = olGroup.findIndex(
          h => h.utmsPartOrderId === record.utmsPartOrderId,
        );
        olGroup[index].isChecked = !record.isChecked;

        return state.set('orderListOfGroup', [].concat(olGroup));
      }
      // 订单列表，全选择
      case CONSANTS.ORDER_ALL_CHECK: {
        const olg = state.get('orderListOfGroup') as any[];
        const checked = action.payload;
        olg.map(item => {
          return (item.isChecked = checked);
        });
        return state.set('orderListOfGroup', [].concat(olg));
      }

      // 从组里删除订单
      case CONSANTS.ORL_DELETE_FROM_GROUP: {
        return state
          .set('deleteOrderFromGroupError', false)
          .set('deleteOrderLoading', true);
      }
      case CONSANTS.ORL_DELETE_FROM_GROUP_S: {
        return state.set('deleteOrderLoading', false);
      }
      case CONSANTS.ORL_DELETE_FROM_GROUP_E: {
        return state
          .set('deleteOrderFromGroupError', action.error)
          .set('orderGroupLoading', false);
      }

      // 移动订单到别的组
      case CONSANTS.ORL_MOVE_TO_GROUP: {
        return state
          .set('moveOrderToGroupError', false)
          .set('moveOrderLoading', true);
      }
      case CONSANTS.ORL_MOVE_TO_GROUP_SUCCESS: {
        return state.set('moveOrderLoading', false);
      }
      case CONSANTS.ORL_MOVE_TO_GROUP_ERROR: {
        return state
          .set('moveOrderToGroupError', action.error)
          .set('moveOrderLoading', false);
      }

      // 已定位订单列表
      case CONSANTS.GET_LOC_ORDER:
        return state
          .set('locOrderListError', false)
          .set('locOrderListLoading', true);
      case CONSANTS.GET_LOC_ORDER_SUCCESS:
        const orderGroupList = state.get('orderGroupList') as any[];

        const list = action.payload.data.map(item => {
          const group = orderGroupList.find(
            h => h.utmsPartGroupId === item.partGroupId,
          );
          const status = item.status === '待配载' ? 0 : 1;
          return {
            ...item,
            status,
            isSelected: false,
            no: status === 1 ? group.no : undefined,
          };
        });

        // const list = action.payload.data
        //   .map(item => {
        //     const group = orderGroupList.find(
        //       h => h.utmsPartGroupId === item.partGroupId,
        //     );
        //     const status = item.status === '待配载' ? 0 : 1;
        //     return {
        //       ...item,
        //       status,
        //       isSelected: false,
        //       no: status === 1 && group ? group.no : undefined,
        //       isError: status === 1 && group === undefined,
        //     };
        //   })
        //   .filter(h => !h.isError);

        return state
          .set('locOrderListLoading', false)
          .set('locOrderList', list)
          .set('locOrderListSign', newLocOrderListSign());
      case CONSANTS.GET_LOC_ORDER_ERROR:
        return state
          .set('locOrderListError', action.error)
          .set('locOrderListLoading', false);

      case CONSANTS.LOC_ORDER_SELECTED: {
        return state
          .set('locOrderList', action.payload)
          .set('locOrderListSign', newLocOrderListSign());
      }

      // 获取未定位订单列表
      case CONSANTS.GET_UNLOC_ORDER:
        return state
          .set('unLocOrderListError', false)
          .set('unLocOrderListLoading', true);
      case CONSANTS.GET_UNLOC_ORDER_SUCCESS:
        return state
          .set('unLocOrderData', fromJS(action.payload))
          .set('unLocOrderListLoading', false);
      case CONSANTS.GET_UNLOC_ORDER_ERROR:
        return state
          .set('unLocOrderListLoading', false)
          .set('unLocOrderListError', action.error);

      // 位置更正
      case CONSANTS.UPDATE_UNLOC_LOCATION:
        return state.set('unLocOrderListLoading', true);
      case CONSANTS.UPDATE_UNLOC_LOCATION_S:
        return state.set('unLocOrderListLoading', false);
      case CONSANTS.UPDATE_UNLOC_LOCATION_E:
        return state.set('unLocOrderListLoading', false);

      // 发货
      case CONSANTS.GROUP_SEND:
        return state
          .set('orderGroupSendError', false)
          .set('orderGroupSendLoading', true);
      case CONSANTS.GROUP_SEND_SUCCESS:
        return state.set('orderGroupSendLoading', false);
      case CONSANTS.GROUP_SEND_ERROR:
        return state
          .set('orderGroupSendError', action.error)
          .set('orderGroupSendLoading', false);
      default:
        return state;
    }
  };
};
