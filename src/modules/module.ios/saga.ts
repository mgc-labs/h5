/**
 * Ios Saga
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */
import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { call, put, take, takeLatest } from 'redux-saga/effects';

import {
  askAddToGroup,
  askAutoGro,
  askCreateGroup,
  askDeleteGroup,
  askDeleteOrder,
  askDeleteOrderFromGroup,
  askGetConfig,
  askGetGroup,
  askGetOrderListOfGroup,
  askGetWarehouse,
  askLocOrder,
  askMoveOrderToGroup,
  askOrderReorder,
  askRecVehicle,
  askSaveConfig,
  askSend,
  askUpdateOrderLocation,
} from './service';

export default (CONSTANTS, actions) => {
  function* getInitData() {
    try {
      yield put(actions.getGroup());
      yield take(CONSTANTS.GET_GROUP_SUCCESS);
      yield put(actions.getWarehouse());
      yield take(CONSTANTS.GET_WAREHOUSE_SUCCESS);
      yield put(actions.getLocOrder());

      yield put(actions.getRecVehicle()); // 获取推荐车型
      yield put(actions.getUnLocOrder());
    } catch (err) {
      // console.log(err);
    }
  }
  function* getRecVehicle() {
    try {
      const data = yield call(askRecVehicle, {});
      yield put(actions.getRecVehicleDone(data));
    } catch (err) {
      yield put(actions.getRecVehicleError(err));
      message.error(err.msg);
    }
  }
  function* getLocOrder() {
    try {
      const data = yield call(askLocOrder, {
        isLocation: 0, // 已定位的
        status: [0, 1], // 0-待配载;1-待发货;2-待调度;3-配送中;4-已签收; 5-未签收
      });
      yield put(actions.getLocOrderDataDone(data));
    } catch (err) {
      yield put(actions.getLocOrderDataError(err));
      message.error(err.msg);
    }
  }
  function* getUnLocOrder(action) {
    try {
      const data = yield call(askLocOrder, {
        isLocation: 1, // 未定位的
        status: [0], // 0-待配载;1-待发货;2-待调度;3-配送中;4-已签收; 5-未签收
        ...action.payload,
      });
      yield put(actions.getUnLocOrderDone(data));
    } catch (err) {
      yield put(actions.getUnLocOrderError(err));
      message.error(err.msg);
    }
  }
  function* getWarehouse() {
    try {
      const data = yield call(askGetWarehouse, { depotType: 'store' });
      yield put(actions.getWarehouseDone(data));
    } catch (err) {
      yield put(actions.getWarehouseError(err));
      message.error(err.msg);
    }
  }
  function* autoGro(action) {
    try {
      const payload = action.payload;
      const data = yield call(askAutoGro, payload);
      yield put(actions.autoGroDone(data));
      yield put(actions.getGroup()); // 重新加载订单组列表
      yield take(CONSTANTS.GET_GROUP_SUCCESS);
      yield put(actions.getLocOrder()); // 重新加载地图的点
      message.success('已完成自动分组');
    } catch (err) {
      yield put(actions.autoGroError(err));
      message.error(err.msg);
    }
  }
  function* createGroup(action) {
    try {
      yield call(askCreateGroup, action.payload);
      yield put(actions.createGroupDone());
      yield put(actions.getGroup()); // 重新加载订单组列表
      yield take(CONSTANTS.GET_GROUP_SUCCESS);
      yield put(actions.getLocOrder()); // 重新加载地图的点
      message.success('已完成手动分组');
    } catch (err) {
      yield put(actions.createGroupError(err));
      message.error(err.msg);
    }
  }
  function* addToGroup(action) {
    try {
      const payload = action.payload;
      yield call(askAddToGroup, payload);
      yield put(actions.addToGroupDone());
      yield put(actions.getGroup()); // 重新加载订单组列表
      yield take(CONSTANTS.GET_GROUP_SUCCESS);

      yield put(actions.getOrderListOfGroup(payload.partGroupId));

      yield put(actions.getLocOrder()); // 重新加载地图的点
      message.success('已添加到组');
    } catch (err) {
      yield put(actions.addToGroupError(err));
      message.error(err.msg);
    }
  }
  function* groupSelected(action) {
    yield put(actions.getOrderListOfGroup(action.payload.utmsPartGroupId));
  }
  function* getGroup() {
    try {
      const data = yield call(askGetGroup, {
        status: 0, // 0-待发货；1-已发货
      });
      yield put(actions.getGroupDone(data || []));
    } catch (err) {
      yield put(actions.getGroupError(err));
      message.error(err.msg);
    }
  }
  function* getOrderListOfGroup(action) {
    try {
      const data = yield call(askGetOrderListOfGroup, {
        partGroupId: action.payload,
      });
      yield put(actions.getOrderListOfGroupDone(data));
    } catch (err) {
      yield put(actions.getOrderListOfGroupError(err));
      message.error(err.msg);
    }
  }
  // 从组里删除订单
  function* deleteOrdersFromGroup(action: any) {
    const payload = action.payload;
    try {
      yield call(askDeleteOrderFromGroup, {
        partGroupId: payload.groupId,
        partOrderIds: payload.orderIds,
      });
      yield put(actions.deleteOrderFromGroupDone());

      yield put(actions.getGroup()); // 重新加载订单组列表
      yield take(CONSTANTS.GET_GROUP_SUCCESS);
      yield put(actions.getOrderListOfGroup(payload.groupId)); // 刷新订单列表
      yield put(actions.getLocOrder()); // 重新加载地图的点
    } catch (err) {
      yield put(actions.deleteOrderFromGroupError(err));
      message.error(err.msg);
    }
  }
  function* orderReorder(pars: any) {
    try {
      const payload = pars.payload;

      yield call(askOrderReorder, {
        partGroupId: payload.groupId,
        partOrderIds: payload.orderList.map(item => item.utmsPartOrderId),
      });
      yield put(actions.orderReOrderDone(payload.orderList));
    } catch (err) {
      yield put(actions.orderReOrderError(err));
      message.error(err.msg);
    }
  }

  function* moveOrdersToGroup(action: any) {
    const payload = action.payload;
    try {
      yield call(askMoveOrderToGroup, {
        fromPartGroupId: payload.fromPartGroupId,
        toPartGroupId: payload.toPartGroupId,
        partOrderIds: payload.orderIds,
      });
      yield put(actions.moveOrderToGroupDone());
      yield put(actions.getOrderListOfGroup(payload.fromPartGroupId));

      yield put(actions.getGroup()); // 重新加载订单组列表
      yield take(CONSTANTS.GET_GROUP_SUCCESS);
      yield put(actions.getLocOrder()); // 重新加载地图的点
      message.success('已完成移动');
    } catch (err) {
      yield put(actions.moveOrderToGroupError(err));
      message.error(err.msg);
    }
  }

  function* deleteGroups(action: any) {
    try {
      const payload = action.payload;
      yield call(askDeleteGroup, {
        partGroupIds: payload.groupIds,
      });
      yield put(actions.deleteGroupDone(payload.groupIds));
      yield put(actions.getGroup()); // 重新获取订单组
      yield take(CONSTANTS.GET_GROUP_SUCCESS);
      yield put(actions.getLocOrder()); // 重新加载点
    } catch (err) {
      yield put(actions.deleteGroupError(err));
      message.error(err.msg);
    }
  }
  function* getConfig() {
    try {
      const data = yield call(askGetConfig, {});
      yield put(actions.getConfigDone(data));
    } catch (err) {
      yield put(actions.getConfigError(err));
      message.error(err.msg);
    }
  }
  function* saveConfig(action: any) {
    try {
      yield call(askSaveConfig, action.payload);
      yield put(actions.saveConfigDone());
      message.success('已完成配载设置');
    } catch (err) {
      yield put(actions.saveConfigError(err));
      message.error(err.msg);
    }
  }

  function* send(action: any) {
    try {
      yield call(askSend, action.payload);
      yield put(actions.sendDone());
      yield put(actions.getGroup()); // 重新加载订单组列表
      yield put(actions.getLocOrder()); // 重新加载地图的点
      message.success('已完成发货');
    } catch (err) {
      yield put(actions.sendError(err));
      message.error(err.msg);
    }
  }

  function* deleteOrder(action) {
    try {
      yield call(askDeleteOrder, action.payload);
      yield put(actions.deleteOrderDone());
      yield put(actions.getUnLocOrder());
    } catch (err) {
      yield put(actions.deleteOrderError(err));
      message.error(err.msg);
    }
  }

  function* gotoEditOrder(action) {
    try {
      const id = action.payload;
      const back = encodeURIComponent('/ios');
      yield put(push(`/ltlOrder/edit?id=${id}&back=${back}`));
    } catch (err) {
      message.error(err.toString());
    }
  }

  function* gotoOrderDetail(action) {
    try {
      yield put(push(`/ltlOrder/detail?id=${action.payload}`));
    } catch (err) {
      message.error(err.toString());
    }
  }

  function* gotoCreateWarehouse(action) {
    try {
      yield put(push(`/company/addressManage`));
    } catch (err) {
      message.error(err.toString());
    }
  }

  function* updateOrderLocation(action: any) {
    try {
      yield call(askUpdateOrderLocation, action.payload);
      yield put(actions.updateOrderLocationDone());
      yield put(actions.getUnLocOrder());
      yield put(actions.getLocOrder());
      message.success('已完成位置修正');
    } catch (err) {
      yield put(actions.updateOrderLocationError(err));
      message.error(err.toString());
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_INIT_DATA, getInitData);
    yield takeLatest(CONSTANTS.GET_VEHICLE_REC, getRecVehicle);
    yield takeLatest(CONSTANTS.GET_LOC_ORDER, getLocOrder);
    yield takeLatest(CONSTANTS.GET_UNLOC_ORDER, getUnLocOrder);
    yield takeLatest(CONSTANTS.GET_WAREHOUSE, getWarehouse);
    yield takeLatest(CONSTANTS.AUTO_GRO, autoGro);
    yield takeLatest(CONSTANTS.CREATE_GROUP, createGroup);
    yield takeLatest(CONSTANTS.ADDTO_GROUP, addToGroup);
    yield takeLatest(CONSTANTS.GET_GROUP, getGroup);
    yield takeLatest(CONSTANTS.GROUP_SELECT, groupSelected);
    yield takeLatest(CONSTANTS.GET_ORDER_OF_GROUP, getOrderListOfGroup);
    yield takeLatest(CONSTANTS.ORL_DELETE_FROM_GROUP, deleteOrdersFromGroup);
    yield takeLatest(CONSTANTS.ORL_MOVE_TO_GROUP, moveOrdersToGroup);
    yield takeLatest(CONSTANTS.ORDER_REORDER, orderReorder);
    yield takeLatest(CONSTANTS.GROUP_DELETE, deleteGroups);
    yield takeLatest(CONSTANTS.GET_CONFIG, getConfig);
    yield takeLatest(CONSTANTS.CONFIG_SAVE, saveConfig);
    yield takeLatest(CONSTANTS.GROUP_SEND, send);
    yield takeLatest(CONSTANTS.GOTO_EDIT_ORDER, gotoEditOrder);
    yield takeLatest(CONSTANTS.GOTO_ORDER_DETAIL, gotoOrderDetail);
    yield takeLatest(CONSTANTS.GOTO_CREATEWAREHOUSE, gotoCreateWarehouse);
    yield takeLatest(CONSTANTS.ORDER_DELETE, deleteOrder);
    yield takeLatest(CONSTANTS.UPDATE_UNLOC_LOCATION, updateOrderLocation);
  };
};
