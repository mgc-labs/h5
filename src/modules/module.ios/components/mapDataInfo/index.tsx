import * as React from 'react';

import styles from './styles.module.less';

interface IMapDataInfoProps {
  locOrderList: any[];
  selectedOrderList: any[];
}

export class MapDataInfo extends React.PureComponent<IMapDataInfoProps, any> {
  render() {
    const { locOrderList, selectedOrderList } = this.props;

    const unGroupOrderList = locOrderList.filter(item => item.status === 0);

    const count = unGroupOrderList.length;
    let weight = 0;
    let volume = 0;

    unGroupOrderList.map(item => {
      weight += item.goodsWeight;
      volume += item.goodsVolume;
    });

    const countOfS = selectedOrderList.length;
    let weightOfS = 0;
    let volumeOfS = 0;

    selectedOrderList.map(item => {
      weightOfS += item.goodsWeight;
      volumeOfS += item.goodsVolume;
    });

    return (
      <div className={styles.mapDataInfo}>
        <div className={styles.numitem}>
          待分组订单：
          <span className={styles.num}>{countOfS}</span>
          <span className={styles.p}>/</span>
          <span className={styles.num}>{count}</span>
        </div>
        <div className={styles.numitem}>
          重量(千克)：
          <span className={styles.num}>{weightOfS.toFixed(2)}</span>
          <span className={styles.p}>/</span>
          <span className={styles.num}>{weight.toFixed(2)}</span>
        </div>
        <div className={styles.numitem}>
          体积(方)：
          <span className={styles.num}>{volumeOfS.toFixed(2)}</span>
          <span className={styles.p}>/</span>
          <span className={styles.num}>{volume.toFixed(2)}</span>
        </div>
      </div>
    );
  }
}
