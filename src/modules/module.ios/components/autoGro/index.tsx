import * as React from 'react';

import Alert from 'antd/es/alert';
import Col from 'antd/es/col';
import Form, { FormComponentProps } from 'antd/es/form';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import styles from './styles.module.less';

interface IAutoGroComponentProps extends FormComponentProps {
  autoGroLoading: boolean;
  defaultWarehouse: any;
  warehouseList: any[];
  onSubmit: (data: any) => void;
  onClose: () => void;
}

class AutoGroComponent extends React.PureComponent<
  IAutoGroComponentProps,
  any
> {
  onSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values);
      }
    });
  };

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 18 },
      },
    };

    const { getFieldDecorator } = this.props.form;
    const { warehouseList, defaultWarehouse } = this.props;

    return (
      <Modal
        title="自动配载"
        visible={true}
        width={520}
        onCancel={this.props.onClose}
        onOk={this.onSubmit}
        okText="继续分组"
        confirmLoading={this.props.autoGroLoading}
        centered={true}
        maskClosable={false}
        closable={!this.props.autoGroLoading}
        cancelButtonProps={{ disabled: this.props.autoGroLoading }}
      >
        <div className={styles.autoIOSConfirm}>
          <Alert
            message="当前订单均由该仓库装货发出，确认是否继续进行自动配载"
            type="info"
            showIcon
          />

          <Form onSubmit={this.onSubmit} className={styles.form}>
            <Row gutter={16}>
              <Col xs={24}>
                <Form.Item {...formItemLayout} label="仓库名">
                  {getFieldDecorator('warehouseId', {
                    initialValue: defaultWarehouse.utmsDepotId,
                    rules: [
                      {
                        required: true,
                        message: '请选择默认仓库',
                      },
                    ],
                  })(
                    <Select placeholder="默认仓库">
                      {warehouseList.map(item => (
                        <Select.Option
                          value={item.utmsDepotId}
                          key={item.utmsDepotId}
                        >
                          {item.depotName}
                        </Select.Option>
                      ))}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </div>
      </Modal>
    );
  }
}

export let AutoGro = Form.create()(AutoGroComponent);
