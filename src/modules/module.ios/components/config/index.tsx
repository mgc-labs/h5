import * as React from 'react';

import Col from 'antd/es/col';
import Form, { FormComponentProps } from 'antd/es/form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import CarModelCarLong from 'components/CarModelCarLong';
import { checkNumberForm } from 'utils/checkString';
import { Span } from '../span';
import styles from './styles.module.less';

export interface IConfigComponentProps extends FormComponentProps {
  data: any;
  saveConfigLoading: boolean;
  onSubmit: (values: any) => void;
  onClose: () => void;
}

export class ConfigComponent extends React.PureComponent<
  IConfigComponentProps,
  any
> {
  constructor(props: IConfigComponentProps, context: any) {
    super(props, context);
  }
  componentDidMount() {
    const {
      form: { setFieldsValue },
      data,
    } = this.props;

    data.carStructName = data.carStruct;
    setFieldsValue({
      ...data,
      carStruct: [{ value: data.carStruct }],
    });
  }

  // isChanged = (keys: string[]) => {
  //   const {
  //     form: { getFieldValue },
  //     data,
  //   } = this.props;
  //   return keys.filter(k => data[k] !== getFieldValue(k)).length > 0;
  // };

  onClose = () => {
    const { onClose } = this.props;
    // if (this.isChanged(['carStructName', 'loadRate', 'maxOrderNo'])) {
    //   Modal.confirm({
    //     title: '有未保存的信息',
    //     content: <p>如放弃，填写的信息将丢失</p>,
    //     okText: '继续填写',
    //     cancelText: '放弃',
    //     onCancel: () => {
    //       onClose();
    //     },
    //   });
    // } else {
    //   onClose();
    // }

    onClose();
  };

  onCarModelCarLongChanged = v => {
    if (v) {
      const { setFieldsValue } = this.props.form;
      const value = v[0];

      if (value) {
        setFieldsValue({
          carStructName: value.value,
          carLong: value.vehicleLong,
          maxVolume: value.vehicleVolume,
          maxWeight: value.vehicleLoad,
        });
      } else {
        setFieldsValue({
          carStruct: '',
          carLong: '',
          maxVolume: '',
          maxWeight: '',
        });
      }
    }
  };

  max = (value1, max) => {
    if (value1 > max) {
      return max;
    }
    return value1;
  };

  onSubmit = () => {
    const {
      form: { getFieldValue, validateFields },
      data,
      onSubmit,
    } = this.props;

    validateFields((err, values) => {
      if (!err) {
        // maxVolume 最大体积，单位立方
        // maxWeight 最大重量，单位千克
        const result = {
          utmsGroupConfigId: data.utmsGroupConfigId,
          carStruct: getFieldValue('carStructName'),
          carLong: getFieldValue('carLong'),
          maxVolume: values.maxVolume,
          maxWeight: values.maxWeight,
          loadRate: this.max(values.loadRate, 100),
          maxOrderNo: this.max(values.maxOrderNo, 30),
        };
        if (result.loadRate === '') {
          result.loadRate = 100;
        }
        if (result.maxOrderNo === '') {
          result.maxOrderNo = 30;
        }

        onSubmit(result);
      }
    });
  };

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 18 },
      },
    };
    const {
      form: { getFieldDecorator },
      saveConfigLoading,
    } = this.props;

    return (
      <div>
        <Modal
          title="配载设置"
          visible={true}
          width={520}
          onCancel={this.onClose}
          onOk={this.onSubmit}
          confirmLoading={saveConfigLoading}
          closable={!saveConfigLoading}
          cancelButtonProps={{ disabled: saveConfigLoading }}
          centered={true}
          maskClosable={false}
        >
          <div className={styles.config}>
            <Form>
              <Row gutter={16}>
                <Col xs={24}>
                  {getFieldDecorator('carStructName')(<Input type="hidden" />)}
                  {getFieldDecorator('carLong')(<Input type="hidden" />)}
                  <Form.Item {...formItemLayout} label="车型/车长">
                    {getFieldDecorator('carStruct', {
                      rules: [
                        {
                          required: true,
                          message: '请选择车型/车长',
                        },
                      ],
                    })(
                      <CarModelCarLong
                        placeholder="请选择车型/车长"
                        onChange={this.onCarModelCarLongChanged}
                        maxSelect={1}
                        style={{ marginTop: '6px' }}
                      />,
                    )}
                  </Form.Item>
                  <Row gutter={16} className={styles.rowinfo}>
                    <Col xs={6}>
                      <div className={styles.title}>载重质量:</div>
                    </Col>
                    <Col xs={5}>
                      <div>{getFieldDecorator('maxVolume')(<Span />)}方</div>
                    </Col>
                    <Col xs={5}>
                      <div className={styles.title}>总重量:</div>
                    </Col>
                    <Col xs={8}>
                      <div>
                        {getFieldDecorator('maxWeight')(<Span />)}
                        千克
                      </div>
                    </Col>
                  </Row>
                  <div className={styles.divQuality} />
                  <Form.Item {...formItemLayout} label="装载率">
                    {getFieldDecorator('loadRate', {
                      initialValue: '',
                      rules: [
                        {
                          validator: checkNumberForm(
                            '请输入1至100间的浮点数，最多2位小数',
                            {
                              max: 100,
                              min: 1,
                              decimal: 2,
                            },
                          ),
                        },
                      ],
                    })(<Input placeholder="请输入装载率" addonAfter="%" />)}
                  </Form.Item>
                  <Form.Item {...formItemLayout} label="每车配送订单数">
                    {getFieldDecorator('maxOrderNo', {
                      initialValue: '',
                      rules: [
                        {
                          validator: checkNumberForm('请输入1至30间的整数', {
                            max: 30,
                            min: 1,
                            decimal: 2,
                          }),
                        },
                      ],
                    })(<Input placeholder="请输入30以内订单数" />)}
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </div>
        </Modal>
      </div>
    );
  }
}

export const Config = Form.create()(ConfigComponent);
