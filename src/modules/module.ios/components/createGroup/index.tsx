import * as React from 'react';

import Alert from 'antd/es/alert';
import Col from 'antd/es/col';
import Form, { FormComponentProps } from 'antd/es/form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import { InputBox } from 'components/InputBox';
import { ImmutableListMap } from 'utils/types';
import styles from './styles.module.less';

interface ICreateGroupComponentProps extends FormComponentProps {
  selectedOrderList: any[];
  recVehicleList: ImmutableListMap;
  defaultWarehouse: any;
  warehouseList: any[];
  onSubmit: (data: any) => void;
  onClose: () => void;
  createGroupLoading: boolean;
  selectedOrderIdList: string[]; // 地图上的点
}

class CreateGroupComponent extends React.PureComponent<
  ICreateGroupComponentProps,
  any
> {
  onSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit({
          ...values,
          partOrderIds: this.props.selectedOrderIdList,
        });
      }
    });
  };
  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 18 },
      },
    };

    const { getFieldDecorator } = this.props.form;
    const {
      createGroupLoading,
      defaultWarehouse,
      warehouseList,
      selectedOrderList,
      recVehicleList,
    } = this.props;

    const orderCount = selectedOrderList.length;
    let totalCount = 0;
    let totalVolume = 0;
    let totalWeight = 0;

    selectedOrderList.map(h => {
      totalCount += h.goodsNumber;
      totalVolume += h.goodsVolume;
      totalWeight += h.goodsWeight;
    });

    let recVehicle = '';
    let recVehicleError = false;
    const recVehicleListJson = recVehicleList.toJS();
    for (const h of recVehicleListJson) {
      if (totalVolume <= h.vehicleVolume) {
        recVehicle = `${h.vehicleLong / 1000}米${h.vehicleType}`;
        break;
      }
    }
    if (!recVehicle) {
      recVehicle = '显示超载，不允许添加分组';
      recVehicleError = true;
    }

    return (
      <Modal
        title="创建订单组"
        visible={true}
        width={520}
        onCancel={this.props.onClose}
        onOk={this.onSubmit}
        confirmLoading={createGroupLoading}
        centered={true}
        maskClosable={false}
        closable={!createGroupLoading}
        cancelButtonProps={{ disabled: createGroupLoading }}
        okButtonProps={{ disabled: recVehicleError }}
      >
        <div className={styles.createGroup}>
          {recVehicleError ? (
            <Alert message={`${recVehicle}`} type="error" showIcon />
          ) : (
            <Alert message={`建议车型：${recVehicle}`} type="info" showIcon />
          )}

          <Form onSubmit={this.onSubmit} className={styles.form}>
            <Row gutter={16} className={styles.datainfo}>
              <Col xs={6}>
                <div className={styles.title}>订单数:</div>
              </Col>
              <Col xs={5}>
                <div>{orderCount}个</div>
              </Col>
              <Col xs={4}>
                <div className={styles.title}>总件数:</div>
              </Col>
              <Col xs={9}>
                <div>{totalCount}件</div>
              </Col>

              <Col xs={6}>
                <div className={styles.title}>总体积:</div>
              </Col>
              <Col xs={5}>
                <div>{totalVolume.toFixed(2)}方</div>
              </Col>
              <Col xs={4}>
                <div className={styles.title}>总质量:</div>
              </Col>
              <Col xs={9}>
                <div>
                  {totalWeight.toFixed(2)}
                  千克
                </div>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col xs={24}>
                <Form.Item {...formItemLayout} label="仓库名">
                  {getFieldDecorator('warehouseId', {
                    initialValue: defaultWarehouse.utmsDepotId,
                    rules: [
                      {
                        required: true,
                        message: '请选择默认仓库',
                      },
                    ],
                  })(
                    <Select placeholder="默认仓库">
                      {warehouseList.map(item => (
                        <Select.Option
                          value={item.utmsDepotId}
                          key={item.utmsDepotId}
                        >
                          {item.depotName}
                        </Select.Option>
                      ))}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col xs={24}>
                <Form.Item {...formItemLayout} label="分组名称">
                  {getFieldDecorator('name', {
                    initialValue: '',
                    rules: [
                      {
                        required: true,
                        message: '请输入分组名称',
                      },
                    ],
                  })(
                    <InputBox
                      placeholder="请输入分组名称"
                      autoComplete="off"
                      maxLength={50}
                    />,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </div>
      </Modal>
    );
  }
}

export let CreateGroup = Form.create()(CreateGroupComponent);
