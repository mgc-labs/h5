import * as React from 'react';

import Spin from 'antd/es/spin';
import { ImmutableListMap } from 'utils/types';
import { MapComponent } from '../map';
import { MapDataInfo } from '../mapDataInfo';
import { MapOrderGroup } from '../mapOrderGroup';
import { MapOrderSort } from '../mapOrderSort';
import { MapToolBar } from '../mapToolsBar';
import styles from './styles.module.less';

interface ILocProps {
  autoGroLoading: boolean;
  isShowAutoGroConfirm: boolean;
  recVehicleList: ImmutableListMap;
  defaultWarehouse: any;
  warehouseList: any[];
  orderGroupList: any[];
  orderGroupLoading: boolean;
  orderGroupShow: boolean;
  selectedGroupId: string;
  checkedGroupIdList: string[];
  isShowOrderListOfGroup: boolean;
  getOrderListLoading: boolean;
  orderListOfGroup: any[];
  locOrderList: any[]; // 已定位订单，包括未分组，已分组
  locOrderListLoading: boolean;
  locOrderListSign: string;
  orderGroupSendLoading: boolean;
  deleteGroupLoading: boolean;
  deleteOrderLoading: boolean;
  moveOrderLoading: boolean;
  onGroupShow: (show: boolean) => void;
  onGroupCheck: (record: any) => void;
  onGroupAllCheck: (checked: boolean) => void;
  onOrderCheck: (record: any) => void;
  onOrderAllCheck: (checked: boolean) => void;
  onOrderListHide: () => void;
  onGroupSelected: (record: any) => void;
  onOrderReOrder: (pars: any) => void;
  onDeleteOrderFromGroup: (data: any) => void;
  onMoveOrderToGroup: (data: any) => void;
  onDeleteGroup: (data: any) => void;
  onAutoGro: (data) => void;
  onAutoGroOpen: () => void;
  onAutoGroClose: () => void;

  isShowCreateGroup: boolean;
  createGroupLoading: boolean;
  onCreateGroupOpen: () => void;
  onCreateGroup: (data) => void;
  onCreateGroupClose: () => void;

  isShowAddToGroup: boolean;
  addToGroupLoading: boolean;
  onAddToGroupOpen: () => void;
  onAddToGroup: (data) => void;
  onAddToGroupClose: () => void;

  onSelectPoints: (selectedPoints) => void;

  onSend: (data) => void;
  onGotoCreateWarehouse: () => void;
}

export class LocComponent extends React.PureComponent<ILocProps, any> {
  private mapComponent: MapComponent;
  constructor(props: ILocProps, context: any) {
    super(props, context);
  }

  onOpenRectSelect = (isOpen: boolean) => {
    this.mapComponent.openRectSelect(isOpen);
  };

  onSelectNull = () => {
    this.mapComponent.selectNull();
  };

  render() {
    const props = this.props;
    // 过滤出未分组的订单
    const ungroupdOrderList = props.locOrderList.filter(h => h.status === 0);
    // 选中中订单
    const selectedOrderList = ungroupdOrderList.filter(h => h.isSelected);

    return (
      <Spin
        tip="加载中..."
        spinning={
          props.locOrderListLoading ||
          props.autoGroLoading ||
          props.deleteGroupLoading ||
          props.orderGroupSendLoading ||
          props.deleteOrderLoading ||
          props.moveOrderLoading
        }
      >
        <div className={styles.locOrders}>
          <div className={styles.toolBarLine}>
            <MapToolBar
              isShowAutoGroConfirm={props.isShowAutoGroConfirm}
              autoGroLoading={props.autoGroLoading}
              onGroupShow={props.onGroupShow}
              orderGroupShow={props.orderGroupShow}
              orderGroupList={props.orderGroupList}
              defaultWarehouse={props.defaultWarehouse}
              warehouseList={props.warehouseList}
              onAutoGro={props.onAutoGro}
              onAutoGroOpen={props.onAutoGroOpen}
              onAutoGroClose={props.onAutoGroClose}
              isShowCreateGroup={props.isShowCreateGroup}
              createGroupLoading={props.createGroupLoading}
              onCreateGroupOpen={props.onCreateGroupOpen}
              onCreateGroup={props.onCreateGroup}
              onCreateGroupClose={props.onCreateGroupClose}
              isShowAddToGroup={props.isShowAddToGroup}
              addToGroupLoading={props.addToGroupLoading}
              onAddToGroupOpen={props.onAddToGroupOpen}
              onAddToGroup={props.onAddToGroup}
              onAddToGroupClose={props.onAddToGroupClose}
              ungroupdOrderList={ungroupdOrderList}
              selectedOrderList={selectedOrderList}
              recVehicleList={props.recVehicleList}
              onOpenRectSelect={this.onOpenRectSelect}
              onSelectNull={this.onSelectNull}
            />

            <MapDataInfo
              locOrderList={props.locOrderList}
              selectedOrderList={selectedOrderList}
            />
          </div>
          <div className={styles.groupAndOrder}>
            {props.orderGroupShow && (
              <MapOrderGroup
                orderGroupList={props.orderGroupList}
                orderGroupLoading={props.orderGroupLoading}
                selectedGroupId={props.selectedGroupId}
                onGroupCheck={props.onGroupCheck} // 单行选择
                onGroupAllCheck={props.onGroupAllCheck} // 全选
                onSelected={props.onGroupSelected} // 选中一个组时
                checkedGroupIdList={props.checkedGroupIdList}
                onDelete={props.onDeleteGroup}
                onSend={props.onSend}
              />
            )}
            {props.isShowOrderListOfGroup && (
              <MapOrderSort
                isEditing={true}
                getOrderListLoading={props.getOrderListLoading}
                orderGroupList={props.orderGroupList}
                selectedGroupId={props.selectedGroupId}
                orderListOfGroup={props.orderListOfGroup}
                onOrderCheck={props.onOrderCheck} // 单行选择
                onOrderAllCheck={props.onOrderAllCheck} // 全选
                onOrderReOrder={props.onOrderReOrder}
                onDelete={props.onDeleteOrderFromGroup}
                onMoveToGroup={props.onMoveOrderToGroup}
                onHide={props.onOrderListHide}
              />
            )}
          </div>

          <MapComponent
            ref={obj => (this.mapComponent = obj)}
            defaultWarehouse={props.defaultWarehouse}
            locOrderList={props.locOrderList}
            locOrderListSign={props.locOrderListSign}
            onSelectPoints={props.onSelectPoints}
          />
        </div>
      </Spin>
    );
  }
}
