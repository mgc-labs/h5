/**
 * LtlOrder Component
 * @author djd
 * @date 2018/9/13 下午1:48:50
 */
import * as React from 'react';

import Icon from 'antd/es/icon';
import Popconfirm from 'antd/es/popconfirm';
import MapPointPicker from 'components/MapPointPicker';
import MyTable from 'components/MyTable';
import { OptionButtons } from 'components/OptionButtons';
import debounce from 'lodash/debounce';
import { ImmutableMap } from 'utils/types';
import styles from './styles.module.less';

export interface IUnLocListProps {
  unLocOrderData: ImmutableMap;
  unLocOrderListLoading: boolean;
  defaultWarehouse: any;
  permissions: any;
  onGetUnLocOrderList: (pars: any) => void;
  onGotoEditOrder: (data) => void;
  onGotoDetail: (data) => void;
  onDeleteOrder: (data) => void;
  onUpdateOrderLocation: (data) => void;
}

interface IState {
  current: number;
  isOpenMapPointPicker: boolean;
  selectedRecord: any;
  tableScrollHeight: number;
}

export class UnLocList extends React.PureComponent<IUnLocListProps> {
  public state: IState;
  private divFrame: HTMLDivElement;
  private resize: () => void;
  constructor(props: IUnLocListProps, context: any) {
    super(props, context);

    this.state = {
      current: 1,
      isOpenMapPointPicker: false,
      selectedRecord: null,
      tableScrollHeight: 0,
    };

    this.resize = debounce(() => {
      if (this.divFrame) {
        this.setState({
          tableScrollHeight: this.divFrame.offsetHeight - 45 - 80,
        });
      }
    }, 100);

    window.addEventListener('resize', this.resize);
  }
  componentDidMount() {
    this.resize();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.resize);
  }

  fetchData = (pars: any) => {
    this.props.onGetUnLocOrderList(pars);

    this.setState({
      current: (pars ? pars.current : 1) || 1,
    });
  };

  onOpenMapPointPicker = (record: any) => {
    this.setState({
      isOpenMapPointPicker: true,
      selectedRecord: record,
    });
  };

  onMapPointPicked = (pars: any) => {
    const { lat, lng, province, city, region, address } = pars;
    const { utmsPartOrderId } = this.state.selectedRecord;
    this.props.onUpdateOrderLocation({
      utmsPartOrderId,
      province: `${province}${city}${region}`,
      address,
      latitude: lat,
      longitude: lng,
    });
  };
  onMapPointClose = () => {
    this.setState({
      isOpenMapPointPicker: false,
    });
  };

  render() {
    const {
      unLocOrderData,
      unLocOrderListLoading,
      permissions,
      onGotoEditOrder,
      onDeleteOrder,
      defaultWarehouse,
    } = this.props;
    const {
      current,
      isOpenMapPointPicker,
      selectedRecord,
      tableScrollHeight,
    } = this.state;

    const columns = [
      {
        title: '第三方订单号',
        dataIndex: 'thirdSystemId',
        width: 150,
        fixed: 'left',
      },
      {
        title: '订单号',
        width: 225,
        fixed: 'left',
        render: (record: any) => (
          <a
            href="javascript:;"
            onClick={() => this.props.onGotoDetail(record.utmsPartOrderId)}
          >
            {record.orderNumber}
          </a>
        ),
      },
      {
        title: '发布时间',
        dataIndex: 'gmtCreate',
        width: 185,
      },
      {
        title: '要求到达时间',
        width: 185,
        dataIndex: 'requireDate',
      },
      {
        title: '收货人',
        width: 80,
        dataIndex: 'contact',
      },
      {
        title: '联系电话',
        width: 135,
        dataIndex: 'phone',
      },
      {
        title: '省市区',
        width: 180,
        dataIndex: 'province',
      },
      {
        title: '收货地址',
        dataIndex: 'address',
      },
      {
        title: '操作',
        key: 'operation',
        fixed: 'right',
        width: 130,
        render: (record: any) => (
          <span>
            <OptionButtons>
              <a
                href="javascript:;"
                onClick={() => this.onOpenMapPointPicker(record)}
              >
                位置修正
              </a>
              {permissions.editOrder && (
                <React.Fragment>
                  <a
                    href="javascript:;"
                    onClick={() => onGotoEditOrder(record.utmsPartOrderId)}
                  >
                    修改
                  </a>
                </React.Fragment>
              )}

              <Popconfirm
                title="确认删除"
                icon={<Icon type="question-circle" theme="outlined" />}
                onConfirm={() => onDeleteOrder([record.utmsPartOrderId])}
              >
                <a href="javascript:;">删除</a>
              </Popconfirm>
            </OptionButtons>
          </span>
        ),
      },
    ];

    let width = 250;
    columns.map(col => {
      width += col.width || 0;
    });

    const list = unLocOrderData.get('data').toJS();

    return (
      <div className={styles.unloc} ref={obj => (this.divFrame = obj)}>
        <MyTable
          columns={columns}
          rowKey="utmsPartOrderId"
          dataSource={list}
          scroll={{ x: width, y: tableScrollHeight }}
          loading={unLocOrderListLoading}
          pagination={{
            className: styles.pagination,
            total: unLocOrderData.get('count'),
            onChange: this.fetchData,
            onShowSizeChange: this.fetchData,
            current,
          }}
        />

        {isOpenMapPointPicker && (
          <MapPointPicker
            onOk={this.onMapPointPicked}
            onClose={this.onMapPointClose}
            currentAddress={{
              city: defaultWarehouse.city,
              address: selectedRecord.address,
              province: defaultWarehouse.province,
              region: defaultWarehouse.region,
            }}
          />
        )}
      </div>
    );
  }
}
