declare const BMAP_POINT_SIZE_BIGGER: number;
declare const BMAP_POINT_SIZE_NORMAL: number;
declare const BMAP_POINT_SIZE_HUGE: number;

export const MaxSelectedGroupCount = 10; // 选中组数上限

export const ColorGroupedPoint = '#e427f5'; // 已分组的点颜色

export const ColorSerialNumber = '#ffffff'; // 序号颜色

export const ColorUnGroupdUnSelectedPoint = '#2f8af1'; // 未分组中未选中点
export const ColorUnGroupdSelectedPoint = '#004DBF'; // 未分组中已选中点

export const ColorCurrGroupUnSelectedPoint = '#63bf3e'; // 当前分组的未选中点颜色
export const ColorCurrGroupSelectedPoint = '#269A00'; // 当前分组的未选中点颜色

export const PointSizeUnGroupdOutside = BMAP_POINT_SIZE_BIGGER; // 点外圆
export const PointSizeUnGroupdInside = BMAP_POINT_SIZE_NORMAL; // 点内圆

export const PointSizeCurrGroupOutside = BMAP_POINT_SIZE_HUGE; // 点外圆
export const PointSizeCurrGroupInside = BMAP_POINT_SIZE_BIGGER; // 点内圆
