import * as React from 'react';

import { CustomInfoWindow } from './ctrls/customInfoWindow';
import { CustomPoint } from './ctrls/customPoint';
import { Graph } from './ctrls/graph';
import warehouseIcon from './img/warehouse.png';
import styles from './styles.module.less';
import { groupColor } from './util/groupdOrderColorful';

declare const BMap: any;
declare const BMapLib: any;
declare const BMAP_DRAWING_RECTANGLE: any;

interface IMapComponentProps {
  defaultWarehouse: any;
  locOrderList: any[];
  locOrderListSign: string;
  onSelectPoints: (selectedPointList: any[]) => void;
}

export class MapComponent extends React.PureComponent<IMapComponentProps, any> {
  private divMap: HTMLDivElement;
  private warehouseMarker: any;
  private map: any;
  private drawingManager: any;
  private customInfoWindow: CustomInfoWindow;
  private cpointList: CustomPoint[] = [];
  private haveSetViewport: boolean = false; // 是否已经有地图缩放到显示所有点

  componentDidMount() {
    this.initMap();
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const n = nextProps.defaultWarehouse;
    if (n && n.longitude) {
      this.paintWarehouse(n);
    }

    if (nextProps.locOrderListSign !== this.props.locOrderListSign) {
      this.mergePoints(nextProps.locOrderList);
    }
  }

  removeMapElement = (element: Element) => {
    if (element) {
      element.parentElement.removeChild(element);
    }
  };

  getMapHeight = () => {
    return this.divMap.offsetHeight;
  };

  initMap = () => {
    // setTimeout(() => {
    this.map = new BMap.Map(this.divMap, {
      minZoom: 1,
      maxZoom: 19,
      enableMapClick: false,
    });

    this.map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放

    // 去掉地图左下解的LOGO与文字
    this.map.addEventListener('tilesloaded', () => {
      this.removeMapElement(document.querySelector('.anchorBL'));
      this.removeMapElement(document.querySelector('.BMap_cpyCtrl'));
      this.addGraph();
    });

    this.map.centerAndZoom('杭州', 12);

    this.initSelectRect();
    // });
  };
  addGraph = () => {
    const graph = new Graph();
    this.map.addControl(graph);
  };

  paintWarehouse = (defaultWarehouse: any) => {
    if (this.map) {
      if (this.warehouseMarker) {
        return;
      }
      const icon = new BMap.Icon(warehouseIcon, new BMap.Size(20, 30), {
        anchor: new BMap.Size(10, 30),
      });

      const point = new BMap.Point(
        defaultWarehouse.longitude,
        defaultWarehouse.latitude,
      );
      this.warehouseMarker = new BMap.Marker(point, {
        icon,
        enableClicking: false,
      });

      this.map.addOverlay(this.warehouseMarker);

      this.map.centerAndZoom(point, 12);
    }
    // this.map.setViewport(groupPointList);
  };

  onShowInfo = data => {
    if (!this.customInfoWindow) {
      this.customInfoWindow = new CustomInfoWindow();
      this.customInfoWindow.setPosition(
        new BMap.Point(data.longitude, data.latitude),
      );
      this.map.addOverlay(this.customInfoWindow);
    } else {
      this.customInfoWindow.setPosition(
        new BMap.Point(data.longitude, data.latitude),
      );
    }
    this.customInfoWindow.setData(data);
    this.customInfoWindow.show();
  };
  onHideInfo = data => {
    this.customInfoWindow.hide();
  };

  mergePoints(locOrderList: any[]) {
    const groupdOrderList = locOrderList.filter(h => h.status === 1);
    groupColor(groupdOrderList);
    const locOrderListOrderIdList: string[] = locOrderList.map(
      item => item.utmsPartOrderId,
    );
    const cpointDataList: any[] = this.cpointList.map(item => item.data);
    const cpointOrderIdList: string[] = cpointDataList.map(
      item => item.utmsPartOrderId,
    );
    const addPointList = locOrderList.filter(
      h => cpointOrderIdList.indexOf(h.utmsPartOrderId) === -1,
    );
    const delPointList = cpointDataList.filter(
      h => locOrderListOrderIdList.indexOf(h.utmsPartOrderId) === -1,
    );
    const modPointList = locOrderList.filter(
      h1 =>
        cpointDataList.findIndex(
          h2 =>
            h2.utmsPartOrderId === h1.utmsPartOrderId &&
            (h2.isSelected !== h1.isSelected ||
              h2.status !== h1.status ||
              h2.no !== h1.no ||
              h2.latitude !== h1.latitude ||
              h2.longitude !== h1.longitude),
        ) > -1,
    );
    modPointList.map(item => {
      const index = cpointDataList.findIndex(
        h => h.utmsPartOrderId === item.utmsPartOrderId,
      );

      const cp = this.cpointList[index];
      cp.setData(item);
      cp.draw();
    });
    delPointList.map(item => {
      const index = this.cpointList.findIndex(
        h => h.data.utmsPartOrderId === item.utmsPartOrderId,
      );
      const cp = this.cpointList.splice(index, 1)[0];
      if (cp) {
        this.map.removeOverlay(cp);
        cp.destroy();
      }
    });
    // addPointList.map(item => {
    //   const cp = new CustomPoint({
    //     data: item,
    //     onClick: this.onCPClick,
    //   });
    //   this.map.addOverlay(cp);
    //   this.cpointList.push(cp);
    // });

    this.addCPs(addPointList)
      .then(() => {
        this.setViewport();
      })
      .catch();
  }

  addCP = (adds: any[]) => {
    return new Promise((resolve, reject) => {
      requestAnimationFrame(() => {
        for (const item of adds) {
          const cp = new CustomPoint({
            data: item,
            onClick: this.onCPClick,
            onShowInfo: this.onShowInfo,
            onHideInfo: this.onHideInfo,
          });
          this.map.addOverlay(cp);
          this.cpointList.push(cp);
        }
        resolve();
      });
    });
  };

  addCPs = async (adds: any[]) => {
    const step = 1000;
    for (let i = 0; i < adds.length; i += step) {
      await this.addCP(adds.slice(i, i + step));
    }
  };

  onCPClick = (cpoint: CustomPoint) => {
    const utmsPartOrderId = cpoint.data.utmsPartOrderId;
    const list = this.props.locOrderList.map(h => {
      if (h.utmsPartOrderId === utmsPartOrderId) {
        return {
          ...h,
          isSelected: h.status === 0 ? !h.isSelected : false,
        };
      } else {
        return h;
      }
    });
    this.props.onSelectPoints(list);
  };

  setViewport = () => {
    if (!this.haveSetViewport) {
      const pointList = this.cpointList.map(h => h.point);
      this.map.setViewport(pointList, {
        margins: [62, 0, 12, 0],
        enableAnimation: true,
      });
      this.haveSetViewport = true;
    }
  };

  // 初始化框选功能
  initSelectRect() {
    // 框选
    const styleOptions = {
      strokeColor: 'red', // 边线颜色。
      fillColor: 'red', // 填充颜色。当参数为空时，圆形将没有填充效果。
      fillOpacity: 0.3, // 填充的透明度，取值范围0 - 1。
      strokeWeight: 1, // 边线的宽度，以像素为单位。
      strokeOpacity: 0.8, // 边线透明度，取值范围0 - 1。
      strokeStyle: 'solid', // 边线的样式，solid或dashed。
    };
    // 实例化鼠标绘制工具
    this.drawingManager = new BMapLib.DrawingManager(this.map, {
      // isOpen: true, // 是否开启绘制模式
      drawingMode: BMAP_DRAWING_RECTANGLE,
      enableCalculate: false,
      enableDrawingTool: false, // 是否显示工具栏
      rectangleOptions: styleOptions, // 矩形的样式
    });
    // 当框选完成时回调
    this.drawingManager.addEventListener('rectanglecomplete', overlay => {
      // const overlayRect = overlay;
      this.map.removeOverlay(overlay);

      const bounds = overlay.getBounds();

      const ap1 = bounds.getSouthWest();
      const ap2 = bounds.getNorthEast();

      const addSelectedPointList = [];

      this.cpointList.map(cp => {
        if (cp.data.status === 0) {
          const pLng = cp.data.longitude % 180;
          const pLat = cp.data.latitude;

          if (
            pLng >= ap1.lng &&
            pLng <= ap2.lng &&
            pLat >= ap1.lat &&
            pLat <= ap2.lat
          ) {
            addSelectedPointList.push(cp.data.utmsPartOrderId);
          }
        }
      });

      if (addSelectedPointList.length) {
        const list = this.props.locOrderList.map(h => {
          if (addSelectedPointList.indexOf(h.utmsPartOrderId) > -1) {
            return {
              ...h,
              isSelected: h.status === 0 ? true : false,
            };
          } else {
            return h;
          }
        });

        this.props.onSelectPoints(list);
      }
    });
  }

  openRectSelect(isOpen: boolean) {
    if (isOpen) {
      this.drawingManager.open();
    } else {
      this.drawingManager.close();
    }
  }

  selectNull() {
    const list = this.props.locOrderList.map(h => ({
      ...h,
      isSelected: false,
    }));
    this.props.onSelectPoints(list);
  }

  render() {
    return <div ref={obj => (this.divMap = obj)} className={styles.map} />;
  }
}
