// 'use strict';

// export function QuickGCP(data, skipColors, light, minDis) {
//   const skipColor = skipColors.length ? skipColors : ['#ffffff', '#000000'];
//   const lights = light.length ? light : [0.2, 0.9];
//   const skipColorRGB = skipColor.map(h => Hex2Rgb(h));
//   return getColor(data, skipColorRGB, lights, minDis);
// }
// function distance_par_estimation(pars) {
//   const length = pars.length;
//   let d = 40;
//   if (length < d) {
//     d = length;
//   }
//   let i = 0;
//   for (let j = 0; j < d; j++) {
//     let n = 0;
//     const k = Math.floor(Math.random() * length);
//     const l = pars[k].points;
//     const m = l.length;
//     const o = [l[0].lat, l[0].lat, l[0].lng, l[0].lng];
//     for (let p = 1; p < m; p++) {
//       const q = l[p];
//       if (q.lat < o[0]) {
//         o[0] = q.lat;
//       } else if (q.lat > o[1]) {
//         o[1] = q.lat;
//       }
//       if (q.lng < o[2]) {
//         o[2] = q.lng;
//       } else if (q.lng > o[3]) {
//         o[3] = q.lng;
//       }
//     }
//     n = o[1] - o[0] > o[3] - o[2] ? o[1] - o[0] : o[3] - o[2];
//     i += n;
//   }
//   return (i /= d);
// }
// function getColor(data, skipColor, lights, minDis) {
//   const l = [];
//   if (-1 === minDis) {
//     minDis = 0.2 * distance_par_estimation(data);
//   }
//   const neighbor = neighbor_matrix2(data, minDis);

//   const skipColorHSL = skipColor.map(h => rgb2hsl(h));

//   for (let k = 0; k < data.length; k++) {
//     const m = neighbor[k];
//     for (let n = 0; n < k; n++) {
//       if (1 === m[n]) {
//         skipColorHSL.push(rgb2hsl(l[n]));
//       }
//     }
//     l.push(select_colour_hsl(skipColorHSL, lights[0], lights[1]));
//   }
//   const result: any = {};
//   l.map((h, index) => {
//     result[data[index].groupId] = `#${Rgb2Hex(h[0], h[1], h[2])}`;
//   });

//   return result;
// }
// function get_min_distance(points1, points2) {
//   let minDis = 1e7;
//   points1.map(p1 => {
//     points2.map(p2 => {
//       const dis = Math.sqrt(
//         Math.pow(p1.lat - p2.lat, 2) + Math.pow(p1.lng - p2.lng, 2),
//       );
//       if (dis < minDis) {
//         minDis = dis;
//       }
//     });
//   });
//   return minDis;
// }
// function neighbor_matrix2(pars, pars1) {
//   const length = pars.length;
//   const result = pars.map(h => new Array(length));

//   for (let i = 0; i < length; i++) {
//     for (let j = i + 1; j < length; j++) {
//       const minDis = get_min_distance(pars[i].points, pars[j].points);
//       if (pars1 > minDis) {
//         result[i][j] = 1;
//         result[j][i] = 1;
//       }
//     }
//   }
//   return result;
// }
// function rgb2hsl(pars) {
//   const p1 = pars.map(h => h / 255);
//   const p2 = Math.max(...p1);
//   const p3 = Math.min(...p1);
//   const p4 = p2 - p3;
//   const p5 = (p2 + p3) / 2;
//   const p6 = 0 === p4 ? 0 : p5 < 0.5 ? p4 / (p2 + p3) : p4 / (2 - p2 - p3);
//   let p7 = 0;

//   if (0 !== p4) {
//     if (p1[0] === p2) {
//       p7 = (p1[1] - p1[2]) / p4;
//     } else if (p1[1] === p2) {
//       p7 = 2 + (p1[2] - p1[0]) / p4;
//     } else if (p1[2] === p2) {
//       p7 = 4 + (p1[0] - p1[1]) / p4;
//     }
//   }

//   p7 = (p7 / 6) * 360;
//   if (p7 < 0) {
//     p7 += 360;
//   }

//   return [p7, p6, p5];
// }
// function hsl2rgb_f1(pars) {
//   if (pars < 0) {
//     return pars + 1;
//   } else if (pars > 1) {
//     return pars - 1;
//   } else {
//     return pars;
//   }
// }
// function hsl2rgb_f2(pars1, pars2, pars3) {
//   let p10 = 0;
//   if (pars3 < 1 / 6) {
//     p10 = pars1 + 6 * (pars2 - pars1) * pars3;
//   } else if (pars3 < 0.5 && pars3 >= 1 / 6) {
//     p10 = pars2;
//   } else if (pars3 < 2 / 3 && pars3 >= 0.5) {
//     p10 = pars1 + 6 * (pars2 - pars1) * (2 / 3 - pars3);
//   } else {
//     p10 = pars1;
//   }
//   return Math.floor(255 * p10);
// }
// function hsl2rgb(fpars) {
//   const p1 = fpars[0];
//   const p2 = fpars[1];
//   const p3 = fpars[2];
//   const p4 = p3 < 0.5 ? p3 * (1 + p2) : p3 + p2 - p3 * p2;
//   const p5 = 2 * p3 - p4;
//   const p6 = p1 / 360;
//   const p7 = hsl2rgb_f1(p6 + 1 / 3);
//   const p8 = hsl2rgb_f1(p6);
//   const p9 = hsl2rgb_f1(p6 - 1 / 3);
//   return [
//     hsl2rgb_f2(p5, p4, p7),
//     hsl2rgb_f2(p5, p4, p8),
//     hsl2rgb_f2(p5, p4, p9),
//   ];
// }
// function zip() {
//   const p1 = [].slice.call(arguments);
//   return (0 === p1.length
//     ? []
//     : p1.reduce((h, p) => {
//         return h.length < p.length ? h : p;
//       })
//   ).map((h, i) => {
//     return p1.map(h1 => {
//       return h1[i];
//     });
//   });
// }
// function max_interval_middle(pars1, pars2, pars3) {
//   const p1 = [0, 0.6, pars2];
//   const p2 = [0, 0, 0];
//   const p3 = pars1.map(h => h[0]).sort(CMP);
//   const p4 = pars1.map(h => h[1]).sort(CMP);
//   const p5 = pars1.map(h => h[2]).sort(CMP);
//   const p6 = zip(p3, p4, p5);
//   for (let i = 0; i < p6.length - 1; i++) {
//     const h = p6[i];
//     const h1 = p6[i + 1];
//     if (h1[0] - h[0] > p2[0]) {
//       p2[0] = h1[0] - h[0];
//       p1[0] = (h1[0] + h[0]) / 2;
//     }
//     if (h1[1] - h[1] > p2[1] && (h1[1] + h[1]) / 2 > 0.6) {
//       p2[1] = h1[1] - h[1];
//       p1[1] = (h1[1] + 5 * h[1]) / 6;
//     }
//     if (
//       h1[2] - h[2] > p2[2] &&
//       (3 * h1[2] + h[2]) / 4 > pars2 &&
//       (3 * h1[2] + h[2]) / 4 < pars3
//     ) {
//       p2[2] = h1[2] - h[2];
//       p1[2] = (3 * h1[2] + h[2]) / 4;
//     }
//   }
//   return [p1[0], p1[1], p1[2]];
// }
// function select_colour_hsl(skipColorHSL, lightMin, lightMax) {
//   if (0 === skipColorHSL.length) {
//     return 255;
//   }
//   const p = skipColorHSL.concat([[0, 0, lightMin], [360, 1, lightMax]]);
//   return hsl2rgb(max_interval_middle(p, lightMin, lightMax));
// }
// function Rgb2Hex(pars1: number, pars2: number, pars3: number) {
//   const color = ((pars1 << 16) | (pars2 << 8) | pars3).toString(16);
//   return ('00000' + color).slice(-6);
// }
// function CMP(pars1, pars2) {
//   return pars1 > pars2 ? 1 : pars1 < pars2 ? -1 : 0;
// }
// function Hex2Rgb(pars) {
//   if (/#(..)(..)(..)/g.test(pars)) {
//     return [
//       parseInt(RegExp.$1, 16),
//       parseInt(RegExp.$2, 16),
//       parseInt(RegExp.$3, 16),
//     ];
//   }
//   return [0, 0, 0];
// }
