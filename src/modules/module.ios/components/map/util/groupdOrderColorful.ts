// import { isEqual } from 'lodash';
// import {
//   ColorCurrGroupSelectedPoint,
//   ColorUnGroupdUnSelectedPoint,
// } from '../const';

// import { QuickGCP } from './quickGCP';

// let lastGroupPointList: [] = null;
// let lastResult = null;

// export function groupColor(groupPointList) {
//   if (isEqual(lastGroupPointList, groupPointList) && lastResult) {
//     // 如果入参数据相同，直接返回上次处理结果
//     return lastResult;
//   }

//   const groupPoints: any = {};

//   (groupPointList || []).map(gp => {
//     groupPoints[gp.no] = groupPoints[gp.no] || [];
//     groupPoints[gp.no].push({
//       lat: gp.latitude,
//       lng: gp.longitude,
//     });
//   });
//   const groupPointList2: any[] = [];

//   for (const k of Reflect.ownKeys(groupPoints)) {
//     groupPointList2.push({
//       groupId: Number(k),
//       points: groupPoints[k],
//     });
//   }

//   // let strIn = [
//   //   {
//   //     "groupId": 'a001', "points": [
//   //       { lat: 30.27624, lng: 120.15605 },
//   //       { lat: 30.27624, lng: 30.54491 }
//   //     ]
//   //   },
//   //   {
//   //     "groupId": 'a002', "points": [
//   //       { lat: 30.54491, lng: 120.49496 },
//   //       { lat: 30.54491, lng: 120.48452 }
//   //     ]
//   //   }];

//   // 入参1：数据，入参2：禁止颜色，入参3：[最低亮度,最高亮度]，入参4：判定两个集合的距离小于此值时为相邻，颜色应当不相同
//   lastResult = QuickGCP(
//     groupPointList2,
//     [ColorUnGroupdUnSelectedPoint, ColorCurrGroupSelectedPoint, '#ffffff'],
//     [],
//     0.1,
//   );

//   lastGroupPointList = groupPointList;

//   return lastResult;
// }

function getRandom() {
  const v = Math.ceil(Math.random() * 255).toString(16);
  return v.length < 2 ? '0' + v : v;
}

export function groupColor(groupPointList) {
  const groupHash: any = {};
  groupPointList.map(h => {
    if (!groupHash[h.no]) {
      groupHash[h.no] = `#${getRandom()}${getRandom()}${getRandom()}`;
    }
  });

  groupPointList.map(h => {
    h.color = groupHash[h.no];
  });
}
