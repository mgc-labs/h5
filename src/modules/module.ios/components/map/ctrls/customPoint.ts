import {
  ColorUnGroupdSelectedPoint,
  ColorUnGroupdUnSelectedPoint,
} from '../const';

import styles from './customPoint.module.less';

const pointWidth = 29;
const pointCenter = Math.ceil(pointWidth / 2);
declare const BMap: any;

export class CustomPoint extends BMap.Overlay {
  public data: any;
  constructor({ data, onClick, onShowInfo, onHideInfo }) {
    super();
    this.data = data;

    this.onClick = onClick;
    this.onShowInfo = onShowInfo;
    this.onHideInfo = onHideInfo;
  }

  initialize(map) {
    this.map = map;
    const div = document.createElement('div');
    div.className = styles.point;
    const divIn = document.createElement('div');
    divIn.className = styles.pointIn;

    div.appendChild(divIn);

    div.onclick = e => {
      if (this.onClick) {
        this.onClick(this);
      }
    };

    divIn.onmouseenter = () => {
      if (this.onShowInfo) {
        this.onShowInfo(this.data);
      }
    };

    divIn.onmouseout = () => {
      if (this.onHideInfo) {
        this.onHideInfo(this.data);
      }
    };

    this.map.getPanes().labelPane.appendChild(div);
    this.div = div;
    this.divIn = divIn;

    this.setData(this.data);

    return div;
  }

  setData(data) {
    this.data = data;
    this.point = new BMap.Point(data.longitude, data.latitude);
    const { status, color, isSelected, no } = data;
    this.div.style.backgroundColor =
      status === 0 ? ColorUnGroupdUnSelectedPoint : color;

    if (status === 0) {
      // 未分组
      this.div.style.cursor = 'pointer';
      if (isSelected) {
        this.div.style.zIndex = '3';
        this.divIn.style.backgroundColor = ColorUnGroupdSelectedPoint;
      } else {
        this.div.style.zIndex = '2';
        this.divIn.style.backgroundColor = 'transparent';
      }
      this.divIn.innerHTML = '';
      this.divIn.className = styles.pointIn;
    } else {
      // 已分组
      this.div.style.zIndex = '1';
      this.divIn.innerHTML = no;
      this.divIn.style.backgroundColor = 'transparent';
      this.divIn.className = styles.pointIn2;
    }
  }

  draw() {
    const pixel = this.map.pointToOverlayPixel(this.point);
    this.div.style.left = pixel.x - pointCenter + 'px';
    this.div.style.top = pixel.y - pointCenter + 'px';
  }

  destroy() {
    this.divIn.onmouseenter = null;
    this.divIn.onmouseout = null;
    this.divIn.innerHTML = '';
    this.div.onclick = null;
    this.div.innerHTML = '';

    this.divIn = null;
    this.div = null;

    this.data = null;
    this.point = null;
    this.onClick = null;
    this.onShowInfo = null;
    this.onHideInfo = null;
    this.map = null;
  }
}
