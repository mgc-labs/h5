import styles from './graph.module.less';

declare const BMap: any;
declare const BMAP_ANCHOR_BOTTOM_LEFT: any;

export class Graph extends BMap.Control {
  constructor() {
    super();
    this.defaultAnchor = BMAP_ANCHOR_BOTTOM_LEFT;
    this.defaultOffset = new BMap.Size(10, 10);
  }
  initialize(map) {
    const div = document.createElement('div');
    div.className = styles.graph;

    div.innerHTML = `
    <div class="${styles.legend}">
      <div class="${styles.legendImg}"></div>
      <div class="${styles.title}">未分组</div>
    </div>
  `;

    map.getContainer().appendChild(div);
    return div;
  }
}
