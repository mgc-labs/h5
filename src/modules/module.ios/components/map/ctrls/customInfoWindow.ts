import styles from './customInfoWindow.module.less';

declare const BMap: any;

export class CustomInfoWindow extends BMap.Overlay {
  constructor() {
    super();
    this.width = 300;
    this.height = 132;
  }

  initialize(map) {
    this.map = map;

    this.div = document.createElement('div');
    this.div.style.width = `${this.width}px`;
    this.div.style.height = `${this.height}px`;
    this.div.className = styles.divInfoWindow;

    this.divCurrow = document.createElement('div');
    this.divCurrow.className = styles.curr;

    this.div.appendChild(this.divCurrow);

    this.content = document.createElement('div');
    this.content.className = styles.divf;
    this.div.appendChild(this.content);

    this.map.getPanes().labelPane.appendChild(this.div);

    return this.div;
  }

  draw() {
    const pixel = this.map.pointToOverlayPixel(this.point);
    // this.div.style.left = pixel.x - this.width / 2 + 'px';
    // this.div.style.top = pixel.y - this.height - 40 + 'px';
    // const height = this.div.offsetHeight;

    // 框位置
    let fx = pixel.x - this.width / 2;
    let fy = pixel.y - this.height - 16;

    // 箭头位置
    let cx = this.width / 2 - 11;
    let cy = this.height - 5;

    // 下面的做法，没有找到官方的做法，只能自己写
    const posDiv = this.div.parentElement.parentElement.parentElement;
    if (posDiv) {
      const container = this.map.getContainer();
      const mWidth = container.offsetWidth;
      const mHeight = container.offsetHeight;

      const mx = fx + posDiv.offsetLeft;
      const mt = fy + posDiv.offsetTop;

      if (mx < 0) {
        // 如果左边距超出
        fx = pixel.x - this.width / 2 - mx;
        cx = cx + mx + 0;
      } else if (mx + this.width > mWidth) {
        // 如果右边距超出
        fx = pixel.x - this.width / 2 - (this.width + mx - mWidth) - 0;
        cx = cx + (this.width + mx - mWidth) + 3 + 1;
      }
      if (mt - 90 < 0) {
        fy = pixel.y + 16 + 8;
        cy = -10;
        this.divCurrow.style.transform = 'rotate(45deg)';
      } else {
        fy -= 8;
        cy -= 7;
        this.divCurrow.style.transform = 'rotate(-45deg)';
      }
    }

    this.div.style.left = fx + 'px';
    this.div.style.top = fy + 'px';

    this.divCurrow.style.left = cx + 'px';
    this.divCurrow.style.top = cy + 'px';
  }

  setSize() {
    this.div.style.width = this.width + 'px';
    this.div.style.height = this.height + 'px';
  }

  show() {
    super.show();
    this.draw();
    // this.setSize();
  }

  setPosition(point) {
    this.point = point;
  }

  showValue(value) {
    if (value === null || value === undefined || value === '') {
      return '';
    }
    return value;
  }

  setData(data) {
    this.content.innerHTML = `
      <table>
        <tr>
          <th>订单号：</th>
          <td>${this.showValue(data.orderNumber)}</td>
        </tr>
        <tr>
          <th>重量(千克)：</th>
          <td>${this.showValue(data.goodsWeight)}</td>
        </tr>
        <tr>
          <th>体积(方)：</th>
          <td>${this.showValue(data.goodsVolume)}</td>
        </tr>
        <tr>
          <th>要求送达时间：</th>
          <td>${this.showValue(data.requireDate)}</td></tr>
        <tr>
          <th>派送地址：</th>
          <td>
            <div class="${styles.address}">
              ${this.showValue(data.address)}
            </div>
          </td>
        </tr>
      </table>
    `;
  }
}
