import * as React from 'react';

import Col from 'antd/es/col';
import Form, { FormComponentProps } from 'antd/es/form';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import styles from './styles.module.less';

interface IAddToGroupComponentProps extends FormComponentProps {
  orderGroupList: any[];
  onSubmit: (data: any) => void;
  onClose: () => void;
  addToGroupLoading: boolean;
  selectedOrderIdList: string[]; // 地图上的点
}

class AddToGroupComponent extends React.PureComponent<
  IAddToGroupComponentProps,
  any
> {
  onSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit({
          ...values,
          partOrderIds: this.props.selectedOrderIdList,
        });
      }
    });
  };

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 18 },
      },
    };

    const { getFieldDecorator } = this.props.form;
    const { addToGroupLoading, orderGroupList } = this.props;
    const defaultGroup = orderGroupList[0] || {};
    return (
      <Modal
        title={'添加到组'}
        visible={true}
        width={520}
        onCancel={this.props.onClose}
        onOk={this.onSubmit}
        confirmLoading={addToGroupLoading}
        centered={true}
        maskClosable={false}
        closable={!addToGroupLoading}
        cancelButtonProps={{ disabled: addToGroupLoading }}
      >
        <div className={styles.addToGroup}>
          <Form className={styles.form}>
            <Row gutter={16}>
              <Col xs={24}>
                <Form.Item {...formItemLayout} label="分组名称">
                  {getFieldDecorator('partGroupId', {
                    initialValue: defaultGroup.utmsPartGroupId,
                    rules: [
                      {
                        required: true,
                        message: '请选择分组',
                      },
                    ],
                  })(
                    <Select placeholder="请选择分组">
                      {orderGroupList.map(item => (
                        <Select.Option
                          value={item.utmsPartGroupId}
                          key={item.utmsPartGroupId}
                        >
                          {item.name}
                        </Select.Option>
                      ))}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </div>
      </Modal>
    );
  }
}

export let AddToGroup = Form.create()(AddToGroupComponent);
