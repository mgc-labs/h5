import * as React from 'react';

import Button from 'antd/es/button';
import Icon from 'antd/es/icon';
import Popover from 'antd/es/popover';
import { ImmutableListMap } from 'utils/types';
import { AddToGroup } from '../addToGroup';
import { AutoGro } from '../autoGro';
import { CreateGroup } from '../createGroup';
import styles from './styles.module.less';

interface IMapToolBarProps {
  recVehicleList: ImmutableListMap;
  defaultWarehouse: any;
  warehouseList: any[];
  orderGroupList: any[];
  orderGroupShow: boolean; // 是否显示订单组
  onGroupShow: (show: boolean) => void;

  isShowAutoGroConfirm: boolean;
  autoGroLoading: boolean;
  onAutoGro: (data) => void;
  onAutoGroOpen: () => void;
  onAutoGroClose: () => void;

  isShowCreateGroup: boolean;
  createGroupLoading: boolean;
  onCreateGroupOpen: () => void;
  onCreateGroup: (data) => void;
  onCreateGroupClose: () => void;

  isShowAddToGroup: boolean;
  addToGroupLoading: boolean;
  onAddToGroupOpen: () => void;
  onAddToGroup: (data) => void;
  onAddToGroupClose: () => void;

  onOpenRectSelect: (isOpen: boolean) => void;
  onSelectNull: () => void;

  ungroupdOrderList: any[]; // 未分组的订单
  selectedOrderList: any[]; // 地图上的已选中的点
}

export class MapToolBar extends React.PureComponent<IMapToolBarProps, any> {
  constructor(props: IMapToolBarProps, context: any) {
    super(props, context);

    this.state = {
      isOpenRectSelect: false,
    };
  }
  onOpenRectSelect = () => {
    this.setState(
      {
        isOpenRectSelect: !this.state.isOpenRectSelect,
      },
      () => {
        this.props.onOpenRectSelect(this.state.isOpenRectSelect);
      },
    );
  };
  render() {
    const {
      onGroupShow,
      orderGroupShow,
      orderGroupList,
      defaultWarehouse,
      warehouseList,
      recVehicleList, // 推荐车型

      isShowAutoGroConfirm,
      autoGroLoading,
      onAutoGro,
      onAutoGroClose,
      onAutoGroOpen,

      isShowCreateGroup,
      createGroupLoading,
      onCreateGroup,
      onCreateGroupClose,
      onCreateGroupOpen,

      isShowAddToGroup,
      addToGroupLoading,
      onAddToGroupOpen,
      onAddToGroup,
      onAddToGroupClose,

      ungroupdOrderList,
      selectedOrderList,
    } = this.props;

    const { isOpenRectSelect } = this.state;

    const selectedOrderIdList = selectedOrderList.map(
      item => item.utmsPartOrderId,
    );

    const selectedOrderCount = selectedOrderIdList.length;
    const groupCount = orderGroupList.length;

    let openCloseLinkBtn = null;
    if (groupCount) {
      openCloseLinkBtn = orderGroupShow ? (
        <div onClick={() => onGroupShow(false)} className={styles.linkBtn}>
          <a href="javascript:;">
            收起
            <Icon
              type="up-circle"
              theme="outlined"
              className={styles.linkBtnIcon}
            />
          </a>
        </div>
      ) : (
        <div onClick={() => onGroupShow(true)} className={styles.linkBtn}>
          <a href="javascript:;">
            展开
            <Icon
              type="down-circle"
              theme="outlined"
              className={styles.linkBtnIcon}
            />
          </a>
        </div>
      );
    }

    return (
      <div className={styles.mapToolBar}>
        <div className={styles.area1}>
          <Popover
            placement="topLeft"
            content={<div>点击自动配载，对所有未分组订单进行分组</div>}
          >
            <Button
              type="primary"
              onClick={onAutoGroOpen}
              disabled={ungroupdOrderList.length < 1}
            >
              自动配载
            </Button>
          </Popover>
          <Popover
            placement="topLeft"
            content={
              <div>
                点选
                <div className={styles.tbpic1} />
                或框选
                <div className={styles.tbpic2} />
                后进行配载操作
              </div>
            }
          >
            <Button
              onClick={onCreateGroupOpen}
              disabled={selectedOrderCount < 1}
            >
              手动配载
            </Button>
          </Popover>
          <Popover
            placement="topLeft"
            content={
              <div>
                点选
                <div className={styles.tbpic1} />
                或框选
                <div className={styles.tbpic2} />
                后添加到已有订单组
              </div>
            }
          >
            <Button
              onClick={onAddToGroupOpen}
              disabled={selectedOrderCount < 1 || groupCount < 1}
            >
              添加到组
            </Button>
          </Popover>

          <div className={styles.count}>
            订单组(
            {groupCount})
          </div>
          {openCloseLinkBtn}
          {isShowAutoGroConfirm && (
            <AutoGro
              autoGroLoading={autoGroLoading}
              onSubmit={onAutoGro}
              onClose={onAutoGroClose}
              defaultWarehouse={defaultWarehouse}
              warehouseList={warehouseList}
            />
          )}

          {isShowCreateGroup && (
            <CreateGroup
              defaultWarehouse={defaultWarehouse}
              warehouseList={warehouseList}
              createGroupLoading={createGroupLoading}
              onSubmit={onCreateGroup}
              onClose={onCreateGroupClose}
              selectedOrderIdList={selectedOrderIdList}
              selectedOrderList={selectedOrderList}
              recVehicleList={recVehicleList}
            />
          )}

          {isShowAddToGroup && (
            <AddToGroup
              orderGroupList={orderGroupList}
              addToGroupLoading={addToGroupLoading}
              onSubmit={onAddToGroup}
              onClose={onAddToGroupClose}
              selectedOrderIdList={selectedOrderIdList}
            />
          )}
        </div>
        <div className={styles.area2}>
          <Button.Group>
            <Button onClick={this.onOpenRectSelect}>
              {isOpenRectSelect ? '取消框选' : '一键框选'}
            </Button>
            {this.props.selectedOrderList.length > 0 && (
              <Button onClick={this.props.onSelectNull}>取消选中</Button>
            )}
          </Button.Group>
        </div>
      </div>
    );
  }
}
