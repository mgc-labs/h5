import * as React from 'react';

import Col from 'antd/es/col';
import Form, { FormComponentProps } from 'antd/es/form';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import styles from './styles.module.less';

interface IMoveToGroupComponentProps extends FormComponentProps {
  selectedGroupId: string;
  orderGroupList: any[];
  onSubmit: (groupId: string) => void;
  onClose: () => void;
}

class MoveToGroupComponent extends React.PureComponent<
  IMoveToGroupComponentProps,
  any
> {
  onSubmit = () => {
    const {
      form: { validateFields },
      onSubmit,
    } = this.props;

    validateFields((err, values) => {
      if (!err) {
        onSubmit(values.toPartGroupId);
      }
    });
  };

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 18 },
      },
    };

    const {
      form: { getFieldDecorator },
      selectedGroupId,
      orderGroupList,
      onClose,
    } = this.props;

    const canSelectOrderGroupList = orderGroupList.filter(
      h => h.utmsPartGroupId !== selectedGroupId,
    );

    const defaultGroupId = canSelectOrderGroupList[0].utmsPartGroupId;

    return (
      <Modal
        title={'移动到组'}
        visible={true}
        width={520}
        onCancel={onClose}
        onOk={this.onSubmit}
      >
        <div className={styles.addToGroup}>
          <Form className={styles.form}>
            <Row gutter={16}>
              <Col xs={24}>
                <Form.Item {...formItemLayout} label="分组名称">
                  {getFieldDecorator('toPartGroupId', {
                    initialValue: defaultGroupId,
                    rules: [
                      {
                        required: true,
                        message: '请输入分组名称',
                      },
                    ],
                  })(
                    <Select placeholder="请选择分组名称">
                      {canSelectOrderGroupList.map(item => (
                        <Select.Option
                          value={item.utmsPartGroupId}
                          key={item.utmsPartGroupId}
                        >
                          {item.name}
                        </Select.Option>
                      ))}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </div>
      </Modal>
    );
  }
}

export let MoveToGroup = Form.create()(MoveToGroupComponent);
