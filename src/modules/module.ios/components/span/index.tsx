import * as React from 'react';

interface ISpanProps {
  value?: string;
}

export function Span(props: ISpanProps) {
  return <span>{props.value}</span>;
}
