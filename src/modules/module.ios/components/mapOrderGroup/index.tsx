import * as React from 'react';

import Button from 'antd/es/button';
import CheckBox from 'antd/es/checkbox';
import Modal from 'antd/es/modal';
import Popover from 'antd/es/popover';
import Spin from 'antd/es/spin';
import Table from 'antd/es/table';
import debounce from 'lodash/debounce';
import styles from './styles.module.less';

interface IMapOrderGroupProps {
  orderGroupLoading: boolean;
  orderGroupList: any[];
  checkedGroupIdList: string[];
  selectedGroupId: string;
  onGroupCheck: (groupId: string) => void;
  onGroupAllCheck: (checked: boolean) => void;
  onSelected: (record: any) => void;
  onDelete: (data: any) => void;
  onSend: (data: any) => void;
}

const RowKey = 'utmsPartGroupId';

export class MapOrderGroup extends React.PureComponent<
  IMapOrderGroupProps,
  any
> {
  private divMaxHeight: HTMLDivElement;
  private resize: () => void;

  constructor(props: IMapOrderGroupProps, context: any) {
    super(props, context);

    this.state = {
      tableScrollHeight: 0,
    };

    this.resize = debounce(() => {
      if (this.divMaxHeight) {
        this.setState({
          tableScrollHeight: this.divMaxHeight.offsetHeight - 123,
        });
      }
    }, 100);

    window.addEventListener('resize', this.resize);
  }

  componentDidMount() {
    this.resize();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.resize);
  }

  onRowClick = (record, event) => {
    const target: HTMLInputElement = event.target;
    if (
      !target.querySelector('[type="checkbox"]') &&
      target.type !== 'checkbox'
    ) {
      this.props.onSelected(record);
    }
  };

  onDelete = () => {
    const { checkedGroupIdList, onDelete } = this.props;

    Modal.confirm({
      title: '确定需要删除吗？',
      content: '删除后本组订单将解散，回到未分组状态',
      okText: '确认',
      cancelText: '取消',
      onOk: () => {
        onDelete({
          groupIds: checkedGroupIdList,
        });
      },
    });
  };

  onRow = record => {
    const { selectedGroupId } = this.props;
    return {
      className: record[RowKey] === selectedGroupId ? styles.current : null,
      onClick: e => this.onRowClick(record, e),
    };
  };

  onSend = () => {
    this.props.onSend(this.props.checkedGroupIdList);
  };

  render() {
    const {
      orderGroupList,
      orderGroupLoading,
      checkedGroupIdList,
      onGroupCheck,
      onGroupAllCheck,
    } = this.props;

    const { tableScrollHeight } = this.state;

    const list = orderGroupList.map(h => ({
      ...h,
      loadRate: h.loadRate.toFixed(2),
    }));

    const columns = [
      {
        width: 32,
        key: 'checkbox',
        render: (h: any) => (
          <CheckBox
            checked={checkedGroupIdList.indexOf(h.utmsPartGroupId) > -1}
            onChange={() => onGroupCheck(h.utmsPartGroupId)}
          />
        ),
      },
      {
        title: '序号',
        dataIndex: 'no',
        width: 40,
      },
      {
        title: '分组名',
        // dataIndex: 'name',
        width: 60,
        render: (h: any) => (
          <Popover placement="topLeft" content={h.name}>
            <div className={styles.groupName}>{h.name}</div>
          </Popover>
        ),
      },
      {
        title: '订单数',
        dataIndex: 'orderCount',
        width: 45,
      },
      {
        title: '体积(方)',
        dataIndex: 'volume',
        width: 52,
      },
      {
        title: '推荐车型',
        dataIndex: 'adviceCarStruct',
      },
      {
        title: '装载率',
        dataIndex: 'loadRate',
        width: 50,
      },
    ];

    const checkedCount = checkedGroupIdList.length;
    const groupCount = list.length;
    return (
      <div className={styles.mapOrderGroup}>
        <div
          className={styles.divMaxHeight}
          ref={obj => (this.divMaxHeight = obj)}
        />
        <Spin tip="加载中..." spinning={orderGroupLoading}>
          <Table
            columns={columns}
            rowKey={RowKey}
            dataSource={list}
            pagination={{
              pageSize: 10,
              hideOnSinglePage: true,
              size: 'small',
              style: { margin: '10px 13px 0px 0' },
            }}
            scroll={{ y: tableScrollHeight }}
            onRow={this.onRow}
          />

          <div className={styles.ctrl}>
            <CheckBox
              className={styles.check}
              onChange={e => onGroupAllCheck(e.target.checked)}
              indeterminate={checkedCount > 0 && groupCount > checkedCount}
              checked={checkedCount > 0 && groupCount === checkedCount}
            >
              全选
            </CheckBox>
            <div>
              选中：
              {checkedCount} / {groupCount}
            </div>
            <div className={styles.split} />
            <Button
              className={styles.btn}
              disabled={checkedCount < 1}
              onClick={this.onDelete}
            >
              删除
            </Button>
            <Button
              className={styles.btn}
              type="primary"
              disabled={checkedCount < 1}
              onClick={this.onSend}
            >
              发货
            </Button>
          </div>
        </Spin>
      </div>
    );
  }
}
