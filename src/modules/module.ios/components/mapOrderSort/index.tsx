import * as React from 'react';

import Button from 'antd/es/button';
import CheckBox from 'antd/es/checkbox';
import Icon from 'antd/es/icon';
import Modal from 'antd/es/modal';
import Spin from 'antd/es/spin';
import Table from 'antd/es/table';
import debounce from 'lodash/debounce';
import { toYYYYMMDDHHmm } from 'utils/dateTimeHelper';
import { MoveToGroup } from '../moveToGroup';
import styles from './styles.module.less';

interface IMapOrderSortProps {
  isEditing: boolean;
  getOrderListLoading: boolean;
  orderGroupList: any[];
  selectedGroupId: string;
  orderListOfGroup: any[];
  onOrderReOrder: (pars: { groupId: string; orderList: any[] }) => void;
  onOrderCheck: (record: any) => void;
  onOrderAllCheck: (checked: boolean) => void;
  onDelete: (data: any) => void;
  onMoveToGroup: (data: any) => void;
  onHide: () => void;
}

const RowKey = 'utmsPartOrderId';

export class MapOrderSort extends React.PureComponent<IMapOrderSortProps, any> {
  private divOrderSort: HTMLDivElement;
  private moveTrStartY: number;
  private moveTr: HTMLTableRowElement;
  private isTrMoving: boolean;
  private haveDragingMoved: boolean;
  private tbody: HTMLTableSectionElement;
  private movingTbody: HTMLTableSectionElement;
  private movingLineData: any;
  private divMaxHeight: HTMLDivElement;
  private resize: () => void;

  constructor(props: IMapOrderSortProps, context: any) {
    super(props, context);
    this.state = {
      isShowMoveToModal: false,
      tableScrollHeight: 0,
    };

    this.resize = debounce(() => {
      if (this.divMaxHeight) {
        this.setState({
          tableScrollHeight: this.divMaxHeight.offsetHeight - 50 - 39,
        });
      }
    }, 100);

    window.addEventListener('resize', this.resize);
  }

  componentDidMount() {
    this.resize();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.resize);
  }

  onBeginDrag = event => {
    const target = event.target;

    if (target.className !== 'imgMove') {
      return;
    }

    const td = target.parentNode;

    const item = JSON.parse(td.querySelector('.divJsonData').innerHTML);

    const tr: HTMLTableRowElement = td.parentNode;
    this.tbody = tr.parentNode as HTMLTableSectionElement;
    const table = this.tbody.parentNode as HTMLTableElement;
    this.movingTbody = table.tBodies[1];
    if (!this.movingTbody) {
      this.movingTbody = document.createElement('tbody');
      table.appendChild(this.movingTbody);
      this.moveTr = document.createElement('tr');
      this.moveTr.className = styles.moveTr;
      for (const td01 of tr.cells) {
        const tdn = document.createElement('td');
        tdn.innerHTML = td01.innerHTML;
        tdn.className = td01.className;
        this.moveTr.appendChild(tdn);
      }
      this.movingTbody.appendChild(this.moveTr);
    } else {
      this.moveTr = this.movingTbody.rows[0];
      for (let i = 0; i < tr.cells.length; i++) {
        this.moveTr.cells[i].innerHTML = tr.cells[i].innerHTML;
      }
    }

    this.movingLineData = item;
    this.isTrMoving = true;
    this.haveDragingMoved = false; // 是否有拖动过（单击会有问题）

    const moveDrag: HTMLDivElement = this.moveTr.querySelector('.tdMove');
    moveDrag.onmousemove = this.onMove;
    moveDrag.onmouseup = this.onMoveFinish;
    moveDrag.onmouseout = this.onMoveCancel;

    this.moveTrStartY =
      event.clientY - this.divOrderSort.offsetTop - tr.offsetTop;

    this.moveTr.style.top = `${tr.offsetTop}px`;
    this.moveTr.style.height = `${tr.offsetHeight}px`;

    // 将拖动的行的宽度设成与原来的一致
    for (let i = 0; i < tr.cells.length; i++) {
      const td0 = tr.cells[i];
      this.moveTr.cells[i].style.width = `${td0.offsetWidth}px`;
    }
  };

  onMove = (event: MouseEvent) => {
    event.stopPropagation();
    if (this.isTrMoving && this.props.isEditing) {
      this.haveDragingMoved = true;
      const y = event.clientY - this.divOrderSort.offsetTop - this.moveTrStartY;
      this.moveTr.style.top = `${y}px`;
    }
  };

  onMoveFinish = event => {
    const { orderListOfGroup, onOrderReOrder, selectedGroupId } = this.props;
    this.isTrMoving = false;
    if (this.haveDragingMoved) {
      let insertAfterTrIndex = 0;
      const y = event.clientY - this.divOrderSort.offsetTop - this.moveTrStartY;

      for (let i = 0; i < this.tbody.rows.length; i++) {
        const tr = this.tbody.rows[i];
        if (tr.offsetTop < y) {
          insertAfterTrIndex = i + 1;
        }
      }

      let toIndex = 0;
      if (insertAfterTrIndex === 0) {
        toIndex = 0;
      } else {
        const lastIndex1 = orderListOfGroup.findIndex(
          item => item[RowKey] === this.movingLineData[RowKey],
        );

        toIndex =
          insertAfterTrIndex > lastIndex1
            ? insertAfterTrIndex - 1
            : insertAfterTrIndex;
      }

      const list = orderListOfGroup;
      const lastIndex = list.findIndex(
        item => item[RowKey] === this.movingLineData[RowKey],
      );

      const value = list[lastIndex];
      list.splice(lastIndex, 1);

      list.splice(toIndex, 0, value);

      onOrderReOrder({
        groupId: selectedGroupId,
        orderList: list,
      });
    }
    this.movingLineData = null;

    this.movingTbody.parentElement.removeChild(this.movingTbody);
  };

  onMoveCancel = event => {
    if (this.props.isEditing) {
      this.isTrMoving = false;
      this.movingLineData = null;
    }
  };
  onMoveStop = event => {
    this.isTrMoving = false;
    if (this.movingTbody && this.movingTbody.parentElement) {
      this.movingTbody.parentElement.removeChild(this.movingTbody);
    }
  };

  onDelete = () => {
    const { orderListOfGroup, onDelete, selectedGroupId } = this.props;

    Modal.confirm({
      title: '确定需要删除吗？',
      content: '删除后订单后将从该组去除，回到未分组状态',
      okText: '确认',
      cancelText: '取消',
      onOk: () => {
        // 过滤出选中的分组
        onDelete({
          groupId: selectedGroupId,
          orderIds: orderListOfGroup
            .filter(item => item.isChecked)
            .map(item => item[RowKey]),
        });
      },
    });
  };

  onMoveToModalVisibleChanged = visible => {
    this.setState({
      isShowMoveToModal: visible,
    });
  };

  onMoveToGroupSubmit = (toPartGroupId: string) => {
    const { orderListOfGroup, selectedGroupId, onMoveToGroup } = this.props;
    this.onMoveToModalVisibleChanged(false);
    onMoveToGroup({
      fromPartGroupId: selectedGroupId,
      toPartGroupId,
      orderIds: orderListOfGroup
        .filter(item => item.isChecked)
        .map(item => item[RowKey]),
    });
  };

  render() {
    const { tableScrollHeight, isShowMoveToModal } = this.state;

    const {
      orderListOfGroup,
      selectedGroupId,
      getOrderListLoading,
      onHide,
      orderGroupList,
      onOrderCheck,
      onOrderAllCheck,
    } = this.props;

    const list = orderListOfGroup.map((item, index) => ({
      ...item,
      no: index + 1,
      requireDate: toYYYYMMDDHHmm(item.requireDate),
    }));

    const columns = [
      {
        width: 28,
        key: 'checkbox',
        render: (record: any) => (
          <CheckBox
            checked={record.isChecked}
            onChange={() => onOrderCheck(record)}
          />
        ),
      },
      {
        title: '序号',
        dataIndex: 'no',
        width: 30,
      },
      // {
      //   title: '订单号',
      //   dataIndex: 'orderNumber',
      //   width: 100,
      // },
      {
        title: '派送地址',
        dataIndex: 'address',
        // width: 150,
      },
      {
        title: '联系人',
        dataIndex: 'contact',
        width: 45,
      },
      {
        title: '联系电话',
        dataIndex: 'phone',
        width: 95,
      },
      {
        title: '要求到达时间',
        dataIndex: 'requireDate',
        width: 120,
      },
      {
        title: '重量(千克)',
        dataIndex: 'goodsWeight',
        width: 62,
      },
      {
        title: '体积(方)',
        dataIndex: 'goodsVolume',
        width: 50,
      },
      {
        width: 25,
        key: 'move',
        className: 'tdMove',
        render: (record: any) => (
          <React.Fragment>
            <div className="imgMove" />
            <div className="divJsonData" style={{ display: 'none' }}>
              {JSON.stringify(record)}
            </div>
          </React.Fragment>
        ),
      },
    ];

    const checkedCount = list.filter(item => item.isChecked).length;

    return (
      <div
        className={styles.mapOrderSort}
        ref={obj => (this.divOrderSort = obj)}
        onMouseMove={this.onMoveStop}
        onMouseDown={event => this.onBeginDrag(event)}
      >
        <div
          className={styles.divMaxHeight}
          ref={obj => (this.divMaxHeight = obj)}
        />
        <div className={styles.hold} onClick={onHide}>
          <Icon type="menu-unfold" theme="outlined" />
        </div>
        <Spin tip="加载中..." spinning={getOrderListLoading}>
          <Table
            columns={columns}
            rowKey={RowKey}
            dataSource={list}
            pagination={false}
            scroll={{ y: tableScrollHeight }}
          />
          <div className={styles.ctrl}>
            <CheckBox
              className={styles.check}
              onChange={e => onOrderAllCheck(e.target.checked)}
              indeterminate={checkedCount > 0 && list.length > checkedCount}
              checked={checkedCount > 0 && list.length === checkedCount}
            >
              全选
            </CheckBox>
            <div>
              选中：
              {checkedCount} / {list.length}
            </div>
            <div className={styles.split} />
            <Button
              className={styles.btn}
              disabled={checkedCount < 1}
              onClick={this.onDelete}
            >
              删除
            </Button>
            <Button
              className={styles.btn}
              type="primary"
              disabled={checkedCount < 1 || orderGroupList.length < 2}
              onClick={() => this.onMoveToModalVisibleChanged(true)}
            >
              移动
            </Button>
          </div>

          {isShowMoveToModal && (
            <MoveToGroup
              onSubmit={this.onMoveToGroupSubmit}
              selectedGroupId={selectedGroupId}
              orderGroupList={orderGroupList}
              onClose={() => this.onMoveToModalVisibleChanged(false)}
            />
          )}
        </Spin>
      </div>
    );
  }
}
