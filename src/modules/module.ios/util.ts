import { ImmutableListMap } from 'utils/types';

export function newLocOrderListSign() {
  return `${new Date().getTime()}${Math.random()}`;
}
