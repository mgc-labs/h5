/**
 * Ios Component
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Modal from 'antd/es/modal';
import msngr from 'msngr';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Config } from './components/config';
import { LocComponent } from './components/loc';
import styles from './index.module.less';
import {
  makeSelectAddToGroupLoading,
  makeSelectAutoGroLoading,
  makeSelectCheckedGroupIdList,
  makeSelectConfig,
  makeSelectCreateGroupLoading,
  makeSelectDefaultWarehouse,
  makeSelectDeleteGroupLoading,
  makeSelectDeleteOrderLoading,
  makeSelectGetConfigLoading,
  makeSelectGetOrderListLoading,
  makeSelectIsShowAddToGroup,
  makeSelectIsShowAutoGroConfirm,
  makeSelectIsShowConfigModal,
  makeSelectIsShowCreateGroup,
  makeSelectIsShowOrderListOfGroup,
  makeSelectLocOrderList,
  makeSelectLocOrderListLoading,
  makeSelectLocOrderListSign,
  makeSelectMoveOrderLoading,
  makeSelectOrderGroupList,
  makeSelectOrderGroupLoading,
  makeSelectOrderGroupSendLoading,
  makeSelectOrderGroupShow,
  makeSelectOrderListOfGroup,
  makeSelectRecVehicleList,
  makeSelectSaveConfigLoading,
  makeSelectSelectedGroupId,
  makeSelectUnLocOrderData,
  makeSelectUnLocOrderListLoading,
  makeSelectWarehouseList,
  makeSelectWarehouseLoaded,
} from './selectors';

import Tabs from 'antd/es/tabs';
import { UnLocList } from './components/unLocList';

class Ios extends React.PureComponent<any, any> {
  private divMain: HTMLDivElement;
  private isModalShown = false;
  constructor(props: any, context: any) {
    super(props, context);
    this.state = {
      isShowConfig: false,
      permissions: {},
    };
  }

  componentDidMount() {
    this.props.getInitData();

    this.divMain.onselectstart = () => {
      return false;
    };

    // 当添加了 close 事件的订阅
    // 则一定要执行 callback 中的 requestClose 页面才会关闭
    this.props.eventEmitter.on('close', requestClose => {
      requestClose();
    });

    this.applicationPermission();

    msngr('GLOBAL', 'activeRouteId', 'change').on(this.onRefresh);
  }

  componentWillUnmount() {
    msngr('GLOBAL', 'activeRouteId', 'change').drop(this.onRefresh);
  }

  onRefresh = (routeId: string) => {
    if (routeId === '/ios') {
      this.props.getInitData();
    }
  };

  applicationPermission = () => {
    const p: any[] = this.props.permissions.toJS();
    this.setState({
      permissions: {
        editOrder: p.filter(h => h.operateKey === 'editOrder').length > 0,
      },
    });
  };

  componentWillReceiveProps(nextProps: any, nextContext: any) {
    const {
      close,
      onGotoCreateWarehouse,
      warehouseLoaded,
      defaultWarehouse,
    } = nextProps;
    if (warehouseLoaded && defaultWarehouse === undefined) {
      if (!this.isModalShown) {
        Modal.error({
          title: '跳转到新键地址',
          content: '当前无仓库装货位置，请先创建仓库位置',
          okText: '去创建',
          onOk() {
            onGotoCreateWarehouse();
            close();
          },
        });
        this.isModalShown = true;
      }
    }
  }

  onSaveConfig = (values: any) => {
    this.props.onSaveConfig(values);
  };

  render() {
    const {
      isShowConfigModal,
      getConfigLoading,
      config,
      saveConfigLoading,
      onSaveConfigCancel,

      unLocOrderData,
      unLocOrderListLoading,
      onGetUnLocOrderList,
      onGotoEditOrder,
      onGotoOrderDetail,
      onDeleteOrder,
      onUpdateOrderLocation,

      ...other
    } = this.props;

    const { permissions } = this.state;

    const operations = (
      <div className={styles.btnConfig}>
        <Button
          type="primary"
          onClick={this.props.onGetConfig}
          loading={getConfigLoading}
        >
          配载设置
        </Button>
      </div>
    );

    return (
      <div className={styles.main} ref={obj => (this.divMain = obj)}>
        <Card bordered={true}>
          <Tabs tabBarExtraContent={operations}>
            <Tabs.TabPane
              tab={`已定位订单(${other.locOrderList.length})`}
              key="1"
              style={{ height: '100%' }}
            >
              <LocComponent {...other as any} />
            </Tabs.TabPane>
            <Tabs.TabPane
              tab={`未定位订单(${unLocOrderData.get('count')})`}
              key="2"
              style={{ height: '100%' }}
            >
              <UnLocList
                unLocOrderData={unLocOrderData}
                onGetUnLocOrderList={onGetUnLocOrderList}
                unLocOrderListLoading={unLocOrderListLoading}
                onGotoEditOrder={onGotoEditOrder}
                onGotoDetail={onGotoOrderDetail}
                onDeleteOrder={onDeleteOrder}
                onUpdateOrderLocation={onUpdateOrderLocation}
                defaultWarehouse={other.defaultWarehouse}
                permissions={permissions}
              />
            </Tabs.TabPane>
          </Tabs>
        </Card>

        {isShowConfigModal && (
          <Config
            onSubmit={this.onSaveConfig}
            onClose={onSaveConfigCancel}
            data={config}
            saveConfigLoading={saveConfigLoading}
          />
        )}
      </div>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getInitData: () => dispatch(actions.getInitData()),
    onGroupShow: d => dispatch(actions.groupShow(d)),
    onGroupCheck: d => dispatch(actions.groupCheck(d)),
    onGroupAllCheck: d => dispatch(actions.groupAllCheck(d)),
    onGroupSelected: d => dispatch(actions.groupSelected(d)),
    onOrderReOrder: d => dispatch(actions.orderReOrder(d)),
    onOrderCheck: d => dispatch(actions.orderCheck(d)),
    onOrderAllCheck: d => dispatch(actions.orderAllCheck(d)),
    onOrderListHide: () => dispatch(actions.orderListHide()),
    onDeleteOrderFromGroup: d => dispatch(actions.deleteOrderFromGroup(d)),
    onMoveOrderToGroup: d => dispatch(actions.moveOrderToGroup(d)),
    onDeleteGroup: d => dispatch(actions.deleteGroup(d)),
    onGetConfig: () => dispatch(actions.getConfig()),
    onSaveConfig: d => dispatch(actions.saveConfig(d)),
    onSaveConfigCancel: () => dispatch(actions.saveConfigCancel()),
    onAutoGro: d => dispatch(actions.autoGro(d)),
    onAutoGroOpen: () => dispatch(actions.autoGroOpen()),
    onAutoGroClose: () => dispatch(actions.autoGropClose()),
    onCreateGroup: d => dispatch(actions.createGroup(d)),
    onCreateGroupOpen: () => dispatch(actions.createGroupOpen()),
    onCreateGroupClose: () => dispatch(actions.createGroupClose()),
    onAddToGroupOpen: () => dispatch(actions.addToGroupOpen()),
    onAddToGroup: d => dispatch(actions.addToGroup(d)),
    onAddToGroupClose: () => dispatch(actions.addToGroupClose()),
    onSelectPoints: d => dispatch(actions.locOrderSelect(d)),
    onSend: d => dispatch(actions.send(d)),
    onGetUnLocOrderList: d => dispatch(actions.getUnLocOrder(d)),
    onGotoEditOrder: d => dispatch(actions.gotoEditOrder(d)),
    onGotoOrderDetail: d => dispatch(actions.gotoOrderDetail(d)),
    onDeleteOrder: d => dispatch(actions.deleteOrder(d)),
    onGotoCreateWarehouse: () => dispatch(actions.gotoCreateWarehouse()),
    onUpdateOrderLocation: d => dispatch(actions.updateOrderLocation(d)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    recVehicleList: makeSelectRecVehicleList(selectState),
    warehouseList: makeSelectWarehouseList(selectState),
    warehouseLoaded: makeSelectWarehouseLoaded(selectState),
    defaultWarehouse: makeSelectDefaultWarehouse(selectState),
    locOrderList: makeSelectLocOrderList(selectState), // 已定位订单，包括未分组，已分组
    unLocOrderData: makeSelectUnLocOrderData(selectState), // 未定位订单
    unLocOrderListLoading: makeSelectUnLocOrderListLoading(selectState),
    locOrderListSign: makeSelectLocOrderListSign(selectState),
    locOrderListLoading: makeSelectLocOrderListLoading(selectState),
    orderGroupList: makeSelectOrderGroupList(selectState),
    orderGroupLoading: makeSelectOrderGroupLoading(selectState),
    orderGroupShow: makeSelectOrderGroupShow(selectState),
    selectedGroupId: makeSelectSelectedGroupId(selectState),
    checkedGroupIdList: makeSelectCheckedGroupIdList(selectState),
    isShowOrderListOfGroup: makeSelectIsShowOrderListOfGroup(selectState),
    getOrderListLoading: makeSelectGetOrderListLoading(selectState),
    orderListOfGroup: makeSelectOrderListOfGroup(selectState),
    isShowConfigModal: makeSelectIsShowConfigModal(selectState),
    getConfigLoading: makeSelectGetConfigLoading(selectState),
    config: makeSelectConfig(selectState),
    saveConfigLoading: makeSelectSaveConfigLoading(selectState),
    autoGroLoading: makeSelectAutoGroLoading(selectState),
    isShowAutoGroConfirm: makeSelectIsShowAutoGroConfirm(selectState),
    createGroupLoading: makeSelectCreateGroupLoading(selectState),
    isShowCreateGroup: makeSelectIsShowCreateGroup(selectState),
    isShowAddToGroup: makeSelectIsShowAddToGroup(selectState),
    addToGroupLoading: makeSelectAddToGroupLoading(selectState),
    orderGroupSendLoading: makeSelectOrderGroupSendLoading(selectState),
    deleteGroupLoading: makeSelectDeleteGroupLoading(selectState),
    deleteOrderLoading: makeSelectDeleteOrderLoading(selectState),
    moveOrderLoading: makeSelectMoveOrderLoading(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(Ios);
};
