const list = [];
for (let i = 0; i < 1000; i++) {
  const status = i % 2;
  list.push({
    utmsPartOrderId: i,
    partGroupId: status === 1 ? (i % 5) + 1 : null,
    orderNumber: `O000${i}`,
    status,
    customerCode: 'A000003',
    customerName: '张三',
    thirdSystemId: 'B00005',
    requireDate: '2018-09-10 11:00:00',
    contact: '张三',
    phone: '13822302819',
    address: '萧山区传化大厦',
    province: '浙江省杭州市',
    longitude: 120.0760657 + 0.4 * Math.random() - 0.5 * Math.random(),
    latitude: 30.296932 + 0.4 * Math.random() - 0.5 * Math.random(),
    goodsName: '快销品',
    goodsNumber: 11,
    goodsWeight: 0.011,
    goodsVolume: 11,
    delegateAmount: 3520,
    isReceipt: true,
    remark: '急',
    gmtCreate: '2018-09-15 10:32:50',
  });
}

export default {
  result: 'success',
  count: 18,
  data: list,
  code: 'EHD123456789123',
  msg: '查询成功 ',
};
