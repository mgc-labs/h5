const list = [];
const n = Math.random() * 100;
for (let i = 0; i < n; i++) {
  list.push({
    utmsPartOrderId: i + 1,
    orderNumber: `${(Math.random() * 1000).toFixed(0)}`,
    status: '待配载',
    customerCode: `A00000${i}`,
    customerName: `张${i}`,
    thirdSystemId: `B0000${i}`,
    requireDate: '2018-09-10 11:00:00',
    contact: `张${i}`,
    phone: '13822302819',
    address: '萧山区传化大厦',
    province: '浙江省杭州市',
    longitude: 120.358605,
    latitude: 30.265433,
    goodsName: '快销品',
    goodsNumber: 11,
    goodsWeight: 0.011,
    goodsVolume: 11,
    delegateAmount: 3520,
    isReceipt: true,
    remark: '急',
  });
}

export default {
  result: 'success',
  count: 18,
  data: {
    utmsPartGroupId: 1,
    name: 'G0001',
    customerName: '张三',
    realName: '李四',
    gmtCreate: '2018-09-10 18:31:22',
    orderCount: 6,
    weight: 4.8,
    volume: 5.2,
    status: '待发货',
    partOrders: list,
  },
  code: 'EHD123456789123',
  msg: '查询成功 ',
};
