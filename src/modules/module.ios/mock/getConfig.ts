export default {
  result: 'success',
  count: 18,
  data: {
    utmsGroupConfigId: 1,
    carStruct: '4.2米厢式货车',
    maxVolume: 0.3,
    maxWeight: 0.5,
    loadRate: 60,
    maxOrderNo: 20,
  },
  code: 'EHD123456789123',
  msg: '查询成功 ',
};
