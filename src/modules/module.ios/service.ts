/**
 * Auth Service
 * @author djd
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_GET_VEHICLE_REC =
  '/ehuodiGateway/utmsDispatch/utmsVehicleTypeInfocs/selectUtmsVehicleTypeInfo?a=1';

const API_GET_WAREHOUSE = '/ehuodiGateway/utmsCore/utmsDepotcs/selectDepotList';

const API_GET_LOC_ORDER =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/partOrderList?coordinateType=BD09';

const API_AUTO_GRO =
  '/ehuodiGateway/utmsTrade/utmsPartGroup/partOrderAutoGroup';

const API_CREATE_GROUP =
  '/ehuodiGateway/utmsTrade/utmsPartGroup/partOder2Group';
const API_ADDTO_GROUP =
  '/ehuodiGateway/utmsTrade/utmsPartGroup/partOderAddGroup';

const API_GET_GROUP = '/ehuodiGateway/utmsTrade/utmsPartGroup/partGroupList';

const API_GET_ORDERBYGROUP =
  '/ehuodiGateway/utmsTrade/utmsPartGroup/partGroupDetail';

const API_DELETE_ORDER_FROM_GROUP =
  '/ehuodiGateway/utmsTrade/utmsPartGroup/deleteFromGroup';

const API_MOVE_ORDER_TO_GROUP =
  '/ehuodiGateway/utmsTrade/utmsPartGroup/partOderMoveGroup';

const API_DELETE_GROUP = '/ehuodiGateway/utmsTrade/utmsPartGroup/deleteGroup';

const API_ORDER_REORDER = '/ehuodiGateway/utmsTrade/utmsPartGroup/adjustSort';

const API_SAVE_CONFIG =
  '/ehuodiGateway/utmsTrade/utmsGroupConfig/updateGroupConfig';

const API_GET_CONFIG =
  '/ehuodiGateway/utmsTrade/utmsGroupConfig/selectGroupConfig';

const API_SEND = '/ehuodiGateway/utmsTrade/utmsPartGroup/sendPartOrder?a=1';

const API_DELETE_ORDER =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/deletePartOrder';

const API_UPDATE_LOCATION =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/updateCoordinate';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码

  mocker
    .on('post', API_GET_VEHICLE_REC)
    .reply(() =>
      import('./mock/vehicleInfo').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_GET_WAREHOUSE)
    .reply(() =>
      import('./mock/getWarehouse').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_GET_LOC_ORDER)
    .reply(() =>
      import('./mock/locOrder').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_AUTO_GRO)
    .reply(() =>
      import('./mock/autoGro').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_CREATE_GROUP)
    .reply(() =>
      import('./mock/createGroup').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_ADDTO_GROUP)
    .reply(() =>
      import('./mock/addToGroup').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_GET_GROUP)
    .reply(() =>
      import('./mock/orderGroup').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_GET_ORDERBYGROUP)
    .reply(() =>
      import('./mock/orderListOfGroup').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_DELETE_ORDER_FROM_GROUP)
    .reply(() =>
      import('./mock/orderDeleteFromGroup').then(exports => [
        200,
        exports.default,
      ]),
    );

  mocker
    .on('post', API_ORDER_REORDER)
    .reply(() =>
      import('./mock/orderReorder').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_MOVE_ORDER_TO_GROUP)
    .reply(() =>
      import('./mock/moveOrderToGroup').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_DELETE_GROUP)
    .reply(() =>
      import('./mock/deleteOrderGroup').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_SAVE_CONFIG)
    .reply(() =>
      import('./mock/saveConfig').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_GET_CONFIG)
    .reply(() =>
      import('./mock/getConfig').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_SEND)
    .reply(() => import('./mock/send').then(exports => [200, exports.default]));

  mocker
    .on('post', API_UPDATE_LOCATION)
    .reply(() =>
      import('./mock/updateLocation').then(exports => [200, exports.default]),
    );
}

/**
 * 获取推荐车型
 */
export const askRecVehicle = params =>
  request(
    {
      method: 'post',
      url: API_GET_VEHICLE_REC,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 获取仓库列表
 */
export const askGetWarehouse = params =>
  request(
    {
      method: 'post',
      url: API_GET_WAREHOUSE,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 获取已定位订单列表
 */
export const askLocOrder = params =>
  request(
    {
      method: 'post',
      url: API_GET_LOC_ORDER,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);

/**
 * 自动配载
 */
export const askAutoGro = params =>
  request(
    {
      method: 'post',
      url: API_AUTO_GRO,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 手动配载
 */
export const askCreateGroup = params =>
  request(
    {
      method: 'post',
      url: API_CREATE_GROUP,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 添加到组
 */
export const askAddToGroup = params =>
  request(
    {
      method: 'post',
      url: API_ADDTO_GROUP,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 查询订单组列表
 */
export const askGetGroup = params =>
  request(
    {
      method: 'post',
      url: API_GET_GROUP,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 查询分组下的订单
 */
export const askGetOrderListOfGroup = params =>
  request(
    {
      method: 'post',
      url: API_GET_ORDERBYGROUP,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => {
    if (res.data === null) {
      return [];
    }
    if (res.data.partOrders === null) {
      return [];
    }
    return res.data.partOrders;
  });

/**
 * 从组里删除订单
 */
export const askDeleteOrderFromGroup = params =>
  request(
    {
      method: 'post',
      url: API_DELETE_ORDER_FROM_GROUP,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 调整顺序
 */
export const askOrderReorder = params =>
  request(
    {
      method: 'post',
      url: API_ORDER_REORDER,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 从组里移动订单到别的组
 */
export const askMoveOrderToGroup = params =>
  request(
    {
      method: 'post',
      url: API_MOVE_ORDER_TO_GROUP,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 删除订单组
 */
export const askDeleteGroup = params =>
  request(
    {
      method: 'post',
      url: API_DELETE_GROUP,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 获取配置
 */
export const askGetConfig = params =>
  request(
    {
      method: 'post',
      url: API_GET_CONFIG,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 保存配置
 */
export const askSaveConfig = params =>
  request(
    {
      method: 'post',
      url: API_SAVE_CONFIG,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 发货
 */
export const askSend = params =>
  request(
    {
      method: 'post',
      url: API_SEND,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

export const askDeleteOrder = params =>
  request(
    {
      method: 'post',
      url: API_DELETE_ORDER,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

// 位置修正
export const askUpdateOrderLocation = params =>
  request(
    {
      method: 'post',
      url: API_UPDATE_LOCATION,
      data: params,
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);
