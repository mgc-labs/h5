/**
 * Ios selectors
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectRecVehicleList = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('recVehicleList'),
  );

export const makeSelectWarehouseList = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('warehouseList'),
  );

export const makeSelectWarehouseLoaded = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('warehouseLoaded'),
  );

export const makeSelectDefaultWarehouse = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('defaultWarehouse'),
  );

export const makeSelectLocOrderList = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('locOrderList'),
  );

export const makeSelectLocOrderListSign = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('locOrderListSign'),
  );

export const makeSelectLocOrderListLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('locOrderListLoading'),
  );

export const makeSelectUnLocOrderData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('unLocOrderData'),
  );

export const makeSelectUnLocOrderListLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('unLocOrderListLoading'),
  );

export const makeSelectAutoGroLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('autoGroLoading'),
  );
export const makeSelectIsShowAutoGroConfirm = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('isShowAutoGroConfirm'),
  );

export const makeSelectCreateGroupLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('createGroupLoading'),
  );
export const makeSelectIsShowCreateGroup = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('isShowCreateGroup'),
  );

export const makeSelectAddToGroupLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('addToGroupLoading'),
  );
export const makeSelectIsShowAddToGroup = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('isShowAddToGroup'),
  );

export const makeSelectOrderGroupList = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('orderGroupList'),
  );
export const makeSelectOrderGroupLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('orderGroupLoading'),
  );

export const makeSelectSelectedGroupId = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('selectedGroupId'),
  );

export const makeSelectCheckedGroupIdList = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('checkedGroupIdList'),
  );

export const makeSelectOrderGroupShow = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('orderGroupShow'),
  );

export const makeSelectDeleteGroupLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('deleteGroupLoading'),
  );

export const makeSelectIsShowOrderListOfGroup = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('isShowOrderListOfGroup'),
  );

export const makeSelectGetOrderListLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('getOrderListLoading'),
  );

export const makeSelectOrderListOfGroup = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('orderListOfGroup'),
  );

export const makeSelectIsShowConfigModal = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('isShowConfigModal'),
  );

export const makeSelectGetConfigLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('getConfigLoading'),
  );

export const makeSelectConfig = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('config'));

export const makeSelectSaveConfigLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('saveConfigLoading'),
  );

export const makeSelectOrderGroupSendLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('orderGroupSendLoading'),
  );

export const makeSelectDeleteOrderLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('deleteOrderLoading'),
  );

export const makeSelectMoveOrderLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('moveOrderLoading'),
  );
