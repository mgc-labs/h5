/**
 * Ios Actions
 * @author djd
 * @date 2018/9/15 下午5:43:33
 */
import { AnyAction } from 'redux';

export default CONSANTS => ({
  getInitData: (): AnyAction => ({
    type: CONSANTS.GET_INIT_DATA,
  }),

  // 推荐车型
  getRecVehicle: (): AnyAction => ({
    type: CONSANTS.GET_VEHICLE_REC,
  }),
  getRecVehicleDone: (data): AnyAction => ({
    type: CONSANTS.GET_VEHICLE_REC_SUCCESS,
    payload: data,
  }),
  getRecVehicleError: (error): AnyAction => ({
    type: CONSANTS.GET_VEHICLE_REC_ERROR,
    error,
  }),

  // 自动配载
  autoGroOpen: (): AnyAction => ({
    type: CONSANTS.AUTO_GRO_OPEN,
  }),
  autoGropClose: (): AnyAction => ({
    type: CONSANTS.AUTO_GRO_CLOSE,
  }),
  autoGro: (data): AnyAction => ({
    type: CONSANTS.AUTO_GRO,
    payload: data,
  }),
  autoGroDone: (): AnyAction => ({
    type: CONSANTS.AUTO_GRO_SUCCESS,
  }),
  autoGroError: (error): AnyAction => ({
    type: CONSANTS.AUTO_GRO_ERROR,
    error,
  }),

  // 手动配载
  createGroupOpen: (): AnyAction => ({
    type: CONSANTS.CREATE_GROUP_OPEN,
  }),
  createGroupClose: (): AnyAction => ({
    type: CONSANTS.CREATE_GROUP_CLOSE,
  }),
  createGroup: (data): AnyAction => ({
    type: CONSANTS.CREATE_GROUP,
    payload: data,
  }),
  createGroupDone: (): AnyAction => ({
    type: CONSANTS.CREATE_GROUP_SUCCESS,
  }),
  createGroupError: (error): AnyAction => ({
    type: CONSANTS.CREATE_GROUP_ERROR,
    error,
  }),

  // 添加到组
  addToGroupOpen: (): AnyAction => ({
    type: CONSANTS.ADDTO_GROUP_OPEN,
  }),
  addToGroupClose: (): AnyAction => ({
    type: CONSANTS.ADDTO_GROUP_CLOSE,
  }),
  addToGroup: (data): AnyAction => ({
    type: CONSANTS.ADDTO_GROUP,
    payload: data,
  }),
  addToGroupDone: (): AnyAction => ({
    type: CONSANTS.ADDTO_GROUP_SUCCESS,
  }),
  addToGroupError: (error): AnyAction => ({
    type: CONSANTS.ADDTO_GROUP_ERROR,
    error,
  }),

  // 获取仓库列表
  getWarehouse: (): AnyAction => ({
    type: CONSANTS.GET_WAREHOUSE,
  }),

  getWarehouseDone: (data): AnyAction => ({
    type: CONSANTS.GET_WAREHOUSE_SUCCESS,
    payload: data,
  }),

  getWarehouseError: (error): AnyAction => ({
    type: CONSANTS.GET_WAREHOUSE_ERROR,
    error,
  }),

  // 订单组列表
  getGroup: (): AnyAction => ({
    type: CONSANTS.GET_GROUP,
  }),

  getGroupDone: (data): AnyAction => ({
    type: CONSANTS.GET_GROUP_SUCCESS,
    payload: data,
  }),

  getGroupError: (error): AnyAction => ({
    type: CONSANTS.GET_GROUP_ERROR,
    error,
  }),

  // 组删除
  deleteGroup: (data): AnyAction => ({
    type: CONSANTS.GROUP_DELETE,
    payload: data,
  }),
  deleteGroupDone: (data): AnyAction => ({
    type: CONSANTS.GROUP_DELETE_SUCCESS,
    payload: data,
  }),
  deleteGroupError: (error): AnyAction => ({
    type: CONSANTS.GROUP_DELETE_ERROR,
    error,
  }),

  // 发货
  send: (data): AnyAction => ({
    type: CONSANTS.GROUP_SEND,
    payload: data,
  }),
  sendDone: (): AnyAction => ({
    type: CONSANTS.GROUP_SEND_SUCCESS,
  }),
  sendError: (error): AnyAction => ({
    type: CONSANTS.GROUP_SEND_ERROR,
    error,
  }),

  // 获取配置
  getConfig: (): AnyAction => ({
    type: CONSANTS.GET_CONFIG,
  }),
  getConfigDone: (data): AnyAction => ({
    type: CONSANTS.GET_CONFIG_SUCCESS,
    payload: data,
  }),
  getConfigError: (error): AnyAction => ({
    type: CONSANTS.GET_CONFIG_ERROR,
    error,
  }),

  // 保存配置
  saveConfig: (data): AnyAction => ({
    type: CONSANTS.CONFIG_SAVE,
    payload: data,
  }),
  saveConfigDone: (data): AnyAction => ({
    type: CONSANTS.CONFIG_SAVE_SUCCESS,
  }),
  saveConfigError: (error): AnyAction => ({
    type: CONSANTS.CONFIG_SAVE_ERROR,
    error,
  }),
  saveConfigCancel: (): AnyAction => ({
    type: CONSANTS.CONFIG_CANCEL,
  }),

  // 订单列表显示状态变化
  groupShow: (show): AnyAction => ({
    type: CONSANTS.GROUP_SHOW,
    payload: show,
  }),

  // 订单组列表 选择
  groupCheck: (data): AnyAction => ({
    type: CONSANTS.GROUP_CHECK,
    payload: data,
  }),
  groupAllCheck: (data): AnyAction => ({
    type: CONSANTS.GROUP_ALL_CHECK,
    payload: data,
  }),

  // 订单组，选中一个组时
  groupSelected: (record): AnyAction => ({
    type: CONSANTS.GROUP_SELECT,
    payload: record,
  }),

  // 获取一个组下的订单
  getOrderListOfGroup: (groupId): AnyAction => ({
    type: CONSANTS.GET_ORDER_OF_GROUP,
    payload: groupId,
  }),
  getOrderListOfGroupDone: (data): AnyAction => ({
    type: CONSANTS.GET_ORDER_OF_GROUP_SUCCESS,
    payload: data,
  }),
  getOrderListOfGroupError: (error): AnyAction => ({
    type: CONSANTS.GET_ORDER_OF_GROUP_ERROR,
    error,
  }),
  orderListHide: (): AnyAction => ({
    type: CONSANTS.ORDER_HIDE,
  }),

  // 订单手工排序
  orderReOrder: (data): AnyAction => ({
    type: CONSANTS.ORDER_REORDER,
    payload: data,
  }),
  orderReOrderDone: (data): AnyAction => ({
    type: CONSANTS.ORDER_REORDER_SUCCESS,
    payload: data,
  }),
  orderReOrderError: (error): AnyAction => ({
    type: CONSANTS.ORDER_REORDER_ERROR,
    error,
  }),

  // 订单组列表 选择
  orderCheck: (data): AnyAction => ({
    type: CONSANTS.ORDER_CHECK,
    payload: data,
  }),
  orderAllCheck: (data): AnyAction => ({
    type: CONSANTS.ORDER_ALL_CHECK,
    payload: data,
  }),
  // 从组里把订单删除
  deleteOrderFromGroup: (data): AnyAction => ({
    type: CONSANTS.ORL_DELETE_FROM_GROUP,
    payload: data,
  }),
  deleteOrderFromGroupDone: (): AnyAction => ({
    type: CONSANTS.ORL_DELETE_FROM_GROUP_S,
  }),
  deleteOrderFromGroupError: (error): AnyAction => ({
    type: CONSANTS.ORL_DELETE_FROM_GROUP_E,
    error,
  }),

  // 从组里将订单移到别的组
  moveOrderToGroup: (data): AnyAction => ({
    type: CONSANTS.ORL_MOVE_TO_GROUP,
    payload: data,
  }),
  moveOrderToGroupDone: (): AnyAction => ({
    type: CONSANTS.ORL_MOVE_TO_GROUP_SUCCESS,
  }),
  moveOrderToGroupError: (error): AnyAction => ({
    type: CONSANTS.ORL_MOVE_TO_GROUP_ERROR,
    error,
  }),

  // 已定位订单列表
  getLocOrder: (): AnyAction => ({
    type: CONSANTS.GET_LOC_ORDER,
  }),

  getLocOrderDataDone: (data): AnyAction => ({
    type: CONSANTS.GET_LOC_ORDER_SUCCESS,
    payload: data,
  }),

  getLocOrderDataError: (error): AnyAction => ({
    type: CONSANTS.GET_LOC_ORDER_ERROR,
    error,
  }),

  locOrderSelect: (list): AnyAction => ({
    type: CONSANTS.LOC_ORDER_SELECTED,
    payload: list,
  }),

  // 未定位订单列表
  getUnLocOrder: (pars): AnyAction => ({
    type: CONSANTS.GET_UNLOC_ORDER,
    payload: pars,
  }),

  getUnLocOrderDone: (data): AnyAction => ({
    type: CONSANTS.GET_UNLOC_ORDER_SUCCESS,
    payload: data,
  }),

  getUnLocOrderError: (error): AnyAction => ({
    type: CONSANTS.GET_UNLOC_ORDER_ERROR,
    error,
  }),

  deleteOrder: (pars): AnyAction => ({
    type: CONSANTS.ORDER_DELETE,
    payload: pars,
  }),
  deleteOrderDone: (): AnyAction => ({
    type: CONSANTS.ORDER_DELETE_SUCCESS,
  }),
  deleteOrderError: (error): AnyAction => ({
    type: CONSANTS.ORDER_DELETE_ERROR,
    error,
  }),

  gotoEditOrder: (data): AnyAction => ({
    type: CONSANTS.GOTO_EDIT_ORDER,
    payload: data,
  }),
  gotoOrderDetail: (data): AnyAction => ({
    type: CONSANTS.GOTO_ORDER_DETAIL,
    payload: data,
  }),
  gotoCreateWarehouse: (data): AnyAction => ({
    type: CONSANTS.GOTO_CREATEWAREHOUSE,
  }),

  updateOrderLocation: (data): AnyAction => ({
    type: CONSANTS.UPDATE_UNLOC_LOCATION,
    payload: data,
  }),

  updateOrderLocationDone: (): AnyAction => ({
    type: CONSANTS.UPDATE_UNLOC_LOCATION_S,
  }),

  updateOrderLocationError: (error): AnyAction => ({
    type: CONSANTS.UPDATE_UNLOC_LOCATION_E,
    error,
  }),
});
