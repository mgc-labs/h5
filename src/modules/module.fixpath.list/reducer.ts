/**
 * FixpathList Reducers
 * @author ryan bian
 * @date 2018-9-14 16:12:09
 */

import { fromJS, List, Map } from 'immutable';

// The initial state of the App
const initialState = Map({
  data: List(),
  dataCount: 0,
  filter: Map(),
  error: false,
  loading: false,
  pageIndex: 0,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_LIST:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_LIST_SUCCESS:
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data || []))
          .set('dataCount', action.payload.count)
          .set('pageIndex', action.meta);
      case CONSANTS.GET_LIST_ERROR:
        return state.set('error', action.error).set('loading', false);
      case CONSANTS.UPDATE_FILTER:
        return state.set('filter', Map(action.payload)).set('pageIndex', 0);
      default:
        return state;
    }
  };
};
