/**
 * FixpathList Constants
 * @author ryan bian
 * @date 2018-9-14 16:12:09
 */
export const GET_LIST = 'FIXPATH_LIST/GET_LIST';
export const GET_LIST_SUCCESS = 'FIXPATH_LIST/GET_LIST_SUCCESS';
export const GET_LIST_ERROR = 'FIXPATH_LIST/GET_LIST_ERROR';
export const UPDATE_FILTER = 'FIXPATH_LIST/UPDATE_FILTER';
export const SET_PAGE_INDEX = 'FIXPATH_LIST/SET_PAGE_INDEX';
