/**
 * FixpathList Actions
 * @author ryan bian
 * @date 2018-9-14 16:12:09
 */
import { AnyAction } from 'redux';

export default CONSANTS => ({
  getList: (options, pageIndex): AnyAction => ({
    payload: options,
    meta: pageIndex,
    type: CONSANTS.GET_LIST,
  }),

  getListDone: (data, pageIndex): AnyAction => ({
    payload: data,
    meta: pageIndex,
    type: CONSANTS.GET_LIST_SUCCESS,
  }),

  getListError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSANTS.GET_LIST_ERROR,
  }),

  updateFilter: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.UPDATE_FILTER,
  }),
});
