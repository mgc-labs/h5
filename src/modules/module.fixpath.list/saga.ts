/**
 * FixpathList Saga
 * @author ryan bian
 * @date 2018-9-14 16:12:09
 */
import message from 'antd/es/message';
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

import { getResources } from './service';

export default (CONSTANTS, actions, routeId) => {
  function* getData(action) {
    const { payload, meta } = action;
    try {
      const filterData = yield select(state =>
        state.getIn([routeId, 'filter']),
      );
      const data = yield call(
        getResources,
        Object.assign(
          filterData.toJS(),
          {
            skipCount: 0,
            pageSize: 15,
          },
          payload,
        ),
      );
      yield put(actions.getListDone(data, meta));
    } catch (err) {
      yield put(actions.getListError(err));
      message.error(err.toString());
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_LIST, getData);
    yield takeEvery(CONSTANTS.UPDATE_FILTER, getData);
  };
};
