/**
 * FixpathList Component
 * @author ryan bian
 * @date 2018-9-14 16:12:09
 */
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Divider from 'antd/es/divider';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import Table from 'components/MyTable';
import { List, Map } from 'immutable';
import msngr from 'msngr';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectData,
  makeSelectDataCount,
  makeSelectError,
  makeSelectLoading,
  makeSelectPageIndex,
} from './selectors';

import Badge from 'antd/es/badge';
import styles from './index.module.less';

const FormItem = Form.Item;
const Option = Select.Option;
export interface IFixpathListProps {
  children: React.ReactChildren;
  dataCount: number;
  form: WrappedFormUtils;
  list: List<Map<string, any>>;
  loading: boolean;
  pageIndex: number;
  permissions: List<Map<string, any>>;
  doSearch: (options?: object, pageIndex?: number) => any;
  updateFilter: (data?: object) => any;
  refresh: (data?) => any;
}
interface IFixpathListState {
  permissions: any[];
}

class FixpathList extends React.PureComponent<
  IFixpathListProps,
  IFixpathListState
> {
  constructor(props) {
    super(props);
    this.state = {
      permissions: [],
    };
  }
  public componentDidMount() {
    this.setStatus();
    this.props.doSearch();
    this.handlePermissions();
    msngr('/fixpath/list', 'refresh').on(d => {
      this.props.doSearch();
    });
  }

  public componentWillUnmount() {
    msngr('/fixpath/list', 'refresh').drop(d => {
      this.props.doSearch();
    });
  }
  // 取权限
  public handlePermissions() {
    const { permissions } = this.props;
    this.setState({
      permissions: permissions.toArray().map(data => data.get('operateKey')),
    });
  }
  // 查询
  public handleSubmit = e => {
    e.preventDefault();
    this.props.updateFilter(this.props.form.getFieldsValue());
  };
  // 搜索栏重置
  public handleReset = () => {
    this.props.form.resetFields();
    this.props.updateFilter();
    this.setStatus();
  };
  // 设置状态为'全部'
  public setStatus() {
    const { setFieldsValue } = this.props.form;
    setFieldsValue({ status: '' });
  }
  // render搜索栏
  public renderFilterForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form
        className={styles.FixpathList__Form}
        layout="inline"
        onSubmit={this.handleSubmit}
      >
        <Row type="flex">
          <Col span={24}>
            <FormItem label="线路名称" colon={false}>
              {getFieldDecorator('routeName')(
                <Input style={{ width: '165px' }} />,
              )}
            </FormItem>
            <FormItem label="客户名称" colon={false}>
              {getFieldDecorator('customerName')(
                <Input style={{ width: '165px' }} />,
              )}
            </FormItem>
            <FormItem label="状态" colon={false}>
              {getFieldDecorator('status')(
                <Select>
                  <Option value="">全部</Option>
                  <Option value="1">生效中</Option>
                  <Option value="0">失效</Option>
                </Select>,
              )}
            </FormItem>
            <FormItem className={styles.FixpathList__FormAction}>
              <Button
                type="primary"
                htmlType="submit"
                style={{ marginRight: '12px' }}
              >
                查 询
              </Button>
              <Button onClick={this.handleReset}>重 置</Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  public renderList() {
    const { dataCount, doSearch, list, loading, pageIndex } = this.props;
    const statusMap = ['default', 'success'];
    const status = ['失效', '生效中'];
    const { permissions } = this.state;
    const showDivider =
      permissions.includes('/fixpath/updatepath') &&
      permissions.includes('/fixpath/schedule');
    const tableProps = {
      columns: [
        {
          title: '路线名称',
          dataIndex: 'routeName',
          render: (text, record) =>
            permissions.includes('/fixpath/detail') ? (
              <Link
                to={`/fixpath/detail?id=${record.routeId}`}
                onClick={() => {
                  this.props.refresh(`/fixpath/detail?id=${record.routeId}`);
                }}
              >
                {text}
              </Link>
            ) : (
              <span>{text}</span>
            ),
        },
        {
          title: '状态',
          dataIndex: 'status',
          render: val => <Badge status={statusMap[val]} text={status[val]} />,
        },
        {
          title: '客户名称',
          dataIndex: 'customerName',
        },
        {
          title: '添加时间',
          dataIndex: 'createDate',
        },
        {
          title: '联系电话',
          dataIndex: 'phone',
        },
        {
          title: '操作',
          key: 'action',
          render(item) {
            return (
              <React.Fragment>
                {permissions.includes('/fixpath/updatepath') ? (
                  <Link to={`/fixpath/updatepath?id=${item.routeId}`}>
                    修改
                  </Link>
                ) : (
                  ''
                )}
                {showDivider ? <Divider type="vertical" /> : ''}
                {permissions.includes('/fixpath/schedule') ? (
                  <Link to={`/fixpath/schedule?routeId=${item.routeId}`}>
                    运力计划
                  </Link>
                ) : (
                  ''
                )}
              </React.Fragment>
            );
          },
        },
      ],
      dataSource: list
        .map(d => ({
          key: d.get('routeId'),
          routeName: d.get('routeName'),
          status: d.get('status'),
          customerName: d.get('customerName'),
          routeId: d.get('routeId'),
          createDate: d.get('createDate'),
          phone: d.getIn(['routeWayPoints', 0, 'phone']),
        }))
        .toArray(),
      loading,
      pagination: {
        current: pageIndex,
        total: dataCount,
        onChange(pageData, index) {
          doSearch(pageData, index);
        },
        onShowSizeChange(data) {
          doSearch({
            skipCount: data.skipCount,
            pageSize: data.pageSize,
          });
        },
      },
    };
    return <Table {...tableProps} />;
  }
  public render() {
    const { permissions } = this.state;
    return (
      <Card
        title="固定线路"
        bordered={true}
        className={styles.FixpathList}
        extra={
          permissions.includes('/fixpath/addpath') ? (
            <Link to="/fixpath/addpath">
              <Button type="primary">新建线路</Button>
            </Link>
          ) : (
            ''
          )
        }
      >
        {this.renderFilterForm()}
        {this.renderList()}
      </Card>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    doSearch: (options, pageIndex) =>
      dispatch(actions.getList(options, pageIndex)),
    updateFilter: data => dispatch(actions.updateFilter(data)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    list: makeSelectData(selectState),
    dataCount: makeSelectDataCount(selectState),
    pageIndex: makeSelectPageIndex(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(
    withConnect,
    Form.create(),
  )(FixpathList);
};
