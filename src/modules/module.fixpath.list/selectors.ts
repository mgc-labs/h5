/**
 * FixpathList selectors
 * @author ryan bian
 * @date 2018-9-14 16:12:09
 */
import { List, Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('data', List()),
  );

export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('dataCount'),
  );

export const makeSelectPageIndex = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('pageIndex'),
  );
