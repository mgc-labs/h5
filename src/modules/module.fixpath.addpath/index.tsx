/**
 * FixpathAddpath Component
 * @author lhf
 * @date 2018-9-20 09:54:47
 */
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import CarModelCarLong from 'components/CarModelCarLong';
import CustomerSelect from 'components/CustomerSelect';
import LoadInfoEdit from 'components/LoadInfoEdit';
import { PageBottom, PageBottomButtons } from 'components/PageBottom';
import { Map } from 'immutable';
import AddOrderCommonInfo from 'modules/module.vehicle.order.add/AddOrderCommonInfo';
import * as React from 'react';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
  makeSelectSaveData,
} from './selectors';

import Spin from 'antd/es/spin';
import styles from './index.module.less';

const FormItem = Form.Item;
const confirm = Modal.confirm;
export interface IFixpathAddpathProps {
  children: React.ReactChildren;
  loading: boolean;
  form: WrappedFormUtils;
  searchParams: {
    id: number;
  };
  routeData: Map<string, any>;
  saveData: string;
  getRoute: (routeId: number) => any;
  saveRouteConfig: (data) => void;
  toList: () => void;
  close: () => void;
  toSchedule: (routeId: number) => void;
  refresh: (data?) => any;
  eventEmitter: object;
}
interface IFixpathAddpathState {
  loadType: number; // load/unload，当前选择的是装还是卸的地址,0-装货，1-卸货
  loadIndex: number; // 装卸的index
  loadData: {};
  unloadData: object[];
  selectCustomer: string;
}
const gutter = 16;
const colLayProps = {
  sm: 24,
  xs: 24,
  md: 6,
  lg: 6,
  xl: 6,
};

class FixpathAddpath extends React.PureComponent<
  IFixpathAddpathProps,
  IFixpathAddpathState
> {
  selectedAddress: any;
  constructor(props) {
    super(props);
    this.state = {
      loadIndex: 0,
      loadType: 0,
      loadData: {},
      unloadData: [],
      selectCustomer: '',
    };
  }
  public componentDidMount() {
    const { searchParams, form } = this.props;
    if (searchParams.id) {
      this.props.getRoute(Number(searchParams.id));
    }
    this.props.eventEmitter.on('close', (requestClose, eventSource) => {
      if (
        eventSource === 'FROM_TAB_CLOSE' ||
        eventSource === 'FROM_TAB_REFRESH'
      ) {
        this.cancleEdit(requestClose);
      } else {
        requestClose();
      }
    });
  }

  public componentWillReceiveProps(nextProps) {
    if (nextProps.saveData && nextProps.saveData !== this.props.saveData) {
      const { id } = this.props.searchParams;
      this.toSchedule(id ? id : nextProps.saveData);
    }
    if (nextProps.routeData.size && !this.props.routeData.size) {
      this.setFormData(nextProps);
    }
  }

  public render() {
    const { loading } = this.props;
    return (
      <div className={styles.FixpathAddpath}>
        <Spin spinning={loading}>
          <Form layout="horizontal">
            {this.renderBasicInfo()}
            <AddOrderCommonInfo form={this.props.form} />
            <PageBottom
              rightChild={
                <PageBottomButtons
                  buttons={[
                    <Button
                      key={1}
                      onClick={() => {
                        this.cancleEdit();
                      }}
                    >
                      取消
                    </Button>,
                    <Button
                      key={2}
                      type="primary"
                      htmlType="submit"
                      onClick={() => {
                        this.onHandleSubmit();
                      }}
                    >
                      保存线路
                    </Button>,
                  ]}
                />
              }
            />
          </Form>
        </Spin>
      </div>
    );
  }
  private renderBasicInfo() {
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    const { loadData, unloadData } = this.state;
    const loadInfoProps = {
      closeAddressModal: (data, formData, loadtype) => {
        this.closeAddressModal(data, formData, loadtype);
      },
      showAddressModal: (type, index) => {
        this.showAddressModal(type, index);
      },
      form: this.props.form,
    };
    return (
      <Card title="基本信息">
        <Row gutter={gutter}>
          <Col {...colLayProps}>
            <FormItem label="" type="hidden" style={{ margin: 0 }}>
              {getFieldDecorator('customerCode')(<Input type="hidden" />)}
            </FormItem>
            <FormItem label="选择客户">
              {getFieldDecorator('customerName', {
                rules: [
                  {
                    required: true,
                    message: '请选择客户',
                  },
                ],
              })(<CustomerSelect onChange={this.onChangeCustomer} />)}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="线路名称">
              {getFieldDecorator('routeName', {
                rules: [
                  {
                    required: true,
                    message: '请输入线路名称',
                  },
                ],
              })(<Input placeholder="线路名称" maxLength={50} />)}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="所需车型" required>
              {getFieldDecorator('carStruct', {
                rules: [
                  {
                    required: true,
                    message: '请选择车型',
                  },
                ],
              })(
                <CarModelCarLong
                  placeholder="请选择车型"
                  maxSelect={1}
                  style={{ marginTop: '6px' }}
                />,
              )}
            </FormItem>
          </Col>
        </Row>

        <LoadInfoEdit
          fields={{ name: 'load', value: [loadData] }}
          loadtype={0}
          {...loadInfoProps}
        />

        <LoadInfoEdit
          fields={{ name: 'unload', value: unloadData }}
          loadtype={1}
          {...loadInfoProps}
          onDele={key => {
            this.deleUnloadData(key);
          }}
        />
      </Card>
    );
  }

  // 选择客户
  private onChangeCustomer = data => {
    const { selectCustomer } = this.state;
    if (selectCustomer === '') {
      this.setState({ selectCustomer: data });
    }
  };

  // 显示常用地址，设置当前loadtype和index
  private showAddressModal(loadType: number, index) {
    this.setState({ loadType, loadIndex: index });
  }
  // 关闭常用地址弹出框，填充数据
  private closeAddressModal(data, formData, loadtype) {
    const { form } = this.props;
    const { loadIndex, unloadData } = this.state;
    if (loadtype === 0) {
      form.setFieldsValue({
        load: [formData],
      });
      this.setState({ loadData: data });
    } else if (loadtype === 1) {
      unloadData[loadIndex] = data;
      const unloadData2 = form.getFieldValue('unload');
      unloadData2[loadIndex] = formData;
      form.setFieldsValue({
        unload: unloadData2,
      });
      this.setState({ unloadData });
    }
  }

  // 删除卸货信息
  private deleUnloadData(key) {
    const { unloadData } = this.state;
    unloadData[key] = null;
    this.setState({ unloadData });
  }

  // 确认按钮
  private onHandleSubmit() {
    const { form } = this.props;
    form.validateFields((errors, values) => {
      if (!errors) {
        const data = this.getSubmitData();
        this.props.saveRouteConfig(data);
      }
    });
  }

  // 获取提交的数据
  private getSubmitData() {
    const { loadData, unloadData } = this.state;
    const { getFieldsValue } = this.props.form;
    const formData = getFieldsValue();
    const points = [Object.assign({}, formData.load[0], loadData)].concat(
      unloadData.map((value, index) => {
        if (value && formData.unload[index]) {
          return Object.assign({}, formData.unload[index], value);
        }
      }),
    );
    const customer = formData.customerName;
    const data = Object.assign(formData, {
      carLong: formData.carStruct[0].vehicleLong,
      carStruct: formData.carStruct[0].vehicleType,
      routeWayPoints: JSON.stringify(points).toString(),
    });
    delete data.load;
    delete data.unload;
    delete data.unloadKeys;

    if (customer && customer.indexOf(',') > 0) {
      return Object.assign(data, {
        customerCode: customer.split(',')[0],
        customerName: customer.split(',')[1],
      });
    } else {
      return data;
    }
  }

  // 取消页面编辑
  private cancleEdit(requestClose?) {
    const { form, searchParams, routeData } = this.props;
    const { selectCustomer } = this.state;
    const keys = form.getFieldValue('unloadKeys');
    const comparativeFields = [
      'routeName',
      'load',
      'load[0].pointAddress',
      'load[0].contact',
      'load[0].phone',
      'unload',
      'goodsName',
      'goodsWeight',
      'goodsVolume',
      'goodsNumber',
      'isReceipt',
      'collictionAmount',
      'customerFreight',
      'driverFreight',
    ];
    keys.map(d => {
      comparativeFields.push(`unload[${d}].pointAddress`);
      comparativeFields.push(`unload[${d}].contact`);
      comparativeFields.push(`unload[${d}].phone`);
    });
    let showConfirm = false;
    if (!searchParams.id) {
      comparativeFields.push('carStruct');
      if (
        selectCustomer !== form.getFieldValue('customerName') ||
        form.isFieldsTouched(comparativeFields)
      ) {
        showConfirm = true;
      }
    } else {
      const propsCar =
        routeData.get('carLong') / 1000 + '米' + routeData.get('carStruct');
      if (
        form.isFieldsTouched(comparativeFields) ||
        propsCar !== form.getFieldValue('carStruct')[0].value
      ) {
        showConfirm = true;
      }
    }
    if (showConfirm) {
      confirm({
        title: '有未保存的信息',
        content: '如放弃，填写的信息将丢失',
        okText: '继续填写',
        cancelText: '放弃',
        centered: true,
        onCancel: () => {
          if (requestClose) {
            requestClose();
          } else {
            this.toList();
          }
        },
      });
    } else {
      if (requestClose) {
        requestClose();
      } else {
        this.toList();
      }
    }
  }

  private toList() {
    this.props.toList();
    this.props.close();
  }

  // 开单成功之后
  private toSchedule(id) {
    const isEdit = this.props.searchParams.id ? true : false;
    confirm({
      title: `${isEdit ? '修改' : '创建'}成功`,
      content: `线路${isEdit ? '修改' : '创建'}成功，是否立刻设置运力计划`,
      okText: '设置运力',
      cancelText: '暂不设置',
      centered: true,
      iconType: 'check-circle',
      onOk: () => {
        this.props.toSchedule(id);
        this.props.refresh('/fixpath/list');
        this.props.close();
      },
      onCancel: () => {
        this.props.toList();
        this.props.refresh('/fixpath/list');
        this.props.close();
      },
    });
  }

  // 修改的情况下，设置formdata
  private setFormData(props) {
    const { routeData, form } = props;
    const unload = routeData
      .get('routeWayPoints')
      .filter(d => d.get('loadType') === 1 || d.get('loadType') === '1')
      .toJS();
    const load = routeData
      .get('routeWayPoints')
      .filter(d => d.get('loadType') === 0 || d.get('loadType') === '0')
      .toJS()[0];
    const carString =
      routeData.get('carLong') / 1000 + '米' + routeData.get('carStruct');
    if (routeData.size) {
      form.setFieldsValue({
        routeName: routeData.get('routeName'),
        customerName: routeData.get('customerName'),
        customerCode: routeData.get('customerCode'),
        carStruct: [{ value: carString }],
        goodsName: routeData.get('goodsName'),
        goodsNumber: routeData.get('goodsNumber'),
        goodsVolume: routeData.get('goodsVolume'),
        goodsWeight: routeData.get('goodsWeight'),
        isReceipt: routeData.get('isReceipt').toString(),
        customerFreight: routeData.get('customerFreight'),
        driverFreight: routeData.get('driverFreight'),
        collictionAmount: routeData.get('collictionAmount'),
        unloadKeys: unload.map((data, index) => index),
        load: [
          {
            pointAddress: load.pointAddress,
            phone: load.phone,
            contact: load.contact,
          },
        ],
      });
      setTimeout(() => {
        form.setFieldsValue({
          unload: unload.map(data => ({
            pointAddress: data.pointAddress,
            phone: data.phone,
            contact: data.contact,
          })),
        });
      }, 0);
      this.setState({
        loadData: load,
        unloadData: unload,
      });
    }
  }
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        routeData: makeSelectData(currentState),
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
        saveData: makeSelectSaveData(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getRoute: options => dispatch(actions.getRoute(options)),
        saveRouteConfig: options => dispatch(actions.saveRouteConfig(options)),
        toList: options => dispatch(actions.toList(options)),
        toSchedule: options => dispatch(actions.toSchedule(options)),
      }),
    };
  })(Form.create()(FixpathAddpath));
};
