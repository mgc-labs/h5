/**
 * fixpath.list Service
 * @author ryan bian
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_ADD_ROUTE = '/ehuodiGateway/utmsDispatch/routecs/addRoute';
const API_UPDATE_ROUTE = '/ehuodiGateway/utmsDispatch/routecs/updateRoute';
const API_GET_ROUTE = '/ehuodiGateway/utmsDispatch/routecs/queryById';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_GET_ROUTE)
    .reply(() => import('./mock/mock').then(exports => [200, exports.default]));
}

// 新增线路
export const addRoute = data => {
  return request(
    {
      method: 'post',
      url: API_ADD_ROUTE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
// 更新线路
export const updateRoute = data => {
  return request(
    {
      method: 'post',
      url: API_UPDATE_ROUTE,
      data: qs.stringify(data),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  );
};
// 查询线路
export const getRoute = data => {
  return request(
    {
      method: 'post',
      url: API_GET_ROUTE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};
