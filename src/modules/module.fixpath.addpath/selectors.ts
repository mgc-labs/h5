/**
 * FixpathAddpath selectors
 * @author lhf
 * @date 2018-9-20 09:54:47
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => {
    return state.get('data');
  });

export const makeSelectSaveData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('saveData'),
  );
