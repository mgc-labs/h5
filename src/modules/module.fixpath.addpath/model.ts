import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { fromJS, List, Map } from 'immutable';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { addRoute, getRoute, updateRoute } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: Map(),
      error: false,
      loading: false,
      saveData: '',
    }),
    actions: {
      GET_ROUTE: routeId => routeId,
      GET_ROUTE_ERROR: error => error,
      GET_ROUTE_SUCCESS: data => data,
      SAVE_ROUTE_CONFIG: data => data,
      SAVE_ROUTE_CONFIG_DONE: data => data,
      SAVE_ROUTE_CONFIG_ERROR: error => error,
      TO_LIST: () => null,
      TO_SCHEDULE: routeId => routeId,
    },
    reducers: {
      GET_ROUTE(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_ROUTE_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_ROUTE_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data));
      },
      SAVE_ROUTE_CONFIG(state, action) {
        return state.set('loading', true);
      },
      SAVE_ROUTE_CONFIG_DONE(state, action) {
        return state.set('loading', false).set('saveData', action.payload);
      },
      SAVE_ROUTE_CONFIG_ERROR(state, action) {
        return state.set('loading', false).set('error', action.payload);
      },
      TO_LIST(state) {
        return state
          .set('error', false)
          .set('loading', false)
          .set('saveData', '');
      },
      TO_SCHEDULE(state) {
        return state
          .set('error', false)
          .set('loading', false)
          .set('saveData', '');
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const data = yield call(getRoute, { routeId: action.payload });
          yield put(actions.getRouteSuccess(data));
        } catch (err) {
          yield put(actions.getRouteError(err));
          message.error(err.toString());
        }
      }

      function* submitRoute(action) {
        const data = yield select(state => state.getIn([namespace, 'data']));
        const routeId = data.get('routeId');
        const updateData = Object.assign({ routeId }, action.payload);
        try {
          const resultData = routeId
            ? yield call(updateRoute, updateData)
            : yield call(addRoute, action.payload);
          if (resultData.code) {
            message.error(resultData.msg);
            yield put(actions.saveRouteConfigError(resultData.msg));
          } else {
            yield put(actions.saveRouteConfigDone(resultData.data));
          }
        } catch (err) {
          message.error(err.toString());
        }
      }

      function* toList() {
        yield put(push('/fixpath/list'));
      }

      function* toSchedule(action) {
        yield put(push('/fixpath/schedule?routeId=' + action.payload));
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ROUTE, fetchData);
        yield takeLatest(CONSTANTS.SAVE_ROUTE_CONFIG, submitRoute);
        yield takeLatest(CONSTANTS.TO_LIST, toList);
        yield takeLatest(CONSTANTS.TO_SCHEDULE, toSchedule);
      };
    },
  });
