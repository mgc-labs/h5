/**
 * waybill Saga
 * @author yanw
 * @date 2018-9-14 16:12:09
 */
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

import { cancleTradeService, getResources, getStatistics } from './service';

export default (CONSTANTS, actions, routeId) => {
  function* getData(action) {
    const { payload, meta } = action;
    try {
      // const filterData = yield select(state =>
      //   state.getIn([routeId, 'filter']),
      // );
      const data = yield call(
        getResources,
        payload,
        // Object.assign(
        //   // filterData.toJS(),
        //   {
        //     skipCount: 0,
        //     pageSize: 15,
        //   },
        //   payload,
        // ),
      );
      yield put(actions.getListDone(data, meta));
    } catch (err) {
      yield put(actions.getListError(err));
    }
  }

  function* cancle(action) {
    const { payload } = action;
    try {
      const data = yield call(cancleTradeService, payload);
      yield put(actions.cancleTradeDone(data));
    } catch (err) {
      yield put(actions.cancleTradeError(err));
    }
  }

  function* statistics(action) {
    const { payload } = action;
    try {
      const data = yield call(getStatistics, payload);
      yield put(actions.getStatisticsDone(data));
    } catch (err) {
      yield put(actions.getStatisticsError(err));
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_LIST, getData);
    yield takeEvery(CONSTANTS.GET_STATISTICS, statistics);
    yield takeEvery(CONSTANTS.UPDATE_FILTER, getData);
    yield takeEvery(CONSTANTS.CANCLE_TRADE, cancle);
  };
};
