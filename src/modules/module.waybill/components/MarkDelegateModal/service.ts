/**
 * Auth Service
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../../../CONFIG';

// API list
const API_MARK_DELEGATE = '/ehuodiGateway/utmsTrade/utmsTrade/markDelegate';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_MARK_DELEGATE)
    .reply(() =>
      import('./mock/delegate').then(exports => [200, exports.default]),
    );
}

/**
 * 保存
 */
export const getMarkDelegate = data => {
  return request(
    {
      method: 'post',
      url: API_MARK_DELEGATE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
    },
  ).then(res => res);
};
