/**
 * DateTimePicker Component
 * @author yan
 * @date 2018/9/12 下午2:41:24
 */
import { Form, Input, message, Modal, Select } from 'antd';
// const { TextArea } = Input;
import TextArea from 'components/TextArea';
import * as React from 'react';
import { checkNumberForm } from 'utils/checkString';
import { getMarkDelegate } from './service';

const FormItem = Form.Item;
const Option = Select.Option;

interface IProps {
  visible: boolean;
  onCancel: () => void;
  onOk: (e: any) => void;
  form: any;
  data: object;
}

interface IState {
  visible: boolean;
  confirmLoading: boolean;
  data?: object;
}

class MarkDelegate extends React.PureComponent<IProps> {
  state: IState;
  selectedAddress: object;
  constructor(props) {
    super(props);
    this.state = {
      confirmLoading: false,
      visible: props.visible,
    };
  }
  handleSelectChange = value => {
    console.log(value);
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
    this.props.onCancel();
  };
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.save(values);
      }
    });
  };
  handleClose = () => {
    this.props.form.resetFields();
  };
  public render() {
    // const props = { ...this.props };
    // const state = this.state;
    // props.visible = state.visible;
    // props.onCancel = this.hideModal;
    // props.onOk = this.handleSubmit;

    const formItemLayout = {
      labelCol: {
        span: 5,
      },
      wrapperCol: { span: 16 },
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal
        title="标记代收"
        centered={true}
        maskClosable={false}
        confirmLoading={this.state.confirmLoading}
        visible={this.state.visible}
        onCancel={this.handleCancel}
        onOk={this.handleOk}
        afterClose={this.handleClose}
      >
        <div>
          <FormItem {...formItemLayout} label="回款金额">
            {getFieldDecorator('amount', {
              rules: [
                {
                  required: true,
                  message: '请填写回款金额',
                },
                {
                  validator: checkNumberForm('请输入0-100000，支持两位小数', {
                    max: 100000,
                    min: 0,
                    decimal: 2,
                  }),
                },
              ],
            })(<Input placeholder={'请输入回款金额'} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="备注">
            {getFieldDecorator('remark', {
              rules: [{ max: 100, message: '字数限制100' }],
            })(
              <TextArea
                maxLength={100}
                placeholder="请输入备注"
                autosize={{ minRows: 7, maxRows: 10 }}
              />,
            )}
          </FormItem>
        </div>
      </Modal>
    );
  }

  public componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.visible,
    });
  }
  private save = async values => {
    this.setState({
      confirmLoading: true,
    });
    const options = { ...values };
    const params = {
      tradeNumber: this.props.data.tradeNumber,
      ...options,
    };

    try {
      const data = await getMarkDelegate(params);
      if (data.result === 'success') {
        message.success('标记代收成功');
        if (this.props.onOk) {
          this.props.onOk({ ...this.props.data, ...options });
        }
      } else {
        message.error(data.result.msg || '标记代收失败');
      }
    } catch (err) {
      message.error(err.msg || '标记代收失败');
    }

    this.setState({
      confirmLoading: false,
    });
  };
}
const MarkDelegateModal = Form.create()(MarkDelegate);
export default MarkDelegateModal;
