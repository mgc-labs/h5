/**
 * DateTimePicker Component
 * @author yan
 * @date 2018/9/12 下午2:41:24
 */
import { Form, Input, message, Modal, Select } from 'antd';
import TextArea from 'components/TextArea';
import * as React from 'react';
import { getMarkReceipt } from './service';

const FormItem = Form.Item;
const Option = Select.Option;

interface IProps {
  visible: boolean;
  onCancel: () => void;
  onOk: (e: any) => void;
  form: any;
  data: object;
}

interface IState {
  visible: boolean;
  confirmLoading: boolean;
  data?: object;
}

class MarkReceipt extends React.PureComponent<IProps> {
  state: IState;
  selectedAddress: object;
  constructor(props) {
    super(props);
    this.state = {
      confirmLoading: false,
      visible: props.visible,
    };
  }
  handleSelectChange = value => {
    console.log(value);
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
    this.props.onCancel();
  };
  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.save(values);
      }
    });
  };
  handleClose = () => {
    this.props.form.resetFields();
  };
  public render() {
    const formItemLayout = {
      labelCol: {
        span: 5,
      },
      wrapperCol: { span: 16 },
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal
        title="标记回单"
        centered={true}
        maskClosable={false}
        confirmLoading={this.state.confirmLoading}
        visible={this.state.visible}
        onCancel={this.handleCancel}
        onOk={this.handleOk}
        afterClose={this.handleClose}
      >
        <div>
          <FormItem {...formItemLayout} label="回单状态">
            {getFieldDecorator('status', {
              rules: [
                {
                  required: true,
                  message: '请选择回单状态',
                },
              ],
            })(
              <Select
                placeholder="请选择回单状态"
                onChange={this.handleSelectChange}
                style={{ width: '100%' }}
              >
                <Option value="2">部分交回</Option>
                <Option value="3">全部交回</Option>
                <Option value="4">回单丢失</Option>
              </Select>,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="备注">
            {getFieldDecorator('remark', {
              rules: [{ max: 100, message: '字数限制100' }],
            })(
              <TextArea
                maxLength={100}
                placeholder="请输入备注"
                autosize={{ minRows: 7, maxRows: 10 }}
              />,
            )}
          </FormItem>
        </div>
      </Modal>
    );
  }

  public componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.visible,
    });
  }
  private save = async values => {
    this.setState({
      confirmLoading: true,
    });
    const options = { ...values };
    const params = {
      tradeNumber: this.props.data.tradeNumber,
      ...options,
    };
    try {
      const data = await getMarkReceipt(params);

      if (data.result === 'success') {
        message.success('标记回单成功');
        if (this.props.onOk) {
          this.props.onOk({ ...this.props.data, ...options });
        }
      } else {
        message.error(data.result.msg || '标记回单失败');
      }
    } catch (err) {
      message.error(err.msg || '标记回单失败');
    }

    this.setState({
      confirmLoading: false,
    });
  };
}
const MarkReceiptModal = Form.create()(MarkReceipt);
export default MarkReceiptModal;
