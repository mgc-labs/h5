/**
 * Auth Service
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../../../CONFIG';

// API list
const API_MARK_RECEIPT = '/ehuodiGateway/utmsTrade/utmsTrade/markReceipt';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_MARK_RECEIPT)
    .reply(() =>
      import('./mock/receipt').then(exports => [200, exports.default]),
    );
}

/**
 * 保存
 */
export const getMarkReceipt = data => {
  return request(
    {
      method: 'post',
      url: API_MARK_RECEIPT,
      data: qs.stringify(data),
    },
    {
      useMock: false,
    },
  ).then(res => res);
};
