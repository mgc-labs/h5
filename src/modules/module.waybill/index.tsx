/**
 * Waybill Component
 * @author yanw
 * @date 2018-9-14 16:12:09
 */
import Button from 'antd/es/button';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import message from 'antd/es/message';
import Popconfirm from 'antd/es/popconfirm';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import Table from 'components/MyTable';
import { OptionButtons } from 'components/OptionButtons';
import { List, Map } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import GLOBAL from 'utils/GLOBAL';
import MarkDelegateModal from './components/MarkDelegateModal';
import MarkReceiptModal from './components/MarkReceiptModal';

const { RangePicker } = DatePicker;
const Option = Select.Option;

import { cancleTradeService } from './service';

import {
  makeSelectData,
  makeSelectDataCount,
  makeSelectError,
  makeSelectLoading,
  makeSelectStatistics,
} from './selectors';

import styles from './index.module.less';

const FormItem = Form.Item;
const gutter = 16;
const colProps = {
  sm: 24,
  xs: 24,
  md: 12,
  lg: 12,
  xl: 6,
};

export interface IWaybillProps {
  children: React.ReactChildren;
  dataCount: number;
  // status: number; // 回单状态
  // remark: string; // 回单备注
  // amount: string; // 代收金额
  // amountRemark: string; // 代收备注
  form: WrappedFormUtils;
  list: List<Map<string, any>>;
  loading: boolean;
  statistics: any;
  getStatistics: () => any;
  cancleTrade: (params?: object) => any;
  // markModalVisible: boolean;
  // currentItem?: object;
  markReceipt: (params?: object) => any;
  markDelegate: (params?: object) => any;
  doSearch: (options?: object) => any;
  updateFilter: (data?: object) => any;
}

interface IState {
  receiptModalvisible: boolean;
  fieldsValue: object;
  currentItem: object;
  remark: string;
  record: object;
  delegeteModalvisible: boolean;
  current: number;
  skipCount: number;
  pageSize: number;
}
class Waybill extends React.PureComponent<IWaybillProps> {
  state: IState;
  constructor(props) {
    super(props);
    this.state = {
      fieldsValue: {},
      current: 1,
      skipCount: 0,
      pageSize: 15,
      remark: '', // 回单备注
      record: {}, // 操作行
      currentItem: {},
      // popVisible: false, // 标记弹窗
      receiptModalvisible: false, // 标记回单弹窗
      delegeteModalvisible: false, // 标记代收弹窗
    };
  }
  componentDidMount() {
    this.props.doSearch({
      skipCount: this.state.skipCount,
      pageSize: this.state.pageSize,
    });
    this.props.getStatistics();
  }

  getSubmitValue = fieldsValue => {
    const {
      creatdate,
      usecardate,
      tradeNumberKey,
      driverName,
      carPlateNumber,
      status,
    } = fieldsValue;

    const values = {
      createDateStart:
        creatdate && creatdate.length
          ? creatdate[0].format('YYYY-MM-DD') + ' 00:00:00'
          : '',
      createDateEnd:
        creatdate && creatdate.length
          ? creatdate[1].format('YYYY-MM-DD') + ' 23:59:59'
          : '',
      useCarDateStart:
        usecardate && usecardate.length
          ? usecardate[0].format('YYYY-MM-DD') + ' 00:00:00'
          : '',
      useCarDateEnd:
        usecardate && usecardate.length
          ? usecardate[1].format('YYYY-MM-DD') + ' 23:59:59'
          : '',
      tradeNumberKey,
      driverName,
      carPlateNumber,
      status,
    };
    return values;
  };
  public handleSubmit = e => {
    if (e) {
      e.preventDefault();
    }
    this.setState(
      {
        current: 1,
        skipCount: 0,
      },
      () => {
        this.props.form.validateFields((err, value) => {
          const values = this.getSubmitValue(value);
          this.setState(
            {
              fieldsValue: values,
            },
            () => {
              this.props.doSearch(
                Object.assign(
                  {
                    current: 1,
                    skipCount: 0,
                    pageSize: this.state.pageSize,
                  },
                  values,
                ),
              );
            },
          );
        });
      },
    );
  };
  public handleReset = () => {
    this.props.form.resetFields();
    // this.handleSubmit();
    this.setState(
      {
        skipCount: 0,
        current: 1,
        fieldsValue: {},
      },
      () => {
        this.props.doSearch({
          skipCount: this.state.skipCount,
          pageSize: this.state.pageSize,
          current: this.state.current,
        });
      },
    );
  };
  /**
   * 标记回单弹窗
   */
  handleCancelReceipt = () => {
    this.setState({
      receiptModalvisible: false,
    });
  };
  handleReceiptSuccess = () => {
    this.handleCancelReceipt();
    this.handleSubmit();
  };
  showModalReceipt = record => {
    this.setState({
      receiptModalvisible: true,
      currentItem: record,
      // popVisible: false,
    });
  };
  handleVisibleChange = visible => {
    // this.setState({
    //   popVisible: visible,
    // });
  };
  /**
   * 标记代收
   */
  showModalDelegate = record => {
    this.setState({
      delegeteModalvisible: true,
      currentItem: record,
      // popVisible: false,
    });
  };
  handleCancelDelegate = () => {
    this.setState({
      delegeteModalvisible: false,
    });
  };
  handleDelegateSuccess = () => {
    this.handleCancelDelegate();
    this.handleSubmit();
  };
  public renderActions = (text, record) => {
    return (
      <div className={styles.WaybillList__actions}>
        <OptionButtons>
          {record.statusTXT === '待签到' ||
          record.statusTXT === '待装货' ||
          record.statusTXT === '待卸货' ||
          record.statusTXT === '已完成' ||
          (record.statusTXT === '已取消' &&
            record.driverMobilenumber !== null &&
            record.driverMobilenumber !== '') ? (
            <span>
              <Link to={`/waybill/map?tradeNumber=${record.tradeNumber}`}>
                查看轨迹
              </Link>
            </span>
          ) : null}

          {/* <Popover
            placement="bottom"
            content={content}
            title=""
            trigger="click"
            // visible={this.state.popVisible}
            // onVisibleChange={this.handleVisibleChange}
          >
            <a href="javascript;">更多</a>
          </Popover> */}
          {(record.status === 5 || record.status === 6) &&
          record.receiptStatus === 1 ? (
            <span onClick={e => this.showModalReceipt(record)}>
              <a>标记回单</a>
            </span>
          ) : null}
          {(record.status === 5 || record.status === 6) &&
          record.delegateStatus !== 0 &&
          record.markDelegateStatus === 0 ? (
            <span onClick={e => this.showModalDelegate(record)}>
              <a>标记代收</a>
            </span>
          ) : null}
          {record.statusTXT === '待调度' ||
          record.statusTXT === '待接单' ||
          record.statusTXT === '待签到' ||
          record.statusTXT === '待装货' ||
          record.statusTXT === '待卸货' ||
          record.statusTXT === '待改派' ? (
            <Popconfirm
              title="确认取消"
              onConfirm={() => this.confirmRecord(record)}
            >
              <span>
                <a>取消</a>
              </span>
            </Popconfirm>
          ) : null}
        </OptionButtons>
      </div>
    );
  };
  public renderFilterForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form
        className={styles.WaybillList__Form}
        layout="inline"
        onSubmit={this.handleSubmit}
      >
        <Row type="flex" gutter={gutter}>
          <Col {...colProps}>
            <FormItem label="发布时间">
              {getFieldDecorator('creatdate')(
                <RangePicker format="YYYY-MM-DD" style={{ width: '100%' }} />,
              )}
            </FormItem>
          </Col>
          <Col {...colProps}>
            <FormItem label="用车时间">
              {getFieldDecorator('usecardate')(
                <RangePicker format="YYYY-MM-DD" style={{ width: '100%' }} />,
              )}
            </FormItem>
          </Col>
          <Col {...colProps}>
            <FormItem label="运单号">
              {getFieldDecorator('tradeNumberKey')(<Input />)}
            </FormItem>
          </Col>
        </Row>
        <Row type="flex" gutter={gutter}>
          <Col {...colProps}>
            <FormItem label="司机姓名">
              {getFieldDecorator('driverName')(<Input />)}
            </FormItem>
          </Col>
          <Col {...colProps}>
            <FormItem label="车牌号">
              {getFieldDecorator('carPlateNumber')(<Input />)}
            </FormItem>
          </Col>
          <Col {...colProps}>
            <FormItem label="状态">
              {getFieldDecorator('status', { initialValue: '' })(
                <Select
                  style={{
                    width: '100%',
                  }}
                >
                  <Option value="">全部</Option>
                  <Option value="0">待调度</Option>
                  <Option value="1">待接单</Option>
                  <Option value="2">待签到</Option>
                  <Option value="3">待装货</Option>
                  <Option value="4">待卸货</Option>
                  <Option value="5">已完成</Option>
                  <Option value="6">已取消</Option>
                  <Option value="7">待改派</Option>
                </Select>,
              )}
            </FormItem>
          </Col>
          <Col {...colProps}>
            <FormItem className={styles.WaybillList__FormAction}>
              <Button type="primary" htmlType="submit">
                查 询
              </Button>
              <Button onClick={this.handleReset}>重 置</Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  public renderStatistics() {
    const { statistics } = this.props;
    return (
      <div className={styles.WaybillWrap__Top}>
        <div>
          <h3>总运单</h3>
          <h4>{statistics.total || '- -'}</h4>
        </div>
        <div>
          <h3>待调度</h3>
          <h4>{statistics.pendingScheduling || '- -'}</h4>
        </div>
        <div>
          <h3>待签到</h3>
          <h4>{statistics.waitSign || '- -'}</h4>
        </div>
        <div>
          <h3>待装货</h3>
          <h4>{statistics.waitLoad || '- -'}</h4>
        </div>
        <div>
          <h3>待卸货</h3>
          <h4>{statistics.waitUnload || '- -'}</h4>
        </div>
        <div>
          <h3>已完成</h3>
          <h4>{statistics.finished || '- -'}</h4>
        </div>
        <div>
          <h3>已取消</h3>
          <h4>{statistics.cancel || '- -'}</h4>
        </div>
      </div>
    );
  }
  handleSearch = pageData => {
    const { doSearch } = this.props;
    // const values = this.props.form.getFieldsValue();
    this.setState(
      {
        pageSize: pageData.pageSize,
        skipCount:
          pageData.current === 0
            ? 0
            : pageData.pageSize * (pageData.current - 1),
        current: pageData.current,
      },
      () => {
        const postData = Object.assign(this.state.fieldsValue, {
          pageSize: this.state.pageSize,
          skipCount: this.state.skipCount,
          current: this.state.current,
        });
        doSearch(postData);
      },
    );
  };
  public renderList() {
    const { dataCount, doSearch, list, loading } = this.props;
    const tableProps = {
      columns: [
        {
          title: '运单号',
          dataIndex: 'tradeNumber',
          fixed: 'left',
          width: 240,
          render: text => (
            <Link to={`/waybill/detail?tradeNumber=${text}`}>{text}</Link>
          ),
        },
        {
          title: '发布时间',
          width: 250,
          dataIndex: 'createDate',
        },
        {
          title: '用车时间',
          width: 250,
          dataIndex: 'useCarDate',
        },
        {
          title: '货物名称',
          width: 150,
          dataIndex: 'goodsName',
        },
        {
          title: '重量（千克）',
          width: 150,
          dataIndex: 'goodsWeight',
        },
        {
          title: '体积（方）',
          width: 150,
          dataIndex: 'goodsVolume',
        },
        {
          title: '件数（件）',
          width: 150,
          dataIndex: 'goodsNumber',
        },
        {
          title: '司机姓名',
          width: 150,
          dataIndex: 'driverName',
        },
        {
          title: '车牌',
          width: 150,
          dataIndex: 'driverCarplatenumber',
        },
        {
          title: '手机号',
          width: 250,
          dataIndex: 'driverMobilenumberTXT',
        },
        {
          title: '运单状态',
          width: 150,
          dataIndex: 'statusTXT',
        },
        {
          title: '代收',
          width: 150,
          dataIndex: 'delegateamount',
        },
        {
          title: '回单状态',
          width: 150,
          dataIndex: 'receiptStatusTXT',
        },
        {
          title: '操作',
          key: 'action',
          fixed: 'right',
          width: 160,
          render: this.renderActions,
        },
      ],
      dataSource: list
        .map((d, i) => {
          let statusTXT;
          let receiptStatusTXT;
          if (d.get('status') === 0) {
            statusTXT = '待调度';
          } else if (d.get('status') === 1) {
            statusTXT = '待接单';
          } else if (d.get('status') === 2) {
            statusTXT = '待签到';
          } else if (d.get('status') === 3) {
            statusTXT = '待装货';
          } else if (d.get('status') === 4) {
            statusTXT = '待卸货';
          } else if (d.get('status') === 5) {
            statusTXT = '已完成';
          } else if (d.get('status') === 6) {
            statusTXT = '已取消';
          } else if (d.get('status') === 7) {
            statusTXT = '待改派';
          }

          if (d.get('receiptStatus') === 0) {
            receiptStatusTXT = '无回单';
          } else if (d.get('receiptStatus') === 1) {
            receiptStatusTXT = '待交回';
          } else if (d.get('receiptStatus') === 2) {
            receiptStatusTXT = '部分交回';
          } else if (d.get('receiptStatus') === 3) {
            receiptStatusTXT = '全部交回';
          } else if (d.get('receiptStatus') === 4) {
            receiptStatusTXT = '回单丢失';
          } else {
            receiptStatusTXT = GLOBAL.emptyRecord;
          }
          return {
            key: i,
            tradeNumber: d.get('tradeNumber'),
            createDate: d.get('createDate'),
            useCarDate: d.get('useCarDate'),
            goodsName: d.get('goodsName'),
            goodsWeight: d.get('goodsWeight'),
            goodsVolume: d.get('goodsVolume'),
            goodsNumber: d.get('goodsNumber'),
            driverName: d.get('driverName') || GLOBAL.emptyRecord,
            driverCarplatenumber:
              d.get('driverCarplatenumber') || GLOBAL.emptyRecord,
            driverMobilenumber: d.get('driverMobilenumber'),
            driverMobilenumberTXT:
              d.get('driverMobilenumber') || GLOBAL.emptyRecord,
            status: d.get('status'),
            statusTXT,
            delegateamount:
              (d.get('actualDelegateAmount') || GLOBAL.emptyRecord) +
              `/` +
              (d.get('preDelegateAmount') || GLOBAL.emptyRecord),
            receiptStatus:
              d.get('receiptStatus') !== null ? d.get('receiptStatus') : '',
            receiptStatusTXT,
            delegateStatus:
              d.get('delegateStatus') !== null ? d.get('delegateStatus') : '',
            markDelegateStatus:
              d.get('markDelegateStatus') !== null
                ? d.get('markDelegateStatus')
                : '',
          };
        })
        .toArray(),
      loading,
      pagination: {
        current: this.state.current,
        total: dataCount,
        pageSize: this.state.pageSize,
        onChange: (pageData, index) => {
          this.handleSearch(pageData);
        },
        onShowSizeChange: pageData => {
          this.handleSearch(pageData);
        },
      },
      scroll: { x: 2660 },
    };
    return <Table {...tableProps} />;
  }
  public render() {
    const {
      receiptModalvisible,
      currentItem,
      delegeteModalvisible,
    } = this.state;

    return (
      <div className={styles.WaybillWrap}>
        {this.renderStatistics()}
        <div className={styles.WaybillList}>
          {this.renderFilterForm()}
          {this.renderList()}
        </div>
        <MarkReceiptModal
          visible={receiptModalvisible}
          onCancel={this.handleCancelReceipt}
          onOk={this.handleReceiptSuccess}
          data={currentItem}
        />
        <MarkDelegateModal
          visible={delegeteModalvisible}
          onCancel={this.handleCancelDelegate}
          onOk={this.handleDelegateSuccess}
          data={currentItem}
        />
      </div>
    );
  }
  /**
   * 取消运单
   */
  private confirmRecord = async record => {
    const params = {
      tradeNumber: record.tradeNumber,
    };
    try {
      const data = await cancleTradeService(params);
      if (data.result === 'success') {
        message.success('取消运单成功');
        this.props.getStatistics();
        this.handleSubmit();
      } else {
        message.error(data.result.msg || '取消运单失败');
      }
    } catch (err) {
      message.error(err.msg || '取消运单失败');
    }
  };
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getStatistics: () => dispatch(actions.getStatistics()),
    cancleTrade: params => dispatch(actions.cancleTrade(params)),
    markReceipt: params => dispatch(actions.markReceipt(params)),
    markDelegate: params => dispatch(actions.markDelegate(params)),
    doSearch: options => dispatch(actions.getList(options)),
    updateFilter: data => dispatch(actions.updateFilter(data)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    statistics: makeSelectStatistics(selectState),
    list: makeSelectData(selectState),
    dataCount: makeSelectDataCount(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(
    withConnect,
    Form.create(),
  )(Waybill);
};
