export default {
  result: 'success',

  count: 1,

  data: {
    TOTAL: 1234, // 总订单数量
    PENDING_SCHEDULING: '213', // 待调度
    WAIT_SIGN: '213', // 待签到
    WAIT_LOAD: '213', // 待装货
    WAIT_UNLOAD: '213', // 待卸货
    FINISHED: '123', // 已完成
    CANCEL: '123', // 已取消
  },

  code: 'EHD123456789123',

  msg: '标记代收成功',
};
