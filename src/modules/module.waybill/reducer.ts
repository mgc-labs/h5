/**
 * waybill Reducers
 * @author yanw
 * @date 2018-9-14 16:12:09
 */

import { fromJS, List, Map } from 'immutable';

// The initial state of the App
const initialState = Map({
  statistics: {},
  data: List(),
  dataCount: 0,
  filter: Map(),
  error: false,
  loading: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_LIST:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_LIST_SUCCESS:
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data))
          .set('dataCount', action.payload.count);
      case CONSANTS.GET_LIST_ERROR:
        return state.set('error', action.error).set('loading', false);
      case CONSANTS.GET_STATISTICS:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_STATISTICS_SUCCESS:
        return state
          .set('loading', false)
          .set('statistics', action.payload.data);
      case CONSANTS.GET_STATISTICS_ERROR:
        return state.set('error', action.error).set('loading', false);
      case CONSANTS.CANCLE_TRADE:
        return state.set('error', false).set('loading', true);
      case CONSANTS.CANCLE_TRADE_SUCCESS:
        return state.set('loading', false);
      case CONSANTS.CANCLE_TRADE_ERROR:
        return state.set('error', action.error).set('loading', false);
      default:
        return state;
    }
  };
};
