/**
 * waybill.list Service
 * @author yanw
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const PATHLIST_RESOURCE = '/ehuodiGateway/utmsTrade/utmsTrade/selectTradeList';
const CANCLETRADE_RESOURCE = '/ehuodiGateway/utmsTrade/utmsTrade/cancelTrade';
const STATISTICS_RESOURCE =
  '/ehuodiGateway/utmsTrade/utmsTrade/tradeStatistics';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', PATHLIST_RESOURCE)
    .reply(() => import('./mock/list').then(exports => [200, exports.default]));

  mocker
    .on('post', CANCLETRADE_RESOURCE)
    .reply(() =>
      import('./mock/cancle').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', STATISTICS_RESOURCE)
    .reply(() =>
      import('./mock/statistics').then(exports => [200, exports.default]),
    );
}

/**
 * 获取资源信息
 */
export const getStatistics = () => {
  return request(
    {
      method: 'post',
      url: STATISTICS_RESOURCE,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

export const getResources = data => {
  return request(
    {
      method: 'post',
      url: PATHLIST_RESOURCE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
};

export const cancleTradeService = data => {
  return request(
    {
      method: 'post',
      url: CANCLETRADE_RESOURCE,
      data: qs.stringify(data),
    },
    {
      useMock: false,
    },
  );
};
