/**
 * waybill Actions
 * @author yanw
 * @date 2018-9-14 16:12:09
 */
import { AnyAction } from 'redux';

export default CONSANTS => ({
  getStatistics: (): AnyAction => ({
    type: CONSANTS.GET_STATISTICS,
  }),

  getStatisticsDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_STATISTICS_SUCCESS,
  }),

  getStatisticsError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSANTS.GET_STATISTICS_ERROR,
  }),

  getList: (options): AnyAction => ({
    payload: options,
    type: CONSANTS.GET_LIST,
  }),

  getListDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_LIST_SUCCESS,
  }),

  getListError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSANTS.GET_LIST_ERROR,
  }),

  cancleTrade: (params): AnyAction => ({
    payload: params,
    type: CONSANTS.CANCLE_TRADE,
  }),

  cancleTradeDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.CANCLE_TRADE_SUCCESS,
  }),

  cancleTradeError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSANTS.CANCLE_TRADE_ERROR,
  }),

  updateFilter: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.UPDATE_FILTER,
  }),
});
