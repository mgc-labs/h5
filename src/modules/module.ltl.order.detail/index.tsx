/**
 * LtlOrderDetail Component
 * @author djd
 * @date 2018/9/14 下午1:25:27
 */
import msngr from 'msngr';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import Button from 'antd/es/button';
import Col from 'antd/es/col';
import Row from 'antd/es/row';
import {
  FormDetailCard,
  FormDetailField,
  formLayoutCols2,
  formLayoutCols3,
  gutter,
} from 'components/FormDetailCard';

import GLOBAL from 'utils/GLOBAL';

import styles from './index.module.less';
import {
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

export interface ILtlOrderDetailProps {
  children: React.ReactChildren;
  data?: object;
  routeId?: string;
  searchParams?: {
    id: number | string;
  };
  getAsyncData?: (param?: object) => any;
}
const DEFAULT_NULL = GLOBAL.emptyRecord;
class LtlOrderDetail extends React.PureComponent<ILtlOrderDetailProps> {
  constructor(props: ILtlOrderDetailProps, context: any) {
    super(props, context);
    this.state = {
      customerName: '',
      thirdSystemId: '',
      contact: '',
      requireDate: '',
      phone: '',
      address: '',
      remark: '',
      goodsName: '',
      goodsWeight: '',
      goodsVolume: '',
      goodsNumber: '',
      isReceipt: true,
      delegateAmount: '',
    };
  }
  public componentDidMount() {
    const { getAsyncData, searchParams, routeId } = this.props;
    getAsyncData({ utmsPartOrderId: searchParams.id });
    msngr(routeId, 'refresh').on(this.updateHandler);
  }
  public componentWillUnmount() {
    msngr(this.props.routeId, 'refresh').drop(this.updateHandler);
  }
  public updateHandler = () => {
    const { searchParams, getAsyncData } = this.props;
    getAsyncData({ utmsPartOrderId: searchParams.id });
  };
  public render() {
    const {
      customerName,
      thirdSystemId,
      contact,
      requireDate,
      phone,
      address,
      remark,
      goodsName,
      goodsWeight,
      goodsVolume,
      goodsNumber,
      isReceipt,
      delegateAmount,
      orderNumber,
      tradeNumber,
      driverName,
      driverPhone,
      status,
      tradeStatus,
    } = this.props.data;

    return (
      <div className={styles.LtlOrderDetail}>
        <FormDetailCard title="运输信息">
          <Row gutter={gutter}>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="订单号"
                value={orderNumber || DEFAULT_NULL}
                style={{ marginTop: 0 }}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="状态"
                value={status || DEFAULT_NULL}
                style={{ marginTop: 0 }}
              />
            </Col>
          </Row>
          <Row gutter={gutter}>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="运单号"
                value={tradeNumber || DEFAULT_NULL}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="状态"
                value={tradeStatus || DEFAULT_NULL}
              />
            </Col>
          </Row>
          <Row gutter={gutter}>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="司机姓名"
                value={driverName || DEFAULT_NULL}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="司机电话"
                value={driverPhone || DEFAULT_NULL}
              />
            </Col>
          </Row>
        </FormDetailCard>
        <FormDetailCard title="基本信息">
          <Row gutter={gutter}>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="客户"
                value={customerName || DEFAULT_NULL}
                style={{ marginTop: 0 }}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="第三方订单号"
                value={thirdSystemId || DEFAULT_NULL}
                style={{ marginTop: 0 }}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="要求送达时间"
                value={requireDate || DEFAULT_NULL}
                style={{ marginTop: 0 }}
              />
            </Col>
          </Row>

          <Row gutter={gutter}>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="收货人姓名"
                value={contact || DEFAULT_NULL}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField label="联系电话" value={phone || DEFAULT_NULL} />
            </Col>
          </Row>
          <Row gutter={gutter}>
            <Col {...formLayoutCols2}>
              <FormDetailField
                label="收货地址"
                value={address || DEFAULT_NULL}
              />
            </Col>
          </Row>

          <Row gutter={gutter}>
            <Col {...formLayoutCols2}>
              <FormDetailField label="备注" value={remark || DEFAULT_NULL} />
            </Col>
            <Col span={8} />
          </Row>
        </FormDetailCard>

        <FormDetailCard title="货物信息">
          <Row gutter={gutter}>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="货物名称"
                value={goodsName || DEFAULT_NULL}
                style={{ marginTop: 0 }}
              />
            </Col>
          </Row>

          <Row gutter={gutter}>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="重量（千克）"
                value={goodsWeight || DEFAULT_NULL}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="体积（方）"
                value={goodsVolume || DEFAULT_NULL}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="件数（件）"
                value={goodsNumber || DEFAULT_NULL}
              />
            </Col>
          </Row>
        </FormDetailCard>

        <FormDetailCard title="增值服务">
          <Row gutter={gutter}>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="回单"
                value={isReceipt || DEFAULT_NULL}
                style={{ marginTop: 0 }}
              />
            </Col>
            <Col {...formLayoutCols3}>
              <FormDetailField
                label="代收（元）"
                value={delegateAmount || DEFAULT_NULL}
                style={{ marginTop: 0 }}
              />
            </Col>
          </Row>
        </FormDetailCard>

        {/* <PageBottom
          buttons={[
            <Button key={1}>取消</Button>,
            <Button key={2} type="primary" htmlType="submit">
              确认开单
            </Button>,
          ]}
        /> */}
      </div>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getAsyncData: param => dispatch(actions.getAsyncData(param)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    data: makeSelectData(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(LtlOrderDetail);
};
