/**
 * LtlOrderDetail Saga
 * @author djd
 * @date 2018/9/14 下午1:25:27
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import request, { mocker } from 'utils/request';
import { getOrderDetail } from './service';
export default (CONSTANTS, actions) => {
  function* getData(action) {
    try {
      const data = yield call(getOrderDetail, action.payload);
      yield put(actions.getAsyncDataDone(data));
    } catch (err) {
      yield put(actions.getAsyncDataError(err));
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_ASYNC_DATA, getData);
  };
};
