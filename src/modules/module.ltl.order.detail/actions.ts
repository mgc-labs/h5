/**
 * LtlOrderDetail Actions
 * @author djd
 * @date 2018/9/14 下午1:25:27
 */
import { AnyAction } from 'redux';
import {
  GET_ASYNC_DATA,
  GET_ASYNC_DATA_ERROR,
  GET_ASYNC_DATA_SUCCESS,
} from './constants';

export default CONSANTS => ({
  getAsyncData: (param): AnyAction => ({
    payload: param,
    type: CONSANTS.GET_ASYNC_DATA,
  }),

  getAsyncDataDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_ASYNC_DATA_SUCCESS,
  }),

  getAsyncDataError: (error): AnyAction => ({
    error,
    type: CONSANTS.GET_ASYNC_DATA_ERROR,
  }),
});
