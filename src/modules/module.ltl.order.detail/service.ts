import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_ORDER_DETAIL =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/partOrderDetail';
if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_ORDER_DETAIL)
    .reply(() => import('./mock/mock').then(exports => [200, exports.default]));
}
/**
 * 查询订单详情
 */
export const getOrderDetail = params => {
  return request(
    {
      method: 'post',
      url: API_ORDER_DETAIL,
      data: qs.stringify(params),
    },
    {
      useMock: USE_MOCK,
    },
  );
};
