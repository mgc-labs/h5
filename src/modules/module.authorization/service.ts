/**
 * Auth Service
 * @author ryan bian
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';
// const USE_MOCK = true;

// API list
const API_USERINFO = '/ehuodiGateway/utmsCore/utmsUserscs/selectUtmsUsers';
const API_RESOURCE = `/ehuodiGateway/utmsCore/utmsResourcecs/selectUtmsResourceList${
  USE_MOCK ? '?mock' : ''
}`;
const API_ACCOUNT_INFO =
  '/ehuodiGateway/utmsCore/utmsClientcs/selectUtmsClient';

if (USE_MOCK) {
  mocker
    .on('post', API_USERINFO)
    .reply(() =>
      import('./mock/userInfo').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_RESOURCE)
    .reply(() =>
      import('./mock/resource').then(exports => [200, exports.default]),
    );
}

/**
 * 获取用户信息
 */
export const getUserInfo = userName =>
  request(
    {
      method: 'post',
      url: API_USERINFO,
      data: {
        userName,
      },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getResources = data =>
  request(
    {
      method: 'post',
      url: API_RESOURCE,
      data: qs.stringify(data),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

/**
 * 获取资源信息
 * 其实就是拿菜单树
 */
export const getAccountInfo = () =>
  request(
    {
      method: 'post',
      url: API_ACCOUNT_INFO,
      data: qs.stringify({}),
    },
    {
      // useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  ).then(res => res.data);
