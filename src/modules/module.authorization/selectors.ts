/**
 * The app state selectors
 */
import { List, Seq } from 'immutable';
import { createSelector } from 'reselect';

const selectState = state => state.get('authorization');

export const makeSelectUserData = () =>
  createSelector(selectState, state => state.get('userData'));

export const makeSelectAccountInfo = () =>
  createSelector(selectState, state => state.get('accountInfo'));

export const makeSelectError = () =>
  createSelector(selectState, state => state.get('error'));

export const makeSelectLoading = () =>
  createSelector(selectState, state => state.get('loading'));

export const makeSelectResourceStatus = () =>
  createSelector(selectState, state => state.get('resouceLoaded'));

export const makeSelectResourceMap = () =>
  createSelector(selectState, state => state.get('resourceByKey', Seq.Keyed()));
