/**
 * Passport Actions
 * @author ryan bian
 */
import { isError } from 'lodash';
import { Action, BaseAction } from 'redux-actions';
import { AuthActionTypes } from './constants';

export const getUserData = (): BaseAction => ({
  type: AuthActionTypes.GET_USERINFO,
});

export const getUserDataDone = (userData): Action<any> => ({
  payload: userData,
  type: AuthActionTypes.GET_USERINFO_SUCCESS,
});

export const getUserDataError = (error): Action<void> => ({
  error,
  type: AuthActionTypes.GET_USERINFO_ERROR,
});

export const getResource = (): BaseAction => ({
  type: AuthActionTypes.GET_RESOURCE,
});
export const getResourceDone = (resources): Action<any> => ({
  type: AuthActionTypes.GET_RESOURCE_SUCCESS,
  payload: resources,
});
export const getResourceError = (error): Action<void> => ({
  error,
  type: AuthActionTypes.GET_RESOURCE_ERROR,
});

export const setAccountInfo = (data): Action<any> => ({
  type: AuthActionTypes.SET_ACCOUNT_INFO,
  error: isError(data),
  payload: data,
});
