/**
 * Passport Saga
 * @author ryan bian
 */
// import { delay } from 'redux-saga';
import { replace } from 'connected-react-router';
import { Map } from 'immutable';
import Cookies from 'js-cookie';
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

import {
  getResourceDone,
  getResourceError,
  getUserDataDone,
  getUserDataError,
  setAccountInfo,
} from './actions';
import { AuthActionTypes } from './constants';
import { getAccountInfo, getResources, getUserInfo } from './service';

import routerConfig from '../../routerConfig';

function* fetchUser() {
  const userName = Cookies.get('userName');
  try {
    const userinfo = yield call(getUserInfo, userName);
    yield put(getUserDataDone(userinfo));
    yield call(fetchAccountInfo);
  } catch (err) {
    yield put(getUserDataError(err));
    yield put(replace('/login'));
  }
}

function* fetchAccountInfo() {
  try {
    const accountInfo = yield call(getAccountInfo);
    yield put(setAccountInfo(accountInfo));
  } catch (err) {
    yield put(setAccountInfo(new Error(err.msg)));
  }
}

function* fetchResources() {
  try {
    const userData = yield select((state: Map<string, any>) =>
      state.getIn(['authorization', 'userData']),
    );
    const accountInfo = yield select((state: Map<string, any>) =>
      state.getIn(['authorization', 'accountInfo']),
    );
    const isAdmin = parseInt(userData.get('isAdmin'), 10) === 1;
    const utmsResourceIds = userData.get('utmsResourceIds');
    // 如果是 管理员账号 直接获取所有菜单
    const resourceParams = {
      versionsCode: accountInfo.get('versionsCode'),
    };
    if (!isAdmin) {
      // 不是管理员，需要判断 utmsResourceIds 是不是为空
      if (utmsResourceIds) {
        Object.assign(resourceParams, {
          utmsResourceIds,
        });
      } else {
        // 如果为空，说明当前用户没有任何权限，不需要去获取菜单列表了
        yield put(getResourceDone([]));
        return;
      }
    }
    const resources = yield call(getResources, resourceParams);
    resources.forEach(item => {
      let showInMenu;
      if (routerConfig.has(item.resourceUrl)) {
        const routeConfig = routerConfig.get(item.resourceUrl);
        if (!routeConfig.excludeFromMenu) {
          // 按钮权限需要排除掉
          showInMenu = item.resourceType === 0;
        } else {
          showInMenu = false;
        }
      } else {
        showInMenu = item.resourceType === 0;
      }
      Object.assign(item, {
        showInMenu,
      });
    });
    // yield delay(1000000);
    yield put(getResourceDone(resources));
  } catch (err) {
    yield put(getResourceError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* rootSaga() {
  yield takeLatest(AuthActionTypes.GET_USERINFO, fetchUser);
  yield takeEvery(AuthActionTypes.SET_ACCOUNT_INFO, fetchResources);
}
