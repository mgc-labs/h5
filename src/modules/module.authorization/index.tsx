/**
 * 权限控制
 * @author ryan bian
 */
import * as H from 'history';
import { List, Map, Seq } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import LoadingIndicator from 'components/LoadingIndicator';
import injectSaga from 'utils/injectSaga';
import { THEME_ENUM } from '../../CONFIG';
import { getUserData } from './actions';
import rootSaga from './saga';
import {
  makeSelectAccountInfo,
  makeSelectError,
  makeSelectLoading,
  makeSelectResourceMap,
  makeSelectResourceStatus,
  makeSelectUserData,
} from './selectors';

import styles from './index.module.less';

interface IAuthorizationProps extends React.Props<Authorization> {
  accountInfo: Map<string, string | number | boolean>;
  location: H.Location;
  userData: Map<string, any>;
  resourceMap: Seq.Keyed<string, List<any>>;
  resouceLoaded: boolean;
  getUserData: () => any;
  children: any;
}

class Authorization extends React.PureComponent<IAuthorizationProps> {
  public componentDidMount() {
    this.props.getUserData();
  }
  public componentWillReceiveProps(nextProps) {
    if (nextProps.accountInfo.equals(this.props.accountInfo)) {
      const style = nextProps.accountInfo.get('style', '3BA1FF') || '3BA1FF';
      const styleKey = style.toLowerCase();
      if (THEME_ENUM.has(styleKey)) {
        const theme = THEME_ENUM.get(styleKey);
        if (theme !== 'default') {
          import(/* webpackChunkName: "theme" */ `../../theme/${theme}/index.ts`);
        }
      }
    }
  }
  public render() {
    const { children, location, resourceMap, resouceLoaded } = this.props;
    return resouceLoaded ? (
      React.cloneElement(children, {
        matchRoute: resourceMap
          .get(location.pathname, List())
          .filter(m => m.get('resourceType') === 0)
          .first(),
      })
    ) : (
      <div className={styles.LoadingWrapper}>
        <LoadingIndicator />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getUserData: () => dispatch(getUserData()),
});

const mapStateToProps = createStructuredSelector({
  accountInfo: makeSelectAccountInfo(),
  error: makeSelectError(),
  loading: makeSelectLoading(),
  userData: makeSelectUserData(),
  resourceMap: makeSelectResourceMap(),
  resouceLoaded: makeSelectResourceStatus(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withSaga = injectSaga({ key: 'authorization', saga: rootSaga });

export default compose(
  withConnect,
  withSaga,
)(Authorization);
