/*
 * Passport Reducer
 * @author ryan bian
 */

import { fromJS, List, Map, Seq } from 'immutable';
import { AnyAction } from 'redux';

import arrayToTree from 'utils/arrayToTree';
import { AuthActionTypes } from './constants';

// The initial state of the App
const initialState = Map({
  error: false,
  loading: false,
  userData: Map(),
  resourceTree: List(),
  resourceList: List(),
  resourceByKey: Seq.Keyed(),
  resourceResponsed: false,
  accountInfo: Map(),
});

const reducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case AuthActionTypes.GET_USERINFO:
      return state.set('error', false).set('loading', true);
    case AuthActionTypes.GET_USERINFO_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('userData', fromJS(action.payload || {}))
        .updateIn(['userData', 'utmsResourceIds'], str => {
          return str
            ? str
                .split(/\],\[|\[|\]/)
                .filter(d => !!d)
                .join(',')
            : '';
        });
    case AuthActionTypes.GET_USERINFO_ERROR:
      return state.set('error', action.error).set('loading', false);
    case AuthActionTypes.GET_RESOURCE_ERROR:
      return state.set('resourceResponsed', true);
    case AuthActionTypes.GET_RESOURCE_SUCCESS:
      const resourceList = action.payload;
      const resourceTree = arrayToTree(resourceList, {
        parentProperty: 'parentResourceId',
        customID: 'utmsResourceId',
      });
      const iResourceList = fromJS(resourceList);
      return state.withMutations(map => {
        map
          .set('resourceTree', fromJS(resourceTree))
          .set('resourceList', iResourceList)
          .set(
            'resourceByKey',
            iResourceList.groupBy(d => d.get('resourceUrl')),
          )
          .set('resouceLoaded', true);
      });
    case AuthActionTypes.SET_ACCOUNT_INFO:
      if (action.error) {
        return state;
      }
      return state.set('accountInfo', Map(action.payload));
    default:
      return state;
  }
};

export default reducer;
