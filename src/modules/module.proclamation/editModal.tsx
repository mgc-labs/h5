/**
 * Proclamation Component
 * @author ggm
 * @date 2018/9/14 下午4:28:11
 */
import Button from 'antd/es/button';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Form, { FormComponentProps } from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';

import TextArea from 'components/TextArea';
import moment from 'moment';
import * as React from 'react';
import { conditionItemLayout } from 'utils/conditionLayout';
import styles from './index.module.less';

const formItemLayout = { ...conditionItemLayout, colon: false };
const FormItem = Form.Item;

export interface IEditModalProps extends FormComponentProps {
  visible?: boolean;
  isInsert?: boolean;
  utmsNoticeId?: string | number;
  title?: string;
  content?: string;
  invalidDate?: string;
  onSave?: (options?: object, isInsert?: boolean, cb?: () => any) => any;
  onCancel?: () => any;
}

class EditModal extends React.PureComponent<IEditModalProps> {
  state = {
    utmsNoticeId: '',
    title: '',
    content: '',
    invalidDate: '',
    isInsert: true,
    loading: false,
  };
  public componentWillReceiveProps(nextProps) {
    const { resetFields, isFieldsTouched } = nextProps.form;
    if (nextProps.visible && !isFieldsTouched()) {
      this.setState({
        utmsNoticeId: nextProps.utmsNoticeId,
        title: nextProps.title,
        content: nextProps.content,
        invalidDate: nextProps.invalidDate,
        isInsert: nextProps.isInsert,
        loading: false,
      });
    } else if (!nextProps.visible) {
      resetFields();
    }
  }
  public render() {
    const { title, content, invalidDate, isInsert, loading } = this.state;
    const { visible } = this.props;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    return (
      <Modal
        title={isInsert ? '新建公告' : '修改公告'}
        visible={visible}
        onOk={this.onSave}
        onCancel={this.onCancel}
        okText="保存"
        cancelText="取消"
        centered={true}
        maskClosable={false}
        okButtonProps={{ loading }}
        bodyStyle={{
          paddingTop: '12px',
          paddingBottom: '16px',
        }}
      >
        <div>
          <Form>
            <Row>
              <FormItem
                {...formItemLayout}
                label="公告标题"
                style={{ paddingBottom: '2px' }}
              >
                {getFieldDecorator('title', {
                  initialValue: title,
                  rules: [
                    {
                      required: true,
                      message: '请填写公告标题',
                    },
                  ],
                })(
                  <Input
                    placeholder="简单描述公告主要内容"
                    maxLength={20}
                    addonAfter={`${getFieldValue('title').length || 0}/20`}
                  />,
                )}
              </FormItem>
            </Row>
            <Row>
              <FormItem
                {...formItemLayout}
                label="公告内容"
                style={{ paddingBottom: '2px' }}
              >
                {getFieldDecorator('content', {
                  initialValue: content,
                  rules: [
                    {
                      required: true,
                      message: '请填写公告内容',
                    },
                  ],
                })(
                  <TextArea
                    rows={5}
                    autosize={false}
                    maxLength={200}
                    placeholder="请完整描述公告通知内容"
                    style={{ marginBottom: '5px' }}
                  />,
                )}
              </FormItem>
            </Row>
            <Row>
              <FormItem
                {...formItemLayout}
                label="失效时间"
                style={{ paddingBottom: '2px' }}
              >
                {getFieldDecorator('invalidDate', {
                  initialValue: invalidDate
                    ? moment(invalidDate, 'YYYY-MM-DD HH:mm:ss')
                    : invalidDate,
                  rules: [
                    {
                      required: true,
                      message: '请选择失效时间',
                    },
                  ],
                })(
                  <DatePicker
                    format="YYYY-MM-DD HH:mm:ss"
                    showTime={true}
                    style={{ width: '100%' }}
                  />,
                )}
              </FormItem>
            </Row>
          </Form>
        </div>
      </Modal>
    );
  }
  private onSave = () => {
    const { validateFields } = this.props.form;
    const { isInsert } = this.props;
    validateFields((err, values) => {
      if (!err) {
        const opts = Object.assign(values);
        if (!isInsert) {
          opts.utmsNoticeId = this.state.utmsNoticeId;
          opts.isDeleted = 0;
        }
        opts.invalidDate =
          typeof opts.invalidDate === 'string'
            ? opts.invalidDate
            : opts.invalidDate.format('YYYY-MM-DD HH:mm:ss');
        this.props.onSave(opts, isInsert, () => {
          this.setState({
            loading: false,
          });
        });
        this.setState({
          loading: true,
        });
      }
    });
  };
  private onCancel = () => {
    this.props.onCancel();
  };
}
export default Form.create()(EditModal);
