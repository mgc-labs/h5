/**
 * Auth Service
 * @author ggm
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

const API_GET_DATA_LIST =
  '/ehuodiGateway/utmsCore/utmsNoticecs/selectUtmsNoticeList';
const API_INSERT_DATA = '/ehuodiGateway/utmsCore/utmsNoticecs/addUtmsNotice';
const API_UPDATE_DATA = '/ehuodiGateway/utmsCore/utmsNoticecs/modifyUtmsNotice';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_GET_DATA_LIST)
    .reply(() => import('./mock/mock').then(exports => [200, exports.default]));
  mocker
    .on('post', API_INSERT_DATA)
    .reply(() =>
      import('./mock/insert').then(exports => [200, exports.default]),
    );
}

export const getDataList = opts =>
  request(
    {
      url: API_GET_DATA_LIST,
      data: qs.stringify(opts),
      method: 'post',
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
export const insertData = opts =>
  request(
    {
      url: API_INSERT_DATA,
      data: qs.stringify(opts),
      method: 'post',
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
export const updateData = opts =>
  request(
    {
      url: API_UPDATE_DATA,
      data: qs.stringify(opts),
      method: 'post',
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
export const deleteData = opts =>
  request(
    {
      url: API_UPDATE_DATA,
      data: qs.stringify(opts),
      method: 'post',
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  );
