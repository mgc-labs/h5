/**
 * Proclamation selectors
 * @author ggm
 * @date 2018/9/14 下午4:28:11
 */
import { List, Map } from 'immutable';
import { createSelector } from 'reselect';
const initialState = Map({
  data: List(),
  error: false,
  loading: false,
  dataCount: 0,
});
export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any> = initialState) =>
    state.get('error'),
  );

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any> = initialState) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any> = initialState) =>
    state.get('data'),
  );
export const makeSelectDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any> = initialState) =>
    state.get('dataCount'),
  );
