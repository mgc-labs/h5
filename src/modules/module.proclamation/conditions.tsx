/**
 * Proclamation Component
 * @author ggm
 * @date 2018/9/14 下午4:28:11
 */
import * as React from 'react';

import Button from 'antd/es/button';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Form, { FormComponentProps } from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Row from 'antd/es/row';
import { conditionItemLayout } from 'utils/conditionLayout';
import styles from './index.module.less';

const formItemLayout = { ...conditionItemLayout, colon: false };
const FormItem = Form.Item;
const { RangePicker } = DatePicker;

export interface IConditionsProps extends FormComponentProps {
  doSearch: (options?: object) => any;
}

class Conditions extends React.PureComponent<IConditionsProps> {
  layout = {
    xxl: { span: 6 },
    xl: { span: 6 },
    md: { span: 12 },
    sm: { span: 12 },
    xs: { span: 24 },
  };
  public render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form onSubmit={this.submit}>
          <Row type="flex" gutter={16}>
            <Col {...this.layout}>
              <FormItem {...formItemLayout} label="发布时间：">
                {getFieldDecorator('pubDate')(<RangePicker />)}
              </FormItem>
            </Col>
            <Col {...this.layout}>
              <FormItem {...formItemLayout} label="关键词搜索">
                {getFieldDecorator('keywords')(
                  <Input placeholder="公告标题/发布人" />,
                )}
              </FormItem>
            </Col>
            <Col {...this.layout}>
              <FormItem {...formItemLayout}>
                <Button
                  type="primary"
                  htmlType="submit"
                  style={{ marginRight: '12px' }}
                >
                  查询
                </Button>
                <Button onClick={this.onClear}>重置</Button>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
  private submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { validateFields } = this.props.form;
    validateFields((err, values) => {
      let dateStart = '';
      let dateEnd = '';
      if (values.pubDate && values.pubDate.length > 0) {
        dateStart = values.pubDate[0].format('YYYY-MM-DD') + ' 00:00:00';
        dateEnd = values.pubDate[1].format('YYYY-MM-DD') + ' 23:59:59';
      }
      const param = {
        dateStart,
        dateEnd,
        keywords: values.keywords,
      };
      const keys = Object.keys(param);
      for (const key of keys) {
        if (!param[key]) {
          delete param[key];
        }
      }
      this.props.doSearch(param);
    });
  };
  private onClear = () => {
    const { resetFields } = this.props.form;
    resetFields();
    this.props.doSearch();
  };
}
export default Form.create()(Conditions);
