/**
 * Proclamation Component
 * @author ggm
 * @date 2018/9/14 下午4:28:11
 */
import msngr from 'msngr';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Divider from 'antd/es/divider';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Message from 'antd/es/message';
import Popconfirm from 'antd/es/popconfirm';
import Row from 'antd/es/row';
import Table from 'components/MyTable';
import { List, Map } from 'immutable';
import Conditions from './conditions';
import EditModal from './editModal';
import { deleteData, insertData, updateData } from './service';

import {
  makeSelectData,
  makeSelectDataCount,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

import styles from './index.module.less';

const FormItem = Form.Item;

export interface IProclamationProps {
  children: React.ReactChildren;
  form: WrappedFormUtils;
  dataCount: number;
  loading: boolean;
  data: List<Map<string, any>>;
  getDataList: (opts?: object) => any;
  refresh: (param?: object) => any;
}

class Proclamation extends React.PureComponent<IProclamationProps> {
  public state = {
    utmsNoticeId: '',
    title: '',
    content: '',
    invalidDate: '',
    current: '',
    isInsert: true,
    visible: false,
  };
  conditions = {
    pageSize: 15,
    skipCount: 0,
  };
  componentDidMount() {
    this.props.getDataList(this.conditions);
  }
  public render() {
    const {
      utmsNoticeId,
      title,
      content,
      invalidDate,
      isInsert,
      visible,
    } = this.state;
    return (
      <Card
        title="公告管理"
        extra={
          <Button type="primary" onClick={this.onInsert}>
            新建公告
          </Button>
        }
        className={styles.Proclamation}
      >
        <Conditions doSearch={this.doSearch} />
        {this.renderTable()}
        <EditModal
          utmsNoticeId={utmsNoticeId}
          title={title}
          content={content}
          invalidDate={invalidDate}
          isInsert={isInsert}
          visible={visible}
          onSave={this.onSave}
          onCancel={this.onCancel}
        />
      </Card>
    );
  }
  private renderTable = () => {
    const { loading, dataCount, data } = this.props;
    const { current } = this.state;
    const list = data.toJS();
    const columns = [
      {
        title: '标题',
        // fixed: 'left',
        width: 300,
        dataIndex: 'title',
      },
      {
        title: '发布内容',
        width: 500,
        dataIndex: 'content',
      },
      {
        title: '发布时间',
        width: 190,
        dataIndex: 'gmtCreate',
      },
      {
        title: '发布人',
        width: 200,
        dataIndex: 'createOperatorName',
      },
      {
        title: '发布组织',
        width: 250,
        dataIndex: 'organizationName',
      },
      {
        title: '操作',
        width: 110,
        fixed: 'right',
        render: (text, record) => {
          return (
            <React.Fragment>
              <a href="javascript;" onClick={this.onEdit.bind(this, record)}>
                修改
              </a>
              <Divider type="vertical" />
              <Popconfirm
                placement="bottomRight"
                title={'是否删除该公告'}
                onConfirm={this.onDelete.bind(this, record)}
                okText="是"
                cancelText="否"
              >
                <a href="javascript;">删除</a>
              </Popconfirm>
            </React.Fragment>
          );
        },
      },
    ];
    let width = 0;
    columns.map(col => {
      width += col.width || 0;
    });
    const pagination = {
      current,
      onChange: this.onFetch,
      onShowSizeChange: this.onShowSizeChange,
      total: dataCount,
    };
    const tableProps = {
      columns,
      loading,
      rowKey: 'utmsNoticeId',
      dataSource: list,
      scroll: { x: width },
      pagination,
    };
    return <Table {...tableProps} />;
  };
  private onShowSizeChange = pageInfo => {
    if (pageInfo) {
      pageInfo.current = 1;
    }
    this.onFetch(pageInfo);
  };
  private onFetch = (pageInfo?: any) => {
    this.conditions.skipCount = pageInfo
      ? pageInfo.skipCount
      : this.conditions.skipCount;
    this.conditions.pageSize =
      pageInfo && pageInfo.pageSize
        ? pageInfo.pageSize
        : this.conditions.pageSize;
    this.props.getDataList(this.conditions);
    this.setState({
      current: (pageInfo ? pageInfo.current : 1) || 1,
    });
  };
  private doSearch = param => {
    if (!param) {
      this.conditions = {
        pageSize: this.conditions.pageSize,
        skipCount: 0,
      };
      return this.onFetch();
    }
    this.conditions = {
      pageSize: this.conditions.pageSize,
      skipCount: 0,
      ...param,
    };
    this.onFetch();
  };
  private onInsert = () => {
    this.setState({
      title: '',
      content: '',
      invalidDate: '',
      isInsert: true,
      visible: true,
    });
  };
  private onEdit = (item, e: React.FormEvent<HTMLFontElement>) => {
    e.preventDefault();
    this.setState({
      utmsNoticeId: item.utmsNoticeId,
      title: item.title,
      content: item.content,
      invalidDate: item.invalidDate,
      isInsert: false,
      visible: true,
    });
  };
  private onSave = (opts, isInsert, cb) => {
    const saveFunc = isInsert ? insertData : updateData;
    saveFunc(opts)
      .then(res => {
        cb();
        if (res.result === 'success') {
          Message.success(res.msg);
          this.props.refresh();
        } else {
          Message.error(res.msg);
        }
      })
      .catch(() => {
        cb();
      });
  };
  private onDelete = (item, e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    deleteData({
      utmsNoticeId: item.utmsNoticeId,
      isDeleted: 1,
    })
      .then(res => {
        if (res.result === 'success') {
          Message.success('删除成功');
          this.props.refresh();
        } else {
          Message.error(res.msg);
        }
      })
      .catch(() => {
        Message.error('请稍后重试');
      });
  };
  private onCancel = () => {
    this.setState({
      visible: false,
    });
  };
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getDataList: param => dispatch(actions.getDataList(param)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    dataCount: makeSelectDataCount(selectState),
    data: makeSelectData(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(
    withConnect,
    Form.create(),
  )(Proclamation);
};
