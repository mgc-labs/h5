/**
 * Proclamation Reducers
 * @author ggm
 * @date 2018/9/14 下午4:28:11
 */

import { fromJS, List, Map } from 'immutable';

// The initial state of the App
const initialState = Map({
  data: List(),
  error: false,
  loading: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_ASYNC_DATA:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_LIST_SUCCESS:
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data))
          .set('dataCount', fromJS(action.payload.count));
      case CONSANTS.GET_LIST_ERROR:
        return state.set('error', action.error).set('loading', false);
      default:
        return state;
    }
  };
};
