/**
 * Proclamation Saga
 * @author ggm
 * @date 2018/9/14 下午4:28:11
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import request, { mocker } from 'utils/request';
import { getDataList } from './service';
export default (CONSTANTS, actions) => {
  function* getList(action) {
    try {
      const data = yield call(getDataList, action.payload);
      yield put(actions.getListSuccess(data));
    } catch (err) {
      yield put(actions.getListError(err));
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_DATA_LIST, getList);
  };
};
