/**
 * Proclamation Constants
 * @author ggm
 * @date 2018/9/14 下午4:28:11
 */
export const GET_DATA_LIST = 'PROCLAMATION/GET_DATA_LIST';
export const GET_LIST_SUCCESS = 'PROCLAMATION/GET_LIST_SUCCESS';
export const GET_LIST_ERROR = 'PROCLAMATION/GET_LIST_ERROR';
export const UPDATE_FILTERS = 'PROCLAMATION/UPDATE_FILTERS';
