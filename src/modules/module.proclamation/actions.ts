/**
 * Proclamation Actions
 * @author ggm
 * @date 2018/9/14 下午4:28:11
 */
import { AnyAction } from 'redux';
import { GET_DATA_LIST, GET_LIST_ERROR, GET_LIST_SUCCESS } from './constants';

export default CONSTANS => ({
  getDataList: (options): AnyAction => ({
    payload: options,
    type: CONSTANS.GET_DATA_LIST,
  }),

  getListSuccess: (data): AnyAction => ({
    payload: data,
    type: CONSTANS.GET_LIST_SUCCESS,
  }),

  getListError: (error): AnyAction => ({
    error,
    type: CONSTANS.GET_LIST_ERROR,
  }),
  updateFilter: (data): AnyAction => ({
    payload: data,
    type: CONSTANS.UPDATE_FILTERS,
  }),
});
