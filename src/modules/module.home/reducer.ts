/*
 * Home Reducer
 * @author ryan bian
 */

import { List, Map } from 'immutable';
import { combineReducers } from 'redux-immutable';

const initialState = Map({
  data: Map({
    todayCountData: Map(),
    noticeList: List(),
    todoList: Map(),
    trandStatistics: List(),
  }),
  error: false,
  loading: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_HOMEDATA:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_HOMEDATA_SUCCESS:
        return state
          .setIn(['data', 'todayCountData'], action.payload.todayCountData)
          .setIn(['data', 'noticeList'], action.payload.noticeList)
          .setIn(['data', 'todoList'], action.payload.todoList)
          .setIn(['data', 'trandStatistics'], action.payload.trandStatistics)
          .set('loading', false);
        return state;
      case CONSANTS.GET_HOMEDATA_ERROR:
        return state.set('error', action.error).set('loading', false);

      case CONSANTS.UPDATE_TODAY_DATA:
        return state.set('error', false).set('loading', true);
      case CONSANTS.UPDATE_TODAY_DATA_SUCCESS:
        return state
          .setIn(['data', 'todayCountData'], action.payload.todayCountData)
          .set('loading', false);
      default:
        return state;
    }
  };

  /**
   * 订单统计
   * @param state
   * @param action
   */
  // const orderReducer = (
  //   state = initialState.get('orderStatistics'),
  //   action,
  // ) => {
  //   switch (action.type) {
  //     case CONSANTS.GET_ORDERTODAYSTATISTICS:
  //       return state.set('error', false).set('loading', true);
  //     case CONSANTS.GET_ORDERTODAYSTATISTICS_SUCCESS:
  //       return state.set('loading', false).set('data', action.data);
  //     case CONSANTS.GET_ORDERTODAYSTATISTICS_ERROR:
  //       return state.set('error', action.error).set('loading', false);
  //     default:
  //       return state;
  //   }
  // };

  // const tradeReducer = (
  //   state = initialState.get('statDayTradeList'),
  //   action,
  // ) => {
  //   switch (action.type) {
  //     case CONSANTS.GET_STATDAYTRADELIST:
  //       return state.set('error', false).set('loading', true);
  //     case CONSANTS.GET_STATDAYTRADELIST_SUCCESS:
  //       return state.set('loading', false).set('data', action.data);
  //     case CONSANTS.GET_STATDAYTRADELIST_ERROR:
  //       return state.set('error', action.error).set('loading', false);
  //     default:
  //       return state;
  //   }
  // };

  // return combineReducers({

  //   orderStatistics: orderReducer,
  //   statDayTradeList: tradeReducer,
  // });
};
