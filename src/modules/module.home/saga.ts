/**
 * Home Saga
 * @author ryan bian
 */
import { delay } from 'redux-saga';
import {
  all,
  call,
  put,
  take,
  takeEvery,
  takeLatest,
} from 'redux-saga/effects';

import request, { mocker } from 'utils/request';

import {
  reqNoticeList,
  reqTodayCount,
  reTodoList,
  reTrendStatistics,
} from './service';

export default (CONSTANTS, actions) => {
  function* pollData() {
    while (true) {
      try {
        yield call(delay, 60000);
        yield put(actions.updateTodayData());
        const todayCountData = yield call(reqTodayCount);
        yield put(
          actions.updateTodayDataDone({
            todayCountData,
          }),
        );
      } catch (err) {
        yield put(actions.updateTodayDataError(err));
        return;
      }
    }
  }

  function* getData() {
    try {
      const [todayCountData, noticeList, todoList, trandStatistics] = yield all(
        [
          call(reqTodayCount),
          call(reqNoticeList),
          call(reTodoList),
          call(reTrendStatistics),
        ],
      );

      yield put(
        actions.getHomeDataDone({
          todayCountData,
          noticeList,
          todoList,
          trandStatistics,
        }),
      );

      yield pollData();
    } catch (err) {
      yield put(actions.getHomeDataError(err));
    }
  }

  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_HOMEDATA, getData);
  };
};
