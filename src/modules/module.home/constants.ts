/**
 * Home Constants
 * @author ryan bian
 */
export const GET_HOMEDATA = 'GET_HOMEDATA';
export const GET_HOMEDATA_SUCCESS = 'GET_HOMEDATA_SUCCESS';
export const GET_HOMEDATA_ERROR = 'GET_HOMEDATA_ERROR';

export const UPDATE_TODAY_DATA = 'UPDATE_TODAY_DATA';
export const UPDATE_TODAY_DATA_SUCCESS = 'UPDATE_TODAY_DATA_SUCCESS';
export const UPDATE_TODAY_DATA_ERROR = 'UPDATE_TODAY_DATA_SUCCESS';
