export default {
  result: 'success',
  count: 1,
  data: {
    total: 1234, // 总订单数量
    inTransit: 1222, // 运输中数量
    signed: 10, // 已签收数量
    unSigned: 2, // 未签收数量
  },
  code: null,
  msg: '绑定成功',
};
