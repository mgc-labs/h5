export default {
  result: 'success',
  count: 1,
  data: [
    {
      date: '09-01',
      tradeCount: 12,
    },
    {
      date: '09-02',
      tradeCount: 239849,
    },
    {
      date: '09-03',
      tradeCount: 52,
    },
    {
      date: '09-04',
      tradeCount: 76,
    },
    {
      date: '09-05',
      tradeCount: 90,
    },
    {
      date: '09-06',
      tradeCount: 123,
    },
    {
      date: '09-07',
      tradeCount: 234,
    },
    {
      date: '09-08',
      tradeCount: 543,
    },
    {
      date: '09-09',
      tradeCount: 1567,
    },
    {
      date: '09-10',
      tradeCount: 1231,
    },
    {
      date: '09-11',
      tradeCount: 3289,
    },
    {
      date: '09-12',
      tradeCount: 12,
    },
    {
      date: '09-13',
      tradeCount: 23489,
    },
    {
      date: '09-14',
      tradeCount: 12,
    },
    {
      date: '09-15',
      tradeCount: 134,
    },
    {
      date: '09-16',
      tradeCount: 12,
    },
    {
      date: '09-17',
      tradeCount: 12,
    },
    {
      date: '09-18',
      tradeCount: 12,
    },
    {
      date: '09-19',
      tradeCount: 23452,
    },
    {
      date: '09-20',
      tradeCount: 348,
    },
    {
      date: '09-21',
      tradeCount: 12,
    },
    {
      date: '09-22',
      tradeCount: 23489,
    },
    {
      date: '09-23',
      tradeCount: 12,
    },
    {
      date: '09-24',
      tradeCount: 234,
    },
    {
      date: '09-25',
      tradeCount: 12,
    },
    {
      date: '09-26',
      tradeCount: 12312,
    },
    {
      date: '09-27',
      tradeCount: 23423,
    },
    {
      date: '09-28',
      tradeCount: 138942,
    },
    {
      date: '09-29',
      tradeCount: 12,
    },
    {
      date: '09-30',
      tradeCount: 138492,
    },
    {
      date: '09-31',
      tradeCount: 12,
    },
  ],
  code: 'EHD123456789123',
  msg: '查询成功',
};
