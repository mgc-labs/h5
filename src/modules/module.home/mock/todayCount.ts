export default {
  result: 'success',
  count: 18,
  data: {
    totalPOWO: '1231',
    distributionPOWO: '21321',
    haveSignedInPOWO: '21312',
    notSignedPOWO: '1231',
    totalTO: '21321',
    pendingSchedulingTO: '21312',
    distributionTO: '1231',
    finishedTO: '21321',
  },
  code: 'EHD123456789123',
  msg: '统计成功 ',
};
// export default {
//   result: 'error',
//   count: 0,
//   data: null,
//   code: 'EHD123456789123',
//   msg: '统计失败',
// };
