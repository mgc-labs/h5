export default {
  result: 'success',
  count: null,
  data: [
    {
      utmsNoticeId: 1129,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      title: '通知', // 标题
      content: '1、这里是公告的内容，这里是公告', // 内容
      invalidDate: '2018-09-07 10:10:12', // 失效时间
    },
    {
      utmsNoticeId: 1128,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      title: '通知', // 标题
      content: '2、重要通知，这里是公告内容', // 内容
      invalidDate: '2018-09-07 10:10:12', // 失效时间
    },
    {
      utmsNoticeId: 1127,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      title: '通知', // 标题
      content: '3、重要通知，这里是公告内容', // 内容
      invalidDate: '2018-09-07 10:10:12', // 失效时间
    },
    {
      utmsNoticeId: 1126,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      title: '通知', // 标题
      content: '4、重要通知，这里是公告内容', // 内容
      invalidDate: '2018-09-07 10:10:12', // 失效时间
    },
    {
      utmsNoticeId: 1125,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      title: '通知', // 标题
      content: '5、重要通知，这里是公告内容', // 内容
      invalidDate: '2018-09-07 10:10:12', // 失效时间
    },
    {
      utmsNoticeId: 1124,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      title: '通知', // 标题
      content: '6、重要通知，这里是公告内容', // 内容
      invalidDate: '2018-09-07 10:10:12', // 失效时间
    },
  ],
  code: '',
  msg: '公告列表查询成功 ',
};
