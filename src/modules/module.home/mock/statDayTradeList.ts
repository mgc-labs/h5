export default {
  result: 'success',
  count: 1,
  data: {
    totalTradeCount: 120, // 总运单
    waitDispatcherTradeCount: 120, // 待调度
    finishedTradeCount: 120, // 已完成
    transprtingTradeCount: 120, // 运输中
  },
  code: 'EHD123456789123',
  msg: '查询成功',
};
