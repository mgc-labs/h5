/**
 * 首页
 */
import { Map } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import {
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

import styles from './index.module.less';

import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Row from 'antd/es/row';
import Spin from 'antd/es/spin';

import 'echarts/lib/chart/line';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/title';
import 'echarts/lib/component/tooltip';
import echarts from 'echarts/lib/echarts';

import NoticeBoard from '../../components/NoticeBoard';

const option = {
  axisPointer: {
    lineStyle: {
      color: '#D8D8D8',
    },
  },
  grid: {
    containLabel: true,
    left: 40,
    right: 16,
  },
  series: [
    {
      data: [],
      itemStyle: {
        color: '#6AC9FF',
      },
      showSymbol: false,
      symbol: 'circle',
      type: 'line',
    },
  ],
  tooltip: {
    axisPointer: {
      type: 'line',
    },
    backgroundColor: 'rgba(255,255,255,0.95)',
    extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);',
    formatter: '{b}<br/>订单数：{c}单',
    padding: [10, 15, 10, 15],
    textStyle: '#1E283C',
    trigger: 'axis',
  },
  xAxis: {
    axisLabel: {
      color: '#9BA0AA',
    },
    axisLine: {
      lineStyle: {
        color: '#9BA0AA',
        width: 2,
      },
    },
    axisTick: {
      show: false,
    },
    data: [],
    interval: 0,
    type: 'category',
  },
  yAxis: {
    axisLabel: {
      color: '#9BA0AA',
    },
    axisLine: {
      show: false,
    },
    axisTick: {
      show: false,
    },
    name: '订单数',
    nameTextStyle: {
      align: 'right',
      color: '#9BA0AA',
      padding: [0, 50, 0, 0],
    },
    splitLine: {
      lineStyle: {
        color: '#E4E7EF',
        type: 'dotted',
      },
    },
    type: 'value',
  },
};

export interface IHomeProps {
  children: React.ReactChildren;
  getHomeData: () => void;
  getOrderStatistics: () => void;
  getStatDayTradeList: () => void;
  loading: boolean;
  data: Map<string, any>;
}

class Home extends React.PureComponent<IHomeProps> {
  private chartEl: HTMLDivElement;
  private chart: echarts.ECharts;
  public componentDidMount() {
    this.chart = echarts.init(this.chartEl);
    this.chart.setOption(option);

    this.props.getHomeData();

    window.onresize = () => {
      this.chart.resize();
    };
  }
  public componentDidUpdate() {
    this.chartData();
  }
  public render() {
    return (
      <div className={styles.Home}>
        <Spin tip="Loading..." spinning={this.props.loading}>
          <Row className={styles.sectionWrap}>{this.getTodoList()}</Row>
          <Row gutter={12} className={styles.sectionWrap}>
            <Col xs={24} lg={16}>
              <Row gutter={12}>
                <Col xs={24} md={12}>
                  {this.getStatDayTrade()}
                </Col>
                <Col xs={24} md={12}>
                  {this.getOrderStatistics()}
                </Col>
              </Row>
            </Col>
            <Col xs={24} lg={8}>
              {this.getNoticePlan()}
            </Col>
          </Row>
          <Row>{this.getTrand()}</Row>
        </Spin>
      </div>
    );
  }
  private chartData() {
    const { data } = this.props;
    const trandStatisticsJs = data.get('trandStatistics') || [];
    let month: number;
    const seriesData: number[] = [];
    const xAxisData: string[] = [];

    trandStatisticsJs.map((item, index) => {
      const dateArr = item.date.split('-');

      if (index === 0) {
        month = parseInt(dateArr[0], 10);
      }
      xAxisData.push(`${parseInt(dateArr[1], 10)}日`);
      seriesData.push(item.tradeCount);
    });

    option.tooltip.formatter = (params: []) => {
      const values = params[0];
      const item = trandStatisticsJs[params[0].dataIndex];
      const month = Number(item.date.split('-')[0]);
      return `${month}月${
        values.axisValue
      }<p style="color: #1E283C;margin-top:5px;">订单数：${values.value}单</p>`;
    };
    // `${month}月{b}<p style="color: #1E283C;margin-top:5px;">订单数：{c}单</p>`
    option.series[0].data = seriesData;
    option.xAxis.data = xAxisData;

    this.chart.setOption(option);
  }
  private getTrand() {
    return (
      <Card
        bodyStyle={{ padding: 0 }}
        title="近30日用车趋势"
        headStyle={{ borderBottom: 'none' }}
        className={styles.situationCont}
      >
        <div
          ref={chart => (this.chartEl = chart)}
          style={{ width: '100%', height: '423px' }}
        />
      </Card>
    );
  }
  private getTodoList() {
    const { data } = this.props;
    const todoListJs = data.get('todoList');
    return (
      <Card
        title="待办事项"
        headStyle={{ borderBottom: 'none' }}
        bodyStyle={{ padding: 0 }}
        className={styles.situationCont}
      >
        <Link to="/stowage" key={'todolist_1'}>
          <Card.Grid className={styles.CardGridStyle}>
            待配载
            <span className={styles.toDoValue}>{todoListJs.waitStowed}</span>
          </Card.Grid>
        </Link>
        <Link to="/transportDispatch" key={'todolist_2'}>
          <Card.Grid className={styles.CardGridStyle}>
            待调度
            <span className={styles.toDoValue}>
              {todoListJs.pendingScheduling}
            </span>
          </Card.Grid>
        </Link>
        <Link to="/waybill/list" key={'todolist_3'}>
          <Card.Grid className={styles.CardGridStyle}>
            待回单
            <span className={styles.toDoValue}>{todoListJs.waitReturn}</span>
          </Card.Grid>
        </Link>
      </Card>
    );
  }
  private getOrderStatistics() {
    const { data } = this.props;
    const todayCountDataJs = data.get('todayCountData') || {};
    return (
      <Card title="当日订单概况" className={styles.situationCont}>
        <p>
          总订单
          <span style={{ fontSize: '16px' }}>
            {todayCountDataJs.totalPOWO || 0}
          </span>
          单
        </p>
        <p>
          配送中
          <span>{todayCountDataJs.distributionPOWO || 0}</span>单
        </p>
        <p>
          已签收
          <span>{todayCountDataJs.haveSignedInPOWO || 0}</span>单
        </p>
        <p>
          未签收
          <span>{todayCountDataJs.notSignedPOWO || 0}</span>单
        </p>
      </Card>
    );
  }
  private getStatDayTrade() {
    const { data } = this.props;
    const todayCountDataJs = data.get('todayCountData') || {};

    return (
      <Card
        title="当日出车概况"
        extra={<Link to="/transportCapacity">查看实时大屏</Link>}
        className={styles.situationCont}
      >
        <p>
          总用车
          <span style={{ fontSize: '16px' }}>
            {todayCountDataJs.totalTO || 0}
          </span>
          车次
        </p>
        <p>
          待调度
          <span>{todayCountDataJs.pendingSchedulingTO || 0}</span>
          车次
        </p>
        <p>
          运输中
          <span>{todayCountDataJs.distributionTO || 0}</span>
          车次
        </p>
        <p>
          已完成
          <span>{todayCountDataJs.finishedTO || 0}</span>
          车次
        </p>
      </Card>
    );
  }
  private getNoticePlan() {
    const { data } = this.props;
    const noticeDataSource = data.get('noticeList') || {};

    return (
      <NoticeBoard
        boardTitle="公司公告"
        dataSource={noticeDataSource}
        length={3}
        bodyStyle={{
          height: '136px',
          padding: '0 16px 4px 16px',
          lineHeight: '1.5',
          overflow: 'hidden',
        }}
      />
    );
  }
}
export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getHomeData: () => dispatch(actions.getHomeData()),
    getOrderStatistics: () => dispatch(actions.getOrderTodayStatistics()),
    getStatDayTradeList: () => dispatch(actions.getStatDayTradeList()),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    data: makeSelectData(selectState),
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    // orderStatistics: makeSelectOrderStatisticsData(selectState),
    // statDayTradeList: makeSelectStatDayTradeListData(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(Home);
};
