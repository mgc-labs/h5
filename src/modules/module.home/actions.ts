/**
 * Home Actions
 * @author ryan bian
 */
export default CONSTANTS => ({
  getHomeData: () => ({
    type: CONSTANTS.GET_HOMEDATA,
  }),

  getHomeDataDone: data => ({
    payload: data,
    type: CONSTANTS.GET_HOMEDATA_SUCCESS,
  }),

  getHomeDataError: error => ({
    error: true,
    payload: error,
    type: CONSTANTS.GET_HOMEDATA_ERROR,
  }),

  updateTodayData: () => ({
    type: CONSTANTS.UPDATE_TODAY_DATA,
  }),

  updateTodayDataDone: data => ({
    payload: data,
    type: CONSTANTS.UPDATE_TODAY_DATA_SUCCESS,
  }),

  updateTodayDataError: error => ({
    error: true,
    payload: error,
    type: CONSTANTS.UPDATE_TODAY_DATA_ERROR,
  }),
});
