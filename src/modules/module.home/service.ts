/**
 * Auth Service
 * @author ryan bian
 */
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_URL = {
  ORDERSTATISTICS: '/ehuodiGateway/utmsTrade/order/orderTodayStatistics',
  REQSTATDAYTRADELIST: '/ehuodiGateway/utmsTrade/utmsTrade/statDayTradeList',
  NOTICELIST: '/ehuodiGateway/utmsCore/utmsNoticecs/selectUtmsNoticeList',
  TODOLIST: '/ehuodiGateway/utmsTrade/index/indexWaitDoBusiness',
  TRENDSTATISTICS:
    '/ehuodiGateway/utmsTrade/utmsTrade/statUseCarConditionThirty',
  TODAYCOUNT: '/ehuodiGateway/utmsTrade/index/indexTodayCount',
};

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_URL.TODAYCOUNT)
    .reply(() =>
      import('./mock/todayCount').then(exports => [200, exports.default]),
    );

  // mocker
  //   .on('post', API_URL.NOTICELIST)
  //   .reply(() =>
  //     import('./mock/noticeList').then(exports => [200, exports.default]),
  //   );

  mocker
    .on('post', API_URL.TODOLIST)
    .reply(() =>
      import('./mock/todoList').then(exports => [200, exports.default]),
    );

  mocker
    .on('post', API_URL.TRENDSTATISTICS)
    .reply(() =>
      import('./mock/trendStatistics').then(exports => [200, exports.default]),
    );
}

/**
 * 首页当日概况
 */
export const reqTodayCount = params =>
  request(
    {
      method: 'post',
      url: API_URL.TODAYCOUNT,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

/**
 * 公告列表查询
 * @param params
 */
export const reqNoticeList = params =>
  request(
    {
      method: 'post',
      url: API_URL.NOTICELIST,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

/**
 * 待办事项
 * @param params
 */
export const reTodoList = params =>
  request(
    {
      method: 'post',
      url: API_URL.TODOLIST,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);

/**
 * 近30日用车趋势
 * @param params
 */
export const reTrendStatistics = params =>
  request(
    {
      method: 'post',
      url: API_URL.TRENDSTATISTICS,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res.data);
