/**
 * CustomerAdd Component
 * @author lhf
 * @date 2018-10-24 14:27:35
 */
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';
import message from 'antd/es/message';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Upload from 'antd/es/upload';
import AddressSelect from 'components/AddressSelect';
import CarModelCarLong from 'components/CarModelCarLong';
import moment from 'moment';

import {
  addDictionary,
  deleteDictionary,
  getDictionary,
  getSaveHttp,
} from 'components/CustomerAdd/service';
import IndustryPicker from 'components/IndustryPicker';
import MutiSelectWithEditableOptions from 'components/MutiSelectWithEditableOptions';
import StaffPicker from 'components/StaffPicker';
import { fromJS, List } from 'immutable';

import * as React from 'react';
import { createStructuredSelector } from 'reselect';
import {
  // makeSelectCustomerTags,
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

import styles from './index.module.less';

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const confirm = Modal.confirm;
export interface ICustomerAddProps {
  children: React.ReactChildren;
  loading: boolean;
  form: WrappedFormUtils;
  searchParams: {
    id: number;
  };
  data?: any;
  customerTags?: any;
  getData: (routeId: number) => any;
  toList: (handleClose?) => void;
  close: () => void;
  refresh: (data?) => any;
  eventEmitter: object;
}
interface IState {
  data?: object;
  fileList: [];
  customertagList: List<Map<string, any>>;
  customerTags: [];
}
interface IIndustyData {
  industry: string;
  industryCode: string | number;
}
class CustomerAdd extends React.PureComponent<ICustomerAddProps> {
  state: IState;
  selectedAddress: object;
  industyData: IIndustyData;
  constructor(props) {
    super(props);
    this.state = {
      customertagList: List(),
      customerTags: this.props.data.customerTags || [],
      fileList: [],
    };
  }

  public componentDidMount() {
    const { searchParams, data } = this.props;
    if (searchParams.id) {
      this.props.getData(searchParams.id);
    }
    this.setState({
      customerTags: data.toJS().customerTags || [],
    });
    this.getTags();

    this.props.eventEmitter.on('close', (requestClose, eventSource) => {
      if (
        eventSource === 'FROM_TAB_CLOSE' ||
        eventSource === 'FROM_TAB_REFRESH'
      ) {
        this.cancleEdit(requestClose);
      } else {
        requestClose();
      }
    });
  }
  componentWillReceiveProps(nextprops) {
    if (nextprops.data.toJS().customerTags) {
      this.setState({
        customerTags: nextprops.data.toJS().customerTags || [],
      });
      this.getTags();
    }
  }
  public render() {
    const { custAttachments } = this.props.data.toJS();
    const { id } = this.props.searchParams;

    const state = this.state;
    const formItemLayout = {
      labelCol: {
        span: 5,
      },
      wrapperCol: { span: 16 },
    };
    const {
      getFieldDecorator,
      getFieldValue,
      setFieldsValue,
    } = this.props.form;

    const customerTagsProps = {
      options: state.customertagList,
      placeholder: '创建标签，打标客户，分层管理',
      addInputPlaceHolder: '请输入添加的客户标签',
      addBtnText: '添加标签',
      resetSelect: false,
      isMuti: true,
      tagSize: 5,
      optionSize: 20,
      removeOption: option => this.removeOption('customerTags', option),
      addOption: option => this.addOption('customerTags', option),
      addToForm: departments => this.addToForm('customerTags', departments),
    };

    const defaultFiles = [];
    if (custAttachments && custAttachments.length) {
      custAttachments.map(item => {
        const urlArr = item.attachmentUrl.split('/');
        defaultFiles.push({
          uid: item.attachmentSeq.toString(),
          name: urlArr[urlArr.length - 1],
          status: 'done',
          url: item.attachmentUrl,
        });
      });
    }

    console.log(defaultFiles, 'dddd', custAttachments);
    return (
      <div className={styles.CustomerAdd}>
        <Card title={`${id ? '修改' : '新增'}客户`} bordered={true}>
          <Form layout="horizontal">
            <FormItem {...formItemLayout} label="客户名称：">
              {getFieldDecorator('customerName', {
                rules: [
                  {
                    required: true,
                    message: '请输入客户名称',
                  },
                ],
              })(<Input placeholder="请输入客户名称" />)}
            </FormItem>
            <FormItem {...formItemLayout} required label="联系人：">
              <Row>
                <Col md={8}>
                  <FormItem>
                    {getFieldDecorator('contact', {
                      rules: [
                        {
                          required: true,
                          message: '请输入客户联系人姓名',
                        },
                      ],
                    })(
                      <Input
                        type="text"
                        placeholder="请输入姓名"
                        style={{ width: 100 }}
                        maxLength={20}
                      />,
                    )}
                  </FormItem>
                </Col>
                <Col md={16}>
                  <FormItem>
                    {getFieldDecorator('contactWay', {
                      rules: [
                        {
                          required: true,
                          message: '请输入客户联系方式',
                        },
                        {
                          pattern: /^[\d|\-|\s]*$/,
                          message: '请输入正确的联系方式',
                        },
                      ],
                    })(
                      <Input
                        type="text"
                        placeholder="请输入联系方式"
                        maxLength={20}
                      />,
                    )}
                  </FormItem>
                </Col>
              </Row>
            </FormItem>
            <FormItem {...formItemLayout} label="客户编码">
              {getFieldDecorator('customerCode', {
                rules: [
                  {
                    required: true,
                    message: '请输入客户编码',
                  },
                ],
              })(<Input placeholder="请输入客户编码" />)}
            </FormItem>
            <FormItem {...formItemLayout} label="行业分类：">
              {getFieldDecorator('industry', {})(
                <IndustryPicker onChange={this.handleIndustyPicker} />,
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="结算方式：">
              {getFieldDecorator('settlementType', {})(
                <IndustryPicker
                  type="SettlementType"
                  placeholder="请选择结算方式"
                />,
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="合作期限：">
              {getFieldDecorator('cooperateDate')(
                <RangePicker style={{ width: '100%' }} />,
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="客户打标：">
              {getFieldDecorator('customerTags', {})(
                <MutiSelectWithEditableOptions {...customerTagsProps} />,
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="公司地址">
              {getFieldDecorator('address', {})(
                <AddressSelect
                  placeholder="请输入公司地址"
                  onSelected={this.onSelectedAddress}
                  style={{ width: '100%' }}
                />,
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="禁入车型：">
              {getFieldDecorator('offLimitCarTypes')(
                <CarModelCarLong placeholder="请选择车型" />,
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="销售经理：">
              {getFieldDecorator('salesman', {})(<StaffPicker maxLength={5} />)}
            </FormItem>
            <FormItem {...formItemLayout} label="上传文件：">
              {getFieldDecorator('custAttachments')(
                <Upload
                  action="/ehuodiGateway/utmsCore/basecs/uploadCustAttachment"
                  onChange={this.handleUploadFile}
                  defaultFileList={defaultFiles}
                >
                  <Button disabled={state.fileList.length >= 5}>
                    <Icon type="upload" /> 点击上传
                  </Button>
                </Upload>,
              )}
            </FormItem>
            <div style={{ textAlign: 'center' }}>
              <Button
                key={0}
                type="primary"
                htmlType="submit"
                onClick={e => {
                  this.handleSubmit(e);
                }}
              >
                保存
              </Button>
              <Button
                style={{ marginLeft: 10 }}
                key={1}
                onClick={() => {
                  this.cancleEdit();
                }}
              >
                取消
              </Button>
            </div>

            {/* <CustomerAddModal data={data.toJS()} onCancel={this.cancleEdit} /> */}
          </Form>
        </Card>
      </div>
    );
  }

  private handleIndustyPicker = (value, options) => {
    this.industyData = options;
  };
  // 处理上传文件
  private handleUploadFile = file => {
    const data = file.fileList.map((item, index) => {
      return {
        attachmentseq: index,
        attachmenturl: item.status === 'done' ? item.response.data : '',
      };
    });
    this.setState({
      fileList: data,
    });
  };
  private save = async values => {
    const propsdata = this.props.data.toJS();

    const cooperateDate = values.cooperateDate;
    const dateData = {
      cooperateBeginDate:
        cooperateDate && cooperateDate.length
          ? cooperateDate[0].format('YYYY-MM-DD 00:00:00')
          : '',
      cooperateEndDate:
        cooperateDate && cooperateDate.length
          ? cooperateDate[1].format('YYYY-MM-DD 23:59:59')
          : '',
    };
    const files = {
      custAttachments: JSON.stringify(this.state.fileList),
    };
    const options = {
      ...values,
      ...this.selectedAddress,
      ...dateData,
      ...files,
    };
    const salesman = [];
    if (options.salesman && options.salesman.length) {
      options.salesman.map(d => {
        salesman.push({
          jobcard: d.key,
          name: d.label[0],
        });
      });
      options.salesman = salesman;
    }
    options.offLimitCarTypes = JSON.stringify(options.offLimitCarTypes);
    if (typeof options.customerTags !== 'string') {
      let tags = '';
      options.customerTags.map(d => {
        tags += d.key + ',';
      });
      options.customerTags = tags;
    }

    try {
      const data = await getSaveHttp({
        ...propsdata,
        ...options,
        ...this.industyData,
      });

      if (options.utmsCustomerId) {
        message.success('客户信息修改成功');
      } else {
        message.success('客户新增成功');
      }
      this.props.toList();
    } catch (err) {
      const msg = err && err.msg ? err.msg : '网络错误，请重试...';
      message.error(msg || '操作失败');
    }
  };
  private handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.save(values);
      }
    });
  };
  // 选择地址之后
  private onSelectedAddress = options => {
    this.selectedAddress = options || {
      region: '',
      province: '',
      city: '',
    };
  };
  // 选择客户标签
  private addToForm = (type, options) => {
    const { form } = this.props;
    const tags = options
      .filter(option => {
        return option.checked;
      })
      .map(option => {
        return option.value;
      })
      .join(',');
    form.setFieldsValue({
      customerTags: tags,
    });
    setTimeout(() => {
      this.setState({
        customerTags: options.filter(option => option.checked),
        customertagList: options,
      });
    }, 0);
  };
  // 新增客户打标
  private addOption = async (type, option) => {
    const params = {
      bizType: type,
      dictionaryCode: option,
    };
    const result = await addDictionary(params);
    if (result.result === 'success') {
      await this.getTags();
      // this.setState({
      //   customertagList: fromJS(await this.getTags()),
      // });
    }
  };
  // 删除客户打标
  private removeOption = async (type, option) => {
    const params = {
      bizType: type,
      dictionaryCode: option,
    };
    const result = await deleteDictionary(params);
    if (result.result === 'success') {
      await this.getTags();
    }
  };
  // 获取全部客户标签
  private getTags = async () => {
    const result = await getDictionary({
      parentDictionaryCode: 'customerTags',
    });
    if (result.result === 'success') {
      const tags = [];
      let oldTags = '';
      const { customerTags, customertagList } = this.state;
      customerTags.map(d => {
        oldTags += d.key + ',';
      });
      result.data.map((d, i) => {
        let checked = false;
        if (oldTags.includes(d.dictionaryName)) {
          customerTags.map(tag => {
            if (tag.key === d.dictionaryName) {
              checked = tag.checked;
            }
          });
        }

        const t = {
          key: d.dictionaryName,
          value: d.dictionaryName,
          checked,
        };
        tags.push(t);
      });
      this.setState({
        customertagList: tags,
      });
    }
  };

  private cancleEdit = (requestClose?) => {
    const comparativeFields = ['customerName'];
    let showConfirm = false; // 判断是否需要触发离开提示窗口

    if (this.props.form.isFieldsTouched(comparativeFields)) {
      showConfirm = true;
    }
    if (showConfirm) {
      confirm({
        title: '有未保存的信息',
        content: '如放弃，填写的信息将丢失',
        okText: '继续填写',
        cancelText: '放弃',
        centered: true,
        onCancel: () => {
          if (requestClose) {
            requestClose();
          } else {
            this.props.toList();
          }
        },
      });
    } else {
      if (requestClose) {
        requestClose();
      } else {
        this.props.toList();
      }
    }
  };
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        data: makeSelectData(currentState),
        // customerTags: makeSelectCustomerTags(currentState),
        error: makeSelectError(currentState),
        loading: makeSelectLoading(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        toList: options => dispatch(actions.toList(options)),
        getData: id => dispatch(actions.getAsyncData(id)),
      }),
    };
  })(
    Form.create({
      mapPropsToFields(props: any) {
        const data = props.data.toJS();
        const fields = {};
        Object.keys(data || {}).forEach(d => {
          let itemData = data[d];

          if (d === 'salesman' && itemData && itemData.length) {
            const salesman = [];
            JSON.parse(itemData).map(item => {
              salesman.push({
                key: item.jobcard,
                label: [item.name],
              });
            });
            itemData = salesman;
          }
          if (d === 'offLimitCarTypes' && itemData && itemData.length) {
            itemData = JSON.parse(itemData);
          }
          // if (d === 'custAttachments' && itemData && itemData.length) {
          //   const files = [];
          //   itemData.map(item => {
          //     const urlArr = item.attachmentUrl.split('/');
          //     files.push({
          //       uid: item.attachmentSeq.toString(),
          //       url: item.attachmentUrl,
          //       status: 'done',
          //       name: urlArr[urlArr.length - 1],
          //     });
          //   });
          //   itemData = files;
          // }
          fields[d] = Form.createFormField({
            value: itemData,
          });
          fields.cooperateDate = Form.createFormField({
            value: [
              moment(data.cooperateBeginDate),
              moment(data.cooperateEndDate),
            ],
          });
        });
        console.log(fields, 'ffff');
        return {
          ...fields,
        };
      },
    })(CustomerAdd),
  );
};
