import message from 'antd/es/message';
import { push } from 'connected-react-router';
import { fromJS, List, Map } from 'immutable';
import { call, put, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import { getData } from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      data: List(),
      error: false,
      loading: false,
    }),
    actions: {
      GET_ASYNC_DATA: id => id,
      GET_ASYNC_DATA_ERROR: error => error,
      GET_ASYNC_DATA_SUCCESS: data => data,
      TO_LIST: handleClose => handleClose,
    },
    reducers: {
      GET_ASYNC_DATA(state, action) {
        return state.set('error', false).set('loading', true);
      },
      GET_ASYNC_DATA_ERROR(state, action) {
        return state.set('error', action.payload).set('loading', false);
      },
      GET_ASYNC_DATA_SUCCESS(state, action) {
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data));
      },
      TO_LIST(state) {
        return state.set('error', false).set('loading', false);
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchData(action) {
        try {
          const data = yield call(getData, { utmsCustomerId: action.payload });
          const customerTags = data.data.customerTags || '';
          // const tags = customerTags.split(',').map(d => ({
          //   key: d,
          //   value: d,
          //   checked: true,
          // }));
          data.data.customerTags =
            customerTags.split(',').map(d => ({
              key: d,
              value: d,
              checked: true,
            })) || [];
          yield put(actions.getAsyncDataSuccess(data));
        } catch (err) {
          yield put(actions.getAsyncDataError(err));
          message.error(err.toString());
        }
      }

      function* toList() {
        yield put(push('/customer'));
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ASYNC_DATA, fetchData);
        yield takeLatest(CONSTANTS.TO_LIST, toList);
      };
    },
  });
