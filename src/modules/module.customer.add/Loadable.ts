/**
 * CustomerAdd Loadable
 * @author lhf
 * @date 2018-10-24 14:27:35
 */
import Loadable from 'react-loadable';

import LoadingIndicator from 'components/LoadingIndicator';

let Component;
if (process.env.NODE_ENV === 'development') {
  Component = require('./index').default;
} else {
  Component = config =>
    Loadable({
      loader: () =>
        import('./index').then(exports => {
          return exports.default(config);
        }),
      loading: LoadingIndicator,
    });
}

export default Component;

export { default as modelFactory } from './model';
