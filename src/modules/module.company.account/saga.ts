/**
 * CompanyAccount Saga
 * @author HuangSiFei
 * @date 2018-9-13 14:37:34
 */
import message from 'antd/es/message';
import qs from 'qs';
import { call, put, takeLatest } from 'redux-saga/effects';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_ACCOUNTINFO = '/ehuodiGateway/utmsCore/utmsClientcs/selectUtmsClient';
const API_UPDATEINVOICE =
  '/ehuodiGateway/utmsCore/utmsClientcs/updateUtmsClientInvoice';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_ACCOUNTINFO)
    .reply(() =>
      import('./mock/account').then(exports => [200, exports.default]),
    );
}

/**
 * 获取账户信息
 */
const getAccountInfo = () =>
  request(
    {
      method: 'post',
      url: API_ACCOUNTINFO,
    },
    {
      useMock: false,
    },
  ).then(res => res.data);

/**
 * 更新开票信息
 */
const updateInvoice = params =>
  request(
    {
      method: 'post',
      url: API_UPDATEINVOICE,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => {
    message.success('保存成功');
    return res.data;
  });

export default (CONSTANTS, actions) => {
  function* fetchAccountInfo() {
    try {
      const data = yield call(getAccountInfo);
      const isInvoiceEmpty =
        !data.invoiceRise &&
        !data.invoiceDuty &&
        !data.invoiceAddress &&
        !data.invoiceMobileNumber &&
        !data.openBank &&
        !data.openAccountNumber;
      yield put(actions.getAccountDataDone(data, isInvoiceEmpty));
    } catch (err) {
      yield put(actions.getAccountDataError(err));
    }
  }

  function* updateInvoiceData(action) {
    try {
      const data = yield call(updateInvoice, action.payload);
      const isInvoiceEmpty =
        !action.payload.invoiceRise &&
        !action.payload.invoiceDuty &&
        !action.payload.invoiceAddress &&
        !action.payload.invoiceMobileNumber &&
        !action.payload.openBank &&
        !action.payload.openAccountNumber;
      yield put(actions.updateInvoiceDataDone(data, isInvoiceEmpty));
    } catch (err) {
      yield put(actions.updateInvoiceDataError(err));
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_ACCOUNT_DATA, fetchAccountInfo);
    yield takeLatest(CONSTANTS.UPDATE_INVOICE_DATA, updateInvoiceData);
  };
};
