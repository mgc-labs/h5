/**
 * CompanyAccount Reducers
 * @author HuangSiFei
 * @date 2018-9-13 14:37:34
 */

import { fromJS, Map } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  accountInfo: Map(),
  error: false,
  submitError: false,
  loading: false,
  isInvoiceEmpty: false,
  toggleStatus: 'details',
});

export default CONSTANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSTANTS.GET_ACCOUNT_DATA:
        return state.set('error', false).set('loading', true);
      case CONSTANTS.GET_ACCOUNT_DATA_SUCCESS:
        return state
          .set('loading', false)
          .set('accountInfo', fromJS(action.payload))
          .set('isInvoiceEmpty', action.isInvoiceEmpty);
      case CONSTANTS.GET_ACCOUNT_DATA_ERROR:
        return state.set('error', action.error).set('loading', false);
      case CONSTANTS.UPDATE_INVOICE_DATA:
        return state.set('submitError', false).set('loading', true);
      case CONSTANTS.UPDATE_INVOICE_DATA_SUCCESS:
        return state
          .set('loading', false)
          .set('isInvoiceEmpty', action.isInvoiceEmpty)
          .set('toggleStatus', 'details');
      case CONSTANTS.UPDATE_INVOICE_DATA_ERROR:
        return state.set('submitError', action.error).set('loading', false);
      case CONSTANTS.TOGGLE_STATUS:
        return state.set(
          'toggleStatus',
          action === 'edit' ? 'details' : 'edit',
        );
      default:
        return state;
    }
  };
};
