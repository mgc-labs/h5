/**
 * CompanyAccount selectors
 * @author HuangSiFei
 * @date 2018-9-13 14:37:34
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectInvoiceEmpty = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('isInvoiceEmpty'),
  );

export const makeSelectAccountInfo = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('accountInfo'),
  );
export const makeSelectSubmitError = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('submitError'),
  );
export const makeSelectToggleStatus = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('toggleStatus'),
  );
