/**
 * account info Component
 * @author HuangSiFei
 * @date 2018-9-13 14:37:34
 */
import Col from 'antd/es/col';
import Row from 'antd/es/row';
import * as React from 'react';
import styles from './index.module.less';

export default ({ details }) => {
  const expireDate = `${details.get('buyCreateDate')}至${details.get(
    'buyEndDate',
  )}`;
  let versionCode = '';
  switch (details.get('versionsCode')) {
    case 'P':
      versionCode = '基础版';
      break;
    case 'Z':
      versionCode = '智能版';
      break;
    case 'D':
      versionCode = '定制版';
      break;
  }

  return (
    <div>
      <Row>
        <Col className={styles.detailWrapper}>
          <div className={styles.label}>当前账户：</div>
          <div className={styles.value}>{details.get('clientName')}</div>
        </Col>
      </Row>
      <Row>
        <Col className={styles.detailWrapper}>
          <div className={styles.label}>组织编码：</div>
          <div className={styles.value}>{details.get('domainName')}</div>
        </Col>
      </Row>
      <Row>
        <Col className={styles.detailWrapper}>
          <div className={styles.label}>版本：</div>
          <div className={styles.value}>{versionCode}</div>
        </Col>
      </Row>
      <Row>
        <Col className={styles.detailWrapper}>
          <div className={styles.label}>版本号：</div>
          <div className={styles.value}>{details.get('versionNo')}</div>
        </Col>
      </Row>
      <Row>
        <Col className={styles.detailWrapper}>
          <div className={styles.label}>有效期：</div>
          <div className={styles.value}>{expireDate}</div>
        </Col>
      </Row>
      <Row>
        <Col className={styles.detailWrapper}>
          <div className={styles.label}>累积保驾护航：</div>
          <div className={styles.value}>{details.get('applyDays')}天</div>
        </Col>
      </Row>
    </div>
  );
};
