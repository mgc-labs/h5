/**
 * CompanyAccount Actions
 * @author HuangSiFei
 * @date 2018-9-13 14:37:34
 */
import { AnyAction } from 'redux';

export default CONSTANTS => ({
  getAccountData: (): AnyAction => ({
    type: CONSTANTS.GET_ACCOUNT_DATA,
  }),

  getAccountDataDone: (data, isInvoiceEmpty): AnyAction => ({
    payload: data,
    isInvoiceEmpty,
    type: CONSTANTS.GET_ACCOUNT_DATA_SUCCESS,
  }),

  getAccountDataError: (error): AnyAction => ({
    error,
    type: CONSTANTS.GET_ACCOUNT_DATA_ERROR,
  }),

  updateInvoiceData: (data): AnyAction => ({
    payload: data,
    type: CONSTANTS.UPDATE_INVOICE_DATA,
  }),

  updateInvoiceDataDone: (data, isInvoiceEmpty): AnyAction => ({
    payload: data,
    isInvoiceEmpty,
    type: CONSTANTS.UPDATE_INVOICE_DATA_SUCCESS,
  }),

  updateInvoiceDataError: (error): AnyAction => ({
    error,
    type: CONSTANTS.UPDATE_INVOICE_DATA_ERROR,
  }),

  updateStatus: (data): AnyAction => ({
    data,
    type: CONSTANTS.TOGGLE_STATUS,
  }),
});
