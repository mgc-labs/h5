/**
 * CompanyAccount Component
 * @author HuangSiFei
 * @date 2018-9-13 14:37:34
 */
import Form, { ComponentDecorator } from 'antd/es/form';
import Tabs from 'antd/es/tabs';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import AccountInfo from './accountInfo';

import Button from 'antd/es/button';
import Col from 'antd/es/col';
import Input from 'antd/es/input';
import Row from 'antd/es/row';
import { List, Map } from 'immutable';

import './index.less';
import styles from './index.module.less';

import {
  makeSelectAccountInfo,
  makeSelectError,
  makeSelectInvoiceEmpty,
  makeSelectLoading,
  makeSelectSubmitError,
  makeSelectToggleStatus,
} from './selectors';

const { TabPane } = Tabs;
const { Item: FormItem } = Form;

export interface ICompanyAccountProps {
  children: React.ReactChildren;
  accountInfo: Map<string, string | number>;
  form: ComponentDecorator;
  toggleStatus: string;
  permissions: List<Map<string, any>>;
  isInvoiceEmpty: boolean;
  submitError: boolean;
  updateStatus: (key: string) => any;
  getAccountData: () => any;
  getFieldDecorator: (key: string, options: any) => any;
  updateInvoiceData: (data?: object) => any;
}

class CompanyAccount extends React.PureComponent<ICompanyAccountProps> {
  constructor(props) {
    super(props);
    // this.state = {
    //   toggleStatus: 'details',
    // };
  }
  public componentDidMount() {
    this.props.getAccountData();
  }

  public renderTabs() {
    const tabProps = {
      defaultActiveKey: '1',
      type: 'card',
    };
    const { permissions } = this.props;
    const permissionArray = [];
    permissions.toArray().map(cur => {
      return permissionArray.push(cur.get('operateKey'));
    });
    return (
      <div className={'cardContainer'}>
        <Tabs {...tabProps}>
          <TabPane tab="账户信息" key="1">
            {this.renderAccount()}
          </TabPane>
          {permissionArray.includes('edit') ? (
            <TabPane tab="开票信息" key="2">
              {this.renderInvoice()}
            </TabPane>
          ) : (
            ''
          )}
        </Tabs>
      </div>
    );
  }

  public toggleBtn(e, toggleStatus) {
    e.preventDefault();
    const { accountInfo } = this.props;
    const { getFieldValue } = this.props.form;
    const status =
      !getFieldValue('invoiceRise') &&
      !getFieldValue('invoiceDuty') &&
      !getFieldValue('invoiceAddress') &&
      !getFieldValue('invoiceMobileNumber') &&
      !getFieldValue('openBank') &&
      !getFieldValue('openAccountNumber')
        ? 'edit'
        : toggleStatus === 'details'
          ? 'edit'
          : 'details';
    // this.setState({
    //   toggleStatus: status,
    // });
    this.props.updateStatus(toggleStatus);
  }

  public handleSubmit = (e, toggleStatus) => {
    const { accountInfo } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        // this.toggleBtn(e, toggleStatus);
        this.props.updateInvoiceData(
          Object.assign(
            { utmsClientId: accountInfo.get('utmsClientId') },
            values,
          ),
        );
      }
    });
  };

  public renderInvoice() {
    const { accountInfo, isInvoiceEmpty, toggleStatus } = this.props;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
        lg: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 20 },
        sm: { span: 20 },
        lg: { span: 20 },
      },
    };
    const formItemColStyle = {
      lg: { span: 8 },
      sm: { span: 12 },
      xs: { span: 24 },
    };
    // const { toggleStatus } = this.state;
    const isEdit = isInvoiceEmpty ? true : toggleStatus === 'edit';
    return (
      <Form
        layout="vertical"
        onSubmit={e => this.handleSubmit(e, toggleStatus)}
      >
        <Row>
          <Col {...formItemColStyle}>
            <FormItem {...formItemLayout} label="发票抬头：">
              {getFieldDecorator('invoiceRise', {
                initialValue: accountInfo.get('invoiceRise'),
              })(<Input disabled={!isEdit} maxLength={50} />)}
            </FormItem>
          </Col>
          <Col {...formItemColStyle}>
            <FormItem {...formItemLayout} label="发票税号：">
              {getFieldDecorator('invoiceDuty', {
                initialValue: accountInfo.get('invoiceDuty'),
              })(<Input disabled={!isEdit} maxLength={20} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col {...formItemColStyle}>
            <FormItem {...formItemLayout} label="开票地址：">
              {getFieldDecorator('invoiceAddress', {
                initialValue: accountInfo.get('invoiceAddress'),
              })(<Input disabled={!isEdit} maxLength={500} />)}
            </FormItem>
          </Col>
          <Col {...formItemColStyle}>
            <FormItem {...formItemLayout} label="开票电话：">
              {getFieldDecorator('invoiceMobileNumber', {
                initialValue: accountInfo.get('invoiceMobileNumber'),
              })(<Input disabled={!isEdit} maxLength={13} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col {...formItemColStyle}>
            <FormItem {...formItemLayout} label="开户银行：">
              {getFieldDecorator('openBank', {
                initialValue: accountInfo.get('openBank'),
              })(<Input disabled={!isEdit} maxLength={50} />)}
            </FormItem>
          </Col>
          <Col {...formItemColStyle}>
            <FormItem {...formItemLayout} label="开户账号：">
              {getFieldDecorator('openAccountNumber', {
                initialValue: accountInfo.get('openAccountNumber'),
              })(<Input disabled={!isEdit} maxLength={32} />)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col>
            <FormItem>
              {isInvoiceEmpty || toggleStatus === 'edit' ? (
                <Button type="primary" htmlType="submit">
                  保存
                </Button>
              ) : (
                <Button onClick={e => this.toggleBtn(e, toggleStatus)}>
                  编辑
                </Button>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  public renderAccount() {
    const { accountInfo } = this.props;
    const accountInfoProps = {
      details: accountInfo,
    };
    return <AccountInfo {...accountInfoProps} />;
  }

  public render() {
    return <div>{this.renderTabs()}</div>;
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getAccountData: () => dispatch(actions.getAccountData()),
    updateInvoiceData: data => dispatch(actions.updateInvoiceData(data)),
    updateStatus: status => dispatch(actions.updateStatus()),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    accountInfo: makeSelectAccountInfo(selectState),
    isInvoiceEmpty: makeSelectInvoiceEmpty(selectState),
    submitError: makeSelectSubmitError(selectState),
    toggleStatus: makeSelectToggleStatus(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(
    withConnect,
    Form.create(),
  )(CompanyAccount);
};
