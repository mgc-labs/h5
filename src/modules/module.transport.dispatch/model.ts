/**
 * TransportDispatch model
 * @author chengle
 * @date 2018-9-18 15:28:01
 */
import message from 'antd/es/message';
import { fromJS, List, Map } from 'immutable';
import { call, put, takeLatest } from 'redux-saga/effects';
import modelFactory from 'utils/modelFactory';

import {
  getVehicleDetails,
  getVehicleList,
  getWaybillList,
  setResourcedispatch,
} from './service';

export default namespace =>
  modelFactory({
    namespace,
    state: Map({
      vehicleData: List(),
      vehicleError: false,
      vehicleLoading: false,
      vehicleDataCount: 0,
      waybillData: List(),
      waybillError: false,
      waybillLoading: false,
      waybillDataCount: 0,
    }),
    actions: {
      GET_ASYNC_VEHICLELIST: data => data,
      GET_ASYNC_VEHICLELIST_ERROR: error => error,
      GET_ASYNC_VEHICLELIST_SUCCESS: data => data,
      GET_ASYNC_WAYBILLLIST: data => data,
      GET_ASYNC_WAYBILLLIST_ERROR: error => error,
      GET_ASYNC_WAYBILLLIST_SUCCESS: data => data,
    },
    reducers: {
      GET_ASYNC_VEHICLELIST(state, action) {
        return state.set('vehicleError', false).set('vehicleLoading', true);
      },
      GET_ASYNC_VEHICLELIST_ERROR(state, action) {
        return state
          .set('vehicleError', action.payload)
          .set('vehicleLoading', false);
      },
      GET_ASYNC_VEHICLELIST_SUCCESS(state, action) {
        return state
          .set('vehicleLoading', false)
          .set('vehicleDataCount', action.payload.count)
          .set('vehicleData', fromJS(action.payload.data));
      },
      GET_ASYNC_WAYBILLLIST(state, action) {
        return state.set('waybillError', false).set('waybillLoading', true);
      },
      GET_ASYNC_WAYBILLLIST_ERROR(state, action) {
        return state
          .set('waybillError', action.payload)
          .set('waybillLoading', false);
      },
      GET_ASYNC_WAYBILLLIST_SUCCESS(state, action) {
        return state
          .set('waybillLoading', false)
          .set('waybillDataCount', action.payload.count)
          .set('waybillData', fromJS(action.payload.data));
      },
    },
    effects(CONSTANTS, actions) {
      function* fetchVehicle(action) {
        const { payload } = action;
        try {
          const data = yield call(getVehicleList, payload);
          yield put(actions.getAsyncVehiclelistSuccess(data));
        } catch (err) {
          yield put(actions.getAsyncVehiclelistError(err));
          message.error(err.toString());
        }
      }

      function* fetchWaybill(action) {
        const { payload } = action;
        try {
          const data = yield call(getWaybillList, payload);
          yield put(actions.getAsyncWaybilllistSuccess(data));
        } catch (err) {
          yield put(actions.getAsyncWaybilllistError(err));
          message.error(err.toString());
        }
      }

      /**
       * Root saga manages watcher lifecycle
       */
      return function* rootSaga() {
        yield takeLatest(CONSTANTS.GET_ASYNC_VEHICLELIST, fetchVehicle);
        yield takeLatest(CONSTANTS.GET_ASYNC_WAYBILLLIST, fetchWaybill);
      };
    },
  });
