/**
 * TransportDispatch Component
 * @author chengle
 * @date 2018-9-18 15:28:01
 */
import * as React from 'react';
import { createStructuredSelector } from 'reselect';
import { Map } from './components/map';

import {
  makeSelectVehicleData,
  makeSelectVehicleDataCount,
  makeSelectVehicleError,
  makeSelectVehicleLoading,
  makeSelectWaybillData,
  makeSelectWaybillDataCount,
  makeSelectWaybillError,
  makeSelectWaybillLoading,
} from './selectors';

import { ImmutableListMap } from 'utils/types';
import styles from './index.module.less';

export interface ITransportDispatchProps {
  children: React.ReactChildren;
  vehicleData: ImmutableListMap;
  getAsyncVehiclelist: (options?: object) => any;
  vehicleLoading: boolean;
  vehicleDataCount: number;
  waybillData: ImmutableListMap;
  getAsyncWaybilllist: (options?: object) => any;
  waybillLoading: boolean;
  waybillDataCount: number;
}

class TransportDispatch extends React.PureComponent<ITransportDispatchProps> {
  public componentDidMount() {
    // this.props.getAsyncData();
  }
  public render() {
    const {
      vehicleData,
      getAsyncVehiclelist,
      vehicleLoading,
      vehicleDataCount,
      waybillData,
      getAsyncWaybilllist,
      waybillLoading,
      waybillDataCount,
    } = this.props;
    return (
      <div className={styles.TransportDispatch}>
        <Map
          vehicleData={vehicleData}
          getAsyncVehiclelist={getAsyncVehiclelist}
          vehicleLoading={vehicleLoading}
          vehicleDataCount={vehicleDataCount}
          waybillData={waybillData}
          getAsyncWaybilllist={getAsyncWaybilllist}
          waybillLoading={waybillLoading}
          waybillDataCount={waybillDataCount}
        />
      </div>
    );
  }
}

export default ({ routeId, connectModel }) => {
  return connectModel(actions => {
    const currentState = state => state.get(routeId);
    return {
      mapStateToProps: createStructuredSelector({
        vehicleError: makeSelectVehicleError(currentState),
        vehicleLoading: makeSelectVehicleLoading(currentState),
        vehicleData: makeSelectVehicleData(currentState),
        vehicleDataCount: makeSelectVehicleDataCount(currentState),
        waybillError: makeSelectWaybillError(currentState),
        waybillLoading: makeSelectWaybillLoading(currentState),
        waybillData: makeSelectWaybillData(currentState),
        waybillDataCount: makeSelectWaybillDataCount(currentState),
      }),
      mapDispatchToProps: dispatch => ({
        getAsyncVehiclelist: options =>
          dispatch(actions.getAsyncVehiclelist(options)),
        getAsyncWaybilllist: options =>
          dispatch(actions.getAsyncWaybilllist(options)),
      }),
    };
  })(TransportDispatch);
};
