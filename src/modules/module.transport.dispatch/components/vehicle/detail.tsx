import { Alert, Avatar, Button, Form, Icon, Input, Modal } from 'antd';

import CarModelCarLong from 'components/CarModelCarLong';
import React, { PureComponent } from 'react';
import GLOBAL from 'utils/GLOBAL.ts';
import { getVehicleDetails } from '../../service';

const FormItem = Form.Item;

class DetailModule extends PureComponent {
  state = {
    data: {},
  };

  public async componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      await getVehicleDetails({
        utmsDriverId: nextProps.utmsDriverId,
      }).then(data => {
        this.setState({
          data: data.data,
        });
      });
    }
  }

  render() {
    // console.log(this.props);
    const { visible, onCancel, onCreate } = this.props;
    const formItemLayout = {
      labelCol: {
        span: 6,
      },
      wrapperCol: {
        span: 15,
      },
    };
    return (
      <Modal
        visible={visible}
        onOk={onCreate}
        onCancel={() => {
          this.setState({ data: {} });
          onCancel();
        }}
      >
        <div style={{ textAlign: 'center' }}>
          <Avatar
            size={64}
            icon="user"
            src={this.state.data.profilePicture || GLOBAL.emptyRecord}
          />
          <h1 style={{ margin: '10px 0 0 0' }}>
            {this.state.data.driverName || GLOBAL.emptyRecord}
          </h1>
          <p>{this.state.data.phoneNumber || GLOBAL.emptyRecord}</p>
        </div>
        <Form layout="horizontal">
          <FormItem {...formItemLayout} label="车辆类型">
            {this.state.data.vehicleLong
              ? this.state.data.vehicleLong / 1000 +
                '米' +
                this.state.data.vehicleType
              : GLOBAL.emptyRecord}
          </FormItem>
          <FormItem {...formItemLayout} label="绑定车辆">
            {this.state.data.plateNumber || GLOBAL.emptyRecord}
          </FormItem>
          <FormItem {...formItemLayout} label="驾照类型">
            {this.state.data.driverLicenseType || GLOBAL.emptyRecord}
          </FormItem>
          <FormItem {...formItemLayout} label="年龄">
            {this.state.data.age || GLOBAL.emptyRecord}
          </FormItem>
          <FormItem {...formItemLayout} label="驾龄">
            {this.state.data.drivingAge || GLOBAL.emptyRecord}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default DetailModule;
