import {
  Button,
  Card,
  Cascader,
  Col,
  Divider,
  Form,
  Icon,
  Input,
  InputNumber,
  List,
  message,
  Modal,
  Pagination,
  Row,
  Select,
  Tag,
} from 'antd';
import CarModelCarLong from 'components/CarModelCarLong';
import { ImmutableListMap } from 'utils/types';

import DetailModule from './detail';
import ShortcutModule from './shortcut';

import * as React from 'react';

import styles from './styles.module.less';

import GLOBAL from 'utils/GLOBAL.ts';
import { setResourcedispatch } from '../../service';

const FormItem = Form.Item;
const Search = Input.Search;
const Option = Select.Option;

interface IMapVehicleProps {
  vehicleData: ImmutableListMap;
  selectVehicle: object;
  transfer: (options?: object) => any;
  getAsyncVehiclelist: (options?: object) => any;
  vehicleLoading: boolean;
  vehicleDataCount: number;
  selectWaybill: object;
  initFn: any;
}

@Form.create()
export class Vehicle extends React.PureComponent<IMapVehicleProps, any> {
  constructor(props) {
    super(props);
    this.state = {
      listData: [],
      vehicleStatus: false,
      shortcutVisible: false,
      shortcut: {
        visible: false,
        confirmLoading: false,
        title: '快捷派单',
      },
      shortcutStatus: 1,
      detail: {
        visible: false,
      },
      selectVehicle: props.selectVehicle,
      selectWaybill: { utmsTradeWayPoints: [] },
      current: 1,
      pageSize: 10,
    };
  }
  public componentWillReceiveProps(nextProps) {
    if (!nextProps.vehicleData.equals(this.props.vehicleData)) {
      this.setState({ listData: nextProps.vehicleData.toJS() }, () => {
        this.selectActive(
          nextProps.selectVehicle,
          nextProps.selectVehicle.index,
        );
      });
    }
    if (
      nextProps.selectWaybill &&
      nextProps.selectWaybill.tradeNumber !==
        this.props.selectWaybill.tradeNumber
    ) {
      this.setState({ selectWaybill: nextProps.selectWaybill }, () => {
        this.props.form.setFieldsValue({
          carModelCarLong: [
            {
              value:
                nextProps.selectWaybill.fromCarLong / 1000 +
                '米' +
                nextProps.selectWaybill.fromCarStruct,
            },
          ],
        });
        setTimeout(this.seve, 100);
      });
    }
    if (nextProps.selectVehicle.index !== this.props.selectVehicle.index) {
      this.selectActive(nextProps.selectVehicle, nextProps.selectVehicle.index);
    }
  }
  public handleSubmit = e => {
    e.preventDefault();
    this.setState({ current: 1 }, this.seve);
  };
  public seve = () => {
    const value = this.props.form.getFieldsValue();
    const ops = {
      searchInfo: value.searchInfo,
    };
    if (value.carModelCarLong && value.carModelCarLong.length > 0) {
      ops.vehicleType = value.carModelCarLong
        ? value.carModelCarLong[0].vehicleType
        : '';
      ops.vehicleLong = value.carModelCarLong
        ? value.carModelCarLong[0].vehicleLong
        : '';
    }
    this.props.getAsyncVehiclelist(
      Object.assign(
        {
          longitude:
            this.state.selectWaybill.tradeNumber &&
            this.state.selectWaybill.utmsTradeWayPoints[0]
              ? this.state.selectWaybill.utmsTradeWayPoints[0].longitude
              : 120.209761,
          latitude:
            this.state.selectWaybill.tradeNumber &&
            this.state.selectWaybill.utmsTradeWayPoints[0]
              ? this.state.selectWaybill.utmsTradeWayPoints[0].latitude
              : 30.255578,
          sortRule: this.state.selectWaybill.tradeNumber ? 2 : 1,
          skipCount: (this.state.current - 1) * this.state.pageSize,
          pageSize: this.state.pageSize,
          enterCoordinateType: 'BD09',
          outputCoordinateType: 'BD09',
        },
        ops,
      ),
    );
    this.setState({ selectVehicle: {} }, () => {
      this.props.transfer({});
      const arr = this.state.listData.map(item => {
        item.active = false;
        return item;
      });
      this.setState({ listData: arr });
    });
  };
  public handleReset = () => {
    this.props.form.resetFields();
    this.setState({ current: 1, selectVehicle: {} }, () => {
      this.seve();
    });
  };

  public handleOk = () => {
    const { selectVehicle, selectWaybill, shortcutStatus } = this.state;
    const form = this.formRef.props.form;
    form.validateFields(async (err, values) => {
      if (err) {
        return;
      }
      let ops = {};
      if (selectVehicle.utmsDriverId) {
        ops = {
          step: shortcutStatus,
          phoneNumber: selectVehicle.phoneNumber,
          freight: values.driverIncome,
          tradeNumber: selectWaybill.tradeNumber,
        };
      } else {
        ops = {
          step: shortcutStatus,
          phoneNumber: values.driverMobilenumber,
          freight: values.driverIncome,
          tradeNumber: selectWaybill.tradeNumber,
        };
      }
      if (shortcutStatus === 2 || shortcutStatus === 3) {
        ops.plateNumber = values.driverCarplatenumber;
      }
      if (shortcutStatus === 3) {
        ops.vehicleType = values.carModelCarLong[0].vehicleType;
        ops.vehicleLong = values.carModelCarLong[0].vehicleLong;
      }
      this.setState({ shortcut: { confirmLoading: true } });
      await setResourcedispatch(ops).then(
        data => {
          message.success(selectWaybill.status === 0 ? '派单成功' : '改派成功');
          this.setState(
            {
              shortcut: { visible: false, confirmLoading: false },
            },
            () => {
              form.resetFields();
              this.props.initFn();
            },
          );
        },
        err => {
          this.setState({ shortcut: { visible: true, confirmLoading: false } });
          if (err.code) {
            if (err.code === 'UTMS0020301001') {
              this.setState({ shortcutStatus: 2 });
            }
            if (err.code === 'UTMS0020301002') {
              this.setState({ shortcutStatus: 3 });
            }
            if (err.code === 'UTMS0020301003') {
              this.setState({ shortcutStatus: 3, errCode: err.code }, () => {
                form.setFieldsValue({
                  driverCarplatenumber: err.data.plateNumber,
                });
              });
            }
          } else {
            message.error(
              selectWaybill.status === 0
                ? '派单失败 ' + err.msg
                : '改派失败 ' + err.msg,
            );
          }
        },
      );
    });
  };
  public handleCancel = () => {
    this.setState({ shortcut: { visible: false } });
  };

  public saveFormRef = formRef => {
    this.formRef = formRef;
  };

  public toggleTopCollapse = () => {
    this.setState({
      vehicleStatus: !this.state.vehicleStatus,
    });
  };
  public selectActive = (it, i) => {
    if (it.active) {
      return;
    }
    this.setState(
      {
        selectVehicle: it,
      },
      () => {
        const items = this.state.listData.map((item, index) => {
          if (i === index) {
            item.active = true;
          } else {
            item.active = false;
          }
          return item;
        });
        this.setState({ listData: items });
        this.props.transfer(it);
      },
    );
  };

  public sendOrders = (item, type) => {
    if (!this.state.selectWaybill.tradeNumber) {
      message.error('请先选择货源');
      return;
    }
    const form = this.formRef.props.form;
    if (type === 1) {
      this.setState(
        {
          shortcut: { visible: true, title: '快捷派单' },
          shortcutStatus: 1,
        },
        form.setFieldsValue({
          driverIncome: this.state.selectWaybill.cost,
        }),
      );
    }

    if (type === 2) {
      this.setState(
        {
          shortcut: { visible: true, title: '派单' },
          shortcutStatus: 1,
        },
        form.setFieldsValue({
          driverIncome: this.state.selectWaybill.cost,
          driverMobilenumber: item.phoneNumber,
        }),
      );
    }

    if (type === 3) {
      this.setState(
        {
          shortcut: { visible: true, title: '改派' },
          shortcutStatus: 1,
        },
        form.setFieldsValue({
          driverIncome: this.state.selectWaybill.cost,
          driverMobilenumber: item.phoneNumber,
        }),
      );
    }
  };

  render() {
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    const { vehicleData, vehicleLoading, vehicleDataCount } = this.props;
    const formItemLayout = {
      labelCol: {
        span: 7,
      },
      wrapperCol: {
        span: 16,
      },
    };

    return (
      <div className={styles.vehicleContainer}>
        <Card
          type="inner"
          title="运力列表"
          className={[
            styles.detailBox,
            this.state.vehicleStatus ? styles.detailBoxHide : '',
          ].join(' ')}
          bodyStyle={{ paddingLeft: 10, paddingRight: 10 }}
          extra={
            <a
              href="javascript:;"
              onClick={this.toggleTopCollapse.bind(this, 1)}
            >
              <i
                className={
                  styles.memu +
                  ' ' +
                  (this.state.vehicleStatus ? styles.memuUP : styles.memuDOWN)
                }
              />
            </a>
          }
        >
          <div style={{ borderBottom: '1px solid #E4E7EF', marginBottom: 5 }}>
            <Form
              className={styles.searchCondition}
              onSubmit={this.handleSubmit}
              style={{ marginBottom: 16 }}
            >
              <div className="pl5 pr5 mb10">
                <Button
                  type="primary"
                  block
                  onClick={this.sendOrders.bind(this, '', 1)}
                >
                  手机号快捷派单
                </Button>
              </div>
              <FormItem
                {...formItemLayout}
                label="车型车长"
                className={styles.carModelCar}
                style={{ marginBottom: 5 }}
              >
                {getFieldDecorator('carModelCarLong')(
                  <CarModelCarLong maxSelect={1} noHot={true} />,
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="关键词搜索">
                {getFieldDecorator('searchInfo', {
                  initialValue: '',
                })(<Input placeholder="请输入司机姓名/联系电话" />)}
              </FormItem>
              <FormItem className="clearfix">
                <Button
                  type="primary"
                  className="pull-right ml10 mr5"
                  htmlType="submit"
                >
                  查 询
                </Button>
                <Button onClick={this.handleReset} className="pull-right">
                  重 置
                </Button>
              </FormItem>
            </Form>
          </div>
          <div className={styles.blockBox}>
            <List
              style={{ overflow: 'auto', height: '100%' }}
              itemLayout="vertical"
              dataSource={this.state.listData}
              loading={vehicleLoading}
              renderItem={(item, i) => (
                <List.Item key={item.utmsUsersDriversId}>
                  <div
                    className={[
                      styles.blockTag,
                      item.active ? styles.active : '',
                    ].join(' ')}
                    onClick={this.selectActive.bind(this, item, i)}
                  >
                    <Row className="mb5">
                      <Col span={24}>
                        <label className={styles.label}>
                          姓名：
                          <span className={styles.span}>
                            {item.driverName || GLOBAL.emptyRecord}
                          </span>
                        </label>
                      </Col>
                    </Row>
                    <Row className="mb5">
                      <Col span={24}>
                        <label className={styles.label}>
                          联系电话：
                          <span className={styles.span}>
                            {item.phoneNumber}
                          </span>
                        </label>
                      </Col>
                    </Row>
                    <Row className="mb5">
                      <Col span={24}>
                        <label className={styles.label}>
                          车型：
                          <span className={styles.span}>
                            {item.vehicleLong
                              ? item.vehicleLong / 1000 +
                                '米' +
                                item.vehicleType
                              : ''}
                          </span>
                        </label>
                      </Col>
                    </Row>
                    <Row className="mb5">
                      <Col span={24}>
                        {this.props.selectWaybill.tradeNumber ? (
                          <span className={styles.span}>
                            距离发货地：
                            {item.distance ? item.distance.toFixed(2) : ''} 公里
                          </span>
                        ) : (
                          ''
                        )}
                      </Col>
                    </Row>
                    <Row>
                      <Col span={24}>
                        {!this.state.selectWaybill.tradeNumber ||
                        this.state.selectWaybill.status === 0 ? (
                          <Button
                            className="pull-right ml10 mr5"
                            type="primary"
                            onClick={this.sendOrders.bind(this, item, 2)}
                          >
                            派单
                          </Button>
                        ) : (
                          <Button
                            className="pull-right ml10 mr5"
                            type="primary"
                            onClick={this.sendOrders.bind(this, item, 3)}
                          >
                            改派
                          </Button>
                        )}
                        <Button
                          className="pull-right"
                          onClick={() => {
                            this.setState({
                              detail: {
                                visible: true,
                                utmsDriverId: item.utmsDriverId,
                              },
                            });
                          }}
                        >
                          详情
                        </Button>
                      </Col>
                    </Row>
                  </div>
                </List.Item>
              )}
            />
          </div>
          <Pagination
            hideOnSinglePage={true}
            className="mt5"
            current={this.state.current}
            onChange={page => {
              this.setState(
                {
                  current: page,
                },
                this.seve,
              );
            }}
            total={this.props.vehicleDataCount}
            pageSize={10}
            size="small"
            showQuickJumper
            style={{ textAlign: 'left' }}
          />
        </Card>
        <ShortcutModule
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.shortcut.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleOk}
          confirmLoading={this.state.shortcut.confirmLoading}
          selectVehicle={this.state.selectVehicle}
          title={this.state.shortcut.title}
          status={this.state.shortcutStatus}
          errCode={this.state.errCode}
        />
        <DetailModule
          visible={this.state.detail.visible}
          onCancel={() => {
            this.setState({ detail: { visible: false } });
          }}
          onCreate={() => {
            this.setState({ detail: { visible: false } });
          }}
          utmsDriverId={this.state.detail.utmsDriverId}
        />
      </div>
    );
  }
}
