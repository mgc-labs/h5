import { Alert, Button, Form, Icon, Input, InputNumber, Modal } from 'antd';

import CarModelCarLong from 'components/CarModelCarLong';
import React, { PureComponent } from 'react';
import { MOBILE_PHONE } from 'utils/commonRegExp.ts';
import styles from './styles.module.less';

const FormItem = Form.Item;

class FormModule extends PureComponent {
  render() {
    const {
      visible,
      onCancel,
      onCreate,
      form,
      confirmLoading,
      formContent,
      title,
      errCode,
    } = this.props;
    // console.log(this.props.title);
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        span: 6,
      },
      wrapperCol: {
        span: 15,
      },
    };
    return (
      <Modal
        centered={true}
        maskClosable={false}
        title={title}
        visible={visible}
        confirmLoading={confirmLoading}
        onOk={onCreate}
        onCancel={() => {
          form.resetFields();
          onCancel();
        }}
      >
        {this.props.status > 1 ? (
          <Alert
            message="该司机未绑定车辆信息，请补充车辆信息"
            type="info"
            showIcon
            className="mb10"
          />
        ) : (
          ''
        )}
        <Form layout="horizontal">
          <FormItem {...formItemLayout} label="司机号码">
            {getFieldDecorator('driverMobilenumber', {
              rules: [
                { required: true, message: '请输入司机手机号码!' },
                {
                  pattern: MOBILE_PHONE,
                  message: '请输入正确的联系方式',
                },
              ],
            })(
              <Input
                maxLength="20"
                disabled={
                  this.props.selectVehicle.utmsDriverId && title !== '快捷派单'
                }
              />,
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="运价">
            {getFieldDecorator('driverIncome', {
              rules: [{ required: true, message: '请输入运价!' }],
            })(
              <InputNumber
                maxLength="9"
                precision={2}
                step={0.01}
                min={0}
                style={{ width: 295 }}
              />,
            )}
          </FormItem>
          {this.props.status === 2 || this.props.status === 3 ? (
            <FormItem {...formItemLayout} label="车牌号码">
              {getFieldDecorator('driverCarplatenumber', {
                rules: [{ required: true, message: '请输入车牌号码!' }],
              })(
                <Input
                  maxLength="10"
                  disabled={
                    this.props.status === 3 &&
                    errCode &&
                    errCode === 'UTMS0020301003'
                  }
                />,
              )}
            </FormItem>
          ) : (
            ''
          )}
          {this.props.status === 3 ? (
            <FormItem {...formItemLayout} label="车辆类型">
              {getFieldDecorator('carModelCarLong', {
                rules: [{ required: true, message: '请选择车辆类型!' }],
              })(<CarModelCarLong maxSelect={1} noHot={true} />)}
            </FormItem>
          ) : (
            ''
          )}
        </Form>
      </Modal>
    );
  }
}

const ShortcutModule = Form.create()(FormModule);

export default ShortcutModule;
