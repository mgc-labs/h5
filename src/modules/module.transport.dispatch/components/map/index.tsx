import * as React from 'react';
import ReactDOM from 'react-dom';

import styles from './styles.module.less';

import { ImmutableListMap } from 'utils/types';

import { Vehicle } from '../vehicle';
import { Waybill } from '../waybill';

import HuoSvg from './img/location_huo@2x.png';
import XieSvg from './img/location_xie@2x.png';
import ZhuangSvg from './img/location_zhuang@2x.png';

declare const BMap: any;
declare const BMAP_STATUS_SUCCESS: any;

interface IMapProps {
  vehicleData: ImmutableListMap;
  getAsyncVehiclelist: (options?: object) => any;
  vehicleLoading: boolean;
  vehicleDataCount: number;
  waybillData: ImmutableListMap;
  getAsyncWaybilllist: (options?: object) => any;
  waybillLoading: boolean;
  waybillDataCount: number;
}

export class Map extends React.PureComponent<IMapProps, any> {
  state = {
    goodsMarkers: [],
    selectedGoodsMarker: [],
    waybillData: [],
    selectWaybill: {},
    driverMarkers: [],
    // selectedDriverMarker: [],
    vehicleData: [],
    selectVehicle: {},
    bigDriver: {},
    current: 1,
    skipCount: 0,
    pageSize: 10,
  };
  private divMap: HTMLDivElement;
  private map: any;
  componentDidMount() {
    this.initMap();
    this.init();
  }
  init = () => {
    this.props.getAsyncWaybilllist({
      skipCount: this.state.skipCount,
      pageSize: this.state.pageSize,
      coordinateType: 'BD09',
    });
    this.props.getAsyncVehiclelist({
      sortRule: 1,
      longitude: 120.209761,
      latitude: 30.255578,
      enterCoordinateType: 'BD09',
      outputCoordinateType: 'BD09',
      skipCount: this.state.skipCount,
      pageSize: this.state.pageSize,
    });
  };

  componentWillReceiveProps(nextProps) {
    if (!nextProps.waybillData.equals(this.props.waybillData)) {
      this.setState({ waybillData: nextProps.waybillData.toJS() }, () => {
        if (this.state.waybillData) {
          this.addGoodsInMap();
        }
      });
    }
    if (!nextProps.vehicleData.equals(this.props.vehicleData)) {
      this.setState({ vehicleData: nextProps.vehicleData.toJS() }, () => {
        if (this.state.vehicleData) {
          this.addVehicleInMap();
        }
      });
    }
  }

  removeElement = (element: Element) => {
    if (element) {
      element.parentElement.removeChild(element);
    }
  };

  initMap = () => {
    setTimeout(() => {
      this.map = new BMap.Map(this.divMap, {
        minZoom: 5,
        maxZoom: 19,
        enableMapClick: false,
      });
      this.map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
      // 去掉地图左下解的LOGO与文字
      this.map.addEventListener('tilesloaded', () => {
        this.removeElement(document.querySelector('.anchorBL'));
        this.removeElement(document.querySelector('.BMap_cpyCtrl'));
      });
      this.map.centerAndZoom('杭州', 13);
    });
  };

  // 货源展示
  addGoodsInMap = () => {
    this.removeMarkers(this.state.goodsMarkers);
    this.removeMarkers(this.state.selectedGoodsMarker);
    this.setState({ goodsMarkers: [], selectedGoodsMarker: [] });
    this.state.waybillData.map((item, i) => {
      // 创建标注
      if (item.utmsTradeWayPoints.length === 0) {
        return;
      }
      const marker = new BMap.Marker(
        new BMap.Point(
          item.utmsTradeWayPoints[0].longitude,
          item.utmsTradeWayPoints[0].latitude,
        ),
        {
          icon: new BMap.Icon(HuoSvg, new BMap.Size(36, 43)),
        },
      );
      marker.index = i;
      this.map.addOverlay(marker);
      // marker.setZIndex(999);
      const goodsMarkers = this.state.goodsMarkers;
      goodsMarkers.push(marker);
      this.setState({
        goodsMarkers,
      });
      /*点击标志显示路线和终点*/
      marker.onclick = event => {
        const mark = event.currentTarget;
        if (mark) {
          const select = item;
          select.index = mark.index;
          this.setState({ selectWaybill: select }, this.goodSelect);
        }
      };
    });
  };
  // 选中货源，显示规划路径
  goodSelect = () => {
    this.removeMarkers(this.state.selectedGoodsMarker);
    this.setState({ selectedGoodsMarker: [] });
    const obj = this.state.selectWaybill;
    if (!obj.tradeNumber) {
      return;
    }
    const startIcon = new BMap.Icon(ZhuangSvg, new BMap.Size(36, 43));
    const endIcon = new BMap.Icon(XieSvg, new BMap.Size(36, 43));
    let sites = obj.utmsTradeWayPoints.map(item => {
      if (item.longitude) {
        let marker = {};
        if (item.loadType === 0) {
          marker = new BMap.Marker(
            new BMap.Point(item.longitude, item.latitude),
            {
              icon: startIcon,
            },
          );
        } else if (item.loadType === 1) {
          marker = new BMap.Marker(
            new BMap.Point(item.longitude, item.latitude),
            {
              icon: endIcon,
            },
          );
        }
        this.map.addOverlay(marker);
        const arr = this.state.selectedGoodsMarker;
        arr.push(marker);
        this.setState({ selectedGoodsMarker: arr });
        return marker;
      }
    });
    sites = sites.filter(s => {
      return s;
    });
    const driving = new BMap.DrivingRoute(this.map, {
      onSearchComplete: results => {
        if (driving.getStatus() === BMAP_STATUS_SUCCESS) {
          let ptsArr = [];
          for (let i = 0; i < results.getPlan(0).getNumRoutes(); i++) {
            const pts = results
              .getPlan(0)
              .getRoute(i)
              .getPath();
            const polyline = new BMap.Polyline(pts, {
              strokeColor: '#227DE6',
              strokeWeight: 8,
              strokeOpacity: 1,
              icons: [draw_line_direction()],
            });
            this.map.addOverlay(polyline);
            const arr = this.state.selectedGoodsMarker;
            arr.push(polyline);
            this.setState({ selectedGoodsMarker: arr });
            ptsArr = ptsArr.concat(ptsArr, pts);
          }
          this.map.setViewport(ptsArr);
        }
      },
    });
    const draw_line_direction = () => {
      const icons = new BMap.IconSequence(
        new BMap.Symbol('M0 -4 L-3 -1 L0 -3 L3 -1 Z', {
          scale: 0.9,
          strokeWeight: 1,
          strokeColor: '#fff',
        }),
        '100%',
        '5%'
      );
      return icons;
    };
    driving.search(sites[0], sites[sites.length - 1], {
      waypoints: sites.slice(1, sites.length - 1),
    });
  };

  // 车辆
  addVehicleInMap = () => {
    // console.log(this.state.driverMarkers);
    this.removeMarkers(this.state.driverMarkers);
    // this.removeMarkers(this.state.selectedDriverMarker);
    this.setState(
      {
        driverMarkers: [],
        // selectedDriverMarker: [],
        bigDriver: {},
      },
      () => {
        this.state.vehicleData.map((item, i) => {
          const point = new BMap.Point(item.longitude, item.latitude);
          let html = '';
          let marker = {};
          html = this.driverMarkerDiv(item, false);
          marker = new BMapLib.RichMarker(html, point, {
            anchor: new BMap.Size(-30, 0),
          });
          this.map.addOverlay(marker);
          marker.data = item;
          marker.data.driverIndex = i;
          marker.index = i;
          const driverMarkers = this.state.driverMarkers;
          driverMarkers.push(marker);
          this.setState({
            driverMarkers,
          });
          marker.onclick = event => {
            const mark = event.currentTarget;
            if (mark) {
              const select = item;
              select.index = mark.index;
              this.setState({ selectVehicle: select }, this.driverSelect);
            }
          };
        });
      },
    );
  };

  driverSelect = () => {
    if (
      this.state.selectWaybill.utmsTradeWayPoints &&
      this.state.selectWaybill.utmsTradeWayPoints[0].longitude &&
      this.state.selectVehicle.phoneNumber
    ) {
      this.map.setViewport([
        new BMap.Point(
          this.state.selectWaybill.utmsTradeWayPoints[0].longitude,
          this.state.selectWaybill.utmsTradeWayPoints[0].latitude,
        ),
        new BMap.Point(
          this.state.selectVehicle.longitude,
          this.state.selectVehicle.latitude,
        ),
      ]);
    } else if (this.state.selectVehicle.phoneNumber) {
      this.map.setViewport([
        new BMap.Point(
          this.state.selectVehicle.longitude,
          this.state.selectVehicle.latitude,
        ),
      ]);
    }
    if (this.state.bigDriver.data) {
      this.state.bigDriver.setContent(
        this.driverMarkerDiv(this.state.bigDriver.data, false),
      );
      ReactDOM.findDOMNode(this.state.bigDriver._container).style.zIndex = 0;
    }
    this.state.vehicleData.map((item, i) => {
      if (
        this.state.selectVehicle &&
        item.phoneNumber === this.state.selectVehicle.phoneNumber
      ) {
        this.state.driverMarkers[i].setContent(
          this.driverMarkerDiv(item, true),
        );
        this.setState({ bigDriver: this.state.driverMarkers[i] });
        // console.log(this.state.driverMarkers[i]);
        ReactDOM.findDOMNode(
          this.state.driverMarkers[i]._container,
        ).style.zIndex = 1;
      }
    });
  };
  driverMarkerDiv = (data, show) => {
    let content = '';
    const icon = {
      高栏: styles.gaolan,
      栏板车: styles.lanban,
      厢式货车: styles.xiangshi,
      中面: styles.mianbao,
      大面: styles.mianbao,
      小面: styles.mianbao,
      面包车: styles.mianbao,
      挂车: styles.guache,
    };
    const backClass = icon[data.vehicleType];
    if (show) {
      content = `<div class="${styles.driverMarker} ${
        styles.driverMarkerBig
      } ${backClass}">
        <div class="${styles.driverMarkerMain}">
          <span>车型：${
            data.vehicleLong
              ? data.vehicleLong / 1000 + '米' + data.vehicleType
              : ''
          }</span><br/>
          <span>距离发货地：${
            data.distance ? data.distance.toFixed(2) : ''
          }公里</span><br/>
          <span>司机： ${data.driverName ? data.driverName : ''}</span><br/>
          <span>时间： ${
            data.lastUploadPositionDate ? data.lastUploadPositionDate : ''
          }</span><br/>
          <span>电话： ${data.phoneNumber}</span><br/>
        </div>
      </div>`;
    } else {
      content =
        "<div class='" +
        styles.driverMarker +
        ' ' +
        backClass +
        "'>" +
        "<div class='" +
        styles.driverMarkerMain +
        "'>" +
        '<span>' +
        data.vehicleLong / 1000 +
        '米</span></div>' +
        '</div>';
    }
    return content;
  };

  // 清除不需要的标志，比如终点，线路等
  removeMarkers = markers => {
    // console.log(markers);
    if (markers) {
      if (markers.length > 0) {
        markers.map(item => {
          item.hide();
          this.map.removeOverlay(item);
        });
        markers = [];
      }
    }
  };

  transferWaybill = obj => {
    this.setState({ selectWaybill: obj }, this.goodSelect);
  };
  transferVehicle = obj => {
    this.setState({ selectVehicle: obj }, this.driverSelect);
  };

  render() {
    const {
      vehicleData,
      getAsyncVehiclelist,
      vehicleLoading,
      vehicleDataCount,
      getAsyncWaybilllist,
      waybillData,
      waybillLoading,
      waybillDataCount,
    } = this.props;
    return (
      <div className={styles.mapContainer}>
        <Waybill
          waybillData={waybillData}
          selectWaybill={this.state.selectWaybill}
          getAsyncWaybilllist={getAsyncWaybilllist}
          waybillLoading={waybillLoading}
          waybillDataCount={waybillDataCount}
          transfer={this.transferWaybill}
          initFn={this.init}
        />
        <Vehicle
          vehicleData={vehicleData}
          getAsyncVehiclelist={getAsyncVehiclelist}
          vehicleLoading={vehicleLoading}
          vehicleDataCount={vehicleDataCount}
          selectWaybill={this.state.selectWaybill}
          selectVehicle={this.state.selectVehicle}
          transfer={this.transferVehicle}
          initFn={this.init}
        />
        <div ref={obj => (this.divMap = obj)} className={styles.mapBox} />
      </div>
    );
  }
}
