import {
  Button,
  Card,
  Cascader,
  Col,
  Divider,
  Form,
  Icon,
  Input,
  InputNumber,
  List,
  Modal,
  Pagination,
  Row,
  Select,
  Tag,
} from 'antd';

import { WrappedFormUtils } from 'antd/es/form/Form';

import * as React from 'react';
import { ImmutableListMap } from 'utils/types';
import styles from './styles.module.less';

const FormItem = Form.Item;
const Search = Input.Search;
const Option = Select.Option;

interface IMapWaybillProps {
  waybillData: ImmutableListMap;
  selectWaybill: object;
  transfer: (options?: object) => any;
  getAsyncWaybilllist: (options?: object) => any;
  waybillLoading: boolean;
  waybillDataCount: number;
  initFn: any;
}

@Form.create()
export class Waybill extends React.PureComponent<IMapWaybillProps, any> {
  constructor(props) {
    super(props);
    this.state = {
      listData: props.waybillData,
      waybillStatus: false,
      selectWaybill: props.selectWaybill,
      current: 1,
      pageSize: 10,
    };
  }

  public componentWillReceiveProps(nextProps) {
    if (!nextProps.waybillData.equals(this.props.waybillData)) {
      this.setState({ listData: nextProps.waybillData.toJS() }, () => {
        this.selectActive(
          nextProps.selectWaybill,
          nextProps.selectWaybill.index,
        );
      });
    }
    if (nextProps.selectWaybill.index !== this.props.selectWaybill.index) {
      this.selectActive(nextProps.selectWaybill, nextProps.selectWaybill.index);
    }
  }

  public handleSubmit = e => {
    this.props.getAsyncWaybilllist(
      Object.assign(
        {
          coordinateType: 'BD09',
          skipCount: (this.state.current - 1) * this.state.pageSize,
          pageSize: this.state.pageSize,
        },
        this.props.form.getFieldsValue(),
      ),
    );
    const arr = this.state.listData.map(item => {
      item.active = false;
      return item;
    });
    this.setState({ listData: arr }, () => {
      this.props.transfer({});
    });
  };

  public toggleTopCollapse = (type, i) => {
    if (type === 1) {
      this.setState({
        waybillStatus: !this.state.waybillStatus,
      });
    }

    if (type === 2) {
      const items = this.state.listData;
      items[i].show = !items[i].show;
      this.setState({
        listData: items,
      });
    }
  };
  public selectActive = (it, i) => {
    if (it.active) {
      return;
    }

    this.setState(
      {
        selectWaybill: it,
      },
      () => {
        const items = this.state.listData.map((item, index) => {
          if (i === index) {
            item.active = true;
          } else {
            item.active = false;
          }
          return item;
        });
        this.setState({ listData: items });
        this.props.transfer(it);
      },
    );
  };

  public selectChange = (key, value) => {
    const ops = {};
    ops[key] = value;
    this.props.form.setFieldsValue(ops);
    this.setState({ current: 1 }, this.handleSubmit);
  };
  public handleReset = () => {
    this.props.form.resetFields();
    this.setState({ current: 1, selectWaybill: {} }, this.handleSubmit);
    this.props.transfer({});
  };

  render() {
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        span: 7,
      },
      wrapperCol: {
        span: 16,
      },
    };
    const status = [
      '待调度',
      '待接单',
      '待签到',
      '待装货',
      '待卸货',
      '已完成',
      '已取消',
      '待改派',
    ];

    return (
      <div className={styles.waybillContainer}>
        <Card
          type="inner"
          title="调度列表"
          className={[
            styles.detailBox,
            this.state.waybillStatus ? styles.detailBoxHide : '',
          ].join(' ')}
          bodyStyle={{ paddingLeft: 10, paddingRight: 10 }}
          extra={
            <a
              href="javascript:;"
              onClick={this.toggleTopCollapse.bind(this, 1)}
            >
              <i
                className={
                  styles.memu +
                  ' ' +
                  (this.state.waybillStatus ? styles.memuUP : styles.memuDOWN)
                }
              />
            </a>
          }
        >
          <div>
            <Form
              className={styles.searchCondition}
              style={{ marginBottom: 16 }}
            >
              <FormItem {...formItemLayout} label="状态" className="mb5">
                {getFieldDecorator('status', {
                  initialValue: '',
                })(
                  <Select onChange={this.selectChange.bind(this, 'status')}>
                    <Option value="">全部</Option>
                    <Option value="0">待调度</Option>
                    <Option value="1">待接单</Option>
                    <Option value="7">待改派</Option>
                  </Select>,
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="关键词搜索">
                {getFieldDecorator('keywords', {
                  initialValue: '',
                })(
                  <Input
                    placeholder="请输入运单号/联系电话"
                    onKeyDown={e => {
                      if (e.keyCode === 13) {
                        this.selectChange(
                          'searchInfo',
                          this.props.form.getFieldValue('keywords'),
                        );
                      }
                    }}
                  />,
                )}
              </FormItem>
              <FormItem className="clearfix">
                <Button
                  type="primary"
                  onClick={this.selectChange.bind('keywords')}
                  className="pull-right ml10 mr5"
                >
                  查 询
                </Button>
                <Button onClick={this.handleReset} className="pull-right">
                  重 置
                </Button>
              </FormItem>
            </Form>
          </div>
          <div
            style={{
              textAlign: 'center',
              borderTop: '1px solid #E4E7EF',
              padding: '10px 0',
            }}
          >
            有<span className="ml5 mr5">{this.props.waybillDataCount}</span>
            笔待调度运单
            <a
              href="javascript:;"
              className="ml10"
              onClick={this.selectChange.bind('keywords')}
            >
              <Icon type="sync" theme="outlined" />
            </a>
          </div>
          <div className={styles.blockBox}>
            <List
              style={{ overflow: 'auto', height: '100%' }}
              itemLayout="vertical"
              dataSource={this.state.listData}
              loading={this.props.waybillLoading}
              renderItem={(item, i) => (
                <List.Item key={item.tradeNumber}>
                  <div
                    className={[
                      styles.blockTag,
                      item.active ? styles.active : '',
                    ].join(' ')}
                    onClick={this.selectActive.bind(this, item, i)}
                  >
                    <Row className="mb5">
                      <Col span={18}>
                        <label className={styles.label}>
                          所需车型：
                          <span className={styles.span}>
                            {item.fromCarLong / 1000}米{item.fromCarStruct}
                          </span>
                        </label>
                      </Col>
                      <Col span={6}>
                        <span
                          style={{
                            color: '#67AE19',
                            border: '1px solid #67AE19',
                            borderRadius: 4,
                            padding: '3px 5px',
                          }}
                        >
                          {status[item.status]}
                        </span>
                        {/*<Tag color="#67AE19">{status[item.status]}</Tag>*/}
                      </Col>
                    </Row>
                    <Row className="mb5">
                      <Col span={24}>
                        <label className={styles.label}>
                          装卸点（个）：
                          <span className={styles.span}>
                            {item.wayPointNum}
                          </span>
                        </label>
                      </Col>
                    </Row>
                    <Row className="mb5">
                      <Col span={12}>
                        <label className={styles.label}>
                          重量（千克）：
                          <span className={styles.span}>
                            {item.goodsWeight}
                          </span>
                        </label>
                      </Col>
                      <Col span={12}>
                        <label className={styles.label}>
                          体积（方）：
                          <span className={styles.span}>
                            {item.goodsVolume}
                          </span>
                        </label>
                      </Col>
                    </Row>
                    <Row className="mb5">
                      <Col span={24}>
                        <label className={styles.label}>
                          货主电话：
                          <span className={styles.span}>
                            {item.customerMobilenumber}
                          </span>
                        </label>
                      </Col>
                    </Row>
                    {item.utmsTradeWayPoints.map((data, index) => {
                      if (index <= 1) {
                        return (
                          <Row className="mb5">
                            <Col span={3}>
                              {data.loadType === 1 ? (
                                <span
                                  style={{
                                    color: '#e63a00',
                                    border: '1px solid #e63a00',
                                    padding: '0 2px',
                                    borderRadius: 30,
                                  }}
                                >
                                  卸
                                </span>
                              ) : (
                                <span
                                  style={{
                                    color: '#7bb507',
                                    border: '1px solid #7bb507',
                                    padding: '0 2px',
                                    borderRadius: 30,
                                  }}
                                >
                                  装
                                </span>
                              )}
                            </Col>
                            <Col span={21}>
                              <label className={styles.label}>
                                <span className={styles.span}>
                                  {data.address}
                                </span>
                              </label>
                            </Col>
                          </Row>
                        );
                      }
                    })}
                    {item.utmsTradeWayPoints.length > 2 ? (
                      <div>
                        <div className={item.show ? styles.show : styles.hide}>
                          {item.utmsTradeWayPoints.map((data, index) => {
                            if (index >= 2) {
                              return (
                                <Row className="mb5">
                                  <Col span={3}>
                                    {data.loadType === 1 ? (
                                      <span
                                        style={{
                                          color: '#e63a00',
                                          border: '1px solid #e63a00',
                                          padding: '0 2px',
                                          borderRadius: 30,
                                        }}
                                      >
                                        卸
                                      </span>
                                    ) : (
                                      <span
                                        style={{
                                          color: '#7bb507',
                                          border: '1px solid #7bb507',
                                          padding: '0 2px',
                                          borderRadius: 30,
                                        }}
                                      >
                                        装
                                      </span>
                                    )}
                                  </Col>
                                  <Col span={21}>
                                    <label className={styles.label}>
                                      <span className={styles.span}>
                                        {data.address}
                                      </span>
                                    </label>
                                  </Col>
                                </Row>
                              );
                            }
                          })}
                        </div>
                        <div style={{ textAlign: 'center' }}>
                          <a
                            href="javascript:;"
                            onClick={this.toggleTopCollapse.bind(this, 2, i)}
                          >
                            展开
                            <Icon
                              type={item.show ? 'up' : 'down'}
                              theme="outlined"
                            />
                          </a>
                        </div>
                      </div>
                    ) : (
                      ''
                    )}
                    <Row>
                      <Col span={15}>
                        <label className={styles.label}>
                          {item.useCarDate}
                        </label>
                      </Col>
                      <Col span={8}>
                        <span style={{ color: '#000', fontSize: 14 }}>
                          {item.cost ? '￥' + item.cost.toFixed(2) : ''}
                        </span>
                      </Col>
                    </Row>
                  </div>
                </List.Item>
              )}
            />
          </div>
          <Pagination
            hideOnSinglePage={true}
            className="mt5"
            current={this.state.current}
            onChange={page => {
              this.setState(
                {
                  current: page,
                },
                this.handleSubmit,
              );
            }}
            total={this.props.waybillDataCount}
            pageSize={this.state.pageSize}
            size="small"
            showQuickJumper
            style={{ textAlign: 'left' }}
          />
        </Card>
      </div>
    );
  }
}
