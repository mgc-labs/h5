/**
 * TransportDispatch selectors
 * @author chengle
 * @date 2018-9-18 15:28:01
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

// 运力列表
export const makeSelectVehicleError = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('vehicleError'),
  );

export const makeSelectVehicleLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('vehicleLoading'),
  );

export const makeSelectVehicleData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('vehicleData'),
  );

export const makeSelectVehicleDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('vehicleDataCount'),
  );

// 调度列表
export const makeSelectWaybillError = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('waybillError'),
  );

export const makeSelectWaybillLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('waybillLoading'),
  );

export const makeSelectWaybillData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('waybillData'),
  );

export const makeSelectWaybillDataCount = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('waybillDataCount'),
  );
