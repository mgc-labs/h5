/**
 * TransportDispatch service
 * @author chengle
 * @date 2018-9-18 15:28:01
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
// import { USE_MOCK } from '../../CONFIG';
const USE_MOCK = false;

// API list
const API_WAYBILLLIST =
  '/ehuodiGateway/utmsTrade/utmsTrade/selectDiapacherList'; // 调度列表
const API_VEHICLELIST =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/selectDispatchUtmsDriverList'; // 车辆列表
const API_RESOURCEDISPATCH =
  '/ehuodiGateway/utmsDispatch/utmsDispatchcs/quickOrder'; // 派单和快捷派单、改派
const API_VEHICLEDETAILS =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/selectUtmsDriverDetailById'; // 司机详情

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_WAYBILLLIST)
    .reply(() =>
      import('./mock/waybilllist').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_VEHICLELIST)
    .reply(() =>
      import('./mock/vehiclelist').then(exports => [200, exports.default]),
    );
}

/**
 * 调度列表
 */
export const getWaybillList = ops => {
  return request(
    {
      method: 'post',
      url: API_WAYBILLLIST,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  );
};

/**
 * 车辆列表
 */
export const getVehicleList = ops => {
  return request(
    {
      method: 'post',
      url: API_VEHICLELIST,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  );
};

/**
 * 派单和快捷派单、改派
 */
export const setResourcedispatch = ops => {
  return request(
    {
      method: 'post',
      url: API_RESOURCEDISPATCH,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: false,
    },
  );
};

/**
 * 司机详情
 */
export const getVehicleDetails = ops => {
  return request(
    {
      method: 'post',
      url: API_VEHICLEDETAILS,
      data: qs.stringify(ops),
    },
    {
      useMock: USE_MOCK,
      globalErrorMsg: true,
    },
  );
};
