/**
 * The app state selectors
 */
import { Map, Seq } from 'immutable';
import { createSelector } from 'reselect';

const routeState = state => state.getIn(['global', 'route']);
const uiState = state => state.getIn(['global', 'ui']);
const accountState = state => state.getIn(['global', 'account']);
const authData = state => state.get('authorization');

export const makeSelectUiStatus = () => createSelector(uiState, state => state);

export const makeSelectRoutes = () =>
  createSelector(routeState, state => state.get('routeEntities'));

export const makeSelectValidRouteIds = () =>
  createSelector(routeState, state => {
    const routeIds = state.get('routeIds');
    const routes = state.get('routeEntities');
    return routeIds.filter(id => routes.has(id)).sortBy(
      id => {
        const route = routes.get(id);
        return (
          route.getIn(['routeConfig', 'defaultOpen']) &&
          route.get('hasPermission')
        );
      },
      (valueA, valueB) => {
        if (valueA) {
          return -1;
        } else if (valueB) {
          return 1;
        }
        return 0;
      },
    );
  });

export const makeSelectActiveRouteId = () =>
  createSelector(routeState, state => state.get('activeRouteId'));

export const makeSelectUserData = () =>
  createSelector(authData, state => state.get('userData', Map()));

export const makeSelectResourceTree = () =>
  createSelector(authData, state => state.get('resourceTree'));

export const makeSelectAccountInfo = () =>
  createSelector(authData, state => state.get('accountInfo'));

export const makeSelectAccount = () =>
  createSelector(accountState, state => state);

export const makeSelectResourceMap = () =>
  createSelector(authData, state => state.get('resourceByKey', Seq.Keyed()));
