/**
 * App Saga
 * @author ryan bian
 */
import message from 'antd/es/message';
import {
  LOCATION_CHANGE,
  LocationChangeAction,
  push,
} from 'connected-react-router';
import { isImmutable, List, Map } from 'immutable';
import Cookies from 'js-cookie';
import { concat, omit, uniq } from 'lodash';
import { Action } from 'redux-actions';
import { delay } from 'redux-saga';
import {
  all,
  call,
  put,
  race,
  select,
  take,
  takeEvery,
  takeLatest,
} from 'redux-saga/effects';
import actionTypePrefixer from 'utils/actionTypePrefixer';
import encrypt from 'utils/encrypt';
import { PersistActionTypes } from 'utils/persistence/actionTypes';
import { MAX_TABS_LIMIT } from '../../CONFIG';
import routerConfig, { IRouterConfig } from '../../routerConfig';
import {
  initRoute,
  insertRouteId,
  refreshRoutePending,
  removeRouteAllowed,
  requestChangePasswordDone,
  setActiveRoute,
} from './actions';
import { AppActionTypes } from './constants';
import DependenceManager from './dependenceManger';
import { changePassword, logout } from './service';

interface IRouteData {
  constants: Map<string, any>;
  invalid: boolean;
  hasPermission: boolean;
  permissions: List<any>;
  resourceList: Map<string, any>;
  routeConfig: IRouterConfig;
  routePath: string;
  title: string;
}

const IGNORE_ROUTE_KEY: string[] = [
  'component',
  'constants',
  'actionFactory',
  'reducerFactory',
  'sagaFactory',
  'modelFactory',
  'connectModels',
];
// 需要排除的 routeId
const ROUTE_ID_BLACKLIST: string[] = ['/', '/login', '/transportCapacity'];

const dependenceManager: DependenceManager = new DependenceManager();

function createRouteById(
  routeId: string,
  resourceByKey: Map<string, List<any>>,
  resourceList: Map<string, Map<string, any>>,
): IRouteData {
  const routePath: string = routeId.split('?').shift();
  const routeData = {
    invalid: false,
    hasPermission: false,
    routePath,
    title: routePath,
  };
  // 首先判断有没有路由配置
  if (routerConfig.has(routePath)) {
    // 如果有，需要注入 saga 和 reducer
    const routeConfig = routerConfig.get(routePath);
    // 这里兼容新老两种逻辑
    let reducer;
    let saga;
    if (routeConfig.modelFactory) {
      // 新的写法
      const modelInstance = routeConfig.modelFactory(routeId);
      reducer = modelInstance.reducer;
      saga = modelInstance.saga;
      if (!routeConfig.connectModels) {
        Object.assign(routeConfig, {
          connectModels: {},
        });
      }
      Object.assign(routeConfig.connectModels, {
        [routeId]: modelInstance.connectModel,
      });
      Object.assign(routeData, {
        routeConfig: Map(omit(routeConfig, IGNORE_ROUTE_KEY)),
      });
    } else {
      // 老的写法
      const CONSTANTS = actionTypePrefixer(routeId, routeConfig.constants);
      const actions = routeConfig.actionFactory(CONSTANTS, routeId);
      saga = routeConfig.sagaFactory(CONSTANTS, actions, routeId);
      reducer = routeConfig.reducerFactory(CONSTANTS, routeId);
      Object.assign(routeData, {
        constants: Map(CONSTANTS),
        routeConfig: Map(omit(routeConfig, IGNORE_ROUTE_KEY)),
      });
    }

    dependenceManager.inject(routeId, reducer, saga);
    // 继续判断菜单中是否有配置
    if (resourceByKey.has(routePath)) {
      const matchList = resourceByKey.get(routePath);
      const menuRoute = matchList
        // 这里把是否是按钮权限的判断去掉，兼容根据按钮权限来决定内页逻辑
        // .filter(d => d.get('resourceType') === 0)
        .first();
      const utmsResourceId = menuRoute.get('utmsResourceId');

      Object.assign(routeData, {
        hasPermission: true,
        resourceList,
        title: menuRoute ? menuRoute.get('resourceName') : routePath,
        permissions: resourceList.filter(
          item => item.get('parentResourceId') === utmsResourceId,
        ),
      });
    }
  } else {
    // 没有这个路由，则标记 invalid
    Object.assign(routeData, {
      invalid: true,
    });
  }
  return routeData as IRouteData;
}

function* deleteRoute(action: Action<string>) {
  const targetRouteId = action.payload;
  const routeIds = yield select((state: Map<string, any>) =>
    state.getIn(['global', 'route', 'routeIds']),
  );
  if (routeIds.has(targetRouteId)) {
    const activeRouteId = yield select((state: Map<string, any>) =>
      state.getIn(['global', 'route', 'activeRouteId']),
    );
    if (activeRouteId === targetRouteId) {
      // 当前处于选中状态 需要切换到前一个
      const routeIdsList = routeIds.toList();
      let nextActiveRouteId;
      const index = routeIdsList.indexOf(targetRouteId);
      if (index === 0) {
        nextActiveRouteId = routeIdsList.get(1);
      } else {
        nextActiveRouteId = routeIdsList.get(index - 1);
      }
      yield put(push(nextActiveRouteId));
    }
    yield put(removeRouteAllowed(targetRouteId));
    const needEjected = yield select((state: Map<string, any>) =>
      state.has(targetRouteId),
    );
    if (needEjected) {
      dependenceManager.eject(targetRouteId);
    }
  }
}

function* handleLocationChange(action: LocationChangeAction) {
  const location = action.payload.location;
  const { pathname, search } = location;
  let routeId: string;
  if (ROUTE_ID_BLACKLIST.includes(pathname)) {
    return;
  }
  if (routerConfig.has(pathname)) {
    // 判断有没有路由配置
    const localConfig = routerConfig.get(pathname);
    if (localConfig.allowMultipleInstance) {
      // 允许多开
      routeId = `${pathname}${search}`;
    } else {
      routeId = pathname;
    }
  } else {
    // 不存在配置的话，也给它生成一个
    routeId = pathname;
  }
  const route = yield select((state: Map<string, any>) =>
    state.getIn(['global', 'route']),
  );
  const routeIds = route.get('routeIds');
  if (routeIds.has(routeId)) {
    // 已经存在, 则激活它
    if (route.hasIn(['routeEntities', routeId])) {
      yield put(setActiveRoute(routeId));
    } else {
      yield call(initRouteEntity);
    }
  } else {
    // 不存在，加一个
    if (routeIds.size >= MAX_TABS_LIMIT) {
      // 没有新增成功的时候才需要提示
      message.warn('请先关闭多余窗口，再打开新窗口');
    } else {
      yield put(insertRouteId(routeId, true));
    }
  }
}

/**
 * 重新加载 route
 */
function* reloadRoute(action: Action<string>) {
  const routeId = action.payload;
  const hasRouteId = yield select((state: Map<string, any>) =>
    state.hasIn(['global', 'route', 'routeIds', routeId]),
  );
  if (!hasRouteId) {
    return;
  }
  const routePath = yield select((state: Map<string, any>) =>
    state.getIn(['global', 'route', 'routeEntities', routeId, 'routePath']),
  );
  if (routerConfig.has(routePath)) {
    const routeConfig = routerConfig.get(routePath);
    let reducer;
    let saga;
    if (routeConfig.modelFactory) {
      // 新的写法
      const modelInstance = routeConfig.modelFactory(routeId);
      reducer = modelInstance.reducer;
      saga = modelInstance.saga;
    } else {
      // 老的写法
      const CONSTANTS = actionTypePrefixer(routeId, routeConfig.constants);
      const actions = routeConfig.actionFactory(CONSTANTS, routeId);
      saga = routeConfig.sagaFactory(CONSTANTS, actions, routeId);
      reducer = routeConfig.reducerFactory(CONSTANTS, routeId);
    }

    // 1. 注销 reducer 和 saga
    dependenceManager.eject(routeId);
    // 2. 重新注入 reducer 和 saga
    dependenceManager.inject(routeId, reducer, saga);
  }
  // 3. 发送完成的消息
  yield put(refreshRoutePending(routeId));
}

function* initRouteEntity() {
  const route = yield select((state: Map<string, any>) =>
    state.getIn(['global', 'route']),
  );
  const resourceByKey = yield select((state: Map<string, any>) =>
    state.getIn(['authorization', 'resourceByKey']),
  );
  const resourceList = yield select((state: Map<string, any>) =>
    state.getIn(['authorization', 'resourceList']),
  );
  const routeEntities = route.get('routeEntities');
  const updateEntities = {};
  route.get('routeIds').forEach(routeId => {
    if (!routeEntities.has(routeId)) {
      // 没有 route entity, 需要创建一个
      const routeData = createRouteById(routeId, resourceByKey, resourceList);
      Object.assign(updateEntities, {
        [routeId]: routeData,
      });
    } else {
      // 如果已经有了，目前就不更新了，节省性能
    }
  });
  if (Object.keys(updateEntities).length) {
    // 如果有需要更新的 route entity
    yield put(initRoute(updateEntities));
  }
}

function* handleChangePassword(action: Action<object>) {
  const { payload } = action;
  try {
    const userData = yield select((state: Map<string, any>) =>
      state.getIn(['authorization', 'userData']),
    );
    const params = {
      operateType: 'UPDATE',
      utmsUsersId: userData.get('utmsUsersId'),
      userName: userData.get('userName'),
    };
    Object.keys(payload).forEach(key => {
      Object.assign(params, {
        [key]: encrypt(payload[key]),
      });
    });
    const result = yield call(changePassword, params);
    yield put(requestChangePasswordDone(result));
    message.success('密码修改成功!');
    const sessionKey = Cookies.get('sessionKey');
    yield delay(1000);
    yield logout(sessionKey);
    window.onbeforeunload = null;
    const domainName = yield select((state: Map<string, any>) =>
      state.getIn(['authorization', 'accountInfo', 'domainName']),
    );
    Cookies.remove('sessionKey', { path: '/' });
    Cookies.remove('userName', { path: `/${domainName}` });
    location.href = `/${domainName}/login`;
    // yield put(push('/login'));
  } catch (err) {
    yield put(requestChangePasswordDone(new Error(err.msg)));
  }
}

/**
 * 从 url 获取 routeId
 */
function* getRouteFromUrl() {
  const location = yield select((state: Map<string, any>) =>
    state.getIn(['router', 'location']),
  );
  let pathname;
  let search;
  if (isImmutable(location)) {
    pathname = location.get('pathname');
    search = location.get('search');
  } else {
    pathname = location.pathname;
    search = location.search;
  }
  let routeId: string;
  if (routerConfig.has(pathname)) {
    // 判断有没有路由配置
    const localConfig = routerConfig.get(pathname);
    if (localConfig.allowMultipleInstance) {
      // 允许多开
      routeId = `${pathname}${search}`;
    } else {
      routeId = pathname;
    }
  } else {
    // 不存在配置的话，也给它生成一个
    routeId = pathname;
  }
  return routeId ? [routeId] : [];
}
/**
 * 从 storage 获取 routeId
 */
function* getRouteFromStorage() {
  const { loadStateDone } = yield race({
    loadStateDone: take(PersistActionTypes.STATE_LOAD_SUCCESS),
    loadStateFail: take(PersistActionTypes.STATE_LOAD_FAILURE),
  });
  if (loadStateDone) {
    return loadStateDone.payload
      .getIn(['global', 'route', 'routeIds'], List())
      .toArray();
  }
  return [];
}
/**
 * 从 routeConfig 获取 routeId
 */
function* getRouteFromConfig() {
  const routeIds = [];
  const resourceByKey = yield select((state: Map<string, any>) =>
    state.getIn(['authorization', 'resourceByKey']),
  );
  for (const [path, config] of routerConfig) {
    if (resourceByKey.has(path) && config.defaultOpen) {
      // 这里不考虑默认开启且有search参数的页面
      // 即约定默认开启的页面的 path 和 routeId 一致
      routeIds.push(path);
    }
  }
  return routeIds;
}

/**
 * 启动 saga
 * 1.根据 url 加载标签
 * 2.读取本地储存的标签
 * 3.根据 routerConfig 加载默认打开的标签
 */
function* routeSaga() {
  // 读取本地储存的标签
  const domainName = yield select((state: Map<string, any>) =>
    state.getIn(['authorization', 'accountInfo', 'domainName']),
  );
  const userName = yield select((state: Map<string, any>) =>
    state.getIn(['authorization', 'userData', 'userName']),
  );
  yield put({
    type: PersistActionTypes.INIT,
    payload: {
      domainName,
      userName,
    },
  });

  yield takeEvery(AppActionTypes.REMOVE_ROUTE, deleteRoute);
  yield takeEvery(LOCATION_CHANGE, handleLocationChange);
  yield takeEvery(AppActionTypes.INSERT_ROUTE_ID, initRouteEntity);
  yield takeLatest(AppActionTypes.REFRESH_ROUTE, reloadRoute);
  yield takeLatest(
    AppActionTypes.REQUEST_CHANGE_PASSWORD,
    handleChangePassword,
  );

  const routeIdsMap = yield all({
    routeIdsFromUrl: call(getRouteFromUrl),
    routeIdsFromStorage: call(getRouteFromStorage),
    routeIdsFromConfig: call(getRouteFromConfig),
  });
  const routeIds = uniq(
    concat(
      routeIdsMap.routeIdsFromUrl,
      routeIdsMap.routeIdsFromStorage,
      routeIdsMap.routeIdsFromConfig,
    ),
  ).filter((id: string) => !ROUTE_ID_BLACKLIST.includes(id));
  let activeRouteId;
  if (
    routeIdsMap.routeIdsFromUrl[0] &&
    !ROUTE_ID_BLACKLIST.includes(routeIdsMap.routeIdsFromUrl[0])
  ) {
    activeRouteId = routeIdsMap.routeIdsFromUrl[0];
  } else {
    activeRouteId = routeIds[0];
  }
  yield put(insertRouteId(routeIds, false));
  yield put(setActiveRoute(activeRouteId));
}

export default function* rootSaga() {
  yield routeSaga();
}
