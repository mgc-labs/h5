/**
 * 框架头部
 * @author ryan bian
 */
// import Button from 'antd/es/button';
import Badge from 'antd/es/badge';
import Dropdown from 'antd/es/dropdown';
import Icon from 'antd/es/icon';
import Menu from 'antd/es/menu';
import classnames from 'classnames';
import LoadingIndicator from 'components/LoadingIndicator';
import Cookies from 'js-cookie';
// import moment from 'moment';
import * as React from 'react';
import { Link } from 'react-router-dom';
import defaultLogo from '../assets/logo.png';
import { logout } from '../service';

import styles from '../index.module.less';

const AccountExpireStatusMap = new Map([
  [0, 'error'],
  [1, 'warning'],
  [2, 'success'],
  [3, 'success'],
]);

const handleLogout = (basename: string) => {
  const sessionKey = Cookies.get('sessionKey');
  logout(sessionKey).then(() => {
    Cookies.remove('sessionKey', { path: '/' });
    Cookies.remove('userName', { path: basename });
    // history.replace('/login');
    location.href = `${basename}/login`;
  });
};

const Header = ({
  basename,
  // history,
  userData,
  accountInfo,
  showPasswordSetting /*onToggleSider*/,
  resourceMap,
}) => {
  const greeting = [
    `客户经理: ${accountInfo.get('clientDirectorUsername')}`,
    accountInfo.get('clientDirectorMobile'),
  ];
  const expiredDays = accountInfo.get('expiredDays');
  const accountMenu = (
    <Menu>
      <Menu.Item>
        <Link to="https://google.com">用户手册</Link>
      </Menu.Item>
      <Menu.Item>
        <a
          href="#"
          onClick={e => {
            e.preventDefault();
            showPasswordSetting();
          }}
        >
          修改密码
        </a>
      </Menu.Item>
      <Menu.Item>
        <Link to="/home/update">更新说明</Link>
      </Menu.Item>
      <Menu.Item>
        <a
          href="#"
          onClick={e => {
            e.preventDefault();
            window.onbeforeunload = null;
            handleLogout(basename);
          }}
        >
          退出
        </a>
      </Menu.Item>
    </Menu>
  );
  const logoUrl = accountInfo.get('indexLogoImg') || defaultLogo;
  const expireStatus = AccountExpireStatusMap.get(Math.floor(expiredDays / 10));
  return (
    <div className={styles.Header}>
      <h1
        className={styles.Header__BrandLogo}
        style={{ backgroundImage: `url('${logoUrl}')` }}
      />
      <div className={styles.Header__Greeting}>
        {userData.size > 0 ? (
          <React.Fragment>
            <span>{greeting.join(' ')}</span>
            {expiredDays <= 30 && (
              <span className={styles.Header__Outdate}>
                <Badge status={expireStatus} />
                账户
                <em
                  className={classnames({
                    [styles['Header__Outdate--Error']]:
                      expireStatus === 'error',
                    [styles['Header__Outdate--Warning']]:
                      expireStatus === 'warning',
                    [styles['Header__Outdate--Success']]:
                      expireStatus === 'success',
                  })}
                >
                  {expiredDays}
                </em>
                天后到期
              </span>
            )}
          </React.Fragment>
        ) : (
          <LoadingIndicator />
        )}
      </div>
      <div className={styles.Header__Jumper}>
        {resourceMap.get('/vehicleOrder/add') && (
          <Link
            className={classnames(
              'ant-btn',
              'ant-btn-background-ghost',
              styles.Header__InstanceEntrance,
            )}
            to="/vehicleOrder/add"
          >
            整车开单
          </Link>
        )}
        {resourceMap.get('/ltlOrder/add') && (
          <Link
            className={classnames(
              'ant-btn',
              'ant-btn-background-ghost',
              styles.Header__InstanceEntrance,
            )}
            to="/ltlOrder/add"
          >
            零担开单
          </Link>
        )}
      </div>
      <Dropdown overlay={accountMenu}>
        <div className={styles.Header__Account}>
          <Icon type="user" theme="outlined" style={{ fontSize: 24 }} />
          <span className={styles['Header__Account--name']}>
            {userData.get('realName')}
          </span>
          <Icon type="caret-down" theme="outlined" />
        </div>
      </Dropdown>
    </div>
  );
};

export default Header;
