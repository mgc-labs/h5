/**
 * App Shell Component
 * include header & menu
 * @author ryan bian
 */
import Icon from 'antd/es/icon';
import Layout from 'antd/es/layout';
import Modal from 'antd/es/modal';
import { push } from 'connected-react-router';
import { List, Map, Seq } from 'immutable';
import localForage from 'localforage';
import {
  LOGIN_RESPONSE_CODE,
  PASSWORD_SECURITY_NOTIFY,
} from 'modules/module.login/saga';
import * as React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as actions from '../actions';
import * as selectors from '../selectors';
import Header from './Header';
import ResetPassword from './ResetPassword';
import SiderNav from './SiderNav';

import styles from '../index.module.less';

interface IShellProps {
  accountData: Map<string, any>;
  accountInfo: Map<string, any>;
  activeRouteId: string;
  basename: string;
  routerTree: List<Map<string, any>>;
  routes: Map<string, any>;
  resourceMap: Seq.Keyed<string, List<any>>;
  requestChangePassword: (data: object) => void;
  setUrl: (url: string) => void;
  toggleNavCollapse: () => void;
  togglePasswordSetting: (show: boolean) => void;
  uiStatus: Map<string, any>;
  userData: Map<string, string | number>;
}

class Shell extends React.PureComponent<IShellProps> {
  public componentDidMount() {
    this.checkSecurityNotify();
  }
  public showPasswordSetting = () => {
    this.props.togglePasswordSetting(true);
  };
  public hidePasswordSetting = () => {
    this.props.togglePasswordSetting(false);
  };
  public handleClickMenu = ({ key }) => {
    this.props.setUrl(key);
  };
  public handleToggleSider = () => {
    this.props.toggleNavCollapse();
  };
  public async checkSecurityNotify() {
    try {
      const code = await localForage.getItem(PASSWORD_SECURITY_NOTIFY);
      let content;
      switch (code) {
        case LOGIN_RESPONSE_CODE.PASSWORD_TOO_SIMPLE:
          content = '当前密码为默认密码，安全度较低，请尽快修改！';
          break;
        case LOGIN_RESPONSE_CODE.PASSWORD_EXPIRED:
          content = '当前密码已使用6个月，为保证数据安全，建议修改密码！';
          break;
      }
      if (content) {
        Modal.confirm({
          title: '安全提示',
          content,
          okText: '立刻修改',
          cancelText: '以后再说',
          onOk: () => {
            this.showPasswordSetting();
          },
        });
        await localForage.removeItem(PASSWORD_SECURITY_NOTIFY);
      }
    } catch (err) {
      // no need to notify
    }
  }
  public render() {
    const {
      accountData,
      accountInfo,
      activeRouteId,
      basename,
      routerTree,
      routes,
      requestChangePassword,
      uiStatus,
      userData,
      children,
      resourceMap,
    } = this.props;
    const headerProps = {
      basename,
      userData,
      accountInfo,
      resourceMap,
      showPasswordSetting: this.showPasswordSetting,
    };
    return (
      <Layout className={styles.App}>
        <Header {...headerProps} />
        <Layout>
          <SiderNav
            collapsed={uiStatus.get('collapsed')}
            onClick={this.handleClickMenu}
            routeTree={routerTree}
            activeKey={routes.getIn([activeRouteId, 'routePath'])}
            version={accountInfo.get('versionNo')}
          >
            <Icon
              className={styles.App__SiderToggle}
              type={uiStatus.get('collapsed') ? 'menu-unfold' : 'menu-fold'}
              onClick={this.handleToggleSider}
              style={{ color: '#fff', fontSize: 16 }}
            />
          </SiderNav>
          {children}
        </Layout>
        <ResetPassword
          onSubmit={requestChangePassword}
          data={accountData}
          closePasswordModal={this.hidePasswordSetting}
        />
      </Layout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  accountData: selectors.makeSelectAccount(),
  accountInfo: selectors.makeSelectAccountInfo(),
  activeRouteId: selectors.makeSelectActiveRouteId(),
  routes: selectors.makeSelectRoutes(),
  routerTree: selectors.makeSelectResourceTree(),
  uiStatus: selectors.makeSelectUiStatus(),
  userData: selectors.makeSelectUserData(),
  resourceMap: selectors.makeSelectResourceMap(),
});
const mapDispatchToProps = dispatch => ({
  requestChangePassword: data => dispatch(actions.requestChangePassword(data)),
  setUrl: url => dispatch(push(url)),
  toggleNavCollapse: () => dispatch(actions.toggleNavCollapse()),
  togglePasswordSetting: show => dispatch(actions.togglePasswordSetting(show)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Shell);
