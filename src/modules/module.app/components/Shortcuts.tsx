/**
 * Keyboard Shortcuts Component
 * @author ryan bian
 */
import * as Mousetrap from 'mousetrap';
import * as React from 'react';

interface IShortcutsProps {
  config: Map<string, () => any>;
}

export default class Shortcuts extends React.PureComponent<IShortcutsProps> {
  public componentDidMount() {
    const { config } = this.props;
    config.forEach((fn, binding) => {
      Mousetrap.bind(binding, fn);
    });
  }
  public componentWillUnmount() {
    Mousetrap.reset();
  }
  public render() {
    return this.props.children;
  }
}
