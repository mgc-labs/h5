/**
 * Invalid Page Component
 * @author ryan bian
 */
import Button from 'antd/es/button';
import Icon from 'antd/es/icon';
import * as React from 'react';
import { Link } from 'react-router-dom';

import styles from '../index.module.less';

export default ({ close }) => (
  <div className={styles.InvalidPage}>
    <div className={styles.InvalidPage__Content}>
      <Icon type="frown" theme="outlined" /> 您访问的地址不存在
    </div>
    <Link to="/home" onClick={close}>
      <Button type="primary">返回首页</Button>
    </Link>
  </div>
);
