/**
 * 左侧菜单
 * @author ryan bian
 */
import Icon from 'antd/es/icon';
import Layout from 'antd/es/layout';
import Menu from 'antd/es/menu';
import classnames from 'classnames';
import { List, Map } from 'immutable';
import { throttle } from 'lodash';
import * as React from 'react';
import { animateScroll as scroll } from 'react-scroll';
import { install } from 'resize-observer';

import styles from '../index.module.less';

declare const ResizeObserver;

if (!(window as any).ResizeObserver) {
  install();
}

const { SubMenu, Item } = Menu;
const { Sider } = Layout;

interface ISiderNavProps {
  activeKey: string;
  collapsed: boolean;
  version: string;
  children: JSX.Element;
  onClick: (arg: any) => any;
  routeTree: List<Map<string, any>>;
}

interface ISiderNavState {
  showScrollController: boolean;
}

const SCROLL_EASING: string = 'linear';
const SCROLL_DURATION: number = 50;
const SCROLL_STEP: number = 30;
const SCROLL_CONFIG = {
  containerId: 'J_Menu',
  duration: SCROLL_DURATION,
  smooth: SCROLL_EASING,
};
const SCROLL_JUMP_CONFIG = {
  containerId: 'J_Menu',
  smooth: 'easeOutQuad',
};

export default class SiderNav extends React.PureComponent<
  ISiderNavProps,
  ISiderNavState
> {
  static defaultProps = {
    collapsed: false,
  };
  static scrollUp = throttle(() => {
    scroll.scrollMore(-SCROLL_STEP, SCROLL_CONFIG);
  }, SCROLL_DURATION);
  static scrollDown = throttle(() => {
    scroll.scrollMore(SCROLL_STEP, SCROLL_CONFIG);
  }, SCROLL_DURATION);
  static renderMenu(node, showIcon = false) {
    let ret;
    let icon;
    if (node.get('resourceIcon')) {
      icon = <Icon type={node.get('resourceIcon')} theme="outlined" />;
    }
    const validChildNodes = node
      .get('children', List())
      .filter(d => d.get('showInMenu'));

    if (validChildNodes.size === 0) {
      ret = (
        <Item key={node.get('resourceUrl')}>
          {icon}
          <span>{node.get('resourceName')}</span>
        </Item>
      );
    } else {
      ret = (
        <SubMenu
          key={node.get('utmsResourceId')}
          title={
            <React.Fragment>
              {icon}
              <span>{node.get('resourceName')}</span>
            </React.Fragment>
          }
        >
          {validChildNodes
            .map(subNode => SiderNav.renderMenu(subNode))
            .toArray()}
        </SubMenu>
      );
    }
    return ret;
  }
  public state = {
    showScrollController: false,
  };
  private timer;
  private ob;
  public componentDidMount() {
    const outerNode = document.querySelector('#J_Menu');
    const innerNode = document.querySelector('#J_MenuContainer');
    this.ob = new ResizeObserver(entries => {
      let outer;
      let inner;
      for (const entry of entries) {
        if (entry.target === innerNode) {
          inner = entry.contentRect;
        } else if (entry.target === outerNode) {
          outer = entry.contentRect;
        }
      }
      if (outer || inner) {
        this.calculateMenuHeight(
          outer || outerNode.getBoundingClientRect(),
          inner || innerNode.getBoundingClientRect(),
        );
      }
    });
    this.ob.observe(outerNode);
    this.ob.observe(innerNode);
  }
  public componentWillUnmount() {
    const outerNode = document.querySelector('#J_Menu');
    const innerNode = document.querySelector('#J_MenuContainer');
    if (this.ob) {
      outerNode && this.ob.disconnect(outerNode);
      innerNode && this.ob.disconnect(innerNode);
    }
  }
  /**
   * 计算是否需要 scroller 按钮
   */
  public calculateMenuHeight(outer, inner) {
    this.setState({
      showScrollController: inner.height > outer.height,
    });
  }
  public handleScrollUp = () => {
    this.timer = setInterval(() => {
      SiderNav.scrollUp();
    }, SCROLL_DURATION);
  };
  public handleScrollDown = () => {
    this.timer = setInterval(() => {
      SiderNav.scrollDown();
    }, SCROLL_DURATION);
  };
  public handleStopScroll = () => {
    clearInterval(this.timer);
  };
  public handleScrollToTop = () => {
    clearInterval(this.timer);
    scroll.scrollToTop(SCROLL_JUMP_CONFIG);
  };
  public handleScrollToBottom = () => {
    clearInterval(this.timer);
    scroll.scrollToBottom(SCROLL_JUMP_CONFIG);
  };
  public handleMouseScroll = e => {
    if (!this.state.showScrollController) {
      return;
    }
    if (e.deltaY > 0) {
      SiderNav.scrollUp.cancel();
      SiderNav.scrollDown();
    } else {
      SiderNav.scrollDown.cancel();
      SiderNav.scrollUp();
    }
  };
  public render() {
    const {
      activeKey,
      children,
      collapsed,
      routeTree,
      version,
      onClick,
    } = this.props;
    const menuProps = {
      inlineCollapsed: collapsed,
      mode: 'inline',
      onClick,
      selectedKeys: [activeKey],
      theme: 'dark',
      id: 'J_MenuContainer',
      inlineIndent: 12,
    };
    return (
      <Sider
        className={styles.App__Sider}
        collapsible
        collapsed={collapsed}
        width={150}
        trigger={null}
        collapsedWidth={43}
      >
        <div
          className={styles.App__Menu}
          id="J_Menu"
          onWheel={this.handleMouseScroll}
        >
          <Menu {...menuProps}>
            {routeTree.map(node => SiderNav.renderMenu(node, true)).toArray()}
          </Menu>
        </div>
        {children}
        <div
          className={classnames(styles.App__MenuController, {
            [styles['App__MenuController--visible']]: this.state
              .showScrollController,
          })}
        >
          <Icon
            type="up"
            className={styles['App__MenuController--up']}
            onMouseEnter={this.handleScrollUp}
            onMouseLeave={this.handleStopScroll}
            onClick={this.handleScrollToTop}
          />
          <Icon
            type="down"
            className={styles['App__MenuController--down']}
            onMouseEnter={this.handleScrollDown}
            onMouseLeave={this.handleStopScroll}
            onClick={this.handleScrollToBottom}
          />
        </div>
        <footer
          className={classnames(styles.App__SiderBottom, {
            [styles['App__SiderBottom--hide']]: collapsed,
          })}
        >
          <p>
            版本号：
            {version}
          </p>
          <p>Copyright ©️ 货嘀科技</p>
        </footer>
      </Sider>
    );
  }
}
