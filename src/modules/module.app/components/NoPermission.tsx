/**
 * NoPermission Component
 * @author ryan bian
 */
import * as React from 'react';

import defaultLogo from 'modules/module.login/img/logo.png';

import styles from '../index.module.less';

export default ({ logoImage = defaultLogo }) => (
  <div className={styles.NoPermission}>
    <div
      className={styles.NoPermission__Logo}
      style={{ backgroundImage: `url(${logoImage})` }}
    />
    <h5>温馨提示</h5>
    <h6>暂未配置任何权限，请联系当前公司管理员配置权限！</h6>
  </div>
);
