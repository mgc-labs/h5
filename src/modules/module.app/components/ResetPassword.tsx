/**
 * Reset Password Modal
 * @author ryan bian
 */
import Alert from 'antd/es/alert';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import PasswordStrength from 'components/PasswordStrength/';
import { Map } from 'immutable';
import * as React from 'react';

import styles from '../index.module.less';

const FormItem = Form.Item;
const formLayout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 12 },
  style: {
    display: 'flex',
    alignItems: 'center',
  },
};

interface IResetPasswordProps {
  form?: WrappedFormUtils;
  data: Map<string, any>;
  onSubmit: (data: object) => void;
  closePasswordModal: () => void;
}

@Form.create({})
class ResetPassword extends React.PureComponent<IResetPasswordProps> {
  public componentWillReceiveProps(nextProps) {
    if (nextProps.data.get('show') && !this.props.data.get('show')) {
      nextProps.form.resetFields();
    }
  }
  public handleChangeNewPassword = () => {
    const { isFieldTouched, validateFields } = this.props.form;
    if (isFieldTouched('confirmUserPassword')) {
      setTimeout(() => {
        validateFields(['confirmUserPassword'], {
          force: true,
        });
      }, 0);
    }
  };
  public handleSubmit = () => {
    const {
      onSubmit,
      form: { validateFields },
    } = this.props;
    validateFields((errors, values) => {
      if (!errors) {
        onSubmit(values);
      }
    });
  };
  public handleClose = () => {
    this.props.closePasswordModal();
  };
  public renderForm() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    return (
      <Form className={styles.ResetPassword__Form}>
        <FormItem {...formLayout} required label="原始密码">
          {getFieldDecorator('userPassword', {
            rules: [
              { required: true, message: '请输入原始密码' },
              { whitespace: true, message: '请勿使用空格' },
            ],
          })(<Input type="password" placeholder="请输入原始密码" />)}
        </FormItem>
        <FormItem {...formLayout} required label="新密码">
          {getFieldDecorator('newUserPassword', {
            rules: [
              { required: true, message: '请输入新密码' },
              { min: 6, message: '为了账户安全，密码最小为6位' },
              { max: 18, message: '密码最大长度不能超过18位' },
              { whitespace: true, message: '请勿使用空格' },
              {
                pattern: /^[a-zA-Z0-9,./\\@!]+$/,
                message: '请勿使用特殊符号作为密码',
              },
              {
                validator(rule, value, callback) {
                  const errors = [];
                  if (value === getFieldValue('userPassword')) {
                    errors.push(new Error('新旧密码不能相同'));
                  }
                  callback(errors);
                },
              },
            ],
          })(
            <Input
              type="password"
              placeholder="请输入新密码"
              onChange={this.handleChangeNewPassword}
            />,
          )}
        </FormItem>
        <FormItem {...formLayout} label="密码强度">
          <PasswordStrength
            value={getFieldValue('newUserPassword') || ''}
            style={{ width: '70%' }}
          />
        </FormItem>
        <FormItem {...formLayout} required label="确认新密码">
          {getFieldDecorator('confirmUserPassword', {
            rules: [
              { required: true, message: '请再次输入新密码' },
              {
                validator(rule, value, callback) {
                  const errors = [];
                  if (value !== getFieldValue('newUserPassword')) {
                    errors.push(new Error('两次密码输入不一致，请检查'));
                  }
                  callback(errors);
                },
              },
            ],
          })(<Input type="password" placeholder="请再次输入新密码" />)}
        </FormItem>
      </Form>
    );
  }
  public render() {
    const { data } = this.props;
    const modalProps = {
      title: '修改密码',
      visible: data.get('show'),
      width: 600,
      okText: '修改',
      onOk: this.handleSubmit,
      onCancel: this.handleClose,
      confirmLoading: data.get('pending'),
      maskClosable: false,
      centered: true,
    };
    return (
      <Modal {...modalProps}>
        <Alert
          message="警告：修改密码完成后会强制重新登录，请完成当前操作以免工作进度丢失。"
          type="warning"
          showIcon
        />
        {this.renderForm()}
      </Modal>
    );
  }
}

export default ResetPassword;
