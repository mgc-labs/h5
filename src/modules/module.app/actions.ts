/**
 * App Actions
 * @author ryan bian
 */
import { isError } from 'lodash';

import { AppActionTypes } from './constants';

export const initRoute = data => ({
  payload: data,
  type: AppActionTypes.INIT_ROUTE,
});
export const insertRouteId = (
  routeId: string | string[],
  active?: boolean,
) => ({
  payload: {
    active,
    routeId,
  },
  type: AppActionTypes.INSERT_ROUTE_ID,
});

export const refreshRoute = routeId => ({
  payload: routeId,
  type: AppActionTypes.REFRESH_ROUTE,
});

export const refreshRoutePending = routeId => ({
  payload: routeId,
  type: AppActionTypes.REFRESH_ROUTE_PENDING,
});
export const refreshRouteCompleted = routeId => ({
  payload: routeId,
  type: AppActionTypes.REFRESH_ROUTE_COMPLETED,
});

export const removeRoute = routeId => ({
  payload: routeId,
  type: AppActionTypes.REMOVE_ROUTE,
});
export const removeRouteAllowed = routeId => ({
  payload: routeId,
  type: AppActionTypes.REMOVE_ROUTE_ALLOWED,
});

export const setActiveRoute = routeId => ({
  payload: routeId,
  type: AppActionTypes.SET_ACTIVE_ROUTE,
});

export const toggleNavCollapse = () => ({
  type: AppActionTypes.TOGGLE_NAV_COLLAPSE,
});

export const togglePasswordSetting = show => ({
  type: AppActionTypes.TOGGLE_PASSWORD_SETTING,
  payload: show,
});

export const requestChangePassword = data => ({
  type: AppActionTypes.REQUEST_CHANGE_PASSWORD,
  payload: data,
});

export const requestChangePasswordDone = data => ({
  type: AppActionTypes.REQUEST_CHANGE_PASSWORD_DONE,
  payload: data,
  error: isError(data),
});
