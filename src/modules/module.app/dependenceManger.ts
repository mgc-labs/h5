/**
 * 管理模块的 saga 和 reducer 注入和卸载
 * @author ryan bian
 */
import getReducerInjectors from 'utils/reducerInjectors';
import getSagaInjectors from 'utils/sagaInjectors';

export default class DependenceManger {
  reducerInjectors;
  sagaInjectors;
  constructor() {
    this.createInjectors();
  }
  createInjectors() {
    if (window.store) {
      this.reducerInjectors = getReducerInjectors(window.store);
      this.sagaInjectors = getSagaInjectors(window.store);
    } else {
      setTimeout(() => {
        this.createInjectors();
      }, 10);
    }
  }
  inject(key, reducer, saga) {
    this.reducerInjectors.injectReducer(key, reducer);
    this.sagaInjectors.injectSaga(key, { saga });
  }
  eject(key) {
    this.reducerInjectors.ejectReducer(key);
    this.sagaInjectors.ejectSaga(key);
  }
}
