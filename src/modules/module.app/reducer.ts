/**
 * App Reducer
 * @author ryan bian
 */
import { Map, OrderedSet } from 'immutable';
import { combineReducers } from 'redux-immutable';

import { AppActionTypes } from './constants';

const initialState = Map({
  route: Map({
    activeRouteId: null,
    routeEntities: Map(),
    routeIds: OrderedSet(),
  }),
  ui: Map({
    collapsed: false,
    theme: 'default',
  }),
  account: Map({
    pending: false,
    show: false,
  }),
});

const routeReducer = (
  state: Map<string, any> = initialState.get('route'),
  action,
) => {
  const { payload, type } = action;
  switch (type) {
    case AppActionTypes.INIT_ROUTE:
      const updateEntities = payload;
      return state.withMutations(map => {
        Object.keys(updateEntities).forEach(id => {
          const entity = updateEntities[id];
          map.setIn(['routeEntities', id], Map(entity));
        });
      });
    case AppActionTypes.INSERT_ROUTE_ID:
      return state.withMutations(map => {
        map.update('routeIds', set => {
          if (Array.isArray(payload.routeId)) {
            return set.concat(payload.routeId);
          } else {
            return set.add(payload.routeId);
          }
        });
        if (payload.active) {
          map.set('activeRouteId', payload.routeId);
        }
      });
    case AppActionTypes.REFRESH_ROUTE_COMPLETED:
      return state.setIn(['routeEntities', payload, 'needRefresh'], false);
    case AppActionTypes.REFRESH_ROUTE_PENDING:
      return state.setIn(['routeEntities', payload, 'needRefresh'], true);
    case AppActionTypes.REMOVE_ROUTE_ALLOWED:
      const routeId = payload;
      if (!state.hasIn(['routeIds', routeId])) {
        return state;
      }
      return state
        .deleteIn(['routeIds', routeId])
        .deleteIn(['routeEntities', routeId]);
    case AppActionTypes.SET_ACTIVE_ROUTE:
      return state.set('activeRouteId', payload);
  }
  return state;
};

const uiReducer = (
  state: Map<string, any> = initialState.get('ui'),
  action,
) => {
  const { type } = action;
  switch (type) {
    case AppActionTypes.TOGGLE_NAV_COLLAPSE:
      return state.set('collapsed', !state.get('collapsed'));
  }
  return state;
};

const accountReducer = (
  state: Map<string, any> = initialState.get('account'),
  action,
) => {
  const { type } = action;
  switch (type) {
    case AppActionTypes.TOGGLE_PASSWORD_SETTING:
      return state.set('show', action.payload);
    case AppActionTypes.REQUEST_CHANGE_PASSWORD:
      return state.set('pending', true);
    case AppActionTypes.REQUEST_CHANGE_PASSWORD_DONE:
      return state.set('pending', false).set('show', action.error);
  }
  return state;
};

export default combineReducers({
  route: routeReducer,
  ui: uiReducer,
  account: accountReducer,
});
