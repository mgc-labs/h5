/**
 * App Service
 * @author ryan bian
 */
import qs from 'qs';
import request from 'utils/request';

const API_UPDATE_PASSWORD =
  '/ehuodiGateway/utmsCore/utmsUserscs/updateUtmsUsersPwd';
const API_LOGOUT = '/ehuodiGateway/utmsCore/utmsBaseconfigcs/logout';

export const logout = sessionKey =>
  request({
    method: 'post',
    url: API_LOGOUT,
    data: qs.stringify({
      sessionKey,
    }),
  });

export const changePassword = params =>
  request({
    method: 'post',
    url: API_UPDATE_PASSWORD,
    data: params,
  });
