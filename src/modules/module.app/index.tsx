/**
 * App Component
 */
import Dropdown from 'antd/es/dropdown';
import Icon from 'antd/es/icon';
import Layout from 'antd/es/layout';
import Menu from 'antd/es/menu';
import Tabs from 'antd/es/tabs';
import classnames from 'classnames';
import { push } from 'connected-react-router';
import { Map, OrderedSet } from 'immutable';
import msngr from 'msngr';
import ScrollableTabBar from 'rc-tabs/lib/ScrollableTabBar';
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import EventEmitter from 'utils/eventemitter';
import injectSaga from 'utils/injectSaga';
import routerConfig from '../../routerConfig';
import {
  refreshRoute,
  refreshRouteCompleted,
  removeRoute,
  setActiveRoute,
} from './actions';
import InvalidPage from './components/InvalidPage';
import NoPermission from './components/NoPermission';
import Shell from './components/Shell';
import Shortcuts from './components/Shortcuts';
import rootSaga from './saga';
import {
  makeSelectAccountInfo,
  makeSelectActiveRouteId,
  makeSelectRoutes,
  makeSelectValidRouteIds,
} from './selectors';

const { TabPane } = Tabs;

import './index.less';
import styles from './index.module.less';

interface IAppProps {
  basename: string;
  activeRouteId: string;
  routeIds: OrderedSet<any>;
  routes: Map<string, any>;
  accountInfo: Map<string, any>;
  refreshRouteCompleted: (routeId: string) => void;
  refreshRoute: (routeId: string) => void;
  removeRoute: (routeId: string) => void;
  setActiveRoute: (routeId: string) => void;
  setUrl: (url: string) => void;
  children: React.ReactChildren | React.ReactChild;
}

interface IContextMenuPosition {
  left: number;
  top: number;
}

interface IAppState {
  contextMenuPosition: IContextMenuPosition;
  contextMenuVisible: boolean;
  contextMenuTriggerId: string;
}

export enum EvnetSourceTypes {
  FROM_TAB_CLOSE = 'FROM_TAB_CLOSE',
  FROM_TAB_REFRESH = 'FROM_TAB_REFRESH',
  FROM_PROPS_CLOSE = 'FROM_PROPS_CLOSE',
  FROM_PROPS_REFRESH = 'FROM_PROPS_REFRESH',
}

class App extends React.PureComponent<IAppProps, IAppState> {
  static renderTabBar(defaultTabBarProps) {
    return <ScrollableTabBar {...defaultTabBarProps} prefixCls="tms-tab" />;
  }
  static createComponent(id, entity, context) {
    const routePath = entity.get('routePath');
    const { actionFactory, connectModels, component } = routerConfig.get(
      routePath,
    );
    let Component;
    if (connectModels) {
      Component = component({
        connectModel: connectModels[id],
        routeId: id,
      });
    } else {
      const CONSTANTS = entity.get('constants').toObject();
      const actions = actionFactory(CONSTANTS, id);
      Component = component({
        actions,
        routeId: id,
      });
    }
    const searchParams = {};
    if (id.includes('?')) {
      const params = new URLSearchParams(id.split('?').pop());
      for (const key of params.keys()) {
        Object.assign(searchParams, {
          [key]: params.get(key),
        });
      }
    }
    const closeHandler = () => {
      context.onClosePage(id, EvnetSourceTypes.FROM_PROPS_CLOSE);
    };
    const refreshHandleer = (routeId?: string) => {
      context.onClosePage(
        routeId ? routeId : id,
        EvnetSourceTypes.FROM_PROPS_REFRESH,
      );
    };
    context.emitters[id] = new EventEmitter();
    return (
      <Component
        routeId={id}
        searchParams={searchParams}
        close={closeHandler}
        eventEmitter={context.emitters[id]}
        permissions={entity.get('permissions')}
        refresh={refreshHandleer}
      />
    );
  }
  public state = {
    contextMenuPosition: {
      left: 0,
      top: 0,
    },
    contextMenuVisible: false,
    contextMenuTriggerId: null,
  };
  private routeComponents: object = {};
  private emitters: object = {};
  private contextMenuNode: React.RefObject<HTMLDivElement> = React.createRef();
  private contentNode: React.RefObject<HTMLDivElement> = React.createRef();
  private tabBarNode: HTMLDivElement;
  private shortcutsConfig;
  public componentWillMount() {
    // setup shortcut config
    this.shortcutsConfig = new (window as any).Map([
      [
        'option+w',
        () => {
          this.onClosePage(
            this.props.activeRouteId,
            EvnetSourceTypes.FROM_TAB_CLOSE,
          );
        },
      ],
    ]);
  }
  public componentWillReceiveProps(nextProps) {
    if (!nextProps.routes.equals(this.props.routes)) {
      this.updateRouteComponents(nextProps.routes);
    }
    if (
      nextProps.activeRouteId &&
      nextProps.activeRouteId !== this.props.activeRouteId
    ) {
      // 发布 切换标签 的消息
      msngr('GLOBAL', 'activeRouteId', 'change').emit(nextProps.activeRouteId);
    }
  }
  public componentDidMount() {
    this.updateRouteComponents(this.props.routes);
    window.onbeforeunload = this.handleQuit;
    this.tabBarNode = this.contentNode.current.querySelector(
      '.tms-tab-nav-wrap',
    );
    this.tabBarNode.addEventListener(
      'contextmenu',
      this.handleTabContextMenu,
      false,
    );
    this.contextMenuNode.current.addEventListener(
      'blur',
      this.handleHideContextMenu,
      false,
    );
  }
  public componentDidUpdate(prevProps, prevState) {
    if (!prevState.contextMenuVisible && this.state.contextMenuVisible) {
      // context 出现的时候 ，需要 手动 focus 一下
      this.contextMenuNode.current.focus();
    }
  }
  public componentWillUnmount() {
    this.tabBarNode.removeEventListener(
      'contextmenu',
      this.handleTabContextMenu,
      false,
    );
    this.contextMenuNode.current.removeEventListener(
      'blur',
      this.handleHideContextMenu,
      false,
    );
    window.onbeforeunload = null;
  }
  public updateRouteComponents(routes) {
    const logoImage = this.props.accountInfo.get('loginLogoImg');
    routes.forEach((entity, id) => {
      if (!this.routeComponents[id]) {
        if (entity.get('invalid')) {
          // 无效的标签
          this.routeComponents[id] = (
            <InvalidPage
              close={() => {
                this.onClosePage(id, EvnetSourceTypes.FROM_TAB_CLOSE);
              }}
            />
          );
        } else if (!entity.get('hasPermission')) {
          this.routeComponents[id] = <NoPermission logoImage={logoImage} />;
        } else {
          const component = App.createComponent(id, entity, this);
          Object.assign(this.routeComponents, {
            [id]: component,
          });
        }
      } else {
        if (entity.get('needRefresh')) {
          if (!entity.get('invalid') && entity.get('hasPermission')) {
            // 重新加载这个 components
            delete this.routeComponents[id];
            this.routeComponents[id] = App.createComponent(id, entity, this);
          }
          this.props.refreshRouteCompleted(id);
        }
      }
    });
  }
  public handleQuit() {
    return '确定要退出吗？';
  }
  public handleTabContextMenu = e => {
    e.preventDefault();
    let clickInTab: boolean = false;
    let id: string;
    if (Array.isArray(e.path)) {
      let node;
      for (let i = 0, len = e.path.length; i < len; i += 1) {
        node = e.path[i];
        if (node === this.tabBarNode) {
          break;
        }
        if (node.classList.contains('tms-tab-tab')) {
          clickInTab = true;
          id = node.querySelector('.J_TabItem').dataset.id;
          break;
        }
      }
    } else {
      let node = e.target;
      while (this.tabBarNode.contains(node)) {
        if (node.classList.contains('tms-tab-tab')) {
          clickInTab = true;
          id = node.querySelector('.J_TabItem').dataset.id;
          break;
        }
        node = node.parentNode;
      }
    }
    if (clickInTab) {
      this.setState({
        contextMenuPosition: {
          left: e.clientX,
          top: e.clientY,
        },
        contextMenuVisible: clickInTab,
        contextMenuTriggerId: id,
      });
    }
  };
  public handleHideContextMenu = e => {
    this.setState({
      contextMenuVisible: false,
      contextMenuTriggerId: null,
    });
  };
  public handleChangeRoute = routeId => {
    this.props.setUrl(routeId);
  };
  public handleEditRoute = (routeId, action) => {
    if (action === 'remove') {
      this.onClosePage(routeId, EvnetSourceTypes.FROM_TAB_CLOSE);
    }
  };
  public handleRefreshRoute = e => {
    const target = e.currentTarget;
    const routeId = target.dataset.id;
    this.onClosePage(routeId, EvnetSourceTypes.FROM_TAB_REFRESH);
  };
  public handleTabControlEvent = ({ key }) => {
    const { routeIds, routes, activeRouteId } = this.props;
    const { contextMenuVisible, contextMenuTriggerId } = this.state;
    let closeId;
    if (contextMenuVisible) {
      closeId = contextMenuTriggerId;
    } else {
      closeId = activeRouteId;
    }
    const routeIdsAry = routeIds
      .filter(id => routes.getIn([id, 'routeConfig', 'closable']))
      .toArray();
    this.setState({
      contextMenuVisible: false,
      contextMenuTriggerId: null,
    });
    switch (key) {
      case 'CLOSE_ALL':
        routeIdsAry.reduce(
          (p, id) =>
            p.then(() => this.onClosePage(id, EvnetSourceTypes.FROM_TAB_CLOSE)),
          Promise.resolve(),
        );
        break;
      case 'CLOSE_OTHERS':
        routeIdsAry.reduce(
          (p, id) =>
            p.then(() => {
              if (id !== closeId) {
                return this.onClosePage(id, EvnetSourceTypes.FROM_TAB_CLOSE);
              }
              return Promise.resolve();
            }),
          Promise.resolve(),
        );
        break;
    }
  };
  public onClosePage(id, eventSource: EvnetSourceTypes) {
    let handler;
    if (eventSource.endsWith('_CLOSE')) {
      // close event
      const { routes } = this.props;
      const closable = routes.getIn([id, 'routeConfig', 'closable'], false);
      if (!closable) {
        return Promise.reject(
          `current page: ${id} does not allowed to be closed!`,
        );
      }
      handler = (routeId: string) => this.closePage(routeId);
    } else {
      // refresh event
      handler = (routeId: string) => this.props.refreshRoute(routeId);
    }

    if (this.emitters[id] && this.emitters[id].has('close')) {
      return new Promise(resolve => {
        this.emitters[id].emit(
          'close',
          () => {
            // this.closePage(id);
            handler(id);
            resolve(id);
          },
          eventSource,
        );
      });
    } else {
      // this.closePage(id);
      handler(id);
      return Promise.resolve(id);
    }
  }
  public renderTabControlMenu(otherProps: object = {}) {
    return (
      <Menu
        prefixCls={'ant-dropdown-menu'}
        onClick={this.handleTabControlEvent}
        {...otherProps}
      >
        <Menu.Item key="CLOSE_ALL">关闭全部标签页</Menu.Item>
        <Menu.Item key="CLOSE_OTHERS">关闭其它标签页</Menu.Item>
      </Menu>
    );
  }
  public renderContent() {
    const { activeRouteId, routeIds, routes } = this.props;
    const { contextMenuPosition, contextMenuVisible } = this.state;
    const tabProps = {
      activeKey: activeRouteId,
      animated: false,
      hideAdd: true,
      onChange: this.handleChangeRoute,
      onEdit: this.handleEditRoute,
      type: 'editable-card',
      renderTabBar: App.renderTabBar,
      tabBarExtraContent: (
        <Dropdown
          trigger={['click']}
          overlay={this.renderTabControlMenu()}
          placement="bottomRight"
        >
          <button className={styles.App__TabControl}>
            <Icon type="ellipsis" style={{ fontSize: 16 }} />
          </button>
        </Dropdown>
      ),
    };
    return (
      <Layout className={styles.App__Content}>
        <div id="J_Container" ref={this.contentNode}>
          <Tabs {...tabProps}>
            {routeIds
              .map(routeId => {
                const route = routes.get(routeId);
                return (
                  <TabPane
                    tab={
                      <div className={'J_TabItem'} data-id={routeId}>
                        <Icon
                          type="sync"
                          data-id={routeId}
                          onClick={this.handleRefreshRoute}
                        />
                        {route.get('title')}
                      </div>
                    }
                    key={routeId}
                    closable={
                      route.getIn(['routeConfig', 'closable'], true) ||
                      !route.get('hasPermission')
                    }
                  >
                    <div className={styles.Page__Container}>
                      {this.routeComponents[routeId]}
                    </div>
                  </TabPane>
                );
              })
              .toArray()}
          </Tabs>
          <div
            className={classnames('ant-dropdown', styles.App__TabContextMenu)}
            style={Object.assign({}, contextMenuPosition, {
              display: contextMenuVisible ? '' : 'none',
            })}
            tabIndex={-1}
            ref={this.contextMenuNode}
          >
            {this.renderTabControlMenu({
              mode: 'vertical',
              selectable: false,
            })}
          </div>
        </div>
      </Layout>
    );
  }
  public render() {
    return (
      <Shell basename={this.props.basename}>
        <Shortcuts config={this.shortcutsConfig}>
          {this.renderContent()}
        </Shortcuts>
      </Shell>
    );
  }
  private closePage(id) {
    if (this.routeComponents[id]) {
      delete this.routeComponents[id];
    }
    if (this.emitters[id]) {
      delete this.emitters[id];
    }
    this.props.removeRoute(id);
  }
}

const mapStateToProps = createStructuredSelector({
  activeRouteId: makeSelectActiveRouteId(),
  routeIds: makeSelectValidRouteIds(),
  routes: makeSelectRoutes(),
  accountInfo: makeSelectAccountInfo(),
});

const mapDispatchToProps = dispatch => ({
  refreshRoute: routeId => dispatch(refreshRoute(routeId)),
  refreshRouteCompleted: routeId => dispatch(refreshRouteCompleted(routeId)),
  removeRoute: routeId => dispatch(removeRoute(routeId)),
  setActiveRoute: routeId => dispatch(setActiveRoute(routeId)),
  setUrl: url => dispatch(push(url)),
});

const withSaga = injectSaga({
  key: 'global',
  saga: rootSaga,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withRouter,
  withConnect,
  withSaga,
)(App);
