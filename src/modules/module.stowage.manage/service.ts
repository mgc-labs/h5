import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_URL = {
  PAERGROUPLIST: '/ehuodiGateway/utmsTrade/utmsPartGroup/partGroupList',
  SENDPARTORDER: '/ehuodiGateway/utmsTrade/utmsPartGroup/sendPartOrder',
  DELGROUP: '/ehuodiGateway/utmsTrade/utmsPartGroup/deleteGroup',
};

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  // mocker
  //   .on('post', API_URL.PAERGROUPLIST)
  //   .reply(() =>
  //     import('./mock/partGroupList').then(exports => [200, exports.default]),
  //   );

  mocker
    .on('post', API_URL.SENDPARTORDER)
    .reply(() =>
      import('./mock/sendPartOrder').then(exports => [200, exports.default]),
    );

  // mocker
  //   .on('post', API_URL.DELGROUP)
  //   .reply(() =>
  //     import('./mock/delGroup').then(exports => [200, exports.default]),
  //   );
}

/**
 * 列表
 */
export const reqPartGroupList = params =>
  request(
    {
      method: 'post',
      url: API_URL.PAERGROUPLIST,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 发货
 */
export const reqSendPartOrder = params =>
  request(
    {
      method: 'post',
      url: API_URL.SENDPARTORDER,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

/**
 * 撤销配载
 */
export const reqDelGroup = params =>
  request(
    {
      method: 'post',
      url: API_URL.DELGROUP,
      data: params,
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);
