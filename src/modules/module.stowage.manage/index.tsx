/**
 * StowageManage Component
 * @author hefan
 * @date 2018/9/11 上午9:05:16
 */
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { toYYYYMMDDE, toYYYYMMDDS } from 'utils/dateTimeHelper';

import {
  makeSelectData,
  makeSelectDataCount,
  makeSelectDelState,
  makeSelectError,
  makeSelectLoading,
  makeSelectShipState,
} from './selectors';

import styles from './index.module.less';

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import DatePicker from 'antd/es/date-picker';
import Form from 'antd/es/form';
import FormItem from 'antd/es/form/FormItem';
import Input from 'antd/es/input';
import Popconfirm from 'antd/es/popconfirm';
import Select from 'antd/es/select';
import Spin from 'antd/es/spin';
import Download from 'components/Download';
import MyTable from 'components/MyTable';
import {
  PageBottom,
  PageBottomButtons,
  PageBottomCheck,
} from 'components/PageBottom';
const RangePicker = DatePicker.RangePicker;
const Option = Select.Option;

const columns = [
  {
    title: '配载单ID',
    dataIndex: 'utmsPartGroupId',
    render: text => (
      <Link to={`/stowageManage/stowageDetail?partGroupId=${text}`}>
        {text}
      </Link>
    ),
  },
  {
    title: '配载单名称',
    dataIndex: 'name',
  },
  {
    title: '客户名称',
    dataIndex: 'customerName',
  },
  {
    title: '生成时间',
    dataIndex: 'gmtCreate',
  },
  {
    title: '收货点数',
    dataIndex: 'orderCount',
  },
  {
    title: ' 总重量(千克)',
    dataIndex: 'weight',
  },
  {
    title: '总体积(方)',
    dataIndex: 'volume',
  },
  {
    title: '状态',
    dataIndex: 'status',
  },
];

export interface IStowageManageProps {
  children: React.ReactChildren;
  getList: (params: {}) => void;
  dataCount: number;
  putShipGoods: (params: {}) => void;
  delGroup: (params: {}) => void;
  toDetail: (param: number) => void;
  shipOk: boolean;
  delOk: boolean;
}

class StowageManage extends React.PureComponent<IStowageManageProps> {
  state = {
    selectedRowKeys: [],
    skipCount: 0,
    pageSize: 15,
    current: 1,
  };
  public componentDidMount() {
    this.fetchDataFun();
  }
  public UNSAFE_componentWillReceiveProps() {
    // 重置选择项
    if (this.props.shipOk || this.props.delOk) {
      this.onSelectChange([]);
    }
  }
  /**
   * getFields()
   */
  public getFields() {
    const { getFieldDecorator, getFieldValue } = this.props.form;

    return (
      <Form layout="inline">
        <FormItem label="订单分组名称">
          {getFieldDecorator('name')(
            <Input placeholder="请输入订单分组名称" />,
          )}
        </FormItem>

        <FormItem label="状态">
          {getFieldDecorator('status', {
            initialValue: '',
          })(
            <Select style={{ width: '90px' }}>
              <Option value="">全部</Option>
              <Option value={0}>待发货</Option>
              <Option value={1}>已发货</Option>
            </Select>,
          )}
        </FormItem>

        <FormItem label="下单时间">
          {getFieldDecorator('createDate')(
            <RangePicker style={{ width: 215 }} />,
          )}
        </FormItem>

        <FormItem>
          <Button
            onClick={() => {
              this.searchHandle();
            }}
            htmlType="submit"
            type="primary"
          >
            查询
          </Button>
        </FormItem>

        <FormItem>
          <Button onClick={this.reSetHandle}>重置</Button>
        </FormItem>
      </Form>
    );
  }
  /**
   * getTableList
   */
  public getTableList() {
    const { data, dataCount, loading } = this.props;
    const { selectedRowKeys, current } = this.state;

    const dataSource = data
      ? data
          .map(d => ({
            key: d.get('utmsPartGroupId'),
            name: d.get('name'),
            utmsPartGroupId: d.get('utmsPartGroupId'),
            customerName: d.get('customerName'),
            gmtCreate: d.get('gmtCreate'),
            orderCount: d.get('orderCount'),
            weight: d.get('weight'),
            volume: d.get('volume'),
            status: d.get('status'),
          }))
          .toArray()
      : [];

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      getCheckboxProps: record => ({
        disabled: record.status === '已发货', // Column configuration not to be checked
        status: record.status,
      }),
    };

    const paginationConfig = {
      current,
      total: dataCount,
      onChange: this.paginationChange,
      onShowSizeChange: this.paginationChange,
    };

    return (
      <div style={{ marginTop: '20px' }}>
        <Spin spinning={loading}>
          <MyTable
            rowSelection={rowSelection}
            columns={columns}
            dataSource={dataSource}
            pagination={paginationConfig}
          />
        </Spin>

        <PageBottom
          leftChild={
            <PageBottomCheck
              total={this.allowSelect()}
              checked={selectedRowKeys.length}
              onAllCheckChanged={this.onAllCheckChanged}
            />
          }
          rightChild={
            <PageBottomButtons
              buttons={[
                <Popconfirm
                  title="是否确认？"
                  okText="确认"
                  cancelText="取消"
                  key="3"
                  onConfirm={this.delGroupHandle}
                >
                  <Button
                    key={1}
                    type="primary"
                    disabled={!selectedRowKeys.length || loading}
                  >
                    撤销配载
                  </Button>
                </Popconfirm>,
                <Button
                  key={2}
                  type="primary"
                  disabled={!selectedRowKeys.length || loading}
                  onClick={this.putSHipGoodsHandle}
                >
                  发货
                </Button>,
              ]}
            />
          }
        />
      </div>
    );
  }
  public render() {
    const { getFieldValue } = this.props.form;
    return (
      <div>
        <div className={styles.StowageManageTitle}>
          <div className={styles.StowageManageTitleTxt}>配载管理</div>
          <div className="btn">
            <Download
              url={'/ehuodiGateway/utmsTrade/utmsPartGroup/exportPartGroupList'}
              params={{
                name: getFieldValue('name'),
                status: getFieldValue('status'),
                createDateStart: getFieldValue('createDate')
                  ? toYYYYMMDDS(getFieldValue('createDate')[0])
                  : undefined,
                careteDateEnd: getFieldValue('createDate')
                  ? toYYYYMMDDE(getFieldValue('createDate')[1])
                  : undefined,
              }}
            >
              导出
            </Download>
          </div>
        </div>

        <Card>
          {this.getFields()}
          {this.getTableList()}
        </Card>
      </div>
    );
  }

  /**
   * 页码变化
   */
  private paginationChange = params => {
    const { skipCount, pageSize, current } = params;
    this.setState({
      skipCount,
      pageSize,
      current,
    });

    this.onSelectChange([]);

    this.fetchDataFun({
      skipCount,
      pageSize,
    });
  };

  /**
   * 可选项
   */
  private allowSelect(): number {
    if (this.props.data) {
      const data = this.props.data.toJS() || [];

      const dataSource = data.filter(d => d.status === '待发货');

      return dataSource.length;
    }
    return 0;
  }

  /**
   * 设置选择项
   */
  private onSelectChange = (selectedRowKeys: []) => {
    this.setState({ selectedRowKeys });
  };

  /**
   * 全选
   */
  private onAllCheckChanged = checked => {
    const data = this.props.data.toJS();

    if (!checked) {
      this.setState({ selectedRowKeys: [] });
    } else {
      const selectedRowKeys = [];
      data.map(d => {
        if (d.status === '待发货') {
          selectedRowKeys.push(d.utmsPartGroupId);
        }
      });

      this.setState({ selectedRowKeys });
    }
  };

  /**
   * 发货
   */
  private putSHipGoodsHandle = () => {
    const { selectedRowKeys, pageSize, skipCount } = this.state;

    const values = this.props.form.getFieldsValue();
    const { createDate, ...options } = values;

    const createDateStart = createDate ? toYYYYMMDDS(createDate[0]) : undefined;
    const createDateEnd = createDate ? toYYYYMMDDE(createDate[1]) : undefined;

    this.props.putShipGoods({
      partGroupIds: selectedRowKeys,
      createDateStart,
      createDateEnd,
      ...options,
      pageSize,
      skipCount,
    });
  };

  /**
   * 取消配载/撤销配置/删除分组
   */
  private delGroupHandle = () => {
    const { selectedRowKeys, pageSize, skipCount } = this.state;

    const formData = this.props.form.getFieldsValue();

    const { createDate, ...options } = formData;

    const createDateStart = createDate ? toYYYYMMDDS(createDate[0]) : undefined;
    const createDateEnd = createDate ? toYYYYMMDDE(createDate[1]) : undefined;

    this.props.delGroup({
      partGroupIds: selectedRowKeys,
      createDateStart,
      createDateEnd,
      ...options,
      pageSize,
      skipCount,
    });
  };

  /**
   * 查询
   */
  private searchHandle = () => {
    this.onSelectChange([]);

    this.setState({
      formValues: {},
      skipCount: 0,
      current: 1,
    });

    this.fetchDataFun();
  };

  private fetchDataFun(params?: {}) {
    const formData = this.props.form.getFieldsValue();
    const { createDate, ...options } = formData;
    const { pageSize } = this.state;

    const createDateStart = createDate ? toYYYYMMDDS(createDate[0]) : undefined;
    const createDateEnd = createDate ? toYYYYMMDDE(createDate[1]) : undefined;

    this.props.getList({
      ...options,
      createDateStart,
      createDateEnd,
      pageSize,
      skipCount: 0,
      ...params,
    });
  }

  /**
   * 重置
   */
  private reSetHandle = () => {
    const { form } = this.props;
    form.resetFields();

    this.searchHandle();
  };
}

const StowageManageWrap = Form.create()(StowageManage);

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getList: params => dispatch(actions.getAsyncData(params)),
    putShipGoods: params => dispatch(actions.putShipGoods(params)),
    delGroup: params => dispatch(actions.delGroup(params)),
    toDetail: param => dispatch(actions.toDetail(param)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    data: makeSelectData(selectState),
    dataCount: makeSelectDataCount(selectState),
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    shipOk: makeSelectShipState(selectState),
    delOk: makeSelectDelState(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(StowageManageWrap);
};
