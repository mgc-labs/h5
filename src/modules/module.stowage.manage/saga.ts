/**
 * StowageManage Saga
 * @author hefan
 * @date 2018/9/11 上午9:05:16
 */
import { push } from 'connected-react-router';
import { call, put, takeLatest } from 'redux-saga/effects';

import message from 'antd/es/message';

import { reqDelGroup, reqPartGroupList, reqSendPartOrder } from './service';

export default (CONSTANTS, actions) => {
  function* getData(action) {
    const { status, ...other } = action.payload;
    const newStatus = !status && status !== 0 ? undefined : status;
    try {
      const partGroupList = yield call(reqPartGroupList, {
        ...other,
        status: newStatus,
      });

      yield put(actions.getAsyncDataDone(partGroupList));
    } catch (err) {
      yield put(actions.getAsyncDataError(err));
    }
  }

  function* putShipGoods(action) {
    const { partGroupIds, ...refreshParams } = action.payload;
    try {
      const data = yield call(reqSendPartOrder, partGroupIds);
      message.success('已完成发货');

      yield put(actions.putShipGoodsDone(data));

      yield put(actions.getAsyncData(refreshParams));
    } catch (error) {
      yield put(actions.putShipGoodsError(error));
    }
  }

  function* delGroup(action) {
    const { partGroupIds, ...refreshParams } = action.payload;

    try {
      const data = yield call(reqDelGroup, { partGroupIds });

      message.success('撤销配载完成');

      yield put(actions.delGroupDone(data));

      yield put(actions.getAsyncData(refreshParams));
    } catch (error) {
      yield put(actions.delGroupError(error));
    }
  }

  function* toDetail(action) {
    try {
      yield put(
        push(`/stowageManage/stowageDetail?partGroupId=${action.payload}`),
      );
    } catch (error) {
      message.error(error.toString());
    }
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_ASYNC_DATA, getData);
    yield takeLatest(CONSTANTS.PUT_SHIP_GOODS, putShipGoods);
    yield takeLatest(CONSTANTS.DEL_GROUP, delGroup);
    yield takeLatest(CONSTANTS.GOTO_DETAIL, toDetail);
  };
};
