/**
 * StowageManage Actions
 * @author hefan
 * @date 2018/9/11 上午9:05:16
 */
import { AnyAction } from 'redux';

export default CONSANTS => ({
  getAsyncData: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_ASYNC_DATA,
  }),

  getAsyncDataDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_ASYNC_DATA_SUCCESS,
  }),

  getAsyncDataError: (error): AnyAction => ({
    error: true,
    payload: error,
    type: CONSANTS.GET_ASYNC_DATA_ERROR,
  }),

  putShipGoods: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.PUT_SHIP_GOODS,
  }),

  putShipGoodsDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.PUT_SHIP_GOODS_SUCCESS,
  }),

  putShipGoodsError: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.PUT_SHIP_GOODS_ERROR,
  }),

  delGroup: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.DEL_GROUP,
  }),

  delGroupDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.DEL_GROUP_SUCCESS,
  }),

  delGroupError: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.DEL_GROUP_ERROR,
  }),

  toDetail: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GOTO_DETAIL,
  }),
});
