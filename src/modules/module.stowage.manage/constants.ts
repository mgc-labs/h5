/**
 * StowageManage Constants
 * @author hefan
 * @date 2018/9/11 上午9:05:16
 */
export const GET_ASYNC_DATA = 'GET_ASYNC_DATA';
export const GET_ASYNC_DATA_SUCCESS = 'GET_ASYNC_DATA_SUCCESS';
export const GET_ASYNC_DATA_ERROR = 'GET_ASYNC_DATA_ERROR';

export const PUT_SHIP_GOODS = 'PUT_SHIP_GOODS';
export const PUT_SHIP_GOODS_SUCCESS = 'PUT_SHIP_GOODS_SUCCESS';
export const PUT_SHIP_GOODS_ERROR = 'PUT_SHIP_GOODS_ERROR';

export const DEL_GROUP = 'DEL_GROUP';
export const DEL_GROUP_SUCCESS = 'DEL_GROUP_SUCCESS';
export const DEL_GROUP_ERROR = 'DEL_GROUP_ERROR';

export const GOTO_DETAIL = 'GOTO_DETAIL';
