/**
 * StowageManage Reducers
 * @author hefan
 * @date 2018/9/11 上午9:05:16
 */

import { fromJS, List } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  data: List(),
  dataCount: 0,
  error: false,
  loading: false,
  shipOk: false,
  delOk: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_ASYNC_DATA:
        return state
          .set('error', false)
          .set('shipOk', false)
          .set('delOk', false)
          .set('loading', true);
      case CONSANTS.GET_ASYNC_DATA_SUCCESS:
        return state
          .set('loading', false)
          .set('data', fromJS(action.payload.data))
          .set('dataCount', fromJS(action.payload.count));
      case CONSANTS.GET_ASYNC_DATA_ERROR:
        return state.set('error', action.error).set('loading', false);

      case CONSANTS.PUT_SHIP_GOODS:
        return state.set('error', false).set('loading', true);
      case CONSANTS.PUT_SHIP_GOODS_SUCCESS:
        return state.set('shipOk', true).set('loading', false);
      case CONSANTS.PUT_SHIP_GOODS_ERROR:
        return state.set('error', action.error).set('loading', false);

      case CONSANTS.DEL_GROUP:
        return state.set('error', false).set('loading', true);
      case CONSANTS.DEL_GROUP_SUCCESS:
        return state.set('delOk', true).set('loading', false);
      case CONSANTS.DEL_GROUP_ERROR:
        return state.set('error', action.error).set('loading', false);

      default:
        return state;
    }
  };
};
