/**
 * StowageManage selectors
 * @author hefan
 * @date 2018/9/11 上午9:05:16
 */
import { createSelector } from 'reselect';

export const makeSelectError = selectState =>
  createSelector(selectState, state => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, state => state.get('loading'));

export const makeSelectData = selectState =>
  createSelector(selectState, state => state.get('data'));

export const makeSelectDataCount = selectState =>
  createSelector(selectState, state => state.get('dataCount'));

export const makeSelectShipState = selectState =>
  createSelector(selectState, state => state.get('shipOk'));

export const makeSelectDelState = selectState =>
  createSelector(selectState, state => state.get('delOk'));
