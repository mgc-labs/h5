/**
 * Customer Actions
 * @author yanrong.tian
 * @date 2018-9-13 14:19:59
 */
export default {
  getCustomers: 'CUSTOMER/GET_CUSTOMERS',
  getCustomersDone: 'CUSTOMER/GET_CUSTOMERS_DONE',
  getCustomersError: 'CUSTOMER/GET_CUSTOMERS_ERROR',
  deleteCustomer: 'CUSTOMER/DELETE_CUSTOMER',
  deleteCustomerDone: 'CUSTOMER/DELETE_CUSTOMER_DONE',
  deleteCustomerError: 'CUSTOMER/DELETE_CUSTOMER_ERROR',
  getCustomersTotal: 'CUSTOMER/GET_CUSTOMERS_TOTAL',
  getCustomersTotalDone: 'CUSTOMER/GET_CUSTOMERS_TOTAL_DONE',
  getCustomersTotalError: 'CUSTOMER/GET_CUSTOMERS_TOTAL_ERROR',
  editCustomer: 'CUSTOMER/EDIT_CUSTOMER',
};
