/**
 * Auth Service
 * @author ryan bian
 */
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_CUSTOMERS =
  '/ehuodiGateway/utmsCore/utmsCustomercs/selectCustomerList';
const API_DELETE_CUSTOMER =
  '/ehuodiGateway/utmsCore/utmsCustomercs/deleteCustomer';
const API_CUSTOMERS_TOTAL =
  '/ehuodiGateway/utmsReport/utmsCustomercs/statisticsCustomer';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_CUSTOMERS)
    .reply(() =>
      import('./mock/customers').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_DELETE_CUSTOMER)
    .reply(() =>
      import('./mock/customers').then(exports => [200, exports.default]),
    );
  mocker
    .on('post', API_CUSTOMERS_TOTAL)
    .reply(() =>
      import('./mock/customersTotal').then(exports => [200, exports.default]),
    );
}

/**
 * 获取客户列表
 */
export const getCustomers = options =>
  request(
    {
      method: 'post',
      url: API_CUSTOMERS,
      data: options,
      params: options,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);

export const deleteCustomer = options =>
  request(
    {
      method: 'post',
      url: API_DELETE_CUSTOMER,
      data: { utmsCustomerId: options },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);

export const getCustomersTotal = options =>
  request(
    {
      method: 'post',
      url: API_CUSTOMERS_TOTAL,
      data: options,
      params: options,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);
