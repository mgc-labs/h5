export default {
  result: 'success',
  count: 300,
  data: [
    {
      utmsCustomerId: 1,
      customerName: '客户A',
      contact: '王红宇',
      contactWay: '7277772727',
      organizationName: 'A分公司',
      province: '浙江省',
      city: '杭州',
      region: '滨江区',
      address: '迎春小区',
      industry: '服装',
      industryCode: 54243,
      salesman: [
        {
          jobcard: '15119',
          name: '郭井阳',
        },
      ],
    },
    {
      utmsCustomerId: 2,
      customerName: '客户B',
      contact: '王红A',
      contactWay: '7277772727',
      organizationName: 'A分公司',
    },
    {
      utmsCustomerId: 3,
      customerName: '客户C',
      contact: '王红B',
      contactWay: '7277772727',
      organizationName: '总公司',
    },
    {
      utmsCustomerId: 4,
      customerName: '客户D',
      contact: '王红C',
      contactWay: '7277772727',
      organizationName: 'A分公司',
      province: '浙江省',
      city: '杭州',
      region: '萧山区',
      address: '恒大帝景',
      industry: '生产制造',
      industryCode: 54245,
      salesman: [
        {
          jobcard: '15129',
          name: '田荣',
        },
        {
          jobcard: '19484',
          name: '刘小月',
        },
      ],
    },
  ],
};
