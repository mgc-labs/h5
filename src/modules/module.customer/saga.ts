/**
 * Customer Saga
 * @author yanrong.tian
 * @date 2018-9-13 14:19:59
 */
import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';

import { deleteCustomer, getCustomers, getCustomersTotal } from './service';

const options: any = {
  getCustomers: {
    skipCount: 0,
    pageSize: 15,
  },
};

export default (CONSTANTS, actions) => {
  function* callCustomers(action) {
    try {
      options.getCustomers = {
        ...options.getCustomers,
        ...(action.payload || {}),
      };
      const data = yield call(getCustomers, options.getCustomers);

      (data.data || []).map(item => {
        item.salesman = JSON.parse(item.salesman || '[]');
      });
      yield put(actions.getCustomersDone(data));
    } catch (err) {
      yield put(actions.getCustomersError(err));
    }
  }

  function* callDeleteCustomer(action) {
    try {
      const data = yield call(deleteCustomer, action.payload);
      yield put(actions.deleteCustomerDone(data));
      yield put(actions.getCustomers());
    } catch (err) {
      yield put(actions.deleteCustomerError(err));
    }
  }

  function* callCustomerTotal(action) {
    try {
      const data = yield call(getCustomersTotal, action.payload);
      yield put(actions.getCustomersTotalDone(data));
    } catch (err) {
      yield put(actions.getCustomersTotalError(err));
    }
  }

  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_CUSTOMERS, callCustomers);
    yield takeLatest(CONSTANTS.DELETE_CUSTOMER, callDeleteCustomer);
    yield takeLatest(CONSTANTS.GET_CUSTOMERS_TOTAL, callCustomerTotal);
  };
};
