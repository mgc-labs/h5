/**
 * Customer Loadable
 * @author yanrong.tian
 * @date 2018-9-13 14:19:59
 */
import Loadable from 'react-loadable';

import LoadingIndicator from 'components/LoadingIndicator';

let Component;
if (process.env.NODE_ENV === 'development') {
  Component = require('./index').default;
} else {
  Component = config =>
    Loadable({
      loader: () =>
        import(/* webpackChunkName: "customer" */ './index').then(exports => {
          return exports.default(config);
        }),
      loading: LoadingIndicator,
    });
}

export default Component;

import { createActionFactory, createConstants } from 'utils/createAction';
import actions from './actions';

const constants = createConstants(actions);
const actionFactory = createActionFactory(actions);
export { constants };
export { actionFactory };
export { default as sagaFactory } from './saga';
export { default as reducerFactory } from './reducer';
