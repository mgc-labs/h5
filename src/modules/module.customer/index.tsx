/**
 * Customer Component
 * @author yanrong.tian
 * @date 2018-9-13 14:19:59
 */
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Divider from 'antd/es/divider';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';
import Popconfirm from 'antd/es/popconfirm';
import Row from 'antd/es/row';
import MyTable from 'components/MyTable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import GLOBAL from 'utils/GLOBAL';
import styles from './index.module.less';
import {
  makeCustomers,
  makeCustomersTotal,
  makeSelectError,
  makeSelectLoading,
} from './selectors';

const Search = Input.Search;

interface IProps {
  children: React.ReactChildren;
  deleteCustomer: (options: any) => void;
  getCustomers: (options: any) => void;
  getCustomersTotal: () => void;
  editCustomer: (options: any) => void;
  permissions: any;
}

interface IState {
  customerAddVisible: boolean;
  currentItem?: object;
  keyword: string;
  current: number;
}

class Customer extends React.PureComponent<IProps> {
  state: IState;
  keywordInput: any;
  constructor(props) {
    super(props);
    this.state = {
      customerAddVisible: false,
      keyword: '',
      current: 1,
    };
  }
  public componentDidMount() {
    this.fetchCustomersData();
    // this.props.getCustomersTotal();
  }

  public render() {
    const { customers = {}, customersTotal = {}, loading }: any = this.props;
    const { current } = this.state;
    const columns = this.getColumns();
    const paginationConfig = {
      total: customers.count,
      onChange: this.fetchCustomersData,
      onShowSizeChange: this.fetchCustomersData,
      current,
    };
    const buttonAdd = () => {
      if (!this.getIspermission('/customer/add')) {
        return '';
      }
      return (
        <Button type="primary">
          <Link to="/customer/add">新建客户</Link>
        </Button>
      );
    };
    return (
      <div>
        {/* {this.renderTotalInfo()} */}
        <Card title="客户列表" bordered={true} extra={buttonAdd()}>
          {this.renderSearchForm()}
          <MyTable
            columns={columns}
            dataSource={customers.data}
            pagination={paginationConfig}
            rowKey={'utmsCustomerId'}
            loading={loading}
          />
        </Card>
        {/* <CustomerAddModal
          visible={this.state.customerAddVisible}
          onCancel={this.hanldModelCancel}
          onOk={this.hanldCustomerAddSuccess}
          data={this.state.currentItem}
        /> */}
      </div>
    );
  }

  private getIspermission = key => {
    const { permissions } = this.props;
    let isPermissions = false;
    try {
      permissions
        .map(item => {
          if (key === item.get('operateKey')) {
            isPermissions = true;
          }
        })
        .toArray();
    } catch (err) {
      //
    }
    return isPermissions;
  };

  private emitEmpty = () => {
    this.keywordInput.focus();
    this.setState({ keyword: '' });
  };

  private renderSearchForm() {
    const { keyword } = this.state;
    const suffix = keyword ? (
      <Icon type="close-circle" onClick={this.emitEmpty} />
    ) : null;
    return (
      <div style={{ marginBottom: 15 }}>
        <Input
          placeholder="客户名称/联系方式"
          style={{ width: 250, marginRight: 15 }}
          suffix={suffix}
          value={keyword || ''}
          onPressEnter={e => {
            this.fetchCustomersData({
              skipCount: 0,
              keyword: this.state.keyword,
            });
          }}
          onChange={e => {
            this.setState({
              keyword: e.target.value,
            });
          }}
          ref={node => (this.keywordInput = node)}
        />
        <Button
          type="primary"
          style={{ marginRight: 12 }}
          onClick={() => {
            this.fetchCustomersData({
              skipCount: 0,
              keyword: this.state.keyword,
            });
          }}
        >
          查询
        </Button>
        <Button
          onClick={() => {
            this.setState({
              keyword: '',
            });
            this.fetchCustomersData({
              skipCount: 0,
              keyword: '',
            });
          }}
        >
          重置
        </Button>
      </div>
    );
  }

  private renderTotalInfo() {
    const { customersTotal = {} }: any = this.props;
    const totalInfo = {
      xs: 6,
      sm: 6,
      lg: 6,
      xl: 6,
      className: styles.totalInfoCard,
    };
    return (
      <Card className={styles.totalCard}>
        <Row gutter={10}>
          <Col {...totalInfo}>
            <h3>总客户</h3>
            <p>{customersTotal.totalCnt || GLOBAL.emptyRecord}</p>
          </Col>
          <Col {...totalInfo}>
            <h3>累计发货客户</h3>
            <p>{customersTotal.releaseCnt || GLOBAL.emptyRecord}</p>
          </Col>
          <Col {...totalInfo}>
            <h3>客户总订单</h3>
            <p>{customersTotal.tradeCnt || GLOBAL.emptyRecord}</p>
          </Col>
          <Col {...totalInfo}>
            <h3>客户总营收（万）</h3>
            <p>{customersTotal.revenue || GLOBAL.emptyRecord}</p>
          </Col>
        </Row>
      </Card>
    );
  }

  private hanldCustomerAddSuccess = options => {
    this.hanldModelCancel();
    if (options.utmsCustomerId) {
      this.setState({
        currentItem: options,
      });
      this.props.editCustomer(options);
    } else {
      this.fetchCustomersData();
    }
  };

  private hanldModelCancel = () => {
    this.setState({
      customerAddVisible: false,
    });
  };

  private getColumns() {
    const { deleteCustomer } = this.props;

    const addCustomerButton = record => {
      if (this.getIspermission('/customer/edit')) {
        return (
          <Link to={`/customer/edit?id=${record.utmsCustomerId}`}>修改</Link>
        );
      }
      return <span />;
    };

    const deleteCustomerButton = record => {
      if (this.getIspermission('deleteCustomer')) {
        return (
          <Popconfirm
            title="确认删除"
            onConfirm={() => deleteCustomer(record.utmsCustomerId)}
          >
            <a href="javascript:;">删除</a>
          </Popconfirm>
        );
      }
      return <span />;
    };

    const divider = () => {
      if (this.getIspermission('deleteCustomer')) {
        return <Divider type="vertical" />;
      }
    };
    return [
      {
        title: '客户名称',
        dataIndex: 'customerName',
        key: 'customerName',
      },
      {
        title: '联系人',
        dataIndex: 'contact',
        key: 'contact',
        render: (text, record) => {
          return (
            <span className="my-nowrap">{text || GLOBAL.emptyRecord}</span>
          );
        },
      },
      {
        title: '联系方式',
        dataIndex: 'contactWay',
        key: 'contactWay',
        render: (text, record) => {
          return (
            <span className="my-nowrap">{text || GLOBAL.emptyRecord}</span>
          );
        },
      },
      {
        title: '公司地址',
        key: 'organizationName',
        render: (text, record) => {
          let address = GLOBAL.emptyRecord;
          if (record.address) {
            address = `${record.province || ''} ${record.city ||
              ''} ${record.region || ''} ${record.address}`;
          }
          return address;
        },
      },
      {
        title: '销售经理',
        key: 'salesman',
        render: (text, record) => {
          const salesman = text.salesman || [];
          if (!salesman.length) {
            return GLOBAL.emptyRecord;
          }
          return salesman.map((d, i) => {
            const dot = i === salesman.length - 1 ? '' : '、';
            return (
              <span key={d.jobcard} className="my-nowrap">
                {d.name}
                {dot}
              </span>
            );
          });
        },
      },
      {
        title: '操作',
        key: 'action',
        render: (text, record) => (
          <span className="my-nowrap">
            {addCustomerButton(record)}
            {divider()}
            {deleteCustomerButton(record)}
          </span>
        ),
      },
    ];
  }

  private showModal = record => {
    this.setState({
      customerAddVisible: true,
      currentItem: record,
    });
  };

  private hideModal = utmsCustomerId => {
    this.setState({
      customerAddVisible: false,
    });
  };

  // 获取客户列表数据
  private fetchCustomersData: any = options => {
    this.props.getCustomers(options);
    this.setState({
      current: (options ? options.current : 1) || 1,
    });
  };
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getCustomers: opts => dispatch(actions.getCustomers(opts)),
    deleteCustomer: opts => dispatch(actions.deleteCustomer(opts)),
    getCustomersTotal: opts => dispatch(actions.getCustomersTotal(opts)),
    editCustomer: opts => dispatch(actions.editCustomer(opts)),
  });

  const selectState = state => state.get(routeId);
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    customers: makeCustomers(selectState),
    customersTotal: makeCustomersTotal(selectState),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  return compose(withConnect)(Customer);
};
