/**
 * Customer Reducers
 * @author yanrong.tian
 * @date 2018-9-13 14:19:59
 */

import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  data: {},
  error: false,
  loading: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_CUSTOMERS:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_CUSTOMERS_DONE:
        return state.set('loading', false).set('customers', action.payload);
      case CONSANTS.GET_CUSTOMERS_ERROR:
        return state.set('error', action.error).set('loading', false);
      case CONSANTS.GET_CUSTOMERS_TOTAL_DONE:
        return state.set('customersTotal', action.payload.data);
      case CONSANTS.EDIT_CUSTOMER:
        const customers = state.get('customers');
        const newCustomrsData = [];
        customers.data.map(item => {
          if (item.utmsCustomerId === action.payload.utmsCustomerId) {
            newCustomrsData.push({
              ...item,
              ...action.payload,
            });
          } else {
            newCustomrsData.push(item);
          }
        });
        return state.set('customers', {
          ...customers,
          ...{ data: newCustomrsData },
        });
      default:
        return state;
    }
  };
};
