/**
 * LtlOrderEdit Actions
 * @author djd
 * @date 2018/9/13 下午1:52:15
 */
import { AnyAction } from 'redux';
import {
  GET_ASYNC_DATA,
  GET_ASYNC_DATA_ERROR,
  GET_ASYNC_DATA_SUCCESS,
} from './constants';

export default CONSANTS => ({
  getAsyncData: (param): AnyAction => ({
    payload: param,
    type: CONSANTS.GET_ASYNC_DATA,
  }),

  getAsyncDataDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_ASYNC_DATA_SUCCESS,
  }),

  getAsyncDataError: (error): AnyAction => ({
    error,
    type: CONSANTS.GET_ASYNC_DATA_ERROR,
  }),
  getCustomerData: (): AnyAction => ({
    type: CONSANTS.GET_CUSTOMER_DATA,
  }),

  getCustomerDataDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.GET_CUSTOMER_DATA_SUCCESS,
  }),
  insertPartOrder: (param): AnyAction => ({
    payload: param,
    type: CONSANTS.INSERT_PART_ORDER,
  }),

  insertPartOrderDone: (data): AnyAction => ({
    payload: data,
    type: CONSANTS.INSERT_PART_ORDER_SUCCESS,
  }),
  insertPartOrderError: (error): AnyAction => ({
    error,
    type: CONSANTS.INSERT_PART_ORDER_ERROR,
  }),
  updatePartOrder: (param): AnyAction => ({
    payload: param,
    type: CONSANTS.UPDATE_PART_ORDER,
  }),
  toList: (param, url): AnyAction => ({
    payload: param,
    url,
    type: CONSANTS.TO_LIST,
  }),
});
