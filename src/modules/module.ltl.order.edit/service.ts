import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';
// API list
const API_ORDER_DETAIL =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/partOrderDetail';
const API_GET_CUSTOMER =
  '/ehuodiGateway/utmsCore/utmsCustomercs/selectCustomerList';
const API_INSERT_PART_ORDER =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/insertPartOrder';
const API_UPDATE_PART_ORDER =
  '/ehuodiGateway/utmsTrade/utmsPartOrder/updatePartOrder';
if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
}
/**
 * 查询订单详情
 */
export const getOrderDetail = params => {
  return request(
    {
      method: 'post',
      url: API_ORDER_DETAIL,
      data: qs.stringify(params),
    },
    {
      globalErrorMsg: true,
      useMock: false,
    },
  );
};
/**
 * 获取客户信息
 * @param params
 */
export const getCustomer = keyword => {
  return request(
    {
      method: 'post',
      url: API_GET_CUSTOMER,
      data: {
        keyword,
        pageSize: 100,
      },
    },
    {
      globalErrorMsg: true,
      useMock: false,
    },
  );
};
/**
 * 开单
 * @param params
 */
export const insertPartOrder = data => {
  return request(
    {
      method: 'post',
      url: API_INSERT_PART_ORDER,
      data,
    },
    {
      globalErrorMsg: true,
      useMock: false,
    },
  );
};
/**
 * 修改零担订单
 * @param params
 */
export const updatePartOrder = data => {
  return request(
    {
      method: 'post',
      url: API_UPDATE_PART_ORDER,
      data,
    },
    {
      globalErrorMsg: true,
      useMock: false,
    },
  );
};
