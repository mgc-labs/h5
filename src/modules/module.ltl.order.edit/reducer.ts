/**
 * LtlOrderEdit Reducers
 * @author djd
 * @date 2018/9/13 下午1:52:15
 */

import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  data: {},
  error: false,
  loading: false,
});

export default CONSANTS => {
  return (state = initialState, action) => {
    switch (action.type) {
      case CONSANTS.GET_ASYNC_DATA:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_ASYNC_DATA_SUCCESS:
        return state.set('loading', false).set('data', action.payload.data);
      case CONSANTS.GET_ASYNC_DATA_ERROR:
        return state.set('error', action.error).set('loading', false);
      case CONSANTS.GET_CUSTOMER_DATA:
        return state.set('error', false).set('loading', true);
      case CONSANTS.GET_CUSTOMER_DATA_SUCCESS:
        return state
          .set('loading', false)
          .set('customers', action.payload.data);
      case CONSANTS.INSERT_PART_ORDER:
        return state
          .set('error', false)
          .set('loading', true)
          .set('insert', null);
      case CONSANTS.UPDATE_PART_ORDER:
        return state
          .set('error', false)
          .set('loading', true)
          .set('insert', null);
      case CONSANTS.INSERT_PART_ORDER_ERROR:
        return state.set('loading', false).set('error', action.error);
      case CONSANTS.INSERT_PART_ORDER_SUCCESS:
        return state.set('loading', false).set('insert', action.payload);
      default:
        return state;
    }
  };
};
