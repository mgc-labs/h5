/**
 * LtlOrderEdit Saga
 * @author djd
 * @date 2018/9/13 下午1:52:15
 */
import msngr from 'msngr';
import { call, put, takeLatest } from 'redux-saga/effects';

import request, { mocker } from 'utils/request';

import { push } from 'connected-react-router';
// import { TOGGLE_NAV_COLLAPSE } from '../module.app/constants';
import {
  getCustomer,
  getOrderDetail,
  insertPartOrder,
  updatePartOrder,
} from './service';

export default (CONSTANTS, actions) => {
  function* getData(antion) {
    try {
      const data = yield call(getOrderDetail, antion.payload);
      yield put(actions.getAsyncDataDone(data));
    } catch (err) {
      yield put(actions.getAsyncDataError(err));
    }
  }
  function* getCustomerData() {
    try {
      const data = yield call(getCustomer, {});
      yield put(actions.getCustomerDataDone(data));
    } catch (err) {
      console.log(err);
    }
  }
  function* insertOrder(action) {
    try {
      const data = yield call(insertPartOrder, action.payload);
      yield put(actions.insertPartOrderDone(data));
    } catch (err) {
      yield put(actions.insertPartOrderError(err));
    }
  }
  function* updateOrder(action) {
    try {
      const data = yield call(updatePartOrder, action.payload);
      yield put(actions.insertPartOrderDone(data));
    } catch (err) {
      yield put(actions.insertPartOrderError(err));
    }
  }
  function* toList(action) {
    const url = action.payload
      ? decodeURIComponent(action.payload)
      : '/ltlOrder';
    for (const ref of action.url) {
      msngr(ref, 'refresh').emit(true);
    }
    yield put(push(url));
  }

  /**
   * Root saga manages watcher lifecycle
   */
  return function* rootSaga() {
    yield takeLatest(CONSTANTS.GET_ASYNC_DATA, getData);

    yield takeLatest(CONSTANTS.GET_CUSTOMER_DATA, getCustomerData);
    yield takeLatest(CONSTANTS.INSERT_PART_ORDER, insertOrder);
    yield takeLatest(CONSTANTS.UPDATE_PART_ORDER, updateOrder);
    yield takeLatest(CONSTANTS.TO_LIST, toList);
  };
};
