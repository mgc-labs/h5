/**
 * LtlOrderEdit selectors
 * @author djd
 * @date 2018/9/13 下午1:52:15
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

const globalState = state => state.get('global');

export const makeSelectError = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('error'));

export const makeSelectLoading = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('loading'),
  );

export const makeSelectData = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('data'));
export const makeSelectCustomerData = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('customers'),
  );
export const makeSelectInsert = selectState =>
  createSelector(selectState, (state: Map<string, any>) => state.get('insert'));
export const makeSelectRouteIds = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.getIn(['route', 'routeIds']),
  );
