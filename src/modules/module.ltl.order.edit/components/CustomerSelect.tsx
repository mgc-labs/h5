/**
 * LtlOrderEdit Component
 * @author ggm
 * @date 2018/9/27 下午1:52:15
 */
import Select from 'antd/es/select';
import * as React from 'react';

const Option = Select.Option;
export interface IProps {
  children?: React.ReactChildren;
  value?: any;
  defaultValue?: any;
  customers?: any[];
  disabled?: boolean;
  onChange?: (value: any) => any;
}

export class CustomerSelect extends React.PureComponent<IProps> {
  setDefaultValue: boolean = false;
  setCustomers: boolean = false;
  constructor(props) {
    super(props);
    this.state = {
      options: [],
    };
    this.onChange = this.onChange.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.customers) {
      if (!this.setCustomers) {
        this.setCustomers = true;
        const options = this.renderOptions(nextProps.customers);
        this.setState((prevState: any, props) => {
          prevState.options.push(options);
          if (!nextProps.value) {
            if (nextProps.customers[0]) {
              this.props.onChange({
                name: nextProps.customers[0].customerName,
                code: nextProps.customers[0].customerCode,
              });
            }
            return {
              defaultValue: nextProps.customers[0]
                ? nextProps.customers[0].customerName
                : '',
              options: prevState.options,
            };
          }
          return {
            options: prevState.options,
          };
        });
      }
    }
    if (nextProps.value) {
      if (!this.setDefaultValue) {
        this.setDefaultValue = true;
        const [name, code] = nextProps.value.split(',');
        this.setState({ defaultValue: name });
      }
    }
  }
  render() {
    const { options, defaultValue } = this.state;
    return (
      <Select
        value={defaultValue}
        onChange={this.onChange}
        disabled={this.props.disabled}
      >
        {options}
      </Select>
    );
  }
  private onChange(v, flag = true) {
    if (v) {
      const [name, code] = v.split(',');
      this.props.onChange({ name, code });
      if (!flag) {
        return name;
      }
      this.setState({ defaultValue: name });
    }
  }
  private renderOptions(customers) {
    if (customers) {
      const c = customers.map((item, index) => {
        return (
          <Option
            key={item.customerCode + index}
            value={`${item.customerName},${item.customerCode}`}
          >
            {item.customerName}
          </Option>
        );
      });
      return c;
    }
    return [];
  }
}
