/**
 * LtlOrderEdit Component
 * @author djd
 * @date 2018/9/13 下午1:52:15
 */
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Col from 'antd/es/col';
import Form, { FormComponentProps } from 'antd/es/form';
import Input from 'antd/es/input';
import message from 'antd/es/message';
import Modal from 'antd/es/modal';
import Radio from 'antd/es/radio';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import Spin from 'antd/es/spin';
import AddressSelect from 'components/AddressSelect';
import DateTimePicker from 'components/DateTimePicker';
import FormEditCard, {
  formItemLayout,
  formLayoutCols3,
  gutter,
} from 'components/FormEditCard';
import { PageBottom, PageBottomButtons } from 'components/PageBottom';
import TextArea from 'components/TextArea';
import { CustomerSelect } from './components/CustomerSelect';

import { checkNumberForm } from 'utils/checkString';
import { FIXED_TELEPHONE } from 'utils/commonRegExp';

import {
  makeSelectCustomerData,
  makeSelectData,
  makeSelectError,
  makeSelectInsert,
  makeSelectLoading,
  makeSelectRouteIds,
} from './selectors';

import styles from './index.module.less';
const Option = Select.Option;
const RadioGroup = Radio.Group;
export interface ILtlOrderEditProps extends FormComponentProps {
  children: React.ReactChildren;
  data?: any;
  customers?: any;
  insert?: any;
  loading: boolean;
  routeIds: [];
  searchParams?: {
    id: number | string;
    back?: string;
  };
  eventEmitter?: object;
  close: () => any;
  refresh: (param?: any) => any;
  getAsyncData?: (param?: object) => any;
  getCustomerData?: (param?: object) => any;
  insertPartOrder: (param: object) => any;
  updatePartOrder: (param: object) => any;
  toList: (param?: string, url?: string[]) => any;
}

class LtlOrderEdit extends React.PureComponent<ILtlOrderEditProps> {
  repeatSubmit = false;
  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { searchParams, data } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err && !this.repeatSubmit) {
        this.handleRepeatSubmit();
        const param = { ...values };
        if (param.requireDate) {
          param.requireDate =
            param.requireDate.length > 16
              ? param.requireDate
              : param.requireDate + ':00';
        }
        if (searchParams && searchParams.id) {
          param.utmsPartOrderId = data.utmsPartOrderId;
          return this.props.updatePartOrder(param);
        }
        this.props.insertPartOrder(param);
      }
    });
  };
  handleRepeatSubmit = () => {
    this.repeatSubmit = !this.repeatSubmit;
    setTimeout(() => {
      this.repeatSubmit = !this.repeatSubmit;
    }, 1000);
  };
  cancel = (e: React.FormEvent<HTMLFormElement>) => {
    const { close, form } = this.props;
    if (form.isFieldsTouched()) {
      this.cancleEdit(close);
    } else {
      close();
    }
  };
  customerChange = obj => {
    const { setFieldsValue } = this.props.form;
    setFieldsValue({
      customerCode: obj.code,
      customerName: obj.name,
    });
  };
  onAddressSelected = result => {
    const { setFieldsValue } = this.props.form;
    setFieldsValue({
      longitude: result ? result.lng : '',
      latitude: result ? result.lat : '',
      province: result ? result.province + result.city + result.region : '',
    });
  };
  cancleEdit = cb => {
    const { form } = this.props;
    Modal.confirm({
      title: '有未保存的信息',
      content: '如放弃，填写的信息将丢失',
      okText: '继续填写',
      cancelText: '放弃',
      iconType: 'info-circle',
      centered: true,
      onCancel() {
        if (cb) {
          cb();
        }
      },
    });
  };
  componentDidMount() {
    const {
      searchParams,
      getAsyncData,
      getCustomerData,
      toList,
      routeIds,
      refresh,
      eventEmitter,
      form,
    } = this.props;
    getCustomerData();
    if (searchParams && searchParams.id) {
      getAsyncData({ utmsPartOrderId: searchParams.id });
    }
    eventEmitter.on('close', (requestClose, eventSource) => {
      if (
        (eventSource === 'FROM_TAB_CLOSE' ||
          eventSource === 'FROM_TAB_REFRESH') &&
        form.isFieldsTouched()
      ) {
        this.cancleEdit(requestClose);
      } else {
        requestClose();
      }
    });
  }
  componentDidUpdate() {
    const { insert, searchParams, close, toList, routeIds } = this.props;
    if (insert && insert.result === 'success') {
      let msg = '开单成功';
      if (searchParams && searchParams.id) {
        msg = '修改成功';
      }
      message.success(msg);
      close();
      let back = '';
      if (searchParams && searchParams.id) {
        back = searchParams.back ? decodeURIComponent(searchParams.back) : back;
      }
      const refreshUrl = [];
      for (const item of routeIds) {
        if (
          item.split('?').includes('/ltlOrder') ||
          (back && item.indexOf(back) >= 0) ||
          item.split('&').includes(`/ltlOrder/detail?id=${searchParams.id}`)
        ) {
          refreshUrl.push(item);
        }
      }
      toList(back, refreshUrl);
    }
  }
  public render() {
    const { getFieldDecorator } = this.props.form;
    const {
      customerName = '',
      customerCode = '',
      thirdSystemId = '',
      requireDate = '',
      contact = '',
      phone = '',
      province = '',
      address = '',
      longitude = '',
      latitude = '',
      goodsName = '',
      goodsNumber = '',
      goodsWeight = '',
      goodsVolume = '',
      delegateAmount = '',
      isReceipt = true,
      remark = '',
    } = this.props.data || {};
    const { loading, searchParams } = this.props;
    const customer =
      customerName && customerCode ? `${customerName},${customerCode}` : '';
    return (
      <div className={styles.LtlOrderEdit}>
        <Form onSubmit={this.handleSubmit} layout="vertical">
          <FormEditCard title="基本信息">
            <Row gutter={gutter}>
              <Col {...formLayoutCols3}>
                <Form.Item
                  {...formItemLayout}
                  label="客户"
                  style={{ marginBottom: 0 }}
                >
                  <CustomerSelect
                    value={customer}
                    onChange={this.customerChange}
                    customers={this.props.customers}
                    disabled={searchParams && searchParams.id ? true : false}
                  />
                  {getFieldDecorator('customerName', {
                    initialValue: customerName,
                    rules: [
                      {
                        required: true,
                        message: '请选择客户',
                      },
                    ],
                  })(<Input type="hidden" />)}
                </Form.Item>
                <Form.Item {...formItemLayout} style={{ marginBottom: 0 }}>
                  {getFieldDecorator('customerCode', {
                    initialValue: customerCode,
                  })(<Input type="hidden" />)}
                </Form.Item>
              </Col>
              <Col {...formLayoutCols3}>
                <Form.Item {...formItemLayout} label="第三方订单号">
                  {getFieldDecorator('thirdSystemId', {
                    initialValue: thirdSystemId,
                  })(<Input maxLength={40} placeholder="请输入第三方单号" />)}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={gutter}>
              <Col {...formLayoutCols3}>
                <Form.Item {...formItemLayout} label="要求送达时间">
                  {getFieldDecorator('requireDate', {
                    initialValue: requireDate,
                  })(<DateTimePicker placeholder="请选择时间" />)}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={gutter}>
              <Col {...formLayoutCols3}>
                <Form.Item {...formItemLayout} label="收货人">
                  <Col span={11}>
                    {getFieldDecorator('contact', {
                      initialValue: contact,
                      rules: [
                        {
                          required: true,
                          message: '请输入收货人姓名',
                        },
                      ],
                    })(<Input maxLength={40} placeholder="请输入姓名" />)}
                  </Col>
                  <Col span={2}>
                    <span
                      style={{
                        display: 'inline-block',
                        position: 'relative',
                        width: '100%',
                        top: '3px',
                        textAlign: 'center',
                      }}
                    >
                      --
                    </span>
                  </Col>
                  <Col span={11}>
                    <Form.Item>
                      {getFieldDecorator('phone', {
                        initialValue: phone,
                        rules: [
                          {
                            required: true,
                            message: '请输入手机号码',
                          },
                          {
                            pattern: FIXED_TELEPHONE,
                            message: '请输入正确的联系方式',
                          },
                        ],
                      })(<Input maxLength={40} placeholder="请输入联系方式" />)}
                    </Form.Item>
                  </Col>
                </Form.Item>
              </Col>
              <Col {...formLayoutCols3}>
                <Form.Item
                  {...formItemLayout}
                  label="收货地址"
                  style={{ marginBottom: 0, paddingBottom: 0 }}
                >
                  {getFieldDecorator('address', {
                    initialValue: address,
                    rules: [
                      {
                        required: true,
                        message: '请输入收货地址',
                      },
                    ],
                  })(
                    <AddressSelect
                      placeholder="请输入收货地址"
                      onSelected={this.onAddressSelected}
                    />,
                  )}
                </Form.Item>
                <Form.Item
                  {...formItemLayout}
                  style={{ marginBottom: 0, paddingBottom: 0 }}
                >
                  {getFieldDecorator('longitude', {
                    initialValue: longitude,
                  })(<Input type="hidden" placeholder="请在地图上标点" />)}
                </Form.Item>
                <Form.Item
                  {...formItemLayout}
                  style={{ marginBottom: 0, paddingBottom: 0 }}
                >
                  {getFieldDecorator('latitude', {
                    initialValue: latitude,
                  })(<Input type="hidden" placeholder="请在地图上标点" />)}
                </Form.Item>
                <Form.Item
                  {...formItemLayout}
                  style={{ marginBottom: 0, paddingBottom: 0 }}
                >
                  {getFieldDecorator('province', {
                    initialValue: province,
                  })(<Input type="hidden" />)}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={gutter}>
              <Col {...formLayoutCols3}>
                <Form.Item {...formItemLayout} label="备注">
                  {getFieldDecorator('remark', {
                    initialValue: remark,
                  })(<TextArea maxLength={500} placeholder="请输入备注" />)}
                </Form.Item>
              </Col>
              <Col span={8} />
            </Row>
          </FormEditCard>

          <FormEditCard title="货物信息">
            <Row gutter={gutter}>
              <Col {...formLayoutCols3}>
                <Form.Item {...formItemLayout} label="货物名称">
                  {getFieldDecorator('goodsName', {
                    initialValue: goodsName,
                    rules: [
                      {
                        required: true,
                        message: '请输入货物名称',
                      },
                      {
                        min: 2,
                        max: 200,
                        type: 'string',
                        message: '请输入2-200个字',
                      },
                    ],
                  })(<Input maxLength={200} placeholder="请输入货物名称" />)}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={gutter}>
              <Col {...formLayoutCols3}>
                <Form.Item {...formItemLayout} label="重量（千克）">
                  {getFieldDecorator('goodsWeight', {
                    initialValue: goodsWeight,
                    rules: [
                      {
                        required: true,
                        message: '请输入重量',
                      },
                      {
                        validator: checkNumberForm(
                          '请输入0-99999.99，支持两位小数',
                          {
                            max: 99999.99,
                            min: 0.01,
                            decimal: 2,
                          },
                        ),
                      },
                    ],
                  })(<Input maxLength={10} placeholder="请输入重量" />)}
                </Form.Item>
              </Col>
              <Col {...formLayoutCols3}>
                <Form.Item {...formItemLayout} label="体积（方）">
                  {getFieldDecorator('goodsVolume', {
                    initialValue: goodsVolume,
                    rules: [
                      {
                        required: true,
                        message: '请输入体积',
                      },
                      {
                        validator: checkNumberForm(
                          '请输入0-99.99，支持两位小数',
                          {
                            max: 99.99,
                            min: 0.01,
                            decimal: 2,
                          },
                        ),
                      },
                    ],
                  })(<Input maxLength={10} placeholder="请输入体积" />)}
                </Form.Item>
              </Col>
              <Col {...formLayoutCols3}>
                <Form.Item {...formItemLayout} label="件数（件）">
                  {getFieldDecorator('goodsNumber', {
                    initialValue: goodsNumber,
                    rules: [
                      {
                        required: true,
                        message: '请输入件数',
                      },
                      {
                        validator: checkNumberForm(
                          '请输入0-500，只能输入整数',
                          {
                            max: 500,
                            min: 1,
                            decimal: 0,
                          },
                        ),
                      },
                    ],
                  })(<Input maxLength={10} placeholder="请输入件数" />)}
                </Form.Item>
              </Col>
            </Row>
          </FormEditCard>

          <FormEditCard title="增值服务">
            <Row gutter={gutter}>
              <Col {...formLayoutCols3}>
                <Form.Item
                  {...formItemLayout}
                  label="回单"
                  style={{ marginBottom: 0, paddingBottom: 0 }}
                >
                  {getFieldDecorator('isReceipt', {
                    initialValue: isReceipt === '没有' ? 0 : 1,
                  })(
                    <RadioGroup>
                      <Radio value={1}>有</Radio>
                      <Radio value={0}>无</Radio>
                    </RadioGroup>,
                  )}
                </Form.Item>
              </Col>
              <Col {...formLayoutCols3}>
                <Form.Item
                  {...formItemLayout}
                  label="代收（元）"
                  style={{ marginBottom: 0, paddingBottom: 0 }}
                >
                  {getFieldDecorator('delegateAmount', {
                    initialValue: delegateAmount,
                    rules: [
                      {
                        required: false,
                        message: '请输入代收',
                      },
                      {
                        validator: checkNumberForm(
                          '请输入0-9999999.99，支持两位小数',
                          {
                            max: 9999999.99,
                            min: 0,
                            decimal: 2,
                          },
                        ),
                      },
                    ],
                  })(<Input maxLength={10} placeholder="请输入代收" />)}
                </Form.Item>
              </Col>
            </Row>
          </FormEditCard>

          <PageBottom
            rightChild={
              <PageBottomButtons
                buttons={[
                  <Button key={1} onClick={this.cancel}>
                    取消
                  </Button>,
                  <Button
                    key={2}
                    type="primary"
                    htmlType="submit"
                    loading={loading}
                  >
                    {searchParams && searchParams.id ? '保存' : '确认开单'}
                  </Button>,
                ]}
              />
            }
          />
        </Form>
      </div>
    );
  }
}

export default ({ routeId, actions }) => {
  const mapDispatchToProps = dispatch => ({
    getAsyncData: (param?: any) => dispatch(actions.getAsyncData(param)),
    insertPartOrder: (param?: any) => dispatch(actions.insertPartOrder(param)),
    updatePartOrder: (param?: any) => dispatch(actions.updatePartOrder(param)),
    getCustomerData: () => dispatch(actions.getCustomerData()),
    toList: (param?: string, url?: string[]) =>
      dispatch(actions.toList(param, url)),
  });

  const selectState = state => state.get(routeId);
  const globalState = state => state.get('global');
  const mapStateToProps = createStructuredSelector({
    error: makeSelectError(selectState),
    loading: makeSelectLoading(selectState),
    data: makeSelectData(selectState),
    insert: makeSelectInsert(selectState),
    customers: makeSelectCustomerData(selectState),
    routeIds: makeSelectRouteIds(globalState),
    // menuCollapsed: makeSelectMenuCollapsed(),
  });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  const form = Form.create()(LtlOrderEdit);

  return compose(withConnect)(form);
};
