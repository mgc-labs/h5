import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';
// API list
const API_GET_ALL_AUTHORIZATION =
  '/ehuodiGateway/utmsCore/utmsResourcecs/selectUtmsResourceList';
const API_GET_STAFF_DETAIL =
  '/ehuodiGateway/utmsCore/utmsUserscs/selectUtmsUsers?';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码

  mocker
    .on('post', API_GET_ALL_AUTHORIZATION)
    .reply(() =>
      import('modules/module.authorization/mock/resource').then(exports => [
        200,
        exports.default,
      ]),
    );
  mocker
    .on('post', API_GET_STAFF_DETAIL)
    .reply(() =>
      import('components/StaffAuthorizationSetting/mock/staffDetail').then(
        exports => [200, exports.default],
      ),
    );
}

export const getAllAuthorization = data => {
  return request(
    {
      method: 'post',
      url: API_GET_ALL_AUTHORIZATION,
      data: qs.stringify(data),
    },
    {
      useMock: USE_MOCK,
    },
  );
};

export const fetchStaffDetail = userName => {
  return request(
    {
      method: 'post',
      url: API_GET_STAFF_DETAIL,
      data: {
        userName,
      },
    },
    {
      useMock: USE_MOCK,
    },
  );
};
