/**
 * DateTimePicker Component
 * @author yanrong.tian
 * @date 2018/9/12 下午2:41:24
 */
import Tree from 'antd/es/tree';
import styles from 'components/StaffAuthorizationSetting/index.module.less';
import {
  fetchStaffDetail,
  getAllAuthorization,
} from 'components/StaffAuthorizationSetting/service';
import * as React from 'react';
import arrayToTree from 'utils/arrayToTree';

const TreeNode = Tree.TreeNode;

interface Istaff {
  userName: string | number;
  utmsUsersId: number;
  realName: string;
}

interface IProps {
  onCheck: (checkedKeys) => void;
  staff: [Istaff];
  versionsCode: string;
  getInitialKeys: (keys) => void;
}
interface IState {
  dataTree: any[];
  fetching: boolean;
  defaultExpandAll: boolean;
  checkedKeys: any[];
}

class StaffAuthorizationSetting extends React.PureComponent<IProps> {
  state: IState;
  staff: any[];
  constructor(props) {
    super(props);
    this.state = {
      dataTree: [],
      fetching: false,
      defaultExpandAll: false,
      checkedKeys: props.checkedKeys,
    };
  }

  public componentWillReceiveProps(nextProps) {
    if (nextProps.staff !== this.staff) {
      this.staff = this.props.staff;
      this.getStaffDetail(nextProps.staff);
    }
  }

  public componentDidMount(props) {
    this.getStaffDetail(this.props.staff);
    this.getData();
  }

  public render() {
    const { dataTree, checkedKeys } = this.state;
    if (!dataTree.length) {
      return <span />;
    }
    let childrens;
    const props = {
      ...this.props,
      defaultExpandAll: true,
    };

    if (dataTree && dataTree.length) {
      childrens = this.renderTreeNodes([
        {
          description: '',
          isDeleted: 0,
          operateKey: ';',
          orderNumber: 4036159,
          parentResourceId: 0,
          resourceIcon: ';',
          resourceName: '全部',
          resourceType: 0,
          resourceUrl: ';',
          utmsResourceId: 0,
          versionsCodes: 'P,Z',
          children: dataTree,
        },
      ]);
    }

    return (
      <Tree
        showLine
        checkable
        multiple
        {...props}
        className={styles.treeBox}
        checkedKeys={checkedKeys}
        onCheck={this.hanldCheck}
        checkStrictly={true}
      >
        {childrens}
      </Tree>
    );
  }

  private getStaffDetail = async staff => {
    if (staff && staff.length === 1) {
      const data = await fetchStaffDetail(staff[0].userName);
      let newCheckedKeys = data.data.utmsResourceIds || '';
      newCheckedKeys = newCheckedKeys.replace(/\[|\]/g, '');
      this.props.getInitialKeys(newCheckedKeys);
      newCheckedKeys = newCheckedKeys.indexOf(',')
        ? newCheckedKeys.split(',')
        : [newCheckedKeys];
      this.setState({
        checkedKeys: newCheckedKeys,
      });
      this.props.onCheck(newCheckedKeys);
    } else if (staff && staff.length > 1) {
      this.setState({
        checkedKeys: [],
      });
      this.props.onCheck([]);
    }
  };

  private hanldCheck = (checkedKeys, e) => {
    const checkedValues = [];
    (checkedKeys.checked || []).map(item => {
      if (item) {
        checkedValues.push(item);
      }
    });
    const currentNodeChild =
      e.node && e.node.props && e.node.props.dataRef.children
        ? e.node.props.dataRef.children
        : [];
    if (e.checked) {
      e.checkedNodes.map(node => {
        if (node.props && node.props.dataRef.parentKeys) {
          const parentKeys = node.props.dataRef.parentKeys;
          parentKeys.map(item => {
            if (item && !checkedValues.includes(item)) {
              checkedValues.push(item);
            }
          });
        }
      });
      // currentNodeChild.map(item => {
      //   if (item.children && item.children.length) {
      //     item.children.map(subItem => {
      //       if (subItem.utmsResourceId) {
      //         checkedValues.push(subItem.utmsResourceId + '');
      //       }
      //     });
      //   }
      //   if (item.utmsResourceId) {
      //     checkedValues.push(item.utmsResourceId + '');
      //   }
      // });
      this.selectCurrentNodeChild(currentNodeChild, checkedValues);
    } else {
      this.delCurrentNodeChild(currentNodeChild, checkedValues);
      // currentNodeChild.map(item => {
      //   if (item.children && item.children.length) {
      //     item.children.map(subItem => {
      //       checkedValues.map((value, index) => {
      //         if (value === subItem.utmsResourceId + '') {
      //           delete checkedValues[index];
      //         }
      //       });
      //     });
      //   }
      //   checkedValues.map((value, index) => {
      //     if (value === item.utmsResourceId + '') {
      //       delete checkedValues[index];
      //     }
      //   });
      // });
    }
    this.setState({
      checkedKeys: checkedValues,
    });
    this.props.onCheck(checkedValues);
  };

  private selectCurrentNodeChild = (data, checkedValues) => {
    data.map(item => {
      if (item.children && item.children.length) {
        item.children.map(subItem => {
          if (subItem.children && subItem.children.length) {
            this.selectCurrentNodeChild(subItem.children, checkedValues);
          }
          if (subItem.utmsResourceId) {
            checkedValues.push(subItem.utmsResourceId + '');
          }
        });
      }
      if (item.utmsResourceId) {
        checkedValues.push(item.utmsResourceId + '');
      }
    });
  };

  private delCurrentNodeChild = (data, checkedValues) => {
    data.map(item => {
      if (item.children && item.children.length) {
        item.children.map(subItem => {
          if (subItem.children && subItem.children.length) {
            this.delCurrentNodeChild(subItem.children, checkedValues);
          }
          checkedValues.map((value, index) => {
            if (value === subItem.utmsResourceId + '') {
              delete checkedValues[index];
            }
          });
        });
      }
      checkedValues.map((value, index) => {
        if (value === item.utmsResourceId + '') {
          delete checkedValues[index];
        }
      });
    });
  };

  private renderTreeNodes = data => {
    return data.map(item => {
      const isButton = item.resourceType === 1;
      const props = {
        title: item.resourceName,
        dataRef: item,
        className: isButton ? styles.treeButton : '',
        selectable: false,
      };
      if (item.children) {
        return (
          <TreeNode {...props} key={item.utmsResourceId}>
            {this.renderTreeNodes(item.children)}
          </TreeNode>
        );
      }

      return <TreeNode {...props} key={item.utmsResourceId} />;
    });
  };

  private parseData = (dataTree, parentKeys) => {
    dataTree.map(item => {
      if (item.parentResourceId) {
        item.parentKeys = (parentKeys || []).concat([
          item.parentResourceId + '',
        ]);
      }
      if (item.children) {
        this.parseData(item.children, item.parentKeys);
      }
    });
    return dataTree;
  };

  private getData = async () => {
    this.setState({
      fetching: true,
    });
    const { versionsCode } = this.props;
    try {
      const data = await getAllAuthorization({ versionsCode });
      const dataTree = arrayToTree(data.data || [], {
        parentProperty: 'parentResourceId',
        customID: 'utmsResourceId',
      });
      const newTree = this.parseData(dataTree, null);
      this.setState({
        dataTree: newTree,
        fetching: false,
      });
      setTimeout(() => {
        this.setState({
          defaultExpandAll: true,
        });
      }, 10000);
    } catch (err) {
      //
    }
  };
}

export default StaffAuthorizationSetting;
