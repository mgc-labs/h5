export default {
  result: 'success',
  data: [
    {
      vehicleType: '栏板车',
      vehicleLong: 4200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '栏板车',
      vehicleLong: 5200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '栏板车',
      vehicleLong: 6200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '厢式货车',
      vehicleLong: 4200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '栏板车',
      vehicleLong: 7200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '厢式货车',
      vehicleLong: 5200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '栏板车',
      vehicleLong: 8200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '厢式货车',
      vehicleLong: 7200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '厢式货车',
      vehicleLong: 8200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '中面',
      vehicleLong: 2700,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
    {
      vehicleType: '中面',
      vehicleLong: 8200,
      vehicleRealHeight: 10000,
      vehicleRealWidth: 2000,
      vehicleRealLong: 4200,
      vehicleVolume: 100,
      vehicleLoad: 10000,
    },
  ],
};
