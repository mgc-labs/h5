/**
 * CarModelCarLong Service
 * @author ggm
 * @date 2018-9-18 09:32:18
 */
import request, { mocker } from 'utils/request';
// import { USE_MOCK } from '../../CONFIG';
const USE_MOCK = false;

const API_GET_URL =
  '/ehuodiGateway/utmsDispatch/utmsVehicleTypeInfocs/selectUtmsVehicleTypeInfo';

if (process.env.NODE_ENV === 'development') {
  // mocker
  //   .on('post', API_GET_URL)
  //   .reply(() => import('./mock').then(exports => [200, exports.default]));
}
const fetchCarModelCarLong = opts => {
  return request(
    {
      method: 'post',
      url: API_GET_URL,
      data: opts,
    },
    {
      useMock: false,
    },
  ).then(res => res.data);
};
export default fetchCarModelCarLong;
