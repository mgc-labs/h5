/**
 * CarModelCarLong Component
 * @author ggm
 * @date 2018/9/17 上午10:06:10
 */
import Button from 'antd/es/button';
import Col from 'antd/es/col';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';

import Row from 'antd/es/row';
import Tag from 'antd/es/tag';
import * as React from 'react';

import styles from './index.module.less';
import fetchCarModelCarLong from './service';

const { CheckableTag } = Tag;
const HOT_CAR_TIT = '常选';

interface Ivalue {
  value: object | string;
  vehicleType?: number;
  vehicleLong?: number;
  vehicleLoad?: number;
  vehicleVolume?: number;
  vehicleRealLong?: number;
  vehicleRealWidth?: number;
  vehicleRealHeight?: number;
}

export interface ICarModelCarLongProps {
  value?: Ivalue[] | Ivalue;
  placeholder?: string;
  maxSelect?: number;
  disabled?: boolean;
  hotCar?: any;
  noHot?: boolean;
  style?: object;
  onChange?: (value: Ivalue[] | Ivalue) => any;
}

class CarModelCarLong extends React.PureComponent<ICarModelCarLongProps> {
  state = {
    data: [],
    value: [],
    show: false,
    noHover: false,
    placeholder: '请选择车型',
    selectTags: [],
    hotCar: ['4.2米厢式货车', '2.7米中面', '4.2米栏板车'],
  };
  componentWillReceiveProps(nextProps) {
    if ('value' in nextProps) {
      if (nextProps.value) {
        if (
          JSON.stringify(nextProps.value) !== JSON.stringify(this.state.value)
        ) {
          this.handleValue(nextProps.value);
          if (this.state.data && this.state.data.length > 0) {
            setTimeout(() => {
              this.setState((prevState: any, props) => {
                const data = prevState.data.reduce((total, item) => {
                  const list = item.list.reduce((t, i) => {
                    let isSelected = false;
                    for (const v in nextProps.value) {
                      if (nextProps.value[v] === i.value) {
                        isSelected = true;
                        break;
                      }
                    }
                    t.push({
                      ...i,
                      isSelected,
                    });
                    return t;
                  }, []);
                  total.push({
                    ...item,
                    list,
                  });
                  return total;
                }, []);
                return { data };
              });
              this.setDefaultSelectTags(this.state.data);
            }, 10);
          }
        }
      } else {
        this.setState((prevState: any, props) => {
          const data = prevState.data.reduce((total, item) => {
            const list = item.list.reduce((t, i) => {
              t.push({
                ...i,
                isSelected: false,
              });
              return t;
            }, []);
            total.push({
              ...item,
              list,
            });
            return total;
          }, []);
          return { data, selectTags: [] };
        });
        this.handleValue([]);
        this.setFieldsValue([], true);
      }
    }
  }
  componentDidMount() {
    if ('value' in this.props) {
      const value = this.props.value;
      if (value) {
        this.handleValue(value);
      }
    }
    this.fetch();
  }
  public handleValue(value) {
    if (value.length !== undefined) {
      this.setState({ value });
    } else {
      this.setState({ value: [value] });
    }
  }
  public setDefaultSelectTags(data) {
    const value = this.state.value;
    const selectTags = [];
    for (const i of value) {
      data.forEach(item => {
        if (i.value.indexOf(item.name) >= 0) {
          for (const e of item.list) {
            if (e.value === i.value) {
              e.isSelected = true;
              selectTags.push(e);
              break;
            }
          }
        }
      });
    }
    const { placeholder } = this.setFieldsValue(selectTags, true);
    this.setState({ data, selectTags, placeholder });
  }
  public setFieldsValue(value, first = false) {
    const onChange = this.props.onChange;
    let v = value;
    let placeholder = '';
    if (this.props.maxSelect && this.props.maxSelect === 1) {
      v = value[0];
      if (v) {
        v = { ...value[0] };
        delete v.refs;
        delete v.isSelected;
        v = [v];
      } else {
        v = [];
      }
    } else {
      v = v.reduce((total, item) => {
        const val = { ...item };
        delete val.refs;
        delete val.isSelected;
        total.push(val);
        return total;
      }, []);
    }
    if (!v || v.length <= 0) {
      placeholder = this.props.placeholder || '请选择车型';
    }
    if (first && (!v || v.length <= 0)) {
      return { placeholder };
    }
    if (onChange) {
      onChange(v);
    }
    return { placeholder };
  }
  public getHotCar(): any {
    if (this.props.noHot) {
      return null;
    }
    let hotCar = this.props.hotCar || this.state.hotCar;
    if (hotCar && hotCar.length > 0) {
      if (typeof hotCar === 'string') {
        hotCar = [hotCar];
      } else {
        hotCar = hotCar.map(item => {
          return {
            value: item,
          };
        });
      }
    } else {
      hotCar = null;
    }
    return hotCar;
  }
  public handleHotCar(hotCar, item, hotCarLen, hotT) {
    let hc = hotCar;
    if (hotCarLen > 0) {
      hc = hotCar.map((element, index) => {
        for (const v of this.state.value) {
          if (v.value === element.value) {
            element.isSelected = true;
            break;
          }
        }
        if (
          element.value === `${item.vehicleLong / 1000}米${item.vehicleType}`
        ) {
          item.refs = index + 1;
          element = { ...item, ...element };
          hotCarLen--;
          hotT.push(element);
        }
        return element;
      });
    }
    return { hc, ite: item, len: hotCarLen, hotT };
  }
  public fetchFormater(data) {
    let hotCar = this.getHotCar();
    let Thot = [];
    let hotCarLen = hotCar ? hotCar.length : 0;
    data = data.reduce((total, item) => {
      let flag = false;
      const { hc, ite, len, hotT } = this.handleHotCar(
        hotCar,
        item,
        hotCarLen,
        Thot,
      );
      hotCar = hc;
      Thot = hotT;
      hotCarLen = len;
      item = ite;
      for (const i of total) {
        if (i.name === item.vehicleType) {
          i.list.push({
            value: `${item.vehicleLong ? `${item.vehicleLong / 1000}米` : ''}${
              item.vehicleType
            }`,
            refs: item.refs,
            ...item,
          });
          flag = true;
          break;
        }
      }
      if (!flag) {
        total.push({
          name: item.vehicleType,
          list: [
            {
              value: `${
                item.vehicleLong ? `${item.vehicleLong / 1000}米` : ''
              }${item.vehicleType}`,
              refs: item.refs,
              ...item,
            },
          ],
        });
      }
      return total;
    }, []);
    if (Thot && Thot.length > 0) {
      data.unshift({
        name: HOT_CAR_TIT,
        list: Thot,
      });
    }
    return data;
  }
  public fetch(opts: object = {}) {
    fetchCarModelCarLong(opts).then(data => {
      data = this.fetchFormater(data);
      this.setState({
        data,
      });
      this.setDefaultSelectTags(data);
    });
  }
  public changeShowMultipeBox = b => {
    if (b) {
      this.setState({ show: b, noHover: true });
    } else {
      this.setState({ show: b });
    }
  };
  public changeItem = item => {
    if (!item.isSelected) {
      // 限制最大选择数
      if (
        this.props.maxSelect &&
        this.state.selectTags.length >= this.props.maxSelect
      ) {
        return;
      }
      this.setState((prevState: any, props) => {
        prevState.selectTags.push(item);
        const { placeholder } = this.setFieldsValue(prevState.selectTags);
        item.isSelected = !item.isSelected;
        const { pData } = this.handleCheckTagRefs(item, prevState.data, true);
        const noHover =
          prevState.selectTags.length >= this.props.maxSelect
            ? false
            : prevState.noHover;
        return {
          data: pData,
          selectTags: prevState.selectTags,
          placeholder,
          noHover,
        };
      });
    } else {
      this.closeTag(item);
    }
  };
  public closeTag = item => {
    this.setState((prevState: any, props) => {
      item.isSelected = !item.isSelected;
      const { pData, refItem } = this.handleCheckTagRefs(
        item,
        prevState.data,
        false,
      );
      let selectIndex = prevState.selectTags.indexOf(item);
      selectIndex =
        selectIndex < 0 ? prevState.selectTags.indexOf(refItem) : selectIndex;
      if (selectIndex >= 0) {
        prevState.selectTags.splice(selectIndex, 1);
      }
      const { placeholder } = this.setFieldsValue(prevState.selectTags);
      return {
        selectTags: prevState.selectTags,
        data: prevState.data,
        placeholder,
      };
    });
  };
  public handleCheckTagRefs(item, prevData, bool) {
    const pData = Object.assign([], prevData);
    let refItem = null;
    let findCount = 0;
    if (item.refs) {
      for (const data of pData) {
        data.list = data.list.map(i => {
          if (i.refs && i.refs === item.refs) {
            i.isSelected = bool;
            findCount++;
            if (i !== item) {
              refItem = i;
            }
          }
          return i;
        });
        if (findCount >= 2) {
          break;
        }
      }
    }
    return { pData, refItem };
  }
  render() {
    return (
      <div className={styles.CarModelCarLong} style={this.props.style}>
        <Section
          placeholder={this.state.placeholder}
          selectTags={this.state.selectTags}
          disabled={this.props.disabled}
          onClose={this.closeTag}
          changeShowMultipeBox={this.changeShowMultipeBox}
        />
        <MultipeBox
          show={this.state.show}
          noHover={this.state.noHover}
          data={this.state.data}
          changeItem={this.changeItem}
        />
      </div>
    );
  }
}
function Section(props) {
  function onClose(item, e) {
    props.onClose(item, e);
  }
  function changeShowMultipeBox(b) {
    props.changeShowMultipeBox(b);
  }
  const Tags = props.selectTags.map((e, i) => {
    return (
      <Tag
        key={e.value + i}
        closable
        onClose={onClose.bind(this, e)}
        className={styles.CarModelCarLong__Tag}
      >
        {e.value}
      </Tag>
    );
  });
  return (
    <div
      className={
        'CarModelCarLong__section' + ' ' + styles.CarModelCarLong__section
      }
    >
      <Input
        placeholder={props.placeholder}
        className={styles.CarModelCarLong__input}
        onFocus={changeShowMultipeBox.bind(this, true)}
        onBlur={changeShowMultipeBox.bind(this, false)}
        readOnly
        disabled={props.disabled}
      />
      <div className="ant-select-arrow">
        <Icon type="down" />
      </div>
      {Tags}
    </div>
  );
}
function MultipeBox(props) {
  const multipeBox = props.data.map((e, i) => {
    return (
      <div key={i} className={styles.CarModelCarLong__card}>
        <Row
          className={styles.CarModelCarLong__row}
          style={{ fontWeight: 'bold', paddingTop: '3px' }}
        >
          {e.name}
        </Row>
        <Row
          className={`${styles.CarModelCarLong__pl15} ${
            styles.CarModelCarLong__row
          } ${
            e.name === HOT_CAR_TIT ? styles.CarModelCarLong__rowOverHiden : ''
          }`}
        >
          <Col span={24}>
            <Item data={e} changeItem={props.changeItem} />
          </Col>
        </Row>
      </div>
    );
  });
  return (
    <div
      className={`${styles.CarModelCarLong__MultipeBox} ${
        props.show ? styles.CarModelCarLong__MultipeBox_show : ''
      } ${props.noHover ? '' : styles.CarModelCarLong__MultipeBox_nohover}`}
    >
      {multipeBox}
    </div>
  );
}
function Item(props) {
  function changeItem(item) {
    props.changeItem(item);
  }
  const Li = props.data.list.map(item => {
    return (
      <CheckableTag
        key={item.value + props.data.name}
        checked={item.isSelected}
        className={styles.CarModelCarLong__checkableTag}
        onChange={changeItem.bind(this, item)}
      >
        {item.value}
      </CheckableTag>
    );
  });
  return Li;
}
export default CarModelCarLong;
