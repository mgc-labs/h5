export const ErrorBoundary: string;
export const ErrorBoundary__Figure: string;
export const ErrorBoundary__Content: string;
export const ErrorBoundary__Title: string;
export const ErrorBoundary__Explain: string;
export const ErrorBoundary__ErrorDetail: string;
export const ErrorBoundary__ErrorStack: string;
