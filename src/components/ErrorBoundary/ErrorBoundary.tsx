/**
 * ErrorBoundary Component
 * @author ryan bian
 * @date 2018-9-30 15:44:06
 */
import Button from 'antd/es/button';
import Drawer from 'antd/es/drawer';
import * as React from 'react';

import figureImage from './assets/figure.jpg';
import styles from './index.module.less';

interface IErrorBoundaryProps {}
interface IErrorBoundaryState {
  hasError: boolean;
  error: object;
  errorInfo: object;
  showErrorDetail: boolean;
}

export default class ErrorBoundary extends React.PureComponent<
  IErrorBoundaryProps,
  IErrorBoundaryState
> {
  public state = {
    hasError: false,
    error: undefined,
    errorInfo: undefined,
    showErrorDetail: false,
  };
  public componentDidCatch(error, info) {
    this.setState({
      hasError: true,
      error,
      errorInfo: info,
    });
  }
  public handleShowDetail = () => {
    this.setState({
      showErrorDetail: true,
    });
  };
  public handleClose = () => {
    this.setState({
      showErrorDetail: false,
    });
  };
  public render() {
    const { children } = this.props;
    const { hasError, error, errorInfo, showErrorDetail } = this.state;
    if (hasError) {
      return (
        <div className={styles.ErrorBoundary}>
          <figure className={styles.ErrorBoundary__Figure}>
            <img src={figureImage} alt="" />
          </figure>
          <div className={styles.ErrorBoundary__Content}>
            <h1 className={styles.ErrorBoundary__Title}>啊哦，页面出错了！</h1>
            <div className={styles.ErrorBoundary__Explain}>
              不慌，我们最牛逼的工程师正在着手解决这个问题。
            </div>
            <Button onClick={this.handleShowDetail}>到底发生了什么</Button>
          </div>
          <Drawer
            title={'绝密档案'}
            placement="right"
            width={500}
            visible={showErrorDetail}
            onClose={this.handleClose}
          >
            <div className={styles.ErrorBoundary__ErrorDetail}>
              {error.toString()}
            </div>
            <div className={styles.ErrorBoundary__ErrorStack}>
              {errorInfo.componentStack}
            </div>
          </Drawer>
        </div>
      );
    }
    return children;
  }
}
