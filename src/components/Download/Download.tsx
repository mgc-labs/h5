/**
 * Download Component
 * @author hefan
 * @date 2018/9/20 上午9:23:49
 */
import Button from 'antd/es/button';
import * as React from 'react';

import DownloadForm from './DownloadForm';

import styles from './index.module.less';

export interface IDownloadProps {
  btnType?: string;
  url: string;
  params: {};
  method?: string;
  children?: string;
  className?: string;
  style?: {};
}

class Download extends React.PureComponent<IDownloadProps> {
  static defaultProps = {
    btnType: 'primary',
    method: 'post',
  };

  private child: HTMLElement;

  onRef = ref => {
    this.child = ref;
  };

  public render() {
    const { btnType, params, method, url, children, ...props } = this.props;

    return (
      <div {...props}>
        <DownloadForm
          url={url}
          params={params}
          method={method}
          onRef={this.onRef}
        />
        <Button
          icon="download"
          type={btnType}
          htmlType="submit"
          onClick={this.submitHandle}
        >
          {children}
        </Button>
      </div>
    );
  }

  private submitHandle = () => {
    this.child.submit();
  };
}

export default Download;
