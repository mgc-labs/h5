import * as React from 'react';
import { createPortal } from 'react-dom';

export interface IFormDetailCardProps {
  url?: string;
  params?: {};
  method?: string;
  onRef?: (ref: HTMLElement) => void;
}

class DownloadForm extends React.PureComponent<IFormDetailCardProps> {
  private node: HTMLElement;
  componentDidMount() {
    this.props.onRef(this.node);
  }
  render() {
    const doc = window.document;
    // const node = doc.createElement('div');
    // node.className = 'download-form-wrap';
    // doc.body.appendChild(node);
    const { params, method, url } = this.props;

    return createPortal(
      <form
        action={url}
        method={method}
        ref={node => (this.node = node)}
        target="download_form_iframe"
      >
        {params &&
          Object.keys(params).map(
            (key, index) =>
              params[key] === '' ||
              params[key] === undefined ||
              params[key] === null ? null : (
                <input
                  type="hidden"
                  name={key}
                  value={params[key]}
                  key={`download_form_${index}`}
                />
              ),
          )}
        <iframe name="download_form_iframe" style={{ display: 'none' }} />
      </form>,
      doc.body,
    );
  }
}

export default DownloadForm;
