/**
 * InputBox Component
 * @author djd
 * @date 2018/10/12 下午3:16:13
 * 文本框，右边显示最多允许字符个数及已填字符个数
 */

import Input, { InputProps } from 'antd/es/input';
import * as React from 'react';
import styles from './styles.module.less';

export class InputBox extends React.PureComponent<InputProps, any> {
  render() {
    const { value, maxLength } = this.props;
    const v = value as string;

    const suffix = `${v.length}/${maxLength}`;
    return (
      <Input {...this.props} suffix={suffix} className={styles.inputBox} />
    );
  }
}
