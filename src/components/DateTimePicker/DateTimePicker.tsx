/**
 * DateTimePicker Component
 * @author djd
 * @date 2018/9/12 下午2:41:24
 */
import * as React from 'react';

import DatePicker from 'antd/es/date-picker';

import styles from './index.module.less';

import moment, { Moment } from 'moment';

interface IDateTimePickerProps {
  value?: string;
  onChange?: (dateString: string) => void;
  placeholder: string;
}

class DateTimePicker extends React.PureComponent<IDateTimePickerProps, any> {
  onChange = (value: Moment) => {
    if (this.props.onChange) {
      if (value) {
        this.props.onChange(value.format('YYYY-MM-DD HH:mm'));
      } else {
        this.props.onChange('');
      }
    }
  };

  render() {
    const { placeholder, value } = this.props;
    const dateValue = value ? moment(value, 'YYYY-MM-DD HH:mm') : null;

    return (
      <div className={styles.DateTimePicker}>
        <DatePicker
          showTime={{ format: 'HH:mm' }}
          format="YYYY-MM-DD HH:mm"
          placeholder={placeholder}
          value={dateValue}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

export default DateTimePicker;
