/**
 * Auth Service
 * @author yanrong.tian
 */
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

const API_GET_ADDRESS = '/ehuodiGateway/geoService/v1.0/poi/associatePoi';

export const getAddress = options => {
  return request(
    {
      method: 'post',
      url: API_GET_ADDRESS,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      params: {
        keywords: options.keyword,
        dataSource: 'UTMS_' + options.flag,
      },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);
};
