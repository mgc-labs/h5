/**
 * AddressSelect Component
 * @author djd
 * @date 2018/9/12 下午3:16:13
 */
import * as React from 'react';

import Select from 'antd/es/select';
import { connect } from 'react-redux';

import styles from './index.module.less';

import MapPointPicker from 'components/MapPointPicker';
import { createStructuredSelector } from 'reselect';
import { makeUserDate } from './selectors';
import { getAddress } from './service';

import { debounce } from 'lodash';

export interface IAddressSelectResult {
  lng: number;
  lat: number;
  address: string;
  province: string;
  city: string;
  region: string;
}

export interface IAddressSelectProps {
  value?: string;
  placeholder: string;
  onChange?: (address: string) => void;
  onSelected?: (result: IAddressSelectResult) => void;
  style?: object;
  userDate: object;
}

class AddressSelectComponent extends React.PureComponent<IAddressSelectProps, any> {
  isSetWidth: any;

  constructor(props: IAddressSelectProps, context: any) {
    super(props, context);

    this.state = {
      addressList: [],
      showPointPicker: false,
    };
  }

  setSelectUlWidth = obj => {
    if (!obj || this.isSetWidth) {
      return;
    }
    if ((obj.className || '').indexOf('ant-select-dropdown ') >= 0) {
      this.isSetWidth = true;
      obj.style.minWidth = '315px';
    } else {
      this.setSelectUlWidth(obj.parentNode);
    }
  };

  render() {
    const { showPointPicker, addressList = [] } = this.state;
    const { value } = this.props;
    return (
      <div>
        <Select
          showSearch
          value={value === '' ? undefined : value}
          placeholder={this.props.placeholder}
          defaultActiveFirstOption={false}
          showArrow={true}
          filterOption={false}
          onSearch={this.handleSearch}
          notFoundContent={null}
          allowClear={true}
          onChange={this.handleChange}
          style={this.props.style}
          onFocus={value => {
            this.setState({
              inputSearchText: value,
            });
          }}
        >
          <Select.Option value={null} disabled>
            <div
              className={styles.btnLine}
              ref={obj => {
                this.setSelectUlWidth(obj);
              }}
            >
              <span className={styles.info}>没有找到位置，请通过地图建立</span>
              <a
                href="javascript:void(0)"
                onClick={this.onOpenMapPointPicker}
                className={styles.newBtn}
              >
                新增位置
              </a>
            </div>
          </Select.Option>

          {addressList.map((item, index) => (
            <Select.Option
              key={index}
              value={index}
              className={styles.addressOption}
            >
              {item.address}
              <span>{item.city}</span>
            </Select.Option>
          ))}
        </Select>

        {showPointPicker && (
          <MapPointPicker
            onOk={this.onPointPicked}
            onClose={this.onMapPointClose}
            address={this.state.inputSearchText}
          />
        )}
      </div>
    );
  }

  private getFlag = () => {
    const { userDate } = this.props;
    try {
      return userDate.get('domainName');
    } catch (err) {
      return '';
    }
  };

  private onMapPointClose = () => {
    this.setState({
      showPointPicker: false,
    });
  };

  private onPointPicked = result => {
    const { onSelected, onChange } = this.props;
    if (onSelected) {
      onSelected({
        lng: result.lng,
        lat: result.lat,
        address: result.address,
        province: result.province,
        city: result.city,
        region: result.region,
      });
    }
    if (onChange) {
      onChange(result.address);
    }
  };

  private onOpenMapPointPicker = () => {
    this.setState({
      showPointPicker: true,
    });
  };

  private handleChange = (index: number) => {
    const { onSelected, onChange } = this.props;
    if (onSelected) {
      if (index === undefined) {
        // 点了叉叉
        onSelected();
      } else {
        const value = this.state.addressList[index];
        onSelected({
          lng: value.lng,
          lat: value.lat,
          address: value.address,
          province: value.province,
          city: value.city,
          region: value.region,
        });
      }
    }
    if (onChange) {
      if (index === undefined) {
        // 点了叉叉
        onChange('');
      } else {
        const value = this.state.addressList[index];
        onChange(value.address);
      }
    }
  };

  private handleSearch = (text: string) => {
    text = text.replace(/(^\s+)|(\s+$)/g, '');
    this.setState({
      inputSearchText: text,
    });
    this.onFetch(text);
  };

  private onFetch = async keyword => {
    if (!keyword) {
      return;
    }
    try {
      const data = await getAddress({
        keyword,
        flag: this.getFlag(),
      });
      const newData = [];
      (data.data || []).map(item => {
        if (item.address) {
          newData.push({
            ...item,
            address:
              item.name &&
              item.name !== item.address &&
              !item.name.match(/\（|\(/) !== item.address
                ? `${item.name} (${item.address})`
                : item.address,
          });
        }
      });
      this.setState({
        addressList: newData,
      });
    } catch (err) {
      //
    }
  };
}

const mapStateToProps = createStructuredSelector({
  userDate: makeUserDate(),
});

export const AddressSelect = connect(
  mapStateToProps,
  () => ({}),
)(AddressSelectComponent);
