/**
 * SiderCompanyTree Component
 * @author HuangSiFei
 * @date 2018-9-20 09:42:01
 */
import Card from 'antd/es/card';
import Icon from 'antd/es/icon';
import Layout from 'antd/es/layout';
import Tree from 'antd/es/tree';
import classnames from 'classnames';
import { List, Map } from 'immutable';
import * as React from 'react';
import './index.less';

import styles from './index.module.less';

const { Sider } = Layout;
const { TreeNode } = Tree;

interface ISiderCompanyTreeProps {
  companyTree: List<Map<string, any>>;
  selectedKeys?: List<any>;
  handleSelect: (value) => void;
  moveNode?: (nodeId) => void;
}

interface ISiderCompanyTreeStates {
  selectedKeys?: List<any>;
}

export default class SiderCompanyTree extends React.PureComponent<
  ISiderCompanyTreeProps
> {
  public onSelect = selectedKeys => {
    if (selectedKeys.length < 1) {
      return;
    }
    const { handleSelect, companyTree } = this.props;
    const selected = companyTree
      .filter(company => {
        return company.get('utmsOrganizationId') === Number(selectedKeys[0]);
      })
      .toArray();
    handleSelect(selected[0] || {});
  };

  public handleMoveNode = async (direction, event, index, id) => {
    event.stopPropagation();
    if (
      (direction === 'up' && index === 'first') ||
      (direction === 'down' && index === 'last')
    ) {
      return;
    }
    this.props.moveNode(id);
  };

  public renderTitle = (company, index, companyList) => {
    const isFirst = index === 0 ? 'first' : 'other';
    const isLast = index === companyList.length - 1 ? 'last' : 'other';
    const siblingIdUp =
      company.key +
      ',' +
      (isFirst === 'first' ? '' : companyList[index - 1].key);
    const siblingIdDown =
      company.key + ',' + (isLast === 'last' ? '' : companyList[index + 1].key);
    return (
      <React.Fragment>
        <span>{company.title}</span>
        <div
          className={styles.iconWarpper}
          style={{ display: this.props.moveNode ? '' : 'none' }}
        >
          <Icon
            type="caret-up"
            className={classnames(
              styles[`icon_caret_up__${isFirst}`],
              styles.icon_caret,
            )}
            onClick={e => this.handleMoveNode('up', e, isFirst, siblingIdUp)}
          />
          <Icon
            type="caret-down"
            className={classnames(
              styles[`icon_caret_down__${isLast}`],
              styles.icon_caret,
            )}
            onClick={e => this.handleMoveNode('down', e, isLast, siblingIdDown)}
          />
        </div>
      </React.Fragment>
    );
  };

  public renderTreeNode() {
    const { companyTree } = this.props;
    const branch = companyTree
      ? companyTree
          .filter(company => {
            return company.get('organizationType') !== '总公司';
          })
          .map(company => ({
            title: company.get('organizationName'),
            key: company.get('utmsOrganizationId'),
          }))
          .toArray()
      : [];
    return branch
      ? branch.map((company, index, all) => {
          return (
            <TreeNode
              key={company.key}
              title={this.renderTitle(company, index, all)}
              className={styles.treeNode}
            />
          );
        })
      : [];
  }

  public renderSider() {
    const { companyTree, selectedKeys } = this.props;
    const headQuarter = companyTree
      ? companyTree
          .filter(company => {
            return company.get('organizationType') === '总公司';
          })
          .map(company => ({
            title: company.get('organizationName'),
            key: company.get('utmsOrganizationId'),
          }))
          .toArray()
      : [];
    const treeProps = {
      onSelect: this.onSelect,
      expandedKeys: [(headQuarter[0] && headQuarter[0].key) + ''],
      // selectedKeys: (selectedKeys && selectedKeys.toArray()) || [],
    };
    if (selectedKeys && selectedKeys.toArray()) {
      treeProps.selectedKeys = selectedKeys && selectedKeys.toArray();
    }
    return (
      <Tree {...treeProps} className={'company-tree'}>
        <TreeNode {...headQuarter[0]} className={styles.treeRoot}>
          {this.renderTreeNode()}
        </TreeNode>
      </Tree>
    );
  }
  public render() {
    return (
      <Sider style={{ backgroundColor: '#fff' }} width={200}>
        <Card
          style={{
            borderRight: 'none',
            height: '100%',
            borderRadius: '0',
          }}
        >
          {this.renderSider()}
        </Card>
      </Sider>
    );
  }
}
