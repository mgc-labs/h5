/**
 * DriverPicker Service
 * @author ryan bian
 * @date 2018-9-17 16:32:18
 */
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

const API_GET_CUSTOMER =
  '/ehuodiGateway/utmsCore/utmsCustomercs/selectCustomerList';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', `${API_GET_CUSTOMER}?mock=true`)
    .reply(() => import('./customer').then(exports => [200, exports.default]));
}

const fetch = keyword => {
  return request(
    {
      method: 'post',
      url: API_GET_CUSTOMER + (USE_MOCK ? '?mock=true' : ''),
      data: {
        keyword,
        pageSize: 20,
      },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);
};

export default fetch;
