/**
 * CustomerSelect Component
 * @author djd
 * @date 2018/9/12 下午1:34:39
 */
import Select from 'antd/es/select';
import Spin from 'antd/es/spin';
import { debounce } from 'lodash';
import * as React from 'react';
import fetchCustomer from './service';

import styles from './index.module.less';

export interface ICustomerSelectProps {
  placeholder?: string;
  notFoundContent?: string;
  disabled?: boolean;
  defaultValueFields?: string;
  showDefaultValue?: boolean; // 是否默认显示第一个客户,默认展示
  onChange?: (data) => void;
  renderOption?: (item: any) => JSX.Element;
}

interface ICustomerSelectState {
  data: object[];
  pending: boolean;
  isSetCustomer: boolean;
  defaultValue: string;
}

class CustomerSelect extends React.PureComponent<
  ICustomerSelectProps,
  ICustomerSelectState
> {
  static defaultProps = {
    placeholder: '请输入客户名称或联系方式查询',
    notFoundContent: '没有找到匹配的客户',
    disabled: false,
    showDefaultValue: true,
    defaultValueFields: 'customerCode,customerName',
  };
  static renderOption(item) {
    return (
      <Select.Option
        key={item.utmsCustomerId}
        value={`${item.customerCode},${item.customerName}`}
      >
        {item.customerName}
      </Select.Option>
    );
  }
  state = {
    data: [],
    pending: false,
    isSetCustomer: false,
    defaultValue: '',
  };
  lastFetchId = 0;
  public fetch = debounce(keyword => {
    this.lastFetchId += 1;
    const fetchId = this.lastFetchId;
    this.setState({
      data: [],
      pending: true,
    });
    fetchCustomer(keyword)
      .then(data => {
        if (fetchId !== this.lastFetchId) {
          // for fetch callback order
          return;
        }
        this.setState({
          data,
          pending: false,
        });
        if (
          !this.state.isSetCustomer &&
          !this.props.value &&
          this.props.showDefaultValue
        ) {
          this.setCustomer(data);
        }
      })
      .catch(() => {
        this.setState({
          pending: false,
        });
      });
  }, 500);

  constructor(props) {
    super(props);
    this.fetch('');
  }

  public setCustomer(data) {
    const fields = this.props.defaultValueFields.split(',');
    let defaultvalue = '';
    fields.map((key, index) => {
      return index === 0
        ? (defaultvalue += data[0][key])
        : (defaultvalue += ',' + data[0][key]);
    });
    this.setState({
      isSetCustomer: true,
      defaultValue: defaultvalue,
    });
    this.props.onChange(defaultvalue);
  }

  public onChange = data => {
    this.setState({
      defaultValue: data,
    });
    this.props.onChange(data);
  };

  public render() {
    const {
      placeholder,
      notFoundContent,
      renderOption,
      disabled,
      value,
      ...otherProps
    } = this.props;
    const { data, pending, defaultValue } = this.state;
    const props = {
      placeholder,
      disabled,
      showSearch: true,
      onSearch: this.fetch,
      notFoundContent: pending ? <Spin /> : notFoundContent,
      filterOption: false,
      onChange: v => {
        this.onChange(v);
      },
    };
    return (
      <Select {...otherProps} {...props} value={value || defaultValue}>
        {data.map(renderOption || CustomerSelect.renderOption)}
      </Select>
    );
  }
}

export default CustomerSelect;
