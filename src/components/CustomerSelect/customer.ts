export default {
  result: 'success',
  data: [
    {
      utmsCustomerId: '001',
      customerCode: '001',
      customerName: '史润龙',
      contact: '史润龙本人',
      contactWay: 15812345678,
    },
    {
      utmsCustomerId: '002',
      customerCode: '002',
      customerName: '刘强东',
      contact: '章泽天',
      contactWay: 15898765432,
    },
  ],
};
