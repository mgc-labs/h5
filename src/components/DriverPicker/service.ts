/**
 * DriverPicker Service
 * @author ryan bian
 * @date 2018-9-17 16:32:18
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

const API_GET_DRIVER =
  '/ehuodiGateway/utmsDispatch/utmsDrivercs/selectDriverListByCondition';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  // mocker
  //   .on('post', API_GET_DRIVER)
  //   .reply(() =>
  //     import('./mock').then(exports => [200, exports.default]),
  //   );
}

const fetchDriver = (keyword, options) => {
  return request(
    {
      method: 'post',
      url: API_GET_DRIVER,
      data: qs.stringify({
        ...options,
        searchInfo: keyword,
        pageSize: 20,
        skipCount: 0,
      }),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res.data);
};

export default fetchDriver;
