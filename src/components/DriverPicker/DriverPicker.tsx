/**
 * DriverPicker Component
 * @author ryan bian
 * @date 2018-9-17 16:32:18
 */
import Select from 'antd/es/select';
import Spin from 'antd/es/spin';
import { debounce } from 'lodash';
import * as React from 'react';
import fetchDriver from './service';

const { Option } = Select;

export interface IDriverPickerProps {
  placeholder?: string;
  notFoundContent?: string;
  bindingStatus?: 0 | 1 | null | undefined; // 绑定状态。绑定：1、未绑定：0。不传查询全部
  renderOption?: (item: any) => JSX.Element;
}
interface IDriverPickerState {
  data: object[];
  pending: boolean;
}

export default class DriverPicker extends React.PureComponent<
  IDriverPickerProps,
  IDriverPickerState
> {
  static defaultProps = {
    placeholder: '请选择司机',
    notFoundContent: '没有找到匹配的司机',
    bindingStatus: null,
  };
  static renderOption(item) {
    return (
      <Option key={item.utmsUsersDriversId} value={item.utmsUsersDriversId}>
        {item.driverName}-{item.plateNumber}
      </Option>
    );
  }
  state = {
    data: [],
    pending: false,
  };
  lastFetchId = 0;
  public fetch = debounce(keyword => {
    const { bindingStatus } = this.props;
    const fetchOptions = {};
    if (typeof bindingStatus === 'number') {
      Object.assign(fetchOptions, {
        bindingStatus,
      });
    }
    this.lastFetchId += 1;
    const fetchId = this.lastFetchId;
    this.setState({
      data: [],
      pending: true,
    });
    fetchDriver(keyword, fetchOptions)
      .then(data => {
        if (fetchId !== this.lastFetchId) {
          // for fetch callback order
          return;
        }
        this.setState({
          data,
          pending: false,
        });
      })
      .catch(() => {
        this.setState({
          pending: false,
        });
      });
  }, 500);
  public render() {
    const {
      placeholder,
      notFoundContent,
      renderOption,
      ...otherProps
    } = this.props;
    const { data, pending } = this.state;
    const props = {
      // className: styles.DriverPicker,
      placeholder,
      showSearch: true,
      onSearch: this.fetch,
      notFoundContent: pending ? <Spin /> : notFoundContent,
      filterOption: false,
    };
    return (
      <Select {...otherProps} {...props}>
        {data.map(renderOption || DriverPicker.renderOption)}
      </Select>
    );
  }
}
