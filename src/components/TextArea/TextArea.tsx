/**
 * TextArea Component
 * @author djd
 * @date 2018/9/14 上午10:10:00
 */
import * as React from 'react';

import styles from './index.module.less';

import Input, { TextAreaProps } from 'antd/es/input';

export interface ITextAreaProps extends TextAreaProps {}

class TextArea extends React.PureComponent<ITextAreaProps, any> {
  render() {
    const { value } = this.props;
    return (
      <div className={styles.TextArea}>
        <Input.TextArea autosize={{ minRows: 3 }} {...this.props} />
        <span className={styles.wordsInfo}>
          {((value as string) || '').length} / {this.props.maxLength}
        </span>
      </div>
    );
  }
}

export default TextArea;
