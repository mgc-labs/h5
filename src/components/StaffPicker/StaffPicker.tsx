/**
 * DateTimePicker Component
 * @author yanrong.tian
 * @date 2018/9/12 下午2:41:24
 */
import {
  AutoComplete,
  Button,
  Cascader,
  Checkbox,
  Col,
  Form,
  Icon,
  Input,
  Modal,
  Row,
  Select,
  Spin,
  Tooltip,
} from 'antd';
import { getStaffsHttp } from 'components/StaffPicker/service';
import * as React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styles from './index.module.less';
import { makeUserDate } from './selectors';

const FormItem = Form.Item;
const Option = Select.Option;

interface IProps {
  onChange?: (
    value: {
      key: string | number;
      label: any[];
    },
  ) => void;
}
interface IState {
  fetching: boolean;
  staffsData: any[];
  value?: any[];
}

class StaffPickerComponent extends React.PureComponent<IProps> {
  state: IState;
  constructor(props) {
    super(props);
    this.state = {
      staffsData: [],
      fetching: false,
    };
  }

  public render() {
    const { fetching, staffsData, value } = this.state;
    const props: any = {
      ...{
        mode: 'multiple',
        style: { width: '100%' },
        placeholder: '输入员工姓名模糊检索，多选',
        notFoundContent: fetching ? <Spin size="small" /> : null,
        onChange: this.handleChange,
        onSearch: this.getStaffs,
        filterOption: false,
      },
      ...this.props,
      labelInValue: true,
    };
    props.onChange = this.handleChange;
    if (!props.value) {
      props.value = [];
    }
    const isDisplayMaxErr =
      this.state.value && this.state.value.length >= this.props.maxLength;
    return (
      <Select {...props} className={styles.selectOption}>
        <Option key="a0000" style={{ display: isDisplayMaxErr ? '' : 'none' }}>
          <span style={{ color: '#ff0000' }}>已达到上限，请删除后再添加！</span>
        </Option>
        {staffsData.map(d => (
          <Option key={d.userName}>
            {d.realName}
            <span
              style={{
                float: 'right',
                fontSize: '12px',
                color: '#999',
                marginRight: '20px',
                width: '4em',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textAlign: 'right',
              }}
            >
              {d.departmentNames}
            </span>
          </Option>
        ))}
      </Select>
    );
  }

  private getStaffs = async (keywords = '') => {
    if (this.state.value && this.state.value.length >= this.props.maxLength) {
      return;
    }
    keywords = keywords.replace(/(^\s*)|(\s*$)/g, '');
    if (!keywords) {
      return;
    }
    this.setState({
      staffsData: [],
      fetching: true,
    });
    const data = await getStaffsHttp({
      keywords,
      organizationCode: this.props.userDate.get('organizationCode'),
    });
    this.setState({
      staffsData: data.data,
      fetching: false,
    });
  };

  private handleChange = value => {
    if (!this.state.value && !value) {
      return;
    }
    this.setState({
      value,
      staffsData: [],
      fetching: false,
    });
    if (this.props.onChange) {
      this.props.onChange(value);
    }
  };
}

const mapStateToProps = createStructuredSelector({
  userDate: makeUserDate(),
});

export const StaffPicker = connect(
  mapStateToProps,
  () => ({}),
)(StaffPickerComponent);
