/**
 * Auth Service
 * @author yanrong.tian
 */
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_CUSTOMERS = '/ehuodiGateway/utmsCore/utmsUserscs/selectUtmsUsersList';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_CUSTOMERS)
    .reply(() =>
      import('./mock/staffs').then(exports => [200, exports.default]),
    );
}

/**
 * 获取客户列表
 */
export const getStaffsHttp = options =>
  request(
    {
      method: 'post',
      url: API_CUSTOMERS,
      data: {
        ...options,
        status: 1,
      },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);
