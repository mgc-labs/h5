/**
 * FormEditCard Component
 * @author djd
 * @date 2018/9/14 下午3:29:18
 */
import * as React from 'react';

import Card from 'antd/es/card';

import styles from './index.module.less';

export interface IFormEditCardProps {
  title: string;
  children?: React.ReactNode;
}

const FormEditCard = (props: IFormEditCardProps) => (
  <div className={styles.FormEditCard}>
    <Card title={props.title} bordered={true} style={{ width: '100%' }}>
      {props.children}
    </Card>
  </div>
);

export default FormEditCard;

export const formLayoutCols3 = {
  lg: { span: 8 },
  md: { span: 12 },
  xs: { span: 24 },
};

export const formLayoutCols2 = {
  lg: { span: 12 },
  md: { span: 12 },
  xs: { span: 24 },
};

export const formLayoutCols1 = {
  lg: { span: 24 },
  md: { span: 24 },
  xs: { span: 24 },
};

export const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
  },
  wrapperCol: {
    xs: { span: 24 },
  },
};

export const gutter = 16;
