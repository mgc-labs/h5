export {
  default,
  formLayoutCols1,
  formLayoutCols2,
  formLayoutCols3,
  formItemLayout,
  gutter,
} from './FormEditCard';
