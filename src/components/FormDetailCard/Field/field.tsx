/**
 * FormDetailField Component
 * @author djd
 * @date 2018/9/14 下午2:28:02
 */
import * as React from 'react';

import styles from './index.module.less';

export interface IFormDetailFieldProps {
  label: string;
  value: string;
  style?: object;
}

const FormDetailField = (props: IFormDetailFieldProps) => (
  <div className={styles.FormDetailField} style={props.style}>
    <div className={styles.label}>{props.label}</div>：
    <div className={styles.value}>{props.value}</div>
  </div>
);

export default FormDetailField;
