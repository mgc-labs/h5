/**
 * FormDetailCard Component
 * @author djd
 * @date 2018/9/14 下午3:05:36
 */
import * as React from 'react';

import Card from 'antd/es/card';

import styles from './index.module.less';

export interface IFormDetailCardProps {
  title: string;
  children?: React.ReactNode;
}

export const FormDetailCard = (props: IFormDetailCardProps) => (
  <div className={styles.FormDetailCard}>
    <Card title={props.title} bordered={true} style={{ width: '100%' }}>
      {props.children}
    </Card>
  </div>
);

export const formLayoutCols3 = {
  lg: { span: 8 },
  md: { span: 12 },
  xs: { span: 24 },
};

export const formLayoutCols2 = {
  lg: { span: 12 },
  md: { span: 12 },
  xs: { span: 24 },
};

export const formLayoutCols1 = {
  lg: { span: 24 },
  md: { span: 24 },
  xs: { span: 24 },
};

export const gutter = 16;
