import FormDetailField from './Field';

export {
  FormDetailCard,
  formLayoutCols1,
  formLayoutCols2,
  formLayoutCols3,
  gutter,
} from './FormDetailCard';

export { FormDetailField };
