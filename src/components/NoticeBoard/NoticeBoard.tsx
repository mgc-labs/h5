/**
 * NoticeBoard Component
 * @author hefan
 * @date 2018/9/13 下午3:49:56
 */
import * as React from 'react';
import { Link } from 'react-router-dom';

import Card from 'antd/es/card';
import Carousel from 'antd/es/carousel';
import Icon from 'antd/es/icon';

import styles from './index.module.less';

export interface INoticeBoardProps {
  dataSource: string[];
  length: number;
  headStyle?: {};
  bodyStyle?: {};
  boardTitle: string | React.ReactNode;
  listStyle?: {};
}

class NoticeBoard extends React.PureComponent<INoticeBoardProps> {
  static defaultProps = {
    length: 3,
  };

  public getCarouselItem() {
    const { dataSource, length, listStyle } = this.props;
    if (dataSource && dataSource.length) {
      const divList = [];
      for (let i = 0; i < length; i++) {
        if (dataSource[i]) {
          divList.push(
            <Link
              to="/company/proclamation"
              key={`noticeboard_div_${i}`}
              className={styles.NoticeItemWrap}
            >
              <span>[{dataSource[i].title}]</span>
              <span style={{ marginLeft: '5px' }}>{dataSource[i].content}</span>
              <span>{dataSource[i].gmtCreate.substring(5, 10)}</span>
            </Link>,
          );
        }
      }
      return (
        <div>
          <Carousel vertical autoplay dots={false}>
            {divList}
          </Carousel>
        </div>
      );
    }
    return <p style={{ color: '#9BA0AA' }}>公司暂无相关公告信息</p>;
  }
  /**
   * render
   */
  public render() {
    const { boardTitle, bodyStyle } = this.props;
    return (
      <Card
        title={boardTitle}
        headStyle={{ borderBottom: 'none' }}
        bodyStyle={bodyStyle}
        className={styles.NoticeBoard}
        extra={
          <Link
            to="/company/proclamation"
            style={{
              display: 'inline-block',
              color: '#9BA0AA',
            }}
          >
            更多
            <Icon type="right" theme="outlined" />
          </Link>
        }
      >
        {this.getCarouselItem()}
      </Card>
    );
  }
}

export default NoticeBoard;
