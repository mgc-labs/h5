import * as React from 'react';
import Trigger from '../../../node_modules/rc-trigger/es/index';

import styles from 'modules/module.app/index.module.less';

const containerCls = styles.Page__Container;

let stopNode;

Object.assign(Trigger.defaultProps, {
  getPopupContainer(currentNode) {
    if (!stopNode) {
      stopNode = document.querySelector('#J_Container');
    }
    let node = currentNode;
    let popupContainer;
    if (stopNode.contains(node)) {
      while (node && node !== stopNode) {
        if (node.parentNode.classList.contains(containerCls)) {
          popupContainer = node;
          break;
        }
        node = node.parentNode;
      }
    }
    if (popupContainer) {
      return popupContainer;
    }
    return document.body;
  },
});

export default Trigger;
