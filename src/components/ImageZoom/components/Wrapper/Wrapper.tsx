/**
 * 应用主入口
 */

// React Libs
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';

// Config
import { defProp, defType } from '../../config/default';
// Utils
import {
  addListenEventOf,
  mobileCheck,
  removeListenEventOf,
  scrollWidth,
  windowHeight,
} from '../../utils/index';
import Background from '../Background/index';
// Components
import Control from '../Control/index';
import Image from '../Image';
// Style
import style from './index.module.less';

interface IImageSet {
  src: string;
  alt: string;
  text: string;
}

interface IController {
  // 分页
  pagination: boolean;
  // 标题
  title: boolean;
  // 旋转按钮
  rotate: boolean;
  // 缩放按钮
  zoom: boolean;
  // 关闭按钮
  close: boolean;
  // 左右翻页
  flip: boolean;
}

interface IHotKey {
  // 缩放（空格）
  zoom: boolean;
  // 关闭（ESC）
  close: boolean;
  // 翻页（左右键）
  flip: boolean;
}

export interface IWrapperProps {
  children?: React.ReactNode;
  // 封面节点
  cover: {};
  // 图片列表
  set: IImageSet[] | IImageSet;
  // 控制器
  controller: IController;
  // 快捷键
  hotKey: IHotKey;
  // 背景
  backdrop: string;
  // 图片距屏幕边距 (如果有)
  margin: number;
  page: number;
  // 卸载函数
  remove: () => void;
}

interface IWrapperState {
  show: boolean;
  zoom: boolean;
  page: number;
  rotate: number;
  margin: number;
}

export default class Wrapper extends React.PureComponent<
  IWrapperProps,
  IWrapperState
> {
  static defaultProps = {
    // 封面节点
    cover: {},
    // 图片列表
    set: [],
    // 控制器
    controller: defProp.controller,
    // 快捷键
    hotKey: defProp.hotKey,
    // 背景
    backdrop: defProp.backdrop,
    // 图片距屏幕边距 (如果有)
    margin: defProp.margin,
    // 卸载函数
    remove: () => {
      // empty
    },
  };

  constructor(props) {
    super(props);

    // 移动端检测
    // const mobile = mobileCheck();

    this.state = {
      // 显示
      show: false,
      // 缩放
      zoom: false,
      // 页数
      page: 0,
      // 旋转
      rotate: 0,
      // 是否移动端
      // mobile,
      // 图片距屏幕边距 (如果有)
      margin: props.margin,
    };
  }
  componentDidMount() {
    this.setState({
      page: this.props.page,
    });
    this.mountSelf();
  }
  componentWillUnmount() {
    removeListenEventOf('scroll', this.handleScroll);
    removeListenEventOf('keydown', this.handleKeyDown);
    removeListenEventOf('touchmove', this.handleScroll);
  }

  /**
   * 加载器
   */
  mountSelf = () => {
    const { cover } = this.props;
    this.setState({ show: true }, () => {
      // 隐藏封面原图
      // cover.style.visibility = 'hidden';
      // 绑定事件
      addListenEventOf('scroll', this.handleScroll);
      addListenEventOf('keydown', this.handleKeyDown);
      addListenEventOf('touchmove', this.handleScroll);
    });
  };
  unmountSelf = () => {
    // 显示封面原图（当前不为第一页时，遮罩从上方移除会迅速露出，需要立即显示，否则交由图片层处理）
    // if (page !== 0) {
    //   cover.style.visibility = 'visible';
    // }
    this.setState({
      show: false,
    });
  };
  /**
   * 事件处理
   *
   */
  handleKeyDown = e => {
    // 阻止默认事件
    e.preventDefault();
    const { set, hotKey } = this.props;
    const { zoom } = this.state;
    const hasImageSet = set && set.constructor === Array;
    const toPrevPage = this.handleSwitchPages('prev');
    const toNextPage = this.handleSwitchPages('next');
    switch (e.key) {
      case 'ArrowLeft':
        // 上一张
        !zoom && hotKey.flip && hasImageSet && toPrevPage();
        break;
      case 'ArrowRight':
        // 下一张
        !zoom && hotKey.flip && hasImageSet && toNextPage();
        break;
      case ' ':
        // 缩放
        hotKey.zoom && this.handleToggleZoom();
        break;
      case 'Escape':
        // 退出
        hotKey.close && (zoom ? this.handleToggleZoom() : this.unmountSelf());
        break;
      default:
        return;
    }
  };
  handleScroll = () => {
    this.state.show && this.unmountSelf();
  };

  /**
   * 翻页控制
   */
  handleJumpPages = page => {
    this.setState({ page });
  };
  handleSwitchPages = direction => {
    return () => {
      const { set } = this.props;
      const { page } = this.state;
      this.setState({
        page:
          direction === 'prev'
            ? Math.abs(set.length + page - 1) % set.length
            : (page + 1) % set.length,
      });
    };
  };

  /**
   * 旋转控制
   */
  handleToggleRotate = direction => {
    switch (direction) {
      case 'left':
        return () => this.setState({ rotate: this.state.rotate - 90 });
      case 'right':
        return () => this.setState({ rotate: this.state.rotate + 90 });
      default:
        return () => this.setState({ rotate: 0 });
    }
  };

  /**
   * 缩放控制
   */
  handleToggleZoom = () => {
    this.setState({
      zoom: !this.state.zoom,
    });
  };

  render() {
    const { cover, set, controller, backdrop, remove } = this.props;
    const { show, zoom, page, rotate, margin } = this.state;

    return (
      <div className={style.WrapperLayer}>
        {/*背景层*/}
        <Background
          show={show}
          zoom={zoom}
          backdrop={backdrop}
          unmountSelf={this.unmountSelf}
          toggleZoom={this.handleToggleZoom}
        />

        <Control
          set={set}
          show={show}
          zoom={zoom}
          page={page}
          // mobile={mobile}
          controller={controller}
          unmountSelf={this.unmountSelf}
          jumpPages={this.handleJumpPages}
          toggleRotate={this.handleToggleRotate}
          toggleZoom={this.handleToggleZoom}
        />

        <Image
          set={set}
          show={show}
          zoom={zoom}
          page={page}
          cover={cover}
          rotate={rotate}
          // mobile={mobile}
          remove={remove}
          margin={margin}
          toggleZoom={this.handleToggleZoom}
        />
      </div>
    );
  }
}
