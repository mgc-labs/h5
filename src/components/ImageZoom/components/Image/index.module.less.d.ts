export const loadingContainer: string;
export const fadeIn: string;
export const loading: string;
export const spin: string;
export const imageLayer: string;
