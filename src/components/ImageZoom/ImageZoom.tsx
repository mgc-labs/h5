/**
 * 应用主入口
 */

// React Libs
import Col from 'antd/es/col';
import Row from 'antd/es/row';

import { List, Map } from 'immutable';
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
// Components
import Portals from './components/Portals/index';
import Wrapper from './components/Wrapper/index';

// Config
import { defProp, defType } from './config/default';
import styles from './index.module.less';

export interface IImageZoomProps {
  set: object;
  hotKey: object;
  controller: object;
  backdrop: string;
  zIndex: number;
  children?: React.ReactNode;
  onClick: () => void;
}

class ImageZoom extends React.PureComponent<IImageZoomProps, any> {
  static getDerivedStateFromProps(nextProps) {
    return {
      // 数据更新时刷新 set
      set: ImageZoom.buildSet(nextProps),
    };
  }

  // 从初始 props 中生成图片集合
  static buildSet = props => {
    const { set } = props;
    if (Array.isArray(set) && set.length > 0) {
      return set;
    }
  };
  constructor(props) {
    super(props);

    // Refs
    this.cover = React.createRef();

    // States
    this.state = {
      browsing: false,
      set: ImageZoom.buildSet(props),
      page: 0,
    };
  }

  // 切换查看状态
  browsing = page => {
    if (this.props.onClick) {
      this.props.onClick();
    }
    this.setState({ browsing: true, page });
  };
  unBrowsing = () => {
    this.setState({ browsing: false, page: 0 });
  };

  render() {
    const { browsing, set, page } = this.state;
    const {
      controller, // 页面按钮
      hotKey, // 热键
      backdrop, // 背景颜色
      zIndex, // 高度
      ...props // 剩余参数
    } = this.props;
    return (
      <Fragment>
        {/*封面图片*/}
        <div className={styles.ImageBox}>
          <Row gutter={16}>
            {set.map((img, i) => {
              return (
                <Col key={i} span={4} style={{ marginBottom: '10px' }}>
                  <img
                    ref={ref => (this.cover = ref)}
                    src={img.src}
                    alt={img.alt}
                    title={img.alt}
                    style={{ cursor: 'zoom-in' }}
                    onClick={() => {
                      this.browsing(i);
                    }}
                    {...props}
                  />
                </Col>
              );
            })}
          </Row>
        </div>
        {/*查看叠层*/}
        {browsing && (
          <Portals zIndex={zIndex}>
            <Wrapper
              page={page}
              set={set}
              cover={this.cover}
              controller={{ ...defProp.controller, ...controller }}
              hotKey={{ ...defProp.hotKey, ...hotKey }}
              backdrop={backdrop}
              remove={this.unBrowsing}
            />
          </Portals>
        )}
      </Fragment>
    );
  }
}

// ImageZoom.defaultProps = {
//   // 图片列表
//   set: [],

//   // 控制器
//   controller: defProp.controller,
//   // 快捷键
//   hotKey: defProp.hotKey,

//   // 背景色
//   backdrop: defProp.backdrop,
//   // 高度
//   zIndex: defProp.zIndex,
// };

// ImageZoom.propTypes = {
//   // 图片集合, 可以传入单独的图片类型或数组包裹的图片类型
//   set: defType.set,

//   // 控制器
//   controller: defType.controller,
//   // 快捷键
//   hotKey: defType.hotKey,

//   // 背景色
//   backdrop: defType.backdrop,
//   // 高度
//   zIndex: defType.zIndex,
// };

export default ImageZoom;
