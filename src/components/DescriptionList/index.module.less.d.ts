export const descriptionList: string;
export const title: string;
export const term: string;
export const detail: string;
export const small: string;
export const large: string;
export const vertical: string;
