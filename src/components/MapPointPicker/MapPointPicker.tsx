/**
 * MapPointPicker Component
 * @author djd
 * @date 2018/9/12 下午3:15:57
 */
import Alert from 'antd/es/alert';
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import message from 'antd/es/message';
import Modal from 'antd/es/modal';
import Select from 'antd/es/select';
import * as React from 'react';

import styles from './index.module.less';

import { debounce } from 'lodash';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeUserDate } from './selectors';

import mapMark2 from './img/location_zuobiao.svg';
import mapMark1 from './img/location_zuobiao_dis.svg';
import { saveAddress } from './service';

declare const BMap: any;
declare const BMAP_STATUS_SUCCESS: any;

interface IOnOkOptions {
  lng: number;
  lat: number;
  city: string;
  province: string;
  region: string;
  address: string;
}

interface IPropsCurrentAddress {
  city?: string;
  province?: string;
  region?: string;
  address?: string;
  point?: {
    lng: number;
    lat: number;
  };
}

interface IProps {
  currentAddress?: IPropsCurrentAddress;
  onOk: (result: IOnOkOptions) => void;
  onClose: () => void;
  userDate: any;
  title?: string;
  address?: string;
}

class MapPointPickerComponent extends React.PureComponent<IProps, any> {
  private mapDiv: HTMLDivElement;
  private map: any;
  private searchMarkerList = [];
  private makerPointList = [];
  private selectedMarker: any = null;
  private autoComplete: any = null;
  private txt: HTMLInputElement;
  private onFetch: (text: string) => {};
  private currentMapCity = '';
  private preAddress;
  private isSubmiting: boolean;

  constructor(props: IProps, context: any) {
    super(props, context);

    this.state = {
      searchResultList: [],
    };

    this.searchMarkerList = []; // 本出搜出来的所有点

    this.onFetch = debounce(async (searchText: string) => {
      if (!this.txt) {
        return;
      }

      const options = {
        input: this.txt,
        location: this.currentMapCity,
        onSearchComplete: result => {
          const listData = [];
          for (let i = 0; i < result.getNumPois(); i++) {
            const rp = result.getPoi(i);
            listData.push(rp.city + rp.district + rp.business);
          }
          this.setState({
            searchResultList: listData,
          });
        },
      };
      if (!this.autoComplete) {
        this.autoComplete = new BMap.Autocomplete(options);
      }
      this.autoComplete.search(searchText);
    }, 300);
  }

  componentDidMount() {
    this.initMap();
    this.setState({
      address: this.props.address || '',
    });
  }

  removeElement = (element: Element) => {
    if (element) {
      element.parentElement.removeChild(element);
    }
  };

  newMarkIcon = icon => {
    return new BMap.Icon(icon, new BMap.Size(30, 37), {
      anchor: new BMap.Size(10, 30),
    });
  };

  setAddressInfo = rs => {
    let { value } = this.state;
    const { address } = this.state;
    const addressComponents = rs.addressComponents;

    value = value || rs.address;
    value = value
      .replace(addressComponents.city, '')
      .replace(addressComponents.province, '')
      .replace(addressComponents.district, '');

    const stateA = {
      city: addressComponents.city,
      region: addressComponents.district,
      province: addressComponents.province,
      lat: rs.point.lat,
      lng: rs.point.lng,
    };

    let stateB = {};

    if (!address || (address && this.preAddress === address)) {
      stateB = {
        address: value,
      };
    }

    this.setState({ ...stateA, ...stateB });
    this.preAddress = value;
  };

  // 跟据选中的坐标位置获取省、市、区、地止
  getMapAddress = marker => {
    this.selectMarker(marker);
    // 弹出地址详细信息
    this.getAddressInfo(marker.point, rs => {
      this.setAddressInfo(rs);
      const infoWindow = new BMap.InfoWindow(rs.address, {
        width: 200,
        height: 100,
        offset: new BMap.Size(0, -30),
      });
      this.map.openInfoWindow(infoWindow, marker.point);
    });
  };

  getAddressInfo = (point, onSuccess) => {
    // 根据经纬度获取地址信息
    const gc = new BMap.Geocoder();
    gc.getLocation(point, rs => {
      if (onSuccess) {
        onSuccess(rs);
      }
    });
  };

  // 设置地图中心区
  setMapCenter = (currentAddress: any = {}) => {
    this.map.centerAndZoom(new BMap.Point(116.404, 39.915), 11);

    // 根据组件外部props的currentAddress参数与设置中心区
    if (currentAddress.city && currentAddress.address) {
      this.currentMapCity = currentAddress.city;
      setTimeout(() => {
        this.map.setCenter(this.currentMapCity);
      });

      this.setState({
        ...currentAddress,
      });
      return;
    }

    // 根据用户的ip获取城市设置地图中心区
    const myCity = new BMap.LocalCity();
    myCity.get(result => {
      const cityName = result.name;
      this.map.setCenter(cityName);
      this.currentMapCity = cityName;
    });
  };

  initMap = () => {
    setTimeout(() => {
      this.map = new BMap.Map(this.mapDiv, {
        minZoom: 1,
        maxZoom: 19,
        enableMapClick: false,
      });

      this.setMapCenter(this.props.currentAddress);

      this.map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放

      // 去掉地图左下解的LOGO与文字
      this.map.addEventListener('tilesloaded', () => {
        this.removeElement(document.querySelector('.anchorBL'));
        this.removeElement(document.querySelector('.BMap_cpyCtrl'));
      });

      // 右键创建新点
      this.map.addEventListener('rightclick', e => {
        const pt = new BMap.Point(e.point.lng, e.point.lat);
        const marker = this.setNewMarker(pt, true);
        this.map.addOverlay(marker);
        this.getMapAddress(marker);
      });
    });
  };

  // 在地图上新建一个点
  setNewMarker = (point, isCurrentAddress) => {
    const icon = isCurrentAddress
      ? this.newMarkIcon(mapMark2)
      : this.newMarkIcon(mapMark1);
    const marker = new BMap.Marker(point, { icon });
    marker.enableDragging(); // 点支持拖动

    this.searchMarkerList.push(marker);
    marker.addEventListener('click', e => {
      this.getMapAddress(e.target);
    });
    marker.addEventListener('dragstart', e => {
      this.map.closeInfoWindow();
    });
    marker.addEventListener('dragend', e => {
      this.getMapAddress(marker);
    });
    return marker;
  };

  // 当前地址点高亮显示, 其它点置灰
  selectMarker = marker => {
    this.searchMarkerList.map(m => {
      if (m === marker) {
        m.setIcon(this.newMarkIcon(mapMark2));
        this.selectedMarker = m; // 设置当前选中点
      } else {
        m.setIcon(this.newMarkIcon(mapMark1));
      }
    });
  };

  handleSearch = (text: string) => {
    this.onFetch(text);
  };

  handleChange = (value: string) => {
    this.setState({
      value,
    });

    this.makerPointList = [];

    this.localSearch(value);
  };

  // lbs中保存地址，并调用props.onOk函数，传递参数给上一级dome
  handleOk = async () => {
    const { onOk, onClose } = this.props;
    const state = this.state;
    const options = {
      lng: state.lng,
      lat: state.lat,
      city: state.city,
      province: state.province,
      region: state.region,
      address: (state.address || '').replace(/(^\s+)|(\s+$)/g, ''),
    };
    if (!(options.lng && options.lat)) {
      message.error('请在地图上选择一个点');
      return;
    }

    if (!options.address) {
      message.error('请输入地址名称！');
      this.setState({
        isHadSubmited: true,
      });
      return;
    }

    if (this.isSubmiting) {
      return;
    }

    try {
      this.isSubmiting = true;
      const data = await saveAddress(
        {
          ...options,
          latitude: this.state.lat,
          longitude: this.state.lng,
        },
        this.getFlag(),
      );
      if (data.code !== 0) {
        throw data;
      }
      const addressData = data && data.data ? data.data : {};
      if (!(addressData.lng && addressData.lat)) {
        this.isSubmiting = false;
        message.error('网络错误，请重试...');
        return;
      }

      if (onOk) {
        onOk({
          ...options,
          lng: addressData.lng,
          lat: addressData.lat,
        });
      }
      if (onClose) {
        onClose();
      }
    } catch (err) {
      this.isSubmiting = false;
      message.error(err && err.msg ? err.msg : '网络错误，请重试...');
    }
  };
  handleCancel = () => {
    const { onClose } = this.props;
    if (onClose) {
      onClose();
    }
  };

  localSearch = addressText => {
    // 根据文本框内地址在地图上描点
    let local;

    const options = {
      onSearchComplete: results => {
        this.searchMarkerList.map(m => {
          this.map.removeOverlay(m);
        });
        this.searchMarkerList = [];
        if (local.getStatus() === BMAP_STATUS_SUCCESS) {
          for (let i = 0, j = results.getCurrentNumPois(); i < j; i++) {
            const poi = results.getPoi(i);

            this.makerPointList.push(poi.point);

            const maker = this.setNewMarker(poi.point, true);
            this.map.addOverlay(maker);
          }
          this.map.setViewport(this.makerPointList); // 地图缩放到能显示所有点
        }
      },
    };
    local = new BMap.LocalSearch(this.map, options);
    local.search(addressText);
  };

  // 地图地址搜索文本框demo
  renderMapAddressDemo = () => {
    return (
      <div className={styles.mapAddress}>
        <input
          type="hidden"
          ref={obj => {
            return (this.txt = obj);
          }}
          placeholder={'请输入大概的地点'}
        />
        <Select
          showSearch={true}
          allowClear={true}
          value={this.state.value}
          placeholder={'请输入大概的地点'}
          // style={this.props.style}
          defaultActiveFirstOption={false}
          showArrow={false}
          filterOption={false}
          onSearch={this.handleSearch}
          onChange={this.handleChange}
          notFoundContent={null}
          style={{ width: '100%' }}
          autoFocus
        >
          {this.state.searchResultList.map((item, index) => (
            <Select.Option key={index} value={item}>
              {item}
            </Select.Option>
          ))}
        </Select>
      </div>
    );
  };
  handleChangeAddress = e => {
    this.setState({
      address: e.target.value,
    });
  };
  render() {
    const {
      province = '',
      city = '',
      region = '',
      address = '',
      isHadSubmited,
    } = this.state;
    const isError = !address.replace(/(^\s+)|(\s+$)/g, '') && isHadSubmited;
    return (
      <Modal
        title={this.props.title || '新增位置'}
        visible={true}
        width={750}
        className={styles.MapPointPickerModal}
        onCancel={this.handleCancel}
        footer={null}
        centered={true}
        maskClosable={false}
      >
        <Alert
          message="1）请输入大概的地点帮助找到详细位置；2）在地图区域选择正确的坐标位置（可通过检索功能快速定位，在结果中左键选择，或者右键新增）3）在地址名称上进行输入"
          type="info"
          showIcon
        />
        <div className={styles.MapPointPicker}>
          {this.renderMapAddressDemo()}
          <div
            className={styles.map}
            ref={obj => {
              this.mapDiv = obj;
            }}
          />
        </div>
        <div className={styles.bottomLayout + (isError ? ' has-error' : '')}>
          <div style={{ paddingBottom: 5 }}>
            <label className="ant-form-item-required">地址名称：</label>
            {province + ' ' + city + ' ' + region + ' '}
          </div>
          <Input
            placeholder={'此地址名称将保存为地图上位置的名称'}
            value={this.state.address}
            onChange={this.handleChangeAddress}
            maxLength={200}
          />
          <Button type="primary" onClick={this.handleOk}>
            保存
          </Button>
        </div>
      </Modal>
    );
  }
  private getFlag = () => {
    const { userDate } = this.props;
    try {
      return userDate.get('domainName');
    } catch (err) {
      return '';
    }
  };
}

const mapStateToProps = createStructuredSelector({
  userDate: makeUserDate(),
});

export const MapPointPicker = connect(
  mapStateToProps,
  () => ({}),
)(MapPointPickerComponent);
