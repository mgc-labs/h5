import { createSelector } from 'reselect';

const selectState = state => state.get('authorization');

export const makeUserDate = () =>
  createSelector(selectState, state => state.get('accountInfo'));
