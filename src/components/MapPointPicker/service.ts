/**
 * Auth Service
 * @author yanrong.tian
 */
import request, { mocker } from 'utils/request';

const API_GET_ADDRESS = '/ehuodiGateway/geoService/v1.0/poi/createPoi';

export const saveAddress = (options, flag) => {
  return request({
    method: 'post',
    url: API_GET_ADDRESS,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    params: {
      ...options,
      dataSource: 'UTMS_' + flag,
      coordinateType: 'BD09',
      format: 'WGS84',
    },
  }).then(res => res);
};
