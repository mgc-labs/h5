/**
 * DateTimePicker Component
 * @author yanrong.tian
 * @date 2018/9/12 下午2:41:24
 */
import {
  AutoComplete,
  Button,
  Cascader,
  Checkbox,
  Col,
  Form,
  Icon,
  Input,
  Modal,
  Row,
  Select,
  Spin,
  Tooltip,
} from 'antd';
import * as React from 'react';
import { getIndustrys } from './service';

const FormItem = Form.Item;
const Option = Select.Option;

interface IProps {
  type?: string; // 字典code
  onChange?: any;
  placeholder?: string; // 提示语
}
interface IState {
  data: any;
}

class IndustryPicker extends React.PureComponent<IProps> {
  state: IState;
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
    this.getIndustrysData();
  }

  public render() {
    const industrys = this.state.data || [];
    const props = { ...this.props };
    return (
      <Select
        showSearch
        placeholder={`${
          props.placeholder ? props.placeholder : '请选择客户行业'
        }`}
        optionFilterProp="children"
        style={{ width: '100%' }}
        {...props}
        onChange={this.handleChange}
      >
        {industrys.map(d => (
          <Option key={d.dictionaryName}>{d.dictionaryName}</Option>
        ))}
      </Select>
    );
  }

  private handleChange = value => {
    const { data } = this.state;
    const { onChange } = this.props;
    let currentDate = {};
    if (!(data && data.length)) {
      return;
    }
    data.map(item => {
      if (item.dictionaryName === value) {
        currentDate = {
          industry: value,
          industryCode: item.dictionaryCode,
        };
      }
    });
    try {
      if (onChange) {
        onChange(value, currentDate);
      }
    } catch (e) {
      //
    }
  };

  private getIndustrysData = async () => {
    const { type } = this.props;
    const data = await getIndustrys(type);
    this.setState({
      data: data.data,
    });
  };
}

export default IndustryPicker;
