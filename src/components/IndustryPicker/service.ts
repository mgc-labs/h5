/**
 * Auth Service
 * @author yanrong.tian
 */
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_INDUSTRYS =
  '/ehuodiGateway/utmsCore/utmsDictionarycs/selectUtmsDictionaryList';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_INDUSTRYS)
    .reply(() =>
      import('./mock/industrys').then(exports => [200, exports.default]),
    );
}

/**
 * 获取字典列表
 */
export const getIndustrys = type => {
  const options = {
    parentDictionaryCode: type ? type : 'INDUSTRYS',
    returnFormat: 1,
  };
  return request(
    {
      method: 'post',
      url: API_INDUSTRYS,
      data: options,
      params: options,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);
};
