export default {
  result: 'success',
  count: '',
  data: [
    {
      ehdpubdictionaryid: '1',
      dictionaryName: '服装',
      dictionaryCode: '54243',
    },
    {
      ehdpubdictionaryid: '2',
      dictionaryName: '家电',
      dictionaryCode: '54244',
    },
    {
      ehdpubdictionaryid: '3',
      dictionaryName: '生产制造',
      dictionaryCode: '54245',
    },
    {
      ehdpubdictionaryid: '4',
      dictionaryName: '商超/便利店',
      dictionaryCode: '54247',
    },
  ],
};
