/**
 * LoadInfoEdit Component
 * @author lhf
 * @date 2018-9-20 10:24:06
 */
import * as React from 'react';

import Button from 'antd/es/button';
import Col from 'antd/es/col';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';
import Row from 'antd/es/row';
import AddressSelect from 'components/AddressSelect';
import CommonAddressSelect from 'components/CommonAddressSelect/';
import { FIXED_TELEPHONE } from 'utils/commonRegExp';

import styles from './index.module.less';

export interface ILoadInfoEditProps {
  fields: { name: string; value: object[] };
  form: WrappedFormUtils;
  loadtype: number;
  showAddressModal: (type: number, value: number) => void;
  closeAddressModal: (data: {}, formData: {}, loadType: number) => void;
  onDele?: (key) => void;
}
interface ILoadInfoEditState {
  showAddress: boolean;
  dataKey: number;
}

const FormItem = Form.Item;
const gutter = 16;
const colLayProps = {
  sm: 24,
  xs: 24,
  md: 6,
  lg: 6,
  xl: 6,
};

class LoadInfoEdit extends React.PureComponent<
  ILoadInfoEditProps,
  ILoadInfoEditState
> {
  selectedAddress: any;
  constructor(props) {
    super(props);
    this.state = {
      showAddress: false,
      dataKey: 0,
    };
  }

  public render() {
    const { showAddress } = this.state;
    return (
      <div>
        {this.renderLoadItems()}
        <CommonAddressSelect
          visible={showAddress}
          afterClose={() => {
            this.closeAddressModal();
          }}
          onChange={data => {
            this.selectedAddress = data;
          }}
        />
      </div>
    );
  }

  // 渲染装卸货信息
  private renderLoadItems() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { fields, loadtype } = this.props;
    getFieldDecorator('unloadKeys', {
      initialValue: [0],
    });
    const keys = loadtype === 0 ? [0] : getFieldValue('unloadKeys');
    const field = fields.name;
    const formItems = keys.map((k, index) => {
      return (
        <Row gutter={gutter} key={index} className={styles.LoadInfoEdit}>
          <Col {...colLayProps}>
            <FormItem label={`${loadtype === 0 ? '装' : '卸'}货信息`} required>
              {getFieldDecorator(`${field}[${k}].pointAddress`, {
                rules: [
                  {
                    required: true,
                    whitespace: true,
                    message: `请输入${loadtype === 0 ? '装' : '卸'}货地址`,
                  },
                ],
                trigger: 'onChange',
              })(
                <AddressSelect
                  onSelected={option => this.onSelectedAddress(option, k)}
                  placeholder={`请输入${loadtype === 0 ? '装' : '卸'}货地址`}
                />,
              )}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="联系人">
              {getFieldDecorator(`${field}[${k}].contact`, {
                rules: [
                  {
                    required: loadtype === 0,
                    message: '请输入联系人',
                  },
                  {
                    max: 30,
                    message: '联系人最多为30个字',
                  },
                ],
              })(<Input placeholder="请输入姓名" maxLength={30} />)}
            </FormItem>
          </Col>
          <Col {...colLayProps}>
            <FormItem label="联系方式">
              {getFieldDecorator(`${field}[${k}].phone`, {
                rules: [
                  {
                    required: loadtype === 0,
                    message: '请输入联系方式',
                  },
                  {
                    pattern: FIXED_TELEPHONE,
                    message: '请输入正确的联系方式',
                  },
                ],
              })(<Input placeholder="请输入联系方式" maxLength={20} />)}
            </FormItem>
          </Col>

          <Col {...colLayProps}>
            <FormItem label=" " colon={false}>
              {keys.length > 1 && index !== 0 && loadtype === 1 ? (
                <Icon
                  type="minus-circle-o"
                  className={`dynamic-delete-button ${
                    styles.LoadInfoEdit_Icon
                  }`}
                  onClick={() => this.remove(k)}
                />
              ) : null}
              {index === 0 && loadtype === 1 ? (
                <Icon
                  type="plus-circle"
                  className={styles.LoadInfoEdit_Icon}
                  onClick={() => {
                    this.addUnload();
                  }}
                />
              ) : null}
              <Button
                style={{ marginLeft: 10, marginTop: -1 }}
                type="primary"
                onClick={() => {
                  this.showAddressModal(loadtype, k);
                }}
              >
                常用地址
              </Button>
            </FormItem>
          </Col>
        </Row>
      );
    });
    return formItems;
  }

  // 显示常用地址
  private showAddressModal(loadType: number, index) {
    this.setState({ showAddress: true });
    const { showAddressModal } = this.props;
    showAddressModal(loadType, index);
  }

  // 关闭常用地址，读取地址信息
  private closeAddressModal() {
    this.setState({ showAddress: false });
    const { closeAddressModal, form, loadtype } = this.props;
    const data = this.selectedAddress
      ? {
          loadType: loadtype,
          pointAddress: this.selectedAddress.address,
          contact: this.selectedAddress.contact,
          longitude: this.selectedAddress.longitude,
          latitude: this.selectedAddress.latitude,
          phone: this.selectedAddress.contactWay,
          province:
            this.selectedAddress.province +
            this.selectedAddress.city +
            this.selectedAddress.region,
        }
      : {};
    const formData = this.selectedAddress
      ? {
          pointAddress: this.selectedAddress.address,
          contact: this.selectedAddress.contact,
          phone: this.selectedAddress.contactWay,
        }
      : {};
    closeAddressModal(data, formData, loadtype);
  }

  private onSelectedAddress(options, index) {
    const { closeAddressModal, showAddressModal, form, loadtype } = this.props;
    this.selectedAddress = options;
    showAddressModal(loadtype, index);
    const data = {
      loadType: loadtype,
      pointAddress: options ? options.address : '',
      longitude: options ? options.lng : '',
      latitude: options ? options.lat : '',
      province: options ? options.province + options.city + options.region : '',
    };
    const formData = {
      pointAddress: options ? options.address : '',
    };
    setTimeout(() => {
      closeAddressModal(data, formData, loadtype);
    }, 0);
  }

  // 卸货信息需要新增
  private addUnload() {
    const { form } = this.props;
    const keys = form.getFieldValue('unloadKeys');
    const dataKey = keys[keys.length - 1] + 1;
    this.setState({ dataKey });
    const nextKey = keys.concat(dataKey);
    form.setFieldsValue({
      unloadKeys: nextKey,
    });
  }

  // 删除卸货信息
  private remove(k) {
    const { form, onDele } = this.props;
    const data = form.getFieldValue('unloadKeys');
    if (data.length === 1) {
      return;
    }
    form.setFieldsValue({
      unloadKeys: data.filter(key => key !== k),
    });
    onDele(k);
  }
}

export default LoadInfoEdit;
