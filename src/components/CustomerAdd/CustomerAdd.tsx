/**
 * DateTimePicker Component
 * @author yanrong.tian
 * @date 2018/9/12 下午2:41:24
 */
import Button from 'antd/es/button';
import Col from 'antd/es/col';
import DatePicker from 'antd/es/date-picker';
import Form from 'antd/es/form';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';
import message from 'antd/es/message';
import Row from 'antd/es/row';
import Select from 'antd/es/select';
import Upload from 'antd/es/upload';
import AddressSelect from 'components/AddressSelect';
import CarModelCarLong from 'components/CarModelCarLong';
import {
  addDictionary,
  deleteDictionary,
  getDictionary,
  getSaveHttp,
} from 'components/CustomerAdd/service';
import IndustryPicker from 'components/IndustryPicker';
import MutiSelectWithEditableOptions from 'components/MutiSelectWithEditableOptions';
import StaffPicker from 'components/StaffPicker';
import { fromJS, List } from 'immutable';
import moment from 'moment';
import * as React from 'react';

const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;
interface IProps {
  visible: boolean;
  onCancel: () => void;
  onOk: (e: any) => void;
  form: any;
  data: object;
}
interface IState {
  visible: boolean;
  data?: object;
  customertagList: List<Map<string, any>>;
  fileList: [];
  customerTags: [];
}

interface IIndustyData {
  industry: string;
  industryCode: string | number;
}

class CustomerAddComponent extends React.PureComponent<IProps> {
  state: IState;
  selectedAddress: object;
  industyData: IIndustyData;
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
      customertagList: List(),
      customerTags: [],
      fileList: [],
    };
  }

  public componentDidMount() {
    this.getTags();
  }

  public render() {
    const props = { ...this.props };
    const state = this.state;
    props.visible = state.visible;
    props.onOk = this.handleSubmit;

    const formItemLayout = {
      labelCol: {
        span: 5,
      },
      wrapperCol: { span: 16 },
    };
    const {
      getFieldDecorator,
      getFieldValue,
      setFieldsValue,
    } = this.props.form;

    const customerTagsProps = {
      options: state.customerTags,
      placeholder: '创建标签，打标客户，分层管理',
      addInputPlaceHolder: '请输入添加的客户标签',
      addBtnText: '添加标签',
      resetSelect: false,
      isMuti: true,
      tagSize: 5,
      optionSize: 20,
      removeOption: option => this.removeOption('customerTags', option),
      addOption: option => this.addOption('customerTags', option),
      addToForm: departments => this.addToForm('customerTags', departments),
    };
    return (
      <div>
        <FormItem {...formItemLayout} label="客户名称：">
          {getFieldDecorator('customerName', {
            rules: [
              {
                required: true,
                message: '请输入客户名称',
              },
            ],
          })(<Input placeholder="请输入客户名称" />)}
        </FormItem>
        <FormItem {...formItemLayout} required label="联系人：">
          <Row>
            <Col md={8}>
              <FormItem>
                {getFieldDecorator('contact', {
                  rules: [
                    {
                      required: true,
                      message: '请输入客户联系人姓名',
                    },
                  ],
                })(
                  <Input
                    type="text"
                    placeholder="请输入姓名"
                    style={{ width: 100 }}
                    maxLength={20}
                  />,
                )}
              </FormItem>
            </Col>
            <Col md={16}>
              <FormItem>
                {getFieldDecorator('contactWay', {
                  rules: [
                    {
                      required: true,
                      message: '请输入客户联系方式',
                    },
                    {
                      pattern: /^[\d|\-|\s]*$/,
                      message: '请输入正确的联系方式',
                    },
                  ],
                })(
                  <Input
                    type="text"
                    placeholder="请输入联系方式"
                    maxLength={20}
                  />,
                )}
              </FormItem>
            </Col>
          </Row>
        </FormItem>
        <FormItem {...formItemLayout} label="客户编码">
          {getFieldDecorator('customerCode', {
            rules: [
              {
                required: true,
                message: '请输入客户编码',
              },
            ],
          })(<Input placeholder="请输入客户编码" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="行业分类：">
          {getFieldDecorator('industry')(
            <IndustryPicker onChange={this.handleIndustyPicker} />,
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="结算方式：">
          {getFieldDecorator('settlementType')(
            <IndustryPicker
              type="SettlementType"
              placeholder="请选择结算方式"
            />,
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="合作期限：">
          {getFieldDecorator('cooperateDate')(
            <RangePicker style={{ width: '100%' }} />,
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="客户打标：">
          {getFieldDecorator('customerTags')(
            <MutiSelectWithEditableOptions {...customerTagsProps} />,
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="公司地址">
          {getFieldDecorator('address')(
            <AddressSelect
              placeholder="请输入公司地址"
              onSelected={this.onSelectedAddress}
              style={{ width: '100%' }}
            />,
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="禁入车型：">
          {getFieldDecorator('offLimitCarTypes')(
            <CarModelCarLong placeholder="请选择车型" />,
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="销售经理：">
          {getFieldDecorator('salesman')(<StaffPicker maxLength={5} />)}
        </FormItem>
        <FormItem {...formItemLayout} label="上传文件：">
          {getFieldDecorator('custAttachments')(
            <Upload
              action="/ehuodiGateway/utmsCore/basecs/uploadCustAttachment"
              onChange={this.handleUploadFile}
            >
              <Button disabled={state.fileList.length >= 5}>
                <Icon type="upload" /> 点击上传
              </Button>
            </Upload>,
          )}
        </FormItem>
        <div style={{ textAlign: 'center' }}>
          <Button key={0} type="primary" htmlType="submit" onClick={props.onOk}>
            保存
          </Button>
          <Button style={{ marginLeft: 10 }} key={1} onClick={props.onCancel}>
            取消
          </Button>
        </div>
      </div>
    );
  }

  public componentWillReceiveProps(nextProps) {
    // this.setState({
    //   visible: nextProps.visible,
    // });
  }

  // 获取客户标签
  private getTags = async () => {
    const result = await getDictionary({
      parentDictionaryCode: 'customerTags',
    });
    if (result.result === 'success') {
      const tags = [];
      let oldTags = '';
      const { customerTags } = this.state;

      customerTags.map(d => {
        oldTags += d.key + ',';
      });

      result.data.map((d, i) => {
        let checked = false;
        if (oldTags.includes(d.utmsDictionaryId)) {
          customerTags.map(tag => {
            if (tag.key === d.utmsDictionaryId) {
              checked = tag.checked;
            }
          });
        }

        const t = {
          key: d.utmsDictionaryId,
          value: d.dictionaryName,
          checked,
        };
        tags.push(t);
      });
      this.setState({
        customerTags: tags,
      });
    }
  };

  private handleIndustyPicker = (value, options) => {
    this.industyData = options;
  };

  // 新增客户打标
  private addOption = async (type, option) => {
    const params = {
      bizType: type,
      dictionaryCode: option,
    };
    const result = await addDictionary(params);
    if (result.result === 'success') {
      this.setState({
        customertagList: fromJS(await this.getTags()),
      });
    }
  };
  // 删除客户打标
  private removeOption = async (type, option) => {
    const params = {
      bizType: type,
      dictionaryCode: option,
    };
    const result = await deleteDictionary(params);
    if (result.result === 'success') {
      this.setState({
        customertagList: fromJS(await this.getTags()),
      });
    }
  };

  // 选择客户标签
  private addToForm = (type, options) => {
    const { form } = this.props;
    form.setFieldsValue({
      customerTags: options
        .filter(option => {
          return option.checked;
        })
        .map(option => {
          return option.value;
        })
        .join(','),
    });
  };

  // 处理上传文件
  private handleUploadFile = file => {
    const data = file.fileList.map((item, index) => {
      return {
        attachmentseq: index,
        attachmenturl: item.status === 'done' ? item.response.data : '',
      };
    });
    this.setState({
      fileList: data,
    });
  };

  // 选择地址之后
  private onSelectedAddress = options => {
    this.selectedAddress = options || {
      region: '',
      province: '',
      city: '',
    };
  };

  private save = async values => {
    const cooperateDate = values.cooperateDate;
    const dateData = {
      cooperateBeginDate:
        cooperateDate && cooperateDate.length
          ? cooperateDate[0].format('YYYY-MM-DD 00:00:00')
          : '',
      cooperateEndDate:
        cooperateDate && cooperateDate.length
          ? cooperateDate[1].format('YYYY-MM-DD 23:59:59')
          : '',
    };
    const files = {
      custAttachments: this.state.fileList,
    };
    const options = {
      ...values,
      ...this.selectedAddress,
      ...dateData,
      ...files,
    };
    const salesman = [];
    if (options.salesman && options.salesman.length) {
      options.salesman.map(d => {
        salesman.push({
          jobcard: d.key,
          name: d.label[0],
        });
      });
      options.salesman = salesman;
    }

    try {
      const data = await getSaveHttp({
        ...this.props.data,
        ...options,
        ...this.industyData,
      });

      if (options.utmsCustomerId) {
        message.success('客户信息修改成功');
      } else {
        message.success('客户新增成功');
      }

      if (this.props.onOk) {
        this.props.onOk({ ...this.props.data, ...options });
      }
    } catch (err) {
      const msg = err && err.msg ? err.msg : '网络错误，请重试...';
      message.error(msg || '操作失败');
    }
  };
  private handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.save(values);
      }
    });
  };
}
const CustomerAddModal = Form.create({
  mapPropsToFields(props: any) {
    const data = props.data;
    const fields = {};
    Object.keys(data || {}).forEach(d => {
      let itemData = data[d];
      if (d === 'salesman' && itemData && itemData.length) {
        const salesman = [];
        JSON.parse(itemData).map(item => {
          salesman.push({
            key: item.jobcard,
            label: [item.name],
          });
        });
        itemData = salesman;
      }
      fields[d] = Form.createFormField({
        value: itemData,
      });
      fields.cooperateDate = Form.createFormField({
        value: [moment(data.cooperateBeginDate), moment(data.cooperateEndDate)],
      });
    });
    return {
      ...fields,
    };
  },
})(CustomerAddComponent);
export default CustomerAddModal;
