/**
 * Auth Service
 * @author yanrong.tian
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

// API list
const API_SAVE = '/ehuodiGateway/utmsCore/utmsCustomercs/addCustomer'; // 保存
const API_EDIT_SAVE = '/ehuodiGateway/utmsCore/utmsCustomercs/modifyCustomer'; // 修改
const API_DICTIONARY =
  '/ehuodiGateway/utmsCore/utmsDictionarycs/selectCustDictionaryList'; // 字典信息
const API_DELETEDICTIONARY =
  '/ehuodiGateway/utmsCore/utmsDictionarycs/delCustUtmsDictionary';
const API_ADDDICTIONARY =
  '/ehuodiGateway/utmsCore/utmsDictionarycs/createCustUtmsDictionary';

if (process.env.NODE_ENV === 'development') {
  mocker
    .on('post', API_SAVE)
    .reply(() => import('./mock/save').then(exports => [200, exports.default]));
}

/**
 * 保存新建客户
 */
export const getSaveHttp = options => {
  let url = API_SAVE;
  if (options.utmsCustomerId) {
    url = API_EDIT_SAVE;
  }
  return request(
    {
      method: 'post',
      url,
      data: {
        ...options,
        salesman: JSON.stringify(options.salesman),
      },
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);
};

// 获取字典列表
export const getDictionary = options => {
  const option = {
    ...options,
    returnFormat: 1,
  };
  return request({
    method: 'post',
    url: API_DICTIONARY,
    data: option,
    params: option,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
  }).then(res => res);
};

// 删除字典字段
export const deleteDictionary = params =>
  request(
    {
      method: 'post',
      url: API_DELETEDICTIONARY,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);

// 增加字典字段
export const addDictionary = params =>
  request(
    {
      method: 'post',
      url: API_ADDDICTIONARY,
      data: qs.stringify(params),
    },
    {
      useMock: false,
      globalErrorMsg: true,
    },
  ).then(res => res);
