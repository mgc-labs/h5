/**
 * Customer selectors
 * @author yanrong.tian
 * @date 2018-9-13 14:19:59
 */
import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeCustomers = selectState =>
  createSelector(selectState, (state: Map<string, any>) =>
    state.get('userData'),
  );
