/**
 * DateTimePicker Component
 * @author yanrong.tian
 * @date 2018/9/12 下午2:41:24
 */
import Table from 'antd/es/table';
import * as React from 'react';
import './index.less';

// 修改了pagination组件中onChange、onShowSizeChange回调时的参数
interface IPaginationChangeEventReturn {
  skipCount: number;
  pageSize: number;
  current: number;
}
interface IPaginationProps {
  onChange: (pageData: IPaginationChangeEventReturn, pageIndex: number) => void;
  onShowSizeChange: (IPaginationChangeEventReturn) => void;
}

interface IPagination {
  pagination: IPaginationProps;
  [key: string]: any;
}

class MyTable extends React.PureComponent<IPagination> {
  pagination: any;
  constructor(props) {
    super(props);
    this.pagination = {
      pageSize: 15,
      current: 1,
      onChange: this.onChange,
      onShowSizeChange: this.onShowSizeChange,
      pageSizeOptions: ['10', '15', '20', '30', '40', '50', '100', '500'],
      showQuickJumper: true,
      showSizeChanger: true,
      showTotal: total => `共 ${total} 条`,
    };
  }

  public render() {
    const { pagination, ...otherProps } = this.props;
    const paginationProps = Object.assign({}, this.pagination, pagination, {
      onChange: this.pagination.onChange,
      onShowSizeChange: this.pagination.onShowSizeChange,
    });
    return (
      <Table
        className={'my-table'}
        {...otherProps}
        pagination={paginationProps}
      />
    );
  }

  private onChange = page => {
    this.pagination.current = page;
    if (this.props.pagination && this.props.pagination.onChange) {
      this.props.pagination.onChange(
        {
          skipCount: (this.pagination.current - 1) * this.pagination.pageSize,
          pageSize: this.pagination.pageSize,
          current: page,
        },
        page,
      );
    }
  };

  private onShowSizeChange = (page, size) => {
    this.pagination.current = 0;
    this.pagination.pageSize = size;
    if (this.props.pagination && this.props.pagination.onShowSizeChange) {
      this.props.pagination.onShowSizeChange({
        skipCount: 0,
        pageSize: this.pagination.pageSize,
        current: this.pagination.current,
      });
    }
  };
}

export default MyTable;
