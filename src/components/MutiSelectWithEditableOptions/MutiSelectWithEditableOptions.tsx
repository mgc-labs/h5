/**
 * MutiSelectWithEditableOptions Component
 * @author HuangSiFei
 * @date 2018-9-25 16:18:45
 */
import Dropdown from 'antd/es/dropdown';
import Icon from 'antd/es/icon';
import Input from 'antd/es/input';
import Menu from 'antd/es/menu';
import message from 'antd/es/message';
import Tag from 'antd/es/tag';
import classnames from 'classnames';
import { fromJS, List } from 'immutable';
import * as React from 'react';

import './index.less';
import styles from './index.module.less';

export interface IMutiSelectWithEditableOptionsProps {
  // options: List<any>;
  options: any;
  placeholder: string;
  addInputPlaceHolder: string;
  isMuti?: boolean;
  addBtnText?: string;
  formValue?: List<any>;
  resetSelect: boolean;
  tagSize?: number;
  optionSize?: number;
  removeOption: (data) => any;
  addOption: (data) => any;
  addToForm: (options) => any;
}

interface IMutiSelectWithEditableOptionsStates {
  visible: boolean;
  addInputVisible: boolean;
  tagList: List<any>;
  newOption: string;
}

export default class MutiSelectWithEditableOptions extends React.PureComponent<
  IMutiSelectWithEditableOptionsProps,
  IMutiSelectWithEditableOptionsStates
> {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      addInputVisible: false,
      tagList: List(),
      newOption: '',
    };
  }

  public componentWillReceiveProps(nextProps) {
    this.setState({
      tagList: fromJS(
        nextProps.options
          .filter(option => {
            return option.checked;
          })
          .map(option => {
            return {
              value: option.value,
              key: option.key,
            };
          }),
      ),
    });
  }

  public handleVisibleChange = flag => {
    this.setState({ visible: flag });
  };

  public handleClickOption = option => {
    const { tagList: tagList } = this.state;
    const { isMuti, options } = this.props;
    option.checked = !option.checked;
    if (option.checked) {
      const { tagSize } = this.props;
      if (isMuti) {
        if (tagList.size >= tagSize) {
          option.checked = false;
          message.error(`最多选择${tagSize}个`);
          return;
        }
        this.setState({
          tagList: fromJS([
            ...tagList,
            { value: option.value, key: option.key },
          ]),
        });
      } else {
        options.map(o => {
          if (o.key !== option.key) {
            o.checked = false;
          }
        });
        this.setState({
          tagList: fromJS([{ value: option.value, key: option.key }]),
        });
      }
    } else {
      const newArr = tagList.filter(tag => tag.get('value') !== option.value);
      this.setState({
        tagList: newArr,
      });
    }
    this.props.addToForm(this.props.options);
  };

  public handleCloseTag = (tag, event) => {
    event.stopPropagation();
    const { tagList: array } = this.state;
    const { options } = this.props;
    const newArr = array.filter(item => item.get('value') !== tag.get('value'));
    options.map(option => {
      if (option.key === tag.get('key')) {
        option.checked = !option.checked;
      }
    });
    this.props.addToForm(options);
    this.setState({
      tagList: newArr,
    });
  };

  public handleAddOption = show => {
    if (!this.state.newOption) {
      message.error(`请先输入要添加的选项`);
      return;
    }
    const { optionSize, options } = this.props;
    if (options.length >= optionSize) {
      message.error(`最多添加${optionSize}个选项`);
      return;
    }
    this.props.addOption(this.state.newOption);
    this.setState({
      addInputVisible: show,
      newOption: '',
    });
  };

  public toggleAddOption = show => {
    this.setState({
      addInputVisible: show,
    });
  };

  public handleChangeInput = e => {
    this.setState({ newOption: e.target.value });
  };

  public handleRemoveOption = (option, e) => {
    e.stopPropagation();
    if (!option.checked) {
      this.props.removeOption(option.value);
    }
  };

  public render() {
    const { options, addBtnText, addInputPlaceHolder } = this.props;
    const { tagList, addInputVisible } = this.state;
    const btnAfter = <a onClick={() => this.handleAddOption(false)}>添加</a>;
    const menu = (
      <Menu className={'menu'}>
        {options.map(option => {
          const type = option.checked ? 'check' : 'close';
          return (
            <Menu.Item
              key={option.key}
              onClick={() => this.handleClickOption(option)}
              className={classnames(
                styles[`menu_item__${type}`],
                styles.menu_item,
              )}
            >
              <span style={{ float: 'left' }}>{option.value}</span>
              <Icon
                type={type}
                theme="outlined"
                className={classnames(
                  styles[`menu_icon__${type}`],
                  styles.menu_icon,
                )}
                onClick={e => this.handleRemoveOption(option, e)}
              />
            </Menu.Item>
          );
        })}
        <Menu.Divider
          style={{ display: options && options.length > 0 ? '' : 'none' }}
        />
        <Menu.Item key="add">
          {addInputVisible ? (
            <React.Fragment>
              <Input
                addonAfter={btnAfter}
                placeholder={addInputPlaceHolder}
                value={this.state.newOption}
                onChange={e => this.handleChangeInput(e)}
                maxLength={10}
              />
            </React.Fragment>
          ) : (
            <a onClick={() => this.toggleAddOption(true)}>
              {addBtnText ? addBtnText : '添加'}
            </a>
          )}
        </Menu.Item>
      </Menu>
    );
    return (
      <React.Fragment>
        <Dropdown
          overlay={menu}
          trigger={['click']}
          onVisibleChange={this.handleVisibleChange}
          visible={this.state.visible}
          placement={'bottomLeft'}
          getPopupContainer={triggerNode => triggerNode.parentNode}
        >
          <div className={'ant-select ant-select-enabled'}>
            <div
              className={'ant-select-selection ant-select-selection--multiple'}
            >
              <div className={'ant-select-selection__rendered'}>
                {tagList && tagList.size > 0 ? (
                  tagList.map(tag => (
                    <Tag
                      closable
                      onClose={e => this.handleCloseTag(tag, e)}
                      key={tag.get('key')}
                    >
                      {tag.get('value')}
                    </Tag>
                  ))
                ) : (
                  <div className={'ant-select-selection__placeholder'}>
                    请选择
                  </div>
                )}
              </div>
            </div>
          </div>
        </Dropdown>
      </React.Fragment>
    );
  }
}
