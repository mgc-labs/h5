/**
 * LoadingIndicator Component
 * @author ryan bian
 */

import Button from 'antd/es/button';
import Icon from 'antd/es/icon';
import Spin from 'antd/es/spin';
import * as React from 'react';

import styles from './index.module.less';

export interface ILoadingIndicatorProps {
  error?: boolean;
}

const LoadingIndicator = (props: ILoadingIndicatorProps) => {
  if (props.error) {
    return (
      <div className={styles['LoadingIndicator--failed']}>
        <div>
          <Icon type="frown" theme="outlined" style={{ marginRight: 8 }} />
          检测到系统有更新，建议重新刷新后加载页面！
        </div>
        <Button
          onClick={() => {
            window.location.reload();
          }}
        >
          立即刷新
        </Button>
      </div>
    );
  }
  return (
    <div className={styles.LoadingIndicator}>
      <Spin />
    </div>
  );
};

export default LoadingIndicator;
