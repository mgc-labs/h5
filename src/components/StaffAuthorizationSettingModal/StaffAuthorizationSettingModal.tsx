/**
 * StaffAuthorization Component
 * @author yanrong.tian
 * @date 2018-9-19 13:41:45
 */
import Card from 'antd/es/card';
import message from 'antd/es/message';
import Modal from 'antd/es/modal';
import Table from 'antd/es/table';
import * as React from 'react';

import Popconfirm from 'antd/es/popconfirm';
import StaffAuthorizationSetting from 'components/StaffAuthorizationSetting';

import styles from 'components/StaffAuthorizationSettingModal/index.module.less';
import { saveAuthorizationSetting } from 'components/StaffAuthorizationSettingModal/service';

interface Istaff {
  userName: string | number;
  utmsUsersId: number;
  realName: string;
}

interface IProps {
  onOk: () => void;
  onClose: () => void;
  visible: boolean;
  staff: [Istaff];
  versionsCode: string;
}

interface IState {
  staff: any[];
  initial: string;
}

class StaffAuthorizationSettingModal extends React.PureComponent<IProps> {
  utmsResourceIds: any[];
  utmsUsersIds: any[];
  state: IState;
  constructor(props) {
    super(props);
    this.state = {
      staff: [],
      initial: '',
    };
  }

  public componentWillReceiveProps(nextProps) {
    if (nextProps.staff !== this.state.staff) {
      this.setState({
        staff: nextProps.staff,
      });
    }
  }

  public render() {
    const { staff } = this.state;
    const props = this.props;
    return (
      <Modal
        title={'给下列员工分配权限'}
        width={800}
        {...props}
        bodyStyle={{ padding: 0 }}
        onOk={this.save}
        onCancel={this.handleCancel}
        centered={true}
        maskClosable={false}
      >
        <div className={styles.staffAuthorizationContent}>
          <Card className={styles.staffList} bodyStyle={{ padding: '0' }}>
            <div className={styles.tableBox}>{this.renderStaffList(staff)}</div>
          </Card>
          <div className={styles.content}>
            <StaffAuthorizationSetting
              onCheck={this.hanldCheck}
              staff={staff}
              versionsCode={props.versionsCode}
              getInitialKeys={keys => this.setInitialKeys(keys)}
            />
          </div>
        </div>
      </Modal>
    );
  }

  private setInitialKeys = keys => {
    this.setState({
      initial: keys,
    });
  };

  private handleCancel = () => {
    const { onClose } = this.props;
    const initial = this.state.initial
      .split(',')
      .sort()
      .join(',');
    const update = this.utmsResourceIds.sort().join(',');
    if (initial === update) {
      onClose();
    } else {
      Modal.confirm({
        title: '确认要取消修改吗？',
        onOk() {
          onClose();
        },
      });
    }
  };

  private save = async () => {
    const utmsUsersIds = this.utmsUsersIds;
    const utmsResourceIds = this.utmsResourceIds || [];
    if (!(utmsUsersIds && utmsUsersIds.length)) {
      return;
    }
    try {
      const data = await saveAuthorizationSetting({
        utmsResourceIds: utmsResourceIds.join(','),
        utmsUsersIds: utmsUsersIds.join(','),
      });
      message.success('权限分配成功！');
      this.props.onOk();
    } catch (err) {
      const msg = err && err.msg ? err.msg : '网络错误，请稍后...';
      message.error(msg);
    }
  };

  private hanldCheck = checkedKeys => {
    this.utmsResourceIds = checkedKeys;
  };

  private renderStaffList = staff => {
    const columns = [
      {
        title: '姓名',
        dataIndex: 'realName',
        key: 'realName',
      },
      {
        title: '岗位',
        dataIndex: 'postNames',
        key: 'postNames',
      },
    ];

    const utmsUsersIds = [];
    staff.map(item => {
      utmsUsersIds.push(item.utmsUsersId);
    });
    this.utmsUsersIds = utmsUsersIds;
    return (
      <Table
        columns={columns}
        dataSource={staff}
        rowKey={'utmsUsersId'}
        pagination={false}
        size="small"
        bordered={false}
      />
    );
  };
}

export default StaffAuthorizationSettingModal;
