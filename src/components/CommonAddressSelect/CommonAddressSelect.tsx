/**
 * CommonAddressSelect Component
 * @author lhf
 * @date 2018-9-17 17:54:14
 */
import * as React from 'react';

import Button from 'antd/es/button';
import Col from 'antd/es/col';
import Form from 'antd/es/form';
import { WrappedFormUtils } from 'antd/es/form/Form';
import Input from 'antd/es/input';
import Modal from 'antd/es/modal';
import Row from 'antd/es/row';
import Table from 'antd/es/table';
import styles from './index.module.less';
import fetchAddress from './service';

const FormItem = Form.Item;
export interface ICommonAddressSelectProps {
  visible: boolean; // 是否显示
  form: WrappedFormUtils;
  afterClose?: () => void; // 关闭之后的方法
  onChange?: (data?: any) => void; // 获取当前选中的item
}

interface ICommonAddressSelectState {
  list: object[];
  visible: boolean;
  loading: boolean;
  totalCount: number;
  pageIndex: number;
}

class CommonAddressSelect extends React.PureComponent<
  ICommonAddressSelectProps,
  ICommonAddressSelectState
> {
  selectedItem: any;
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      visible: false,
      loading: false,
      totalCount: 1,
      pageIndex: 1,
    };
  }

  public componentWillReceiveProps(nextProps) {
    if (nextProps.visible !== this.props.visible) {
      this.setShow(nextProps);
    }
  }

  public render() {
    const { visible } = this.state;
    const { afterClose } = this.props;
    return (
      <div className={styles.CommonAddressSelect}>
        <Modal
          visible={visible}
          title="选择常用地址"
          afterClose={afterClose}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
          centered={true}
          maskClosable={false}
        >
          {this.renderFilterForm()}
          {this.renderList()}
        </Modal>
      </div>
    );
  }

  private renderFilterForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className={styles.CommonAddressSelect__Form} layout="inline">
        <Row type="flex" style={{ marginBottom: 10 }}>
          <Col span={15}>
            <FormItem label="地址搜索">
              {getFieldDecorator('keyword')(<Input />)}
            </FormItem>
          </Col>
          <Col span={9} className={styles.CommonAddressSelect__RightAction}>
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleSubmit}
            >
              查 询
            </Button>
            <Button style={{ marginLeft: 12 }} onClick={this.handleReset}>
              重 置
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }

  private renderList() {
    const { loading, totalCount, pageIndex, list } = this.state;
    const tableProps = {
      columns: [
        {
          title: '地址名称',
          dataIndex: 'depotName',
        },
        {
          title: '类型',
          dataIndex: 'depotType',
          render: text => <span>{text}</span>,
        },
        {
          title: '详细地址',
          dataIndex: 'address',
        },
        {
          title: '操作',
          key: 'action',
          render: record => (
            <span>
              <a
                href="javascript:void(0);"
                onClick={() => {
                  this.handleClickRow(record);
                }}
              >
                选择
              </a>
            </span>
          ),
        },
      ],
      dataSource: list.map(d => {
        d.key = d.utmsDepotId;
        return d;
      }),
      loading,
      pagination: {
        current: pageIndex,
        total: totalCount,
        size: 'small',
        onChange: (index, pageSize) => {
          this.fetchData(this.props.form.getFieldsValue(), index);
        },
      },
    };
    return (
      <Table
        onRow={record => {
          return {
            onClick: () => {
              this.handleClickRow(record);
            },
          };
        }}
        {...tableProps}
      />
    );
  }

  private handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  };

  private handleCancel = () => {
    this.setState({ visible: false });
  };

  // 重新set
  private setShow(props) {
    this.setState({ visible: props.visible });
    // 新打开时，置空当前选中的item
    if (props.visible) {
      const { onChange } = this.props;
      onChange(null);
      this.fetchData(this.props.form.getFieldsValue(), 1);
    }
  }

  // 查询
  private handleSubmit = e => {
    e.preventDefault();
    this.fetchData(this.props.form.getFieldsValue(), 1);
  };
  // 重置
  private handleReset = () => {
    this.props.form.resetFields();
    this.fetchData({}, 1);
  };

  // 查询数据
  private fetchData(data, index) {
    const param = {
      keyword: data.keyword,
      pageSize: 10,
      skipCount: (index - 1) * 10,
    };
    fetchAddress(param)
      .then(result => {
        this.setState({
          list: result.data,
          totalCount: result.count,
          pageIndex: index,
        });
      })
      .catch(e => {});
  }

  // 选中一行
  private handleClickRow(data) {
    this.selectedItem = data;
    const { onChange } = this.props;
    onChange(data);
    this.setState({
      visible: false,
    });
  }
}
export default Form.create()(CommonAddressSelect);
