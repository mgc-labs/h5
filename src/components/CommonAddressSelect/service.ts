/**
 * CommonAddressSelect Service
 * @author lhf
 * @date 2018-9-18 14:32:18
 */
import qs from 'qs';
import request, { mocker } from 'utils/request';
import { USE_MOCK } from '../../CONFIG';

const API_GET_ADDRESS = '/ehuodiGateway/utmsCore/utmsDepotcs/selectDepotList?i';

if (process.env.NODE_ENV === 'development') {
  // 只在开发环境有效
  // 生产环境通过 tree shaking 可以消除这段代码
  mocker
    .on('post', API_GET_ADDRESS)
    .reply(() => import('./mock').then(exports => [200, exports.default]));
}

const fetchAddress = options => {
  return request(
    {
      method: 'post',
      url: API_GET_ADDRESS,
      data: qs.stringify({
        ...options,
        pageSize: 10,
      }),
    },
    {
      useMock: USE_MOCK,
    },
  ).then(res => res);
};

export default fetchAddress;
