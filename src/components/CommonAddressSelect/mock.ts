export default {
  result: 'success',
  count: 100,
  data: [
    {
      utmsDepotId: 101419,
      gmtCreate: '2018-09-07 10:10:12',
      gmtModified: '2018-09-07 10:10:12',
      createOperatorId: 'a0111',
      createOperatorName: '张三',
      depotType: 'Z00001_K001', // 客户编码
      depotName: '美团', // 客户名称
      contact: '王经理', // 联系人
      contactWay: '18109281203', // 联系方式
      province: '',
      city: '',
      region: '',
      address: 'tttttttttttt',
      longitude: 120.02823,
      latitude: 30.02823,
      isDefault: '1',
    },
    {
      utmsDepotId: 101619,
      gmtCreate: '2018-09-17 10:10:12',
      gmtModified: '2018-09-17 10:10:12',
      createOperatorId: 'a0112',
      createOperatorName: '李四',
      depotType: 'Z00001_K002', // 客户编码
      depotName: '美团11', // 客户名称
      contact: '王经理11', // 联系人
      contactWay: '18109281203', // 联系方式
      province: '',
      city: '',
      region: '',
      address: 'aaaaaaaa',
      longitude: 120.02823,
      latitude: 30.02823,
      isDefault: '0',
    },
    {
      utmsDepotId: 101519,
      gmtCreate: '2018-09-12 10:10:12',
      gmtModified: '2018-09-12 10:10:12',
      createOperatorId: 'a0113',
      createOperatorName: '王二',
      depotType: 'Z00001_K001', // 客户编码
      depotName: '美团22', // 客户名称
      contact: '王经理22', // 联系人
      contactWay: '18109281203', // 联系方式
      province: '',
      city: '',
      region: '',
      address: 'bbbbbb',
      longitude: 120.12823,
      latitude: 30.12823,
      isDefault: '0',
    },
  ],
};
