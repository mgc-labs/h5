/**
 * 密码强度提示组件
 * @author ryan bian
 * @date 2018-10-15 10:42:13
 */
import classnames from 'classnames';
import * as React from 'react';
import zxcvbn from 'zxcvbn';

import styles from './index.module.less';

export interface IPasswordStrengthProps {
  value: string;
  style: object;
}

const PasswordStrength = (
  props: IPasswordStrengthProps = { value: '', style: {} },
) => (
  <div className={classnames(styles.PasswordStrength)} style={props.style}>
    <div
      className={styles['PasswordStrength--fill']}
      data-score={zxcvbn(props.value).score}
    />
  </div>
);

export default PasswordStrength;
