/**
 * PageBottom Component
 * @author djd
 * @date 2018/9/14 上午8:12:01
 */
import * as React from 'react';
import { connect } from 'react-redux';

import classnames from 'classnames';

import { createStructuredSelector } from 'reselect';

import styles from './index.module.less';

import { makeSelectMenuCollapsed } from './selectors';

export interface IPageBottomProps {
  leftChild?: JSX.Element;
  rightChild?: JSX.Element | JSX.Element[];
  collapsed: boolean;
  children?: JSX.Element;
}

const PageBottomComponent = (props: IPageBottomProps) => {
  return (
    <React.Fragment>
      <div
        className={classnames(
          styles.pageBottomMargin,
          props.collapsed ? styles.menuCollapsed : null,
        )}
      />
      <div
        className={classnames(
          styles.PageBottom,
          props.collapsed ? styles.menuCollapsed : null,
        )}
      >
        <div className={styles.left}>{props.leftChild}</div>
        <div className={styles.center}>{props.children}</div>
        <div className={styles.right}>{props.rightChild}</div>
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = createStructuredSelector({
  collapsed: makeSelectMenuCollapsed(),
});

export const PageBottom = connect(
  mapStateToProps,
  () => ({}),
)(PageBottomComponent);
