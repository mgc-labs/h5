/**
 * PageBottomButtons Component
 * @author djd
 * @date 2018/9/15 上午8:16:12
 */
import * as React from 'react';

import styles from './index.module.less';

type Element = JSX.Element;

export interface IPageBottomButtonsProps {
  buttons?: Element[];
}

export const PageBottomButtons = (props: IPageBottomButtonsProps) => (
  <div className={styles.PageBottomButtons}>
    {props.buttons.map((item, index) => (
      <div key={index} className={styles.button}>
        {item}
      </div>
    ))}
  </div>
);
