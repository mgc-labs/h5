import { createSelector } from 'reselect';

const globalState = state => state.get('global');

export const makeSelectMenuCollapsed = () =>
  createSelector(globalState, (state: Map<string, any>) =>
    state.getIn(['ui', 'collapsed']),
  );
