export const PageBottom: string;
export const left: string;
export const right: string;
export const center: string;
export const menuCollapsed: string;
export const pageBottomMargin: string;
