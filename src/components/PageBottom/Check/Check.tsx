/**
 * PageBottomCheck Component
 * @author djd
 * @date 2018/9/15 上午8:44:47
 */
import * as React from 'react';

import styles from './index.module.less';

import CheckBox, { CheckboxChangeEvent } from 'antd/es/checkbox';

export interface IPageBottomCheckProps {
  total: number;
  checked: number;
  onAllCheckChanged: (checked: boolean) => void;
}

export const PageBottomCheck = (props: IPageBottomCheckProps) => (
  <div className={styles.PageBottomCheck}>
    <CheckBox
      indeterminate={props.checked < props.total && props.checked > 0}
      onChange={e => props.onAllCheckChanged(e.target.checked)}
      checked={props.checked === props.total && props.checked > 0}
    >
      全选
    </CheckBox>

    <span>
      选中：
      {props.checked || 0}/{props.total || 0}
    </span>
  </div>
);
