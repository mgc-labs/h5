import Divider from 'antd/es/divider';
import Dropdown from 'antd/es/dropdown';
import Menu from 'antd/es/menu';
import * as React from 'react';
import styles from './styles.module.less';

export interface IOptionButtonsProps {
  count?: number;
}

export class OptionButtons extends React.PureComponent<
  IOptionButtonsProps,
  any
> {
  static defaultProps = {
    count: 2,
  };
  static optionButtonsList = []; // 页面中所有此组件的列表，这里是为了当打开更多时，关闭其它的更多

  constructor(props: IOptionButtonsProps) {
    super(props);
    this.state = {
      isShowMenuList: false,
    };
  }

  onBodyClick = e => {
    let obj = e.target;
    while (obj) {
      const pn = obj.parentNode;
      if (
        pn &&
        pn.className &&
        pn.className.indexOf &&
        pn.className.indexOf('ant-popover-buttons') > -1
      ) {
        obj = null;
        break;
      }
      const cn = obj.className;
      if (
        (cn && cn.indexOf && cn.indexOf(styles.divMore) > -1) || // 点击"更多"按钮时不关闭
        (cn && cn.indexOf && cn.indexOf(styles.moreDropDown) > -1) || // 点击 "菜单时"不关闭
        (cn && cn.indexOf && cn.indexOf('ant-popover') > -1) // 点击 "Popconfirm" 时不关闭
      ) {
        break;
      }
      obj = obj.parentElement;
    }
    if (!obj) {
      this.setState({
        isShowMenuList: false,
      });
    }
  };

  componentWillMount() {
    document.body.addEventListener('click', this.onBodyClick);
    OptionButtons.optionButtonsList.push(this);
  }
  componentWillUnmount() {
    document.body.removeEventListener('click', this.onBodyClick);

    const index = OptionButtons.optionButtonsList.indexOf(this);
    OptionButtons.optionButtonsList.splice(index, 1);
  }

  getShowList() {
    const children = this.getChildren();
    const cut =
      children.length > this.props.count
        ? this.props.count - 1
        : this.props.count;
    const list = children.slice(0, cut) as any[];

    return list.map((item, index, array) => {
      if (index < array.length - 1) {
        return (
          <React.Fragment key={index}>
            <div key={index}>{item}</div>
            <Divider type="vertical" />
          </React.Fragment>
        );
      }
      return <div key={index}>{item}</div>;
    });
  }
  getHideList() {
    const children = this.getChildren();
    const cut =
      children.length > this.props.count
        ? this.props.count - 1
        : this.props.count;

    const list = children.slice(cut) as any[];
    if (list.length > 0) {
      const menu = (
        <Menu onClick={this.onMoreMenuItemClick}>
          {list.map((item, index) => {
            return <Menu.Item key={index}>{item}</Menu.Item>;
          })}
        </Menu>
      );

      return (
        <React.Fragment>
          <Divider type="vertical" />
          <div className={styles.divMore}>
            <Dropdown
              overlay={menu}
              visible={this.state.isShowMenuList}
              overlayClassName={styles.moreDropDown} // 主要是为了z-index，需要将菜单放到 Popconfirm 下面
            >
              <a
                className={styles.moreBtn}
                onMouseEnter={this.showMoreMenuList}
              >
                更多
              </a>
            </Dropdown>
          </div>
        </React.Fragment>
      );
    }

    return null;
  }

  onMoreMenuItemClick = pars => {
    // 菜单项是 Popconfirm 的不关闭菜单
    const type = pars.item.props.children.type;
    if (!type || type.displayName !== 'Popconfirm') {
      this.hideMoreMenuList();
      return;
    }
  };

  showMoreMenuList = () => {
    this.setState({
      isShowMenuList: true,
    });
    OptionButtons.optionButtonsList.map((h: any) => {
      if (this !== h) {
        h.hideMoreMenuList();
      }
    });
  };
  hideMoreMenuList = () => {
    this.setState({
      isShowMenuList: false,
    });
  };

  getChildren(): any[] {
    const list: any[] = [].concat(this.props.children);
    // if (this.props.children instanceof Array) {
    //   list = this.props.children;
    // } else {
    //   list = [this.props.children];
    // }

    return list.filter(h => h !== null && h !== true && h !== false);
  }

  render() {
    return (
      <div className={styles.optionButtons}>
        {this.getShowList()}
        {this.getHideList()}
      </div>
    );
  }
}
