/**
 * Create Store
 */
import {
  connectRouter,
  routerMiddleware,
} from 'connected-react-router/immutable';
import { fromJS, isImmutable } from 'immutable';
import * as React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
// import { createSagaMonitor, DockableSagaView } from 'redux-saga-devtools';
import rootSaga from 'utils/persistence/';
import createReducer from './reducers';

// const monitor = createSagaMonitor();
const sagaMiddleware = createSagaMiddleware({
  /*sagaMonitor: monitor*/
});

const logger = createLogger({
  level: 'info',
  stateTransformer: state => {
    if (isImmutable(state)) {
      return state.toJS();
    }
    const output = {};
    Object.keys(state).forEach(key => {
      if (isImmutable(state[key])) {
        Object.assign(output, {
          [key]: state[key].toJS(),
        });
      } else {
        Object.assign(output, {
          [key]: state[key],
        });
      }
    });
    return output;
  },
});

export default function configureStore(initialState = {}, history) {
  const rootReducer = createReducer();

  const store = ((window as any).store = createStore(
    connectRouter(history)(rootReducer),
    fromJS(initialState),
    compose(
      applyMiddleware(
        sagaMiddleware,
        routerMiddleware(history),
        logger,
        // ... other middlewares ...
      ),
    ),
  ));

  // Extensions
  Object.assign(store, {
    injectedReducers: {}, // Reducer registry
    injectedSagas: {}, // Saga registry
    runSaga: sagaMiddleware.run,
  });

  sagaMiddleware.run(rootSaga);

  // Make reducers hot reloadable, see http://mxs.is/googmo
  if ((module as any).hot) {
    (module as any).hot.accept('./reducers', () => {
      store.replaceReducer(connectRouter(history)(rootReducer));
    });
  }

  return Component => (
    <React.Fragment>
      <Provider store={store}>{Component}</Provider>
      {/* <DockableSagaView monitor={monitor} /> */}
    </React.Fragment>
  );
}
