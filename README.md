# TMS

## 基于以下搭建
- redux
- redux-saga
  - 官方文档：https://redux-saga-in-chinese.js.org/
  - Redux-Saga 漫谈：https://www.yuque.com/lovesueee/blog/redux-saga
  - redux-saga 实践总结：https://zhuanlan.zhihu.com/p/23012870
  - 从redux-thunk到redux-saga实践：https://segmentfault.com/a/1190000009928167
- immutable.js
- reselect
- react-router-dom
- ajax:
  - axios
  - axios-mock-adapter

## 项目初始化

1. `npm install`
2. Jarvis 中导入项目
3. 启动



